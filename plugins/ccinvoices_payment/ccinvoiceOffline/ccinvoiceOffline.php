<?php
/**
* @package    [ccInvoices Offlline payment plugin]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
/** Import library dependencies */
jimport('joomla.event.plugin');
$language = JFactory::getLanguage();
$language->load('plg_ccinvoiceOffline', JPATH_ROOT, 'en-GB', true);
$language->load('plg_ccinvoiceOffline', JPATH_ROOT, null, true);

class plgccinvoices_paymentccinvoiceOffline extends JPlugin
{
	function plgccinvoices_paymentccinvoiceOffline( &$subject , $config)
	{
		parent::__construct( $subject , $config);
		$this->_plugin = JPluginHelper::getPlugin( 'ccinvoices_payment', 'ccinvoiceOffline' );
		//$param = new JParameter( $this->_plugin->params );
		$params["plugin_name"] = "ccinvoiceOffline";
		$params["icon"] = "";
		$params["logo"] = "";
		$params["description"] = JText::_("PAYMENT_METHOD_DESC")."&nbsp;".$this->params->get("bankinfo");;
		$params["payment_method"] = JText::_("PAYMENT_METHOD_NAME");
		$params["payment_method_note"] = JText::_("PAYMENT_METHOD_NAME_NOTE");
		$params["bankinfo"] =$this->params->get("bankinfo");
		$this->params = $params;
	}

	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["icon"] = "";
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["payment_method_note"];
		$paymentIcon["logo"] = $this->params["logo"];
		return $paymentIcon;
	}
	function onPaymentMethodList($val)
	{
		$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->params["plugin_name"]."/images/".$this->params["logo"];
		$html ='<table cellpadding="5" cellspacing="0" height="60px;" width="100%" border="0">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td>
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<p style="margin:8px;text-align:justify;">'.$this->params["description"].'</p>
				</td>
				<td>
					&nbsp;
				</td>
			</tr>
		</table>';
		return $html;
	}
	function onAfterSuccessfulPayment($inv_arg)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT number FROM #__ccinvoices_invoices WHERE id =".$inv_arg[0]." LIMIT 1";
		$db->setQuery($sql);
		$number = $db->loadResult();
		return sprintf(JText::_("AFTER_SUCCESSFULL_MSG"),$number);
	}
	function onAfterFailedPayment($val)
	{
		return JText::_("AFTER_FAILED_MSG");
	}
}
?>