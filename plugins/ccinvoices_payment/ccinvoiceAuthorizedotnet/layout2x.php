
<script language='javascript'>
	function clickFormBtn()
	{
		//document.addtocart.submit1.click();
	}
	function adotnetSubmitForm()
	{

		<?php if($this->params["payment_method"]=="both"){ ?>
		if(document.getElementById('ccardradio').checked==true)
		{
			putDateValue();
			if(document.addtocart.x_card_num.value=='' )
			{
				alert('Credit Card Number Field is Empty!');
				return false;
			}
			else if(document.addtocart.card_code.value=='')
			{
				alert('Credit Card Security Code Field is Empty!');
				return false;
			}
			else
			{
				checkCardNumber(document.addtocart.x_card_num.value);
			}
		}
		else
		{
			if(document.addtocart.x_bank_name.value=='' )
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKNAME_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_num.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKACCNO_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_aba_code.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_ABNNO_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_name.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_NAMEONACC_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_type.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKACC_ALERT');?>');
				return false;
			}
			else
			{
				document.addtocart.submit1.click();
			}

		}
		<?php } ?>
	<?php if($this->params["payment_method"]=="echeck"){ ?>
			if(document.addtocart.x_bank_name.value=='' )
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKNAME_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_num.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKACCNO_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_aba_code.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_ABNNO_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_name.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_NAMEONACC_ALERT');?>');
				return false;
			}
			else if(document.addtocart.x_bank_acct_type.value=='')
			{
				alert('<?php echo JTEXT::_('CCINVOICES_CCCHECK_BANKACC_ALERT');?>');
				return false;
			}
			else
			{
				document.addtocart.submit1.click();
			}
		<?php } ?>
		<?php if($this->params["payment_method"]=="creditcard"){ ?>
			putDateValue();
			if(document.addtocart.x_card_num.value=='' )
			{
				alert('Credit Card Number Field is Empty!');
				return false;
			}
			else if(document.addtocart.card_code.value=='')
			{
				alert('Credit Card Security Code Field is Empty!');
				return false;
			}
			else
			{
				checkCardNumber(document.addtocart.x_card_num.value);
			}
		<?php } ?>

	}
	function putDateValue()
	{
		var mon;
		var yer;
		mon=document.addtocart.exp_date.value;
		yer=document.addtocart.exp_year.value;
		document.addtocart.x_exp_date.value=mon+yer;
	}
	function changeCCOptions(val)
	{

		if(val=="1")
		{
			document.getElementById('cccardfields').style.display='block';
			document.getElementById('echeckfields').style.display='none';
			document.addtocart.x_method.value="CC";
		}
		else
		{
			document.getElementById('cccardfields').style.display='none';
			document.getElementById('echeckfields').style.display='block';
			document.addtocart.x_method.value="ECHECK";
		}
	}
	function displayRecuring()
	{
		if(document.getElementById('x_echeck_type').value=="WEB" || document.getElementById('x_echeck_type').value=="TEL")
		{
			document.getElementById('recTR1').style.display='block';
			document.getElementById('recTR2').style.display='block';
		}
		else
		{
			document.getElementById('recTR1').style.display='none';
			document.getElementById('recTR2').style.display='none';
		}
	}
</script>
		<?php

		$fp_sequence = time();

		$api_login_id = $this->params["login_id"];
		$transaction_key = $this->params["transaction_key"];
		$amount = $newrows->total;
		$fp_timestamp = time();
		$fp_sequence = $val['id'].time(); // Enter an invoice or other unique number.
		$fingerprint = AuthorizeNetSIM_Form::getFingerprint($this->params["login_id"],  $this->params["transaction_key"],$amount,$fp_sequence, $fp_timestamp);
		//AuthorizeNetDPM::getCreditCardForm($this->params["plugin_name"],$newrows->total, $fp_sequence, $action_url,  $this->params["login_id"],  $this->params["transaction_key"],$test_mode,$val['id']);

		$expDate='<select name="exp_year" id="exp_year" onchange="putDateValue()">';
		for($i=date('Y');$i<=date('Y')+20;$i++)
		{
			$expDate.='<option value="'.substr($i,2,2).'">'.$i.'</option>';
		}
		$expDate.='</select>';


		 if($this->params["payment_method"]=="both")
		 {


		$html ='<table class="ccinvoices_paymentmethods">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<table style="padding:0px !important;">
						<tr>
							<td style="padding:0px !important;">
								<p style="text-align:left;">'.$this->params["payment_method_desc1"].'</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0px 0px 0px 0px !important;" align="left">
							<form name="radioselect" id="radioselect" ><table border=0 ><tr>
							<td style="text-align:left !important;padding-left:0px !important;"><input type="radio" name="xmethodtyp" id="ccardradio" onchange="changeCCOptions(1)" value="1" checked="checked" /> '.JText::_("CCINVOICES_CCCARD_LBL").'</td>
							<td style="text-align:left !important;padding-left:10px !important;"><input type="radio" name="xmethodtyp" id="echeckradio" onchange="changeCCOptions(0)" value="0" /> '.JText::_("CCINVOICES_CCCHECK_LBL").'</td>
							</tr></table></form>
							</td>
						</tr>
						<tr>
							<td style="padding:0px !important;">
								<form method="post" id="addtocart" name="addtocart" action="'.$redirectionUrl.'">
										<span id="echeckfields" style="display:none;">
											<table align="left" class="ccinvoices_authForm" width="100%">
												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_BANKNAME_LBL").'</td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_ECHECK_BANKNAME_LBL').'" name="x_bank_name" /></td>
												</tr>

												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_BANKACCNO_LBL").'</td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_ECHECK_BANKACCNO_LBL').'" name="x_bank_acct_num" /></td>
												</tr>

												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_ABNNO_LBL").'</td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_ECHECK_ABA_LBL').'" name="x_bank_aba_code" /></td>
												</tr>

												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_NAMEONACC_LBL").'</td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_ECHECK_NAME_ACC_LBL').'" name="x_bank_acct_name" /></td>
												</tr>

												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE").'</td>
												<td>
													<select name="x_bank_acct_type" >
														<option value="CK">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE1").'</option>
														<option value="SA">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE2").'</option>
														<option value="BC">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE3").'</option>
													</select>
												</td>
												</tr>
												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE").'</td>
												<td >
													<select style="width:210px;" name="x_echeck_type" id="x_echeck_type" onchange="displayRecuring()">
														<option value="ARC">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE1").'</option>
														<option value="BOC">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE2").'</option>
														<option value="CCD">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE3").'</option>
														<option value="PPD">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE4").'</option>
														<option value="TEL">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE5").'</option>
														<option value="WEB">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE6").'</option>
													</select>
												</td>
												</tr>

												<tr >
													<td ><div id="recTR1" style="display:none;">'.JText::_("CCINVOICES_CCCHECK_RECURINGBILL_LBL").'</div></td>
													<td ><div id="recTR2" style="display:none;">
														<table border=0 ><tr>
														<td style="text-align:left !important;padding-left:0px !important;"><input type="radio" name="x_recurring_billing" value="1" checked="checked" /> '.JText::_("CCINVOICES_RECRADIO_LBL1").'</td>
														<td style="text-align:left !important;padding-left:10px !important;"><input type="radio" name="x_recurring_billing" value="0" /> '.JText::_("CCINVOICES_RECRADIO_LBL0").'</td>
														</tr></table>
													</div></td>
												</tr>

											</table>
										</span>
										<span id="cccardfields">
											<table align="left" class="ccinvoices_authForm" >
												<tr>
												<td>Credit card type : </td>
												<td >
												<select name="card_type" id="card_type">
												<option value="visa">Visa</option>
												<option value="Master_Card">Master Card</option>
												<option value="American_Express">American Express</option>
												 <option value="Discover">Discover</option>
												</select>
												</td>
												</tr>

												<tr>
												<td>Credit card number : </td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_CC_CCNO_LBL').'" name="x_card_num" /></td>
												</tr>

												<tr>
												<td>Security code  : </td>
												<td><input type="text" placeholder="'.JText::_('CCINVOICES_AUTH_CC_SECODE_LBL').'" name="card_code" /></td>
												</tr>

												<tr>
												<td>Expiration date : </td>
												<td><select name="exp_date" id="exp_date" onchange="putDateValue()">
													<option value="01">01|January</option>
													<option value="02">02|February</option>
													<option value="03">03|March</option>
													<option value="04">04|April</option>
													<option value="05">05|May</option>
													<option value="06">06|June</option>
													<option value="07">07|July</option>
													<option value="08">08|August</option>
													<option value="09">09|September</option>
													<option value="10">10|October</option>
													<option value="11">11|November</option>
													<option value="12">12|December</option>
									                  </select>' .$expDate.
									                  		'
													  </td>
											  	</tr>
											</table>
										</span>
							<input type="hidden" name="x_login" value="'.$api_login_id.'" />
							<input type="hidden" name="x_fp_hash" value="'.$fingerprint.'" />
							<input type="hidden" name="x_amount" value="'.$amount.'" />
							<input type="hidden" name="x_fp_timestamp" value="'.$fp_timestamp.'" />
							<input type="hidden" name="x_fp_sequence" value="'.$fp_sequence.'" />
							<input type="hidden" name="x_version" value="3.1">
							<input type="hidden" name="x_show_form" value="payment_form">
							<input type="hidden" name="x_test_request" value="'.$test_mode.'" />
							<input type="hidden" name="x_method" value="CC">
							<INPUT TYPE="HIDDEN" NAME="x_relay_response" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_relay_url" VALUE="'.$x_relay_url.'">
							<INPUT TYPE="HIDDEN" NAME="x_relay_always" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_email" VALUE="'.$contactRows->email.'">
							<INPUT TYPE="HIDDEN" NAME="x_first_name" VALUE="'.$contactRows->name.'">
							<INPUT TYPE="HIDDEN" NAME="x_address" VALUE="'.$contactRows->address.'">
							<INPUT TYPE="HIDDEN" NAME="x_phone" VALUE="'.$contactRows->contact_number.'">
							<INPUT TYPE="HIDDEN" NAME="x_exp_date" VALUE="">
							<INPUT TYPE="submit" NAME="submit1" style="display:none;" onclick="return adotnetSubmitForm();" VALUE="">
						</form>
							</td>
						</tr>
					</table>
				</td>
				<td width="100" align="center" style="padding-right:15px;">
					<table cellpadding="0" cellspacing="0" border="0"> <tr> <td align="center" style="padding:0px 0px 0px 0px !important;">
					</td></tr><tr><td align="center">
						<a style="text-decoration:none;" href="javascript:void(0)" onclick="return adotnetSubmitForm();"><img border="0" src="'.JURI::root().'/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					</td></tr></table>
				</td>
			</tr>
		</table>
				<input type="hidden" name="path" id="pathadmin" value="'.JURI::root().'" />';
		 }
		 elseif($this->params["payment_method"]=="echeck")
		 {
$html ='<table class="ccinvoices_paymentmethods">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<table style="padding:0px !important;">
						<tr>
							<td style="padding:0px !important;">
								<p style="text-align:left;">'.$this->params["payment_method_desc1"].'</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0px !important;">
								<form method="post" id="addtocart" name="addtocart" action="'.$redirectionUrl.'">
										<span id="echeckfields" style="display:block;">
											<table align="left" class="ccinvoices_authForm" width="100%">
												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_BANKNAME_LBL").'</td>
												<td><input type="text" name="x_bank_name" /></td>
												</tr>

												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_BANKACCNO_LBL").'</td>
												<td><input type="text" name="x_bank_acct_num" /></td>
												</tr>

												<tr>
												<td>'.JText::_("CCINVOICES_CCCHECK_ABNNO_LBL").'</td>
												<td><input type="text" name="x_bank_aba_code" /></td>
												</tr>

												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_NAMEONACC_LBL").'</td>
												<td><input type="text" name="x_bank_acct_name" /></td>
												</tr>

												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE").'</td>
												<td>
													<select name="x_bank_acct_type" >
														<option value="CK">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE1").'</option>
														<option value="SA">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE2").'</option>
														<option value="BC">'.JText::_("CCINVOICES_CCCHECK_BANKACC_TYPE3").'</option>
													</select>
												</td>
												</tr>
												<tr>
												<td >'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE").'</td>
												<td >
													<select style="width:210px;" name="x_echeck_type" id="x_echeck_type" onchange="displayRecuring()">
														<option value="ARC">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE1").'</option>
														<option value="BOC">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE2").'</option>
														<option value="CCD">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE3").'</option>
														<option value="PPD">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE4").'</option>
														<option value="TEL">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE5").'</option>
														<option value="WEB">'.JText::_("CCINVOICES_CCCHECK_ECHECK_TYPE6").'</option>
													</select>
												</td>
												</tr>

												<tr >
													<td ><div id="recTR1" style="display:none;">'.JText::_("CCINVOICES_CCCHECK_RECURINGBILL_LBL").'</div></td>
													<td ><div id="recTR2" style="display:none;">
														<table border=0 ><tr>
														<td style="text-align:left !important;padding-left:0px !important;"><input type="radio" name="x_recurring_billing" value="1" checked="checked" /> '.JText::_("CCINVOICES_RECRADIO_LBL1").'</td>
														<td style="text-align:left !important;padding-left:10px !important;"><input type="radio" name="x_recurring_billing" value="0" /> '.JText::_("CCINVOICES_RECRADIO_LBL0").'</td>
														</tr></table>
													</div></td>
												</tr>

											</table>
										</span>
							<input type="hidden" name="x_login" value="'.$api_login_id.'" />
							<input type="hidden" name="x_fp_hash" value="'.$fingerprint.'" />
							<input type="hidden" name="x_amount" value="'.$amount.'" />
							<input type="hidden" name="x_fp_timestamp" value="'.$fp_timestamp.'" />
							<input type="hidden" name="x_fp_sequence" value="'.$fp_sequence.'" />
							<input type="hidden" name="x_version" value="3.1">
							<input type="hidden" name="x_show_form" value="payment_form">
							<input type="hidden" name="x_test_request" value="'.$test_mode.'" />
							<input type="hidden" name="x_method" value="CC">
							<INPUT TYPE="HIDDEN" NAME="x_relay_response" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_relay_url" VALUE="'.$x_relay_url.'">
							<INPUT TYPE="HIDDEN" NAME="x_relay_always" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_email" VALUE="'.$contactRows->email.'">
							<INPUT TYPE="HIDDEN" NAME="x_first_name" VALUE="'.$contactRows->name.'">
							<INPUT TYPE="HIDDEN" NAME="x_address" VALUE="'.$contactRows->address.'">
							<INPUT TYPE="HIDDEN" NAME="x_phone" VALUE="'.$contactRows->contact_number.'">
							<INPUT TYPE="HIDDEN" NAME="x_exp_date" VALUE="">
							<INPUT TYPE="submit" NAME="submit1" style="display:none;" onclick="return adotnetSubmitForm();" VALUE="">
						</form>
							</td>
						</tr>
					</table>
				</td>
				<td width="100" align="center" style="padding-right:15px;">
					<table cellpadding="0" cellspacing="0" border="0"> <tr> <td align="center" style="padding:0px 0px 0px 0px !important;">
					</td></tr><tr><td align="center">
						<a style="text-decoration:none;" href="javascript:void(0)" onclick="return adotnetSubmitForm();"><img border="0" src="'.JURI::root().'/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					</td></tr></table>
				</td>
			</tr>
		</table>
				<input type="hidden" name="path" id="pathadmin" value="'.JURI::root().'" />';
		 }
		 else
		 {
$html ='<table class="ccinvoices_paymentmethods">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<table style="padding:0px !important;">
						<tr>
							<td style="padding:0px !important;">
								<p style="text-align:left;">'.$this->params["payment_method_desc1"].'</p>
							</td>
						</tr>
						<tr>
							<td style="padding:0px !important;">
								<form method="post" id="addtocart" name="addtocart" action="'.$redirectionUrl.'">
										<span id="cccardfields">
											<table align="left" class="ccinvoices_authForm" >
												<tr>
												<td>Credit card type : </td>
												<td >
												<select name="card_type" id="card_type">
												<option value="visa">Visa</option>
												<option value="Master_Card">Master Card</option>
												<option value="American_Express">American Express</option>
												 <option value="Discover">Discover</option>
												</select>
												</td>
												</tr>

												<tr>
												<td>Credit card number : </td>
												<td><input type="text" name="x_card_num" /></td>
												</tr>

												<tr>
												<td>Security code  : </td>
												<td><input type="text" name="card_code" /></td>
												</tr>

												<tr>
												<td>Expiration date : </td>
												<td><select name="exp_date" id="exp_date" onchange="putDateValue()">
													<option value="01">01|January</option>
													<option value="02">02|February</option>
													<option value="03">03|March</option>
													<option value="04">04|April</option>
													<option value="05">05|May</option>
													<option value="06">06|June</option>
													<option value="07">07|July</option>
													<option value="08">08|August</option>
													<option value="09">09|September</option>
													<option value="10">10|October</option>
													<option value="11">11|November</option>
													<option value="12">12|December</option>
									                  </select>
													 ' .$expDate.
									                  		'</td>
											  	</tr>
											</table>
										</span>
							<input type="hidden" name="x_login" value="'.$api_login_id.'" />
							<input type="hidden" name="x_fp_hash" value="'.$fingerprint.'" />
							<input type="hidden" name="x_amount" value="'.$amount.'" />
							<input type="hidden" name="x_fp_timestamp" value="'.$fp_timestamp.'" />
							<input type="hidden" name="x_fp_sequence" value="'.$fp_sequence.'" />
							<input type="hidden" name="x_version" value="3.1">
							<input type="hidden" name="x_show_form" value="payment_form">
							<input type="hidden" name="x_test_request" value="'.$test_mode.'" />
							<input type="hidden" name="x_method" value="CC">
							<INPUT TYPE="HIDDEN" NAME="x_relay_response" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_relay_url" VALUE="'.$x_relay_url.'">
							<INPUT TYPE="HIDDEN" NAME="x_relay_always" VALUE="TRUE">
							<INPUT TYPE="HIDDEN" NAME="x_email" VALUE="'.$contactRows->email.'">
							<INPUT TYPE="HIDDEN" NAME="x_first_name" VALUE="'.$contactRows->name.'">
							<INPUT TYPE="HIDDEN" NAME="x_address" VALUE="'.$contactRows->address.'">
							<INPUT TYPE="HIDDEN" NAME="x_phone" VALUE="'.$contactRows->contact_number.'">
							<INPUT TYPE="HIDDEN" NAME="x_exp_date" VALUE="">
							<INPUT TYPE="submit" NAME="submit1" style="display:none;" onclick="return adotnetSubmitForm();" VALUE="">
						</form>
							</td>
						</tr>
					</table>
				</td>
				<td width="100" align="center" style="padding-right:15px;">
					<table cellpadding="0" cellspacing="0" border="0"> <tr> <td align="center" style="padding:0px 0px 0px 0px !important;">
					</td></tr><tr><td align="center">
						<a style="text-decoration:none;" href="javascript:void(0)" onclick="return adotnetSubmitForm();"><img border="0" src="'.JURI::root().'/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					</td></tr></table>
				</td>
			</tr>
		</table>
				<input type="hidden" name="path" id="pathadmin" value="'.JURI::root().'" />';
		 }
	?>