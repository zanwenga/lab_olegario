<?php

/**
* @package    [ccInvoices Authorizedotnet payment plugin]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

defined('_JEXEC') or die('Restricted access');

/** Import library dependencies */
jimport('joomla.event.plugin');


$language = JFactory::getLanguage();
$language->load('plg_ccinvoiceAuthorizedotnet', JPATH_ROOT, 'en-GB', true);
$language->load('plg_ccinvoiceAuthorizedotnet', JPATH_ROOT, null, true);
require_once dirname(__FILE__) .DS.'AuthorizeNet.php'; // The SDK

class plgccinvoices_paymentccinvoiceAuthorizedotnet extends JPlugin
{
	function plgccinvoices_paymentccinvoiceAuthorizedotnet( &$subject, $config)
	{
		parent::__construct($subject, $config);
		$this->loadLanguage('ccinvoiceAuthorizedotnet');
		$params["plugin_name"] = "ccinvoiceAuthorizedotnet";
		$params["icon"] = "";
		$params["logo"] = "authorizedotnet.png";
		$params["description"] = JText::_("PAYMENT_METHOD_DESC");
		$params["payment_method"] = JText::_("PAYMENT_METHOD_NAME");
		$params["payment_method_note"] = JText::_("PAYMENT_METHOD_NAME_NOTE");
		$params["payment_method_desc1"] = JText::_("CCINVOICES_PAYMENT_METHOD_DESC1");
		$params["login_id"] = $this->params->get("login_id");
		$params["transaction_key"] = $this->params->get("transaction_key");
		$params["currency_code"] = $this->params->get('currency_code', "USD");
		$params["account_type"] = $this->params->get('account_type', "test");
		$params["payment_method"] = $this->params->get('payment_method', "both");
		$this->params = $params;
	}
	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["icon"] = $this->params["icon"];
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["description"];
		$paymentIcon["logo"] = $this->params["logo"];
		return $paymentIcon;
	}
	function onProcessPayment()
	{
		$ptype = JRequest::getVar('ptype','');
		$id = JRequest::getInt('id','');
		$html="";
		if($ptype == $this->params["plugin_name"])
		{
			$action = JRequest::getVar('pactiontype','');
			switch ($action)
			{
				case "process" :
				$html = $this->process($id);
				break;
				case "notify" :
				$html = $this->_notify_url();
				break;
				case "paymentmessage" :
				$html = $this->_paymentsuccess();
				break;
				default :
				$html =  $this->process($id);
				break;
			}
		}
		return $html;
	}
	function _notify_url()
	{

		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$id	= JRequest::getVar('id','0');

		$db->setQuery("SELECT * FROM #__ccinvoices_invoices WHERE MD5(id) ='".$id."' LIMIT 1");
		$newrows = $db->loadObject();

		$xRcode=JRequest::getVar('x_response_code','');
		if($xRcode== 1 ) //If payment is transferred update and redirect the page
		{

			$ptype=JRequest::getVar('ptype');

			$txn_id=JRequest::getVar('x_trans_id');;
			$pdate = gmdate("Y-m-d H:i:s");

			$sql = "UPDATE #__ccinvoices_invoices SET status = 4 WHERE id = ".$newrows->id;
			$db->setQuery($sql);
			$db->query();

			$query = 'INSERT INTO #__ccinvoices_payment ( inv_id, method , transaction_id , pdate, 	status)' .
					' VALUES ( "'.$newrows->id.'" ,"'.$this->params["payment_method_note"].'","'.$txn_id.'" ,"'.$pdate.'" ,"1" )'
					;
			$db->setQuery($query);
			$db->query();

		}
		?>
			<form id="invoiceForm" name="invoiceForm" method="post" action="<?php echo JURI::root().'index.php?option=com_ccinvoices&task=paymentReturnUrl&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($newrows->id);?>">
				<input type='hidden' name='tmpText' value='t' />
				<input type="submit" name="submit" id="btnFrom" style="display:none;" />
			</form>
		<script language="JavaScript" type="text/javascript">
			document.getElementById('btnFrom').click();
		</script>
		<?php
	}
	function onPaymentMethodList($val)
	{

		$db = JFactory::getDBO();
		$app = JFactory::getApplication();
		$user	= JFactory::getUser();
		$Itemid = JRequest::getInt("Itemid",'0');
		$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->params["plugin_name"]."/".$this->params["plugin_name"]."/images/".$this->params["logo"];
		//$action_url =  JRoute::_(JURI::root()."index.php");
		$action_url = JURI::root()."index.php?option=com_ccinvoices&task=processPayment&ptype=".$this->params["plugin_name"]."&pactiontype=notify&id=".MD5($val['id']);
		//$action_url = JURI::root()."test.php";
		$returnUrl=JRoute::_(JURI::root().'index.php?option=com_ccinvoices&task=paymentReturnUrl&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($val['id']), false);
		$x_relay_url=$action_url;


		if ($this->params["account_type"]=="test"){
			$test_mode = 'true';
			$redirectionUrl='https://test.authorize.net/gateway/transact.dll';
		}
		elseif ($this->params["account_type"]=="secure"){
			$test_mode = 'false';
			$redirectionUrl='https://secure.authorize.net/gateway/transact.dll';
		}


		$db->setQuery("SELECT * FROM #__ccinvoices_invoices WHERE id =".$val['id']." LIMIT 1");
		$newrows = $db->loadObject();

		$db->setQuery("SELECT * FROM #__ccinvoices_contacts WHERE id =".$newrows->contact_id." LIMIT 1");
		$contactRows = $db->loadObject();

		$html="";
		if($this->versionCompare()>=3)
		 {
			require_once JPATH_BASE.DS.'plugins'.DS.'ccinvoices_payment'.DS.'ccinvoiceAuthorizedotnet'.DS.'layout3x.php';
		 }
		 else
		 {
			require_once JPATH_BASE.DS.'plugins'.DS.'ccinvoices_payment'.DS.'ccinvoiceAuthorizedotnet'.DS.'layout2x.php';
		 }
		return $html;
	}
	function onAfterSuccessfulPayment($inv_arg)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT number FROM #__ccinvoices_invoices WHERE id =".$inv_arg[0]." LIMIT 1";
		$db->setQuery($sql);
		$number = $db->loadResult();
		return sprintf(JText::_("AFTER_SUCCESSFULL_MSG"),$number);
	}
	function onAfterFailedPayment($val)
	{
		return JText::_("AFTER_FAILED_MSG");
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,1);
	}
}

?>