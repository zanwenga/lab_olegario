<?php

/**
* @package    [ccInvoices Paypal payment plugin]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
/** Import library dependencies */
jimport('joomla.event.plugin');
$language = JFactory::getLanguage();
$language->load('plg_ccinvoicePaypal', JPATH_ROOT, 'en-GB', true);
$language->load('plg_ccinvoicePaypal', JPATH_ROOT, null, true);
class plgccinvoices_paymentccinvoicePaypal extends JPlugin
{
	function plgccinvoices_paymentccinvoicePaypal( &$subject , $config )
	{
		parent::__construct( $subject , $config);
		$this->_plugin = JPluginHelper::getPlugin( 'ccinvoices_payment', 'ccinvoicePaypal' );


		//$param = new JParameter( $this->_plugin->params );
		$params["plugin_name"] = "ccinvoicePaypal";
		$params["icon"] = "paypal_icon.png";
		$params["logo"] = "paypal_overview.png";
		$params["description"] = JText::_("PAYMENT_METHOD_DESC");
		$params["payment_method"] = JText::_("PAYMENT_METHOD_NAME");
		$params["payment_method_note"] = JText::_("PAYMENT_METHOD_NAME_NOTE");
		$params["testmode"] = $this->params->get("test");
		$params["currency_code"] = $this->getCurrency();
		$params["email_id"] = $this->params->get("email_id");
		$this->params = $params;

	}
	function getCurrency()
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration where id = 1';
		$db->setQuery($query);
		$config = $db->loadObject();

		$currency='';
		if($config->currency_symbol=="€" OR $config->currency_symbol=="EUR")
			$currency="EUR";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="USD")
			$currency="USD";
		elseif($config->currency_symbol=="£" OR $config->currency_symbol=="GBP")
			$currency="GBP";
		elseif($config->currency_symbol=="¥" OR $config->currency_symbol=="JPY")
			$currency="JPY";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="AUD")
			$currency="AUD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="CAD")
			$currency="CAD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="HKD")
			$currency="HKD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="NZD")
			$currency="NZD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="SGD")
			$currency="SGD";
		elseif($config->currency_symbol=="CHF" OR $config->currency_symbol=="CHF")
			$currency="CHF";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="NOK")
			$currency="NOK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="SEK")
			$currency="SEK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="DKK")
			$currency="DKK";
		return $currency;
	}
	function onProcessPayment()
	{
		$ptype = JRequest::getVar('ptype','');
		$id = JRequest::getInt('id','');
		$html="";
		if($ptype == $this->params["plugin_name"])
		{
			$action = JRequest::getVar('pactiontype','');
			switch ($action)
			{
				case "process" :
				$html = $this->process($id);
				break;
				case "notify" :
				$html = $this->_notify_url();
				break;
				case "paymentmessage" :
				$html = $this->_paymentsuccess();
				break;
				default :
				$html =  $this->process($id);
				break;
			}
		}
		return $html;
	}
	function _notify_url()
	{
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();
		$account_type=$this->params["testmode"];
		$error_email = $mainframe->getCfg("mailfrom");
		$site_name = $mainframe->getCfg("sitename");

		$em_headers = "From: <".$error_email.">\n";
		$em_headers .= "Reply-To: ".$error_email."\n";
		$em_headers .= "Return-Path: from_email\n";
		$em_headers .= "Organization: ".$site_name."\n";
		$em_headers .= "X-Priority: 3\n";
		$paypal_info = $_POST;
		$paypal_ipn = new paypal_ipn($paypal_info);
		foreach ($paypal_ipn->paypal_post_vars as $key=>$value)
		{
			if (getType($key)=="string")
			{
				eval("\$$key=\$value;");
			}
		}
		$paypal_ipn->send_response($account_type);
		$paypal_ipn->error_email = $error_email;
		if (!$paypal_ipn->is_verified())
		{
			$paypal_ipn->error_out("Bad order (PayPal says it's invalid)" . $paypal_ipn->paypal_response , $em_headers);
			die();
		}
		$paymentstatus=0;
		switch($paypal_ipn->get_payment_status())
		{
			case 'Pending':
			$pending_reason=$paypal_ipn->paypal_post_vars['pending_reason'];
			$msg = "Pending Payment - ".$pending_reason;
			break;

			case 'Completed':
			$msg = "Success";
			$id=$paypal_ipn->paypal_post_vars['custom'];
			$txn_id=$paypal_ipn->paypal_post_vars['txn_id'];
			$ptype=JRequest::getVar('ptype');
			$pdate = gmdate("Y-m-d H:i:s");
			$query = 'SELECT count(*)  FROM #__ccinvoices_payment where inv_id = "'.$id.'"';
			$db->setQuery( $query );
			$inv_id = $db->loadResult();
			if($id > 0)
			{
				$sql = "UPDATE #__ccinvoices_invoices SET status = 4 WHERE id = ".$id;
				$db->setQuery($sql);
				$db->query();

				// search query for id
				if($inv_id == 0)
				{
					$query = 'INSERT INTO #__ccinvoices_payment ( inv_id, method , transaction_id , pdate, 	status)' .
							' VALUES ( "'.$id.'" ,"'.$this->params["payment_method_note"].'","'.$txn_id.'" ,"'.$pdate.'" ,"1" )'
							;
					$db->setQuery($query);
					$db->query();
				}
			}
			break;

			case 'Failed':
			$msg = "Failed Payment";
			break;

			case 'Denied':
			$msg = "Denied Payment";
			break;

			case 'Refunded':
			$msg = "Refunded Payment";
			break;

			case 'Canceled':
			$msg = "Cancelled reversal";
			break;

			default:
			$msg = "Unknown Payment Status";
			break;
		}
	}
	function process($id)
	{

		$db = JFactory::getDBO();
		$db->setQuery("SELECT id,total,custom_invoice_number,number  FROM #__ccinvoices_invoices WHERE id =".$id." LIMIT 1");
		$row = $db->loadObject();
		$amount = $row->total;
		if($row->custom_invoice_number=="")
		{
			$itemname = "Invoice for ".$row->number;
		}
		else
		{
			$itemname = "Invoice for ".$row->custom_invoice_number;
		}
		$urlpaypal="";
		if ($this->params["testmode"]=="1")
		{
			$urlpaypal="https://www.sandbox.paypal.com/cgi-bin/webscr";
		}
		elseif ($this->params["testmode"]=="0")
		{
			$urlpaypal="https://www.paypal.com/cgi-bin/webscr";
		}
		$form ='<form id="paypalform" action="'.$urlpaypal.'" method="post">';
		$form .='<input type="hidden" name="cmd" value="_xclick">';
		$form .='<input id="custom" type="hidden" name="custom" value="'.$row->id.'">';
		$form .='<input type="hidden" name="business" value="'.$this->params["email_id"].'">';
		$form .='<input type="hidden" name="currency_code" value="'.$this->params["currency_code"].'">';
		$form .='<input type="hidden" name="item_name" value="'.$itemname.'">';
		$form .='<input type="hidden" name="amount" value="'.$amount.'">';
		$form .='<input type="hidden" name="cancel_return" value="'. JURI::root().'index.php?option=com_ccinvoices&task=paymentOverview&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($row->id).'">';
		$form .='<input type="hidden" name="notify_url"    value="'. JURI::root().'index.php?option=com_ccinvoices&controller=ccinvoices&task=processPayment&ptype='.$this->params["plugin_name"].'&pactiontype=notify">';
		$form .='<input type="hidden" name="return"        value="'. JURI::root().'index.php?option=com_ccinvoices&task=paymentReturnUrl&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($row->id).'">';
		$form .='</form>';
		echo $form;
	?>
		<script type="text/javascript">
			callpayment()
			function callpayment(){
				var id = document.getElementById('custom').value ;
				if ( id > 0 && id != '' ) {
					document.getElementById('paypalform').submit()
				}
			}
		</script>
	<?php
		exit;
	}
	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["icon"] = $this->params["icon"];
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["payment_method_note"];
		$paymentIcon["logo"] = $this->params["logo"];
		return $paymentIcon;
	}
	function onPaymentMethodList($val)
	{
		$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->params["plugin_name"]."/".$this->params["plugin_name"]."/images/".$this->params["logo"];
		$form_action = JRoute :: _("index.php?option=com_ccinvoices&controller=ccinvoices&task=processPayment&ptype=".$this->params["plugin_name"]."&pactiontype=process&id=".$val["id"], false);
		$html ='<table class="ccinvoices_paymentmethods" >
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<p style="text-align:justify;">'.$this->params["description"].'</p>
				</td>
				<td width="100" style="padding-right:15px;">
					<table cellpadding="0" cellspacing="0" border="0"> <tr> <td width="89" align="center" style="padding:0px 0px 0px 0px !important;">
					</td></tr><tr><td align="center">
						<a href="'.$form_action.'"><img src="'.JURI::root().'/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					</td></tr></table>
				</td>
			</tr>
		</table>';
		return $html;
	}
	function onAfterSuccessfulPayment($inv_arg)
	{

		$db = JFactory::getDBO();
		$sql = "SELECT number FROM #__ccinvoices_invoices WHERE id =".$inv_arg[0]." LIMIT 1";
		$db->setQuery($sql);
		$number = $db->loadResult();
		return sprintf(JText::_("AFTER_SUCCESSFULL_MSG"),$number);
	}
	function onAfterFailedPayment($val)
	{
		return JText::_("AFTER_FAILED_MSG");
	}
}
class paypal_ipn
{
	var $paypal_post_vars;
	var $paypal_response;
	var $timeout;
	var $error_email;
	function paypal_ipn($paypal_post_vars) {
		$this->paypal_post_vars = $paypal_post_vars;
		$this->timeout = 120;
	}
	function send_response($account_type)
	{
		$fp  = '';
		if($account_type == '1')
		{
			$fp = @fsockopen( "www.sandbox.paypal.com", 80, $errno, $errstr, 120 );
		}else if($account_type == '0')
		{
			$fp = @fsockopen( "www.paypal.com", 80, $errno, $errstr, 120 );
		}
		if (!$fp) {
			$this->error_out("PHP fsockopen() error: " . $errstr , "");
		} else {
			foreach($this->paypal_post_vars AS $key => $value) {
				if (@get_magic_quotes_gpc()) {
					$value = stripslashes($value);
				}
				$values[] = "$key" . "=" . urlencode($value);
			}
			$response = @implode("&", $values);
			$response .= "&cmd=_notify-validate";
			fputs( $fp, "POST /cgi-bin/webscr HTTP/1.0\r\n" );
			fputs( $fp, "Content-type: application/x-www-form-urlencoded\r\n" );
			fputs( $fp, "Content-length: " . strlen($response) . "\r\n\n" );
			fputs( $fp, "$response\n\r" );
			fputs( $fp, "\r\n" );
			$this->send_time = time();
			$this->paypal_response = "";

			while (!feof($fp)) {
				$this->paypal_response .= fgets( $fp, 1024 );

				if ($this->send_time < time() - $this->timeout) {
					$this->error_out("Timed out waiting for a response from PayPal. ($this->timeout seconds)" , "");
				}
			}
			fclose( $fp );
		}
	}
	function is_verified() {
		if( ereg("VERIFIED", $this->paypal_response) )
			return true;
		else
			return false;
	}
	function get_payment_status() {
		return $this->paypal_post_vars['payment_status'];
	}
	function error_out($message)
	{
		$em_headers = "From: <arun@adodis.com>\n";
		$em_headers .= "Reply-To: arun@adodis.com\n";
		$em_headers .= "Return-Path: from_email\n";
		$em_headers .= "Organization: ccinvoide\n";
		$em_headers .= "X-Priority: 3\n";
		$date = date("D M j G:i:s T Y", time());
		$message .= "\n\nThe following data was received from PayPal:\n\n";
		@reset($this->paypal_post_vars);
		while( @list($key,$value) = @each($this->paypal_post_vars)) {
			$message .= $key . ':' . " \t$value\n";
		}
	}
}
?>