<?php

/**
* @package    [ccInvoices Mollie iDEAL payment plugin]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

defined('_JEXEC') or die('Restricted access');
/** Import library dependencies */
jimport('joomla.event.plugin');

$language = JFactory::getLanguage();
$language->load('plg_ccinvoiceMollieIDEAL', JPATH_ROOT, 'en-GB', true);
$language->load('plg_ccinvoiceMollieIDEAL', JPATH_ROOT, null, true);
class plgccinvoices_paymentccinvoiceMollieIDEAL extends JPlugin
{
	const  MIN_TRANS_AMOUNT=1.18;
	protected $api_host = 'ssl://secure.mollie.nl';
	protected $api_port = 443;
	protected $error_message = '';
	protected $error_code = 0;
	protected $return_url = null;
	protected $report_url = null;
	protected $description = null;
	protected $partner_id = null;
	protected $transaction_id = null;
	protected $bank_url = null;
	protected $paid_status = false;
	protected $amount = 0;
	protected $consumer_info = array ();
	function setAmount($amount) {
		$amount = $amount * 100;
		if (!preg_match('~^[0-9]+$~', $amount)) {
			return false;
		}

		if (self :: MIN_TRANS_AMOUNT >= $amount) {
		return false;
			}
		return ($this->amount = $amount);
		}
	function plgccinvoices_paymentccinvoiceMollieIDEAL( &$subject , $config)
	{
		parent::__construct($subject, $config);
		JPlugin::loadLanguage( 'plg_ccinvoiceMollieIDEAL' );
		$this->_plugin = JPluginHelper::getPlugin( 'ccinvoices_payment', 'ccinvoiceMollieIDEAL' );
		//$param = new JParameter( $this->_plugin->params );
		$params["plugin_name"] = "ccinvoiceMollieIDEAL";
		$params["icon"] = "ideal_icon.png";
		$params["logo"] = "ideal_overview.png";
		$params["description"] = JText::_("PAYMENT_METHOD_DESC");
		$params["payment_method"] = JText::_("PAYMENT_METHOD_NAME");
		$params["payment_method_note"] = JText::_("PAYMENT_METHOD_NAME_NOTE");
		$params["test"] = $this->params->get("test");
		$params["partner_id"] = $this->params->get("partner_id");
		$this->params = $params;
	}
	function getCurrency()
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration where id = 1';
		$db->setQuery($query);
		$config = $db->loadObject();

		$currency='';
		if($config->currency_symbol=="€" OR $config->currency_symbol=="EUR")
			$currency="EUR";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="USD")
			$currency="USD";
		elseif($config->currency_symbol=="£" OR $config->currency_symbol=="GBP")
			$currency="GBP";
		elseif($config->currency_symbol=="¥" OR $config->currency_symbol=="JPY")
			$currency="JPY";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="AUD")
			$currency="AUD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="CAD")
			$currency="CAD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="HKD")
			$currency="HKD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="NZD")
			$currency="NZD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="SGD")
			$currency="SGD";
		elseif($config->currency_symbol=="CHF" OR $config->currency_symbol=="CHF")
			$currency="CHF";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="NOK")
			$currency="NOK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="SEK")
			$currency="SEK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="DKK")
			$currency="DKK";
		return $currency;
	}
	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["icon"] = $this->params["icon"];
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["payment_method_note"];
		$paymentIcon["logo"] = $this->params["logo"];
		return $paymentIcon;
	}
	function onProcessPayment()
	{
		$ptype = JRequest::getVar('ptype','');
		$id = JRequest::getInt('id','');
		$bank_id = JRequest::getVar('bank_id','');
		$html="";
		if($ptype == $this->params["plugin_name"])
		{
			$action = JRequest::getVar('pactiontype','');
			switch ($action)
			{
				case "process" :
				$html = $this->process($id,$bank_id);
				break;
				case "notify" :
				$html = $this->_notify();
				break;
				default :
				$html =  $this->process();
				break;
			}
		}
		return $html;
	}
	function _notify()
	{
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();
		$SiteName = $mainframe->getCfg('sitename');
		$id = JRequest::getInt("id","");
		if (!isset ($_REQUEST["transaction_id"]) || empty ($_REQUEST["transaction_id"]))
		{
			echo "Transaction ID is not set or emtpy!";
		}
		else
		{
			$transactionid = $_GET['transaction_id'];
			$this->setTransactionId($transactionid);
		}
		$result = $this->checkPayment($transactionid);
		if ($this->getPaidStatus() == true)
		{
			$payment->payment_status = $this->paid_status;
		}
		else
		{
			$payment->payment_status = $this->paid_status;
		}
		$pdate = gmdate("Y-m-d H:i:s");
		$query = 'SELECT count(*)  FROM #__ccinvoices_payment where inv_id = "'.$id.'"';
		$db->setQuery( $query );
		$inv_id = $db->loadResult();
		if($payment->payment_status)
		{
			if($id > 0)
			{
				$sql = "UPDATE #__ccinvoices_invoices SET status = 4 WHERE id = ".$id;
				$db->setQuery($sql);
				$db->query();
				// search query for id
				if($inv_id == 0)
				{
					$query = 'INSERT INTO #__ccinvoices_payment ( inv_id, method , transaction_id , pdate, 	status)' .
							' VALUES ( "'.$id.'" ,"'.$this->params["payment_method_note"].'" ,"'.$transactionid.'" ,"'.$pdate.'" ,"'.$payment->payment_status.'" )'
							;
					$db->setQuery($query);
					$db->query();
				}
			}
		}
	}
	protected function process($id,$bank_id)
	{
		$db =JFactory::getDBO();
		$db->setQuery("SELECT total,custom_invoice_number,number  FROM #__ccinvoices_invoices WHERE id =".$id." LIMIT 1");
		$row = $db->loadObject();
		$amount = $row->total;
		$amount = $this->setAmount($amount);

		if(trim($row->custom_invoice_number)!="")
			$description = sprintf(JText::_('CCINVOICES_INV_NO_LBL'),$row->custom_invoice_number);
		else
			$description = sprintf(JText::_('CCINVOICES_INV_NO_LBL'),$row->number);
		$html = '';
		if (!JRequest :: checkToken())
		{
			$text = JText::_('Invalid Token');
			$html = $this->changeToHtml($text);
			return $html;
		}
		if (!$id)
		{
			$text = JText::_('CC_INVALID_INVOICE');
			$html = $this->changeToHtml($text);
			return $html;
		}
		$partner_id = $this->params["partner_id"];
		$return_url = JURI::ROOT()."index.php?option=com_ccinvoices&controller=ccinvoices&task=paymentReturnUrl&ptype=".$this->params["plugin_name"]."&action=showresult&id=".MD5($id);
		$report_url = JURI::ROOT()."index.php?option=com_ccinvoices&controller=ccinvoices&task=processPayment&ptype=".$this->params["plugin_name"]."&pactiontype=notify&type=initial&amount=".$amount."&id=".$id;
		$create_xml = $this->_sendRequest($this->api_host, $this->api_port, '/xml/ideal/', 'a=fetch' .
		'&partnerid=' . urlencode($partner_id) .
		'&bank_id=' . urlencode($bank_id) .
		'&amount=' . urlencode($amount) .
		'&reporturl=' . urlencode($report_url) .
		'&description=' . urlencode($description) .
		'&returnurl=' . urlencode($return_url));
		if (empty ($create_xml))
		return false;
		$create_object = $this->_XMLtoObject($create_xml);
		if (!$create_object || $this->_XMLisError($create_object))
		return false;
		$this->transaction_id = $create_object->order->transaction_id;
		$this->bank_url = $create_object->order->URL;
		header("Location: " . $this->getBankURL());
		exit;

	}
	function changeToHtml($text = '')
	{
		$html = '';
		$html .= "
		<p>
		<table class='userlist'>
		<tbody>
		<tr>
		<td class='input'>
		{$text}
		</td>
		</tr>
		</tbody>
		</table>
		</p>
		";
		return $html;
	}
	function onPaymentMethodList($val)
	{
		JPlugin::loadLanguage( 'plg_ccinvoiceMollieIDEAL' );
		$form_token = JHTML :: _('form.token');
		$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->params["plugin_name"]."/".$this->params["plugin_name"]."/images/".$this->params["logo"];
		$form_action = JRoute :: _("index.php?option=com_ccinvoices&controller=ccinvoices&task=processPayment&ptype=".$this->params["plugin_name"]."&pactiontype=process", false);
		$partner_id = $this->params["partner_id"];
		$testmode = $this->params["test"];
		$set_testmode = $this->setTestmode($testmode);
		$set_partnerid = $this->setpartnerid($partner_id);
		$bank_array[] = $this->getBanks();
		$option = '';
		if(count($bank_array)>0)
		{
			foreach ($bank_array as $bank_names)
			{
				if($bank_names!="")
				{
					foreach ($bank_names as $bank_id => $bank_name)
					{
						$option .= "<option value=\"$bank_id\"> $bank_name</option>";
					}
				}
			}
		}
		$html ='<script language="javascript">function ccidealFormValid(form){try{if(document.ccidealform.bank_id.value == "" || document.ccidealform.bank_id.value == "null"){alert("'.JText::_("BANK_NAME_EMPTY").'");return false;}else{document.ccidealform.submit();}}catch(e){alert("'.JText::_("BANK_NAME_EMPTY").'");} }</script><table class="ccinvoices_paymentmethods">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<p style="text-align:justify;">'.$this->params["description"].'</p>
				</td>
				<td>';
					//<a href="'. JRoute::_('index.php?option=com_ccinvoices&task=paymentProcess&id='.$val["id"].'&paymentname='.$this->params["plugin_name"]).'"><img src="'.JURI::root().'/administrator/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					$imgroot=JURI::root();
				$html .= "<form action='$form_action' method='post' name='ccidealform' id='ccidealform' onsubmit='return ccidealFormValid(this);'>
				<table>
				<tr>
				<td align='right' style='padding-right:15px;'>
						<table cellpadding='0' cellspacing='0' border='0'> <tr> <td align='center' style='padding:0px 0px 0px 0px !important;'>
				<select name='bank_id'>
				<option value=''>".JText::_("SELECT_BANK")."</option>
				$option
				</select></td></tr><tr><td align='center' style='padding:0px 0px 0px 0px !important;'>
					<a style='text-decoration:none;' href='javascript:void(0)' onclick='ccidealFormValid(this)'><img src=".$imgroot."components/com_ccinvoices/assets/images/pay_now_button_active.png width='89' height='28' ></a>
							</td></tr></table>
					</td>
				</tr>
				</table>
				<input type='hidden' name='id' value='{$val["id"]}'>
				{$form_token}
				</form>";
				$html .='</td>
			</tr>
		</table>';
//<input type='submit' style='background:url("'.JURI::root().'/administrator/components/com_ccinvoices/assets/images/pay_now_button_active.png") no-repeat' name='submit' value='Pay via iDEAL' />
		return $html;
	}
	public function setpartnerid($partner_id, $api_host = 'ssl://secure.mollie.nl', $api_port = 443)
	{
		$this->partner_id = $partner_id;
		$this->api_host = $api_host;
		$this->api_port = $api_port;
	}
	protected function getBanks()
	{
		$banks_xml = $this->_sendRequest($this->api_host, $this->api_port, '/xml/ideal/', 'a=banklist' . (($this->testmode) ? '&testmode=true' : ''));
		if (empty ($banks_xml))
		{
			return false;
		}
		$banks_object = $this->_XMLtoObject($banks_xml);

		if (!$banks_object || $this->_XMlisError($banks_object))
		{
			return false;
		}
		$banks_array = array ();
		foreach ($banks_object->bank as $bank)
		{
			$banks_array["{$bank->bank_id}"] = "{$bank->bank_name}";
		}
		return $banks_array;
	}
	public function checkPayment($transaction_id)
	{
		if (!$this->setTransactionId($transaction_id))
		{
			$this->error_message = "Er is een onjuist transactie ID opgegeven";
			return false;
		}

		$check_xml = $this->_sendRequest($this->api_host, $this->api_port, '/xml/ideal/', 'a=check' .
		'&partnerid=' . urlencode($this->getPartnerId()) .
		'&transaction_id=' . urlencode($this->getTransactionId()) .
		(($this->testmode) ? '&testmode=true' : ''));
		if (empty ($check_xml))
		{
			return false;
		}

		$check_object = $this->_XMLtoObject($check_xml);

		if (!$check_object || $this->_XMLisError($check_object))
		{
			return false;
		}

		$this->paid_status = ($check_object->order->payed == 'true');
		$this->amount = $check_object->order->amount;
		$this->consumer_info = (isset ($check_object->order->consumer)) ? (array) $check_object->order->consumer : array ();
		return true;
	}
	public function getPartnerId()
	{
		$this->partner_id = $this->params["partner_id"];
		return $this->partner_id;
	}
	public function setTestmode($enable = true)
	{
		return ($this->testmode = $enable);
	}
	protected function _sendRequest($host, $port, $path, $data)
	{
		$hostname = str_replace('ssl://', '', $host);
		$fp = @fsockopen($host, $port, $errno, $errstr);
		$buf = '';
		if (!$fp)
		{
			$this->error_message = JText::_("COULD_NOT_CONNECT") . $errstr;
			$this->error_code = 0;
			return false;
		}
		@fputs($fp, "POST $path HTTP/1.0\n");
		@fputs($fp, "Host: $hostname\n");
		@fputs($fp, "Content-type: application/x-www-form-urlencoded\n");
		@fputs($fp, "Content-length: " . strlen($data) . "\n");
		@fputs($fp, "Connection: close\n\n");
		@fputs($fp, $data);
		while (!feof($fp))
		{
			$buf .= fgets($fp, 128);
		}
		fclose($fp);
		if (empty ($buf))
		{
			$this->error_message = JText::_("ZERO_SIZED_REPLY");
			return false;
		} else
		{
			list ($headers, $body) = preg_split("/(\r?\n){2}/", $buf, 2);
		}
		return $body;
	}
	protected function _XMLtoObject($xml)
	{
		try
		{
			$xml_object = new SimpleXMLElement($xml);
			if ($xml_object == false)
			{
				$this->error_message = JText::_("COULD_NOT_PROCESS_XML");
				return false;
			}
		} catch (Exception $e)
		{
			return false;
		}
		return $xml_object;
	}
	public function getPaidStatus() {
		return $this->paid_status;
	}
	public function getTransactionId() {
		return $this->transaction_id;
	}
	protected function _XMLisError($xml)
	{
		if (isset ($xml->item))
		{
			$attributes = $xml->item->attributes();
			if ($attributes['type'] == 'error')
			{
				$this->error_message = (string) $xml->item->message;
				$this->error_code = (string) $xml->item->errorcode;
				return true;
			}
		}
		return false;
	}
	public function setTransactionId($transaction_id)
	{
		if (empty ($transaction_id))
		{
			return false;
		}
		return ($this->transaction_id = $transaction_id);
	}
	public function getBankURL()
	{
		return $this->bank_url;
	}
	function onAfterSuccessfulPayment($inv_arg)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT number FROM #__ccinvoices_invoices WHERE id =".$inv_arg[0]." LIMIT 1";
		$db->setQuery($sql);
		$number = $db->loadResult();
		return sprintf(JText::_("AFTER_SUCCESSFULL_MSG"),$number);
	}
	function onAfterFailedPayment($val)
	{
		return JText::_("AFTER_FAILED_MSG");
	}
}
?>