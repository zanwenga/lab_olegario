<?php
/**
* @package    [ccInvoices 2Checkout payment plugin]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

defined('_JEXEC') or die('Restricted access');
/** Import library dependencies */
jimport('joomla.event.plugin');

$language = JFactory::getLanguage();
$language->load('plg_ccinvoice2Checkout', JPATH_ROOT, 'en-GB', true);
$language->load('plg_ccinvoice2Checkout', JPATH_ROOT, null, true);
class plgccinvoices_paymentccinvoice2Checkout extends JPlugin
{
	function plgccinvoices_paymentccinvoice2Checkout( &$subject, $config)
	{
		parent::__construct($subject, $config);
		//JPlugin::loadLanguage( 'plg_jegroupbuyPaypal' );
		$this->loadLanguage('plg_ccinvoice2Checkout');
		$params["plugin_name"] = "ccinvoice2Checkout";
		$params["icon"] = "";
		$params["logo"] = "2checkout.png";
		$params["description"] = JText::_("PAYMENT_METHOD_DESC");
		$params["payment_method"] = JText::_("PAYMENT_METHOD_NAME");
		$params["testmode"] = $this->params->get("test");
		$params["currency_code"] = $this->getCurrency();
		$params["acc_no"] = $this->params->get("acc_no");
		$this->params = $params;
	}
	function getCurrency()
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration where id = 1';
		$db->setQuery($query);
		$config = $db->loadObject();

		$currency='';
		if($config->currency_symbol=="€" OR $config->currency_symbol=="EUR")
			$currency="EUR";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="USD")
			$currency="USD";
		elseif($config->currency_symbol=="£" OR $config->currency_symbol=="GBP")
			$currency="GBP";
		elseif($config->currency_symbol=="¥" OR $config->currency_symbol=="JPY")
			$currency="JPY";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="AUD")
			$currency="AUD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="CAD")
			$currency="CAD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="HKD")
			$currency="HKD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="NZD")
			$currency="NZD";
		elseif($config->currency_symbol=="$" OR $config->currency_symbol=="SGD")
			$currency="SGD";
		elseif($config->currency_symbol=="CHF" OR $config->currency_symbol=="CHF")
			$currency="CHF";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="NOK")
			$currency="NOK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="SEK")
			$currency="SEK";
		elseif($config->currency_symbol=="kr" OR $config->currency_symbol=="DKK")
			$currency="DKK";
		return $currency;
	}
	function onSiteInvoiceOverviewPaymentIcons()
	{
		$paymentIcon["icon"] = $this->params["icon"];
		$paymentIcon["payment_method"] = $this->params["payment_method"];
		$paymentIcon["payment_method_note"] = $this->params["description"];
		$paymentIcon["logo"] = $this->params["logo"];
		return $paymentIcon;
	}
	function onProcessPayment()
	{
		$ptype = JRequest::getVar('ptype','');
		$id = JRequest::getInt('id');
		$html="";

		if($ptype == $this->params["plugin_name"])
		{
			$action = JRequest::getVar('pactiontype','');
			switch ($action)
			{
				case "process" :
				$html = $this->process($id);
				break;
				case "notify" :
				$html = $this->_notify_url();
				break;
				case "paymentmessage" :
				$html = $this->_paymentsuccess();
				break;
				default :
				$html =  $this->process($id);
				break;
			}
		}
		return $html;
	}
	function _notify_url()
	{
		$db = JFactory::getDBO();
		$account_type=$this->params["testmode"];
		$app = JFactory::getApplication();
		$user	= JFactory::getUser();
		$id = JRequest::getInt('id','');
		$user_id	= JRequest::getInt('userid','0');
		$pid = JRequest::getInt('pid','0');
		$Itemid = JRequest::getInt("Itemid",'0');



		require_once(JPATH_BASE.DS."plugins".DS."ccinvoices_payment".DS."ccinvoice2Checkout".DS."ccinvoice2Checkout".DS."TwoCo.php");
		// Create an instance of the authorize.net library
		$my2CO = new TwoCo();
		// Log the IPN results
		$my2CO->ipnLog = TRUE;
		// Specify your authorize login and secret
		$my2CO->setSecret('tango');
		// Enable test mode if needed
		$my2CO->enableTestMode($account_type);
		// Check validity and write down it
		if ($my2CO->validateIpn())
		{
				$db					=  JFactory::getDBO();
				$payer_email		= $my2CO->ipnData['email'];
			// ---------------------------------------------------------------------------------------

		         $item_number			= $my2CO->ipnData['item_number'];
				 $date	 			= date('Y-m-d');
				 $txn_id			= $my2CO->ipnData['key'];
			//----------------------------------------------------------------------------------------
			$id = $item_number;
			$ptype=JRequest::getVar('ptype');
			$pdate = gmdate("Y-m-d H:i:s");

			$sql = "UPDATE #__ccinvoices_invoices SET status = 4 WHERE id = ".$id;
			$db->setQuery($sql);
			$db->query();

			$query = 'INSERT INTO #__ccinvoices_payment ( inv_id, method , transaction_id , pdate, 	status)' .
					' VALUES ( "'.$id.'" ,"'.$this->params["payment_method_note"].'","'.$txn_id.'" ,"'.$pdate.'" ,"1" )'
					;
			$db->setQuery($query);
			$db->query();

			$app->redirect(JRoute::_('index.php?option=com_ccinvoices&task=paymentReturnUrl&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($id)));
			//$mainframe->redirect('index.php?option=com_download&Itemid=84','Payment is successfully made');
		}
		else
		{
		    //file_put_contents('2co.txt', "FAILURE\n\n" . $my2CO->ipnData);
		    $app->redirect(JRoute::_('index.php?option=com_ccinvoices&task=paymentReturnUrl&action=showresult&ptype='.$this->params["plugin_name"].'&id='.MD5($id)));
		}


	}
	function process($id)
	{
		$app = JFactory::getApplication();

		$db = JFactory::getDBO();
		$user	= JFactory::getUser();

		if ( $user->get('guest')) {
			$url = base64_encode(JRoute::_('index.php?option=com_ccinvoices&view=ccinvoices'));
			$link = "index.php?option=com_users&view=login&return=".$url;
			$app->redirect( $link);
		}

		$db->setQuery("SELECT id,total,custom_invoice_number,number,contact_id  FROM #__ccinvoices_invoices WHERE id =".$id." LIMIT 1");
		$row = $db->loadObject();
		$amount = $row->total;

		if($row->custom_invoice_number=="")
		{
			$itemname = $row->number;
		}
		else
		{
			$itemname = $row->custom_invoice_number;
		}

		$db->setQuery("SELECT name,address,email,contact_number FROM #__ccinvoices_contacts WHERE id =".$row->contact_id." LIMIT 1");
		$cRow = $db->loadObject();


		// Include the paypal library

		require_once(JPATH_BASE.DS."plugins".DS."ccinvoices_payment".DS."ccinvoice2Checkout".DS."ccinvoice2Checkout".DS."TwoCo.php");

		// Create an instance of the authorize.net library
		$my2CO = new TwoCo();

		// Specify your 2CheckOut vendor id
		$my2CO->addField('sid', $this->params["acc_no"]);
		$my2CO->addField('email_merchant',TRUE);
		$my2CO->addField('email', $cRow->email);
		$my2CO->addField('first_name', $cRow->name);
		$my2CO->addField('last_name', $cRow->name);
		$my2CO->addField('phone', $cRow->contact_number);
		$my2CO->addField('street_address', $cRow->address);
		$my2CO->addField('lang', $this->getLanguageCode());

		$my2CO->addField('item_name',$itemname);
		$my2CO->addField('item_number',$row->id);


		// Specify the order information
		$my2CO->addField('cart_order_id', $itemname);
		$my2CO->addField('total', filter_var($amount, FILTER_SANITIZE_NUMBER_FLOAT,FILTER_FLAG_ALLOW_FRACTION));

		// Specify the url where authorize.net will send the IPN
		//$url=JURI::root().'2co_ipn.php';
		$url= JURI::root().'index.php?option=com_ccinvoices&task=processPayment&ptype='.$this->params["plugin_name"].'&pactiontype=notify&id='.$row->id;

		$my2CO->addField('x_receipt_link_url', $url);
		$my2CO->addField('tco_currency',$this->params["currency_code"]);
		$my2CO->addField('list_currency', $this->params["currency_code"]);
		// Enable test mode if needed
		$my2CO->enableTestMode($this->params["testmode"]);
	//$my2CO->enableTestMode();
		// Let's start the train!
		$my2CO->submitPayment();

	}
	function getLanguageCode()
	{
		$user = JFactory::getUser();

		$languageCode="en";

		if(trim($this->jVersion())!="1.5")
		{
			$result=str_replace('"','',$user->get( 'params' ));
			$result=str_replace('{','',$result);
			$result=str_replace('}','',$result);
			$newarra=explode(",",$result);
			$i=0;
			$flag=false;

			foreach($newarra as $newarras)
			{

				$newarra1[$i]=explode(":",$newarras);
				if(trim($newarra1[$i][0])=="language")
				{
					$languageCode=substr($newarra1[$i][1],0,2);
					if(strlen(trim($languageCode))>0)
					{
						$flag=true;
					}
				}
				$i++;
			}

			if((count($newarra)<=1 AND $flag==false) OR trim($languageCode)=="")
			{
				$lang = JFactory::getLanguage();
				$languageCode=substr($lang->getTag(),0,2);
			}
		}
		return $languageCode;
	}

	function onPaymentMethodList($val)
	{

		$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->params["plugin_name"]."/".$this->params["plugin_name"]."/images/".$this->params["logo"];
		$form_action = JRoute :: _("index.php?option=com_ccinvoices&task=processPayment&ptype=".$this->params["plugin_name"]."&pactiontype=process&id=".$val["id"], false);
		$html ='<table class="ccinvoices_paymentmethods">
			<tr>';
				if($this->params["logo"] != ""){
			$html .='<td width="160" align="center">
					<img src="'.$paymentLogoPath.'" title="'. $this->params["payment_method"].'"/>
				</td>';
				 }
				$html .='<td>
					<p style="text-align:justify;">'.$this->params["description"].'</p>
				</td>
				<td width="100" align="center" style="padding-right:15px;">
					<table cellpadding="0" cellspacing="0" border="0"> <tr> <td align="center" style="padding:0px 0px 0px 0px !important;">
					</td></tr><tr><td align="center">
					<a  style="text-decoration:none;" href="'.$form_action.'"><img border="0" src="'.JURI::root().'/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
					</td></tr></table>
				</td>
			</tr>
		</table>';
		return $html;
	}
	function jVersion()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		$jversion= substr($current_version,0,3);
		return $jversion;
	}
}
?>