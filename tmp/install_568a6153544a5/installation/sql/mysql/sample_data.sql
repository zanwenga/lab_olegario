-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 26, 2013 at 04:39 PM
-- Server version: 5.5.24-log
-- PHP Version: 5.4.3

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `design2`
--

-- --------------------------------------------------------

--
-- Table structure for table `#__assets`
--

DROP TABLE IF EXISTS `#__assets`;
CREATE TABLE IF NOT EXISTS `#__assets` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set parent.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `level` int(10) unsigned NOT NULL COMMENT 'The cached level in the nested tree.',
  `name` varchar(50) NOT NULL COMMENT 'The unique name for the asset.\n',
  `title` varchar(100) NOT NULL COMMENT 'The descriptive title for the asset.',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_asset_name` (`name`),
  KEY `idx_lft_rgt` (`lft`,`rgt`),
  KEY `idx_parent_id` (`parent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=100 ;

--
-- Dumping data for table `#__assets`
--

INSERT INTO `#__assets` (`id`, `parent_id`, `lft`, `rgt`, `level`, `name`, `title`, `rules`) VALUES
(1, 0, 1, 175, 0, 'root.1', 'Root Asset', '{"core.login.site":{"6":1,"2":1},"core.login.admin":{"6":1},"core.login.offline":{"6":1},"core.admin":{"8":1},"core.manage":{"7":1},"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(2, 1, 1, 2, 1, 'com_admin', 'com_admin', '{}'),
(3, 1, 3, 14, 1, 'com_banners', 'com_banners', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(4, 1, 15, 16, 1, 'com_cache', 'com_cache', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(5, 1, 17, 18, 1, 'com_checkin', 'com_checkin', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(6, 1, 19, 20, 1, 'com_config', 'com_config', '{}'),
(7, 1, 21, 24, 1, 'com_contact', 'com_contact', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(8, 1, 25, 110, 1, 'com_content', 'com_content', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(9, 1, 111, 112, 1, 'com_cpanel', 'com_cpanel', '{}'),
(10, 1, 113, 114, 1, 'com_installer', 'com_installer', '{"core.admin":[],"core.manage":{"7":0},"core.delete":{"7":0},"core.edit.state":{"7":0}}'),
(11, 1, 115, 116, 1, 'com_languages', 'com_languages', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(12, 1, 117, 118, 1, 'com_login', 'com_login', '{}'),
(13, 1, 119, 120, 1, 'com_mailto', 'com_mailto', '{}'),
(14, 1, 121, 122, 1, 'com_massmail', 'com_massmail', '{}'),
(15, 1, 123, 124, 1, 'com_media', 'com_media', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":{"5":1}}'),
(16, 1, 125, 126, 1, 'com_menus', 'com_menus', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(17, 1, 127, 128, 1, 'com_messages', 'com_messages', '{"core.admin":{"7":1},"core.manage":{"7":1}}'),
(18, 1, 129, 130, 1, 'com_modules', 'com_modules', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(19, 1, 131, 140, 1, 'com_newsfeeds', 'com_newsfeeds', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(20, 1, 141, 142, 1, 'com_plugins', 'com_plugins', '{"core.admin":{"7":1},"core.manage":[],"core.edit":[],"core.edit.state":[]}'),
(21, 1, 143, 144, 1, 'com_redirect', 'com_redirect', '{"core.admin":{"7":1},"core.manage":[]}'),
(22, 1, 145, 146, 1, 'com_search', 'com_search', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(23, 1, 147, 148, 1, 'com_templates', 'com_templates', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(24, 1, 149, 152, 1, 'com_users', 'com_users', '{"core.admin":{"7":1},"core.manage":[],"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(25, 1, 153, 160, 1, 'com_weblinks', 'com_weblinks', '{"core.admin":{"7":1},"core.manage":{"6":1},"core.create":{"3":1},"core.delete":[],"core.edit":{"4":1},"core.edit.state":{"5":1},"core.edit.own":[]}'),
(26, 1, 161, 162, 1, 'com_wrapper', 'com_wrapper', '{}'),
(27, 8, 26, 33, 2, 'com_content.category.2', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(28, 3, 4, 5, 2, 'com_banners.category.3', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(29, 7, 22, 23, 2, 'com_contact.category.4', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(30, 19, 132, 133, 2, 'com_newsfeeds.category.5', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(31, 25, 154, 155, 2, 'com_weblinks.category.6', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[],"core.edit.own":[]}'),
(32, 24, 150, 151, 1, 'com_users.category.7', 'Uncategorised', '{"core.create":[],"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(33, 1, 163, 164, 1, 'com_finder', 'com_finder', '{"core.admin":{"7":1},"core.manage":{"6":1}}'),
(34, 1, 165, 166, 1, 'com_joomlaupdate', 'com_joomlaupdate', '{"core.admin":[],"core.manage":[],"core.delete":[],"core.edit.state":[]}'),
(35, 8, 34, 37, 2, 'com_content.category.8', 'Frontpage', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(36, 8, 38, 105, 2, 'com_content.category.9', 'News', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(37, 36, 39, 96, 3, 'com_content.category.10', 'Latest', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(38, 36, 97, 104, 3, 'com_content.category.11', 'News Flash', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(39, 8, 106, 107, 2, 'com_content.category.12', 'Sample Content', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(40, 37, 40, 41, 4, 'com_content.article.1', '3rd Party Component Compatibility', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(41, 37, 42, 43, 4, 'com_content.article.2', 'Built In File Compression', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(43, 37, 44, 45, 4, 'com_content.article.4', 'Fixed Side Tabs', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(44, 37, 46, 47, 4, 'com_content.article.5', 'Google Fonts Enabled', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(45, 37, 48, 49, 4, 'com_content.article.6', 'Hide Article Component Area', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(46, 37, 50, 51, 4, 'com_content.article.7', 'Hide Divs, Content, and Modules', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(47, 37, 52, 53, 4, 'com_content.article.8', 'Setup the Search and Menus', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(48, 37, 54, 55, 4, 'com_content.article.9', 'IE7 and 8 CSS3 Support', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(49, 37, 56, 57, 4, 'com_content.article.10', 'Info Slide', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(50, 37, 58, 59, 4, 'com_content.article.11', 'Installing The Template', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(51, 37, 60, 61, 4, 'com_content.article.12', 'Lazy Load Images', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(52, 37, 62, 63, 4, 'com_content.article.13', 'Login and Register Setup', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(53, 37, 64, 65, 4, 'com_content.article.14', 'Menu Scroll To Section', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(54, 37, 66, 67, 4, 'com_content.article.15', 'Module Positions and Styles', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(55, 37, 68, 69, 4, 'com_content.article.16', 'Multibox', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(56, 37, 70, 71, 4, 'com_content.article.17', 'Page, Column and Row Widths', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(57, 37, 72, 73, 4, 'com_content.article.18', 'Responsive Layout', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(59, 37, 74, 75, 4, 'com_content.article.20', 'S5 Drop Down Panel', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(60, 37, 76, 77, 4, 'com_content.article.21', 'S5 Flex Menu - Menu System', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(63, 38, 98, 99, 4, 'com_content.article.24', 'Sample Article 1', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(64, 38, 100, 101, 4, 'com_content.article.25', 'Sample Article 2', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(65, 38, 102, 103, 4, 'com_content.article.26', 'Sample Article 3', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(66, 27, 27, 28, 3, 'com_content.article.27', 'Sample Content', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(67, 27, 29, 30, 3, 'com_content.article.28', 'Sample Content 2', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(68, 27, 31, 32, 3, 'com_content.article.29', 'Sample Content 3', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(69, 37, 78, 79, 4, 'com_content.article.30', 'Search Engine Optimized ', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(70, 37, 80, 81, 4, 'com_content.article.31', 'Site Shapers', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(71, 37, 82, 83, 4, 'com_content.article.32', 'Stock Photography', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(72, 37, 84, 85, 4, 'com_content.article.33', 'Template Specific Options', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(74, 37, 86, 87, 4, 'com_content.article.35', 'The Template''s Settings', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(75, 37, 88, 89, 4, 'com_content.article.36', 'Tool Tips', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(76, 37, 90, 91, 4, 'com_content.article.37', 'Typography Options', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(77, 8, 108, 109, 2, 'com_content.category.13', 'Sermons', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(78, 19, 134, 135, 2, 'com_newsfeeds.category.14', 'Related Projects', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(79, 19, 136, 137, 2, 'com_newsfeeds.category.15', 'Free and Open Source Software', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(80, 19, 138, 139, 2, 'com_newsfeeds.category.16', 'Joomla!', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(81, 3, 6, 7, 2, 'com_banners.category.17', 'Demo Banner', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(82, 3, 8, 9, 2, 'com_banners.category.18', 'Joomla! Promo', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(83, 3, 10, 11, 2, 'com_banners.category.19', 'Text Ads', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(84, 3, 12, 13, 2, 'com_banners.category.20', 'Joomla', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(85, 25, 156, 157, 2, 'com_weblinks.category.21', 'Joomla! Specific Links', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(86, 25, 158, 159, 2, 'com_weblinks.category.22', 'Other Resources', '{"core.create":{"6":1,"3":1},"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1},"core.edit.own":{"6":1,"3":1}}'),
(91, 35, 35, 36, 3, 'com_content.article.42', 'Shape5 Vertex Featured Article', '{"core.delete":[],"core.edit":[],"core.edit.state":[]}'),
(93, 37, 92, 93, 4, 'com_content.article.44', 'Floating Menu', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(94, 37, 94, 95, 4, 'com_content.article.45', 'Parallax Backgrounds', '{"core.delete":{"6":1},"core.edit":{"6":1,"4":1},"core.edit.state":{"6":1,"5":1}}'),
(96, 1, 167, 168, 1, 'com_tags', 'com_tags', '{}'),
(97, 1, 169, 170, 1, 'com_contenthistory', 'com_contenthistory', '{}'),
(98, 1, 171, 172, 1, 'com_ajax', 'com_ajax', '{}'),
(99, 1, 173, 174, 1, 'com_postinstall', 'com_postinstall', '{}');

-- --------------------------------------------------------

--
-- Table structure for table `#__associations`
--

DROP TABLE IF EXISTS `#__associations`;
CREATE TABLE IF NOT EXISTS `#__associations` (
  `id` int(11) NOT NULL COMMENT 'A reference to the associated item.',
  `context` varchar(50) NOT NULL COMMENT 'The context of the associated item.',
  `key` char(32) NOT NULL COMMENT 'The key for the association computed from an md5 on associated ids.',
  PRIMARY KEY (`context`,`id`),
  KEY `idx_key` (`key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__banners`
--

DROP TABLE IF EXISTS `#__banners`;
CREATE TABLE IF NOT EXISTS `#__banners` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `cid` int(11) NOT NULL DEFAULT '0',
  `type` int(11) NOT NULL DEFAULT '0',
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `imptotal` int(11) NOT NULL DEFAULT '0',
  `impmade` int(11) NOT NULL DEFAULT '0',
  `clicks` int(11) NOT NULL DEFAULT '0',
  `clickurl` varchar(200) NOT NULL DEFAULT '',
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `description` text NOT NULL,
  `custombannercode` varchar(2048) NOT NULL,
  `sticky` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `params` text NOT NULL,
  `own_prefix` tinyint(1) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `reset` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `language` char(7) NOT NULL DEFAULT '',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `idx_state` (`state`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`),
  KEY `idx_banner_catid` (`catid`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `#__banners`
--

INSERT INTO `#__banners` (`id`, `cid`, `type`, `name`, `alias`, `imptotal`, `impmade`, `clicks`, `clickurl`, `state`, `catid`, `description`, `custombannercode`, `sticky`, `ordering`, `metakey`, `params`, `own_prefix`, `metakey_prefix`, `purchase_type`, `track_clicks`, `track_impressions`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `reset`, `created`, `language`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `version`) VALUES
(1, 0, 0, 'Demo Banner', 'demo-banner', 0, 2383, 1, 'http://www.shape5.com/join-now.html', 1, 17, '', '', 0, 1, '', '{"imageurl":"images\\/banner.png","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:24:03', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(2, 0, 0, 'Joomla!', 'joomla', 0, 0, 0, 'http://www.joomla.org', 1, 19, '', '', 0, 1, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:34:07', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(3, 0, 0, 'Joomla! Extensions', 'joomla-extensions', 0, 0, 0, 'http://extensions.joomla.org', 1, 19, '', '', 0, 2, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:35:02', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(4, 0, 0, 'Joomla! Promo Books', 'joomla-promo-books', 0, 0, 0, 'http://shop.joomla.org/amazoncom-bookstores.html', 1, 18, '', '', 0, 1, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:35:25', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(5, 0, 0, 'Joomla! Promo Shop', 'joomla-promo-shop', 0, 0, 0, 'http://shop.joomla.org', 1, 18, '', '', 0, 2, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:35:50', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(6, 0, 0, 'Joomla! Shop', 'joomla-shop', 0, 0, 0, 'http://shop.joomla.org', 1, 19, '', '', 0, 3, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:45:45', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(7, 0, 0, 'JoomlaCode', 'joomlacode', 0, 0, 0, 'http://joomlacode.org', 1, 19, '', '', 0, 4, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:46:17', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(8, 0, 0, 'OSM 1', 'osm-1', 0, 0, 0, 'http://www.opensourcematters.org', 1, 20, '', '', 0, 1, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:46:33', '*', 0, '', '0000-00-00 00:00:00', 0, 1),
(9, 0, 0, 'OSM 2', 'osm-2', 0, 0, 0, 'http://www.opensourcematters.org', 1, 20, '', '', 0, 2, '', '{"imageurl":"","width":"","height":"","alt":""}', 0, '', -1, -1, -1, 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '2013-05-13 01:46:55', '*', 0, '', '0000-00-00 00:00:00', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__banner_clients`
--

DROP TABLE IF EXISTS `#__banner_clients`;
CREATE TABLE IF NOT EXISTS `#__banner_clients` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `contact` varchar(255) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `extrainfo` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `metakey` text NOT NULL,
  `own_prefix` tinyint(4) NOT NULL DEFAULT '0',
  `metakey_prefix` varchar(255) NOT NULL DEFAULT '',
  `purchase_type` tinyint(4) NOT NULL DEFAULT '-1',
  `track_clicks` tinyint(4) NOT NULL DEFAULT '-1',
  `track_impressions` tinyint(4) NOT NULL DEFAULT '-1',
  PRIMARY KEY (`id`),
  KEY `idx_own_prefix` (`own_prefix`),
  KEY `idx_metakey_prefix` (`metakey_prefix`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__banner_tracks`
--

DROP TABLE IF EXISTS `#__banner_tracks`;
CREATE TABLE IF NOT EXISTS `#__banner_tracks` (
  `track_date` datetime NOT NULL,
  `track_type` int(10) unsigned NOT NULL,
  `banner_id` int(10) unsigned NOT NULL,
  `count` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`track_date`,`track_type`,`banner_id`),
  KEY `idx_track_date` (`track_date`),
  KEY `idx_track_type` (`track_type`),
  KEY `idx_banner_id` (`banner_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__categories`
--

DROP TABLE IF EXISTS `#__categories`;
CREATE TABLE IF NOT EXISTS `#__categories` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `extension` varchar(50) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `cat_idx` (`extension`,`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `#__categories`
--

INSERT INTO `#__categories` (`id`, `asset_id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `extension`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `modified_user_id`, `modified_time`, `hits`, `language`, `version`) VALUES
(1, 0, 0, 0, 43, 0, '', 'system', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 791, '2009-10-18 16:07:09', 0, '0000-00-00 00:00:00', 0, '*', 1),
(2, 27, 1, 1, 2, 1, 'uncategorised', 'com_content', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:26:37', 0, '0000-00-00 00:00:00', 0, '*', 1),
(3, 28, 1, 3, 4, 1, 'uncategorised', 'com_banners', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":"","foobar":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:27:35', 0, '0000-00-00 00:00:00', 0, '*', 1),
(4, 29, 1, 5, 6, 1, 'uncategorised', 'com_contact', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:27:57', 0, '0000-00-00 00:00:00', 0, '*', 1),
(5, 30, 1, 7, 8, 1, 'uncategorised', 'com_newsfeeds', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:28:15', 0, '0000-00-00 00:00:00', 0, '*', 1),
(6, 31, 1, 9, 10, 1, 'uncategorised', 'com_weblinks', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*', 1),
(7, 32, 1, 11, 12, 1, 'uncategorised', 'com_users', 'Uncategorised', 'uncategorised', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"target":"","image":""}', '', '', '{"page_title":"","author":"","robots":""}', 791, '2010-06-28 13:28:33', 0, '0000-00-00 00:00:00', 0, '*', 1),
(8, 35, 1, 13, 14, 1, 'frontpage', 'com_content', 'Frontpage', 'frontpage', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 03:23:25', 0, '0000-00-00 00:00:00', 0, '*', 1),
(9, 36, 1, 15, 20, 1, 'news', 'com_content', 'News', 'news', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 03:23:35', 0, '0000-00-00 00:00:00', 3, '*', 1),
(10, 37, 9, 16, 17, 2, 'news/latest', 'com_content', 'Latest', 'latest', '', '<p>The latest news from the Joomla! Team</p>', 1, 983, '2013-05-13 01:11:44', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 03:23:48', 983, '2013-05-13 01:11:44', 3, '*', 1),
(11, 38, 9, 18, 19, 2, 'news/news-flash', 'com_content', 'News Flash', 'news-flash', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 03:24:02', 983, '2013-05-12 03:26:34', 1, '*', 1),
(12, 39, 1, 21, 22, 1, 'sample-content', 'com_content', 'Sample Content', 'sample-content', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 03:25:44', 983, '2013-05-12 03:26:02', 0, '*', 1),
(13, 77, 1, 23, 24, 1, 'sermons', 'com_content', 'Sermons', 'sermons', '', '<p>The S5 MP3 Player Plugin is demonstrated on the blog posts below, and can be used on any Joomla template. <br /><br /> Easily setup streaming mp3''s of any kind on your Joomla site! This plugin comes loaded with features that allow you to adapt this to any site. A full list of features includes: <br /><br /></p>\r\n<ul class="ul_star">\r\n<li>Full responsive design. This player will automatically adjust to 100% of the available width any screen size.</li>\r\n<li>Hide the player and show a text only link for non-flash devices or at a specified screen width for mobile devices.</li>\r\n<li>Change the color of any part of the player! This includes play and pause buttons, backgrounds, streaming bar, hover colors, audio links, download background and even more! You can set them to any color you wish!</li>\r\n<li>Set as many mp3 players as you wish on one page or multiple pages</li>\r\n<li>Choose to enable or disable the download button and choose your own Download text.</li>\r\n<li>Choose how many times a song will loop after it has completed playing.</li>\r\n<li>Auto start or manually start an mp3 player on page load.</li>\r\n<li>All other mp3 players on the same page will stop playing when a new mp3 player is played.</li>\r\n<li>Stream songs from a static url or specify a folder in the plugin''s backend to pull them dynamically.</li>\r\n</ul>', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-12 23:12:26', 0, '0000-00-00 00:00:00', 0, '*', 1),
(14, 78, 1, 25, 26, 1, 'related-projects', 'com_newsfeeds', 'Related Projects', 'related-projects', '', '<p>Joomla builds on and collaborates with many other free and open source projects. Keep up with the latest news from some of them.</p>', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:25:45', 0, '0000-00-00 00:00:00', 0, '*', 1),
(15, 79, 1, 27, 28, 1, 'free-and-open-source-software', 'com_newsfeeds', 'Free and Open Source Software', 'free-and-open-source-software', '', '<p>Read the latest news about free and open source software from some of its leading advocates.</p>', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:26:04', 0, '0000-00-00 00:00:00', 0, '*', 1),
(16, 80, 1, 29, 30, 1, 'joomla', 'com_newsfeeds', 'Joomla!', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:26:19', 0, '0000-00-00 00:00:00', 0, '*', 1),
(17, 81, 1, 31, 32, 1, 'demo-banner', 'com_banners', 'Demo Banner', 'demo-banner', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:32:54', 0, '0000-00-00 00:00:00', 0, '*', 1),
(18, 82, 1, 33, 34, 1, 'joomla-promo', 'com_banners', 'Joomla! Promo', 'joomla-promo', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:33:05', 0, '0000-00-00 00:00:00', 0, '*', 1),
(19, 83, 1, 35, 36, 1, 'text-ads', 'com_banners', 'Text Ads', 'text-ads', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:33:16', 0, '0000-00-00 00:00:00', 0, '*', 1),
(20, 84, 1, 37, 38, 1, 'joomla', 'com_banners', 'Joomla', 'joomla', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 01:33:29', 0, '0000-00-00 00:00:00', 0, '*', 1),
(21, 85, 1, 39, 40, 1, 'joomla-specific-links', 'com_weblinks', 'Joomla! Specific Links', 'joomla-specific-links', '', '<p>A selection of links that are all related to the Joomla! Project.</p>', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 02:20:08', 0, '0000-00-00 00:00:00', 0, '*', 1),
(22, 86, 1, 41, 42, 1, 'other-resources', 'com_weblinks', 'Other Resources', 'other-resources', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{"category_layout":"","image":""}', '', '', '{"author":"","robots":""}', 791, '2013-05-13 02:20:25', 0, '0000-00-00 00:00:00', 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__contact_details`
--

DROP TABLE IF EXISTS `#__contact_details`;
CREATE TABLE IF NOT EXISTS `#__contact_details` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `con_position` varchar(255) DEFAULT NULL,
  `address` text,
  `suburb` varchar(100) DEFAULT NULL,
  `state` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `postcode` varchar(100) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `fax` varchar(255) DEFAULT NULL,
  `misc` mediumtext,
  `image` varchar(255) DEFAULT NULL,
  `email_to` varchar(255) DEFAULT NULL,
  `default_con` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `user_id` int(11) NOT NULL DEFAULT '0',
  `catid` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `mobile` varchar(255) NOT NULL DEFAULT '',
  `webpage` varchar(255) NOT NULL DEFAULT '',
  `sortname1` varchar(255) NOT NULL,
  `sortname2` varchar(255) NOT NULL,
  `sortname3` varchar(255) NOT NULL,
  `language` char(7) NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__contact_details`
--

INSERT INTO `#__contact_details` (`id`, `name`, `alias`, `con_position`, `address`, `suburb`, `state`, `country`, `postcode`, `telephone`, `fax`, `misc`, `image`, `email_to`, `default_con`, `published`, `checked_out`, `checked_out_time`, `ordering`, `params`, `user_id`, `catid`, `access`, `mobile`, `webpage`, `sortname1`, `sortname2`, `sortname3`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `hits`) VALUES
(1, 'Name', 'name', '', 'Street Suburb State Zip Code Country ', '', '', '', '', 'Telephone', 'Fax', '<p>Miscellaneous info</p>', '', 'email@email.com', 0, 1, 983, '2013-05-13 01:17:11', 1, '{"show_contact_category":"","show_contact_list":"","presentation_style":"","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","show_profile":"","show_links":"","linka_name":"","linka":null,"linkb_name":"","linkb":null,"linkc_name":"","linkc":null,"linkd_name":"","linkd":null,"linke_name":"","linke":"","contact_layout":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":""}', 0, 4, 1, '', '', '', '', '', '*', '2013-05-12 04:41:35', 791, '', '2013-05-13 01:17:11', 983, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 3);

-- --------------------------------------------------------

--
-- Table structure for table `#__content`
--

DROP TABLE IF EXISTS `#__content`;
CREATE TABLE IF NOT EXISTS `#__content` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(255) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `introtext` mediumtext NOT NULL,
  `fulltext` mediumtext NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `attribs` varchar(5120) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if article is featured.',
  `language` char(7) NOT NULL COMMENT 'The language code for the article.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=47 ;

--
-- Dumping data for table `#__content`
--

INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(1, 40, '3rd Party Component Compatibility', '3rd-party-component-compatibility', 'This template is compatible with all the major 3rd party components available for Joomla. The following are just some of the ones available that work great with any Shape 5 template. A template itself should in no way hinder the functionality of a component. Although we haven&#39;t tested every single Joomla component available we can say quite confidently that this template will be compatible with any Joomla extension you use with it.</p>\r\n<p> </p>\r\n<p><img src="http://www.shape5.com/demo/images/general/3rdparty.png" border="0" /><br />And many more!', '', 1, 10, '2013-05-12 03:28:34', 791, '', '2013-06-11 17:31:03', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:28:34', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 32, '', '', 1, 10, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(2, 41, 'Built In File Compression', 'built-in-file-compression', '<p>Increase speed and performance by compressing core template CSS and Javascript files to much smaller sizes than the original! Enabling compression is a great way to boost your site''s performance. It simply combines css and js into consolidated files. This reduces the downloads sizes and reduces the numbers of calls to your server, to dramatically help your site''s overall performance. No data is lost during this process, just simply made smaller. Please note that this compression will only compress core template files, not third party files or files from extensions.</p>\r\n<p><br /> <img class="padded" src="http://www.shape5.com/demo/images/general/file_compression.jpg" border="0" alt="" /></p>\r\n<div style="clear: both;"> </div>\r\n<p><br /><br /></p>\r\n<div class="red_box"><span class="alert">Note: Because this feature uses cached versions of your javascript and css this plugin should not be used while developing your site and should only be enabled after you have completed your site. <br /><br /> Gzip must be installed on your server and enabled in PHP in order to function.</span></div>\r\n<p> </p>\r\n<p><strong><span style="font-size: large;">See It In Action!</span></strong></p>\r\n<p>Without Compression Enabled:</p>\r\n<p><img class="padded" src="http://www.shape5.com/demo/images/general/compression_without.png" border="0" /></p>\r\n<p>With Compression nabled:</p>\r\n<p><img class="padded" src="http://www.shape5.com/demo/images/general/compression_with.png" border="0" /></p>\r\n<p> </p>\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div>\r\n<p> </p>', '', 1, 10, '2013-05-12 03:34:33', 791, '', '2013-06-11 16:00:55', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:34:33', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 31, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(4, 43, 'Fixed Side Tabs', 'fixed-side-tabs', 'This template includes a "Fixed Tab" option that you can enable and publish on your site and will show in a fixed position on either the left or right side of the screen. The great feature about the fixed tabs is that you can enter any text you desire and the text is automatically flipped vertically! This is great for search engines to read your text and also saves the hassle of creating an image with vertical text and placing it on the side of your site. The tabs are published site wide and can have the following options that can be changed via the template parameters area and can link to any URL that you desire.\r\n\r\n\r\n<br /><br /><h3>The following is a quick list of features: </h3></p>\r\n<ul class="ul_star">\r\n<li>Change background to any hex color </li>\r\n<li>Change the border to any hex color</li>\r\n<li>Change the font to any hex color</li>\r\n<li>Set vertical position of each tab</li>\r\n<li>Set the height of each tab</li>\r\n<li>Set each tab to either the left or right of the screen</li>\r\n<li>Add a class to each fixed tab to enable s5 box or perhaps a lightbox or other 3rd party extension</li>\r\n<li>Add a URL to each fixed tab so onclick the URL loads</li>\r\n<li>Enter any text you desire</li>\r\n</ul>\r\n\r\n<br /><br />   <div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div>', '', 1, 10, '2013-05-12 03:35:29', 791, '', '2013-06-11 17:30:13', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:35:29', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 30, '', '', 1, 6, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(5, 44, 'Google Fonts Enabled', 'google-fonts-enabled', 'Do you want your own custom font? Not a problem, this template comes with Google Fonts enabled, allowing you to pick from over a dozen font families for your website. In the template parameters area of the template you can choose your own custom font, and preview it from the Vertex interface. Below are some examples of the fonts available.<br/><br/>\r\n\r\n<img alt="" src="http://www.shape5.com/demo/images/general/google_fonts.png"></img>\r\n\r\n<br />\r\n<br /><br />\r\n\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div><br /></ul>', '', 1, 10, '2013-05-12 03:35:52', 791, '', '2013-06-11 17:28:36', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:35:52', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 29, '', '', 1, 8, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(6, 45, 'Hide Article Component Area', 'hide-article-component-area', 'Did you ever need to create a page where this is no article present or no component to be shown, and only load modules? This template makes it all possible! From the template configuration page you can hide the main content area on any page on the site. \r\n\r\n<br /><br />\r\n\r\nBelow is a screenshot of this function from the configuration page, found under the General tab:\r\n\r\n<br /><br />\r\n\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/hide_articles.png" alt=""></img>\r\n\r\n<div style="clear:both; height:0px"></div>', '', 1, 10, '2013-05-12 03:36:11', 791, '', '2013-06-11 15:53:58', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:36:11', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 28, '', '', 1, 5, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(7, 46, 'Hide Divs, Content, and Modules', 'hide-divs-content-and-modules', 'One of the biggest obstacles to overcome when designing a responsive layout site is that not all content, images and extensions were designed to work with responsive layouts. That means that sometimes you need the ability to hide a specific element on only certain sized screens, so that something doesn''t break the site''s layout and everything looks proportionate. We''ve made that all possible and very easy to do for you with hiding classes! There are three main ways to hide content on different size screens, and they are documented below. Please note that these classes are only enabled when the responsive layout is enabled.\r\n<br /><br />\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nBe sure to visit our Tutorials page and download our Responsive Best Practices guide for useful responsive layout tips.</a>\r\n</div>\r\n<br /><br />\r\n<h3>Hide Sections of the Template via the Template Configuration</h3>\r\nThis is the simplest way to hide an area of the template is to use the template interface to easily select areas of the template that you want to hide on tablet sized screens (970px and less) or mobile screens (580px or less). Simply select the area that you want to hide and the Vertex framework takes care of the rest! These fields do work independent of each other, so if you want to hide something on both tablet and mobile sized screens you must select the same area on both fields.\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/hide_divs1.png"></img>\r\n<br /><br />\r\nIMPORTANT - If you turn off a column position such as right or right_inset something else must be in the same column or the layout will not work. For example you can turn off right_inset and keep right, but you cannot turn off both unless something is publihsed to right_top or right_bottom as well.\r\n\r\n\r\n<br /><br />\r\n<h3>Hide Specific Modules via the Hide Classes</h3>\r\nIf you have only a specific module that you would like to hide at certain screen sizes, and not an entire section of the template, this is the best approach. A hide class is a class that can be applied to any element on the page and hides that particular element at certain screen sizes. Classes range in 100px increments for large screens and 50px increments for small screens. <strong>Below are some examples, and at the very bottom of this page there is a list of all the available hide classes.</strong> Use these sparingly if you can. As a recommendation, the primary target of these classes should focus on tablet and mobile sized screens. Wide screen monitors vary in size so it''s much harder to use these classes correctly for large monitors. Tablet and mobile devices are much more consistent in size so it is much easier to apply the smaller hide classes.\r\n<br /><br />\r\nFor example, say you want to hide a specific module when the screen sizes reaches 900px wide, and remain hidden for any screen below 900px. Simply add class=hide_900 to the title of the module like this:\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/hide_divs2.png"></img>\r\n<br /><br />\r\nThe class is simply put into the title of the module. There must be a space just before class= , and don''t worry, the class area of the title won''t actually show on the live site, it''s only shown in the backend. It must be entered exactly as shown above. The title can be published or unpublished, it does not matter which. Simply adding the classes to the title will apply the classes to the entire module.\r\n<br /><br />\r\nWhat if you want it to hide the module only for a certain range? That''s easy just add _only to the end of the class name. hide_900_only will only hide that element from 900px to the next increment in the hide classes, which is 850px. So it will only be hidden from 850px to 900px. You can also add multiple classes to the title like this class=hide_900_only hide_850_only which will apply both classes to the module.\r\n<br /><br />\r\nWhat if you want to hide the module and then show it again later? That''s simple, use show_ in the class instead of hide_. This will make the module show for the specified size no matter what other settings are on the module. So if you want to hide the module from 1000px and below, but you want to show it again later then do something like the following class=hide_1000 show_600. This will hide the module from 600px to 1000px.\r\n<br /><br />\r\nIs there a more simple way? Yes, of course, the above directions are for experienced users who want to tweak their content for every available screen size. If you don''t want to mess around with specific window sizes simply use the following classes instead, which have preset screen sizes applied to them:\r\n<br /><br />\r\nhide_all<br />\r\nhide_wide_screen<br />\r\nhide_standard_screen<br />\r\nhide_large_tablet<br />\r\nhide_small_tablet<br />\r\nhide_mobile<br />\r\nshow_wide_screen<br />\r\nshow_standard_screen<br />\r\nshow_large_tablet<br />\r\nshow_small_tablet<br />\r\nshow_mobile<br />\r\n\r\n<br />\r\n\r\nWhy would someone use hide_all? This is a great tool for anyone wanting to show content only on a mobile or tablet sized screen but hide it on all other devices. To do this you should use class="hide_all show_mobile". The hide_all will set the content or module to display:none on all devices, and one the screen size reaches mobile size the show_mobile will override the display:none and show the content.\r\n<br />\r\n<br />\r\n\r\n<br />\r\n<h3>Hide Specific Content via the Hide Classes</h3>\r\nIf you have only specific content or images within an article or module that you want to hide then use the same hide classes described above, but wrap that specific content inside of the class instead of applying it to the entire module. For example, in the image below, the third paragraph will hide at 900px and then show again at 700px.\r\n\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/hide_divs3.png"></img>\r\n<br /><br />\r\n\r\n\r\n\r\n\r\n<h3>Available Hide Classes</h3>\r\nAll the available hide classes are listed below. Note there is a break at 970px and 580px to accommodate tablet and mobile sized screens. At the bottom of the list you will see inline calls, these calls should only be used if the default block calls cause a layout problem with your content, which can happen with applying a display:block.\r\n<br><br>\r\n\r\n<p>/* HIDE CLASSES<br>\r\n----------------------------------------------------------- */<br>\r\n<br>\r\n@media screen and (max-width: 1600px){<br>\r\n.hide_1600 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1500px) and (max-width: 1600px){<br>\r\n.hide_1600_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1550px){<br>\r\n.hide_1550 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1450px) and (max-width: 1550px){<br>\r\n.hide_1550_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1500px){<br>\r\n.hide_1500 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1400px) and (max-width: 1500px){<br>\r\n.hide_1500_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1450px){<br>\r\n.hide_1450 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1350px) and (max-width: 1450px){<br>\r\n.hide_1450_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1400px){<br>\r\n.hide_1400 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 1400px){<br>\r\n.hide_1400_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1350px){<br>\r\n.hide_1350 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1250px) and (max-width: 1350px){<br>\r\n.hide_1350_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1300px){<br>\r\n.hide_1300 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1200px) and (max-width: 1300px){<br>\r\n.hide_1300_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1250px){<br>\r\n.hide_1250 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1150px) and (max-width: 1250px){<br>\r\n.hide_1250_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1200px){<br>\r\n.hide_1200 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1100px) and (max-width: 1200px){<br>\r\n.hide_1200_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1150px){<br>\r\n.hide_1150 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1050px) and (max-width: 1150px){<br>\r\n.hide_1150_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1100px){<br>\r\n.hide_1100 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1000px) and (max-width: 1100px){<br>\r\n.hide_1100_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1050px){<br>\r\n.hide_1050 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:950px) and (max-width: 1050px){<br>\r\n.hide_1050_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1000px){<br>\r\n.hide_1000 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 1000px){<br>\r\n.hide_1000_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 970px){<br>\r\n.hide_970 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 970px){<br>\r\n.hide_970_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 950px){<br>\r\n.hide_950 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 950px){<br>\r\n.hide_950_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 900px){<br>\r\n.hide_900 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:850px) and (max-width: 900px){<br>\r\n.hide_900_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 850px){<br>\r\n.hide_850 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:800px) and (max-width: 850px){<br>\r\n.hide_850_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 800px){<br>\r\n.hide_800 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 800px){<br>\r\n.hide_800_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 750px){<br>\r\n.hide_750 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:700px) and (max-width: 750px){<br>\r\n.hide_750_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 700px){<br>\r\n.hide_700 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:650px) and (max-width: 700px){<br>\r\n.hide_700_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 650px){<br>\r\n.hide_650 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:600px) and (max-width: 650px){<br>\r\n.hide_650_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 600px){<br>\r\n.hide_600 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 600px){<br>\r\n.hide_600_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.hide_580 {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:550px) and (max-width: 579px){<br>\r\n.hide_580_only {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n.hide_all {<br>\r\ndisplay:none ! important;<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 50000px){<br>\r\n.hide_wide_screen {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:971px) and (max-width: 1299px){<br>\r\n.hide_standard_screen {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 970px){<br>\r\n.hide_large_tablet {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 750px){<br>\r\n.hide_small_tablet {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 750px){<br>\r\n.hide_small_tablet {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.hide_mobile {<br>\r\ndisplay:none !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1600px){<br>\r\n.show_1600 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1500px) and (max-width: 1600px){<br>\r\n.show_1600_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1550px){<br>\r\n.show_1550 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1450px) and (max-width: 1550px){<br>\r\n.show_1550_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1500px){<br>\r\n.show_1500 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1400px) and (max-width: 1500px){<br>\r\n.show_1500_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1450px){<br>\r\n.show_1450 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1350px) and (max-width: 1450px){<br>\r\n.show_1450_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1400px){<br>\r\n.show_1400 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 1400px){<br>\r\n.show_1400_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1350px){<br>\r\n.show_1350 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1250px) and (max-width: 1350px){<br>\r\n.show_1350_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1300px){<br>\r\n.show_1300 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1200px) and (max-width: 1300px){<br>\r\n.show_1300_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1250px){<br>\r\n.show_1250 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1150px) and (max-width: 1250px){<br>\r\n.show_1250_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1200px){<br>\r\n.show_1200 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1100px) and (max-width: 1200px){<br>\r\n.show_1200_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1150px){<br>\r\n.show_1150 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1050px) and (max-width: 1150px){<br>\r\n.show_1150_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1100px){<br>\r\n.show_1100 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1000px) and (max-width: 1100px){<br>\r\n.show_1100_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1050px){<br>\r\n.show_1050 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:950px) and (max-width: 1050px){<br>\r\n.show_1050_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1000px){<br>\r\n.show_1000 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 1000px){<br>\r\n.show_1000_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 970px){<br>\r\n.show_970 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 970px){<br>\r\n.show_970_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 950px){<br>\r\n.show_950 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 950px){<br>\r\n.show_950_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 900px){<br>\r\n.show_900 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:850px) and (max-width: 900px){<br>\r\n.show_900_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 850px){<br>\r\n.show_850 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:800px) and (max-width: 850px){<br>\r\n.show_850_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 800px){<br>\r\n.show_800 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 800px){<br>\r\n.show_800_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 750px){<br>\r\n.show_750 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:700px) and (max-width: 750px){<br>\r\n.show_750_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 700px){<br>\r\n.show_700 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:650px) and (max-width: 700px){<br>\r\n.show_700_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 650px){<br>\r\n.show_650 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:600px) and (max-width: 650px){<br>\r\n.show_650_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 600px){<br>\r\n.show_600 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 600px){<br>\r\n.show_600_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.show_580 {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:550px) and (max-width: 579px){<br>\r\n.show_580_only {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 50000px){<br>\r\n.show_wide_screen {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:971px) and (max-width: 1299px){<br>\r\n.show_standard_screen {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 970px){<br>\r\n.show_large_tablet {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 750px){<br>\r\n.show_small_tablet {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.show_mobile {<br>\r\ndisplay:block !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n<br>\r\n/* THE INLINE SHOW CALLS BELOW SHOULD BE USED SELDOMLY. THEY SHOULD ONLY BE USED \r\nIF THE BLOCK SHOW CALLS ABOVE CAUSE A LAYOUT ISSUE WHEN TRIGGERED.<br>\r\n----------------------------------------------------------- */<br>\r\n<br>\r\n@media screen and (max-width: 1600px){<br>\r\n.show_1600_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1500px) and (max-width: 1600px){<br>\r\n.show_1600_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1550px){<br>\r\n.show_1550_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1450px) and (max-width: 1550px){<br>\r\n.show_1550_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1500px){<br>\r\n.show_1500_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1400px) and (max-width: 1500px){<br>\r\n.show_1500_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1450px){<br>\r\n.show_1450_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1350px) and (max-width: 1450px){<br>\r\n.show_1450_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1400px){<br>\r\n.show_1400_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 1400px){<br>\r\n.show_1400_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1350px){<br>\r\n.show_1350_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1250px) and (max-width: 1350px){<br>\r\n.show_1350_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1300px){<br>\r\n.show_1300_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1200px) and (max-width: 1300px){<br>\r\n.show_1300_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1250px){<br>\r\n.show_1250_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1150px) and (max-width: 1250px){<br>\r\n.show_1250_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1200px){<br>\r\n.show_1200_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1100px) and (max-width: 1200px){<br>\r\n.show_1200_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1150px){<br>\r\n.show_1150_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1050px) and (max-width: 1150px){<br>\r\n.show_1150_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1100px){<br>\r\n.show_1100_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:1000px) and (max-width: 1100px){<br>\r\n.show_1100_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1050px){<br>\r\n.show_1050_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:950px) and (max-width: 1050px){<br>\r\n.show_1050_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 1000px){<br>\r\n.show_1000_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 1000px){<br>\r\n.show_1000_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 970px){<br>\r\n.show_970_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 970px){<br>\r\n.show_970_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 950px){<br>\r\n.show_950_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:900px) and (max-width: 950px){<br>\r\n.show_950_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 900px){<br>\r\n.show_900_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:850px) and (max-width: 900px){<br>\r\n.show_900_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 850px){<br>\r\n.show_850_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:800px) and (max-width: 850px){<br>\r\n.show_850_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 800px){<br>\r\n.show_800_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 800px){<br>\r\n.show_800_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 750px){<br>\r\n.show_750_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:700px) and (max-width: 750px){<br>\r\n.show_750_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 700px){<br>\r\n.show_700_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:650px) and (max-width: 700px){<br>\r\n.show_700_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 650px){<br>\r\n.show_650_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:600px) and (max-width: 650px){<br>\r\n.show_650_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 600px){<br>\r\n.show_600_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 600px){<br>\r\n.show_600_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.show_580_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n} <br>\r\n<br>\r\n@media screen and (min-width:550px) and (max-width: 579px){<br>\r\n.show_580_only_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:1300px) and (max-width: 50000px){<br>\r\n.show_wide_screen_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:971px) and (max-width: 1299px){<br>\r\n.show_standard_screen_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:750px) and (max-width: 970px){<br>\r\n.show_large_tablet_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (min-width:580px) and (max-width: 750px){<br>\r\n.show_small_tablet_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}<br>\r\n<br>\r\n@media screen and (max-width: 579px){<br>\r\n.show_mobile_inline {<br>\r\ndisplay:inline !important;<br>\r\n}<br>\r\n}</p>\r\n\r\n<br /><br />\r\n\r\n\r\n\r\n<p><br /></p>\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div>\r\n<br />', '', 1, 10, '2013-05-12 03:36:45', 791, '', '2013-06-11 15:50:44', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:36:45', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 27, '', '', 1, 8, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(8, 47, 'Setup the Search and Menus', 'how-to-setup-the-search-box-and-menus', '<br />\r\n<h3>1. Search Setup</h3>\r\n<span style="font-size: medium;"><strong> <img src="images/search.png" class="padded" /></strong></span>\r\n<ul class="ul_arrow">\r\n<li>Simply publish the default Joomla search module to the search position.</li>\r\n</ul>\r\n\r\n<br />\r\n\r\n<h3>2. Column Menu Setup</h3>\r\n<span style="font-size: medium;"><strong> <img src="images/column_menu.png" class="padded" /></strong></span>\r\n<ul class="ul_arrow">\r\n<li>Publish any menu module to the main body module positions on your site. </li>\r\n<li>There should be no menu style suffixes applied under advanced parameters.</li>\r\n<li> The menu style should be set to list.</li>\r\n</ul>\r\n\r\n\r\n\r\n<br />\r\n<h3>3. Bottom Menu Setup</h3>\r\n<img src="images/bottom_menu.png" class="padded" /><br />\r\n<ul class="ul_arrow">\r\n<li>Publish any menu to the &#39;bottom_menu&#39;  position.</li>\r\n<li> There are no menu style suffixes applied under advanced parameters.</li>\r\n<li> The menu style should be set to list</li>\r\n</ul>\r\n ', '', 1, 10, '2013-05-12 03:37:17', 791, '', '2013-11-26 15:56:19', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:37:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 26, '', '', 1, 32, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(9, 48, 'IE7 and 8 CSS3 Support', 'ie7-and-8-css3-support', 'This template includes limited support of CSS3 for IE7 and IE8. With the power of css3, websites can now be built much faster and with far \r\nless code. Design features such as gradients or shadows that used to be \r\ncreated and then called as images on the page are now simply called \r\nby css code. Transition effects that used to require full javascript libraries can \r\nnow be called with less than 1kb of text. Rounded corners that used to \r\nrequire upwards of eight wrapping div elements can now be done with a \r\nsingle element. What does this mean for you? Simple, a lightning fast website, \r\nthat becomes even more search engine friendly. \r\n\r\n<br /><br />Many modern browsers such as Firefox4 of IE9 already support CSS3 natively, but where does that leave IE7 and IE8? Thankfully a great solution called CSS PIE (Progressive Internet Explorer) has been introduced and is integrated into this template. Simply put, CSS PIE a script that upgrades IE7 and 8 to support most CSS3 formatting.  There are slight variations and some CSS3 formatting isn&#39;t supported, but overall it does a great job and allows us to extend CSS3 support to IE7 and 8.', '', 1, 10, '2013-05-12 03:37:49', 791, '', '2013-06-11 17:30:39', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:37:49', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 25, '', '', 1, 3, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(10, 49, 'Info Slide', 'info-slide', 'The info slide script is a great way to display your content to your customers! It will place a sliding text box over any image on the page. It can be placed inside of content or modules. It will also automatically adjust to any size screen size on window resize. See below for a demonstration.\r\n\r\n\r\n<br /><br />\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nFor a tutorial on how to setup this script be sure to view the Vertex Guide on our Tutorials page.</a>\r\n</div>\r\n\r\n<br />\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load1.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load2.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load3.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load4.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load5.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load6.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load7.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load8.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n<div class="s5_is" style="float:left; width:31%; margin-right:15px; margin-bottom:15px">\r\n	<img alt="" src="http://www.shape5.com/demo/images/general/lazyload/lazy_load9.jpg"></img>\r\n	<div class="s5_is_slide">\r\n	<h3>Example Slide</h3>\r\n	This is dummy text. You can add any text or html markup here.\r\n	</div>\r\n</div>\r\n\r\n\r\n\r\n\r\n\r\n<div style="clear:both"></div>\r\n\r\n<br/>\r\n\r\n\r\n\r\n\r\n\r\n \r\n\r\n\r\n<div class="blue_box">\r\nI like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top"><strong>JOIN TODAY</strong></a>. </div>\r\n', '', 1, 10, '2013-05-12 03:38:09', 791, '', '2013-08-08 22:29:36', 42, 0, '0000-00-00 00:00:00', '2013-05-12 03:38:09', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 4, 24, '', '', 1, 21, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(11, 50, 'Installing The Template', 'installing-the-template', '<br /><ul class="bullet_list"><ul class="ul_numbers"><li class="li_number1">Download the installation package from our download section.</li><li class="li_number2">Once the download is complete go to the backend of Joomla.</li><li class="li_number3">     Navigate through your menu system to Extensions/Extensions Manager.</li>   </ul>   \r\n</ul>\r\n<p>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;\r\n<img border="0" src="http://www.shape5.com/demo/images/general/install_menu.png" /></p>\r\n<ul class="bullet_list"><ul class="ul_numbers">          <li class="li_number4">Once at the installation screen click the browse button and navigate to where you downloaded the template file.</li>     <li class="li_number5">Once you have the file selected click &#39;Upload File and Install&#39;</li>     \r\n  <p>\r\n  <img border="0" src="http://www.shape5.com/demo/images/general/browse.png" /></p>\r\n  <p><b>The template is now installed, now let&#39;s set it as the default template:</b><br />     </p>     <li class="li_number6">Navigate through your menu system to Extensions/Template Manager.</li><li class="li_number7">Find the radio button next to the newly installed template.</li><li class="li_number8">Click on the Default button at the top right of the screen and you&#39;re done!</li><p>\r\n  <img border="0" src="http://www.shape5.com/demo/images/general/default.png" /></ol>   </ul>   <p>&nbsp;</p>', '', 1, 10, '2013-05-12 03:38:31', 791, '', '2013-06-11 17:31:44', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:38:31', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 23, '', '', 1, 10, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(12, 51, 'Lazy Load Images', 'lazy-load-images', 'The lazy load script is a great way to save bandwidth and load your pages much faster. Images that are not visible on the initial page load are not loaded or downloaded until they come into the main viewing area. Once an image comes into view it is then downloaded and faded into visibility. Scroll down this page to see the script in action.\r\n\r\n<br /><br />\r\n\r\nSetup is very easy! By default this script is disabled, in order to enable it simply choose All Images or Individual Images from the drop down, as shown below from inside the template configuration page.\r\n\r\n<br /><br />\r\n\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/lazy_load.png" alt=""></img>\r\n\r\n<br /><br />\r\n\r\nAll images will load every standard image on the page with lazy load. There is no extra configuration or extra code to add with this configuration, it will just happen automatically. Individual images would be used if you want only certain images to load with this script and not all of them. To do this simply add class="s5_lazyload" to the image like so:\r\n\r\n<br />\r\n\r\n<div class="code">\r\n\r\n&lt;img class=&quot;s5_lazyload&quot; src=&quot;http://www.yoursite.com/image.jpg&quot;&gt;&lt;/img&gt;\r\n\r\n</div>\r\n\r\n<br /><br />\r\nThis script is compatible with Firefox3+, IE8+, Chrome14+, Safari5.05+, Opera 11.11+\r\n<br /><br />\r\n<h3>See the script in action:</h3>\r\n<br />\r\n\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load1.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load2.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load3.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load4.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load5.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load6.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load7.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load8.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load9.jpg" alt="" class="s5_lazyload padded"></img>\r\n<img src="http://www.shape5.com/demo/images/general/lazyload/lazy_load10.jpg" alt="" class="s5_lazyload padded"></img>', '', 1, 10, '2013-05-12 03:38:57', 791, '', '2013-06-11 15:52:43', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:38:57', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 22, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(13, 52, 'Login and Register Setup', 'login-and-register-setup', 'Setting up the login and register links is very simple. Just enter the login, logout, and register text in the template configuration like shown below. As long as text is present the links will show. To disable them simply empty the fields and save the configuration.  \r\n<br /><br />\r\n<strong>\r\nAlternatively, you can fill out your own custom urls just below these fields in the template configuration if you do not want to use the default Joomla login and registration pages.\r\n</strong>\r\n<br /><br />\r\n<img src="http://www.shape5.com/demo/vertex/images/login.png" class="padded s5_lazyload" style="opacity: 1;">', '', 1, 10, '2013-05-12 03:39:20', 791, '', '2013-11-26 15:53:41', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:39:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 5, 21, '', '', 1, 42, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(14, 53, 'Menu Scroll To Section', 'menu-scroll-to-section', 'This template includes a scroll to feature that will scroll your page to a specified section of your site. All you have to do is create an external link in your menu manager and then in the URL area enter in any ID on your page. You can reference any of the following IDs in order:<br /><br />\r\n\r\n<ul class="ul_bullet_small">\r\n<li>#s5_header_area1</li>\r\n<li>#s5_top_row1_area1</li>\r\n<li>#s5_top_row2_area1</li>\r\n<li>#s5_top_row3_area1</li>\r\n<li>#s5_center_area1</li>\r\n<li>#s5_bottom_row1_area1</li>\r\n<li>#s5_bottom_row2_area1</li>\r\n<li>#s5_bottom_row3_area1</li>\r\n<li>#s5_footer_area1</li>\r\n\r\n</ul>\r\n<br />\r\nScreenshot of admin area of an external menu item with DIV reference entered:<br /><br />\r\n\r\n<img border="0" src="http://www.shape5.com/demo/images/general/scrollto.jpg" style="">', '', 1, 10, '2013-05-12 03:39:46', 791, '', '2013-06-11 15:53:23', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:39:46', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 20, '', '', 1, 8, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(15, 54, 'Module Positions and Styles', 'module-positions-and-styles', '<br />\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" href="index.php/features-mainmenu-47/template-features/responsive-layout">\r\nBe sure to check out the responsive layout page to see how the layout below is constructed on multiple screen sizes</a>\r\n</div>\r\n<br />\r\n\r\n\r\n\r\n<h3>All modules are fully collapsible</h3>\r\nWhat exactly is a collapsing module? It&#39;s quite simple, whenever a module is not published to a postion that position does not appear on the frontend of the template. Consider the example below:\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/modules_6.png"></img>\r\n<br /><br />\r\nThis particular row has 6 module positions available to publish to. Let&#39;s say you only want to publish to 4 of these positions. The template will automatically collapse the modules you do not want to use and adjust the size of the modules accordingly:\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/demo/images/general/modules_4.png"></img>\r\n<br /><br />\r\nIf no modules are published to the row the entire row itself will not show. The best feature of this is every module can be published to its own unique pages so the layout of your site can change on every page!\r\n<br /><br /><br />\r\n<h3>Infinite Layouts</h3>\r\nBecause there are so many module positions available in so many different areas, the number of layouts you can create are limitless! For example, if you would like to show your main content area on the right side of your site but still have a column of modules, simply published your modules to the right or right_inset positions or both. The same would be true for the opposite, if you want a column on the left simply publish modules to left or left_inset. Of course you can always choose to not have a column at all. Remember, any module not published to will automatically collapse and the remaining area will automatically adjust. There is no need to choose a pre-defined layout for your entire site, simply use the power of collpasing module positions and take advantage of the numerous amount of module positions to create any layout you can dream of! Be sure to checkout the layout of modules below.\r\n<br /><br /><br />\r\n<h3>Dozens of Modules</h3>\r\nBelow is a complete list of all the module positions available for this template.\r\n<br /><br />\r\n<img class="padded" src="http://www.shape5.com/images/products/2013/design_control/design_control_layout.png"></img>\r\n<br /><br />\r\n\r\n<h3>How to install and setup module styles:</h3></p> <ol> <li>   Download any module you wish to publish to your site.</li> <li>In the backend of Joomla    navigate to the menu item Extensions/Install Uninstall</li> <br />   <img class="padded" style="margin-bottom:14px" src="http://www.shape5.com/demo/images/general/install_menu.png" border="0" width="199" height="172" /><br /> <li>Browse for the module&#39;s install file and click Upload File & Install.</li>  <li>   Once the module has be installed navigate to the menu item Extensions/Module    Manager (same menu as above)</li>  <li>Find the Module just installed and click on it&#39;s title.</li>  <li>   Change any parameters that you wish and be sure to set it to published and    publish it to your desired module position.</li><li>To apply a module style simply fill in the module class suffix field with any of this template&#39;s included module styles. This parameter setting is found under Module Parameters on the right side of the screen. </li> <br />   <img class="padded" style="margin-bottom:14px" src="http://www.shape5.com/demo/images/general/module_suffix.png" border="0" width="200" height="72" /><br /> <li>Assign what pages you would like the module to appear on and finally click Save.</li><br />   <img class="padded" src="http://www.shape5.com/demo/images/general/page_assignment.png" border="0" width="200" height="144" /><br />   </ol>            ', '', 1, 10, '2013-05-12 03:40:20', 791, '', '2013-11-25 20:15:43', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:40:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 8, 19, '', '', 1, 165, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(16, 55, 'Multibox', 'multibox', '<br/>\r\n\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nFor a tutorial on how to setup this script be sure to view the Vertex Guide on our Tutorials page.</a>\r\n</div>\r\n\r\n\r\n<br/><h3>Features:</h3>\r\n\r\n<ul class="ul_bullet_small">\r\n<li>Supports a range of multimedia formats: images, flash, video, mp3s, html!</li>\r\n<li>Auto detects formats or you can specify the format</li>\r\n<li>Html descriptions</li>\r\n<li>Enable/Disable page overlay when multibox pops up (via template parameters)</li>\r\n<li>Enable/Disable controls (via template parameters)</li>\r\n\r\n\r\n\r\n\r\n<br/>\r\n\r\n\r\n\r\n<p><strong><font size="2">Images Example</font></strong></p>          \r\n\r\n<a href="http://www.shape5.com/demo/images/multibox1_lrg.jpg" id="mb1" class="s5mb" title="Image #1:">\r\n<img src="http://www.shape5.com/demo/images/multibox1.jpg" style="margin-right:20px" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb1">Image #1. It can support <strong>html</strong>.</div>\r\n&nbsp;&nbsp;\r\n<a href="http://www.shape5.com/demo/images/multibox2_lrg.jpg" id="mb2" class="s5mb" title="Image #2:">\r\n<img src="http://www.shape5.com/demo/images/multibox2.jpg" style="margin-right:20px" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb2">Image #2. It can support <strong>html</strong>.</div>\r\n&nbsp;&nbsp;\r\n<a href="http://www.shape5.com/demo/images/multibox3_lrg.jpg" id="mb3" class="s5mb" title="Image #3:">\r\n<img src="http://www.shape5.com/demo/images/multibox3.jpg" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb3">Image #3. It can support <strong>html</strong>.</div>\r\n\r\n\r\n\r\n<div style="clear:both"></div>\r\n\r\n\r\n<p><strong><font size="2">Separeate Group Images Example</font></strong></p>          \r\n\r\n<a href="http://www.shape5.com/demo/images/multibox1_lrg.jpg" rel="[group1]" id="mb9" class="s5mb" title="Image #1:">\r\n<img src="http://www.shape5.com/demo/images/multibox1.jpg" style="margin-right:20px" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb9">Image #1. It can support <strong>html</strong>.</div>\r\n&nbsp;&nbsp;\r\n<a href="http://www.shape5.com/demo/images/multibox2_lrg.jpg" rel="[group1]" id="mb10" class="s5mb" title="Image #2:">\r\n<img src="http://www.shape5.com/demo/images/multibox2.jpg" style="margin-right:20px" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb10">Image #2. It can support <strong>html</strong>.</div>\r\n&nbsp;&nbsp;\r\n<a href="http://www.shape5.com/demo/images/multibox3_lrg.jpg" rel="[group1]" id="mb11" class="s5mb" title="Image #3:">\r\n<img src="http://www.shape5.com/demo/images/multibox3.jpg" class="boxed" alt="" />\r\n</a>\r\n<div class="s5_multibox mb11">Image #3. It can support <strong>html</strong>.</div>\r\n\r\n\r\n\r\n<div style="clear:both"></div>\r\n\r\n\r\n\r\n\r\n\r\n<p><strong><font size="2">Video Example:</font></strong></p>   \r\n\r\n<a href="http://www.youtube.com/v/VGiGHQeOqII" id="youtube" class="s5mb" title="Youtube.com Video">\r\nYoutube.com Video - CLICK ME\r\n</a>\r\n<div class="s5_multibox youtube">UP: Carl and Ellie </div>\r\n\r\n<br/>\r\n<br/>\r\nYou can use the following video formats: flv, mov, wmv, real and swf.  Just insert the URL to the videos in the href of the hyperlink, here is an example of how we did this for a Youtube video:<br/>\r\n\r\n\r\n\r\n<br/>\r\nYouTube Tutorial:  Simply right click on a youtube video and copy the embed code, then paste into a text editor and look for the embed src and use that URL in your hyperlink.\r\n\r\n\r\n\r\n\r\n<br/><br/>\r\n\r\n\r\n<p><strong><font size="2">MP3 Example:</font></strong></p>   \r\n\r\n<a href="http://www.shape5.com/demo/images/music.mp3"  id="mb8" class="s5mb" title="Music">MP3 example - CLICK ME</a>\r\n<div class="s5_multibox mb8">mp3 example</div><br />\r\n\r\n\r\n\r\n\r\n<br/>\r\n\r\n\r\n<p><strong><font size="2">iFrame:</font></strong></p>   \r\n\r\n<a href="http://www.getfirebug.com" rel="width:790,height:600" id="mb28" class="s5mb" title="getfirebug.com">iFrame/HTML Example - CLICK ME</a>\r\n<div class="s5_multibox mb28">getfirebug.com</div><br />\r\n\r\n\r\n\r\n\r\n<br/><br/>\r\n\r\n\r\n\r\n\r\n\r\n \r\n\r\n\r\n<div class="blue_box">\r\nI like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top"><strong>JOIN TODAY</strong></a>. </div>\r\n <br />', '', 1, 10, '2013-05-12 03:40:47', 791, '', '2013-06-11 15:46:48', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:40:47', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 28, 18, '', '', 1, 37, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(17, 56, 'Page, Column and Row Widths', 'page-column-and-row-widths', '<br />\r\n<h3>Fixed or Fluid Width</h3>\r\nThis template has the ability to set the entire width of your set to either a \r\nfixed pixel width or a fluid percentage width. You can set the width to any size \r\nyou want.\r\n\r\n<br /><br />\r\n\r\n\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" href="index.php/features-mainmenu-47/template-features/responsive-layout">\r\nBe sure to check out the responsive layout page to see how the layout below is constructed on multiple screen sizes. The responsive layout will work with both fixed or fluid settings. Responsive layouts are completely optional.</a>\r\n</div>\r\n<br />\r\n\r\n<br /><h3>Column Widths</h3>\r\nYou may also set the widths of the following positions to any width that you \r\nneed to: left, left_inset, right, and right_inset. You may set them to any width \r\nyou need to. Columns can either be set to a fixed px width or they can be set to a fluid percentage width. If you are enabling the responsive layout we recommend setting these to percentage width.\r\n<br />\r\n<br /><br /><h3>Row Widths</h3>\r\nThis template comes loaded with module positions, many of which appear in rows \r\nof 6 module positions. Any row that contains 6 module positions can have it&#39;s \r\nrow columns set to automatic widths or manual. For example, in the picture below \r\nthe first row shows 4 modules published and since it&#39;s set to automatic each is \r\nset to 25% width. The second row shows a manual calculation for each module in \r\nthe row. Again, you may do this for any row that contains 6 modules. If you \r\nsetup a manual calculation they must total to 100%. Not all 6 modules need to be \r\nused, as shown below.<p>\r\n</p>\r\n<p>All of this is done very easily in the template configuration.</p>\r\n<p align="center">\r\n<img style="border:solid 1px #333333" src="http://www.shape5.com/demo/images/general/custom_widths.png" width="600" height="432" /></p>\r\n<br />', '', 1, 10, '2013-05-12 03:41:17', 791, '', '2013-06-11 17:29:01', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:41:17', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 17, '', '', 1, 9, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(18, 57, 'Responsive Layout', 'responsive-layout', 'Responsive design at its most elementary definition is simply the use of css media queries and some javascript to rearrange a website''s layout to fit multiple screen sizes. This can range from a wide screen monitor down to the smallest of smart phones. It is done all through the same site, same installation, sharing the same files, and no extra layouts needed as in the past. Unlike many pixel grid based templates, Shape5 Vertex templates are already built on a fluid width layout based on percentages, with an optional pixel width wrapper, which is what responsive layouts require. Because of this Vertex templates do not require a responsive layout, instead it is completely optional! That''s right, if you''re uncomfortable with responsive layouts or it won''t work with your site, then simply turn it off and use the standard fixed or fluid width layout instead!\r\n<br /><br />\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nBe sure to visit our Tutorials page and download our Responsive Best Practices guide for useful responsive layout tips.</a>\r\n</div>\r\n<br />\r\n\r\n\r\n<h3>See It Live On Responsinator.com</h3>\r\n<a target="_blank" href="http://responsinator.com/?url=http%3A%2F%2Fwww.shape5.com%2Fdemo%2Fdesign_control">Open this website on responsinator.com to see its layout on multiple devices.</a>\r\n<br /><br />\r\n\r\n\r\n\r\n<h3>What Makes The Vertex Responsive Layout The Best and Most Flexible?</h3>\r\n<ul>\r\n<li>The responsive layout is completely optional, don''t use it if you don''t want it!</li>\r\n<li>Virtually no javascript. The only javascript used is for the mobile bar effects and to add responsive capabilites to IE7. We believe that javascript should only be used as a last resort.</li>\r\n<li>Start with a fixed or fluid layout.</li>\r\n<li>Every area of the template, ie: columns, row, and body; can be set to a fluid layout.</li>\r\n<li>Automatic onclick menu functionality for small touch screens.</li>\r\n<li>Optional IE7/8 responsive layout.</li>\r\n<li>Very easy to understand layout, which allows you to setup your site with very little effort.</li>\r\n<li>Fully customizable mobile bar styling.</li>\r\n<li>Optional row re-distribution at smaller screens.</li>\r\n<li>Hide divs, modules and content easily with hide classes at specified screen sizes.</li>\r\n<li>Wide screen support options.</li>\r\n<li>Multiple options for fluid width images.</li>\r\n<li>And much more!</li>\r\n</ul>\r\n<br />\r\n\r\n\r\n\r\n<h3>Easy To Understand Layout</h3>\r\nOne of the biggest advantages to the Vertex Responsive Layout is that we start the layout as if it were designed for a desktop viewer, and then simply reduce, modify, or remove certain sections or font sizes based on the size of the user''s screen. This is all done through css media queries, and is all done automatically for you! \r\n<br /><br />\r\nThe best way to see this approach, is simply resize your browser to see it all take effect.\r\n<br /><br />\r\nSome responsive designs simply use a series of floating divs that re-arrange themselves on the page and add columns as the screen width increases or decreases. The biggest con to this approach is it can become very confusing as to where your content will actually exist on any given screen; making it very hard to create a specific layout and confusing for a client; especially those with specific product and content placement needs. \r\n<br /><br />\r\nOur approach is much easier to understand. The site will always keep its core layout shown <a href="index.php/features-mainmenu-47/template-features/95-module-positions">here</a>. When the screen changes it will adjust as described below. The majority of rows and columns will not change their basic styling or positions until they reach the mobile version. Again, only minor adjustments are made on varying screen sizes so that you always know what your site will look like and where content will be placed!<br /><br />\r\n<ul>\r\n<li>If the columns are set to use fluid widths then no changes will occur for large screens, everything will simply be percentage widths based on the availalbe area. If you are using the max-body width calculations the column widths will be based off of this setting instead of screen width.</li>\r\n<li>Between 1050px and 600px if you have set the columns to fluid widths, you have an option to combine the inset columns, if they are shown on the page, into a single column with their adjacent column. In other words right_inset and right become one column to save space. Because each site is different, you can choose the screen size to specify this change.\r\n<li>At 580px (a standard mobile device size) the entire body will become one single column and module stack on top of each other. Some adjustments to the header and footer will occur.</li>\r\n<li>Please note - you have an option to use either fluid or fixed width columns (right_inset, right, left_inset, and left). We highly recommend using fluid columns when responsive is enabled. If you choose fixed widths there are other responsive options available for this setting as well in the template configuration under the Layout tab.\r\n</ul>\r\n<br />\r\n\r\nBelow are illustrations of the center columns only in the scenarios described above.\r\n<br /><br />\r\n\r\n\r\n<img class="padded" alt="" src="http://www.shape5.com/demo/images/general/responsive_columns.png"></img>\r\n\r\n<br /><br />\r\n\r\n<h3>Row Re-Distribution</h3>\r\nEach row in a Vertex template contains six module positions. If you are using a lot of modules in a particular row the content can become squished when the screen becomes too narrow, specifically on smaller seven inch tablet screens. The row re-distribution features allow you to change the layout into multiple rows with new module widths to give each position more room at specific screen sizes. You get to specify the screen width that this change will trigger at, and you can specify from several preset width options for each row indepent of each other. Single Column will set all modules in that row to 100% and one module per row. Two Columns will set all modules in that row to 50% and two modules per row. Three Columns will set all modules in that row to 33.33% and three modules per row. Redis. 33/50 will set positions 1 through 3 to 33.33% and positions 4 through 6 to 50%, this setting is meant to be used when positions 1 through 5 are in use. Redis. 50/100 will set positions 1 through 2 to 50% and positions 3 through 6 to 100%. Redis. 100/33 will set position 1 to 100% and positions 2 through 6 to 33.33%. Redis. 100/50 will set position 1 to 100% and positions 2 through 6 to 50%. This setting will override any custom row widths you set under the Row Sizes tab when the screen size reaches the selected width. All modules will always change to a single column at 580px for mobile view. This feature is NOT available for IE7 or IE8!\r\n<br /><br />\r\n\r\n\r\n\r\nBelow you will find a screenshot of the responsive options available in the Vertex framework. Below that you will find documentation for each feature shown.\r\n<br /><br />\r\n<img class="padded" alt="" src="http://www.shape5.com/demo/images/general/responsive1.png"></img>\r\n<br /><br />\r\n\r\n\r\n<h3>General Layout</h3>\r\nThe general layout options are the ones starting with Enable Font Resizer? and ending with Right Inset Width. These parameters are the core layout options that apply to all templates, whether responsive is enabled or not. Configuring this area is the first step in configuring a responsive layout. You can choose to set the site to either a fixed pixel width or fluid percentage width, the responsive layout will work with either option. If you are using the fluid width option we recommend enabling the Max Body Width option so that that your site does not go above the set pixels; this helps keep your site looking proportionate across very wide screen.\r\n<br /><br />\r\n\r\n\r\n\r\n<h3>Enable The Responsive Layout</h3>\r\nIf you wish to use the responsive layout simply turn on the switch and the Vertex framework will take care of the rest! It''s really that simple! Notice there is a separate switch for IE7 and IE8. This browser does not support css media queries natively, which is what makes responsive layouts possible. In order to add this functionality the template must use javascript, which can affect the performance of a website. With this added javascript IE7 and IE8 will support the responsive layout, but we highly recommend leaving it turned off. Keep in mind that the primary target for responsive websites is tablet and mobile devices, where IE7 and IE8 do not exist. Please also note that some features such as the row re-distribution functions are not supported by IE7/8.\r\n<br /><br />\r\n\r\n\r\n\r\n<h3>Hide Tablet and Mobile Divs</h3>\r\nOne of the biggest obstacles to overcome when designing a responsive layout site is that not all content, images and extensions were designed to work with responsive layouts. That means that sometimes you need the ability to hide a specific element on only certain sized screens, so that something doesn''t break the site''s layout and everything looks proportionate. We''ve made that all possible and very easy to do for you with hiding classes! There are three main ways to hide content on different size screens, and they are documented <a href="index.php/features-mainmenu-47/template-features/hide-content-and-modules">here</a>.\r\n<br /><br />\r\n\r\n\r\n<h3>Column Settings</h3>\r\nDepnding on whether you have set the side columns to fixed or fluid widths, you have several options to choose from. We do highly recommend fluid columns when responsive is enabled. The first option, which pertains to both fixed and fluid, allows you to combine articles, will set modules published to middle_top and middle_bottom positions as well as articles published to the component area to a single column at the chosen screen width, this setting effects only those areas and nothing else. The settings below that are split into two sections depending on your column width setting. These settings effect combining the inset columns, creating a single column, and reducing the column widths. Be sure to read the tooltip of each section for a full description.\r\n<br /><br />\r\n\r\n\r\n<h3>Mobile Bars</h3>\r\nWhen the screen size reaches 750px wide (anything smaller than a standard 10 inch tablet), a navigation bar will appear across the top of the site and the bottom. This bar replaces the main menu, login, register, and search, for easier user on mobile devices. In the configuration you can choose what to enable on these bars as well as change the colors and style however you would like.\r\n<br /><br />\r\n<img class="padded" alt="" src="http://www.shape5.com/demo/images/general/responsive2.jpg"></img>\r\n<br /><br />\r\n<img class="padded" alt="" src="http://www.shape5.com/demo/images/general/responsive3.jpg"></img>\r\n<br /><br />\r\n\r\n\r\n\r\n<h3>Mobile Links</h3>\r\nEven though this is a responsive layout and there is no separate layout page for mobile devices, that does not mean that your site''s viewers will understand this or even know what a responsive layout is. It has become a standard for websites that use a mobile layout to have a link to view the desktop view of the website, and this is what your site viewers will expect to see. This link simply turns off the responsive configuration using a cookie and calls the website like a standard desktop would when viewing in mobile. There is then a link to return back to the mobile view of the site presented.\r\n<br /><br />\r\n\r\n\r\n<div class="blue_box">\r\nI like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top"><strong>JOIN TODAY</strong></a>. </div>', '', 1, 10, '2013-05-12 03:41:33', 791, '', '2013-11-25 19:12:01', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:41:33', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 6, 16, '', '', 1, 276, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(20, 59, 'S5 Drop Down Panel', 's5-drop-down-panel', 'The S5 Drop Down Panel is a slide down panel that can be demo&#39;d at the top of this page. The panel itself contains six module positions. You may publish any module that you wish into these positions. It comes packed with features so be sure to check out the list and screenshot below.\r\n<br />\r\n<br />\r\nNote - If the responsive layout is enabled the drop down will disable when the screen size reaches 750px so that it does not interfere with the mobile menu bar\r\n\r\n<br />\r\n<br />\r\n<img alt="" style="border:1px solid #CCCCCC" class="padded" src="http://www.shape5.com/demo/images/general/dropdown_tab.png" />\r\n<div style="clear:both"></div>\r\n<br />\r\n<ul class="ul_star"> \r\n<li>Customize almost everything! Shadows, borders, gradient, opacity</li> \r\n<li>Contains 6 module positions drop_down_1, drop_down_2, drop_down_3, drop_down_4, drop_down_5 and drop_down_6</li>\r\n<li>Auto adjust to the height of your content</li> \r\n<li>Set your own open and close text</li> \r\n<li>Auto collapse if no modules are published to it</li>\r\n<li>And many more features!</li>\r\n</ul>\r\n<br />\r\n<br />\r\n<h3>Screenshot of Drop Down admin in template configuration area:</h3><br />\r\n<img alt="" style="border:1px solid #CCCCCC" class="padded" src="http://www.shape5.com/demo/images/general/dropdown.png" />\r\n\r\n<div style="clear:both; height:0px"></div>', '', 1, 10, '2013-05-12 03:42:02', 791, '', '2013-06-11 15:54:45', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:42:02', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 14, '', '', 1, 32, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(21, 60, 'S5 Flex Menu - Menu System', 's5-flex-menu-menu-system', 'The S5 Flex Menu system is a very powerful plugin that provides functionality \r\nfar beyond what the standard Joomla menu system can provide. This plugin \r\nis intended to be as an extension of the existing menu system in Joomla to add many new features! Please see the full list of features below. \r\n\r\n<br /><br />\r\n\r\nThis menu system works off of the core Joomla jquery/mootools scripts so no extra javascript library is needed and keep download sizes to a minimum. Also, if you do not want to use this menu you can simply turn it it off from the template configuration page.\r\n<br /><br />\r\n\r\nTake your website to  the next design level by using the robust and feature\r\nrich S5 Flex Menu System. Organize your links with ease and show content \r\nin places you never could before!\r\n\r\n<br /><br />\r\n<h3>Menu Features:</h3>\r\n\r\n\r\n\r\n<ul class="ul_star">\r\n<li>Automatic onclick functionality for tablet sized touch screens. If a device''s screen is detected as touch screen and is of table size, then the menu will function with an onclick method rather than the onmouseover effect that is shown on a laptop or desktop that has a mouse for use.</li>\r\n<li>Multiple javascript effects such as fade, slide, etc.</li>\r\n<li>Multiple columns for menu items or modules.</li>\r\n<li>Modules load directly into the menu.</li>\r\n<li>Group sub menu items into the same column or fly out.</li>\r\n<li>Optional sub texts for each menu item.</li>\r\n<li>Optional menu icon images for each menu item.</li>\r\n<li>Automatic multi-language menu change. If you are setting up a multi-language site, the flex menu will automatically change if your site''s visitor has a language specific menu setup for their language in the menu manager.</li>\r\n<li>And much more!</li>\r\n</ul>\r\n\r\n\r\n<br /><br />\r\n<h3>Menu Screenshot:</h3>\r\n\r\n<img class="padded" src="images/flexmenu.jpg" alt=""></img>\r\n\r\n<div style="clear:both;height:0px"></div>\r\n\r\n<br /><br />\r\n\r\n\r\n\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div><br /></ul>', '', 1, 10, '2013-05-12 03:42:18', 791, '', '2013-08-12 14:45:24', 42, 0, '0000-00-00 00:00:00', '2013-05-12 03:42:18', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 13, '', '', 1, 105, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(24, 63, 'Sample Article 1', 'sample-article-1', '<p>Qui mea vix duo no lorem ipsum dolor sit amet, minim menandri eam at, usu ex modo copiosae. Cibo atomorum ocurreret ei qui, causae fabellas sapientem quo an, ut nemore aperiri appellantur nec.</p>\r\n', '\r\n<p>Ei primis assentior deseruisse vel. Ne est magna dolorem temporibus. Ne latine molestiae qui, an eum soluta dicunt vulputate, augue ignota sadipscing mel ut. Prompta constituam usu in. <br /><br /> Idque veniam pri et, no suscipit probatus voluptatibus sea. Odio laudem persius ex has. Percipit atomorum ne mea. Ad nec veniam nominavi accusata, an quo recteque assueverit, ex his vocibus accusamus. <br /><br /> Pro eu integre labitur detraxit, aperiri eruditi prodesset eum eu. Dolorum sapientem ei sit, duo no diam sensibus accommodare. His ex quaeque convenire, an denique cotidieque vix, eos epicurei urbanitas in. Mea ea fabulas suscipit efficiendi, quo ea soluta timeam assueverit. Paulo labore euismod ei vel, vim quod everti an. <br /><br /> Qui ea quod liber, vis mazim nostro id. Id omnis tempor duo, liber legendos cu duo. Mei nominavi honestatis te, feugiat vulputate eloquentiam eam in. Brute melius mea at. Te qui ferri debet noluisse, mea minimum aliquando ea, ea possit perpetua vix. <br /><br /> Labore perpetua tincidunt per cu. Ne diam conceptam inciderint mea, no mea vidit menandri. Omnium lucilius ad sed, ea eirmod noluisse euripidis vel. Ex sed detraxit corrumpit, sea graeco recteque at. Ut mundi adolescens quo. <br /><br /> Nec paulo nobis platonem ex, facer fuisset constituto quo eu. An vis diam ancillae qualisque. Has suas lucilius interpretaris at, id mel etiam vidisse percipit. Ut sed purto reprehendunt, ea has erroribus constituto referrentur. No sea quaestio facilisis intellegat, cum meis omnis urbanitas in. Stet appetere partiendo cu sed. <br /><br /> Sed ei aperiri constituam, at mea zril noster. Vis ex nibh mazim legimus. Ex vis decore inimicus appellantur. Sit modo vide ex, ut vis nonumes adversarium, agam justo mea ea. Id alii abhorreant pro, cetero iriure vis ut. <br /><br /> Ex ignota labitur sea, no postea periculis nam. Mei congue consul populo ad, ut facilisi posidonium quo. At mundi verear eam, id his ridens graeci, est quod erat sapientem id. Ubique phaedrum praesent no nam. Vim eruditi oporteat ad, ex enim pertinax vix. <br /><br /> Tota alterum nostrum est ex, duo ea eligendi eleifend neglegentur. Ea sea nostrum mandamus interesset, at pro malorum cotidieque definitiones, ex iuvaret saperet explicari his. Ius in affert sanctus scripserit, minim fastidii democritum et vel, pro ut fuisset consulatu. Per et ceteros comprehensam, ei cibo viderer mel. Aeterno pericula quo no, vix id vitae comprehensam.</p>', 1, 11, '2013-05-12 03:43:38', 791, '', '2013-08-12 18:03:34', 42, 0, '0000-00-00 00:00:00', '2013-05-12 03:43:38', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 3, 2, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(25, 64, 'Sample Article 2', 'sample-article-2', '<p>Idque veniam pri et, no suscipit probatus voluptatibus sea. Odio laudem persius ex has. Percipit atomorum ne mea. Ad nec veniam nominavi accusata, an quo recteque assueverit, ex his vocibus accusamus.</p>\r\n', '\r\n<p><br /><br /> Lorem ipsum dolor sit amet, minim menandri eam at, usu ex modo copiosae. Cibo atomorum ocurreret ei qui, causae fabellas sapientem quo an, ut nemore aperiri appellantur nec. <br /><br /> Ei primis assentior deseruisse vel. Ne est magna dolorem temporibus. Ne latine molestiae qui, an eum soluta dicunt vulputate, augue ignota sadipscing mel ut. Prompta constituam usu in. <br /><br /> Pro eu integre labitur detraxit, aperiri eruditi prodesset eum eu. Dolorum sapientem ei sit, duo no diam sensibus accommodare. His ex quaeque convenire, an denique cotidieque vix, eos epicurei urbanitas in. Mea ea fabulas suscipit efficiendi, quo ea soluta timeam assueverit. Paulo labore euismod ei vel, vim quod everti an. <br /><br /> Qui ea quod liber, vis mazim nostro id. Id omnis tempor duo, liber legendos cu duo. Mei nominavi honestatis te, feugiat vulputate eloquentiam eam in. Brute melius mea at. Te qui ferri debet noluisse, mea minimum aliquando ea, ea possit perpetua vix. <br /><br /> Labore perpetua tincidunt per cu. Ne diam conceptam inciderint mea, no mea vidit menandri. Omnium lucilius ad sed, ea eirmod noluisse euripidis vel. Ex sed detraxit corrumpit, sea graeco recteque at. Ut mundi adolescens quo. <br /><br /> Nec paulo nobis platonem ex, facer fuisset constituto quo eu. An vis diam ancillae qualisque. Has suas lucilius interpretaris at, id mel etiam vidisse percipit. Ut sed purto reprehendunt, ea has erroribus constituto referrentur. No sea quaestio facilisis intellegat, cum meis omnis urbanitas in. Stet appetere partiendo cu sed. <br /><br /> Sed ei aperiri constituam, at mea zril noster. Vis ex nibh mazim legimus. Ex vis decore inimicus appellantur. Sit modo vide ex, ut vis nonumes adversarium, agam justo mea ea. Id alii abhorreant pro, cetero iriure vis ut. <br /><br /> Ex ignota labitur sea, no postea periculis nam. Mei congue consul populo ad, ut facilisi posidonium quo. At mundi verear eam, id his ridens graeci, est quod erat sapientem id. Ubique phaedrum praesent no nam. Vim eruditi oporteat ad, ex enim pertinax vix. <br /><br /> Tota alterum nostrum est ex, duo ea eligendi eleifend neglegentur. Ea sea nostrum mandamus interesset, at pro malorum cotidieque definitiones, ex iuvaret saperet explicari his. Ius in affert sanctus scripserit, minim fastidii democritum et vel, pro ut fuisset consulatu. Per et ceteros comprehensam, ei cibo viderer mel. Aeterno pericula quo no, vix id vitae comprehensam.</p>', 1, 11, '2013-05-12 03:43:54', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-05-12 03:43:54', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(26, 65, 'Sample Article 3', 'sample-article-3', '<p>Lorem diceret ut vim, qui ipsum dolor sit amet, minim menandri eam at, usu ex modo copiosae. Cibo atomorum ocurreret ei qui, causae fabellas sapientem quo an, ut nemore aperiri appellantur nec.</p>\r\n', '\r\n<p><br /><br /> Ei primis assentior deseruisse vel. Ne est magna dolorem temporibus. Ne latine molestiae qui, an eum soluta dicunt vulputate, augue ignota sadipscing mel ut. Prompta constituam usu in. <br /><br /> Idque veniam pri et, no suscipit probatus voluptatibus sea. Odio laudem persius ex has. Percipit atomorum ne mea. Ad nec veniam nominavi accusata, an quo recteque assueverit, ex his vocibus accusamus. <br /><br /> Pro eu integre labitur detraxit, aperiri eruditi prodesset eum eu. Dolorum sapientem ei sit, duo no diam sensibus accommodare. His ex quaeque convenire, an denique cotidieque vix, eos epicurei urbanitas in. Mea ea fabulas suscipit efficiendi, quo ea soluta timeam assueverit. Paulo labore euismod ei vel, vim quod everti an. <br /><br /> Qui ea quod liber, vis mazim nostro id. Id omnis tempor duo, liber legendos cu duo. Mei nominavi honestatis te, feugiat vulputate eloquentiam eam in. Brute melius mea at. Te qui ferri debet noluisse, mea minimum aliquando ea, ea possit perpetua vix. <br /><br /> Labore perpetua tincidunt per cu. Ne diam conceptam inciderint mea, no mea vidit menandri. Omnium lucilius ad sed, ea eirmod noluisse euripidis vel. Ex sed detraxit corrumpit, sea graeco recteque at. Ut mundi adolescens quo. <br /><br /> Nec paulo nobis platonem ex, facer fuisset constituto quo eu. An vis diam ancillae qualisque. Has suas lucilius interpretaris at, id mel etiam vidisse percipit. Ut sed purto reprehendunt, ea has erroribus constituto referrentur. No sea quaestio facilisis intellegat, cum meis omnis urbanitas in. Stet appetere partiendo cu sed. <br /><br /> Sed ei aperiri constituam, at mea zril noster. Vis ex nibh mazim legimus. Ex vis decore inimicus appellantur. Sit modo vide ex, ut vis nonumes adversarium, agam justo mea ea. Id alii abhorreant pro, cetero iriure vis ut. <br /><br /> Ex ignota labitur sea, no postea periculis nam. Mei congue consul populo ad, ut facilisi posidonium quo. At mundi verear eam, id his ridens graeci, est quod erat sapientem id. Ubique phaedrum praesent no nam. Vim eruditi oporteat ad, ex enim pertinax vix. <br /><br /> Tota alterum nostrum est ex, duo ea eligendi eleifend neglegentur. Ea sea nostrum mandamus interesset, at pro malorum cotidieque definitiones, ex iuvaret saperet explicari his. Ius in affert sanctus scripserit, minim fastidii democritum et vel, pro ut fuisset consulatu. Per et ceteros comprehensam, ei cibo viderer mel. Aeterno pericula quo no, vix id vitae comprehensam.</p>', 1, 11, '2013-05-12 03:44:11', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-05-12 03:44:11', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(27, 66, 'Sample Content', 'sample-content', '<p>Integer varius tempor auctor. Etiam fringilla venenatis mollis. Duis ullamcorper massa eu sapien fringilla consequat. Aliquam nec ligula mi, quis tincidunt odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rutrum dui eros. Praesent nec nibh diam. <br /><br /> Proin augue risus, molestie at dictum quis, viverra non ipsum. Praesent lacus dui, euismod ut mollis vel, lacinia vel arcu. In commodo arcu vitae velit elementum a luctus nibh mattis. Ut vel turpis eros. Nam mattis velit sed nibh scelerisque in bibendum felis sodales. Vivamus ornare pellentesque pellentesque. Nullam dignissim semper quam nec mollis. In vel lacus lectus, ac tristique leo. <br /><br /> Aenean venenatis egestas iaculis. Nullam consectetur condimentum dolor at bibendum. Nulla in enim quis ipsum pulvinar imperdiet vitae nec velit. Donec non urna quam. Aliquam congue magna nec risus iaculis posuere. Vivamus bibendum interdum molestie. Sed libero risus, varius eu dictum ac, pharetra ac elit. Curabitur a nibh id ipsum sagittis blandit. Morbi cursus commodo leo quis rhoncus. In nec purus magna, id porta enim. Donec pulvinar aliquet vulputate. Donec sit amet justo sit amet ipsum posuere imperdiet id sed magna. <br /><br /> Sed ultricies condimentum augue, sed congue erat dapibus nec. Etiam quis tempor nibh. Vestibulum urna dui, sodales ut ornare vitae, porta non augue. Curabitur nunc ipsum, facilisis nec luctus blandit, luctus at sem. Vestibulum pharetra augue vitae orci mollis scelerisque. Quisque gravida diam vel mauris ultricies ac faucibus turpis volutpat. Maecenas augue elit, rutrum pharetra mattis ullamcorper, commodo varius enim. Phasellus dapibus mattis ipsum sit amet tincidunt. In hac habitasse platea dictumst. Sed nisi dolor, ullamcorper eget pretium molestie, commodo non dolor. Praesent volutpat aliquet neque ut bibendum. Pellentesque tincidunt tortor non ipsum venenatis ultricies. <br /><br /> Proin at justo eu orci condimentum ornare sed pellentesque orci. Duis nec interdum massa. Proin porta semper enim, sit amet condimentum lorem interdum non. Etiam vel sodales tellus. Nullam vulputate luctus sapien, in feugiat arcu pulvinar vitae. Phasellus risus orci, posuere non fringilla nec, placerat sit amet mauris. Mauris a sem turpis, vitae hendrerit risus. Mauris suscipit, purus ac ornare elementum, enim dolor convallis mauris, sit amet convallis urna metus at leo. Curabitur eget nisl a risus suscipit auctor. Ut a ultrices lectus. Pellentesque sed justo magna. Donec congue mi id odio sodales tincidunt. Integer cursus leo in libero imperdiet ac pharetra tortor venenatis. In nec purus at justo adipiscing condimentum.</p>', '', 1, 2, '2013-05-12 03:44:29', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-05-12 03:44:29', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 2, '', '', 1, 2, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(28, 67, 'Sample Content 2', 'sample-content-2', '<p>Integer varius tempor auctor. Etiam fringilla venenatis mollis. Duis ullamcorper massa eu sapien fringilla consequat. Aliquam nec ligula mi, quis tincidunt odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rutrum dui eros. Praesent nec nibh diam. <br /><br /> Proin augue risus, molestie at dictum quis, viverra non ipsum. Praesent lacus dui, euismod ut mollis vel, lacinia vel arcu. In commodo arcu vitae velit elementum a luctus nibh mattis. Ut vel turpis eros. Nam mattis velit sed nibh scelerisque in bibendum felis sodales. Vivamus ornare pellentesque pellentesque. Nullam dignissim semper quam nec mollis. In vel lacus lectus, ac tristique leo. <br /><br /> Aenean venenatis egestas iaculis. Nullam consectetur condimentum dolor at bibendum. Nulla in enim quis ipsum pulvinar imperdiet vitae nec velit. Donec non urna quam. Aliquam congue magna nec risus iaculis posuere. Vivamus bibendum interdum molestie. Sed libero risus, varius eu dictum ac, pharetra ac elit. Curabitur a nibh id ipsum sagittis blandit. Morbi cursus commodo leo quis rhoncus. In nec purus magna, id porta enim. Donec pulvinar aliquet vulputate. Donec sit amet justo sit amet ipsum posuere imperdiet id sed magna.</p>\r\n', '\r\n<p><br /><br /> Sed ultricies condimentum augue, sed congue erat dapibus nec. Etiam quis tempor nibh. Vestibulum urna dui, sodales ut ornare vitae, porta non augue. Curabitur nunc ipsum, facilisis nec luctus blandit, luctus at sem. Vestibulum pharetra augue vitae orci mollis scelerisque. Quisque gravida diam vel mauris ultricies ac faucibus turpis volutpat. Maecenas augue elit, rutrum pharetra mattis ullamcorper, commodo varius enim. Phasellus dapibus mattis ipsum sit amet tincidunt. In hac habitasse platea dictumst. Sed nisi dolor, ullamcorper eget pretium molestie, commodo non dolor. Praesent volutpat aliquet neque ut bibendum. Pellentesque tincidunt tortor non ipsum venenatis ultricies. <br /><br /> Proin at justo eu orci condimentum ornare sed pellentesque orci. Duis nec interdum massa. Proin porta semper enim, sit amet condimentum lorem interdum non. Etiam vel sodales tellus. Nullam vulputate luctus sapien, in feugiat arcu pulvinar vitae. Phasellus risus orci, posuere non fringilla nec, placerat sit amet mauris. Mauris a sem turpis, vitae hendrerit risus. Mauris suscipit, purus ac ornare elementum, enim dolor convallis mauris, sit amet convallis urna metus at leo. Curabitur eget nisl a risus suscipit auctor. Ut a ultrices lectus. Pellentesque sed justo magna. Donec congue mi id odio sodales tincidunt. Integer cursus leo in libero imperdiet ac pharetra tortor venenatis. In nec purus at justo adipiscing condimentum.</p>', 1, 2, '2013-05-12 03:44:51', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-05-12 03:44:51', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 1, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(29, 68, 'Sample Content 3', 'sample-content-3', '<p>Integer varius tempor auctor. Etiam fringilla venenatis mollis. Duis ullamcorper massa eu sapien fringilla consequat. Aliquam nec ligula mi, quis tincidunt odio. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum rutrum dui eros. Praesent nec nibh diam. <br /><br /> Proin augue risus, molestie at dictum quis, viverra non ipsum. Praesent lacus dui, euismod ut mollis vel, lacinia vel arcu. In commodo arcu vitae velit elementum a luctus nibh mattis. Ut vel turpis eros. Nam mattis velit sed nibh scelerisque in bibendum felis sodales. Vivamus ornare pellentesque pellentesque. Nullam dignissim semper quam nec mollis. In vel lacus lectus, ac tristique leo. <br /><br /> Aenean venenatis egestas iaculis. Nullam consectetur condimentum dolor at bibendum. Nulla in enim quis ipsum pulvinar imperdiet vitae nec velit. Donec non urna quam. Aliquam congue magna nec risus iaculis posuere. Vivamus bibendum interdum molestie. Sed libero risus, varius eu dictum ac, pharetra ac elit. Curabitur a nibh id ipsum sagittis blandit. Morbi cursus commodo leo quis rhoncus. In nec purus magna, id porta enim. Donec pulvinar aliquet vulputate. Donec sit amet justo sit amet ipsum posuere imperdiet id sed magna.</p>\r\n', '\r\n<p><br /><br /> Sed ultricies condimentum augue, sed congue erat dapibus nec. Etiam quis tempor nibh. Vestibulum urna dui, sodales ut ornare vitae, porta non augue. Curabitur nunc ipsum, facilisis nec luctus blandit, luctus at sem. Vestibulum pharetra augue vitae orci mollis scelerisque. Quisque gravida diam vel mauris ultricies ac faucibus turpis volutpat. Maecenas augue elit, rutrum pharetra mattis ullamcorper, commodo varius enim. Phasellus dapibus mattis ipsum sit amet tincidunt. In hac habitasse platea dictumst. Sed nisi dolor, ullamcorper eget pretium molestie, commodo non dolor. Praesent volutpat aliquet neque ut bibendum. Pellentesque tincidunt tortor non ipsum venenatis ultricies. <br /><br /> Proin at justo eu orci condimentum ornare sed pellentesque orci. Duis nec interdum massa. Proin porta semper enim, sit amet condimentum lorem interdum non. Etiam vel sodales tellus. Nullam vulputate luctus sapien, in feugiat arcu pulvinar vitae. Phasellus risus orci, posuere non fringilla nec, placerat sit amet mauris. Mauris a sem turpis, vitae hendrerit risus. Mauris suscipit, purus ac ornare elementum, enim dolor convallis mauris, sit amet convallis urna metus at leo. Curabitur eget nisl a risus suscipit auctor. Ut a ultrices lectus. Pellentesque sed justo magna. Donec congue mi id odio sodales tincidunt. Integer cursus leo in libero imperdiet ac pharetra tortor venenatis. In nec purus at justo adipiscing condimentum.</p>', 1, 2, '2013-05-12 03:45:10', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-05-12 03:45:10', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 0, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(30, 69, 'Search Engine Optimized ', 'search-engine-optimized', '<br/>\r\n<h3>SEO - Get your site noticed!</h3>\r\n<p>Not only is this template beautifully designed but it is great for search engine optimization as well! What is SEO? It is simple the act of altering a web site so that it does well in the organic, crawler-based listings of search engines such as google.com. How does this template accomplish this? It&#39;s simple, the majority of your most valuable content is found in the main body of your site, through css we are able to alter the layout of the site and call the main content before the left and right columns are called. This allows for your content to be found first by search engines before it reaches your other content, which is vital in search engine optimization. This is a common feature this can be done with almost all of Shape 5 templates as well.</p>\r\n<p> </p>\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top">JOIN TODAY</a>.</strong></div>', '', 1, 10, '2013-05-12 03:45:28', 791, '', '2013-06-11 15:52:09', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:45:28', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 9, '', '', 1, 11, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(31, 70, 'Site Shapers', 'site-shapers', '<p><br />So what are Site Shapers? They are quick installs of Joomla combined with all the modules, content, etc used on our demo, excluding stock photography. Within a few minutes you can have your site up, running and looking just like our demo. No more importing SQL dumps and installing modules.  Just head on over to the download section of this template and grab a Site Shaper.  Simply install the Site Shaper like any other Joomla installation, it&#39;s that easy!<br /><br /></p>\r\n\r\n\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nPlease also visit our tutorials download page for a more in depth site shaper tutorial.</a>\r\n</div>\r\n\r\n<br /><br />\r\n\r\n<h3>How to setup a Site Shaper</h3>\r\n\r\n\r\n<ul class="ul_bullet">\r\n<li>Login to your cpanel or your server admin panel.</li>\r\n<li>Locate the area where your databases are    (usually labeled Mysql Databases)</li>\r\n<li>Create a new database</li>\r\n<li>Next create a new database user and assign    it to this newly created database in the previous step</li>\r\n<li>You will then    need to extract the site shaper to either a folder on your server or the root    directory such as WWW. NOTE: if you already have a website in the root of your    WWW folder, we suggest creating a new folder and extract the site shaper    there. If your cpanel does not have an extract option or you cannot find it,    you may also extract the contents of your site shaper in to a folder on your    desktop and upload all the files via an ftp client to your server.</li>\r\n<li>Next, navigate to the url where you extracted the site shaper via your web browser.</li>\r\n<li>Continue through each screen until you reach the below screenshot:</li>\r\n<br /> <img src="http://www.shape5.com/demo/images/general/siteshaper.png" border="0" /> <br /><br />\r\n<li>At the above screen be sure to enter localhost as shown, continue to fill in the following text fields with your newly created database and username information</li>\r\n<li>Follow through the rest of the site shaper setup and click the install sample data at the last screen and the installation is complete! (be sure to rename/remove the installation directory after finishing the install)</li>\r\n</ul>\r\n<p><br /><br /></p>', '', 1, 10, '2013-05-12 03:45:45', 791, '', '2013-06-11 17:28:07', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:45:45', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 8, '', '', 1, 14, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(32, 71, 'Stock Photography', 'stock-photography', 'All content and images shown on this site is for demo, presentation purposes only. This site is intended to exemplify a live website and does not make any claim of any kind to the validity of non-Shape5 content, images or posts published. Stock photography and icons were purchased from <a href="http://www.shutterstock.com" target="_blank">shutterstock.com</a> for this demo only, and is not included with this template. You may not use these items for your own use without first purchasing them from their copyright owner. Links will be provided for some images if you wish to purchase them from their copyright owners. ', '', 1, 10, '2013-05-12 03:46:04', 791, '', '2013-06-11 17:27:44', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:46:04', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 7, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(33, 72, 'Template Specific Options', 'template-specific-options', 'This template is built on the very powerful S5 Vertex Framework, which comes packed with amazing features! <a href="http://www.shape5.com/joomla/framework/vertex_framework.html" target="blank">Learn More About Vertex...</a> \r\n<br /><br />\r\nEvery template built on Vertex also comes with its own unique template specific options applicable to that particular template such as highlight colors, social icons, and much more. These features are in addition to the standard functions of Vertex, and are still controlled through the very user friendly interface of Vertex. This page will document the features specific to this template.\r\n\r\n<br /><br />\r\n<h3>Template Specific Configuration Interface of Vertex</h3>\r\nBelow is a screenshot that shows all the template specific features available in the user friendly Vertex admin:<br /><br />\r\n<img class="padded" style="margin:0px" src="images/template_specific.png"></img>\r\n\r\n\r\n\r\n<br /><br />\r\n<h3>Custom Highlight Colors</h3>\r\nSet your own custom color scheme with the built in highlight colors. There are two built in highlight colors that control menu items, titles, buttons, hover over effects, and much more. You can set these colors to any color that you want.\r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/example1.jpg"></img>\r\n\r\n\r\n\r\n<br /><br />\r\n<h3>Top Row1 Inset Shadow</h3>\r\nThe top_row1 area of this template was designed to have a background image, which is controlled under the Backgrounds tab of Vertex. In addition to that background you set an inset shadow to cover the background image. You can set the size and opacity of this shadow to any size you wish.\r\n\r\n\r\n\r\n<br /><br />\r\n<h3>Social Icons</h3>\r\nEasily link to a social media site with the built in social icons found in the header of this template. Simply enter the url of your social site in the configuration and the icon will automatically appear. To disable an icon simply leave the url blank for that particular icon. \r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/social.jpg"></img>\r\n\r\n\r\n<br /><br />\r\n<h3>Uppercase Leters</h3>\r\nChoose to enable or disable uppercase letters on most menu items, buttons, titles and more.\r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/uppercase1.jpg"></img>\r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/uppercase2.jpg"></img>\r\n\r\n\r\n\r\n<br /><br />\r\n<h3>Small Menu</h3>\r\nThe S5 Flex Menu gives you the ability to have subtext on each menu item. If you choose not to use subtext on the first level links simply choose to disable this option and the menu will automatically down size.\r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/small_menu1.jpg"></img><br />\r\n<br />\r\n<img class="padded" style="margin:0px" src="images/small_menu2.jpg"></img>\r\n\r\n\r\n<br /><br />\r\n<h3>Custom Header Background</h3>\r\nAt the bottom of the Template Specific tab you will see a separate section dedicated to custom header background options. These options allow you to specify the color or background image of the header. In the image below the header is shown with a repeating static background image. The default background is white, but you can override this with your own background settings as needed. Be sure to read the titles and tooltips of each feature to see what each does.\r\n<br /><br />\r\n<img class="padded" style="margin:0px" src="images/header.jpg"></img><br />\r\n\r\n\r\n\r\n\r\n<br /><br />\r\n\r\n', '', 1, 10, '2013-05-12 03:46:20', 791, '', '2013-11-26 16:25:01', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:46:20', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 46, 6, '', '', 1, 129, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(35, 74, 'The Template''s Settings', 'the-template-s-settings', '<p>This template comes loaded with options that you can use to customize your site exactly how you want it. Here&#39;s how to get to these custom settings:</p>\r\n<ol>\r\n<li>In the backend of Joomla go menu item Extensions/Template Manager.</li>\r\n<li>Click on the title of the template.</li>\r\n<li>This will bring you to the template manager screen where you can edit the template&#39;s parameters.</li>\r\n<li>Click save when you are done</li>\r\n</ol>\r\n<p><img src="http://www.shape5.com/demo/images/general/template_edit15.jpg" border="0" alt=" " width="500" height="156" /> <br /><br /><br /></p>\r\n<div class="blue_box"><strong>I like what I see! I want to <a href="http://www.shape5.com/join-now.html" target="_top" style="color:#1B6FC2; text-decoration:underline">JOIN TODAY</a>.</strong></div>\r\n<p> </p>', '', 1, 10, '2013-05-12 03:47:23', 791, '', '2013-06-11 17:32:48', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:47:23', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 5, '', '', 1, 22, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(36, 75, 'Tool Tips', 'tool-tips', '<br /><div class="blue_box"><strong>Note - The tool tips script is by default disabled. If you wish to use it you must enable this script in the template&#39;s configuration area. This also includes site shaper installations.</strong></div>\r\n<br/>\r\n<div class="blue_box">\r\n<a style="text-decoration:underline" target="_blank" href="http://www.shape5.com/joomla_tutorials.html">\r\nFor a tutorial on how to setup this script be sure to view the Vertex Guide on our Tutorials page.</a>\r\n</div>\r\n\r\n<br/>\r\n<br/>\r\n<strong>\r\nDemo 1:\r\n</strong>\r\n<br />\r\n\r\n\r\n\r\n\r\n<a onmouseover="Tip(&#39;This is a sample tooltip.&#39;, WIDTH, 140, OPACITY, 80, ABOVE, true, OFFSETX, 1, FADEIN, 200, FADEOUT, 300,SHADOW, true, SHADOWCOLOR, &#39;#000000&#39;,SHADOWWIDTH, 2, BGCOLOR, &#39;#000000&#39;,BORDERCOLOR, &#39;#000000&#39;,FONTCOLOR, &#39;#FFFFFF&#39;, PADDING, 9)" href="http://www.shape5.com/demo/etensity/">\r\n\r\n<img class="boxed2" alt="" src="http://www.shape5.com/demo/smart_blogger/images/tooltip.jpg"/>\r\n\r\n</a>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<br/><br />\r\n<strong>\r\nDemo 2:\r\n</strong>\r\n<br />\r\n\r\n\r\n\r\n<a href="index.htm" onmouseover="Tip(&#39;Image Demo<br /> <br /><img src=http://www.shape5.com/demo/smart_blogger/images/tooltip.jpg width=220 height=147>&#39;)"><strong>Demo 2 Image Tool Tip</strong></a><br/><br/> \r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n<strong>\r\nDemo 3:\r\n</strong>\r\n<br />\r\n\r\n\r\n\r\n\r\n<a href="#" onmouseover="Tip(&#39;Image Demo<br /> <br /><img src=http://www.shape5.com/demo/smart_blogger/images/tooltip.jpg width=220 height=147>&#39;,SHADOW, true,  BGCOLOR, &#39;#000000&#39;, FADEIN, 400, FADEOUT, 400, SHADOWCOLOR, &#39;#000000&#39;, BORDERCOLOR, &#39;#000000&#39;, OPACITY, 90,FONTCOLOR, &#39;#FFFFFF&#39;)"><strong>Demo 3 Image Tool Tip</strong></a>\r\n<br/><br/>', '', 1, 10, '2013-05-12 03:47:41', 791, '', '2013-06-11 15:47:21', 516, 0, '0000-00-00 00:00:00', '2013-05-12 03:47:41', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":null,"urlatext":"","targeta":"","urlb":null,"urlbtext":"","targetb":"","urlc":null,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 4, '', '', 1, 18, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');
INSERT INTO `#__content` (`id`, `asset_id`, `title`, `alias`, `introtext`, `fulltext`, `state`, `catid`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `images`, `urls`, `attribs`, `version`, `ordering`, `metakey`, `metadesc`, `access`, `hits`, `metadata`, `featured`, `language`, `xreference`) VALUES
(37, 76, 'Typography Options', 'typography-options', '<blockquote>\r\n<p>This is a sample blockquote. Use <strong>&lt;blockquote&gt;&lt;p&gt;Your content goes here!&lt;/p&gt;&lt;/blockquote&gt;</strong> to create a blockquote.</p>\r\n</blockquote>\r\n<div class="s5_greenbox">\r\n<div class="point">&lt;div class="s5_greenbox"&gt; &lt;div class="point"&gt; Your text here &lt;/div&gt; &lt;/div&gt;</div>\r\n</div>\r\n<p> </p>\r\n<div class="s5_redbox">\r\n<div class="point">&lt;div class="s5_redbox"&gt; &lt;div class="point"&gt; Your text here &lt;/div&gt; &lt;/div&gt;</div>\r\n</div>\r\n<p> </p>\r\n<div class="s5_bluebox">\r\n<div class="point">&lt;div class="s5_bluebox"&gt; &lt;div class="point"&gt; Your text here &lt;/div&gt; &lt;/div&gt;</div>\r\n</div>\r\n<p> </p>\r\n<div class="s5_graybox">\r\n<div class="point">&lt;div class="s5_graybox"&gt; &lt;div class="point"&gt; Your text here &lt;/div&gt; &lt;/div&gt;</div>\r\n</div>\r\n<p> </p>\r\n<div class="black_box">This is a styled box. Use <strong>&lt;div class="black_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="gray_box">This is a styled box. Use <strong>&lt;div class="gray_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="red_box">This is a styled box. Use <strong>&lt;div class="red_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="blue_box">This is a styled box. Use <strong>&lt;div class="blue_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="green_box">This is a styled box. Use <strong>&lt;div class="green_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="yellow_box">This is a styled box. Use <strong>&lt;div class="yellow_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p> </p>\r\n<div class="orange_box">This is a styled box. Use <strong>&lt;div class="orange_box"&gt;Your content goes here!&lt;/div&gt;</strong></div>\r\n<p><br /> This is an image with the "boxed" class applied:<br /><br /> <img class="boxed" src="http://www.shape5.com/demo/images/small1.jpg" alt="" /> <br /><br /><br /><br /> <br /> This is an image with the "boxed_black" class applied:<br /><br /> <img class="boxed_black" src="http://www.shape5.com/demo/images/small1.jpg" alt="" /> <br /><br /> This is an image with the "padded" class applied:<br /><br /> <img class="padded" src="http://www.shape5.com/demo/images/small1.jpg" alt="" /> <br /><br /> This is an image with the "full_width" class applied. This can also be done automatically in the template configuration. Be sure to review our Vertex Guide <a href="http://www.shape5.com/joomla_tutorials.html" target="_blank">here</a> for other full width image options.<br /><br /> <img class="full_width" src="http://www.shape5.com/demo/images/general/typography_full_width.jpg" alt="" /> <br /><br /></p>\r\n<h1>Heading 1</h1>\r\n<h2>Heading 2</h2>\r\n<h3>Heading 3</h3>\r\n<h4>Heading 4</h4>\r\n<h5>Heading 5</h5>\r\n\r\n<br /> &lt;a&gt; With readon_highlight1 class applied:<br /><br /> <a class="readon_highlight1" href="">readon_highlight1</a><br /> <br />\r\n\r\n&lt;a&gt; With readon_highlight2 class applied:<br /><br /> <a class="readon_highlight2" href="">readon_highlight2</a><br /> <br />\r\n\r\n&lt;a&gt; With highlight1_button class applied:<br /><br /> <a class="highlight1_button" href="">highlight1_button</a><br /> <br />\r\n\r\n&lt;a&gt; With highlight2_button class applied:<br /><br /> <a class="highlight2_button" href="">highlight2_button</a><br /> <br />\r\n\r\n&lt;span&gt; With highlight1_text class applied:<br /><br /> <span class="highlight1_text">highlight1_text</span><br /> <br />\r\n\r\n&lt;span&gt; With highlight2_text class applied:<br /><br /> <span class="highlight2_text">highlight2_text</span><br /> <br />\r\n\r\n&lt;span&gt; With uppercase_letters class applied:<br /><br /> <span class="uppercase_letters">uppercase_letters</span><br /> <br />\r\n\r\n\r\n\r\n<div class="code">This is a sample code div. Use <strong>&lt;div class="code"&gt;Your content goes here!&lt;/div&gt;</strong> to create a code div.<br /><br />#s5_code { width: 30px; color: #fff; line-height: 45px; }</div>\r\n<p> </p>\r\n<ol>\r\n<li>This is an <strong>Ordered List</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ol>\r\n<p> </p>\r\n<ul>\r\n<li>This is an <strong>Unordered List</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ul>\r\n<p> </p>\r\n<ul class="plus">\r\n<li>This is an <strong>Unordered List with class plus</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ul>\r\n<p> </p>\r\n<ul class="ul_arrow">\r\n<li>This is an <strong>Unordered List with class ul_arrow</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ul>\r\n<p> </p>\r\n<ul class="ul_star">\r\n<li>This is an <strong>Unordered List with class ul_star</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ul>\r\n<p> </p>\r\n<ul class="ul_bullet">\r\n<li>This is an <strong>Unordered List with class ul_bullet</strong></li>\r\n<li>Congue Quisque augue elit dolor nibh.</li>\r\n<li>Condimentum elte quis.</li>\r\n<li>Opsum dolor sit amet consectetuer.</li>\r\n</ul>\r\n<p><br /> The following list will support lists up to number 9, add the following class to the UL wrapping the below LI elements, class="ul_numbers": <br /><br /></p>\r\n<ul class="ul_numbers">\r\n<li class="li_number1">This is a sample styled number list <strong>&lt;li class="li_number1"&gt;Your content goes here!&lt;/li&gt;</strong></li>\r\n<li class="li_number2">This is a sample styled number list <strong>&lt;li class="li_number2"&gt;Your content goes here!&lt;/li&gt;</strong></li>\r\n<li class="li_number3">This is a sample styled number list <strong>&lt;li class="li_number3"&gt;Your content goes here!&lt;/li&gt;</strong></li>\r\n<li class="li_number4">This is a sample styled number list <strong>&lt;li class="li_number4"&gt;Your content goes here!&lt;/li&gt;</strong></li>\r\n</ul>\r\n\r\n\r\n\r\n\r\n<div class="s5_pricetable_3">\r\n\r\n<div class="s5_pricetable_column">\r\n   <div class="s5_pricetable_column_padding">\r\n     <div class="s5_title">Basic</div>\r\n\r\n    <span class="dollarsign">$</span><span class="price">49</span><span class="month">/per month</span>\r\n\r\n   <div class="s5_options">\r\n    Unlimited Space &amp; Traffic<br>\r\n    99.9% Server Uptime<br>\r\n    24/7 Customer Care<br>\r\n    30 Days Money Back<br><br>\r\n   </div>\r\n\r\n   <div class="s5_horizontalrule"></div>\r\n   <div class="s5_buttoncenter"><a href="#" class="button s5_pricetable">Choose</a></div>\r\n\r\n   </div>\r\n</div>\r\n\r\n<div class="s5_pricetable_column recommended">\r\n   <div class="s5_pricetable_column_padding">\r\n      <div class="s5_title">Standard</div> \r\n\r\n    <span class="dollarsign">$</span><span class="price">79</span><span class="month">/per month</span>\r\n\r\n   <div class="s5_options">\r\n    Unlimited Space &amp; Traffic<br>\r\n    99.9% Server Uptime<br>\r\n    24/7 Customer Care<br>\r\n    30 Days Money Back<br>\r\n  FREE Domain Name<br>\r\n   Personal Concierge\r\n   </div>\r\n\r\n   <div class="s5_horizontalrule"></div>\r\n   <div class="s5_buttoncenter"><a href="#" class="button s5_pricetable">Choose</a></div>\r\n\r\n   </div>\r\n</div>\r\n\r\n<div class="s5_pricetable_column">\r\n   <div class="s5_pricetable_column_padding">\r\n     <div class="s5_title">Premium</div>\r\n\r\n     <span class="dollarsign">$</span><span class="price">99</span><span class="month">/per month</span>\r\n\r\n   <div class="s5_options">\r\n   Unlimited Space &amp; Traffic <br>\r\n   99.9% Server Uptime<br>\r\n   24/7 Customer Care<br>\r\n   30 Days Money Back<br>\r\n   FREE Domain Name\r\n\r\n   </div>\r\n\r\n   <div class="s5_horizontalrule"></div>\r\n   <div class="s5_buttoncenter"><a href="#" class="button s5_pricetable">Choose</a></div>\r\n\r\n\r\n   </div>\r\n</div>\r\n\r\n<div style="clear:both;"></div>\r\n</div>\r\n\r\n<br />\r\n\r\n<div class="code">\r\n\r\nTo use the price table on your site grab the following example code below and add to your site.  The price table is fully responsive and can display up to 7 price columns. Once you have determined the number of columns you will be using set the wrapper div to the number of columns that you''ve added. You can use the wrapping classes of "s5_pricetable_1" to "s5_pricetable_7". <br><br><br><br>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n&lt;div class="s5_pricetable_3"&gt;<br>\r\n\r\n&lt;div class="s5_pricetable_column"&gt;<br>\r\n   &lt;div class="s5_pricetable_column_padding"&gt;<br>\r\n     &lt;div class="s5_title"&gt;Basic&lt;/div&gt;<br><br>\r\n\r\n    &lt;span class="dollarsign"&gt;$&lt;/span&gt;&lt;span class="price"&gt;49&lt;/span&gt;&lt;span class="month"&gt;/per month&lt;/span&gt;<br><br>\r\n\r\n   &lt;div class="s5_options"&gt;<br>\r\n    Unlimited Space &amp; Traffic&lt;br&gt;<br>\r\n    99.9% Server Uptime&lt;br&gt;<br>\r\n    24/7 Customer Care&lt;br&gt;<br>\r\n    30 Days Money Back&lt;br&gt;&lt;br&gt;<br>\r\n   &lt;/div&gt;<br><br>\r\n\r\n   &lt;div class="s5_horizontalrule"&gt;&lt;/div&gt;<br>\r\n   &lt;div class="s5_buttoncenter"&gt;&lt;a class="button s5_pricetable" href="#"&gt;Choose&lt;/a&gt;&lt;/div&gt;<br><br>\r\n\r\n   &lt;/div&gt;<br>\r\n&lt;/div&gt;<br><br>\r\n\r\n&lt;div class="s5_pricetable_column recommended"&gt;<br>\r\n   &lt;div class="s5_pricetable_column_padding"&gt;<br>\r\n      &lt;div class="s5_title"&gt;Standard&lt;/div&gt; <br><br>\r\n\r\n    &lt;span class="dollarsign"&gt;$&lt;/span&gt;&lt;span class="price"&gt;79&lt;/span&gt;&lt;span class="month"&gt;/per month&lt;/span&gt;<br><br>\r\n\r\n   &lt;div class="s5_options"&gt;<br>\r\n    Unlimited Space &amp; Traffic&lt;br&gt;<br>\r\n    99.9% Server Uptime&lt;br&gt;<br>\r\n    24/7 Customer Care&lt;br&gt;<br>\r\n    30 Days Money Back&lt;br&gt;<br>\r\n  FREE Domain Name&lt;br&gt;<br>\r\n   Personal Concierge<br>\r\n   &lt;/div&gt;<br><br>\r\n\r\n   &lt;div class="s5_horizontalrule"&gt;&lt;/div&gt;<br>\r\n   &lt;div class="s5_buttoncenter"&gt;&lt;a class="button s5_pricetable" href="#"&gt;Choose&lt;/a&gt;&lt;/div&gt;<br><br>\r\n\r\n   &lt;/div&gt;<br>\r\n&lt;/div&gt;<br><br>\r\n\r\n&lt;div class="s5_pricetable_column"&gt;<br>\r\n   &lt;div class="s5_pricetable_column_padding"&gt;<br>\r\n     &lt;div class="s5_title"&gt;Premium&lt;/div&gt;<br><br>\r\n\r\n     &lt;span class="dollarsign"&gt;$&lt;/span&gt;&lt;span class="price"&gt;99&lt;/span&gt;&lt;span class="month"&gt;/per month&lt;/span&gt;<br><br>\r\n\r\n   &lt;div class="s5_options"&gt;<br>\r\n   Unlimited Space &amp; Traffic &lt;br&gt;<br>\r\n   99.9% Server Uptime&lt;br&gt;<br>\r\n   24/7 Customer Care&lt;br&gt;<br>\r\n   30 Days Money Back&lt;br&gt;<br>\r\n   FREE Domain Name<br><br>\r\n\r\n   &lt;/div&gt;<br><br>\r\n\r\n   &lt;div class="s5_horizontalrule"&gt;&lt;/div&gt;<br>\r\n   &lt;div class="s5_buttoncenter"&gt;&lt;a class="button s5_pricetable" href="#"&gt;Choose&lt;/a&gt;&lt;/div&gt;<br><br>\r\n\r\n\r\n   &lt;/div&gt;<br>\r\n&lt;/div&gt;<br><br>\r\n\r\n&lt;div style="clear:both;"&gt;&lt;/div&gt;<br>\r\n&lt;/div&gt;<br><br>\r\n\r\n\r\n\r\n\r\n\r\n\r\n\r\n</div>\r\n\r\n\r\n<br><br>\r\n\r\n\r\n\r\nMake any Youtube, Vimeo, etc video you embed to your site work with responsive by wrapping with a DIV with a class of "s5_video_container".  The below Youtube Video will shrink when the area its contained in gets too small for it:\r\n\r\n<br><br>\r\n<div class="s5_video_container">\r\n<iframe width="560" height="315" src="//www.youtube.com/embed/V-wmpoIN2gs?wmode=opaque" frameborder="0" allowfullscreen></iframe>\r\n</div>\r\n\r\n\r\n<br /><br />\r\n\r\nUse the icons below in conjunction with the class icon_circle:<br /><br />\r\n\r\n<i class="icon-cogs icon_circle" style="float:left"></i><br /><br /><br /><br />\r\n\r\n&lt;i class=&quot;icon-cogs icon_circle&quot; style=&quot;float:left&quot;&gt;&lt;/i&gt;\r\n\r\n<br /><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-adjust"></i>icon-adjust</span><br />\r\n<span><i style="margin-right:8px" class="icon-anchor"></i>icon-anchor</span><br />\r\n<span><i style="margin-right:8px" class="icon-archive"></i>icon-archive</span><br />\r\n<span><i style="margin-right:8px" class="icon-asterisk"></i>icon-asterisk</span><br />\r\n<span><i style="margin-right:8px" class="icon-barcode"></i>icon-barcode</span><br />\r\n<span><i style="margin-right:8px" class="icon-beer"></i>icon-beer</span><br />\r\n<span><i style="margin-right:8px" class="icon-bell"></i>icon-bell</span><br />\r\n<span><i style="margin-right:8px" class="icon-bolt"></i>icon-bolt</span><br />\r\n<span><i style="margin-right:8px" class="icon-book"></i>icon-book</span><br />\r\n<span><i style="margin-right:8px" class="icon-bookmark"></i>icon-bookmark</span><br />\r\n<span><i style="margin-right:8px" class="icon-briefcase"></i>icon-briefcase</span><br />\r\n<span><i style="margin-right:8px" class="icon-bug"></i>icon-bug</span><br />\r\n<span><i style="margin-right:8px" class="icon-bullhorn"></i>icon-bullhorn</span><br />\r\n<span><i style="margin-right:8px" class="icon-bullseye"></i>icon-bullseye</span><br />\r\n<span><i style="margin-right:8px" class="icon-calendar"></i>icon-calendar</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-bullhorn"></i>icon-bullhorn</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-bullseye"></i>icon-bullseye</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-calendar"></i>icon-calendar</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-camera"></i>icon-camera</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-camera-retro"></i>icon-camera-retro</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-certificate"></i>icon-certificate</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-check"></i>icon-check</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-circle"></i>icon-circle</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-cloud"></i>icon-cloud</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-cloud-download"></i>icon-cloud-download</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-cloud-upload"></i>icon-cloud-upload</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-code"></i>icon-code</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-code-fork"></i>icon-code-fork</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-coffee"></i>icon-coffee</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-cog"></i>icon-cog</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-cogs"></i>icon-cogs</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-comment"></i>icon-comment</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-comments"></i>icon-comments</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-compass"></i>icon-compass</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-credit-card"></i>icon-credit-card</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-crop"></i>icon-crop</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-dashboard"></i>icon-dashboard</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-desktop"></i>icon-desktop</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-download"></i>icon-download</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-edit"></i>icon-edit</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-envelope"></i>icon-envelope</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-eraser"></i>icon-eraser</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-exchange"></i>icon-exchange</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-exclamation"></i>icon-exclamation</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-external-link"></i>icon-external-link</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-female"></i>icon-female</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-fighter-jet"></i>icon-fighter-jet</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-film"></i>icon-film</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-filter"></i>icon-filter</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-fire"></i>icon-fire</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-fire-extinguisher"></i>icon-fire-extinguisher</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-flag"></i>icon-flag</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-flag-checkered"></i>icon-flag-checkered</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-folder-open"></i>icon-folder-open</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-gamepad"></i>icon-gamepad</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-gear"></i>icon-gear</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-gears"></i>icon-gears</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-gift"></i>icon-gift</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-glass"></i>icon-glass</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-globe"></i>icon-globe</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-group"></i>icon-group</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-headphones"></i>icon-headphones</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-heart"></i>icon-heart</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-home"></i>icon-home</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-inbox"></i>icon-inbox</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-info"></i>icon-info</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-key"></i>icon-key</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-laptop"></i>icon-laptop</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-leaf"></i>icon-leaf</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-legal"></i>icon-legal</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-level-down"></i>icon-level-down</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-level-up"></i>icon-level-up</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-location-arrow"></i>icon-location-arrow</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-lock"></i>icon-lock</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-magic"></i>icon-magic</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-magnet"></i>icon-magnet</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-mail-forward"></i>icon-mail-forward</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-mail-reply"></i>icon-mail-reply</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-mail-reply-all"></i>icon-mail-reply-all</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-male"></i>icon-male</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-map-marker"></i>icon-map-marker</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-microphone"></i>icon-microphone</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-minus"></i>icon-minus</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-mobile-phone"></i>icon-mobile-phone</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-money"></i>icon-money</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-music"></i>icon-music</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-pencil"></i>icon-pencil</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-phone"></i>icon-phone</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-plane"></i>icon-plane</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-power-off"></i>icon-power-off</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-print"></i>icon-print</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-puzzle-piece"></i>icon-puzzle-piece</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-qrcode"></i>icon-qrcode</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-question"></i>icon-question</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-quote-left"></i>icon-quote-left</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-quote-right"></i>icon-quote-right</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-random"></i>icon-random</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-refresh"></i>icon-refresh</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-reply"></i>icon-reply</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-reply-all"></i>icon-reply-all</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-retweet"></i>icon-retweet</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-road"></i>icon-road</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-rocket"></i>icon-rocket</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-search"></i>icon-search</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-share"></i>icon-share</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-shield"></i>icon-shield</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-shopping-cart"></i>icon-shopping-cart</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-signal"></i>icon-signal</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-sitemap"></i>icon-sitemap</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-sort"></i>icon-sort</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-sort-up"></i>icon-sort-up</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-spinner"></i>icon-spinner</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-star"></i>icon-star</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-star-half"></i>icon-star-half</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-star-half-empty"></i>icon-star-half-empty</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-star-half-full"></i>icon-star-half-full</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-subscript"></i>icon-subscript</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-suitcase"></i>icon-suitcase</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-superscript"></i>icon-superscript</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-tablet"></i>icon-tablet</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-tag"></i>icon-tag</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-tags"></i>icon-tags</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-tasks"></i>icon-tasks</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-terminal"></i>icon-terminal</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-thumbs-down"></i>icon-thumbs-down</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-thumbs-up"></i>icon-thumbs-up</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-ticket"></i>icon-ticket</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-trophy"></i>icon-trophy</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-truck"></i>icon-truck</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-umbrella"></i>icon-umbrella</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-unlock"></i>icon-unlock</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-unlock-alt"></i>icon-unlock-alt</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-upload"></i>icon-upload</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-user"></i>icon-user</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-volume-down"></i>icon-volume-down</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-volume-off"></i>icon-volume-off</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-volume-up"></i>icon-volume-up</span><br />\r\n\r\n<span><i style="margin-right:8px" class="icon-wrench"></i>icon-wrench</span><br />\r\n', '', 1, 10, '2013-05-12 03:48:00', 791, '', '2013-11-26 00:16:17', 791, 0, '0000-00-00 00:00:00', '2013-05-12 03:48:00', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 36, 3, '', '', 1, 55, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(42, 91, 'Shape5 Vertex Featured Article', 'homepage-article-4', '<img src="images/s5_vertex.png" alt="" class="featured_image" />\r\n\r\n<h2>Responsive Layout</h2>\r\nAt Shape5 we make responsive sites very easy to use and understand. There are many great features to allow you to setup your site just the way you want across multiple platforms.\r\n<br /><br />\r\n<h2>Row Distribution</h2>\r\nSometimes it''s hard to make all your content fit into a single row on narrow screens, thanks to Vertex you can distribute rows of modules to whatever configuration you would like.\r\n\r\n<p class="readmore"><a href="index.php/features-mainmenu-47/template-features/responsive-layout">Read more</a></p>\r\n\r\n', '', 1, 8, '2013-06-10 03:47:00', 791, '', '2013-11-26 15:45:10', 791, 0, '0000-00-00 00:00:00', '2013-06-10 03:47:00', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"0","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 50, 0, '', '', 1, 7, '{"robots":"","author":"","rights":"","xreference":""}', 1, '*', ''),
(44, 93, 'Floating Menu', 'floating-menu', 'The floating menu feature is a great way for your users to easily navigate your website! The floating menu will show at the top of your browser once the screen reaches a certain point. You can determine the exact point at which this occurs via the template specific area of Vertex. NOTE: This is a Vertex addition and is not guaranteed to work with all Vertex Templates.  Some custom CSS may need to be adjusted per template. This feature is not supported by IE7/8.\r\n<br><br>\r\n<h3>Features at a glance:</h3>\r\n<br>\r\n<ul class="ul_star">\r\n<li>Set a backgroud image to the menu, gradient or solid color</li>\r\n<li>Set to snap or smooth scroll in</li>\r\n<li>Determine at which point as you scroll down your page that the menu drops in</li>\r\n<li>and many more features, just check out the screenshot below</li>\r\n</ul>\r\n<br>\r\n<br>\r\nAdmin area of the Floating Menu:\r\n<br>\r\n<br>\r\n<img alt="parallax menu admin" src="http://www.shape5.com/demo/images/general/floatingmenu_admin.jpg">', '', 1, 10, '2013-11-25 21:08:30', 791, '', '0000-00-00 00:00:00', 0, 0, '0000-00-00 00:00:00', '2013-11-25 21:08:30', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 1, 2, '', '', 1, 0, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', ''),
(45, 94, 'Parallax Backgrounds', 'parallax-backgrounds', 'Parallax backgrounds as well as other background options are built directly into the Vertex Framework.  Parallax causes the background image of an element to scroll at a different speed than your browser. For an example of this view the homepage of the Velocity template <a href="http://www.shape5.com/demo/velocity/" target="_blank">here</a> and watch the background of the top_row1 area as you scroll down the page. This feature is found under "Backgrounds" tab in Vetex, where you can control the background for many areas of your website. \r\n<br><br>\r\n\r\n<h3>Features:</h3><br>\r\n<ul class="ul_star">\r\n<li>Set the scroll speed of the images</li>\r\n<li>Set background repeat style</li>\r\n<li>Set background image size, 100%, cover, contain, etc</li>\r\n<li>Set custom backgrounds for all s5 rows in the framework.</li>\r\n<li>Enable or disable parallax on a per row basis</li>\r\n</ul>\r\n\r\n<br><br>\r\nAdmin Area Of The Backgrounds Tab:<br><br>\r\n<img src="http://www.shape5.com/demo/images/general/parallax_admin.jpg" alt="parallax menu admin"/>', '', 1, 10, '2013-11-25 21:09:05', 791, '', '2013-11-25 21:12:09', 791, 0, '0000-00-00 00:00:00', '2013-11-25 21:09:05', '0000-00-00 00:00:00', '{"image_intro":"","float_intro":"","image_intro_alt":"","image_intro_caption":"","image_fulltext":"","float_fulltext":"","image_fulltext_alt":"","image_fulltext_caption":""}', '{"urla":false,"urlatext":"","targeta":"","urlb":false,"urlbtext":"","targetb":"","urlc":false,"urlctext":"","targetc":""}', '{"show_title":"","link_titles":"","show_tags":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_vote":"","show_hits":"","show_noauth":"","urls_position":"","alternative_readmore":"","article_layout":"","show_publishing_options":"","show_article_options":"","show_urls_images_backend":"","show_urls_images_frontend":""}', 2, 1, '', '', 1, 8, '{"robots":"","author":"","rights":"","xreference":""}', 0, '*', '');

-- --------------------------------------------------------

--
-- Table structure for table `#__contentitem_tag_map`
--

DROP TABLE IF EXISTS `#__contentitem_tag_map`;
CREATE TABLE IF NOT EXISTS `#__contentitem_tag_map` (
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `core_content_id` int(10) unsigned NOT NULL COMMENT 'PK from the core content table',
  `content_item_id` int(11) NOT NULL COMMENT 'PK from the content type table',
  `tag_id` int(10) unsigned NOT NULL COMMENT 'PK from the tag table',
  `tag_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT 'Date of most recent save for this tag-item',
  `type_id` mediumint(8) NOT NULL COMMENT 'PK from the content_type table',
  UNIQUE KEY `uc_ItemnameTagid` (`type_id`,`content_item_id`,`tag_id`),
  KEY `idx_tag_type` (`tag_id`,`type_id`),
  KEY `idx_date_id` (`tag_date`,`tag_id`),
  KEY `idx_tag` (`tag_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_core_content_id` (`core_content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Maps items from content tables to tags';

-- --------------------------------------------------------

--
-- Table structure for table `#__content_frontpage`
--

DROP TABLE IF EXISTS `#__content_frontpage`;
CREATE TABLE IF NOT EXISTS `#__content_frontpage` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__content_frontpage`
--

INSERT INTO `#__content_frontpage` (`content_id`, `ordering`) VALUES
(34, 6),
(38, 5),
(39, 4),
(40, 3),
(41, 1),
(42, 2);

-- --------------------------------------------------------

--
-- Table structure for table `#__content_rating`
--

DROP TABLE IF EXISTS `#__content_rating`;
CREATE TABLE IF NOT EXISTS `#__content_rating` (
  `content_id` int(11) NOT NULL DEFAULT '0',
  `rating_sum` int(10) unsigned NOT NULL DEFAULT '0',
  `rating_count` int(10) unsigned NOT NULL DEFAULT '0',
  `lastip` varchar(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`content_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__content_types`
--

DROP TABLE IF EXISTS `#__content_types`;
CREATE TABLE IF NOT EXISTS `#__content_types` (
  `type_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type_title` varchar(255) NOT NULL DEFAULT '',
  `type_alias` varchar(255) NOT NULL DEFAULT '',
  `table` varchar(255) NOT NULL DEFAULT '',
  `rules` text NOT NULL,
  `field_mappings` text NOT NULL,
  `router` varchar(255) NOT NULL DEFAULT '',
  `content_history_options` varchar(5120) NOT NULL COMMENT 'JSON string for com_contenthistory options',
  PRIMARY KEY (`type_id`),
  KEY `idx_alias` (`type_alias`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=16 ;

--
-- Dumping data for table `#__content_types`
--

INSERT INTO `#__content_types` (`type_id`, `type_title`, `type_alias`, `table`, `rules`, `field_mappings`, `router`, `content_history_options`) VALUES
(1, 'Article', 'com_content.article', '{"special":{"dbtable":"#__content","key":"id","type":"Content","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"introtext", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"attribs", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"asset_id"}, "special": {"fulltext":"fulltext"}}', 'ContentHelperRoute::getArticleRoute', '{"formFile":"administrator\\/components\\/com_content\\/models\\/forms\\/article.xml", "hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(2, 'Weblink', 'com_weblinks.weblink', '{"special":{"dbtable":"#__weblinks","key":"id","type":"Weblink","prefix":"WeblinksTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"state","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"url", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special": {}}', 'WeblinksHelperRoute::getWeblinkRoute', '{"formFile":"administrator\\/components\\/com_weblinks\\/models\\/forms\\/weblink.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","featured","images"], "ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"], "convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ]}'),
(3, 'Contact', 'com_contact.contact', '{"special":{"dbtable":"#__contact_details","key":"id","type":"Contact","prefix":"ContactTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"address", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"image", "core_urls":"webpage", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special": {"con_position":"con_position","suburb":"suburb","state":"state","country":"country","postcode":"postcode","telephone":"telephone","fax":"fax","misc":"misc","email_to":"email_to","default_con":"default_con","user_id":"user_id","mobile":"mobile","sortname1":"sortname1","sortname2":"sortname2","sortname3":"sortname3"}}', 'ContactHelperRoute::getContactRoute', '{"formFile":"administrator\\/components\\/com_contact\\/models\\/forms\\/contact.xml","hideFields":["default_con","checked_out","checked_out_time","version","xreference"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"], "displayLookup":[ {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"} ] }'),
(4, 'Newsfeed', 'com_newsfeeds.newsfeed', '{"special":{"dbtable":"#__newsfeeds","key":"id","type":"Newsfeed","prefix":"NewsfeedsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"hits","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"xreference", "asset_id":"null"}, "special": {"numarticles":"numarticles","cache_time":"cache_time","rtl":"rtl"}}', 'NewsfeedsHelperRoute::getNewsfeedRoute', '{"formFile":"administrator\\/components\\/com_newsfeeds\\/models\\/forms\\/newsfeed.xml","hideFields":["asset_id","checked_out","checked_out_time","version"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "hits"],"convertToInt":["publish_up", "publish_down", "featured", "ordering"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(5, 'User', 'com_users.user', '{"special":{"dbtable":"#__users","key":"id","type":"User","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"null","core_alias":"username","core_created_time":"registerdate","core_modified_time":"lastvisitDate","core_body":"null", "core_hits":"null","core_publish_up":"null","core_publish_down":"null","access":"null", "core_params":"params", "core_featured":"null", "core_metadata":"null", "core_language":"null", "core_images":"null", "core_urls":"null", "core_version":"null", "core_ordering":"null", "core_metakey":"null", "core_metadesc":"null", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special": {}}', 'UsersHelperRoute::getUserRoute', ''),
(6, 'Article Category', 'com_content.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContentHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(7, 'Contact Category', 'com_contact.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'ContactHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(8, 'Newsfeeds Category', 'com_newsfeeds.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'NewsfeedsHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(9, 'Weblinks Category', 'com_weblinks.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', 'WeblinksHelperRoute::getCategoryRoute', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(10, 'Tag', 'com_tags.tag', '{"special":{"dbtable":"#__tags","key":"tag_id","type":"Tag","prefix":"TagsTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"featured", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"urls", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"null", "core_xreference":"null", "asset_id":"null"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path"}}', 'TagsHelperRoute::getTagRoute', '{"formFile":"administrator\\/components\\/com_tags\\/models\\/forms\\/tag.xml", "hideFields":["checked_out","checked_out_time","version", "lft", "rgt", "level", "path", "urls", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"],"convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(11, 'Banner', 'com_banners.banner', '{"special":{"dbtable":"#__banners","key":"id","type":"Banner","prefix":"BannersTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"name","core_state":"published","core_alias":"alias","core_created_time":"created","core_modified_time":"modified","core_body":"description", "core_hits":"null","core_publish_up":"publish_up","core_publish_down":"publish_down","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"images", "core_urls":"link", "core_version":"version", "core_ordering":"ordering", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"catid", "core_xreference":"null", "asset_id":"null"}, "special":{"imptotal":"imptotal", "impmade":"impmade", "clicks":"clicks", "clickurl":"clickurl", "custombannercode":"custombannercode", "cid":"cid", "purchase_type":"purchase_type", "track_impressions":"track_impressions", "track_clicks":"track_clicks"}}', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/banner.xml", "hideFields":["checked_out","checked_out_time","version", "reset"],"ignoreChanges":["modified_by", "modified", "checked_out", "checked_out_time", "version", "imptotal", "impmade", "reset"], "convertToInt":["publish_up", "publish_down", "ordering"], "displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"cid","targetTable":"#__banner_clients","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"created_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"modified_by","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(12, 'Banners Category', 'com_banners.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special": {"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["asset_id","checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}'),
(13, 'Banner Client', 'com_banners.client', '{"special":{"dbtable":"#__banner_clients","key":"id","type":"Client","prefix":"BannersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_banners\\/models\\/forms\\/client.xml", "hideFields":["checked_out","checked_out_time"], "ignoreChanges":["checked_out", "checked_out_time"], "convertToInt":[], "displayLookup":[]}'),
(14, 'User Notes', 'com_users.note', '{"special":{"dbtable":"#__user_notes","key":"id","type":"Note","prefix":"UsersTable"}}', '', '', '', '{"formFile":"administrator\\/components\\/com_users\\/models\\/forms\\/note.xml", "hideFields":["checked_out","checked_out_time", "publish_up", "publish_down"],"ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time"], "convertToInt":["publish_up", "publish_down"],"displayLookup":[{"sourceColumn":"catid","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}, {"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}]}'),
(15, 'User Notes Category', 'com_users.category', '{"special":{"dbtable":"#__categories","key":"id","type":"Category","prefix":"JTable","config":"array()"},"common":{"dbtable":"#__ucm_content","key":"ucm_id","type":"Corecontent","prefix":"JTable","config":"array()"}}', '', '{"common":{"core_content_item_id":"id","core_title":"title","core_state":"published","core_alias":"alias","core_created_time":"created_time","core_modified_time":"modified_time","core_body":"description", "core_hits":"hits","core_publish_up":"null","core_publish_down":"null","core_access":"access", "core_params":"params", "core_featured":"null", "core_metadata":"metadata", "core_language":"language", "core_images":"null", "core_urls":"null", "core_version":"version", "core_ordering":"null", "core_metakey":"metakey", "core_metadesc":"metadesc", "core_catid":"parent_id", "core_xreference":"null", "asset_id":"asset_id"}, "special":{"parent_id":"parent_id","lft":"lft","rgt":"rgt","level":"level","path":"path","extension":"extension","note":"note"}}', '', '{"formFile":"administrator\\/components\\/com_categories\\/models\\/forms\\/category.xml", "hideFields":["checked_out","checked_out_time","version","lft","rgt","level","path","extension"], "ignoreChanges":["modified_user_id", "modified_time", "checked_out", "checked_out_time", "version", "hits", "path"], "convertToInt":["publish_up", "publish_down"], "displayLookup":[{"sourceColumn":"created_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"}, {"sourceColumn":"access","targetTable":"#__viewlevels","targetColumn":"id","displayColumn":"title"},{"sourceColumn":"modified_user_id","targetTable":"#__users","targetColumn":"id","displayColumn":"name"},{"sourceColumn":"parent_id","targetTable":"#__categories","targetColumn":"id","displayColumn":"title"}]}');

-- --------------------------------------------------------

--
-- Table structure for table `#__core_log_searches`
--

DROP TABLE IF EXISTS `#__core_log_searches`;
CREATE TABLE IF NOT EXISTS `#__core_log_searches` (
  `search_term` varchar(128) NOT NULL DEFAULT '',
  `hits` int(10) unsigned NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__extensions`
--

DROP TABLE IF EXISTS `#__extensions`;
CREATE TABLE IF NOT EXISTS `#__extensions` (
  `extension_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `type` varchar(20) NOT NULL,
  `element` varchar(100) NOT NULL,
  `folder` varchar(100) NOT NULL,
  `client_id` tinyint(3) NOT NULL,
  `enabled` tinyint(3) NOT NULL DEFAULT '1',
  `access` int(10) unsigned NOT NULL DEFAULT '1',
  `protected` tinyint(3) NOT NULL DEFAULT '0',
  `manifest_cache` text NOT NULL,
  `params` text NOT NULL,
  `custom_data` text NOT NULL,
  `system_data` text NOT NULL,
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) DEFAULT '0',
  `state` int(11) DEFAULT '0',
  PRIMARY KEY (`extension_id`),
  KEY `element_clientid` (`element`,`client_id`),
  KEY `element_folder_clientid` (`element`,`folder`,`client_id`),
  KEY `extension` (`type`,`element`,`folder`,`client_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10021 ;

--
-- Dumping data for table `#__extensions`
--

INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(1, 'com_mailto', 'component', 'com_mailto', '', 0, 1, 1, 1, '{"name":"com_mailto","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MAILTO_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(2, 'com_wrapper', 'component', 'com_wrapper', '', 0, 1, 1, 1, '{"name":"com_wrapper","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(3, 'com_admin', 'component', 'com_admin', '', 1, 1, 1, 1, '{"name":"com_admin","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_ADMIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(4, 'com_banners', 'component', 'com_banners', '', 1, 1, 1, 0, '{"name":"com_banners","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_BANNERS_XML_DESCRIPTION","group":""}', '{"purchase_type":"3","track_impressions":"0","track_clicks":"0","metakey_prefix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(5, 'com_cache', 'component', 'com_cache', '', 1, 1, 1, 1, '{"name":"com_cache","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CACHE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'com_categories', 'component', 'com_categories', '', 1, 1, 1, 1, '{"name":"com_categories","type":"component","creationDate":"December 2007","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(7, 'com_checkin', 'component', 'com_checkin', '', 1, 1, 1, 1, '{"name":"com_checkin","type":"component","creationDate":"Unknown","author":"Joomla! Project","copyright":"(C) 2005 - 2008 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CHECKIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(8, 'com_contact', 'component', 'com_contact', '', 1, 1, 1, 0, '{"name":"com_contact","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTACT_XML_DESCRIPTION","group":""}', '{"contact_layout":"_:default","show_contact_category":"hide","show_contact_list":"0","presentation_style":"sliders","show_name":"1","show_position":"1","show_email":"1","show_street_address":"1","show_suburb":"1","show_state":"1","show_postcode":"1","show_country":"1","show_telephone":"1","show_mobile":"1","show_fax":"1","show_webpage":"1","show_misc":"1","show_image":"1","image":"","allow_vcard":"0","show_articles":"0","show_profile":"0","show_links":"0","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","contact_icons":"0","icon_address":"","icon_email":"","icon_telephone":"","icon_mobile":"","icon_fax":"","icon_misc":"","category_layout":"_:default","show_category_title":"1","show_description":"1","show_description_image":"0","maxLevel":"-1","show_empty_categories":"0","show_subcat_desc":"1","show_cat_items":"1","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_items_cat":"1","show_pagination_limit":"1","show_headings":"1","show_position_headings":"1","show_email_headings":"0","show_telephone_headings":"1","show_mobile_headings":"0","show_fax_headings":"0","show_suburb_headings":"1","show_state_headings":"1","show_country_headings":"1","show_pagination":"2","show_pagination_results":"1","initial_sort":"ordering","captcha":"","show_email_form":"1","show_email_copy":"1","banned_email":"","banned_subject":"","banned_text":"","validate_session":"1","custom_reply":"0","redirect":"","show_feed_link":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(9, 'com_cpanel', 'component', 'com_cpanel', '', 1, 1, 1, 1, '{"name":"com_cpanel","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CPANEL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10, 'com_installer', 'component', 'com_installer', '', 1, 1, 1, 1, '{"name":"com_installer","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_INSTALLER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(11, 'com_languages', 'component', 'com_languages', '', 1, 1, 1, 1, '{"name":"com_languages","type":"component","creationDate":"2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LANGUAGES_XML_DESCRIPTION","group":""}', '{"administrator":"en-GB","site":"en-GB"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(12, 'com_login', 'component', 'com_login', '', 1, 1, 1, 1, '{"name":"com_login","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(13, 'com_media', 'component', 'com_media', '', 1, 1, 0, 1, '{"name":"com_media","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MEDIA_XML_DESCRIPTION","group":""}', '{"upload_extensions":"bmp,csv,doc,gif,ico,jpg,jpeg,odg,odp,ods,odt,pdf,png,ppt,swf,txt,xcf,xls,BMP,CSV,DOC,GIF,ICO,JPG,JPEG,ODG,ODP,ODS,ODT,PDF,PNG,PPT,SWF,TXT,XCF,XLS","upload_maxsize":"10","file_path":"images","image_path":"images","restrict_uploads":"1","allowed_media_usergroup":"3","check_mime":"1","image_extensions":"bmp,gif,jpg,png","ignore_extensions":"","upload_mime":"image\\/jpeg,image\\/gif,image\\/png,image\\/bmp,application\\/x-shockwave-flash,application\\/msword,application\\/excel,application\\/pdf,application\\/powerpoint,text\\/plain,application\\/x-zip","upload_mime_illegal":"text\\/html"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(14, 'com_menus', 'component', 'com_menus', '', 1, 1, 1, 1, '{"name":"com_menus","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MENUS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(15, 'com_messages', 'component', 'com_messages', '', 1, 1, 1, 1, '{"name":"com_messages","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MESSAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(16, 'com_modules', 'component', 'com_modules', '', 1, 1, 1, 1, '{"name":"com_modules","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_MODULES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(17, 'com_newsfeeds', 'component', 'com_newsfeeds', '', 1, 1, 1, 0, '{"name":"com_newsfeeds","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"show_feed_image":"1","show_feed_description":"1","show_item_description":"1","feed_word_count":"0","show_headings":"1","show_name":"1","show_articles":"0","show_link":"1","show_description":"1","show_description_image":"1","display_num":"","show_pagination_limit":"1","show_pagination":"1","show_pagination_results":"1","show_cat_items":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(18, 'com_plugins', 'component', 'com_plugins', '', 1, 1, 1, 1, '{"name":"com_plugins","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_PLUGINS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(19, 'com_search', 'component', 'com_search', '', 1, 1, 1, 0, '{"name":"com_search","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_SEARCH_XML_DESCRIPTION","group":""}', '{"enabled":"0","show_date":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(20, 'com_templates', 'component', 'com_templates', '', 1, 1, 1, 1, '{"name":"com_templates","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_TEMPLATES_XML_DESCRIPTION","group":""}', '{"template_positions_display":"0","upload_limit":"2","image_formats":"gif,bmp,jpg,jpeg,png","source_formats":"txt,less,ini,xml,js,php,css","font_formats":"woff,ttf,otf","compressed_formats":"zip"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(21, 'com_weblinks', 'component', 'com_weblinks', '', 1, 1, 1, 0, '{"name":"com_weblinks","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\n\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_WEBLINKS_XML_DESCRIPTION","group":""}', '{"show_comp_description":"1","comp_description":"","show_link_hits":"1","show_link_description":"1","show_other_cats":"0","show_headings":"0","show_numbers":"0","show_report":"1","count_clicks":"1","target":"0","link_icons":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(22, 'com_content', 'component', 'com_content', '', 1, 1, 0, 1, '{"name":"com_content","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONTENT_XML_DESCRIPTION","group":""}', '{"article_layout":"_:default","show_title":"1","link_titles":"1","show_intro":"1","show_category":"1","link_category":"1","show_parent_category":"0","link_parent_category":"0","show_author":"1","link_author":"0","show_create_date":"0","show_modify_date":"0","show_publish_date":"1","show_item_navigation":"1","show_vote":"0","show_readmore":"1","show_readmore_title":"1","readmore_limit":"100","show_icons":"1","show_print_icon":"1","show_email_icon":"1","show_hits":"1","show_noauth":"0","urls_position":"0","show_publishing_options":"1","show_article_options":"1","show_urls_images_frontend":"1","show_urls_images_backend":"1","targeta":0,"targetb":0,"targetc":0,"float_intro":"left","float_fulltext":"left","category_layout":"_:blog","show_category_heading_title_text":"1","show_category_title":"0","show_description":"0","show_description_image":"0","maxLevel":"1","show_empty_categories":"0","show_no_articles":"1","show_subcat_desc":"1","show_cat_num_articles":"0","show_base_description":"1","maxLevelcat":"-1","show_empty_categories_cat":"0","show_subcat_desc_cat":"1","show_cat_num_articles_cat":"1","num_leading_articles":"1","num_intro_articles":"4","num_columns":"2","num_links":"4","multi_column_order":"0","show_subcategory_content":"0","show_pagination_limit":"1","filter_field":"hide","show_headings":"1","list_show_date":"0","date_format":"","list_show_hits":"1","list_show_author":"1","orderby_pri":"order","orderby_sec":"rdate","order_date":"published","show_pagination":"2","show_pagination_results":"1","show_feed_link":"1","feed_summary":"0","feed_show_readmore":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(23, 'com_config', 'component', 'com_config', '', 1, 1, 0, 1, '{"name":"com_config","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_CONFIG_XML_DESCRIPTION","group":""}', '{"filters":{"1":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"6":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"7":{"filter_type":"NONE","filter_tags":"","filter_attributes":""},"2":{"filter_type":"NH","filter_tags":"","filter_attributes":""},"3":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"4":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"5":{"filter_type":"BL","filter_tags":"","filter_attributes":""},"8":{"filter_type":"NONE","filter_tags":"","filter_attributes":""}}}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(24, 'com_redirect', 'component', 'com_redirect', '', 1, 1, 0, 1, '{"name":"com_redirect","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(25, 'com_users', 'component', 'com_users', '', 1, 1, 0, 1, '{"name":"com_users","type":"component","creationDate":"April 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"COM_USERS_XML_DESCRIPTION","group":""}', '{"allowUserRegistration":"1","new_usertype":"2","useractivation":"1","frontend_userparams":"1","mailSubjectPrefix":"","mailBodySuffix":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(27, 'com_finder', 'component', 'com_finder', '', 1, 1, 0, 0, '{"legacy":false,"name":"com_finder","type":"component","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_FINDER_XML_DESCRIPTION","group":""}', '{"show_description":"1","description_length":255,"allow_empty_query":"0","show_url":"1","show_advanced":"1","expand_advanced":"0","show_date_filters":"0","highlight_terms":"1","opensearch_name":"","opensearch_description":"","batch_size":"50","memory_table_limit":30000,"title_multiplier":"1.7","text_multiplier":"0.7","meta_multiplier":"1.2","path_multiplier":"2.0","misc_multiplier":"0.3","stemmer":"snowball"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(28, 'com_joomlaupdate', 'component', 'com_joomlaupdate', '', 1, 1, 0, 1, '{"legacy":false,"name":"com_joomlaupdate","type":"component","creationDate":"February 2012","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.\\t","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"COM_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{"updatesource":"sts","customurl":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(29, 'com_tags', 'component', 'com_tags', '', 1, 1, 1, 1, '{"name":"com_tags","type":"component","creationDate":"December 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"COM_TAGS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(30, 'com_contenthistory', 'component', 'com_contenthistory', '', 1, 1, 1, 0, '{"name":"com_contenthistory","type":"component","creationDate":"May 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_CONTENTHISTORY_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(31, 'com_ajax', 'component', 'com_ajax', '', 1, 1, 1, 0, '{"name":"com_ajax","type":"component","creationDate":"August 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_AJAX_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(32, 'com_postinstall', 'component', 'com_postinstall', '', 1, 1, 1, 1, '{"name":"com_postinstall","type":"component","creationDate":"September 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"COM_POSTINSTALL_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(100, 'PHPMailer', 'library', 'phpmailer', '', 0, 1, 1, 1, '{"name":"PHPMailer","type":"library","creationDate":"2001","author":"PHPMailer","copyright":"(c) 2001-2003, Brent R. Matzelle, (c) 2004-2009, Andy Prevost. All Rights Reserved., (c) 2010-2013, Jim Jagielski. All Rights Reserved.","authorEmail":"jimjag@gmail.com","authorUrl":"https:\\/\\/github.com\\/PHPMailer\\/PHPMailer","version":"5.2.6","description":"LIB_PHPMAILER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(101, 'SimplePie', 'library', 'simplepie', '', 0, 1, 1, 1, '{"name":"SimplePie","type":"library","creationDate":"2004","author":"SimplePie","copyright":"Copyright (c) 2004-2009, Ryan Parman and Geoffrey Sneddon","authorEmail":"","authorUrl":"http:\\/\\/simplepie.org\\/","version":"1.2","description":"LIB_SIMPLEPIE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(102, 'phputf8', 'library', 'phputf8', '', 0, 1, 1, 1, '{"name":"phputf8","type":"library","creationDate":"2006","author":"Harry Fuecks","copyright":"Copyright various authors","authorEmail":"hfuecks@gmail.com","authorUrl":"http:\\/\\/sourceforge.net\\/projects\\/phputf8","version":"0.5","description":"LIB_PHPUTF8_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(103, 'Joomla! Platform', 'library', 'joomla', '', 0, 1, 1, 1, '{"name":"Joomla! Platform","type":"library","creationDate":"2008","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"http:\\/\\/www.joomla.org","version":"13.1","description":"LIB_JOOMLA_XML_DESCRIPTION","group":""}', '{"mediaversion":"a2ace606f9c6619db08a7a881cfab68f"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(104, 'IDNA Convert', 'library', 'idna_convert', '', 0, 1, 1, 1, '{"name":"IDNA Convert","type":"library","creationDate":"2004","author":"phlyLabs","copyright":"2004-2011 phlyLabs Berlin, http:\\/\\/phlylabs.de","authorEmail":"phlymail@phlylabs.de","authorUrl":"http:\\/\\/phlylabs.de","version":"0.8.0","description":"LIB_IDNA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(105, 'FOF', 'library', 'fof', '', 0, 1, 1, 1, '{"name":"FOF","type":"library","creationDate":"2013-10-22","author":"Nicholas K. Dionysopoulos \\/ Akeeba Ltd","copyright":"(C)2011-2013 Nicholas K. Dionysopoulos","authorEmail":"nicholas@akeebabackup.com","authorUrl":"https:\\/\\/www.akeebabackup.com","version":"2.1.rc4","description":"LIB_FOF_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(200, 'mod_articles_archive', 'module', 'mod_articles_archive', '', 0, 1, 1, 0, '{"name":"mod_articles_archive","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters.\\n\\t\\tAll rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_ARCHIVE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(201, 'mod_articles_latest', 'module', 'mod_articles_latest', '', 0, 1, 1, 0, '{"name":"mod_articles_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(202, 'mod_articles_popular', 'module', 'mod_articles_popular', '', 0, 1, 1, 0, '{"name":"mod_articles_popular","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(203, 'mod_banners', 'module', 'mod_banners', '', 0, 1, 1, 0, '{"name":"mod_banners","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BANNERS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(204, 'mod_breadcrumbs', 'module', 'mod_breadcrumbs', '', 0, 1, 1, 1, '{"name":"mod_breadcrumbs","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_BREADCRUMBS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(205, 'mod_custom', 'module', 'mod_custom', '', 0, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(206, 'mod_feed', 'module', 'mod_feed', '', 0, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(207, 'mod_footer', 'module', 'mod_footer', '', 0, 1, 1, 0, '{"name":"mod_footer","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FOOTER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(208, 'mod_login', 'module', 'mod_login', '', 0, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(209, 'mod_menu', 'module', 'mod_menu', '', 0, 1, 1, 1, '{"name":"mod_menu","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(210, 'mod_articles_news', 'module', 'mod_articles_news', '', 0, 1, 1, 0, '{"name":"mod_articles_news","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_NEWS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(211, 'mod_random_image', 'module', 'mod_random_image', '', 0, 1, 1, 0, '{"name":"mod_random_image","type":"module","creationDate":"July 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RANDOM_IMAGE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(212, 'mod_related_items', 'module', 'mod_related_items', '', 0, 1, 1, 0, '{"name":"mod_related_items","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_RELATED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(213, 'mod_search', 'module', 'mod_search', '', 0, 1, 1, 0, '{"name":"mod_search","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SEARCH_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(214, 'mod_stats', 'module', 'mod_stats', '', 0, 1, 1, 0, '{"name":"mod_stats","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(215, 'mod_syndicate', 'module', 'mod_syndicate', '', 0, 1, 1, 1, '{"name":"mod_syndicate","type":"module","creationDate":"May 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SYNDICATE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(216, 'mod_users_latest', 'module', 'mod_users_latest', '', 0, 1, 1, 0, '{"name":"mod_users_latest","type":"module","creationDate":"December 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_USERS_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(217, 'mod_weblinks', 'module', 'mod_weblinks', '', 0, 1, 1, 0, '{"name":"mod_weblinks","type":"module","creationDate":"July 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WEBLINKS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(218, 'mod_whosonline', 'module', 'mod_whosonline', '', 0, 1, 1, 0, '{"name":"mod_whosonline","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WHOSONLINE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(219, 'mod_wrapper', 'module', 'mod_wrapper', '', 0, 1, 1, 0, '{"name":"mod_wrapper","type":"module","creationDate":"October 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_WRAPPER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(220, 'mod_articles_category', 'module', 'mod_articles_category', '', 0, 1, 1, 0, '{"name":"mod_articles_category","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORY_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(221, 'mod_articles_categories', 'module', 'mod_articles_categories', '', 0, 1, 1, 0, '{"name":"mod_articles_categories","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_ARTICLES_CATEGORIES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(222, 'mod_languages', 'module', 'mod_languages', '', 0, 1, 1, 1, '{"name":"mod_languages","type":"module","creationDate":"February 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LANGUAGES_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(223, 'mod_finder', 'module', 'mod_finder', '', 0, 1, 0, 0, '{"legacy":false,"name":"mod_finder","type":"module","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_FINDER_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(300, 'mod_custom', 'module', 'mod_custom', '', 1, 1, 1, 1, '{"name":"mod_custom","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_CUSTOM_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(301, 'mod_feed', 'module', 'mod_feed', '', 1, 1, 1, 0, '{"name":"mod_feed","type":"module","creationDate":"July 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_FEED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(302, 'mod_latest', 'module', 'mod_latest', '', 1, 1, 1, 0, '{"name":"mod_latest","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LATEST_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(303, 'mod_logged', 'module', 'mod_logged', '', 1, 1, 1, 0, '{"name":"mod_logged","type":"module","creationDate":"January 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGGED_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(304, 'mod_login', 'module', 'mod_login', '', 1, 1, 1, 1, '{"name":"mod_login","type":"module","creationDate":"March 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_LOGIN_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(305, 'mod_menu', 'module', 'mod_menu', '', 1, 1, 1, 0, '{"name":"mod_menu","type":"module","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(307, 'mod_popular', 'module', 'mod_popular', '', 1, 1, 1, 0, '{"name":"mod_popular","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_POPULAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(308, 'mod_quickicon', 'module', 'mod_quickicon', '', 1, 1, 1, 1, '{"name":"mod_quickicon","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_QUICKICON_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(309, 'mod_status', 'module', 'mod_status', '', 1, 1, 1, 0, '{"name":"mod_status","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATUS_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(310, 'mod_submenu', 'module', 'mod_submenu', '', 1, 1, 1, 0, '{"name":"mod_submenu","type":"module","creationDate":"Feb 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_SUBMENU_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(311, 'mod_title', 'module', 'mod_title', '', 1, 1, 1, 0, '{"name":"mod_title","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TITLE_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(312, 'mod_toolbar', 'module', 'mod_toolbar', '', 1, 1, 1, 1, '{"name":"mod_toolbar","type":"module","creationDate":"Nov 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_TOOLBAR_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(313, 'mod_multilangstatus', 'module', 'mod_multilangstatus', '', 1, 1, 1, 0, '{"name":"mod_multilangstatus","type":"module","creationDate":"September 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_MULTILANGSTATUS_XML_DESCRIPTION","group":""}', '{"cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(314, 'mod_version', 'module', 'mod_version', '', 1, 1, 1, 0, '{"legacy":false,"name":"mod_version","type":"module","creationDate":"January 2012","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"MOD_VERSION_XML_DESCRIPTION","group":""}', '{"format":"short","product":"1","cache":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(315, 'mod_stats_admin', 'module', 'mod_stats_admin', '', 1, 1, 1, 0, '{"name":"mod_stats_admin","type":"module","creationDate":"July 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"MOD_STATS_XML_DESCRIPTION","group":""}', '{"serverinfo":"0","siteinfo":"0","counter":"0","increase":"0","cache":"1","cache_time":"900","cachemode":"static"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(316, 'mod_tags_popular', 'module', 'mod_tags_popular', '', 0, 1, 1, 0, '{"name":"mod_tags_popular","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_POPULAR_XML_DESCRIPTION","group":""}', '{"maximum":"5","timeframe":"alltime","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(317, 'mod_tags_similar', 'module', 'mod_tags_similar', '', 0, 1, 1, 0, '{"name":"mod_tags_similar","type":"module","creationDate":"January 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.1.0","description":"MOD_TAGS_SIMILAR_XML_DESCRIPTION","group":""}', '{"maximum":"5","matchtype":"any","owncache":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(400, 'plg_authentication_gmail', 'plugin', 'gmail', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_gmail","type":"plugin","creationDate":"February 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_GMAIL_XML_DESCRIPTION","group":""}', '{"applysuffix":"0","suffix":"","verifypeer":"1","user_blacklist":""}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(401, 'plg_authentication_joomla', 'plugin', 'joomla', 'authentication', 0, 1, 1, 1, '{"name":"plg_authentication_joomla","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(402, 'plg_authentication_ldap', 'plugin', 'ldap', 'authentication', 0, 0, 1, 0, '{"name":"plg_authentication_ldap","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LDAP_XML_DESCRIPTION","group":""}', '{"host":"","port":"389","use_ldapV3":"0","negotiate_tls":"0","no_referrals":"0","auth_method":"bind","base_dn":"","search_string":"","users_dn":"","username":"admin","password":"bobby7","ldap_fullname":"fullName","ldap_email":"mail","ldap_uid":"uid"}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(404, 'plg_content_emailcloak', 'plugin', 'emailcloak', 'content', 0, 1, 1, 0, '{"name":"plg_content_emailcloak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_EMAILCLOAK_XML_DESCRIPTION","group":""}', '{"mode":"1"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(405, 'plg_content_geshi', 'plugin', 'geshi', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_geshi","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"","authorUrl":"qbnz.com\\/highlighter","version":"2.5.0","description":"PLG_CONTENT_GESHI_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(406, 'plg_content_loadmodule', 'plugin', 'loadmodule', 'content', 0, 1, 1, 0, '{"name":"plg_content_loadmodule","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOADMODULE_XML_DESCRIPTION","group":""}', '{"style":"xhtml"}', '', '', 0, '2011-09-18 15:22:50', 0, 0),
(407, 'plg_content_pagebreak', 'plugin', 'pagebreak', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagebreak","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_PAGEBREAK_XML_DESCRIPTION","group":""}', '{"title":"1","multipage_toc":"1","showall":"1"}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(408, 'plg_content_pagenavigation', 'plugin', 'pagenavigation', 'content', 0, 1, 1, 0, '{"name":"plg_content_pagenavigation","type":"plugin","creationDate":"January 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_PAGENAVIGATION_XML_DESCRIPTION","group":""}', '{"position":"1"}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(409, 'plg_content_vote', 'plugin', 'vote', 'content', 0, 1, 1, 0, '{"name":"plg_content_vote","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_VOTE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(410, 'plg_editors_codemirror', 'plugin', 'codemirror', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_codemirror","type":"plugin","creationDate":"28 March 2011","author":"Marijn Haverbeke","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"3.15","description":"PLG_CODEMIRROR_XML_DESCRIPTION","group":""}', '{"lineNumbers":"1","lineWrapping":"1","matchTags":"1","matchBrackets":"1","marker-gutter":"1","autoCloseTags":"1","autoCloseBrackets":"1","autoFocus":"1","theme":"default","tabmode":"indent"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(411, 'plg_editors_none', 'plugin', 'none', 'editors', 0, 1, 1, 1, '{"name":"plg_editors_none","type":"plugin","creationDate":"August 2004","author":"Unknown","copyright":"","authorEmail":"N\\/A","authorUrl":"","version":"3.0.0","description":"PLG_NONE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(412, 'plg_editors_tinymce', 'plugin', 'tinymce', 'editors', 0, 1, 1, 0, '{"name":"plg_editors_tinymce","type":"plugin","creationDate":"2005-2013","author":"Moxiecode Systems AB","copyright":"Moxiecode Systems AB","authorEmail":"N\\/A","authorUrl":"tinymce.moxiecode.com","version":"4.0.10","description":"PLG_TINY_XML_DESCRIPTION","group":""}', '{"mode":"1","skin":"0","entity_encoding":"raw","lang_mode":"0","lang_code":"en","text_direction":"ltr","content_css":"1","content_css_custom":"","relative_urls":"1","newlines":"0","invalid_elements":"script,applet,iframe","extended_elements":"","toolbar":"top","toolbar_align":"left","html_height":"550","html_width":"750","resizing":"true","resize_horizontal":"false","element_path":"1","fonts":"1","paste":"1","searchreplace":"1","insertdate":"1","format_date":"%Y-%m-%d","inserttime":"1","format_time":"%H:%M:%S","colors":"1","table":"1","smilies":"1","media":"1","hr":"1","directionality":"1","fullscreen":"1","style":"1","layer":"1","xhtmlxtras":"1","visualchars":"1","nonbreaking":"1","template":"1","blockquote":"1","wordcount":"1","advimage":"1","advlink":"1","advlist":"1","autosave":"1","contextmenu":"1","inlinepopups":"1","custom_plugin":"","custom_button":""}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(413, 'plg_editors-xtd_article', 'plugin', 'article', 'editors-xtd', 0, 1, 1, 1, '{"name":"plg_editors-xtd_article","type":"plugin","creationDate":"October 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_ARTICLE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(414, 'plg_editors-xtd_image', 'plugin', 'image', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_image","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_IMAGE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(415, 'plg_editors-xtd_pagebreak', 'plugin', 'pagebreak', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_pagebreak","type":"plugin","creationDate":"August 2004","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EDITORSXTD_PAGEBREAK_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(416, 'plg_editors-xtd_readmore', 'plugin', 'readmore', 'editors-xtd', 0, 1, 1, 0, '{"name":"plg_editors-xtd_readmore","type":"plugin","creationDate":"March 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_READMORE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(417, 'plg_search_categories', 'plugin', 'categories', 'search', 0, 1, 1, 0, '{"name":"plg_search_categories","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CATEGORIES_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(418, 'plg_search_contacts', 'plugin', 'contacts', 'search', 0, 1, 1, 0, '{"name":"plg_search_contacts","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTACTS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(419, 'plg_search_content', 'plugin', 'content', 'search', 0, 1, 1, 0, '{"name":"plg_search_content","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_CONTENT_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(420, 'plg_search_newsfeeds', 'plugin', 'newsfeeds', 'search', 0, 1, 1, 0, '{"name":"plg_search_newsfeeds","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);
INSERT INTO `#__extensions` (`extension_id`, `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES
(421, 'plg_search_weblinks', 'plugin', 'weblinks', 'search', 0, 1, 1, 0, '{"name":"plg_search_weblinks","type":"plugin","creationDate":"November 2005","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEARCH_WEBLINKS_XML_DESCRIPTION","group":""}', '{"search_limit":"50","search_content":"1","search_archived":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(422, 'plg_system_languagefilter', 'plugin', 'languagefilter', 'system', 0, 0, 1, 1, '{"name":"plg_system_languagefilter","type":"plugin","creationDate":"July 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGEFILTER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(423, 'plg_system_p3p', 'plugin', 'p3p', 'system', 0, 1, 1, 0, '{"name":"plg_system_p3p","type":"plugin","creationDate":"September 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_P3P_XML_DESCRIPTION","group":""}', '{"headers":"NOI ADM DEV PSAi COM NAV OUR OTRo STP IND DEM"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(424, 'plg_system_cache', 'plugin', 'cache', 'system', 0, 0, 1, 1, '{"name":"plg_system_cache","type":"plugin","creationDate":"February 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CACHE_XML_DESCRIPTION","group":""}', '{"browsercache":"0","cachetime":"15"}', '', '', 0, '0000-00-00 00:00:00', 9, 0),
(425, 'plg_system_debug', 'plugin', 'debug', 'system', 0, 1, 1, 0, '{"name":"plg_system_debug","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_DEBUG_XML_DESCRIPTION","group":""}', '{"profile":"1","queries":"1","memory":"1","language_files":"1","language_strings":"1","strip-first":"1","strip-prefix":"","strip-suffix":""}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(426, 'plg_system_log', 'plugin', 'log', 'system', 0, 1, 1, 1, '{"name":"plg_system_log","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_LOG_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(427, 'plg_system_redirect', 'plugin', 'redirect', 'system', 0, 1, 1, 1, '{"name":"plg_system_redirect","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REDIRECT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 6, 0),
(428, 'plg_system_remember', 'plugin', 'remember', 'system', 0, 1, 1, 1, '{"name":"plg_system_remember","type":"plugin","creationDate":"April 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_REMEMBER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(429, 'plg_system_sef', 'plugin', 'sef', 'system', 0, 1, 1, 0, '{"name":"plg_system_sef","type":"plugin","creationDate":"December 2007","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SEF_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 8, 0),
(430, 'plg_system_logout', 'plugin', 'logout', 'system', 0, 1, 1, 1, '{"name":"plg_system_logout","type":"plugin","creationDate":"April 2009","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LOGOUT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(431, 'plg_user_contactcreator', 'plugin', 'contactcreator', 'user', 0, 0, 1, 0, '{"name":"plg_user_contactcreator","type":"plugin","creationDate":"August 2009","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTACTCREATOR_XML_DESCRIPTION","group":""}', '{"autowebpage":"","category":"34","autopublish":"0"}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(432, 'plg_user_joomla', 'plugin', 'joomla', 'user', 0, 1, 1, 0, '{"name":"plg_user_joomla","type":"plugin","creationDate":"December 2006","author":"Joomla! Project","copyright":"(C) 2005 - 2009 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_JOOMLA_XML_DESCRIPTION","group":""}', '{"autoregister":"1"}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(433, 'plg_user_profile', 'plugin', 'profile', 'user', 0, 0, 1, 0, '{"name":"plg_user_profile","type":"plugin","creationDate":"January 2008","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_USER_PROFILE_XML_DESCRIPTION","group":""}', '{"register-require_address1":"1","register-require_address2":"1","register-require_city":"1","register-require_region":"1","register-require_country":"1","register-require_postal_code":"1","register-require_phone":"1","register-require_website":"1","register-require_favoritebook":"1","register-require_aboutme":"1","register-require_tos":"1","register-require_dob":"1","profile-require_address1":"1","profile-require_address2":"1","profile-require_city":"1","profile-require_region":"1","profile-require_country":"1","profile-require_postal_code":"1","profile-require_phone":"1","profile-require_website":"1","profile-require_favoritebook":"1","profile-require_aboutme":"1","profile-require_tos":"1","profile-require_dob":"1"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(434, 'plg_extension_joomla', 'plugin', 'joomla', 'extension', 0, 1, 1, 1, '{"name":"plg_extension_joomla","type":"plugin","creationDate":"May 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_EXTENSION_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(435, 'plg_content_joomla', 'plugin', 'joomla', 'content', 0, 1, 1, 0, '{"name":"plg_content_joomla","type":"plugin","creationDate":"November 2010","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CONTENT_JOOMLA_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(436, 'plg_system_languagecode', 'plugin', 'languagecode', 'system', 0, 0, 1, 0, '{"name":"plg_system_languagecode","type":"plugin","creationDate":"November 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_SYSTEM_LANGUAGECODE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 10, 0),
(437, 'plg_quickicon_joomlaupdate', 'plugin', 'joomlaupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_joomlaupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_JOOMLAUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(438, 'plg_quickicon_extensionupdate', 'plugin', 'extensionupdate', 'quickicon', 0, 1, 1, 1, '{"name":"plg_quickicon_extensionupdate","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_QUICKICON_EXTENSIONUPDATE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(439, 'plg_captcha_recaptcha', 'plugin', 'recaptcha', 'captcha', 0, 1, 1, 0, '{"name":"plg_captcha_recaptcha","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_CAPTCHA_RECAPTCHA_XML_DESCRIPTION","group":""}', '{"public_key":"","private_key":"","theme":"clean"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(440, 'plg_system_highlight', 'plugin', 'highlight', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"plg_system_highlight","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_SYSTEM_HIGHLIGHT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 7, 0),
(441, 'plg_content_finder', 'plugin', 'finder', 'content', 0, 0, 1, 0, '{"legacy":false,"name":"plg_content_finder","type":"plugin","creationDate":"December 2011","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"PLG_CONTENT_FINDER_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(442, 'plg_finder_categories', 'plugin', 'categories', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_categories","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CATEGORIES_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 1, 0),
(443, 'plg_finder_contacts', 'plugin', 'contacts', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_contacts","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTACTS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 2, 0),
(444, 'plg_finder_content', 'plugin', 'content', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_content","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_CONTENT_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 3, 0),
(445, 'plg_finder_newsfeeds', 'plugin', 'newsfeeds', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_newsfeeds","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_NEWSFEEDS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 4, 0),
(446, 'plg_finder_weblinks', 'plugin', 'weblinks', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_weblinks","type":"plugin","creationDate":"August 2011","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_WEBLINKS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 5, 0),
(447, 'plg_finder_tags', 'plugin', 'tags', 'finder', 0, 1, 1, 0, '{"name":"plg_finder_tags","type":"plugin","creationDate":"February 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_FINDER_TAGS_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(448, 'plg_twofactorauth_totp', 'plugin', 'totp', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_totp","type":"plugin","creationDate":"August 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_TOTP_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(449, 'plg_authentication_cookie', 'plugin', 'cookie', 'authentication', 0, 1, 1, 0, '{"name":"plg_authentication_cookie","type":"plugin","creationDate":"July 2013","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.0.0","description":"PLG_AUTH_COOKIE_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(450, 'plg_twofactorauth_yubikey', 'plugin', 'yubikey', 'twofactorauth', 0, 0, 1, 0, '{"name":"plg_twofactorauth_yubikey","type":"plugin","creationDate":"Se[ptember 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"PLG_TWOFACTORAUTH_YUBIKEY_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(500, 'atomic', 'template', 'atomic', '', 0, 1, 1, 0, '{"legacy":false,"name":"atomic","type":"template","creationDate":"10\\/10\\/09","author":"Ron Severdia","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"contact@kontentdesign.com","authorUrl":"http:\\/\\/www.kontentdesign.com","version":"2.5.0","description":"TPL_ATOMIC_XML_DESCRIPTION","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(502, 'bluestork', 'template', 'bluestork', '', 1, 1, 1, 0, '{"legacy":false,"name":"bluestork","type":"template","creationDate":"07\\/02\\/09","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"2.5.0","description":"TPL_BLUESTORK_XML_DESCRIPTION","group":""}', '{"useRoundedCorners":"1","showSiteName":"0","textBig":"0","highContrast":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(503, 'beez_20', 'template', 'beez_20', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez_20","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ2_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(504, 'hathor', 'template', 'hathor', '', 1, 1, 1, 0, '{"name":"hathor","type":"template","creationDate":"May 2010","author":"Andrea Tarr","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"hathor@tarrconsulting.com","authorUrl":"http:\\/\\/www.tarrconsulting.com","version":"3.0.0","description":"TPL_HATHOR_XML_DESCRIPTION","group":""}', '{"showSiteName":"0","colourChoice":"0","boldText":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(505, 'beez5', 'template', 'beez5', '', 0, 1, 1, 0, '{"legacy":false,"name":"beez5","type":"template","creationDate":"21 May 2010","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"2.5.0","description":"TPL_BEEZ5_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","html5":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(600, 'English (United Kingdom)', 'language', 'en-GB', '', 0, 1, 1, 1, '{"name":"English (United Kingdom)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"en-GB site language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(601, 'English (United Kingdom)', 'language', 'en-GB', '', 1, 1, 1, 1, '{"name":"English (United Kingdom)","type":"language","creationDate":"2013-03-07","author":"Joomla! Project","copyright":"Copyright (C) 2005 - 2013 Open Source Matters. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"en-GB administrator language","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(700, 'files_joomla', 'file', 'joomla', '', 0, 1, 1, 1, '{"name":"files_joomla","type":"file","creationDate":"November 2013","author":"Joomla! Project","copyright":"(C) 2005 - 2013 Open Source Matters. All rights reserved","authorEmail":"admin@joomla.org","authorUrl":"www.joomla.org","version":"3.2.0","description":"FILES_JOOMLA_XML_DESCRIPTION","group":""}', '', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10001, 'System - S5 Flex Menu', 'plugin', 'S5FlexMenu', 'system', 0, 1, 1, 0, '{"legacy":false,"name":"System - S5 Flex Menu","type":"plugin","creationDate":"June 2011","author":"Shape5.com","copyright":"This Plugin is released under the GNU\\/GPL License","authorEmail":"contact@shape5.com","authorUrl":"www.shape5.com","version":"1.0","description":"The S5 Flex Menu system is a very powerful plugin that provides functionality far beyond the standard Joomla menu system.","group":""}', '{}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10016, 'isis', 'template', 'isis', '', 1, 1, 1, 0, '{"name":"isis","type":"template","creationDate":"3\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_ISIS_XML_DESCRIPTION","group":""}', '{"templateColor":"","logoFile":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10017, 'protostar', 'template', 'protostar', '', 0, 1, 1, 0, '{"name":"protostar","type":"template","creationDate":"4\\/30\\/2012","author":"Kyle Ledbetter","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"admin@joomla.org","authorUrl":"","version":"1.0","description":"TPL_PROTOSTAR_XML_DESCRIPTION","group":""}', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10018, 'beez3', 'template', 'beez3', '', 0, 1, 1, 0, '{"name":"beez3","type":"template","creationDate":"25 November 2009","author":"Angie Radtke","copyright":"Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.","authorEmail":"a.radtke@derauftritt.de","authorUrl":"http:\\/\\/www.der-auftritt.de","version":"3.1.0","description":"TPL_BEEZ3_XML_DESCRIPTION","group":""}', '{"wrapperSmall":"53","wrapperLarge":"72","sitetitle":"","sitedescription":"","navposition":"center","templatecolor":"nature"}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10019, 'design_control', 'template', 'design_control', '', 0, 1, 1, 0, '{"name":"design_control","type":"template","creationDate":"December 2014","author":"Shape5.com","copyright":"Shape5","authorEmail":"contact@shape5.com","authorUrl":"http:\\/\\/www.shape5.com","version":"1.0","description":"\\n\\t\\n\\t<h1>Shape 5 - Design Control<\\/h1>\\n\\n\\t<br\\/>\\n\\t<img src=\\"..\\/templates\\/design_control\\/template_thumbnail.png\\" align=\\"left\\" hspace=\\"10\\" style=\\"padding-right:10px;\\"\\/>\\n\\tThis template is a free GPL licensed template Please be sure to visit our site for other great products.  <br \\/><br \\/>\\n\\tFor tutorials pertaining to this template and additional information check out:<br \\/> <a href=\\"http:\\/\\/www.shape5.com\\/demo\\/design_control\\">Design Control Demo<\\/a>\\n\\t<br \\/>\\n\\t<a href=\\"http:\\/\\/www.shape5.com\\/joomla_tutorials.html\\">Joomla and Vertex Tutorials<\\/a>\\n\\t<br \\/>\\n\\t<br \\/><a target=\\"_blank\\" href=\\"http:\\/\\/www.shape5.com\\">Click here<\\/a> to visit Shape5.com\\n\\t<br \\/><br \\/>\\n\\t<div class=\\"vertex-admin-logoback\\"><div class=\\"vertex-admin-logo\\"><\\/div><\\/div>\\n\\t<br \\/><br \\/>\\n\\tPowered by a comprehensive template blue print<br\\/><br\\/>\\n\\t<a target=\\"_blank\\" href=\\"http:\\/\\/www.shape5.com\\/joomla\\/framework\\/vertex_framework.html\\">Read more about Vertex here<\\/a> \\n\\t\\n\\t\\n\\t\\n\\t","group":""}', '{"settings":""}', '', '', 0, '0000-00-00 00:00:00', 0, 0),
(10020, 'S5 Register', 'module', 'mod_s5_register', '', 0, 1, 0, 0, '{"name":"S5 Register","type":"module","creationDate":"June 2013","author":"Shape5.com","copyright":"","authorEmail":"contact@shape5.com","authorUrl":"www.shape5.com","version":"3.0.0","description":"This is the default Joomla registration page convereted into a module. It also works directly with the User Profile plugin.","group":""}', '{"moduleclass_sfx":"","captcha":"0"}', '', '', 0, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_filters`
--

DROP TABLE IF EXISTS `#__finder_filters`;
CREATE TABLE IF NOT EXISTS `#__finder_filters` (
  `filter_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) NOT NULL,
  `state` tinyint(1) NOT NULL DEFAULT '1',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL,
  `created_by_alias` varchar(255) NOT NULL,
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `map_count` int(10) unsigned NOT NULL DEFAULT '0',
  `data` text NOT NULL,
  `params` mediumtext,
  PRIMARY KEY (`filter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links`
--

DROP TABLE IF EXISTS `#__finder_links`;
CREATE TABLE IF NOT EXISTS `#__finder_links` (
  `link_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `url` varchar(255) NOT NULL,
  `route` varchar(255) NOT NULL,
  `title` varchar(255) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `indexdate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `md5sum` varchar(32) DEFAULT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(5) DEFAULT '1',
  `access` int(5) DEFAULT '0',
  `language` varchar(8) NOT NULL,
  `publish_start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `start_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `end_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `list_price` double unsigned NOT NULL DEFAULT '0',
  `sale_price` double unsigned NOT NULL DEFAULT '0',
  `type_id` int(11) NOT NULL,
  `object` mediumblob NOT NULL,
  PRIMARY KEY (`link_id`),
  KEY `idx_type` (`type_id`),
  KEY `idx_title` (`title`),
  KEY `idx_md5` (`md5sum`),
  KEY `idx_url` (`url`(75)),
  KEY `idx_published_list` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`list_price`),
  KEY `idx_published_sale` (`published`,`state`,`access`,`publish_start_date`,`publish_end_date`,`sale_price`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms0`
--

DROP TABLE IF EXISTS `#__finder_links_terms0`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms0` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms1`
--

DROP TABLE IF EXISTS `#__finder_links_terms1`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms1` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms2`
--

DROP TABLE IF EXISTS `#__finder_links_terms2`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms2` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms3`
--

DROP TABLE IF EXISTS `#__finder_links_terms3`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms3` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms4`
--

DROP TABLE IF EXISTS `#__finder_links_terms4`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms4` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms5`
--

DROP TABLE IF EXISTS `#__finder_links_terms5`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms5` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms6`
--

DROP TABLE IF EXISTS `#__finder_links_terms6`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms6` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms7`
--

DROP TABLE IF EXISTS `#__finder_links_terms7`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms7` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms8`
--

DROP TABLE IF EXISTS `#__finder_links_terms8`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms8` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_terms9`
--

DROP TABLE IF EXISTS `#__finder_links_terms9`;
CREATE TABLE IF NOT EXISTS `#__finder_links_terms9` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsa`
--

DROP TABLE IF EXISTS `#__finder_links_termsa`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsa` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsb`
--

DROP TABLE IF EXISTS `#__finder_links_termsb`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsb` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsc`
--

DROP TABLE IF EXISTS `#__finder_links_termsc`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsc` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsd`
--

DROP TABLE IF EXISTS `#__finder_links_termsd`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsd` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termse`
--

DROP TABLE IF EXISTS `#__finder_links_termse`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termse` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_links_termsf`
--

DROP TABLE IF EXISTS `#__finder_links_termsf`;
CREATE TABLE IF NOT EXISTS `#__finder_links_termsf` (
  `link_id` int(10) unsigned NOT NULL,
  `term_id` int(10) unsigned NOT NULL,
  `weight` float unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`term_id`),
  KEY `idx_term_weight` (`term_id`,`weight`),
  KEY `idx_link_term_weight` (`link_id`,`term_id`,`weight`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_taxonomy`
--

DROP TABLE IF EXISTS `#__finder_taxonomy`;
CREATE TABLE IF NOT EXISTS `#__finder_taxonomy` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL,
  `state` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `access` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ordering` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `parent_id` (`parent_id`),
  KEY `state` (`state`),
  KEY `ordering` (`ordering`),
  KEY `access` (`access`),
  KEY `idx_parent_published` (`parent_id`,`state`,`access`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__finder_taxonomy`
--

INSERT INTO `#__finder_taxonomy` (`id`, `parent_id`, `title`, `state`, `access`, `ordering`) VALUES
(1, 0, 'ROOT', 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_taxonomy_map`
--

DROP TABLE IF EXISTS `#__finder_taxonomy_map`;
CREATE TABLE IF NOT EXISTS `#__finder_taxonomy_map` (
  `link_id` int(10) unsigned NOT NULL,
  `node_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`link_id`,`node_id`),
  KEY `link_id` (`link_id`),
  KEY `node_id` (`node_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_terms`
--

DROP TABLE IF EXISTS `#__finder_terms`;
CREATE TABLE IF NOT EXISTS `#__finder_terms` (
  `term_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '0',
  `soundex` varchar(75) NOT NULL,
  `links` int(10) NOT NULL DEFAULT '0',
  `language` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`term_id`),
  UNIQUE KEY `idx_term` (`term`),
  KEY `idx_term_phrase` (`term`,`phrase`),
  KEY `idx_stem_phrase` (`stem`,`phrase`),
  KEY `idx_soundex_phrase` (`soundex`,`phrase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_terms_common`
--

DROP TABLE IF EXISTS `#__finder_terms_common`;
CREATE TABLE IF NOT EXISTS `#__finder_terms_common` (
  `term` varchar(75) NOT NULL,
  `language` varchar(3) NOT NULL,
  KEY `idx_word_lang` (`term`,`language`),
  KEY `idx_lang` (`language`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__finder_terms_common`
--

INSERT INTO `#__finder_terms_common` (`term`, `language`) VALUES
('a', 'en'),
('about', 'en'),
('after', 'en'),
('ago', 'en'),
('all', 'en'),
('am', 'en'),
('an', 'en'),
('and', 'en'),
('ani', 'en'),
('any', 'en'),
('are', 'en'),
('aren''t', 'en'),
('as', 'en'),
('at', 'en'),
('be', 'en'),
('but', 'en'),
('by', 'en'),
('for', 'en'),
('from', 'en'),
('get', 'en'),
('go', 'en'),
('how', 'en'),
('if', 'en'),
('in', 'en'),
('into', 'en'),
('is', 'en'),
('isn''t', 'en'),
('it', 'en'),
('its', 'en'),
('me', 'en'),
('more', 'en'),
('most', 'en'),
('must', 'en'),
('my', 'en'),
('new', 'en'),
('no', 'en'),
('none', 'en'),
('not', 'en'),
('noth', 'en'),
('nothing', 'en'),
('of', 'en'),
('off', 'en'),
('often', 'en'),
('old', 'en'),
('on', 'en'),
('onc', 'en'),
('once', 'en'),
('onli', 'en'),
('only', 'en'),
('or', 'en'),
('other', 'en'),
('our', 'en'),
('ours', 'en'),
('out', 'en'),
('over', 'en'),
('page', 'en'),
('she', 'en'),
('should', 'en'),
('small', 'en'),
('so', 'en'),
('some', 'en'),
('than', 'en'),
('thank', 'en'),
('that', 'en'),
('the', 'en'),
('their', 'en'),
('theirs', 'en'),
('them', 'en'),
('then', 'en'),
('there', 'en'),
('these', 'en'),
('they', 'en'),
('this', 'en'),
('those', 'en'),
('thus', 'en'),
('time', 'en'),
('times', 'en'),
('to', 'en'),
('too', 'en'),
('true', 'en'),
('under', 'en'),
('until', 'en'),
('up', 'en'),
('upon', 'en'),
('use', 'en'),
('user', 'en'),
('users', 'en'),
('veri', 'en'),
('version', 'en'),
('very', 'en'),
('via', 'en'),
('want', 'en'),
('was', 'en'),
('way', 'en'),
('were', 'en'),
('what', 'en'),
('when', 'en'),
('where', 'en'),
('whi', 'en'),
('which', 'en'),
('who', 'en'),
('whom', 'en'),
('whose', 'en'),
('why', 'en'),
('wide', 'en'),
('will', 'en'),
('with', 'en'),
('within', 'en'),
('without', 'en'),
('would', 'en'),
('yes', 'en'),
('yet', 'en'),
('you', 'en'),
('your', 'en'),
('yours', 'en');

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_tokens`
--

DROP TABLE IF EXISTS `#__finder_tokens`;
CREATE TABLE IF NOT EXISTS `#__finder_tokens` (
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `weight` float unsigned NOT NULL DEFAULT '1',
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `language` char(3) NOT NULL DEFAULT '',
  KEY `idx_word` (`term`),
  KEY `idx_context` (`context`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_tokens_aggregate`
--

DROP TABLE IF EXISTS `#__finder_tokens_aggregate`;
CREATE TABLE IF NOT EXISTS `#__finder_tokens_aggregate` (
  `term_id` int(10) unsigned NOT NULL,
  `map_suffix` char(1) NOT NULL,
  `term` varchar(75) NOT NULL,
  `stem` varchar(75) NOT NULL,
  `common` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `phrase` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `term_weight` float unsigned NOT NULL,
  `context` tinyint(1) unsigned NOT NULL DEFAULT '2',
  `context_weight` float unsigned NOT NULL,
  `total_weight` float unsigned NOT NULL,
  `language` char(3) NOT NULL DEFAULT '',
  KEY `token` (`term`),
  KEY `keyword_id` (`term_id`)
) ENGINE=MEMORY DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__finder_types`
--

DROP TABLE IF EXISTS `#__finder_types`;
CREATE TABLE IF NOT EXISTS `#__finder_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `mime` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__languages`
--

DROP TABLE IF EXISTS `#__languages`;
CREATE TABLE IF NOT EXISTS `#__languages` (
  `lang_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `lang_code` char(7) NOT NULL,
  `title` varchar(50) NOT NULL,
  `title_native` varchar(50) NOT NULL,
  `sef` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `description` varchar(512) NOT NULL,
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `sitename` varchar(1024) NOT NULL DEFAULT '',
  `published` int(11) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `ordering` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`lang_id`),
  UNIQUE KEY `idx_sef` (`sef`),
  UNIQUE KEY `idx_image` (`image`),
  UNIQUE KEY `idx_langcode` (`lang_code`),
  KEY `idx_access` (`access`),
  KEY `idx_ordering` (`ordering`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__languages`
--

INSERT INTO `#__languages` (`lang_id`, `lang_code`, `title`, `title_native`, `sef`, `image`, `description`, `metakey`, `metadesc`, `sitename`, `published`, `access`, `ordering`) VALUES
(1, 'en-GB', 'English (UK)', 'English (UK)', 'en', 'en', '', '', '', '', 1, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__menu`
--

DROP TABLE IF EXISTS `#__menu`;
CREATE TABLE IF NOT EXISTS `#__menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL COMMENT 'The type of menu this item belongs to. FK to #__menu_types.menutype',
  `title` varchar(255) NOT NULL COMMENT 'The display title of the menu item.',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT 'The SEF alias of the menu item.',
  `note` varchar(255) NOT NULL DEFAULT '',
  `path` varchar(1024) NOT NULL COMMENT 'The computed path of the menu item based on the alias field.',
  `link` varchar(1024) NOT NULL COMMENT 'The actually link the menu item refers to.',
  `type` varchar(16) NOT NULL COMMENT 'The type of link: Component, URL, Alias, Separator',
  `published` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The published state of the menu link.',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '1' COMMENT 'The parent menu item in the menu tree.',
  `level` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The relative level in the tree.',
  `component_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__extensions.id',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to #__users.id',
  `checked_out_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'The time the menu item was checked out.',
  `browserNav` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'The click behaviour of the link.',
  `access` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'The access level required to view the menu item.',
  `img` varchar(255) NOT NULL COMMENT 'The image of the menu item.',
  `template_style_id` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL COMMENT 'JSON encoded data for the menu item.',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `home` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Indicates if this menu item is the home or default page.',
  `language` char(7) NOT NULL DEFAULT '',
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_client_id_parent_id_alias_language` (`client_id`,`parent_id`,`alias`,`language`),
  KEY `idx_componentid` (`component_id`,`menutype`,`published`,`access`),
  KEY `idx_menutype` (`menutype`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_path` (`path`(255)),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=196 ;

--
-- Dumping data for table `#__menu`
--

INSERT INTO `#__menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(1, '', 'Menu_Item_Root', 'root', '', '', '', '', 1, 0, 0, 0, 0, '0000-00-00 00:00:00', 0, 0, '', 0, '', 0, 207, 0, '*', 0),
(2, 'menu', 'com_banners', 'Banners', '', 'Banners', 'index.php?option=com_banners', 'component', 0, 1, 1, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 1, 10, 0, '*', 1),
(3, 'menu', 'com_banners', 'Banners', '', 'Banners/Banners', 'index.php?option=com_banners', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners', 0, '', 2, 3, 0, '*', 1),
(4, 'menu', 'com_banners_categories', 'Categories', '', 'Banners/Categories', 'index.php?option=com_categories&extension=com_banners', 'component', 0, 2, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-cat', 0, '', 4, 5, 0, '*', 1),
(5, 'menu', 'com_banners_clients', 'Clients', '', 'Banners/Clients', 'index.php?option=com_banners&view=clients', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-clients', 0, '', 6, 7, 0, '*', 1),
(6, 'menu', 'com_banners_tracks', 'Tracks', '', 'Banners/Tracks', 'index.php?option=com_banners&view=tracks', 'component', 0, 2, 2, 4, 0, '0000-00-00 00:00:00', 0, 0, 'class:banners-tracks', 0, '', 8, 9, 0, '*', 1),
(7, 'menu', 'com_contact', 'Contacts', '', 'Contacts', 'index.php?option=com_contact', 'component', 0, 1, 1, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 143, 148, 0, '*', 1),
(8, 'menu', 'com_contact', 'Contacts', '', 'Contacts/Contacts', 'index.php?option=com_contact', 'component', 0, 7, 2, 8, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact', 0, '', 144, 145, 0, '*', 1),
(9, 'menu', 'com_contact_categories', 'Categories', '', 'Contacts/Categories', 'index.php?option=com_categories&extension=com_contact', 'component', 0, 7, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:contact-cat', 0, '', 146, 147, 0, '*', 1),
(10, 'menu', 'com_messages', 'Messaging', '', 'Messaging', 'index.php?option=com_messages', 'component', 0, 1, 1, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages', 0, '', 149, 154, 0, '*', 1),
(11, 'menu', 'com_messages_add', 'New Private Message', '', 'Messaging/New Private Message', 'index.php?option=com_messages&task=message.add', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-add', 0, '', 150, 151, 0, '*', 1),
(12, 'menu', 'com_messages_read', 'Read Private Message', '', 'Messaging/Read Private Message', 'index.php?option=com_messages', 'component', 0, 10, 2, 15, 0, '0000-00-00 00:00:00', 0, 0, 'class:messages-read', 0, '', 152, 153, 0, '*', 1),
(13, 'menu', 'com_newsfeeds', 'News Feeds', '', 'News Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 1, 1, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 155, 160, 0, '*', 1),
(14, 'menu', 'com_newsfeeds_feeds', 'Feeds', '', 'News Feeds/Feeds', 'index.php?option=com_newsfeeds', 'component', 0, 13, 2, 17, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds', 0, '', 156, 157, 0, '*', 1),
(15, 'menu', 'com_newsfeeds_categories', 'Categories', '', 'News Feeds/Categories', 'index.php?option=com_categories&extension=com_newsfeeds', 'component', 0, 13, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:newsfeeds-cat', 0, '', 158, 159, 0, '*', 1),
(16, 'menu', 'com_redirect', 'Redirect', '', 'Redirect', 'index.php?option=com_redirect', 'component', 0, 1, 1, 24, 0, '0000-00-00 00:00:00', 0, 0, 'class:redirect', 0, '', 175, 176, 0, '*', 1),
(17, 'menu', 'com_search', 'Basic Search', '', 'Basic Search', 'index.php?option=com_search', 'component', 0, 1, 1, 19, 0, '0000-00-00 00:00:00', 0, 0, 'class:search', 0, '', 163, 164, 0, '*', 1),
(18, 'menu', 'com_weblinks', 'Weblinks', '', 'Weblinks', 'index.php?option=com_weblinks', 'component', 0, 1, 1, 21, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 165, 170, 0, '*', 1),
(19, 'menu', 'com_weblinks_links', 'Links', '', 'Weblinks/Links', 'index.php?option=com_weblinks', 'component', 0, 18, 2, 21, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks', 0, '', 166, 167, 0, '*', 1),
(20, 'menu', 'com_weblinks_categories', 'Categories', '', 'Weblinks/Categories', 'index.php?option=com_categories&extension=com_weblinks', 'component', 0, 18, 2, 6, 0, '0000-00-00 00:00:00', 0, 0, 'class:weblinks-cat', 0, '', 168, 169, 0, '*', 1),
(21, 'menu', 'com_finder', 'Smart Search', '', 'Smart Search', 'index.php?option=com_finder', 'component', 0, 1, 1, 27, 0, '0000-00-00 00:00:00', 0, 0, 'class:finder', 0, '', 161, 162, 0, '*', 1),
(22, 'menu', 'com_joomlaupdate', 'Joomla! Update', '', 'Joomla! Update', 'index.php?option=com_joomlaupdate', 'component', 0, 1, 1, 28, 0, '0000-00-00 00:00:00', 0, 0, 'class:joomlaupdate', 0, '', 171, 172, 0, '*', 1),
(23, 'main', 'com_tags', 'Tags', '', 'Tags', 'index.php?option=com_tags', 'component', 0, 1, 1, 29, 0, '0000-00-00 00:00:00', 0, 1, 'class:tags', 0, '', 173, 174, 0, '', 1),
(101, 'mainmenu', 'Home', 'home', '', 'home', 'index.php?option=com_content&view=featured', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"featured_categories":[""],"layout_type":"blog","num_leading_articles":"0","num_intro_articles":"6","num_columns":"1","num_links":"0","multi_column_order":"1","orderby_pri":"","orderby_sec":"rdate","order_date":"","show_pagination":"2","show_pagination_results":"1","show_title":"use_article","link_titles":"1","show_intro":"","info_block_position":"","show_category":"0","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"0","link_author":"","show_create_date":"use_article","show_modify_date":"0","show_publish_date":"0","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"0","show_icons":"","show_print_icon":"0","show_email_icon":"0","show_hits":"0","show_noauth":"","show_feed_link":"1","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"Welcome to the Frontpage!","show_page_heading":0,"page_heading":"Featured Articles","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Return Home","s5_group_child":"0"}', 11, 12, 1, '*', 0),
(102, 'mainmenu', 'Responsive', 'responsive', '', 'responsive', 'index.php?Itemid=', 'alias', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"129","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Responsive Layout","s5_group_child":"0"}', 13, 14, 0, '*', 0),
(108, 'mainmenu', 'S5 Flex Menu', 's5-flex-menu', '', 's5-flex-menu', 'index.php?option=com_content&view=article&id=21', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"2","s5_subtext":"Advanced Menu System","s5_group_child":"0"}', 15, 48, 0, '*', 0),
(109, 'mainmenu', 'Drop Down Menu', '2013-05-12-04-01-46', '', 's5-flex-menu/2013-05-12-04-01-46', 'javascript:;', 'url', 1, 108, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"images\\/stories\\/application_put.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Unlimited Level Options","s5_group_child":"0"}', 16, 29, 0, '*', 0),
(110, 'mainmenu', 'Dummy Item', '2013-05-12-04-02-36', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-36', 'javascript:;', 'url', 1, 109, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 17, 18, 0, '*', 0),
(111, 'mainmenu', 'Dummy Item ', '2013-05-12-04-02-37', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-37', 'javascript:;', 'url', 1, 109, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 19, 20, 0, '*', 0),
(112, 'mainmenu', 'Dummy Item ', '2013-05-12-04-02-38', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-38', 'javascript:;', 'url', 1, 109, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 21, 28, 0, '*', 0),
(113, 'mainmenu', 'Dummy Item', '2013-05-12-04-03-33', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-38/2013-05-12-04-03-33', 'javascript:;', 'url', 1, 112, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 22, 23, 0, '*', 0),
(114, 'mainmenu', 'Dummy Item ', '2013-05-12-04-03-34', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-38/2013-05-12-04-03-34', 'javascript:;', 'url', 1, 112, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 24, 25, 0, '*', 0),
(115, 'mainmenu', 'Dummy Item ', '2013-05-12-04-03-35', '', 's5-flex-menu/2013-05-12-04-01-46/2013-05-12-04-02-38/2013-05-12-04-03-35', 'javascript:;', 'url', 1, 112, 4, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 26, 27, 0, '*', 0),
(116, 'mainmenu', 'Menu Module Example', 'menu-module-example', '', 's5-flex-menu/menu-module-example', '', 'separator', 1, 108, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"images\\/stories\\/cog_edit.png","menu_text":1,"s5_load_mod":"1","s5_position":"s5_menu1","s5_columns":"1","s5_subtext":"Publish Any Module to Any Menu","s5_group_child":"1"}', 30, 31, 0, '*', 0),
(117, 'mainmenu', 'Grouped Child Menu', 'grouped-child-menu', '', 's5-flex-menu/grouped-child-menu', '', 'separator', 1, 108, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"images\\/stories\\/application_side_boxes.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Group Children Together","s5_group_child":"1"}', 32, 45, 0, '*', 0),
(118, 'mainmenu', 'Dummy Sample Link', 'dummy-sample-link', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 33, 34, 0, '*', 0),
(119, 'mainmenu', 'Dummy Sample Link ', 'dummy-sample-link-2', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link-2', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 35, 36, 0, '*', 0),
(120, 'mainmenu', 'Dummy Sample Link ', 'dummy-sample-link-3', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link-3', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 37, 38, 0, '*', 0),
(121, 'mainmenu', 'Dummy Sample Link  ', 'dummy-sample-link-4', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link-4', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 39, 40, 0, '*', 0),
(122, 'mainmenu', 'Dummy Sample Link  ', 'dummy-sample-link-5', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link-5', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 41, 42, 0, '*', 0),
(123, 'mainmenu', 'Dummy Sample Link', 'dummy-sample-link-6', '', 's5-flex-menu/grouped-child-menu/dummy-sample-link-6', '', 'separator', 1, 117, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 43, 44, 0, '*', 0),
(124, 'mainmenu', 'Menu With No Menu Icon', 'menu-with-no-menu-icon', '', 's5-flex-menu/menu-with-no-menu-icon', '', 'separator', 1, 108, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Standard Sub Menu Link","s5_group_child":"0"}', 46, 47, 0, '*', 0),
(125, 'mainmenu', 'Features', 'features-mainmenu-47', '', 'features-mainmenu-47', '', 'separator', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"2","s5_subtext":"What''s Included","s5_group_child":"0"}', 49, 126, 0, '*', 0),
(126, 'mainmenu', 'S5 Vertex Framework', '2013-05-12-04-20-05', '', 'features-mainmenu-47/2013-05-12-04-20-05', 'http://www.shape5.com/joomla/framework/vertex_framework.html', 'url', 1, 125, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"images\\/stories\\/application_link.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Learn More About Vertex","s5_group_child":"0"}', 50, 51, 0, '*', 0),
(127, 'mainmenu', 'Template Specific Features', 'template-specific-features', '', 'features-mainmenu-47/template-specific-features', 'index.php?option=com_content&view=article&id=33', 'component', 1, 125, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"images\\/stories\\/color_wheel.png","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Options For This Template","s5_group_child":"0"}', 52, 53, 0, '*', 0),
(128, 'mainmenu', 'Vertex Template Features', 'template-features', '', 'features-mainmenu-47/template-features', '', 'separator', 1, 125, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"images\\/stories\\/application_split.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Powerful Template Settings","s5_group_child":"1"}', 54, 81, 0, '*', 0),
(129, 'mainmenu', 'Responsive Layout', 'responsive-layout', '', 'features-mainmenu-47/template-features/responsive-layout', 'index.php?option=com_content&view=article&id=18', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Adapt to Any Screen Size","s5_group_child":"1"}', 55, 56, 0, '*', 0),
(130, 'mainmenu', 'Hide Content and Modules', 'hide-content-and-modules', '', 'features-mainmenu-47/template-features/hide-content-and-modules', 'index.php?option=com_content&view=article&id=7', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 57, 58, 0, '*', 0),
(131, 'mainmenu', 'Info Slide Enabled', 'info-slide-enabled', '', 'features-mainmenu-47/template-features/info-slide-enabled', 'index.php?option=com_content&view=article&id=10', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 59, 60, 0, '*', 0),
(132, 'mainmenu', '92 Module Positions', '92-module-positions', '', 'features-mainmenu-47/template-features/92-module-positions', 'index.php?option=com_content&view=article&id=15', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Thousands of Layout Options","s5_group_child":"0"}', 61, 62, 0, '*', 0),
(133, 'mainmenu', 'SEO Optimized', 'seo-optimized', '', 'features-mainmenu-47/template-features/seo-optimized', 'index.php?option=com_content&view=article&id=30', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 63, 64, 0, '*', 0),
(134, 'mainmenu', 'Mobile Device Ready', 'mobile-device-ready', '', 'features-mainmenu-47/template-features/mobile-device-ready', 'index.php?option=com_content&view=article&id=18', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 65, 66, 0, '*', 0),
(135, 'mainmenu', 'Tool Tips Enabled', 'tool-tips-enabled', '', 'features-mainmenu-47/template-features/tool-tips-enabled', 'index.php?option=com_content&view=article&id=36', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 67, 68, 0, '*', 0),
(136, 'mainmenu', 'Multibox Enabled', 'multibox-enabled', '', 'features-mainmenu-47/template-features/multibox-enabled', 'index.php?option=com_content&view=article&id=16', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 69, 70, 0, '*', 0),
(137, 'mainmenu', 'Lazy Load Enabled', 'lazy-load-enabled', '', 'features-mainmenu-47/template-features/lazy-load-enabled', 'index.php?option=com_content&view=article&id=12', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 71, 72, 0, '*', 0),
(138, 'mainmenu', 'Menu Scroll To', 'menu-scroll-to', '', 'features-mainmenu-47/template-features/menu-scroll-to', 'index.php?option=com_content&view=article&id=14', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 73, 74, 0, '*', 0),
(139, 'mainmenu', 'Hide Article Component Area', 'hide-article-component-area', '', 'features-mainmenu-47/template-features/hide-article-component-area', 'index.php?option=com_content&view=article&id=6', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 75, 76, 0, '*', 0),
(140, 'mainmenu', 'Drop Down Panel', 'drop-down-panel', '', 'features-mainmenu-47/template-features/drop-down-panel', 'index.php?option=com_content&view=article&id=20', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 77, 78, 0, '*', 0),
(141, 'mainmenu', 'J! Stuff', 'joomla-stuff-mainmenu-26', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26', '', 'separator', 1, 125, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"images\\/stories\\/joomla.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Default Joomla Items","s5_group_child":"0"}', 82, 95, 0, '*', 0),
(142, 'mainmenu', 'Search', 'search', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/search', 'index.php?option=com_search&view=search&searchword=', 'component', 1, 141, 3, 19, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"search_areas":"","show_date":"","searchphrase":"0","ordering":"newest","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 83, 84, 0, '*', 0),
(143, 'mainmenu', 'News', 'news', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/news', 'index.php?option=com_content&view=category&layout=blog&id=9', 'component', 1, 141, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"0","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"1","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"1","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"1","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 85, 86, 0, '*', 0),
(144, 'mainmenu', 'News Feeds', 'news-feeds', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/news-feeds', 'index.php?option=com_newsfeeds&view=categories&id=0', 'component', 1, 141, 3, 17, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_base_description":"","categories_description":"","maxLevelcat":"","show_empty_categories_cat":"","show_subcat_desc_cat":"","show_cat_items_cat":"","show_category_title":"","show_description":"1","show_description_image":"","maxLevel":"","show_empty_categories":"","show_subcat_desc":"","show_cat_items":"","show_pagination_limit":"","show_headings":"1","show_articles":"","show_link":"","show_pagination":"","show_pagination_results":"","show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 87, 88, 0, '*', 0),
(145, 'mainmenu', 'Links', 'links', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/links', 'index.php?option=com_weblinks&view=categories&id=0', 'component', 1, 141, 3, 21, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_base_description":"","categories_description":"","maxLevelcat":"-1","show_empty_categories_cat":"","show_subcat_desc_cat":"","show_cat_num_links_cat":"","show_category_title":"","show_description":"1","show_description_image":"","maxLevel":"","show_empty_categories":"","show_subcat_desc":"","show_cat_num_links":"","show_pagination_limit":"","show_headings":"1","show_link_description":"","show_link_hits":"","show_pagination":"","show_pagination_results":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 89, 90, 0, '*', 0),
(146, 'mainmenu', 'Contact Us', 'contact-us', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/contact-us', 'index.php?option=com_contact&view=contact&id=1', 'component', 1, 141, 3, 8, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"presentation_style":"plain","show_contact_category":"","show_contact_list":"0","show_name":"","show_position":"","show_email":"","show_street_address":"","show_suburb":"","show_state":"","show_postcode":"","show_country":"","show_telephone":"","show_mobile":"","show_fax":"","show_webpage":"","show_misc":"","show_image":"","allow_vcard":"","show_articles":"","show_links":"","linka_name":"","linkb_name":"","linkc_name":"","linkd_name":"","linke_name":"","show_email_form":"","show_email_copy":"","banned_email":"","banned_subject":"","banned_text":"","validate_session":"","custom_reply":"","redirect":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 91, 92, 0, '*', 0),
(147, 'mainmenu', 'Wrapper', 'wrapper', '', 'features-mainmenu-47/joomla-stuff-mainmenu-26/wrapper', 'index.php?option=com_wrapper&view=wrapper', 'component', 1, 141, 3, 2, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"url":"http:\\/\\/getfirebug.com\\/","scrolling":"auto","width":"100%","height":"500","height_auto":"0","add_scheme":"1","frameborder":"1","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 93, 94, 0, '*', 0),
(148, 'mainmenu', 'Joomla and Vertex Tutorials', '2013-05-12-04-44-38', '', 'features-mainmenu-47/2013-05-12-04-44-38', 'http://www.shape5.com/joomla_tutorials.html', 'url', 1, 125, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"images\\/stories\\/help.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Vertex and Joomla Tutorials","s5_group_child":"0"}', 96, 97, 0, '*', 0),
(149, 'mainmenu', 'Continued Vertex Features', 'continued-vertex-features', '', 'features-mainmenu-47/continued-vertex-features', '', 'separator', 1, 125, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"images\\/stories\\/application_view_tile.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"More Great S5 Vertex Options","s5_group_child":"1"}', 98, 125, 0, '*', 0),
(150, 'mainmenu', 'File Compression', 'file-compression', '', 'features-mainmenu-47/continued-vertex-features/file-compression', 'index.php?option=com_content&view=article&id=2', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 99, 100, 0, '*', 0),
(151, 'mainmenu', 'Stock Photography', 'stock-photography', '', 'features-mainmenu-47/continued-vertex-features/stock-photography', 'index.php?option=com_content&view=article&id=32', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 101, 102, 0, '*', 0),
(152, 'mainmenu', 'Site Shaper Available', 'site-shaper-available', '', 'features-mainmenu-47/continued-vertex-features/site-shaper-available', 'index.php?option=com_content&view=article&id=31', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 103, 104, 0, '*', 0),
(153, 'mainmenu', 'Google Fonts Enabled', 'google-fonts-enabled', '', 'features-mainmenu-47/continued-vertex-features/google-fonts-enabled', 'index.php?option=com_content&view=article&id=5', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 105, 106, 0, '*', 0),
(154, 'mainmenu', 'Page, Row and Column Widths', 'page-row-and-column-widths', '', 'features-mainmenu-47/continued-vertex-features/page-row-and-column-widths', 'index.php?option=com_content&view=article&id=17', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 107, 108, 0, '*', 0),
(155, 'mainmenu', 'Typography', 'typography', '', 'features-mainmenu-47/continued-vertex-features/typography', 'index.php?option=com_content&view=article&id=37', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 109, 110, 0, '*', 0),
(157, 'mainmenu', 'Fluid and Fixed Layouts', 'fluid-and-fixed-layouts', '', 'features-mainmenu-47/continued-vertex-features/fluid-and-fixed-layouts', 'index.php?option=com_content&view=article&id=17', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 111, 112, 0, '*', 0),
(158, 'mainmenu', 'Fixed Side Tabs', 'fixed-side-tabs', '', 'features-mainmenu-47/continued-vertex-features/fixed-side-tabs', 'index.php?option=com_content&view=article&id=4', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 113, 114, 0, '*', 0),
(159, 'mainmenu', 'IE7 and 8 CSS3 Support', 'ie7-and-8-css3-support', '', 'features-mainmenu-47/continued-vertex-features/ie7-and-8-css3-support', 'index.php?option=com_content&view=article&id=9', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 115, 116, 0, '*', 0),
(160, 'mainmenu', '3rd Party Component Compatible', '3rd-party-component-compatible', '', 'features-mainmenu-47/continued-vertex-features/3rd-party-component-compatible', 'index.php?option=com_content&view=article&id=1', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 117, 118, 0, '*', 0),
(161, 'mainmenu', 'LTR Language', '2013-05-12-04-53-04', '', 'features-mainmenu-47/continued-vertex-features/2013-05-12-04-53-04', '?lang=ltr', 'url', 1, 149, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 121, 122, 0, '*', 0),
(162, 'mainmenu', 'RTL Language', '2013-05-12-04-54-31', '', 'features-mainmenu-47/continued-vertex-features/2013-05-12-04-54-31', '?lang=rtl', 'url', 1, 149, 3, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 123, 124, 0, '*', 0),
(163, 'mainmenu', 'Tutorials', 'tutorials-menu-48', '', 'tutorials-menu-48', '', 'separator', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Find Help Here","s5_group_child":"0"}', 127, 142, 0, '*', 0),
(164, 'mainmenu', 'Joomla and Vertex Tutorials', '2013-05-12-04-56-04', '', 'tutorials-menu-48/2013-05-12-04-56-04', 'http://www.shape5.com/joomla_tutorials.html', 'url', 1, 163, 2, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"menu-anchor_title":"","menu-anchor_css":"","menu_image":"images\\/stories\\/help.png","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Vertex and Joomla Tutorials","s5_group_child":"0"}', 128, 129, 0, '*', 0),
(165, 'mainmenu', 'Site Shaper Setup', 'site-shaper-setup', '', 'tutorials-menu-48/site-shaper-setup', 'index.php?option=com_content&view=article&id=31', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"Site Shapers Are Highly Recommended","s5_group_child":"0"}', 130, 131, 0, '*', 0),
(166, 'mainmenu', 'Installing The Template', 'installing-the-template', '', 'tutorials-menu-48/installing-the-template', 'index.php?option=com_content&view=article&id=11', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 132, 133, 0, '*', 0),
(167, 'mainmenu', 'Setting Up Module Styles', 'setting-up-module-styles', '', 'tutorials-menu-48/setting-up-module-styles', 'index.php?option=com_content&view=article&id=15', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 134, 135, 0, '*', 0),
(168, 'mainmenu', 'Search and Menus Setup', 'search-and-menus-setup', '', 'tutorials-menu-48/search-and-menus-setup', 'index.php?option=com_content&view=article&id=8', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 138, 139, 0, '*', 0);
INSERT INTO `#__menu` (`id`, `menutype`, `title`, `alias`, `note`, `path`, `link`, `type`, `published`, `parent_id`, `level`, `component_id`, `checked_out`, `checked_out_time`, `browserNav`, `access`, `img`, `template_style_id`, `params`, `lft`, `rgt`, `home`, `language`, `client_id`) VALUES
(169, 'mainmenu', 'Configuring The Template', 'configuring-the-template', '', 'tutorials-menu-48/configuring-the-template', 'index.php?option=com_content&view=article&id=35', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 136, 137, 0, '*', 0),
(170, 'mainmenu', 'Login and Register Setup', 'login-and-register-setup', '', 'tutorials-menu-48/login-and-register-setup', 'index.php?option=com_content&view=article&id=13', 'component', 1, 163, 2, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 140, 141, 0, '*', 0),
(171, 'bottom-menu', 'Home', '2013-05-12-19-29-07', '', '2013-05-12-19-29-07', 'index.php?Itemid=', 'alias', 1, 1, 1, 0, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"aliasoptions":"101","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 177, 178, 0, '*', 0),
(172, 'bottom-menu', 'About Us', 'about-us', '', 'about-us', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 179, 180, 0, '*', 0),
(173, 'bottom-menu', 'News', 'news', '', 'news', 'index.php?option=com_content&view=category&layout=blog&id=11', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 181, 182, 0, '*', 0),
(174, 'bottom-menu', 'Featured', 'featured', '', 'featured', 'index.php?option=com_content&view=category&layout=blog&id=8', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"layout_type":"blog","show_category_heading_title_text":"","show_category_title":"","show_description":"","show_description_image":"","maxLevel":"","show_empty_categories":"","show_no_articles":"","show_subcat_desc":"","show_cat_num_articles":"","page_subheading":"","num_leading_articles":"","num_intro_articles":"","num_columns":"","num_links":"","multi_column_order":"","show_subcategory_content":"","orderby_pri":"","orderby_sec":"","order_date":"","show_pagination":"","show_pagination_results":"","show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_readmore":"","show_readmore_title":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","show_feed_link":"","feed_summary":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 183, 184, 0, '*', 0),
(175, 'bottom-menu', 'Site Terms', 'site-terms', '', 'site-terms', 'index.php?option=com_content&view=article&id=24', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 185, 186, 0, '*', 0),
(181, 'quick-menu', 'Our Privacy Policy', 'our-privacy-policy', '', 'our-privacy-policy', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 187, 188, 0, '*', 0),
(182, 'quick-menu', 'Address Information', 'address-information', '', 'address-information', 'index.php?option=com_content&view=article&id=27', 'component', 0, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 189, 190, 0, '*', 0),
(183, 'quick-menu', 'Network of People', 'network-of-people', '', 'network-of-people', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 191, 192, 0, '*', 0),
(184, 'quick-menu', 'Get To Know Us', 'get-to-know-us', '', 'get-to-know-us', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 193, 194, 0, '*', 0),
(185, 'quick-menu', 'Customer Satisfaction', 'customer-satisfaction', '', 'customer-satisfaction', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 195, 196, 0, '*', 0),
(186, 'quick-menu', 'Non-Profit Project', 'non-profit-project', '', 'non-profit-project', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 197, 198, 0, '*', 0),
(187, 'quick-menu', 'How You Can Help', 'how-you-can-help', '', 'how-you-can-help', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 199, 200, 0, '*', 0),
(188, 'quick-menu', 'Truck Accessories', 'truck-accessories', '', 'truck-accessories', 'index.php?option=com_content&view=article&id=27', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 201, 202, 0, '*', 0),
(189, 'quick-menu', 'Off Road Equipment', 'off-road-equipment', '', 'off-road-equipment', 'index.php?option=com_content&view=article&id=27', 'component', -2, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 203, 204, 0, '*', 0),
(191, 'quick-menu', 'Find a Skate Park', 'find-a-skate-park', '', 'find-a-skate-park', 'index.php?option=com_content&view=article&id=27', 'component', 1, 1, 1, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 205, 206, 0, '*', 0),
(193, 'mainmenu', 'Parallax Backgrounds', 'parallax-backgrounds', '', 'features-mainmenu-47/template-features/parallax-backgrounds', 'index.php?option=com_content&view=article&id=45', 'component', 1, 128, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 79, 80, 0, '*', 0),
(194, 'mainmenu', 'Floating Menu', 'floating-menu', '', 'features-mainmenu-47/continued-vertex-features/floating-menu', 'index.php?option=com_content&view=article&id=44', 'component', 1, 149, 3, 22, 0, '0000-00-00 00:00:00', 0, 1, '', 0, '{"show_title":"","link_titles":"","show_intro":"","info_block_position":"","show_category":"","link_category":"","show_parent_category":"","link_parent_category":"","show_author":"","link_author":"","show_create_date":"","show_modify_date":"","show_publish_date":"","show_item_navigation":"","show_vote":"","show_tags":"","show_icons":"","show_print_icon":"","show_email_icon":"","show_hits":"","show_noauth":"","urls_position":"","menu-anchor_title":"","menu-anchor_css":"","menu_image":"","menu_text":1,"page_title":"","show_page_heading":0,"page_heading":"","pageclass_sfx":"","menu-meta_description":"","menu-meta_keywords":"","robots":"","secure":0,"s5_load_mod":"0","s5_columns":"1","s5_subtext":"","s5_group_child":"0"}', 119, 120, 0, '*', 0),
(195, 'main', 'com_postinstall', 'Post-installation messages', '', 'Post-installation messages', 'index.php?option=com_postinstall', 'component', 0, 1, 1, 32, 0, '0000-00-00 00:00:00', 0, 1, 'class:postinstall', 0, '', 45, 46, 0, '*', 1);

-- --------------------------------------------------------

--
-- Table structure for table `#__menu_types`
--

DROP TABLE IF EXISTS `#__menu_types`;
CREATE TABLE IF NOT EXISTS `#__menu_types` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menutype` varchar(24) NOT NULL,
  `title` varchar(48) NOT NULL,
  `description` varchar(255) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_menutype` (`menutype`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `#__menu_types`
--

INSERT INTO `#__menu_types` (`id`, `menutype`, `title`, `description`) VALUES
(1, 'mainmenu', 'Main Menu', 'The main menu for the site'),
(2, 'bottom-menu', 'Bottom Menu', ''),
(4, 'quick-menu', 'Quick Menu', '');

-- --------------------------------------------------------

--
-- Table structure for table `#__messages`
--

DROP TABLE IF EXISTS `#__messages`;
CREATE TABLE IF NOT EXISTS `#__messages` (
  `message_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id_from` int(10) unsigned NOT NULL DEFAULT '0',
  `user_id_to` int(10) unsigned NOT NULL DEFAULT '0',
  `folder_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `date_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `priority` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(255) NOT NULL DEFAULT '',
  `message` text NOT NULL,
  PRIMARY KEY (`message_id`),
  KEY `useridto_state` (`user_id_to`,`state`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__messages_cfg`
--

DROP TABLE IF EXISTS `#__messages_cfg`;
CREATE TABLE IF NOT EXISTS `#__messages_cfg` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `cfg_name` varchar(100) NOT NULL DEFAULT '',
  `cfg_value` varchar(255) NOT NULL DEFAULT '',
  UNIQUE KEY `idx_user_var_name` (`user_id`,`cfg_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__modules`
--

DROP TABLE IF EXISTS `#__modules`;
CREATE TABLE IF NOT EXISTS `#__modules` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `asset_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'FK to the #__assets table.',
  `title` varchar(100) NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `content` text NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  `position` varchar(50) NOT NULL DEFAULT '',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `module` varchar(50) DEFAULT NULL,
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `showtitle` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `client_id` tinyint(4) NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `published` (`published`,`access`),
  KEY `newsfeeds` (`module`,`published`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=207 ;

--
-- Dumping data for table `#__modules`
--

INSERT INTO `#__modules` (`id`, `asset_id`, `title`, `note`, `content`, `ordering`, `position`, `checked_out`, `checked_out_time`, `publish_up`, `publish_down`, `published`, `module`, `access`, `showtitle`, `params`, `client_id`, `language`) VALUES
(2, 0, 'Login', '', '', 1, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '', 1, '*'),
(3, 0, 'Popular Articles', '', '', 3, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_popular', 3, 1, '{"count":"5","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(4, 0, 'Recently Added Articles', '', '', 4, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_latest', 3, 1, '{"count":"5","ordering":"c_dsc","catid":"","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(8, 0, 'Toolbar', '', '', 1, 'toolbar', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_toolbar', 3, 1, '', 1, '*'),
(9, 0, 'Quick Icons', '', '', 1, 'icon', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_quickicon', 3, 1, '', 1, '*'),
(10, 0, 'Logged-in Users', '', '', 2, 'cpanel', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_logged', 3, 1, '{"count":"5","name":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","automatic_title":"1"}', 1, '*'),
(12, 0, 'Admin Menu', '', '', 1, 'menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 3, 1, '{"layout":"","moduleclass_sfx":"","shownew":"1","showhelp":"1","cache":"0"}', 1, '*'),
(13, 0, 'Admin Submenu', '', '', 1, 'submenu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_submenu', 3, 1, '', 1, '*'),
(14, 0, 'User Status', '', '', 2, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_status', 3, 1, '', 1, '*'),
(15, 0, 'Title', '', '', 1, 'title', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_title', 3, 1, '', 1, '*'),
(79, 0, 'Multilanguage status', '', '', 1, 'status', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_multilangstatus', 3, 1, '{"layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(86, 0, 'Joomla Version', '', '', 1, 'footer', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_version', 3, 1, '{"format":"short","product":"1","layout":"_:default","moduleclass_sfx":"","cache":"0"}', 1, '*'),
(87, 0, 'Banner Position', '', '<p>This is the default banner position.</p>', 1, 'banner', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-style1","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(88, 0, 'Bottom Menu', '', '', 1, 'bottom_menu', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"bottom-menu","startLevel":"1","endLevel":"1","showAllChildren":"1","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(91, 0, 'Bottom Row1 Modules', '', 'This is an example of a module published to the bottom_row_1 row. This row contains 6 modules, read above for a full description.', 1, 'bottom_row1_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(92, 0, 'Bottom Row2 Modules', '', 'This is an example of a module published to the bottom_row_2 row. This row contains 6 modules, read above for a full description.', 1, 'bottom_row2_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(98, 0, 'Bottom Row3 Modules', '', 'This is an example of a module published to the bottom_row_3 row. This row contains 6 modules, read above for a full description.', 1, 'bottom_row3_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(99, 0, 'Latest News', '', '', 1, 'bottom_row3_2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_latest', 1, 1, '{"catid":["2","8","9","10"],"count":"7","show_featured":"0","ordering":"c_dsc","user_id":"0","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(100, 0, 'Quick Menu', '', '', 1, 'bottom_row3_3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 0, 'mod_menu', 1, 1, '{"menutype":"quick-menu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(101, 0, 'Demo Information', '', 'All content and images shown on this site is for demo, presentation purposes only. This site is intended to exemplify a live website and does not make any claim of any kind to the validity of non-Shape5 content, images or posts published. Most photography was purchased from <a href="http://www.shutterstock.com" target="_blank">shutterstock.com</a> for this demo only, and is not included with this template or any membership. You may not use these images for your own without purchase. Links will be provided for some images if you wish to purchase them from their copyright owners. ', 1, 'bottom_row3_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-transparent_dark","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(102, 0, 'Breadcrumbs', '', '', 1, 'breadcrumb', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_breadcrumbs', 1, 0, '{"showHere":"1","showHome":"1","homeText":"Home","showLast":"1","separator":"\\\\\\\\","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(105, 0, 'Sample Drop Down Module', '', '<p>This is an example of a module published to the drop_down row. This row contains 6 modules. To enable the drop down simple publish any module to any of the drop_down_x positions.</p>', 1, 'drop_down_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-transparent_dark","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(118, 0, 'Right', '', 'This is an example of a module published to the right position. There are also left, insets, rows, etc. positions and many others, be sure to read the full description. This is the default style that will appear for most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(124, 0, 'Right Inset', '', 'This is an example of a module published to the right_inset position. There is also a left_inset position and many others, be sure to read the full description.', 1, 'right_inset', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(125, 0, 'S5 Flex Menu Sample Module', '', '<div style="width: 182px; font-size: 0.85em;">\r\n<div style="float: left;"><img src="http://www.shape5.com/demo/images/multibox3.jpg" border="0" alt="" style="width: 72px;" /></div>\r\nThis is a sample module to showcase the functionality of the S5 Flex Menu system. This menu system contains up to 40 module positions and you can publish any module to any of these positions under any menu item.</div>', 0, 's5_menu1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(129, 0, 'Search', '', '', 0, 'search', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_search', 1, 1, '{"label":"","width":"50","text":"","button":"1","button_pos":"right","imagebutton":"","button_text":"Find","opensearch":"1","opensearch_title":"","set_itemid":"212","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid"}', 0, '*'),
(130, 0, 'Top Row1 Modules', '', 'This is an example of a module published to the top_row_1 row with the class -white applied. This row contains 6 modules, read below for a full description.', 1, 'top_row1_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-white","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(134, 0, 'Top Row2 Modules', '', 'This is an example of a module published to the top_row_2 row. This row contains 6 modules, read below for a full description.', 1, 'top_row2_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(135, 0, 'Top Row3 Modules', '', 'This is an example of a module published to the top_row_3 row. This row contains 6 modules, read below for a full description.', 1, 'top_row3_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static"}', 0, '*'),
(138, 0, 'Guests Online', '', '', 17, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_whosonline', 1, 1, '{"showmode":"0","layout":"_:default","moduleclass_sfx":"-dark","cache":"0","filter_groups":"0"}', 0, '*'),
(156, 0, 'Main Menu', '', '', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_menu', 1, 1, '{"menutype":"mainmenu","base":"","startLevel":"1","endLevel":"0","showAllChildren":"0","tag_id":"","class_sfx":"","window_open":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"itemid","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(159, 0, 'Banner', '', '', 1, 'banner', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_banners', 1, 0, '{"target":"1","count":"5","cid":"0","catid":["17"],"tag_search":"0","ordering":"0","header_text":"","footer_text":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900"}', 0, '*'),
(196, 0, 'Login', '', '', 0, 'login', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_login', 1, 1, '{"pretext":"","posttext":"","login":"","logout":"","greeting":"1","name":"0","usesecure":"0","usetext":"1","layout":"_:default","moduleclass_sfx":"","cache":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(197, 0, 'S5 Register', '', '', 1, 'register', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_s5_register', 1, 1, '{"moduleclass_sfx":"","captcha":"0","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(198, 0, 'Welcome to Design Control', '', '<div id="s5_welcome" style="text-align:center;padding-top:23px;padding-bottom:23px;">\r\n<h1 style="font-size:2.7em;line-height:170%;" class="uppercase_letters white_text">Welcome To Your Design Control Center</h1>\r\n<h3 style="font-size:1.7em;color:#666C81;line-height:190%;" class="uppercase_letters">Experience the amazing power of the Shape5 Vertex<br />Template Framework first hand</h3><br />\r\n<a href="index.php/features-mainmenu-47/template-specific-features" class="highlight1_button" style="margin-right:15px;margin-top:6px;">Read More</a>\r\n<a href="index.php/features-mainmenu-47/template-specific-features" class="highlight2_button" style="margin-left:15px;margin-top:6px;">Features</a>\r\n</div>', 1, 'top_row1_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(199, 0, 'Free Joomla Template', '', '<i style="float:left" class="icon-check icon_circle homepage_icon"></i>\r\n<div class="homepage_icon_text" style="overflow:hidden;padding-left:20px;padding-top:14px;padding-bottom:4px;">\r\n	<h2 style="margin-bottom:12px" class="uppercase_letters">Free Joomla Template</h2>\r\n	Design Control is a totally 100% free Joomla template! This template is built off of the very powerful Shape5 Vertex Template Framework. Some of the great features include: Responsive layout, Fixed or Fluid layouts, custom widths, over 90 module positions, S5 Flex Menu, integrated multibox and tooltips, google font integration, file compression, SEF optimized layout, RTL language support, and so much more! \r\n	<br /><br />\r\n	<a href="index.php/features-mainmenu-47/template-specific-features" class="readon_highlight1">Read More</a>\r\n</div>', 1, 'top_row2_1', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(200, 0, 'Flexible Solutions', '', '<i style="float:left" class="icon-cogs icon_circle homepage_icon"></i>\r\n<div class="homepage_icon_text" style="overflow:hidden;padding-left:20px;padding-top:14px;padding-bottom:4px;">\r\n	<h2 style="margin-bottom:12px" class="uppercase_letters">Flexible Solutions</h2>\r\n	Design Control comes loaded with many great template specific features in addition to the amazing default Vertex features. These features include: two custom highlight colors, shadow options for the top_row1 area, custom header background options, enabling or disabling uppercase letters, reducing the height of the flex menu buttons, and much more. Be sure to check out all these great features <a href="index.php/features-mainmenu-47/template-specific-features">here.</a>\r\n	<br /><br />\r\n	<a href="index.php/features-mainmenu-47/template-specific-features" class="readon_highlight1">Read More</a>\r\n</div>', 1, 'top_row2_2', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 0, '{"prepare_content":"0","backgroundimage":"","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(201, 0, '-highlight1', '', 'This is the -highlight1 module style and can be applied to most most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-highlight1","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(202, 0, '-highlight2', '', 'This is the -highlight2 module style and can be applied to most most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-highlight2","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(203, 0, '-grey', '', 'This is the -grey module style and can be applied to most most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-grey","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(204, 0, '-inset', '', 'This is the -inset module style and can be applied to most most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-inset","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(205, 0, '-dark', '', 'This is the -dark module style and can be applied to most most module positions.', 1, 'right', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_custom', 1, 1, '{"prepare_content":"1","backgroundimage":"","layout":"_:default","moduleclass_sfx":"-dark","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*'),
(206, 0, 'Popular Items', '', '', 0, 'bottom_row3_3', 0, '0000-00-00 00:00:00', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, 'mod_articles_popular', 1, 1, '{"catid":["10"],"count":"7","show_front":"1","layout":"_:default","moduleclass_sfx":"","cache":"1","cache_time":"900","cachemode":"static","module_tag":"div","bootstrap_size":"0","header_tag":"h3","header_class":"","style":"0"}', 0, '*');

-- --------------------------------------------------------

--
-- Table structure for table `#__modules_menu`
--

DROP TABLE IF EXISTS `#__modules_menu`;
CREATE TABLE IF NOT EXISTS `#__modules_menu` (
  `moduleid` int(11) NOT NULL DEFAULT '0',
  `menuid` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`moduleid`,`menuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__modules_menu`
--

INSERT INTO `#__modules_menu` (`moduleid`, `menuid`) VALUES
(2, 0),
(3, 0),
(4, 0),
(6, 0),
(7, 0),
(8, 0),
(9, 0),
(10, 0),
(12, 0),
(13, 0),
(14, 0),
(15, 0),
(79, 0),
(86, 0),
(87, 132),
(87, 167),
(88, 0),
(91, 132),
(91, 167),
(92, 132),
(92, 167),
(97, -167),
(97, -132),
(98, 132),
(98, 167),
(99, -167),
(99, -132),
(100, -167),
(100, -132),
(101, -167),
(101, -132),
(102, 0),
(105, 132),
(105, 140),
(105, 167),
(118, 132),
(118, 167),
(124, 132),
(124, 167),
(125, 0),
(129, 0),
(130, 132),
(130, 167),
(134, 132),
(134, 167),
(135, 132),
(135, 167),
(138, 102),
(138, 107),
(138, 108),
(138, 127),
(138, 146),
(138, 152),
(138, 153),
(138, 155),
(138, 157),
(138, 158),
(138, 170),
(142, 0),
(156, -171),
(156, -167),
(156, -132),
(156, -101),
(159, -167),
(159, -132),
(196, 0),
(197, 0),
(198, 101),
(199, 101),
(200, 101),
(201, 132),
(201, 167),
(202, 132),
(202, 167),
(203, 132),
(203, 167),
(204, 132),
(204, 167),
(205, 132),
(205, 167),
(206, -167),
(206, -132);

-- --------------------------------------------------------

--
-- Table structure for table `#__newsfeeds`
--

DROP TABLE IF EXISTS `#__newsfeeds`;
CREATE TABLE IF NOT EXISTS `#__newsfeeds` (
  `catid` int(11) NOT NULL DEFAULT '0',
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `link` varchar(200) NOT NULL DEFAULT '',
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `numarticles` int(10) unsigned NOT NULL DEFAULT '1',
  `cache_time` int(10) unsigned NOT NULL DEFAULT '3600',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rtl` tinyint(4) NOT NULL DEFAULT '0',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `description` text NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`published`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `#__newsfeeds`
--

INSERT INTO `#__newsfeeds` (`catid`, `id`, `name`, `alias`, `link`, `published`, `numarticles`, `cache_time`, `checked_out`, `checked_out_time`, `ordering`, `rtl`, `access`, `language`, `params`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `xreference`, `publish_up`, `publish_down`, `description`, `version`, `hits`, `images`) VALUES
(15, 1, 'Free Software Foundation', 'free-software-foundation', 'http://www.fsf.org/news/RSS', 1, 5, 3600, 0, '0000-00-00 00:00:00', 1, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:27:35', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(15, 2, 'Free Software Foundation Blogs', 'free-software-foundation-blogs', 'ttp://www.fsf.org/blogs/RSS', 1, 5, 3600, 0, '0000-00-00 00:00:00', 2, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:27:51', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(16, 3, 'Joomla! Announcements', 'joomla-announcements', 'http://feeds.joomla.org/JoomlaAnnouncements', 1, 5, 3600, 0, '0000-00-00 00:00:00', 3, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:28:09', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(16, 4, 'Joomla! Community Magazine', 'joomla-community-magazine', 'http://feeds.joomla.org/JoomlaMagazine', 1, 5, 3600, 0, '0000-00-00 00:00:00', 4, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:28:22', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(16, 5, 'Joomla! Core Team Blog', 'joomla-core-team-blog', 'http://feeds.joomla.org/JoomlaCommunityCoreTeamBlog', 1, 5, 3600, 0, '0000-00-00 00:00:00', 5, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:28:36', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(16, 6, 'Joomla! Developer News', 'joomla-developer-news', 'http://feeds.joomla.org/JoomlaDeveloper', 1, 5, 3600, 0, '0000-00-00 00:00:00', 6, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:28:50', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(16, 7, 'Joomla! Security News', 'joomla-security-news', 'http://feeds.joomla.org/JoomlaSecurityNews', 1, 5, 3600, 0, '0000-00-00 00:00:00', 7, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:29:04', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(14, 8, 'Linux Foundation Announcements', 'linux-foundation-announcements', 'http://www.linuxfoundation.org/press/rss20.xml', 1, 5, 3600, 0, '0000-00-00 00:00:00', 8, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:29:31', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(14, 9, 'Mootools Blog', 'mootools-blog', 'http://feeds.feedburner.com/mootools-blog', 1, 5, 3600, 0, '0000-00-00 00:00:00', 9, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:29:48', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(15, 10, 'Open Source Initiative Blog', 'open-source-initiative-blog', 'http://www.opensource.org/blog/feed', 1, 5, 3600, 0, '0000-00-00 00:00:00', 10, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:30:08', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(14, 11, 'PHP News and Announcements', 'php-news-and-announcements', 'http://www.php.net/feed.atom', 1, 5, 3600, 0, '0000-00-00 00:00:00', 11, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:30:33', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(14, 12, 'Planet MySQL', 'planet-mysql', 'http://www.planetmysql.org/rss20.xml', 1, 5, 3600, 0, '0000-00-00 00:00:00', 12, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:30:48', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(15, 13, 'Software Freedom Law Center Blog', 'software-freedom-law-center-blog', 'http://www.softwarefreedom.org/feeds/blog/', 1, 5, 3600, 0, '0000-00-00 00:00:00', 13, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:31:05', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, ''),
(15, 14, 'Software Freedom Law Center News', 'software-freedom-law-center-news', 'http://www.softwarefreedom.org/feeds/news/', 1, 5, 3600, 0, '0000-00-00 00:00:00', 14, 0, 1, '*', '{"show_feed_image":"","show_feed_description":"","show_item_description":"","feed_character_count":"0","newsfeed_layout":"","feed_display_order":""}', '2013-05-13 01:31:24', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', '', 1, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `#__overrider`
--

DROP TABLE IF EXISTS `#__overrider`;
CREATE TABLE IF NOT EXISTS `#__overrider` (
  `id` int(10) NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `constant` varchar(255) NOT NULL,
  `string` text NOT NULL,
  `file` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__redirect_links`
--

DROP TABLE IF EXISTS `#__redirect_links`;
CREATE TABLE IF NOT EXISTS `#__redirect_links` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `old_url` varchar(255) NOT NULL,
  `new_url` varchar(255) NOT NULL,
  `referer` varchar(150) NOT NULL,
  `comment` varchar(255) NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `published` tinyint(4) NOT NULL,
  `created_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_link_old` (`old_url`),
  KEY `idx_link_modifed` (`modified_date`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=36 ;

--
-- Dumping data for table `#__redirect_links`
--

INSERT INTO `#__redirect_links` (`id`, `old_url`, `new_url`, `referer`, `comment`, `hits`, `published`, `created_date`, `modified_date`) VALUES
(1, 'http://mysportwiki.com/s5project/index.php?Itemid=102', '', 'http://mysportwiki.com/s5project/index.php/component/search/?searchword=test&searchphrase=all&Itemid=212', '', 1, 0, '2013-05-13 00:14:01', '0000-00-00 00:00:00'),
(2, 'http://mysportwiki.com/s5project/index.php/2013-05-12-04-18-38/2013-05-12-04-34-34/index.php', '', 'http://mysportwiki.com/s5project/index.php/2013-05-12-04-18-38/2013-05-12-04-34-34/links', '', 1, 0, '2013-05-13 02:15:18', '0000-00-00 00:00:00'),
(3, 'http://localhost/lime/index.php/2013-05-12-04-18-38/2013-05-12-04-34-34/index.php', '', 'http://localhost/lime/index.php/2013-05-12-04-18-38/2013-05-12-04-34-34/contact-us', '', 1, 0, '2013-06-03 17:33:03', '0000-00-00 00:00:00'),
(4, 'http://localhost/lime/index.php/2013-05-12-03-56-22/s5-accordion-menu', '', 'http://localhost/lime/', '', 1, 0, '2013-06-03 17:40:05', '0000-00-00 00:00:00'),
(5, 'http://localhost/lime/index.php/features-mainmenu-47/template-features/index.php', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-features/responsive-layout', '', 10, 0, '2013-06-04 21:19:43', '0000-00-00 00:00:00'),
(6, 'http://localhost/lime/index.php?Itemid=176', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-features/responsive-layout', '', 2, 0, '2013-06-06 20:57:25', '0000-00-00 00:00:00'),
(7, 'http://localhost/lime/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/index.php', '', 'http://localhost/lime/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/search', '', 115, 0, '2013-06-07 01:04:46', '0000-00-00 00:00:00'),
(8, 'http://localhost/lime/index.php/new', '', '', '', 1, 0, '2013-06-10 15:57:17', '0000-00-00 00:00:00'),
(9, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderBottomCenter.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(10, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderBottomLeft.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(11, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderMiddleRight.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(12, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderTopCenter.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(13, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderBottomRight.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(14, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderMiddleLeft.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(15, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderTopRight.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(16, 'http://localhost/lime/index.php/features-mainmenu-47/modules/mod_s5_box/images/internet_explorer/borderTopLeft.png', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 5, 0, '2013-06-10 23:05:45', '0000-00-00 00:00:00'),
(17, 'http://localhost/lime/index.php/features-mainmenu-47/index.php', '', 'http://localhost/lime/index.php/features-mainmenu-47/template-specific-features', '', 1, 0, '2013-06-11 00:10:13', '0000-00-00 00:00:00'),
(18, 'http://localhost/buspro/index.php/features-mainmenu-47/template-features/index.php', '', 'http://localhost/buspro/index.php/features-mainmenu-47/template-features/responsive-layout', '', 8, 0, '2013-08-06 21:17:59', '0000-00-00 00:00:00'),
(19, 'http://localhost/buspro/index.php/extensions/images/frontpage1.jpg', '', 'http://localhost/buspro/index.php/extensions/s5-frontpage-display', '', 8, 0, '2013-08-09 17:06:10', '0000-00-00 00:00:00'),
(20, 'http://localhost/buspro/index.php/extensions/images/fd_active.png', '', 'http://localhost/buspro/index.php/extensions/s5-frontpage-display', '', 13, 0, '2013-08-09 17:06:10', '0000-00-00 00:00:00'),
(21, 'http://localhost/buspro/index.php/extensions/images/frontpage2.jpg', '', 'http://localhost/buspro/index.php/extensions/s5-frontpage-display', '', 7, 0, '2013-08-09 17:08:09', '0000-00-00 00:00:00'),
(22, 'http://localhost/buspro/index.php/extensions/images/frontpage3.jpg', '', 'http://localhost/buspro/index.php/extensions/s5-frontpage-display', '', 5, 0, '2013-08-09 17:08:17', '0000-00-00 00:00:00'),
(23, 'http://localhost/buspro/index.php/extensions/index.php/extensions/s5-frontpage-display', '', 'http://localhost/buspro/index.php/extensions/s5-frontpage-display', '', 2, 0, '2013-08-09 17:10:15', '0000-00-00 00:00:00'),
(24, 'http://localhost/buspro/index.php/features-mainmenu-47/index.php', '', 'http://localhost/buspro/index.php/features-mainmenu-47/template-specific-features', '', 1, 0, '2013-08-09 17:26:34', '0000-00-00 00:00:00'),
(25, 'http://localhost/buspro/index.php/tutorials-menu-48/index.php', '', 'http://localhost/buspro/index.php/tutorials-menu-48/s5-login-and-register-setup', '', 5, 0, '2013-08-10 01:04:56', '0000-00-00 00:00:00'),
(26, 'http://localhost/buspro/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/index.php', '', 'http://localhost/buspro/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/contact-us', '', 9, 0, '2013-08-10 13:21:08', '0000-00-00 00:00:00'),
(27, 'http://localhost/buspro/index.php/features-mainmenu-47/template-features/95-module-positions', '', 'http://localhost/buspro/index.php/features-mainmenu-47/continued-vertex-features/typography', '', 1, 0, '2013-08-12 15:22:38', '0000-00-00 00:00:00'),
(28, 'http://localhost/velocity/index.php/s5-accordion-menu/s5-box', '', 'http://localhost/velocity/index.php/features-mainmenu-47/template-features/responsive-layout', '', 1, 0, '2013-10-05 14:08:03', '0000-00-00 00:00:00'),
(29, 'http://localhost/velocity/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/index.php', '', 'http://localhost/velocity/index.php/features-mainmenu-47/joomla-stuff-mainmenu-26/contact-us', '', 1, 0, '2013-10-10 17:39:32', '0000-00-00 00:00:00'),
(30, 'http://localhost/velocity/index.php/features-mainmenu-47/template-features/index.php', '', 'http://localhost/velocity/index.php/features-mainmenu-47/template-features/seo-optimized', '', 2, 0, '2013-10-10 21:56:38', '0000-00-00 00:00:00'),
(31, 'http://localhost/velocity/index.php/tutorials-menu-48/index.php', '', 'http://localhost/velocity/index.php/tutorials-menu-48/search-and-menus-setup', '', 4, 0, '2013-10-11 16:15:07', '0000-00-00 00:00:00'),
(32, 'http://localhost/velocity/index.php/features-mainmenu-47/template-features/97-module-positions', '', 'http://localhost/velocity/index.php/extensions/s5-image-and-content-fader', '', 1, 0, '2013-10-11 17:31:13', '0000-00-00 00:00:00'),
(33, 'http://localhost/velocity/index.php/features-mainmenu-47/index.php', '', 'http://localhost/velocity/index.php/features-mainmenu-47/template-specific-features', '', 1, 0, '2013-10-11 18:54:27', '0000-00-00 00:00:00'),
(34, 'http://localhost/design/index.php/features-mainmenu-47/continued-vertex-features/index.php', '', 'http://localhost/design/index.php/features-mainmenu-47/continued-vertex-features/typography', '', 1, 0, '2013-11-25 20:02:10', '0000-00-00 00:00:00'),
(35, 'http://localhost/design/index.php/features-mainmenu-47/template-features/index.php', '', 'http://localhost/design/index.php/features-mainmenu-47/template-features/92-module-positions', '', 1, 0, '2013-11-25 20:30:45', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `#__schemas`
--

DROP TABLE IF EXISTS `#__schemas`;
CREATE TABLE IF NOT EXISTS `#__schemas` (
  `extension_id` int(11) NOT NULL,
  `version_id` varchar(20) NOT NULL,
  PRIMARY KEY (`extension_id`,`version_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__schemas`
--

INSERT INTO `#__schemas` (`extension_id`, `version_id`) VALUES
(700, '3.2.0');

-- --------------------------------------------------------

--
-- Table structure for table `#__session`
--

DROP TABLE IF EXISTS `#__session`;
CREATE TABLE IF NOT EXISTS `#__session` (
  `session_id` varchar(200) NOT NULL DEFAULT '',
  `client_id` tinyint(3) unsigned NOT NULL DEFAULT '0',
  `guest` tinyint(4) unsigned DEFAULT '1',
  `time` varchar(14) DEFAULT '',
  `data` mediumtext,
  `userid` int(11) DEFAULT '0',
  `username` varchar(150) DEFAULT '',
  PRIMARY KEY (`session_id`),
  KEY `userid` (`userid`),
  KEY `time` (`time`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `#__session`
--

INSERT INTO `#__session` (`session_id`, `client_id`, `guest`, `time`, `data`, `userid`, `username`) VALUES
('mj6ggj23l818ki0pq78s6t65r0', 1, 0, '1385483849', '__default|a:8:{s:15:"session.counter";i:197;s:19:"session.timer.start";i:1385479666;s:18:"session.timer.last";i:1385483849;s:17:"session.timer.now";i:1385483849;s:22:"session.client.browser";s:72:"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";s:8:"registry";O:9:"JRegistry":1:{s:7:"\\0\\0\\0data";O:8:"stdClass":8:{s:11:"application";O:8:"stdClass":1:{s:4:"lang";s:0:"";}s:11:"com_modules";O:8:"stdClass":3:{s:7:"modules";O:8:"stdClass":4:{s:6:"filter";O:8:"stdClass":8:{s:18:"client_id_previous";i:0;s:6:"search";s:5:"popul";s:6:"access";i:0;s:5:"state";s:0:"";s:8:"position";s:0:"";s:6:"module";s:0:"";s:9:"client_id";i:0;s:8:"language";s:0:"";}s:10:"limitstart";i:0;s:8:"ordercol";s:8:"position";s:9:"orderdirn";s:3:"asc";}s:4:"edit";O:8:"stdClass":1:{s:6:"module";O:8:"stdClass":2:{s:2:"id";a:0:{}s:4:"data";N;}}s:3:"add";O:8:"stdClass":1:{s:6:"module";O:8:"stdClass":2:{s:12:"extension_id";N;s:6:"params";N;}}}s:6:"global";O:8:"stdClass":1:{s:4:"list";O:8:"stdClass":1:{s:5:"limit";i:0;}}s:9:"com_menus";O:8:"stdClass":2:{s:5:"items";O:8:"stdClass":6:{s:6:"filter";O:8:"stdClass":4:{s:8:"menutype";s:10:"quick-menu";s:6:"access";i:0;s:5:"level";i:0;s:8:"language";s:0:"";}s:10:"limitstart";i:0;s:6:"search";s:5:"login";s:9:"published";s:0:"";s:8:"ordercol";s:5:"a.lft";s:9:"orderdirn";s:3:"asc";}s:4:"edit";O:8:"stdClass":1:{s:4:"item";O:8:"stdClass":4:{s:2:"id";a:0:{}s:4:"data";N;s:4:"type";N;s:4:"link";N;}}}s:11:"com_content";O:8:"stdClass":2:{s:8:"articles";O:8:"stdClass":4:{s:6:"filter";O:8:"stdClass":8:{s:6:"search";s:8:"specific";s:6:"access";i:0;s:9:"author_id";s:0:"";s:9:"published";s:0:"";s:11:"category_id";s:0:"";s:5:"level";i:0;s:8:"language";s:0:"";s:3:"tag";s:0:"";}s:10:"limitstart";i:0;s:8:"ordercol";s:7:"a.title";s:9:"orderdirn";s:3:"asc";}s:4:"edit";O:8:"stdClass":1:{s:7:"article";O:8:"stdClass":2:{s:2:"id";a:0:{}s:4:"data";N;}}}s:13:"com_installer";O:8:"stdClass":2:{s:7:"message";s:0:"";s:17:"extension_message";s:0:"";}s:13:"com_templates";O:8:"stdClass":1:{s:4:"edit";O:8:"stdClass":1:{s:5:"style";O:8:"stdClass":2:{s:2:"id";a:0:{}s:4:"data";N;}}}s:16:"com_joomlaupdate";O:8:"stdClass":3:{s:4:"file";N;s:8:"password";s:32:"FKXo340YYBdImQLgHOfyZX2766fD7Wsz";s:8:"filesize";i:8398890;}}}s:4:"user";O:5:"JUser":24:{s:9:"\\0\\0\\0isRoot";b:1;s:2:"id";s:3:"791";s:4:"name";s:10:"Super User";s:8:"username";s:5:"admin";s:5:"email";s:16:"slkfj@sdflkj.com";s:8:"password";s:65:"d5cd6a95bbe501f7d603847015364eba:dflajuqLElsjTPk3rB7KG49Z1k9wdkES";s:14:"password_clear";s:0:"";s:5:"block";s:1:"0";s:9:"sendEmail";s:1:"1";s:12:"registerDate";s:19:"2013-11-06 15:26:57";s:13:"lastvisitDate";s:19:"2013-11-26 02:22:18";s:10:"activation";s:1:"0";s:6:"params";s:0:"";s:6:"groups";a:1:{i:8;s:1:"8";}s:5:"guest";i:0;s:13:"lastResetTime";s:19:"0000-00-00 00:00:00";s:10:"resetCount";s:1:"0";s:10:"\\0\\0\\0_params";O:9:"JRegistry":1:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}}s:14:"\\0\\0\\0_authGroups";a:2:{i:0;i:1;i:1;i:8;}s:14:"\\0\\0\\0_authLevels";a:4:{i:0;i:1;i:1;i:1;i:2;i:2;i:3;i:3;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;}s:13:"session.token";s:32:"0a5d6236fb43efd2c17be917ff99e917";}', 791, 'admin'),
('oquk46gsc3fad7bv3hshm4iap5', 0, 1, '1385483947', '__default|a:9:{s:15:"session.counter";i:78;s:19:"session.timer.start";i:1385478989;s:18:"session.timer.last";i:1385483939;s:17:"session.timer.now";i:1385483946;s:22:"session.client.browser";s:72:"Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101 Firefox/25.0";s:8:"registry";O:9:"JRegistry":1:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}}s:4:"user";O:5:"JUser":24:{s:9:"\\0\\0\\0isRoot";b:0;s:2:"id";i:0;s:4:"name";N;s:8:"username";N;s:5:"email";N;s:8:"password";N;s:14:"password_clear";s:0:"";s:5:"block";N;s:9:"sendEmail";i:0;s:12:"registerDate";N;s:13:"lastvisitDate";N;s:10:"activation";N;s:6:"params";N;s:6:"groups";a:1:{i:0;i:1;}s:5:"guest";i:1;s:13:"lastResetTime";N;s:10:"resetCount";N;s:10:"\\0\\0\\0_params";O:9:"JRegistry":1:{s:7:"\\0\\0\\0data";O:8:"stdClass":0:{}}s:14:"\\0\\0\\0_authGroups";a:1:{i:0;i:1;}s:14:"\\0\\0\\0_authLevels";a:2:{i:0;i:1;i:1;i:1;}s:15:"\\0\\0\\0_authActions";N;s:12:"\\0\\0\\0_errorMsg";N;s:10:"\\0\\0\\0_errors";a:0:{}s:3:"aid";i:0;}s:16:"com_mailto.links";a:4:{s:40:"989203df9a5c99d8af169f6beeac25514f35892b";O:8:"stdClass":2:{s:4:"link";s:81:"http://localhost/design/index.php/features-mainmenu-47/template-specific-features";s:6:"expiry";i:1385483246;}s:40:"0dc5a60c1bf33a35f80e7829df64194509347cff";O:8:"stdClass":2:{s:4:"link";s:92:"http://localhost/design/index.php/features-mainmenu-47/template-features/92-module-positions";s:6:"expiry";i:1385483252;}s:40:"c19671cac3710a613d30214efd604408667dc1f9";O:8:"stdClass":2:{s:4:"link";s:46:"http://localhost/design/index.php/s5-flex-menu";s:6:"expiry";i:1385483871;}s:40:"19107f7a13382062af652cbbf4a2b30c76491b7a";O:8:"stdClass":2:{s:4:"link";s:102:"http://localhost/design/index.php/features-mainmenu-47/continued-vertex-features/site-shaper-available";s:6:"expiry";i:1385483946;}}s:13:"session.token";s:32:"73649f602034a0c6f4107b0ba825df21";}', 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `#__tags`
--

DROP TABLE IF EXISTS `#__tags`;
CREATE TABLE IF NOT EXISTS `#__tags` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0',
  `lft` int(11) NOT NULL DEFAULT '0',
  `rgt` int(11) NOT NULL DEFAULT '0',
  `level` int(10) unsigned NOT NULL DEFAULT '0',
  `path` varchar(255) NOT NULL DEFAULT '',
  `title` varchar(255) NOT NULL,
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `note` varchar(255) NOT NULL DEFAULT '',
  `description` mediumtext NOT NULL,
  `published` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `access` int(10) unsigned NOT NULL DEFAULT '0',
  `params` text NOT NULL,
  `metadesc` varchar(1024) NOT NULL COMMENT 'The meta description for the page.',
  `metakey` varchar(1024) NOT NULL COMMENT 'The meta keywords for the page.',
  `metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `images` text NOT NULL,
  `urls` text NOT NULL,
  `hits` int(10) unsigned NOT NULL DEFAULT '0',
  `language` char(7) NOT NULL,
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `tag_idx` (`published`,`access`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_path` (`path`),
  KEY `idx_left_right` (`lft`,`rgt`),
  KEY `idx_alias` (`alias`),
  KEY `idx_language` (`language`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `#__tags`
--

INSERT INTO `#__tags` (`id`, `parent_id`, `lft`, `rgt`, `level`, `path`, `title`, `alias`, `note`, `description`, `published`, `checked_out`, `checked_out_time`, `access`, `params`, `metadesc`, `metakey`, `metadata`, `created_user_id`, `created_time`, `created_by_alias`, `modified_user_id`, `modified_time`, `images`, `urls`, `hits`, `language`, `version`, `publish_up`, `publish_down`) VALUES
(1, 0, 0, 1, 0, '', 'ROOT', 'root', '', '', 1, 0, '0000-00-00 00:00:00', 1, '{}', '', '', '', 0, '2011-01-01 00:00:01', '', 0, '0000-00-00 00:00:00', '', '', 0, '*', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `#__template_styles`
--

DROP TABLE IF EXISTS `#__template_styles`;
CREATE TABLE IF NOT EXISTS `#__template_styles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `template` varchar(50) NOT NULL DEFAULT '',
  `client_id` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `home` char(7) NOT NULL DEFAULT '0',
  `title` varchar(255) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_template` (`template`),
  KEY `idx_home` (`home`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `#__template_styles`
--

INSERT INTO `#__template_styles` (`id`, `template`, `client_id`, `home`, `title`, `params`) VALUES
(2, 'bluestork', 1, '0', 'Bluestork - Default', '{"useRoundedCorners":"1","showSiteName":"0"}'),
(3, 'atomic', 0, '0', 'Atomic - Default', '{}'),
(4, 'beez_20', 0, '0', 'Beez2 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/joomla_black.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","templatecolor":"personal","html5":"0"}'),
(5, 'hathor', 1, '0', 'Hathor - Default', '{"showSiteName":"0","colourChoice":"","boldText":"0"}'),
(6, 'beez5', 0, '0', 'Beez5 - Default', '{"wrapperSmall":"53","wrapperLarge":"72","logo":"images\\/sampledata\\/fruitshop\\/fruits.gif","sitetitle":"Joomla!","sitedescription":"Open Source Content Management","navposition":"left","html5":"0"}'),
(12, 'protostar', 0, '0', 'protostar - Default', '{"templateColor":"","logoFile":"","googleFont":"1","googleFontName":"Open+Sans","fluidContainer":"0"}'),
(13, 'isis', 1, '1', 'isis - Default', '{"templateColor":"","logoFile":""}'),
(14, 'beez3', 0, '0', 'beez3 - Default', '{"wrapperSmall":53,"wrapperLarge":72,"logo":"","sitetitle":"","sitedescription":"","navposition":"center","bootstrap":"","templatecolor":"nature","headerImage":"","backgroundcolor":"#eee"}'),
(15, 'design_control', 0, '1', 'design_control - Default', '{"settings":""}');

-- --------------------------------------------------------

--
-- Table structure for table `#__ucm_base`
--

DROP TABLE IF EXISTS `#__ucm_base`;
CREATE TABLE IF NOT EXISTS `#__ucm_base` (
  `ucm_id` int(10) unsigned NOT NULL,
  `ucm_item_id` int(10) NOT NULL,
  `ucm_type_id` int(11) NOT NULL,
  `ucm_language_id` int(11) NOT NULL,
  PRIMARY KEY (`ucm_id`),
  KEY `idx_ucm_item_id` (`ucm_item_id`),
  KEY `idx_ucm_type_id` (`ucm_type_id`),
  KEY `idx_ucm_language_id` (`ucm_language_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `#__ucm_content`
--

DROP TABLE IF EXISTS `#__ucm_content`;
CREATE TABLE IF NOT EXISTS `#__ucm_content` (
  `core_content_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `core_type_alias` varchar(255) NOT NULL DEFAULT '' COMMENT 'FK to the content types table',
  `core_title` varchar(255) NOT NULL,
  `core_alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `core_body` mediumtext NOT NULL,
  `core_state` tinyint(1) NOT NULL DEFAULT '0',
  `core_checked_out_time` varchar(255) NOT NULL DEFAULT '',
  `core_checked_out_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_access` int(10) unsigned NOT NULL DEFAULT '0',
  `core_params` text NOT NULL,
  `core_featured` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `core_metadata` varchar(2048) NOT NULL COMMENT 'JSON encoded metadata properties.',
  `core_created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `core_created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `core_created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_modified_user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Most recent user that modified',
  `core_modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `core_language` char(7) NOT NULL,
  `core_publish_up` datetime NOT NULL,
  `core_publish_down` datetime NOT NULL,
  `core_content_item_id` int(10) unsigned DEFAULT NULL COMMENT 'ID from the individual type table',
  `asset_id` int(10) unsigned DEFAULT NULL COMMENT 'FK to the #__assets table.',
  `core_images` text NOT NULL,
  `core_urls` text NOT NULL,
  `core_hits` int(10) unsigned NOT NULL DEFAULT '0',
  `core_version` int(10) unsigned NOT NULL DEFAULT '1',
  `core_ordering` int(11) NOT NULL DEFAULT '0',
  `core_metakey` text NOT NULL,
  `core_metadesc` text NOT NULL,
  `core_catid` int(10) unsigned NOT NULL DEFAULT '0',
  `core_xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `core_type_id` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`core_content_id`),
  KEY `tag_idx` (`core_state`,`core_access`),
  KEY `idx_access` (`core_access`),
  KEY `idx_alias` (`core_alias`),
  KEY `idx_language` (`core_language`),
  KEY `idx_title` (`core_title`),
  KEY `idx_modified_time` (`core_modified_time`),
  KEY `idx_created_time` (`core_created_time`),
  KEY `idx_content_type` (`core_type_alias`),
  KEY `idx_core_modified_user_id` (`core_modified_user_id`),
  KEY `idx_core_checked_out_user_id` (`core_checked_out_user_id`),
  KEY `idx_core_created_user_id` (`core_created_user_id`),
  KEY `idx_core_type_id` (`core_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains core content data in name spaced fields' AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__updates`
--

DROP TABLE IF EXISTS `#__updates`;
CREATE TABLE IF NOT EXISTS `#__updates` (
  `update_id` int(11) NOT NULL AUTO_INCREMENT,
  `update_site_id` int(11) DEFAULT '0',
  `extension_id` int(11) DEFAULT '0',
  `name` varchar(100) DEFAULT '',
  `description` text NOT NULL,
  `element` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `folder` varchar(20) DEFAULT '',
  `client_id` tinyint(3) DEFAULT '0',
  `version` varchar(10) DEFAULT '',
  `data` text NOT NULL,
  `detailsurl` text NOT NULL,
  `infourl` text NOT NULL,
  PRIMARY KEY (`update_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Available Updates' AUTO_INCREMENT=49 ;

--
-- Dumping data for table `#__updates`
--

INSERT INTO `#__updates` (`update_id`, `update_site_id`, `extension_id`, `name`, `description`, `element`, `type`, `folder`, `client_id`, `version`, `data`, `detailsurl`, `infourl`) VALUES
(2, 3, 0, 'Malay', '', 'pkg_ms-MY', 'package', '', 0, '3.1.5.4', '', 'http://update.joomla.org/language/details3/ms-MY_details.xml', ''),
(3, 3, 0, 'Romanian', '', 'pkg_ro-RO', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/ro-RO_details.xml', ''),
(4, 3, 0, 'Flemish', '', 'pkg_nl-BE', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/nl-BE_details.xml', ''),
(5, 3, 0, 'Chinese Traditional', '', 'pkg_zh-TW', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/zh-TW_details.xml', ''),
(6, 3, 0, 'French', '', 'pkg_fr-FR', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/fr-FR_details.xml', ''),
(7, 3, 0, 'German', '', 'pkg_de-DE', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/de-DE_details.xml', ''),
(8, 3, 0, 'Greek', '', 'pkg_el-GR', 'package', '', 0, '3.1.5.1', '', 'http://update.joomla.org/language/details3/el-GR_details.xml', ''),
(9, 3, 0, 'Japanese', '', 'pkg_ja-JP', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/ja-JP_details.xml', ''),
(10, 3, 0, 'Hebrew', '', 'pkg_he-IL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/he-IL_details.xml', ''),
(11, 3, 0, 'Hungarian', '', 'pkg_hu-HU', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/hu-HU_details.xml', ''),
(12, 3, 0, 'Afrikaans', '', 'pkg_af-ZA', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/af-ZA_details.xml', ''),
(13, 3, 0, 'Arabic Unitag', '', 'pkg_ar-AA', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/ar-AA_details.xml', ''),
(14, 3, 0, 'Bulgarian', '', 'pkg_bg-BG', 'package', '', 0, '3.0.3.1', '', 'http://update.joomla.org/language/details3/bg-BG_details.xml', ''),
(15, 3, 0, 'Catalan', '', 'pkg_ca-ES', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/ca-ES_details.xml', ''),
(16, 3, 0, 'Chinese Simplified', '', 'pkg_zh-CN', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/zh-CN_details.xml', ''),
(17, 3, 0, 'Croatian', '', 'pkg_hr-HR', 'package', '', 0, '3.1.5.1', '', 'http://update.joomla.org/language/details3/hr-HR_details.xml', ''),
(18, 3, 0, 'Czech', '', 'pkg_cs-CZ', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/cs-CZ_details.xml', ''),
(19, 3, 0, 'Danish', '', 'pkg_da-DK', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/da-DK_details.xml', ''),
(20, 3, 0, 'Dutch', '', 'pkg_nl-NL', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/nl-NL_details.xml', ''),
(21, 3, 0, 'English AU', '', 'pkg_en-AU', 'package', '', 0, '3.1.0.1', '', 'http://update.joomla.org/language/details3/en-AU_details.xml', ''),
(22, 3, 0, 'English US', '', 'pkg_en-US', 'package', '', 0, '3.1.0.1', '', 'http://update.joomla.org/language/details3/en-US_details.xml', ''),
(23, 3, 0, 'Estonian', '', 'pkg_et-EE', 'package', '', 0, '3.1.2.1', '', 'http://update.joomla.org/language/details3/et-EE_details.xml', ''),
(24, 3, 0, 'Italian', '', 'pkg_it-IT', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/it-IT_details.xml', ''),
(25, 3, 0, 'Korean', '', 'pkg_ko-KR', 'package', '', 0, '3.2.0.3', '', 'http://update.joomla.org/language/details3/ko-KR_details.xml', ''),
(26, 3, 0, 'Latvian', '', 'pkg_lv-LV', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/lv-LV_details.xml', ''),
(27, 3, 0, 'Macedonian', '', 'pkg_mk-MK', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/mk-MK_details.xml', ''),
(28, 3, 0, 'Norwegian Bokmal', '', 'pkg_nb-NO', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/nb-NO_details.xml', ''),
(29, 3, 0, 'Persian', '', 'pkg_fa-IR', 'package', '', 0, '3.1.4.1', '', 'http://update.joomla.org/language/details3/fa-IR_details.xml', ''),
(30, 3, 0, 'Polish', '', 'pkg_pl-PL', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/pl-PL_details.xml', ''),
(31, 3, 0, 'Russian', '', 'pkg_ru-RU', 'package', '', 0, '3.1.5.1', '', 'http://update.joomla.org/language/details3/ru-RU_details.xml', ''),
(32, 3, 0, 'Slovak', '', 'pkg_sk-SK', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/sk-SK_details.xml', ''),
(33, 3, 0, 'Swedish', '', 'pkg_sv-SE', 'package', '', 0, '3.2.1.1', '', 'http://update.joomla.org/language/details3/sv-SE_details.xml', ''),
(34, 3, 0, 'Syriac', '', 'pkg_sy-IQ', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/sy-IQ_details.xml', ''),
(35, 3, 0, 'Tamil', '', 'pkg_ta-IN', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/ta-IN_details.xml', ''),
(36, 3, 0, 'Thai', '', 'pkg_th-TH', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/th-TH_details.xml', ''),
(37, 3, 0, 'Turkish', '', 'pkg_tr-TR', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/tr-TR_details.xml', ''),
(38, 3, 0, 'Ukrainian', '', 'pkg_uk-UA', 'package', '', 0, '3.2.0.6', '', 'http://update.joomla.org/language/details3/uk-UA_details.xml', ''),
(39, 3, 0, 'Uyghur', '', 'pkg_ug-CN', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/ug-CN_details.xml', ''),
(40, 3, 0, 'Albanian', '', 'pkg_sq-AL', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/sq-AL_details.xml', ''),
(41, 3, 0, 'Serbian Latin', '', 'pkg_sr-YU', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/sr-YU_details.xml', ''),
(42, 3, 0, 'Spanish', '', 'pkg_es-ES', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/es-ES_details.xml', ''),
(43, 3, 0, 'Bosnian', '', 'pkg_bs-BA', 'package', '', 0, '3.1.1.1', '', 'http://update.joomla.org/language/details3/bs-BA_details.xml', ''),
(44, 3, 0, 'Serbian Cyrillic', '', 'pkg_sr-RS', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/sr-RS_details.xml', ''),
(45, 3, 0, 'Bahasa Indonesia', '', 'pkg_id-ID', 'package', '', 0, '3.1.4.1', '', 'http://update.joomla.org/language/details3/id-ID_details.xml', ''),
(46, 3, 0, 'Finnish', '', 'pkg_fi-FI', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/fi-FI_details.xml', ''),
(47, 3, 0, 'Swahili', '', 'pkg_sw-KE', 'package', '', 0, '3.2.0.1', '', 'http://update.joomla.org/language/details3/sw-KE_details.xml', ''),
(48, 3, 0, 'Montenegrin', '', 'pkg_srp-ME', 'package', '', 0, '3.2.0.2', '', 'http://update.joomla.org/language/details3/srp-ME_details.xml', '');

-- --------------------------------------------------------

--
-- Table structure for table `#__update_sites`
--

DROP TABLE IF EXISTS `#__update_sites`;
CREATE TABLE IF NOT EXISTS `#__update_sites` (
  `update_site_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) DEFAULT '',
  `type` varchar(20) DEFAULT '',
  `location` text NOT NULL,
  `enabled` int(11) DEFAULT '0',
  `last_check_timestamp` bigint(20) DEFAULT '0',
  PRIMARY KEY (`update_site_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='Update Sites' AUTO_INCREMENT=4 ;

--
-- Dumping data for table `#__update_sites`
--

INSERT INTO `#__update_sites` (`update_site_id`, `name`, `type`, `location`, `enabled`, `last_check_timestamp`) VALUES
(1, 'Joomla Core', 'collection', 'http://update.joomla.org/core/sts/list_sts.xml', 1, 1385483419),
(2, 'Joomla Extension Directory', 'collection', 'http://update.joomla.org/jed/list.xml', 1, 1385483419),
(3, 'Accredited Joomla! Translations', 'collection', 'http://update.joomla.org/language/translationlist_3.xml', 1, 1385483419);

-- --------------------------------------------------------

--
-- Table structure for table `#__update_sites_extensions`
--

DROP TABLE IF EXISTS `#__update_sites_extensions`;
CREATE TABLE IF NOT EXISTS `#__update_sites_extensions` (
  `update_site_id` int(11) NOT NULL DEFAULT '0',
  `extension_id` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`update_site_id`,`extension_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Links extensions to update sites';

--
-- Dumping data for table `#__update_sites_extensions`
--

INSERT INTO `#__update_sites_extensions` (`update_site_id`, `extension_id`) VALUES
(1, 700),
(2, 700),
(3, 600);

-- --------------------------------------------------------

--
-- Table structure for table `#__usergroups`
--

DROP TABLE IF EXISTS `#__usergroups`;
CREATE TABLE IF NOT EXISTS `#__usergroups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `parent_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Adjacency List Reference Id',
  `lft` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set lft.',
  `rgt` int(11) NOT NULL DEFAULT '0' COMMENT 'Nested set rgt.',
  `title` varchar(100) NOT NULL DEFAULT '',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_usergroup_parent_title_lookup` (`parent_id`,`title`),
  KEY `idx_usergroup_title_lookup` (`title`),
  KEY `idx_usergroup_adjacency_lookup` (`parent_id`),
  KEY `idx_usergroup_nested_set_lookup` (`lft`,`rgt`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `#__usergroups`
--

INSERT INTO `#__usergroups` (`id`, `parent_id`, `lft`, `rgt`, `title`) VALUES
(1, 0, 1, 20, 'Public'),
(2, 1, 6, 17, 'Registered'),
(3, 2, 7, 14, 'Author'),
(4, 3, 8, 11, 'Editor'),
(5, 4, 9, 10, 'Publisher'),
(6, 1, 2, 5, 'Manager'),
(7, 6, 3, 4, 'Administrator'),
(8, 1, 18, 19, 'Super Users');

-- --------------------------------------------------------

--
-- Table structure for table `#__users`
--

DROP TABLE IF EXISTS `#__users`;
CREATE TABLE IF NOT EXISTS `#__users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '',
  `username` varchar(150) NOT NULL DEFAULT '',
  `email` varchar(100) NOT NULL DEFAULT '',
  `password` varchar(100) NOT NULL DEFAULT '',
  `block` tinyint(4) NOT NULL DEFAULT '0',
  `sendEmail` tinyint(4) DEFAULT '0',
  `registerDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `lastvisitDate` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `activation` varchar(100) NOT NULL DEFAULT '',
  `params` text NOT NULL,
  `lastResetTime` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Date of last password reset',
  `resetCount` int(11) NOT NULL DEFAULT '0' COMMENT 'Count of password resets since lastResetTime',
  `otpKey` varchar(1000) NOT NULL DEFAULT '' COMMENT 'Two factor authentication encrypted keys',
  `otep` varchar(1000) NOT NULL DEFAULT '' COMMENT 'One time emergency passwords',
  PRIMARY KEY (`id`),
  KEY `idx_name` (`name`),
  KEY `idx_block` (`block`),
  KEY `username` (`username`),
  KEY `email` (`email`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=792 ;



DROP TABLE IF EXISTS `#__user_notes`;
CREATE TABLE IF NOT EXISTS `#__user_notes` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `catid` int(10) unsigned NOT NULL DEFAULT '0',
  `subject` varchar(100) NOT NULL DEFAULT '',
  `body` text NOT NULL,
  `state` tinyint(3) NOT NULL DEFAULT '0',
  `checked_out` int(10) unsigned NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_user_id` int(10) unsigned NOT NULL DEFAULT '0',
  `created_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_user_id` int(10) unsigned NOT NULL,
  `modified_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `review_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `idx_user_id` (`user_id`),
  KEY `idx_category_id` (`catid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `#__user_profiles`
--

DROP TABLE IF EXISTS `#__user_profiles`;
CREATE TABLE IF NOT EXISTS `#__user_profiles` (
  `user_id` int(11) NOT NULL,
  `profile_key` varchar(100) NOT NULL,
  `profile_value` varchar(255) NOT NULL,
  `ordering` int(11) NOT NULL DEFAULT '0',
  UNIQUE KEY `idx_user_id_profile_key` (`user_id`,`profile_key`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Simple user profile storage table';

-- --------------------------------------------------------

--
-- Table structure for table `#__user_usergroup_map`
--

DROP TABLE IF EXISTS `#__user_usergroup_map`;
CREATE TABLE IF NOT EXISTS `#__user_usergroup_map` (
  `user_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__users.id',
  `group_id` int(10) unsigned NOT NULL DEFAULT '0' COMMENT 'Foreign Key to #__usergroups.id',
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



DROP TABLE IF EXISTS `#__viewlevels`;
CREATE TABLE IF NOT EXISTS `#__viewlevels` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT COMMENT 'Primary Key',
  `title` varchar(100) NOT NULL DEFAULT '',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `rules` varchar(5120) NOT NULL COMMENT 'JSON encoded access control.',
  PRIMARY KEY (`id`),
  UNIQUE KEY `idx_assetgroup_title_lookup` (`title`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `#__viewlevels`
--

INSERT INTO `#__viewlevels` (`id`, `title`, `ordering`, `rules`) VALUES
(1, 'Public', 0, '[1]'),
(2, 'Registered', 1, '[6,2,8]'),
(3, 'Special', 2, '[6,3,8]');

-- --------------------------------------------------------

--
-- Table structure for table `#__weblinks`
--

DROP TABLE IF EXISTS `#__weblinks`;
CREATE TABLE IF NOT EXISTS `#__weblinks` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `catid` int(11) NOT NULL DEFAULT '0',
  `title` varchar(250) NOT NULL DEFAULT '',
  `alias` varchar(255) CHARACTER SET utf8 COLLATE utf8_bin NOT NULL DEFAULT '',
  `url` varchar(250) NOT NULL DEFAULT '',
  `description` text NOT NULL,
  `hits` int(11) NOT NULL DEFAULT '0',
  `state` tinyint(1) NOT NULL DEFAULT '0',
  `checked_out` int(11) NOT NULL DEFAULT '0',
  `checked_out_time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `ordering` int(11) NOT NULL DEFAULT '0',
  `access` int(11) NOT NULL DEFAULT '1',
  `params` text NOT NULL,
  `language` char(7) NOT NULL DEFAULT '',
  `created` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `created_by` int(10) unsigned NOT NULL DEFAULT '0',
  `created_by_alias` varchar(255) NOT NULL DEFAULT '',
  `modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `modified_by` int(10) unsigned NOT NULL DEFAULT '0',
  `metakey` text NOT NULL,
  `metadesc` text NOT NULL,
  `metadata` text NOT NULL,
  `featured` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Set if link is featured.',
  `xreference` varchar(50) NOT NULL COMMENT 'A reference to enable linkages to external data sets.',
  `publish_up` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `publish_down` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `version` int(10) unsigned NOT NULL DEFAULT '1',
  `images` text NOT NULL,
  PRIMARY KEY (`id`),
  KEY `idx_access` (`access`),
  KEY `idx_checkout` (`checked_out`),
  KEY `idx_state` (`state`),
  KEY `idx_catid` (`catid`),
  KEY `idx_createdby` (`created_by`),
  KEY `idx_featured_catid` (`featured`,`catid`),
  KEY `idx_language` (`language`),
  KEY `idx_xreference` (`xreference`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `#__weblinks`
--

INSERT INTO `#__weblinks` (`id`, `catid`, `title`, `alias`, `url`, `description`, `hits`, `state`, `checked_out`, `checked_out_time`, `ordering`, `access`, `params`, `language`, `created`, `created_by`, `created_by_alias`, `modified`, `modified_by`, `metakey`, `metadesc`, `metadata`, `featured`, `xreference`, `publish_up`, `publish_down`, `version`, `images`) VALUES
(1, 21, 'Joomla!', 'joomla', 'http://www.joomla.org', '<p>Home of Joomla!</p>', 0, 1, 0, '0000-00-00 00:00:00', 1, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:20:51', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(2, 21, 'Joomla! - Forums', 'joomla-forums', 'http://forum.joomla.org', '<p>Joomla! Forums</p>', 0, 1, 0, '0000-00-00 00:00:00', 2, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:21:05', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(3, 21, 'MySQL', 'mysql', 'http://www.mysql.com', '<p>The database that Joomla! uses</p>', 0, 1, 0, '0000-00-00 00:00:00', 3, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:21:21', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(4, 21, 'Ohloh Tracking of Joomla!', 'ohloh-tracking-of-joomla', 'http://www.ohloh.net/projects/20', '<p>Objective reports from Ohloh about Joomla''s development activity. Joomla! has some star developers with serious kudos.</p>', 0, 1, 0, '0000-00-00 00:00:00', 4, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:21:41', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(5, 21, 'OpenSourceMatters', 'opensourcematters', 'http://www.opensourcematters.org', '<p>Home of OSM</p>', 0, 1, 0, '0000-00-00 00:00:00', 5, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:22:02', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, ''),
(6, 21, 'php.net', 'php-net', 'http://www.php.net', '<p>The language that Joomla! is developed in</p>', 0, 1, 0, '0000-00-00 00:00:00', 6, 1, '{"target":"","width":"","height":"","count_clicks":""}', '*', '2013-05-13 02:22:22', 791, '', '0000-00-00 00:00:00', 0, '', '', '{"robots":"","rights":""}', 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00', 1, '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
