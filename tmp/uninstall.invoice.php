<?php

defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.filesystem.folder' );
jimport('joomla.filesystem.file');
function com_uninstall()
{
	$db =& JFactory::getDBO();
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	$current_version=substr($current_version,0,3);


	if($current_version!="1.5")
	{		
		$sql="DELETE FROM `#__assets` WHERE name='com_ccinvoices'";
		$db->setQuery( $sql);
		$db->query();	
	}

	if (JFolder::exists(JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment'))
	{
		$destinationfile=JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
		JFolder::delete($destinationfile);
		if($current_version=="1.5")
		{
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE folder='ccinvoices_payment'");
			if($db->loadResult())
			{
				$sql="DELETE FROM `#__plugins` WHERE folder='ccinvoices_payment'";
				$db->setQuery( $sql);
				$db->query();			
			}
		}
		else
		{
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE folder='ccinvoices_payment'");
			if($db->loadResult())
			{
				$sql="DELETE FROM `#__extensions` WHERE folder='ccinvoices_payment'";
				$db->setQuery( $sql);
				$db->query();			
			}		
		}
	}
	// Message area
		echo '<p> <b>ccInvoices component and plugins successfully uninstalled!</b> </p>';
}
?>
