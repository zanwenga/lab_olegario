<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
defined('_JEXEC') or die('Restricted access');
JToolBarHelper::title(JText::_('ccInvoices'), 'generic.png');
JToolBarHelper::preferences('com_ccinvoices');
?>
<!-- Deafult administrator message -->
----------- PAYMENT OPTION PAGE UNDER DEVELOPEMNT --------------