<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$editor		=& JFactory::getEditor();
?>
<script language="javascript" type="text/javascript">
function submitbutton(pressbutton)
{
	var form = document.adminForm;
	try
	{
		if ( form.invoice_template.value == "" ) {
			alert("<?php echo JText::_( 'CC_TEMPLATE_EMPTY', true ); ?>");
		} else {

			<?php
				if($this->row->id=="1")
				{
					echo $editor->save( 'invoice_template');
				}
			?>
			submitform(pressbutton);
		}
	}
	catch(e)
	{
		submitform(pressbutton);
	}
}
</script>
<form action="index.php" method="post" name="adminForm">
<table width="100%"  cellpadding="0" cellspacing="0" border="0" >

	<tr>
		<td valign="top">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							<input type="hidden" name="selected_id" value="<?php echo $this->row->id; ?>">
						</td>
					</tr>
					<?php
						if($this->row->id=="1" OR $this->row->id=="2")
						{
						?>
					<tr>
						<td align="left" >
							<input type="hidden" name="title" value="<?php echo $this->row->title;?>" size="40" maxlength="150" />
						</td>
					</tr>
					<tr>
						<td align="left" valign="top">
							&nbsp;
						</td>
						<td align="left" >

							<?php
							if($this->row->id=="1")
							{
								?>
								<fieldset class="adminform" style="width:auto;padding:10px;">
								<legend><?php echo JText::_( 'CC_TEMPLATE_INVOICE' ); ?></legend>
								<?php
								echo $editor->display( 'invoice_template', $this->row->invoice_template , '100%', '550', '25', '10',true ,null) ;
							?>
							<?php
							}else{
							?>
								<fieldset class="adminform" style="align:right;padding:10px;width:auto;">
								<legend><?php echo JText::_( 'CC_TEMPLATE_INVOICE_CSV' ); ?></legend>
								<textarea name="invoice_template" id="invoice_template" style="width:100%;" rows="5" cols="80"><?php echo $this->row->invoice_template; ?></textarea>
							<?php
							}
							 ?>
							 </fieldset>
						</td>
					</tr>
					<?php } ?>

					<?php
						if($this->row->id>2)
						{
						?>
					<tr>
						<td colspan="2" valign="top">
								<!--default note-->
							<?php
								if($this->row->id=="3")
								{
								?>
								<fieldset class="adminform" style="padding:10px;">
									<legend><?php echo JText::_( 'CC_DEFAULT_NOTE' ); ?></legend>
									<table cellpadding="3" cellspacing="0" width="100%" border="0">
										<tr>
											<td align="left" valign="top" >
												&nbsp;
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												 <?php 	echo $editor->display( 'default_note', $this->confrow->default_note , '100%', '200', '25', '10' ); ?>
											</td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>

								<?php
								if($this->row->id=="4")
								{
								?>
								<!--default email-->
								<fieldset class="adminform"  style="padding:10px;">
									<legend><?php echo JText::_( 'CC_DEFAULT_EMAIL' ); ?></legend>
										<table cellpadding="0" cellspacing="0" width="100%" border="0">
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_SUB' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<input type="text" name="default_email_sub" value="<?php echo $this->confrow->default_email_sub; ?>" size="60"/>

												</td>
											</tr>
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_MSG' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<?php 	echo $editor->display( 'default_email', $this->confrow->default_email , '100%', '350', '25', '10' ); ?>
												</br></br>
												</td>
											</tr>
										</table>
								</fieldset>
								<?php } ?>
								<?php
								if($this->row->id=="5")
								{
								?>
								<!--default email reminder-->
								<fieldset class="adminform"  style="padding:10px;">
									<legend><?php echo JText::_( 'CC_DEFAULT_EMAIL_REM' ); ?></legend>
									<table cellpadding="3" cellspacing="0" width="100%" border="0">
										<tr>
											<td align="left" valign="top" >
												<?php echo JText::_( 'CC_EMAIL_SUB' ); ?>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
											<input type="text" name="rem_email_sub" value="<?php echo $this->confrow->rem_email_sub; ?>" size="60"/>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top" >
												<?php echo JText::_( 'CC_EMAIL_MSG' ); ?>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<?php 	echo $editor->display( 'default_email_rem', $this->confrow->default_email_rem , '100%', '350', '25', '10' ); ?>
											</br></br>
											</td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>
						</td>
					</tr>
					<?php
						}
					?>
				</table>
		</td>
		<td width="200" valign="top" style="padding-top:4px;" >
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="left" >
					<?php
						JHTML::_('behavior.tooltip');
						jimport('joomla.html.pane');
						// TODO: allowAllClose should default true in J!1.6, so remove the array when it does.
						$pane1 = &JPane::getInstance('sliders', array('allowAllClose' => true));
						if($this->row->id=="3")
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							{invoice_due_date}<br/>
							{payment_methods}<br/>
							</table>";
						}
						elseif($this->row->id=="4" or $this->row->id=="5")
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							".JText::_( 'CC_EMAIL_TAGS' )."
							</table>";
						}
						else
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							{contact_name}<br/>
							{contact_number}<br/>
							{contact}<br/>
 							{contact_address}<br/>
							{contact_email}<br/><br/>
							{invoice_number}<br/>
							{invoice_date}<br/>
							{invoice_due_date}<br/>
							{invoice_sent_date}<br/>
							{invoice_note}<br/>
							{payment_methods}<br/><br/>
							{item_quantity}<br/>
							{item_name}<br/>
							{item_amount}<br/>
							{item_tax_percentage}<br/>
							{item_tax_amount}<br/>
							{item_total_incl_tax}<br/>
							{item_total_excl_tax}<br/><br/>
							{invoice_discount}<br/>
							{invoice_subtotal}<br/>
							{invoice_tax}<br/>{invoice_tax_total_split}<br/>{invoice_total_excl_tax}<br/>{invoice_total_incl_tax}<br/>
							{invoice_total}<br/><br/>
							{company_name}<br/>
							{company_email}<br/>
							{company_phone}<br/>
							{company_address}<br/>
							{company_details}<br/>
							{company_url}<br/>
							{company_tax_id}<br/>
							</table>";
						}
							echo $pane1->startPane("menu-pane11");
							echo $pane1->startPanel(JText :: _('CC_TEMP_TAGS'), "param-page");
		                    echo $shop_info;
							echo $pane1->endPanel();
						?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="controller" value="templates">
<input type="hidden" name="id" value="<?php echo $this->row->id; ?>">
<input type="hidden" name="edit_by" value="<?php echo $this->edit_by; ?>">
<input type="hidden" name="edit_date" value="<?php echo time(); ?>">
<input type="hidden" name="task" value="">
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<table width="100%">
	<tr>
		<td align="center">
		<?php
			if (isset($this->versionContent))
			{
				echo $this->versionContent;
			}
		?>
		</td>
	</tr>
</table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>