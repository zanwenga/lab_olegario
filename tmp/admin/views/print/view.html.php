<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.view' );

class ccInvoicesViewprint extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db =& JFactory::getDBO();
		require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."models".DS.'invoices.php');
		$invObj = new ccInvoicesModelInvoices();
		$invID = JRequest::getInt('id','');
		$template = $invObj->gettemplatelayout($invID);
		$this->assignRef("template",$template);
		parent::display($tpl);
	}
}
