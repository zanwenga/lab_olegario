<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
defined('_JEXEC') or die('Restricted access');
$values=$this->getinvoicecontact;
?>
<input  type="hidden" id="invoice_contact" value="<?php if( isset($values->contact)) { echo $values->contact;  } ?>">
<input  type="hidden" id="invoice_address" value="<?php if( isset($values->address))  { echo $values->address ; }?>">
<input  type="hidden" id="invoice_email" value="<?php if( isset($values->email))  { echo $values->email ; }?>">
<input  type="hidden" id="contact_number_cc" value="<?php if( isset($values->contact_number))  { echo $values->contact_number ; }?>">
