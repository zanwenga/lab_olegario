<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
defined('_JEXEC') or die('Restricted access');
?>
<script language="javascript" type="text/javascript">
function createFilter(filter)
{
	document.getElementById('filter').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter').value = '';
	submitform();
}
</script>
<script type="text/javascript">
function newinvoice()
{
	var chks = document.getElementsByName('cid[]');
	var hasChecked = false;
	var blocklist=document.getElementById('boxchecked').value;
	if(blocklist == '0')
	{
		window.location='index.php?option=com_ccinvoices&controller=invoices&task=invoiceforcontact';
	}else
	{
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].checked)
			{
				var idid=chks[i].value;
				window.location='index.php?option=com_ccinvoices&controller=invoices&task=invoiceforcontact&clientid='+idid;
			}
		}
	}
	return true;
}
</script>

<style>
.adminlist
{
	font-size:14px;
}
.adminlist p
{
	margin:0px;
	padding:0px;
}
</style>
<form action="index.php" method="post" name="adminForm">
<table>
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'CC_FILTER' ); ?>:
			<input type="text" name="invoice_filter" id="invoice_filter" onchange="createFilter(document.getElementById('invoice_filter').value);" class="text_area" value="<?php echo $this->lists['search']; ?>"/>
			<input type="button" value="<?php echo JText::_('CC_GO'); ?>" onclick="createFilter(document.getElementById('invoice_filter').value);return false;" />
			<input type="button" onclick="eraseFilter();" value="<?php echo JText::_('CC_FILTERRESET'); ?> " />
		</td>
	</tr>
</table>
<table class="adminlist">
<thead>
	<tr>
		<th width="1%" align="left">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
		</th>
		<th width="20%" nowrap="nowrap" align="center">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_NAME' ), 'c.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="9%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ID' ), 'c.contact_number', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_CONTACT' ), 'c.contact', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="22%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_EMAIL' ), 'c.email', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
        <th width="20%" align="center">

			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ADDRESS' ), 'c.address', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="9">
			<?php echo $this->pageNav->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<?php
$k = 0;
for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );
	$link 		= JRoute::_( 'index.php?option=com_ccinvoices&controller=contacts&task=edit&cid[]='. $row->id );
	?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>"><?php echo $row->name; ?></a>
		</td>
		<td align="center">
			<?php echo $row->contact_number; ?>
		</td>
		<td align="center">
			<?php echo $row->contact; ?>
		</td>
		<td align="center">
			<?php echo $row->email; ?>
		</td>
		<td align="center">
			<?php echo substr($row->address, 0, 15); ?>
		</td>
	</tr>
	<?php
	$k = 1 - $k;
}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked"  id="boxchecked" value="0" />
<input type="hidden" name="controller" value="contacts" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>

<script type="text/javascript">
<!--
if(document.getElementById('system-message'))
{
	var tagg = document.getElementsByTagName('dd');
	var tagcount = tagg.length ;
	if(tagcount > 1)
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
			var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
 			if (ieversion>=7)
 			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '152px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '144px';
		}
	} else
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
 			var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			if (ieversion>=7)
			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '102px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '94px';
		}
	}
}
//-->
</script>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>