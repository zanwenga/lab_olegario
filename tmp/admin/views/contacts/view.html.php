<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');
class ccInvoicesViewcontacts extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db =& JFactory::getDBO();
		$model = & $this->getModel();

		if($this->_layout == 'form')
		{
			$row		= $this->get('contacts');
			$query	= "SELECT max(contact_number) FROM #__ccinvoices_contacts";
		        $db->setQuery($query);
		        $contact_id1 = $db->loadResult();
		        $contact_number = $contact_id1 + 1 ;
			$text = $row->id ? JText::_( 'CC_EDIT' ) : JText::_( 'CC_NEW' );
	        JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).': <small><small> ' . $text.' </small></small>', 'ccinvoices.png' );
			JToolBarHelper::apply();
			JToolBarHelper::save();
			JToolBarHelper::cancel();
			$this->assignRef('row',			$row);
			$this->assignRef('contact_number',		$contact_number);
		}
		elseif($this->_layout == 'assigncontact')
		{
			$document =& JFactory::getDocument();
			$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/ccinvoice.css' );
			$inv_id = JRequest::getInt("inv_id","");
			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');
			$rows		= $this->get('data');
			$pagination		= $this->get('pagination');

			$sql = "SELECT c.id,c.name,c.contact,c.contact_number,c.address,c.email  FROM #__ccinvoices_contacts AS c"
				. " LEFT JOIN #__ccinvoices_invoices AS i ON i.contact_id = c.id "
				. " WHERE iu.contact_id = ".$inv_id
				;
			$db->setQuery($sql);
			$userAssined = $db->loadObjectList();

			$sql = "SELECT * FROM #__ccinvoices_contacts WHERE id =".$inv_id." LIMIT 1";
			$db->setQuery($sql);
			$contactDetails = $db->loadObject();
			if($this->versionCompare()!="1.5")
			{
				$query = 'SELECT id as value,title AS text'
					. ' FROM #__usergroups'
					. ' WHERE title != "ROOT"'
					. ' AND title != "USERS"'
				;
			}
			else
			{
				$query = 'SELECT id as value,name AS text'
					. ' FROM #__core_acl_aro_groups'
					. ' WHERE name != "ROOT"'
					. ' AND name != "USERS"'
				;
			}
			$db->setQuery( $query);
			$types[] 		= JHTML::_('select.option',  '0', '- '. JText::_( 'CC_SELECT_GROUP' ) .' -' );
			foreach( $db->loadObjectList() as $rec )
			{
				$types[] = JHTML::_('select.option',  $rec->value, JText::_( $rec->text ) );
			}
			$lists['type'] 	= JHTML::_('select.genericlist',   $types, 'usergroup', 'class="inputbox" size="1" ', 'value', 'text', "0" );


			$lists['order_Dir']	= $sortOrder;
			$lists['order']		= $sortColumn;
			$lists['search'] = $filter;

			$this->assignRef("rows",$rows);
			$this->assignRef("userAssined",$userAssined);
			$this->assignRef("inv_id",$inv_id);
			$this->assignRef("contactDetails",$contactDetails);
			$this->assignRef("lists",$lists);
		}
		else
		{
			JHTML::_('behavior.calendar');
	        JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ) . ': <small><small> ' .  JText::_( 'CC_CONTACT' ).' </small></small>', 'ccinvoices.png' );
			JToolBarHelper::addNewX();
			JToolBarHelper::editListX();
			//JToolBarHelper::custom('send','send.png','send_f2.png',JText::_( 'CC_INVOICES_TOOLBAR_SEND'),false);
			JToolBarHelper::deleteListX();

			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');
			$contacts		= $this->get('data');
			$pagination		= $this->get('pagination');
			$rows=$contacts;
			$ordering = ($sortColumn == 'ordering');
			$lists['order_Dir']	= $sortOrder;
			$lists['order']		= $sortColumn;
			$lists['search']		= $filter;
			// search filter
			//$lists['search']= $search;
			$this->assignRef('items',		$rows);
			$this->assignRef('pageNav',		$pagination);
			$this->assignRef('lists',		$lists);
		}
				/* Component Footer Information Starts*/
		$file = JPATH_COMPONENT_ADMINISTRATOR.DS.'install.xml';
		$xml = JFactory::getXMLParser('Simple');
		$xml->loadFile($file);
		$xml = $xml->document;
		$c_version = $xml->version[0]->data();
		$c_name = $xml->name[0]->data();
		//echo $c_version.$c_name;exit;
		/* Check for New Version */
		$myReadAccess= new versionRead('http://www.chillcreations.com/versionnumbers.txt');
		if($data = $myReadAccess->getFileContents()) {
			$pieces = explode("\n", $data);
			foreach($pieces as $piece)
			{
				$small_pieces[] = explode(",", $piece);
			}
			$versionContent = "";
 			foreach( $small_pieces as $small_piece)
			{
  				if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) != 'none') {
					$versionContent ="<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a>";
					$versionContent .="<br/>".$small_piece[3]."</div>";
  				}
				else if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) == 'none') {
					$versionContent = "<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a></div>";
				}
			}
		}
		if(isset($versionContent) && $versionContent != "") {
			$this->assignRef('versionContent',	$versionContent);
		}
		$this->assignRef('version',		$c_version);
		$this->assignRef('name',		$c_name);
		parent::display($tpl);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}

}
?>