<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
class ccInvoicesControllerConfiguration extends JController
{
	function __construct()
    {
        //Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'configuration');
        }
        $this->item_type = 'Configuration';
        parent::__construct();
		$this->registerTask( 'apply', 'save' );
    }
	function reset_inv()
	{
		$db = &JFactory::getDBO();
		$sql = "UPDATE #__ccinvoices_invoices SET reset_inv = 1";
		$db->setQuery($sql);
		if($db->query())
		{
			$msg = JText::_("CC_INVOICE_RESET_SUCC");
		}else
		{
			$msg = JText::_("CC_INVOICE_RESET_FAILD");
		}

		$this->setRedirect( 'index.php?option=com_ccinvoices' ,$msg);
	}
	function save()
	{
		$post	= JRequest::get( 'post' );
		$model = $this->getModel('configuration');
		if ($model->store())
		{
			$msg = JText::_( 'CC_CONFIG_SAVED_SUCCESS' );
		}else {
			$msg = JText::_( 'CC_CONFIG_SAVED_ERROR' );
		}
		if($post['task'] == 'apply')
		{
			$link 	= 'index.php?option=com_ccinvoices&controller=configuration';
		}else
		{
			$link = 'index.php?option=com_ccinvoices';
		}
		$this->setRedirect($link, $msg);
	}
}
?>