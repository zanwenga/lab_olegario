<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport('joomla.application.component.model');

class ccInvoicesModelConfiguration extends JModel
{
    function __construct()
    {
		parent::__construct();
		$array = JRequest::getVar('id',  0, '', 'array');
		$this->setId((int)$array[0]);
    }
   	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
   	function &getData()
	{
		$row =& $this->getTable();
		$row->load( $this->_id );
		return $row;
	}
	// save
	function store()
	{
		$db	= & JFactory::getDBO();
		$row =& $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );


		$logo = JRequest::getVar('logo', null, 'files', 'array' );
		$img_path =	JPATH_ADMINISTRATOR.DS."components".DS."com_ccinvoices".DS."assets".DS."logo".DS;

		$default_note = $this->convertImgTags($post["default_note"]);

		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		$row->default_note = $default_note;

		if($logo['name'] != '')
		{
			jimport('joomla.filesystem.file');
			$tmp_file_name = time().".".JFile::getExt($logo['name']);
			if (!JFile::upload($logo['tmp_name'], $img_path.$tmp_file_name))
			{
				return 0;
			}else
			{
				$row->logo = $tmp_file_name;
			}
		}
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}

	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}
}
?>