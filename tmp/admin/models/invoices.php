<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.model');

class ccInvoicesModelInvoices extends JModel
{

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;
	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function _buildQuery()
	{
		$mainframe =& JFactory::getApplication();
		$db =& JFactory::getDBO();

		$filter				= JRequest::getVar('invoice_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccinvoices.invoices.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccinvoices.invoices.sortOrder',$sortOrder);
		$clientid = JRequest::getInt("clientid","");

		$where = "";
		$where.= 'custom_invoice_number LIKE "%'.$db->getEscaped($filter).'%" ' .
				'OR LOWER(b.name) LIKE '.$db->Quote( '%'.$db->getEscaped( $filter, true ).'%', false ).'' .
				'OR LOWER(s.number) LIKE '.$db->Quote( '%'.$db->getEscaped( $filter, true ).'%', false ).'';
		if($clientid !="")
		{
			$where.= "AND s.contact_id=".$clientid;
		}
		$this->_query="SELECT s.*,b.name AS name,b.contact AS contact FROM #__ccinvoices_invoices AS s " .
				"LEFT JOIN #__ccinvoices_contacts AS b ON b.id = s.contact_id " .
				"WHERE $where ORDER BY $sortColumn $sortOrder";
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	function __construct()
	{
		parent::__construct();

		$this->_buildQuery();
		$mainframe =& JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('com_ccinvoices.invoices.limit', $limit);
		$this->setState('com_ccinvoices.invoices.limitstart', $limitstart);
	}
	function getData()
	{
		if (empty($this->_data))
			$this->_data=$this->_getList($this->_query,$this->getState('com_ccinvoices.invoices.limitstart'), $this->getState('com_ccinvoices.invoices.limit'));
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getListCount($this->_query);
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccinvoices.invoices.limitstart'), $this->getState('com_ccinvoices.invoices.limit'));
		}
		return $this->_pagination;
	}
	function getInvoices()
	{
		$cid = JRequest::getVar('cid');
		if(is_array($cid)) $cid = intval($cid[0]);
		$row= & JTable::getInstance('invoices','Table');
		$row->load($cid);
		return $row;
	}

    function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

    function contactsearch($contactname)
    {
		$db 	=& JFactory::getDBO();
		$query	= "SELECT id,name FROM #__ccinvoices_contacts WHERE  name like'%$contactname%'";
        $db->setQuery($query);
        $contactsearch = $db->loadObjectlist();
        return $contactsearch;
    }

    function getinvoicecontact($contactid)
    {
    	$db 	=& JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_contacts WHERE  id='$contactid'";
        $db->setQuery($query);
        $value = $db->loadObject();
        return $value;
    }

    function updateinvoicecontact()
    {
		$post['id']=JRequest::getInt('cc_id');
		$post['address']=JRequest::getVar('cc_address', '', 'get', 'string', JREQUEST_ALLOWRAW);
		$post['address'] = str_replace("|", "\n",$post['address']);
		$post['contact']=JRequest::getVar('cc_contact');
		$post['email']=JRequest::getVar('cc_email');
		$post['name']=JRequest::getVar('cc_name');
		$post['contact_number']=JRequest::getVar('contact_number');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$row =& JTable::getInstance('contacts', 'Table');
		// exit;
		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		//echo $row->id;
		echo $row->id;
    }

	function invoicetax()
	{
		$db 	=& JFactory::getDBO();
		$query	= "SELECT value FROM #__tax";
	    $db->setQuery($query);
		$value[] = JHTML::_('select.option',  '0', '- '. JText::_( 'CC_SELECT_STATE' ) .' -' );
		$value = array_merge($value, $db->loadObjectList());
        return $value;
	}

    function store()
	{
		$db			=& JFactory::getDBO();
        $resend	= JRequest::getVar( 'resend','');
        $send	= JRequest::getVar( 'send','');
        $reminder	= JRequest::getVar( 'reminder','');
        $id	= JRequest::getInt( 'id');
		$row =& $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$invoicenumber=$post['number'];
		$previousinvoiceno=$post['numbercheck'];
		if($previousinvoiceno == $invoicenumber )
		{
            $post['numbercheck'] =0;
		}else
		{
			 $post['numbercheck'] =$previousinvoiceno;
		}
		if($id =='0' && $send !='' )
		{
               $post['invoice_sent_date']=date("Y-m-d");
               $post['status']='2';
               $post['communication']='1';
		}else if($reminder)
		{
			$post['communication']='3';
		}else
		{
			$post['communication']=$post['communication'];
		}
		$contact_id = $this->savecontactus();

	    $quantity=$post['quantity'];
		$pname=$post['pname'];
		$amount=$post['price'];
		$tax=$post['tax'];
		$quant='';
		$pnames='';
		$amounts='';
	    $taxes='';
		for($i=0;$i<count($quantity);$i++)
		{
			$quant.= $quantity[$i];
			$pnames.= $pname[$i];
			$amounts.= $amount[$i];
			$taxes.= $tax[$i];
			if($i != (count($quantity)-1))
			{
				$quant .='|';
				$pnames .='|';
				$amounts .='|';
				$taxes .='|';
			}
		}
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['price'] =$amounts;
		$post['tax'] =$taxes;

		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$config = $db->loadObject();

		if (!$row->bind($post)) {
			$this->setError($row->getError());
			return 0;
		}
		if($row->status != 1)
		    if(trim($config->invoice_format)=="")
				$row->custom_invoice_number="";
		if($row->status == 1)
		{
			$row->number = "";
		}else if($row->status != 1 && $row->number == 0)
		{
			$query = 'SELECT invoice_start from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
			$db->setQuery($query);
			$confRow = $db->loadObject();
		    $invoicenumbercheck=$this->invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}
			$row->number = $new_inv_id1;
		}
	//	$row->custom_invoice_number = $invoice_format;
		$row->contact_id = $contact_id;
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		// $insertid=$db->insertid();
		return $row->id;
	}

	function savecontactus()
	{
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$row =& JTable::getInstance('contacts', 'Table');
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$post['id']=$post['contact_id'];

		// $post['invoiceid']=$invoiceid;
		// exit;
		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row =& $this->getTable();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$contctrow =& JTable::getInstance('contacts', 'Table');
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}


	function contactinvoice($clientid)
	{
      	$db 	=& JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_contacts WHERE  id='$clientid'";
        $db->setQuery($query);
        $contactinvoice = $db->loadObject();
        return $contactinvoice;
	}

	function invoicenumbercheck()
	{
   		$db 	=& JFactory::getDBO();
		$query	= "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = ''";
        $db->setQuery($query);
        $invoicenumbercheck = $db->loadResult();
        return $invoicenumbercheck;
   }

	function invoicenumbercheck1($invoicenumbercheck)
	{
   		$y=$invoicenumbercheck;
   	    for ($i = $y; $i <= $y; $i++)
   	    {
	        $db 	=& JFactory::getDBO();
			$query	= "SELECT number FROM #__ccinvoices_invoices where number='$y'";
	        $db->setQuery($query);
	        $invoicenumbercheck1 = $db->loadResult();
	   	    if($invoicenumbercheck1)
	   	    {
	   	    	$y++;
	   	    }else
	   	    {
				return $y;
	   	    	break;
	   	    }
	   }
	}

	function lastinvoicenumber()
	{
		$db 	=& JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_invoices  where reset_inv = '' ORDER BY id DESC Limit 1";
        $db->setQuery($query);
        $lastinvoicenumber = $db->loadObject();
        return $lastinvoicenumber;
	}

	function gettemplatelayout($id)
	{
		jimport("joomla.filesystem.file");
		$db 	=& JFactory::getDBO();
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$template =$db->loadResult();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id=".$id." LIMIT 1";
	    $db->setQuery($query);
		$invRow =$db->loadObject();

		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();

		$query = 'SELECT i.*,c.name,c.contact,c.address,c.email,c.contact_number'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
        $quantity= explode("|",$rows->quantity);
		$pname=explode("|",$rows->pname);
		$amount=explode("|",$rows->price);
		$tax=explode("|",$rows->tax);
		$quant='';
		$pnames='';
		$amounts='';
        $taxes='';
        $subTotal='';
        $tax_percent = '';
        $breaka="<br/>";
        $price = '';
		$totalPrice = '';
		$excl_tax = '';
		$item_total_excl_tax = '';
		$invoice_total_excl_tax = '';
		$amt_tmp1 = '';
		$subto1 = '';
		$invoice_subtotal = '';
		$product_total_price = '';
		$inv_subtotal_amount = '';
		$op_val['product_total_price'] = '';
		$op_val['invoice_subtotal_amount'] = '';
		$order_tax_total_split_tmp = '';
		$tax_rate_values = array();
		$inv_tax_total = '';
                $invoice_discount_price = 0;
		for($i=0;$i<count($quantity);$i++)
		{
			$breakVariable = "";
			$produtStringLen = strlen($pname[$i]);
			$noOrBr = $produtStringLen / 30;
			for($k=1;$k<=$noOrBr;$k++)
			{
				$breakVariable = $breakVariable."<br/>";
			}
			if($breakVariable=="")
				$breakVariable="<br/>";
			$quant.= $quantity[$i].$breakVariable;
			$pnames.=$pname[$i]."<br/>";

			//Discount Calc Ends
			$amounts.= $this->changeCurrencyFormat($amount[$i]).$breakVariable;
			$product_total_price += $amount[$i] * $quantity[$i];
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$inv_subtotal_amount += $disc_amt* $quantity[$i];

			}else
			{
				$inv_subtotal_amount += $amount[$i] * $quantity[$i];
			}
			/*$price += $amount[$i] * $quantity[$i];
			$price_tmp = $amount[$i] * $quantity[$i];
			$totalPrice += $price_tmp * $tax[$i] / 100;
            */
            $discount_price = "0";
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$amt_tmp1 = $disc_amt * $tax[$i] / 100;
				$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}else
			{
				$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
				$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}

			$amt_tmp = $amount[$i] * $tax[$i] / 100;
			$taxes.= $this->changeCurrencyFormat($amt_tmp).$breakVariable;
			$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
			$invoice_subtotal += $subto;
			$subTotal.= $this->changeCurrencyFormat($subto).$breakVariable;
			//{item_total_excl_tax}
			$excl_tax = $amount[$i]* $quantity[$i];
			$invoice_total_excl_tax += $excl_tax;
			$item_total_excl_tax.= $this->changeCurrencyFormat($excl_tax).$breakVariable;
			if($tax[$i] == '')
			{
				$tax_percent .= "0%".$breakVariable;
			}else
			{
				$tax_percent .= $tax[$i]."%".$breakVariable;
			}

			$tmp = '';
			$tax_rate = $tax[$i];
			$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
			$inv_tax_total +=$product_tax_amt;
			if(count($tax_rate_values) == 0)
			{
				$tax_rate_values[$tax_rate] = $product_tax_amt;
			}else
			{
				if (array_key_exists($tax_rate, $tax_rate_values))
				{
					$tmp = $tax_rate_values[$tax_rate];
				    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
				}else
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}
			}
		}
		ksort($tax_rate_values);
		foreach($tax_rate_values as $key=>$value)
		{
			$order_tax_total_split_tmp .= $this->changeCurrencyFormat($value)."&nbsp;(".$key."%)&nbsp;<br/>";
		}
	    //{order_tax_total_split}
	    $orderitem_tax_total = $this->changeCurrencyFormat($inv_tax_total);
	    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
	    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
	    $order_tax_total_split .= $order_tax_total_split_tmp;
	    $op_val['tax_total_split'] = $order_tax_total_split;
	    $op_val['invoice_tax_total_split'] = $order_tax_total_split;
		//print_r ($tax_rate_values);exit;
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['price'] =$amounts;
		$post['tax'] =$taxes;
        /* contact values */
		$op_val['invoice_note'] = $rows->note;
		//GETTING PAYMENT METHOD NAMES
		$paymentmethodnames=$this->getPaymentMethodsName();
		$op_val['payment_methods']= $paymentmethodnames;
        $op_val['contact_name'] = $rows->name;
        $op_val['contact_number'] = $rows->contact_number;
		$op_val['contact'] = $rows->contact;
		$contact_address = str_replace("\n", "<br/>", $rows->address);
		$op_val['contact_address'] = $contact_address;
		$op_val['contact_email'] = $rows->email;
	    $op_val['invoice_number'] = $this->getInvoiceNumberFormat($invRow->number);
		$op_val['invoice_date'] = $this->dateChangeFormat($rows->invoice_date,$conf->date_format);
		$op_val['invoice_due_date'] = $this->dateChangeFormat( $rows->duedate,$conf->date_format);
		if(strtotime($invRow->invoice_sent_date) == '' OR trim($invRow->invoice_sent_date)=="0000-00-00" OR trim($invRow->invoice_sent_date)=="11-30-99" OR trim($invRow->invoice_sent_date)=="30-11-99")
		{
			$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
		}
		else
		{
		$op_val['invoice_sent_date'] = $this->dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
		}
       	$op_val['item_quantity'] = $quant;
	    $op_val['item_name'] = $pnames;
		$op_val['item_amount'] = $amounts;
		$op_val['item_tax'] = $taxes;
		if($rows->discount == '' OR $rows->discount == '0')
		{
			$op_val['invoice_discount'] = '';
		}else
		{
			$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
		}
		$op_val['product_total_price'] = $this->changeCurrencyFormat($product_total_price);
		$op_val['invoice_subtotal_amount'] = $this->changeCurrencyFormat($inv_subtotal_amount);
		$invoice_discount_price=$invoice_subtotal;
		$op_val['invoice_discount_price']=number_format($invoice_discount_price * $rows->discount /100,2);
		$op_val['invoice_discount_amount']=number_format($invoice_discount_price * $rows->discount /100,2);

		$op_val['invoice_tax'] = $this->changeCurrencyFormat($rows->totaltax);
		$op_val['tax_percentage'] = $tax_percent;
		$op_val['item_tax_percentage'] = $tax_percent;
		$op_val['invoice_total'] = '';
		$op_val['invoice_total_excl_tax'] = '';
		$op_val['invoice_total'] = $this->changeCurrencyFormat($rows->total);
	    $op_val['invoice_total_excl_tax'] = $this->changeCurrencyFormat($invoice_total_excl_tax);
	    $op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];
		$op_val['item_total_excl_tax'] = $item_total_excl_tax;
		$op_val['item_total_incl_tax'] = $subTotal;
		$op_val['invoice_subtotal'] = $this->changeCurrencyFormat($invoice_subtotal);

		$op_val['product_tax'] = $taxes;
		$op_val['item_tax_amount'] = $taxes;
		$op_val['user_name'] = $conf->user_name;
	//	$op_val['user_email'] =  $conf->user_email;
		$op_val['company_name'] = $conf->user_company;
		$op_val['company_email'] = $conf->company_email;
		$op_val['company_phone'] = $conf->company_phone;
		$company_address = str_replace("\n", "<br/>", $conf->company_address);
		$op_val['company_address'] = $company_address;
		$other_details = str_replace("\n", "<br/>", $conf->other_details);
		$op_val['company_details'] = $other_details;
		$op_val['company_url'] = $conf->company_url;
		$op_val['tax_id'] = $conf->tax_id;
		$op_val['company_tax_id'] = $conf->tax_id;
		$logo_path_check =  JPATH_SITE.DS."administrator".DS."components".DS."com_ccinvoices".DS."assets".DS."logo".DS.$conf->logo;
		if($conf->logo != "")
		{
			if(JFile::exists($logo_path_check))
			{
		$op_val['logo'] = '<img src="'.JURI::root()."administrator/components/com_ccinvoices/assets/logo/".$conf->logo.'"/>' ;
		}else
		{
				$op_val['logo'] = "";
			}
		}else
		{
			$op_val['logo'] = '';
		}
		$template = $this->convertImgTags($template);
		$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );

		//Get Custom Tags From The Plugin
		$val["id"] = $id;
		$options = array( $val);
		JPluginHelper::importPlugin( 'ccinvoicetags' );
		$dispatcher =& JDispatcher::getInstance();
		$customTag = $dispatcher->trigger( '_getCustomTags',$options);
		foreach ($customTag as $customTags)
		{
			foreach ($customTags as $customTagName=>$value)
			{
				$op_val[$customTagName] = $value;
			}
		}

		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$template = str_replace($find,$replace,$template);
			$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
		}
		return $template;
	}


	function gettemplateCSV($id,$tempnum)
	{
		$db 	=& JFactory::getDBO();
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 2 LIMIT 1";
	    $db->setQuery($query);
	    $template=array();
		$template = strip_tags($db->loadResult());
		//print_r($template);exit;
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id=".$id." LIMIT 1";
	    $db->setQuery($query);
		$invRow =$db->loadObject();

		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$query = 'SELECT i.*,c.name,c.contact,c.address,c.email,c.contact_number'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
        $quantity= explode("|",$rows->quantity);
		$pname=explode("|",$rows->pname);
		$amount=explode("|",$rows->price);
		$tax=explode("|",$rows->tax);
		$quant='';
		$pnames='';
		$amounts='';
        $taxes='';
        $subTotal='';
        $tax_percent = '';
        $breaka="<br/>";
        $price = '';
		$totalPrice = '';
		$excl_tax = '';
		$item_total_excl_tax = '';
		$invoice_total_excl_tax = '';
		$amt_tmp1 = '';
		$subto1 = '';
		$invoice_subtotal = '';
		$product_total_price = '';
		$inv_subtotal_amount = '';
		$op_val['product_total_price'] = '';
		$op_val['invoice_subtotal_amount'] = '';
		$order_tax_total_split_tmp = '';
		$tax_rate_values = array();
		$inv_tax_total = '';
		$productstr="";
		$invoice_discount_price = 0;
		for($i=0;$i<count($quantity);$i++)
		{
			//$invoice_sub_tot=
			$quant.= $quantity[$i]."<br/>";
			$pnames.= $pname[$i]."<br/>";

			//Discount Calc Starts

			//Discount Calc Ends
			$amounts.= $amount[$i]."<br/>";
			$product_total_price += $amount[$i] * $quantity[$i];
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$inv_subtotal_amount += $disc_amt* $quantity[$i];

			}else
			{
				$inv_subtotal_amount += $amount[$i] * $quantity[$i];
			}
            $discount_price = "0";
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$amt_tmp1 = $disc_amt * $tax[$i] / 100;
				$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}else
			{
				$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
				$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}

			$amt_tmp = $amount[$i] * $tax[$i] / 100;
			$taxes.= $amt_tmp."<br/>";
			$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
			$invoice_subtotal += $subto;
			$subTotal.= $subto."<br/>";
			//{item_total_excl_tax}
			$excl_tax = $amount[$i]* $quantity[$i];
			$invoice_total_excl_tax += $excl_tax;
			$item_total_excl_tax.= $excl_tax."<br/>";
			if($tax[$i] == '')
			{
				$tax_percent .= "0%<br/>";
			}else
			{
			$tax_percent .= $tax[$i]."%<br/>";
			}

			$tmp = '';
			$tax_rate = $tax[$i];
			$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
			$inv_tax_total +=$product_tax_amt;
			if(count($tax_rate_values) == 0)
			{
				$tax_rate_values[$tax_rate] = $product_tax_amt;
			}else
			{
				if (array_key_exists($tax_rate, $tax_rate_values))
				{
					$tmp = $tax_rate_values[$tax_rate];
				    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
				}else
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}
			}
		}

		if($tempnum!="0")
		{
			$tempst="";
			$str1="";
			$item_nm=strpos($template,"item_name");
			$item_qty=strpos($template,"item_quantity");
			$item_amt=strpos($template,"{item_price}");
			if(!$item_amt)
				$item_amt= strpos($template,"item_amount");
			else
				$template=str_replace("{item_price}","{item_amount}",$template);
			$item_tax= strpos($template,"{item_tax}");
			if(!$item_tax)
				$item_tax= strpos($template,"item_tax_percentage");
			else
				$template=str_replace("{item_tax}","{item_tax_percentage}",$template);

			if(trim($item_qty)!="" OR trim($item_nm)!="" OR trim($item_amt)!="" OR trim($item_tax)!="")
			{
				if(trim($item_qty)=="")
					$item_qty="1501";
				if(trim($item_nm)=="")
					$item_nm="1502";
				if(trim($item_amt)=="")
					$item_amt="1503";
				if(trim($item_tax)=="")
					$item_tax="1504";
	//item
				$tempst="";
				if($item_nm < $item_qty AND $item_nm < $item_amt AND $item_nm < $item_tax)
				{
					$str1[0]='item_name';
					$tempst[0]=$pname;
				}
				elseif(($item_nm < $item_qty AND $item_nm < $item_amt) OR ($item_nm < $item_tax AND $item_nm < $item_amt) OR ($item_nm < $item_tax AND $item_nm < $item_qty))
				{
					$str1[1]='item_name';
					$tempst[1]=$pname;
				}
				elseif($item_nm < $item_qty OR $item_nm < $item_amt OR $item_nm < $item_tax)
				{
					$str1[2]='item_name';
					$tempst[2]=$pname;
				}
				else
				{
					$str1[3]='item_name';
					$tempst[3]=$pname;
				}
	//qty
				if($item_qty < $item_nm AND $item_qty < $item_amt AND $item_qty < $item_tax)
				{
					$str1[0]='item_quantity';
					$tempst[0]=$quantity;
				}
				elseif(($item_qty < $item_nm AND $item_qty < $item_amt) OR ($item_qty < $item_tax AND $item_qty < $item_amt) OR ($item_qty < $item_tax AND $item_qty < $item_nm))
				{
					$str1[1]='item_quantity';
					$tempst[1]=$quantity;
				}
				elseif($item_qty < $item_nm OR $item_qty < $item_amt OR $item_qty < $item_tax)
				{
					$str1[2]='item_quantity';
					$tempst[2]=$quantity;
				}
				else
				{
					$str1[3]='item_quantity';
					$tempst[3]=$quantity;
				}
	//itemam
				if($item_amt < $item_nm AND $item_amt < $item_qty AND $item_amt < $item_tax)
				{
					$str1[0]='item_amount';
					$tempst[0]=$amount;
				}
				elseif(($item_amt < $item_nm AND $item_amt < $item_qty) OR ($item_amt < $item_tax AND $item_amt < $item_qty) OR ($item_amt < $item_tax AND $item_amt < $item_nm))
				{
					$str1[1]='item_amount';
					$tempst[1]=$amount;
				}
				elseif($item_amt < $item_nm OR $item_amt < $item_qty OR $item_amt < $item_tax)
				{
					$str1[2]='item_amount';
					$tempst[2]=$amount;
				}
				else
				{
					$str1[3]='item_amount';
					$tempst[3]=$amount;
				}
	//itemtax
				if($item_tax < $item_nm AND $item_tax < $item_qty AND $item_tax < $item_amt)
				{
					$str1[0]='item_tax_percentage';
					$tempst[0]=$tax;
				}
				elseif(($item_tax < $item_nm AND $item_tax < $item_qty) OR ($item_tax < $item_amt AND $item_tax < $item_qty) OR ($item_tax < $item_amt AND $item_tax < $item_nm))
				{
					$str1[1]='item_tax_percentage';
					$tempst[1]=$tax;
				}
				elseif($item_tax < $item_nm OR $item_tax < $item_qty OR $item_tax < $item_amt)
				{
					$str1[2]='item_tax_percentage';
					$tempst[2]=$tax;
				}
				else
				{
					$str1[3]='item_tax_percentage';
					$tempst[3]=$tax;
				}
				//create array for invoice product
				for($i=0;$i<=$tempnum;$i++)
				{

					if($i>=count($quantity) AND $tempnum>"1")
					{

					}
					else
					{
						if(count($quantity)>$i)
						{
							if($str1[0]=="item_amount")
								$tempst[0][$i]=$tempst[0][$i];
							if($str1[1]=="item_amount")
								$tempst[1][$i]=$tempst[1][$i];
							if($str1[2]=="item_amount")
								$tempst[2][$i]=$tempst[2][$i];
							if($str1[3]=="item_amount")
								$tempst[3][$i]=$tempst[3][$i];

							if(strpos($template,$str1[0]))
								$op_val[$str1[0]][$i] = $tempst[0][$i];
							if(strpos($template,$str1[1]))
								$op_val[$str1[1]][$i]=$tempst[1][$i];
							if(strpos($template,$str1[2]))
								$op_val[$str1[2]][$i] = $tempst[2][$i];
							if(strpos($template,$str1[3]))
								$op_val[$str1[3]][$i] = $tempst[3][$i];
						}
					}
				}
			}
			else
			{
				$op_val['tax_percentage'] = $tax_percent;
				$op_val['item_tax_percentage'] = $tax_percent;
			}
		}
		ksort($tax_rate_values);
		foreach($tax_rate_values as $key=>$value)
		{
			$order_tax_total_split_tmp .= $value."&nbsp;(".$key."%)&nbsp;<br/>";
		}
	    //{order_tax_total_split}
	    $orderitem_tax_total = $inv_tax_total;

	    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
	    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
	    $order_tax_total_split .= $order_tax_total_split_tmp;
	    $op_val['tax_total_split'] = $order_tax_total_split;
	    $op_val['invoice_tax_total_split'] = $order_tax_total_split;
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['price'] =$amounts;
		$post['tax'] =$taxes;
        /* contact values */
		$op_val['invoice_note'] = $rows->note;
		//GETTING PAYMENT METHOD NAMES
		$paymentmethodnames=$this->getPaymentMethodsName();
		$op_val['payment_methods']= $paymentmethodnames;
        $op_val['contact_name'] = $rows->name;
        $op_val['contact_number'] = $rows->contact_number;
		$op_val['contact'] = $rows->contact;
		$contact_address = str_replace("\n", "<br>", $rows->address);
		$contact_address=str_replace(",",'`',$contact_address);
		$op_val['contact_address'] = $contact_address;
		$op_val['contact_email'] = $rows->email;
	    $op_val['invoice_number'] = $this->getInvoiceNumberFormat($invRow->number);
		$op_val['invoice_date'] = $this->dateChangeFormat($rows->invoice_date,$conf->date_format);
		$op_val['invoice_due_date'] = $this->dateChangeFormat( $rows->duedate,$conf->date_format);
		//Not sent yet
		if(strtotime($invRow->invoice_sent_date) == '' OR trim($invRow->invoice_sent_date)=="0000-00-00" OR trim($invRow->invoice_sent_date)=="11-30-99" OR trim($invRow->invoice_sent_date)=="30-11-99")
		{
			$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
		}
		else
		{
		$op_val['invoice_sent_date'] = $this->dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
		}

		if($rows->discount == '' OR $rows->discount == '0')
		{
			$op_val['invoice_discount'] = '';
		}else
		{
			$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
		}
		$op_val['product_total_price'] = $product_total_price;
		$op_val['invoice_subtotal_amount'] = $inv_subtotal_amount;
		$invoice_discount_price=$invoice_subtotal;
		$op_val['invoice_discount_price']=number_format($invoice_discount_price * $rows->discount /100,2);
		$op_val['invoice_discount_amount']=number_format($invoice_discount_price * $rows->discount /100,2);

		$op_val['invoice_tax'] = $rows->totaltax;
		$op_val['invoice_total'] = '';
		$op_val['invoice_total_excl_tax'] = '';
		$invoice_total=str_replace(",",'`',$rows->total);
		$op_val['invoice_total'] = $rows->total;
	    $op_val['invoice_total_excl_tax'] = $invoice_total_excl_tax;
	    $op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];
		$op_val['item_total_excl_tax'] = $item_total_excl_tax;
		$op_val['item_total_incl_tax'] = $subTotal;
		$invoice_subtotal=str_replace(",",'`',$invoice_subtotal);
		$op_val['invoice_subtotal'] = $invoice_subtotal;
		//$op_val['invoice_subtotal'] = $this->changeCurrencyFormat($invoice_subtotal);

		$op_val['product_tax'] = $taxes;
		$op_val['item_tax_amount'] = $taxes;
		$op_val['user_name'] = $conf->user_name;
		$op_val['company_name'] = $conf->user_company;
		$op_val['company_email'] = $conf->company_email;
		$op_val['company_phone'] = $conf->company_phone;
		$company_address = str_replace("\n", "<br/>", $conf->company_address);
		$op_val['company_address'] = $company_address;
		$other_details = str_replace("\n", "<br/>", $conf->other_details);
		$op_val['company_details'] = $other_details;
		$op_val['company_url'] = $conf->company_url;
		$op_val['tax_id'] = $conf->tax_id;
		$op_val['company_tax_id'] = $conf->tax_id;
		if($conf->logo != "")
		{
		$op_val['logo'] = '<img src="'.JURI::root()."administrator/components/com_ccinvoices/assets/logo/".$conf->logo.'"/>' ;
		}else
		{
			$op_val['logo'] = '';
		}
		$template = $this->convertImgTags($template);
		$templatereturn=$template;
		$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );
		//Get Custom Tags From The Plugin
		$val["id"] = $id;
		$options = array($val);
		JPluginHelper::importPlugin( 'ccinvoicetags' );
		$dispatcher =& JDispatcher::getInstance();
		$customTag = $dispatcher->trigger( '_getCustomTags',$options);
		$temparry="";
		foreach ($customTag as $customTags)
		{
			foreach ($customTags as $customTagName=>$value)
			{
				$op_val[$customTagName] = $value;
			}
		}
		if($tempnum!=0)
		{
			foreach($op_val as $op_vals=>$value)
			{
				if(is_array($value))
				{
					$arrval[]=$value;
					$arrkey[]="{".$op_vals."}";

					//$this->changeCurrencyFormat(
				}
			}

			$booleanvar=0;
			foreach($op_val as $op_vals=>$value)
			{
				if(is_array($value) )
				{
					if($booleanvar=="0")
					{
						$tempval="";
						$tempkey="";
						for($i=0;$i<=count($arrval[0])-1;$i++)
						{
							for($j=0;$j<=count($arrkey)-1;$j++)
							{
								$arrval[$j][$i]=str_replace(",",'`',$arrval[$j][$i]);
								$temparry[$i][$j]=$arrval[$j][$i];
								$tempval.=$arrval[$j][$i].",";
								$tempkey=$arrkey[$j];
							}
						}
						$booleanvar=1;
					}
				}
				else
				{
					$find = "{".$op_vals."}";
					$value=str_replace(",",'`',$value);
					$replace = $value;
					$template = str_replace($find,$replace,$template);
					$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
				}
			}
		}
		return array($template,$templatereturn,$temparry);
	}
	function getPaymentMethodsName()
	{
		$plugins_name=& JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher =& JDispatcher::getInstance();
		$methodnames = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$paymentmethodname="";
		if(count($methodnames)>0)
		{
			$i=0;
			foreach($methodnames as $pluginname)
			{
				if($i==count($methodnames)-2)
				{
					$paymentmethodname.=$pluginname["payment_method_note"]." or ";
				}
				else
				{
					$paymentmethodname.=$pluginname["payment_method_note"].", ";
				}
				$i=$i+1;
			}
		}
		$paymentmethodname=substr($paymentmethodname,0,strlen($paymentmethodname)-2);
		return $paymentmethodname;
	}
	function changeCurrencyFormat($cur_val)
	{
		$db 	=& JFactory::getDBO();
		$query	= "SELECT cformat,currency_symbol,symbol_display FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$format = $conf->cformat;
		$tmp = '';
		$price_amount = "";
		if($format == 0)
		{
			$tmp = number_format($cur_val, 2, '.', ',');
		}else if($format == 1)
		{
			$tmp = number_format($cur_val, 2, ',', '.');
		}else if($format == 2)
		{
			$tmp = number_format($cur_val, 2, '.', ' ');
		}
		if($conf->symbol_display == '1')
		{
			$price_amount = $conf->currency_symbol.$tmp;
		}else
		{
			$price_amount = $tmp.$conf->currency_symbol;
		}
		return $price_amount;
	}
	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}

	function invoicesendmessage()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$FromName	= $mainframe->getCfg('fromname');
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$contactus = $post['contact'];
		$invoice_number = $post['number'];
		$user_name = $post['name'];
		//$user_company = date("Y-m-d");
		$reminder	= JRequest::getVar( 'reminder');
		$resend	= JRequest::getVar( 'resend');
		$sql = "SELECT invoice_sent_date FROM #__ccinvoices_invoices WHERE number=".$invoice_number." LIMIT 1";
		$db->setQuery($sql);
		$invoice_sent_date = $db->loadResult();
		return ;
	}

	function invoicesendsubject()
	{
		$db = JFactory::getDBO();
		$sql = "SELECT default_email_sub,rem_email_sub FROM #__ccinvoices_configuration WHERE id = 1 LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
   		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
   	    $reminder	= JRequest::getVar( 'reminder');
		$op_val['invoice_number'] = $post['number'];

       	if($reminder == '1')
		{
			if($config->rem_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEREMINDER_SUB' );
		}else
		{
				$invoicesend_sub = $config->rem_email_sub;
			}
		}else
		{
			if($config->default_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEEMAIL_SUB' );
			}else
			{
				$invoicesend_sub = $config->default_email_sub;
			}
		}
		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$invoicesend_sub = str_replace($find,$replace,$invoicesend_sub);
		}
		return $invoicesend_sub;
	}
	function dateChangeFormat($date_temp,$date_format)
	{
		if(strtotime($date_temp) != '')
		{
		return JHTML::_('date',  strtotime($date_temp), $date_format);
		}else
		{
			return;
	}
	}

	function getInvoiceNumberFormat($invNum)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$conf = $db->loadObject();
		$invoice_format = $conf->invoice_format;
		if($invoice_format == '')
		{
			return $invNum;
		}else
		{
			$query = 'SELECT custom_invoice_number from #__ccinvoices_invoices WHERE number='.$invNum.' LIMIT 1 ';
			$db->setQuery($query);
			$custom_invoice_number = $db->loadResult();
			if($custom_invoice_number == '' || $custom_invoice_number == '0')
		{
				$custom_invoice_number = $invNum;
		}
			return $custom_invoice_number;
	}
}
}
?>