<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.model');

class ccInvoicesModelTemplates extends JModel
{

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;
	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function _buildQuery()
	{
		$mainframe =& JFactory::getApplication();
		$db =& JFactory::getDBO();

		$filter				= JRequest::getVar('invoice_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccinvoices.templates.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccinvoices.templates.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccinvoices.invoices.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccinvoices.invoices.sortOrder',$sortOrder);



		//$this->_query="SELECT s.*,b.name AS name,b.contact AS contact " .
		//		"FROM #__ccinvoices_invoices AS s LEFT JOIN #__ccinvoices_contacts " .
		//		"AS b ON b.id = s.contact_id WHERE custom_invoice_number " .
		//		"LIKE '%".$db->getEscaped($filter)."%' ORDER BY $sortColumn $sortOrder";
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	function __construct()
	{
		parent::__construct();

		$this->_buildQuery();
		$mainframe =& JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('com_ccinvoices.templates.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_ccinvoices.templates.limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('com_ccinvoices.templates.limit', $limit);
		$this->setState('com_ccinvoices.templates.limitstart', $limitstart);
	}
	function getData()
	{
		if (empty($this->_data))
			$this->_data=$this->_getList($this->_query,$this->getState('com_ccinvoices.templates.limitstart'), $this->getState('com_ccinvoices.templates.limit'));
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getListCount($this->_query);
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccinvoices.templates.limitstart'), $this->getState('com_ccinvoices.templates.limit'));
		}
		return $this->_pagination;
	}
	function getInvoices()
	{
		$cid = JRequest::getVar('cid');
		if(is_array($cid)) $cid = intval($cid[0]);
		$row= & JTable::getInstance('templates','Table');
		$row->load($cid);
		return $row;
	}

	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	// save

	function store()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$row =& $this->getTable();
		// Bind the form fields
		$post			= JRequest::get('post', JREQUEST_ALLOWRAW);
		$selectedid = JRequest::getInt( 'selected_id', '0', 'post', '' );
		//new code to save conf. data
		if($selectedid=="3")
		{
			$query = "UPDATE #__ccinvoices_configuration SET default_note=".$db->Quote($post['default_note'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

		}
		elseif($selectedid=="4")
		{
			$query = "UPDATE #__ccinvoices_configuration SET default_email_sub=".$db->Quote($post['default_email_sub']).",default_email=".$db->Quote($post['default_email'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

		}
		elseif($selectedid=="5")
		{
			$query = "UPDATE #__ccinvoices_configuration SET rem_email_sub=".$db->Quote($post['rem_email_sub']).",default_email_rem=".$db->Quote($post['default_email_rem'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

		}

		if (!$row->bind($post)) {
			return 0;
		}
		// Make sure data is valid
		if (!$row->check()) {
			return 0;
		}
		if (!$row->store())	{
			return 0;
		}
		return $row->id;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row =& $this->getTable();
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}
}
?>