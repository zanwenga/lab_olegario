<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.model');

class ccInvoicesModelContacts extends JModel
{

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;
	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function _buildQuery()
	{
		$mainframe =& JFactory::getApplication();
		$db =& JFactory::getDBO();

		$filter				= JRequest::getVar('invoice_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccinvoices.contacts.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccinvoices.contacts.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccinvoices.contacts.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccinvoices.contacts.sortOrder',$sortOrder);

		$this->_query="SELECT * FROM #__ccinvoices_contacts AS c WHERE name " .
				"LIKE '%".$db->getEscaped($filter)."%' OR " .
				"email LIKE '%".$db->getEscaped($filter)."%' ORDER BY $sortColumn $sortOrder";
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	function __construct()
	{
		parent::__construct();

		$this->_buildQuery();
		$mainframe =& JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('com_ccinvoices.contacts.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_ccinvoices.contacts.limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('com_ccinvoices.contacts.limit', $limit);
		$this->setState('com_ccinvoices.contacts.limitstart', $limitstart);
	}
	function getData()
	{
		if (empty($this->_data))
			$this->_data=$this->_getList($this->_query,$this->getState('com_ccinvoices.contacts.limitstart'), $this->getState('com_ccinvoices.contacts.limit'));
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getListCount($this->_query);
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccinvoices.contacts.limitstart'), $this->getState('com_ccinvoices.contacts.limit'));
		}
		return $this->_pagination;
	}
	function getContacts()
	{
		$cid = JRequest::getVar('cid');
		if(is_array($cid)) $cid = intval($cid[0]);
		$row= & JTable::getInstance('contacts','Table');
		$row->load($cid);
		return $row;
	}

   	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
	// save
	function store()
	{
		$db			=& JFactory::getDBO();
		$row =& $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		if (!$row->bind($post)) {
			$this->setError($row->getError());
			return 0;
		}
		$row->id = $post["contact_id"];
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row =& $this->getTable();
		$db 	=& JFactory::getDBO();
		$cannotdelcontactmsg=0;
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
			    $query	= "SELECT count(*) FROM #__ccinvoices_invoices where contact_id =".$cid."  LIMIT 1";
			    $db->setQuery($query);
			    $rows=$db->loadResult();
			    if($rows<=0)
			    {
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
			    else
			    {
					$cannotdelcontactmsg="1";
			    }
			}
		}
		return array(true,$cannotdelcontactmsg);
	}
}
?>