<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class TableConfiguration extends JTable
{
	var $id = null;
	var $invoice_format = null;
	var $invoice_start = null;
	var $date_format = null;
	var $default_due_days = null;
	var $default_tax = null;
	var $tax = null;
	var $currency_symbol = null;
	var $user_name = null;
	var $user_company = null;
	var $company_email = null;
	var $company_phone = null;
	var $company_url = null;
	var $logo = null;
	var $company_address = null;
	var $other_details = null;
	var $default_note = null;
	//var $default_email = null;
	//var $default_email_rem = null;
	var $symbol_display = null;
	var $cformat = null;
	var $email_cc = null;
	var $email_bcc = null;
	//var $default_email_sub = null;
	//var $rem_email_sub = null;
	var $tax_id = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_configuration', 'id', $db);
	}

	function check()
	{
		return true;
	}
}
?>