<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class Tabletemplates extends JTable
{
	var $id = null;
	var $invoice_template =null;
	var $title =null;
	var $edit_by  =null;
	var $edit_date  =null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_templates', 'id', $db);
	}

	function check()
	{
		return true;
	}
}
?>