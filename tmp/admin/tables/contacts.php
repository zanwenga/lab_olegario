<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class Tablecontacts extends JTable
{
	var $id = null;
	var $name = null;
	var $contact = null;
	var $contact_number = null;
	var $address = null;
	var $email = null;
	var $invoiceid = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_contacts', 'id', $db);
	}

	function check()
	{
		return true;
	}

}
?>