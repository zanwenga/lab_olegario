<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

$user = & JFactory::getUser();
$controllerName = JRequest::getCmd( 'controller', 'invoices' );
switch($controllerName)
{
	case 'contacts':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.$controllerName.'.php' );
		$controllerName = 'ccInvoicesController'.$controllerName;
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&controller=contacts', true);
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&controller=templates');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&controller=configuration');
	break;

	case 'templates':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.$controllerName.'.php' );
		$controllerName = 'ccInvoicesController'.$controllerName;
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&controller=contacts');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&controller=templates', true);
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&controller=configuration');
	break;

	case 'configuration':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.$controllerName.'.php' );
		$controllerName = 'ccInvoicesController'.$controllerName;
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&controller=contacts');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&controller=templates');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&controller=configuration', true);
	break;
	case 'assignusers':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'assignusers'.'.php' );
		$controllerName = 'ccInvoicesControllerassignusers';
	break;
	case 'assigncontacts':
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'assigncontact'.'.php' );
		$controllerName = 'ccInvoicesControllerassigncontacts';
	break;

	default:
		require_once( JPATH_COMPONENT.DS.'controllers'.DS.'invoices'.'.php' );
		$controllerName = 'ccInvoicesControllerinvoices';
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices', true);
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&controller=contacts');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&controller=templates');
		JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&controller=configuration');
	break;
}
require_once( JPATH_COMPONENT.DS.'helper'.DS.'version.php' );
$controller	= new $controllerName( );
$controller->execute( JRequest::getCmd('task'));
$controller->redirect();
?>