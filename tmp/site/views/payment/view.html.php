<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );
jimport('joomla.html.pagination');

class  ccinvoicesViewpayment extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = &JFactory::getDBO();
		$user	=& JFactory::getUser();
		$id = JRequest::getVar("id","");

		$action=JRequest::getVar('action','');
		$ptype=JRequest::getVar('ptype','');
		if($this->_layout != 'process')
		{
			require_once(JPATH_COMPONENT.DS."models".DS.'ccinvoices.php');
			$sql = "SELECT count(*) FROM #__ccinvoices_invoices WHERE MD5(id) ='".$id."'";
			$db->setQuery($sql);
			$count= $db->loadResult();
			if($count == 0)
			{
				JError::raiseNotice( 403, JText::_('CC_INVALID_INVOICE') );
				return;
			}
			$sql = "SELECT id,number,invoice_date,duedate,custom_invoice_number FROM #__ccinvoices_invoices WHERE MD5(id) ='".$id."' LIMIT 1";
			$db->setQuery($sql);
			$row = $db->loadObject();
			if(trim($row->custom_invoice_number) != '')
				$num = $row->custom_invoice_number;
			else
				$num = $row->number;

			$sql = "SELECT user_company,date_format FROM #__ccinvoices_configuration LIMIT 1";
			$db->setQuery($sql);
			$conf = $db->loadObject();
			$invoice_date  = ccinvoicesModelccinvoices::dateChangeFormat($row->invoice_date,$conf->date_format);
			$duedate = ccinvoicesModelccinvoices::dateChangeFormat($row->duedate,$conf->date_format);
			$user_company = $conf->user_company;

			$val["id"] = $row->id;
			$options = array( $val);
			JPluginHelper::importPlugin( 'ccinvoices_payment' );
			$dispatcher =& JDispatcher::getInstance();
			$PaymentMethodDetails = $dispatcher->trigger( 'onPaymentMethodList',$options);
			$plugin_info = JPluginHelper::getPlugin('ccinvoices_payment', $plugin=null);
			$PaymentMethodMessage="";
			$message="";

			if($action=="showresult")
			{
				$inv_arg["inv_id"] = $row->id;
				$arg = ($inv_arg);
				$PaymentMethodMessage = $dispatcher->trigger( 'onAfterFailedPayment',$arg);
				$message=$this->getPaymentPluginStatus($PaymentMethodMessage,$plugin_info,$ptype);
			}
			//$invNumber = ccinvoicesModelccinvoices::getInvoiceNumberFormat($num);
			$this->assignRef("num",$num);
			$this->assignRef("user_company",$user_company);
			$this->assignRef("invoice_date",$invoice_date);
			$this->assignRef("duedate",$duedate);
			$this->assignRef("PaymentMethodDetails",$PaymentMethodDetails);
			$this->assignRef("message",$message);
			$this->assignRef("plugin_info",$plugin_info);
			$this->assignRef("invID",$id);

		}else
		{

			JPluginHelper::importPlugin("ccinvoices_payment");
			$dispatcher =& JDispatcher::getInstance();
			$results = $dispatcher->trigger( 'onProcessPayment');
			$text = trim(implode("\n", $results));
			echo $text;
		}
		parent::display($tpl);
	}
	function getPaymentPluginStatus($PaymentMethodMessage,$plugin_infos,$ptype)
	{
		$i = 0;
		foreach($PaymentMethodMessage as $PaymentMethodMessages)
		{

			if($plugin_infos[$i]->name == $ptype)
			{
				return $PaymentMethodMessages;
			}
			$i++;
		}
	}
}