<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Getting menu id(Itemid)
//onAfterSuccessfulPayment()
$itemid = JRequest::getVar('Itemid');
?>

<style type="text/css">
#headStyle
{
	padding:.3em 1em;
	color:#66A3D3;
	background:#F4F9FE none repeat scroll 0 0;
	font-size:1.0em;
	line-height:2em;
	font-weight:bold;
	color: #678197;
	text-align:left;
	border-bottom: 1px solid #e5eff8;
	border-left:1px solid #e5eff8;
}
#headStyle a
{
	font-weight:bold;
	background-color: #F4F9FE;
	text-decoration:none;
}
#tableMain
{
	border-collapse:collapse;
	border-right:1px solid #E5EFF8;
	border-top:1px solid #E5EFF8;
	margin:1em auto;
}
#tdRecStyle {
	color:#678197;
	border-bottom:1px solid #e5eff8;
	border-left:1px solid #e5eff8;
	padding:.3em 1em;
	text-align:center;
	line-height:2em;
	}
.invoiceRow1
{
	background:#F7FBFF none repeat scroll 0 0;
}
#tdRecStyle1 {
	color:#678197;
	border-bottom:1px solid #e5eff8;
	border-left:1px solid #e5eff8;
	text-align:center;
	line-height:2em;


	}
#tdRecStyle1 a
{
	text-decoration: none;
	background-color: white;
}
#tdRecStyle1 a:hover
{
	text-decoration: none;
	background-color: white;
}

tr, td {
    border: 0 solid #DDDDDD;
}
</style>

<?php
if($this->message!="")
{
	?>

	<table style="width:100%;">
		<tr>
			<td align="center" width="100%" >
				<table cellpadding="0" cellspacing="10" style="width:80%;border: 1px solid rgb(0, 0, 0);background-color:#f0ffe9;border-color:#b4ffa3;">
					<tr>
						<td width="16"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/ticknew.png"></td>
						<td align="left">
							<b color="#060606">
							<?php
								echo $this->message;
							?>
							</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
<?php
}
?>



<table cellpadding="0" cellspacing="0" width="98%" border="0" >
	<tr>
		<td align="right" style="padding-right:10px;" valign="middle">
			<?php
				if($this->showPaymentIcon)
				{
					//echo JText::_( 'CC_PAYMENT_ICON_DESC' )."&nbsp;&nbsp;".$this->paymentIconImg;
					?>
						<div style="float:right;"><?php echo $this->paymentIconImg; ?></div><div style="float:right;margin-top:8px;"><?php echo JText::_( 'CC_PAYMENT_ICON_DESC' ); ?>&nbsp;&nbsp;</div>
					<?php
				}
			?>
		</td>
	</tr>
</table>


<form action="index.php"  method="post" name="adminForm">
<table cellpadding="0" cellspacing="0" width="100%" id="tableMain">
	<tr class="odd">
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_ID' )."&nbsp;", 'i.number', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<!--
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_CONTACT' )."&nbsp;", 'c.contact', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		-->
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_DATE' )."&nbsp;", 'i.invoice_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_DUE_DATE' )."&nbsp;", 'i.duedate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_AMOUNT' )."&nbsp;", 'i.total', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_STATUS' )."&nbsp;", 'i.status', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th id="headStyle">
			<?php echo JText::_( 'CC_INVOICE_ACTION' ); ?>
		</th>
	</tr>
<?php
$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
	$row = &$this->rows[$i];
?>
	<tr class="<?php echo "invoiceRow$k"; ?>">
		<td align="center" id="tdRecStyle">
			<?php
					if($row->custom_invoice_number != '')
					{
						echo $row->custom_invoice_number;
					}else
					{
						echo $row->number;
					}
			?>
		</td>
		<!--
		<td align="center" id="tdRecStyle">
			<?php echo $row->contact; ?>
		</td>
		-->
		<td align="center" id="tdRecStyle">
			<?php echo JHTML::_('date',  strtotime($row->invoice_date), $this->date_format);  ?>
		</td>
		<td align="center" id="tdRecStyle">
			<?php echo JHTML::_('date',  strtotime($row->duedate), $this->date_format);  ?>
		</td>
		<td align="center" id="tdRecStyle">
			<?php

			if($this->symbol_display == '1')
			{
				if($this->cformat == 0)
				{
					echo $this->currency_symbol.number_format($row->total, 2, '.', ',');
				}else if($this->cformat == 1)
				{
					echo $this->currency_symbol.number_format($row->total, 2, ',', '.');
				}else if($this->cformat == 2)
				{
					echo $this->currency_symbol.number_format($row->total, 2, '.', ' ');
				}

			}else
			{
				if($this->cformat == 0)
				{
					echo number_format($row->total, 2, '.', ',').$this->currency_symbol;
				}else if($this->cformat == 1)
				{
					echo number_format($row->total, 2, ',', '.').$this->currency_symbol;
				}else if($this->cformat == 2)
				{
					echo number_format($row->total, 2, '.', ' ').$this->currency_symbol;
				}
			}


			 ?>
		</td>
		<td align="center" id="tdRecStyle" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<?php
			if($row->status == 1)
			{
				$color = "border:solid 1px #818283;background-color:#E3E4E4;color:#818283;text-transform:uppercase;";
			}else if($row->status == 2)
			{
				$color = "border:solid 1px #1BC721;background-color:#F3FFEE;color:#006C09;text-transform:uppercase;";
			}else if($row->status == 3)
			{
				$color = "border:solid 1px #FF7570;background-color:#FFF6F6;color:#E62C2F;text-transform:uppercase;";
			}else if($row->status == 4)
			{
				$color = "border:solid 1px #7DAEE3;background-color:#DCEBFC;color:#006DCF;text-transform:uppercase;";
			}
			?>
				<td style="<?php echo $color; ?>" align="center">
			<?php echo $this->status[$row->status]; ?>
		</td>
			</tr>
			</table>
		</td>
		<td id="tdRecStyle1" >
			<table cellpadding="4" style="margin-left:10px;" cellspacing="0" border="0">
				<tr align="right">
					<td>
						<a href="index.php?option=com_ccinvoices&task=emailInv&id=<?php echo $row->id; ?>&Itemid=<?php echo $itemid; ?>"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/send.png" width="16" height="16" title="<?php echo JText::_("CC_EMAIL_INVOICE"); ?>"></a>
					</td>
					<? if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<? } ?>
					<td>
						<a  href= "index.php?option=com_ccinvoices&task=viewInv&id=<?php echo $row->id; ?>&tmpl=component" class="modal"  rel="{handler: 'iframe', size: {x: 700, y: 500}}"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/view.png" width="16" height="16" title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>"></a>
					</td>
					<? if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<? } ?>
					<td>
						<a href="index.php?option=com_ccinvoices&task=download&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/download.png" width="16" height="16" title="<?php echo JText::_("CC_DOWNLOAD_INVOICE"); ?>"></a>
					</td>
					<? if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<? } ?>
					<?php
					if($this->showPaymentIcon)
					{
						if($row->status == 4 || $row->status == 1)
						{
							?>
							<td>
								<a href="javascript:void(0);"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/pay_now_button_inactive.png" width="89" height="28" ></a>
							</td>
							<?php
						}else
						{
							?>
							<td>
								<a href="<?php echo JRoute::_('index.php?option=com_ccinvoices&task=paymentOverview&id='.md5($row->id)); ?>"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
							</td>
							<?php
						}
					}
					?>
				</tr>
			</table>
		</td>
	</tr>
<?php
	$k = 1 - $k;
}
?>
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="view" value="ccinvoices">
<input type="hidden" name="controller" value="ccinvoices" />
<input type="hidden" name="Itemid" value="<?php echo $itemid; ?>" />
</table>
</form>
<?
function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,3);
}
?>