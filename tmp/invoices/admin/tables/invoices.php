<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class Tableinvoices extends JTable
{

	var $id = null;
	var $number = null;
	var $invoice_date = null;
	var $status= null;
	var $duedate = null;
	var $note = null;
	var $numbercheck=null;
	var $invoice_sent_date = null;
	var $custom_invoice_number = null;
	var $communication = null;
	var $discount = null;
	var $subtotal = null;
	var $totaltax = null;
	var $total = null;
	var $quantity= null;
	var $pname=null;
	var $price = null;
	var $tax=null;
	var $contact_id = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_invoices', 'id', $db);
	}

	function check()
	{
		return true;
	}
}
?>