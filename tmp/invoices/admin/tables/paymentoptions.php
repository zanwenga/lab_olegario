<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class TableItem extends JTable
{
	var $id = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices', 'id', $db);
	}

	function check()
	{
		return true;
	}

}
?>