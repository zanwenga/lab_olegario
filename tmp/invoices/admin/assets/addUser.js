var xmlHttp1

function addUser(form,user_id,contact_id,task)
{

var path = document.adminForm.path.value;
var filter = document.adminForm.filter.value;

if(task == 'createUser')
{
	if(document.adminForm.email_checker.value == "0")
	{
		emailTmp = document.getElementById("email");
		emailTmp.style.borderColor = 'red';
		document.getElementById("emailMSG").innerHTML = "<img  src='"+ path +"components/com_ccinvoices/assets/images/cross.png' width='16' height='16'>&nbsp;"+ document.adminForm.emailMSG_EMPTY.value ;
		return;
	}else if(document.adminForm.email_checker.value == "2")
	{
		emailTmp = document.getElementById("email");
		emailTmp.style.borderColor = 'red';
		document.getElementById("emailMSG").innerHTML = "<img  src='"+ path +"components/com_ccinvoices/assets/images/cross.png' width='16' height='16'>&nbsp;"+ document.adminForm.emailMSG_INVALID.value ;
		return;
	}else if(document.adminForm.email_checker.value == "3")
	{
		emailTmp = document.getElementById("email");
		emailTmp.style.borderColor = 'red';
		document.getElementById("emailMSG").innerHTML = "<img  src='"+ path +"components/com_ccinvoices/assets/images/cross.png' width='16' height='16'>&nbsp;"+ document.adminForm.emailMSGval.value ;
		return;
	}
	var path = document.adminForm.path.value;
	var email = document.adminForm.email.value;
	var name = document.adminForm.name.value;
	var username = document.adminForm.username.value;
	var usergroup = document.adminForm.usergroup.value;
	var root_path = document.adminForm.root_path.value;
	var jversion = document.adminForm.jversion.value;

	if(name == '')
	{
		alert(document.adminForm.nameEmptyMSG.value);
		return false;
	}else if(username == '')
	{
		alert(document.adminForm.usernameEmptyMSG.value);
		return false;
	}else if(usergroup == '0')
	{
		alert(document.adminForm.usergroupEmptyMSG.value);
		return false;
	}
	var url=path+"components/com_ccinvoices/assets/addUser.php";
	url=url+"?contact_id="+contact_id+"&email="+email+"&name="+name+"&username="+username+"&usergroup="+usergroup+"&task="+task+"&jversion="+jversion;

}else
{
	var url=path+"components/com_ccinvoices/assets/addUser.php";
	url=url+"?user_id="+user_id+"&contact_id="+contact_id+"&task="+task+"&filter="+filter;
}
xmlHttp1=GetXmlHttpObject();
if (xmlHttp1==null)
{
		alert ("Your browser does not support AJAX!");
		return;
}
document.getElementById('op_area').innerHTML = "<div id='op_area1'></div>";
xmlHttp1.onreadystatechange= stateChanged1;
xmlHttp1.open("GET",url,true);
xmlHttp1.send(null);
}
function stateChanged1()
{
	if (xmlHttp1.readyState==4)
	{
		if (xmlHttp1.status == 200)
		{
			var response = xmlHttp1.responseText;
			var update = new Array();
	      	if(response.indexOf('|') != -1)
	        {
	            update = response.split('|');
	            changeText(update[0], update[1], update[2]);
	        }

		}
	}
}

function changeText( op1, op2, op3 )
{
	document.getElementById('userListArea').innerHTML =	op1;
	document.getElementById('userAssined').innerHTML =	op2;
	document.getElementById('op_area').innerHTML = "&nbsp;";
}

function GetXmlHttpObject()
{
var xmlHttp1=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp1=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp1=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp1=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp1;
}
