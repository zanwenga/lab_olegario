<?php
define( '_JEXEC', 1 );
chdir("../../../../../");
getcwd();
define('JPATH_BASE', getcwd() );

define('DS', DIRECTORY_SEPARATOR);

require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
if(isset( $_GET["dformat"]))
{
	$dformat = $_GET["dformat"];
	echo JHTML::_('date',  time(), $dformat);
}
?>
