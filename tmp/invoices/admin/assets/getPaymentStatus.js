var xmlHttp

function getPaymentStatus()
{
var path = document.adminForm.pathadmin.value;
invoice_id=document.getElementById('id').value;
var url=path+"administrator/components/com_ccinvoices/assets/getPaymentStatus.php";
url=url+"?invoice_id="+invoice_id;

xmlHttp=GetXmlHttpObject();
if (xmlHttp==null)
{
		alert ("Your browser does not support AJAX!");
		return;
}
xmlHttp.onreadystatechange= stateChanged;
xmlHttp.open("GET",url,true);
xmlHttp.send(null);
}

function stateChanged()
{
	if (xmlHttp.readyState==4)
	{
		if (xmlHttp.status == 200)
		{
			var response = xmlHttp.responseText;
			document.getElementById('ajaxdisplay').value =	response;
			if(document.getElementById("imgaltertext").innerHTML=="")
			{
				if(document.getElementById("status").value=="2")
				{
					document.getElementById("imgid").title=document.getElementById("imagealttext").value;
				}
			}
		}
	}
}
function removestatusimage()
{
	document.getElementById("imgaltertext").style.display='none';
	document.getElementById("imgaltertext").innerHTML="";

}
function changeText( op1, op2 )
{
	document.getElementById('userListArea').innerHTML =	op1;
	document.getElementById('userAssined').innerHTML =	op2;
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}
