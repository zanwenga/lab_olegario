var xmlHttp

function createUser(form,contact_id)
{
var path = document.adminForm.path.value;
var email = document.adminForm.email.value;
var name = document.adminForm.name.value;
var username = document.adminForm.username.value;
var usergroup = document.adminForm.usergroup.value;
if(email == '')
{
	alert("Enter a Valid Email ID");
	return false;
}else if(name == '')
{
	alert("Enter a Valid  Name");
	return false;
}else if(username == '')
{
	alert("Enter a Valid  User Name");
	return false;
}else if(usergroup == '0')
{
	alert("Select the user group");
	return false;
}
var url=path+"components/com_ccinvoices/assets/createUser.php";
url=url+"?contact_id="+contact_id+"&email="+email+"&name="+name+"&username="+username+"&usergroup="+usergroup;

xmlHttp=GetXmlHttpObject();
if (xmlHttp==null)
{
		alert ("Your browser does not support AJAX!");
		return;
}
xmlHttp.onreadystatechange= stateChanged;
xmlHttp.open("GET",url,true);
xmlHttp.send(null);
}
function stateChanged()
{
	if (xmlHttp.readyState==4)
	{
		if (xmlHttp.status == 200)
		{
			var response = xmlHttp.responseText;
			document.getElementById('userAssined').innerHTML =	response;
			/*var update = new Array();
	      	if(response.indexOf('|') != -1)
	        {
	            update = response.split('|');
	            changeText(update[0], update[1]);
	        }*/

		}
	}
}

function changeText( op1, op2 )
{
	document.getElementById('userListArea').innerHTML =	op1;
	document.getElementById('userAssined').innerHTML =	op2;
}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}
