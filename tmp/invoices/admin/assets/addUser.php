<?php
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
global $mainframe;
jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
$mainframe =& JFactory::getApplication('administrator');
$lang = JFactory::getLanguage();
$my = JFactory::getUser();
$lang->load('com_ccinvoices',JPATH_BASE.DS."administrator");
$db = JFactory::getDBO();
$user_id = JRequest::getInt("user_id","0");
$contact_id = JRequest::getInt("contact_id","0");
$task = JRequest::getVar("task","");
$filter = JRequest::getVar("filter","");
$path_new = JURI::root();
$path = str_replace("components/com_ccinvoices/assets/", "", $path_new);
$where = array();
$sql1 = '';

if($my->id == "0")
{
	exit;
}

if($filter != '')
{
	$where[] = " name LIKE  '%".$filter."' OR username LIKE '%".$filter."%' OR email LIKE '%".$filter."%'";
}
$where		= count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '';
if($task == "addUser")
{
	$sql = "INSERT INTO #__ccinvoices_users (`user_id`,`contact_id`) values ('".$user_id."','".$contact_id."')";
	$db->setQuery($sql);
	$db->query();

	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "delUser")
{
	$sql = "DELETE FROM #__ccinvoices_users where user_id = '".$user_id."' AND contact_id = '".$contact_id."'";
	$db->setQuery($sql);
	$db->query();

	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "")
{
	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "createUser")
{


	$usergroup = JRequest::getInt("usergroup","");
	$name =  JRequest::getVar("name","");
	$username = JRequest::getVar("username","");
	$email = JRequest::getVar("email","");
	$joomlaversion=JRequest::getVar("jversion","");
	$password_raw = genRandomSecretcode();
	$salt = JUserHelper::genRandomPassword(32);
    $crypt = JUserHelper::getCryptedPassword($password_raw, $salt);
    $registerDate = date('Y-m-d H:M:S');
    $password = $crypt.':'.$salt;
	if($joomlaversion!="1.5")
		$query = 'SELECT value  FROM #__usergroups where id = "'.$usergroup.'"  LIMIT 1';
	else
		$query = 'SELECT value  FROM #__core_acl_aro_groups where id = "'.$usergroup.'"  LIMIT 1';

	$db->setQuery( $query );
	$usertype = $db->loadResult();

	$block = '0';
	$gid = JRequest::getInt("usergroup","");
	$section_value = 'users';
	$order_value = '0';
	$hidden = '0';
	$msg = '';

	$query = 'SELECT count(*)  FROM #__users where username = "'.$username.'" AND block=1 LIMIT 1';
	$db->setQuery( $query );
	$count_user = $db->loadResult();


	if(trim($email)!="")
	{
	if($username != '')
	{
		if($count_user == 0)
	{

		if($joomlaversion!="1.5")
		{

			$query = 'INSERT INTO #__users ( name, username, email , password , usertype, block, registerDate)' .
					' VALUES ( "'.$name.'", "'.$username.'" ,"'.$email.'" ,"'.$password.'" ,"'.$usertype.'" ,"'.$block.'" ,"'.$registerDate.'" )'
				;
		}
		else
		{

			$query = 'INSERT INTO #__users ( name, username, email , password , usertype, block, gid,registerDate)' .
					' VALUES ( "'.$name.'", "'.$username.'" ,"'.$email.'" ,"'.$password.'" ,"'.$usertype.'" ,"'.$block.'" ,"'.$gid.'","'.$registerDate.'" )'
				;
		}
		$db->setQuery( $query );
		if(!$db->query()) {
			JError::raiseError( 500, $db->stderror() );
		}
		$user_id = $db->insertid();

		if($joomlaversion!="1.5")
		{
			$query = 'INSERT INTO #__user_usergroup_map ( user_id, group_id)' .
					' VALUES ( '.$user_id.', '.$gid.' )'
				;
		}
		else
		{
		$query = 'INSERT INTO #__core_acl_aro ( section_value, value, order_value , name , hidden)' .
				' VALUES ( "'.$section_value.'", "'.$user_id.'" ,"'.$order_value.'" ,"'.$name.'" ,"'.$hidden.'")'
				;
		}
		$db->setQuery( $query );
		if(!$db->query()) {
			JError::raiseError( 500, $db->stderror() );
		}
		if($joomlaversion=="1.5")
		{
			$query = 'SELECT id  FROM #__core_acl_aro ORDER BY id desc LIMIT 1';
			$db->setQuery( $query );
			$aro_id = $db->loadResult();
			$query = 'INSERT INTO #__core_acl_groups_aro_map ( group_id, aro_id)' .
					' VALUES ( "'.$gid.'", "'.$aro_id.'" )'
					;
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
		}
		$sql = "INSERT INTO #__ccinvoices_users (`user_id`,`contact_id`) values ('".$user_id."','".$contact_id."')";
		$db->setQuery($sql);
		$db->query();


		$sql = "SELECT * FROM #__users "
				. $where
				." ORDER BY  registerDate DESC LIMIT 10";
		$db->setQuery($sql);
		$rows = $db->loadObjectList();

		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
		if($config->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC = $config->email_cc;
		}
		if($config->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC = $config->email_bcc;
		}
		$mailfrom 		= $mainframe->getCfg( 'mailfrom' );
		$fromname 		= $mainframe->getCfg( 'fromname' );
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$siteURL		= JURI::root();

		$subject 	= sprintf ( JText::_( 'CC_ACCOUNT_DETAILS_FOR' ), $name, $sitename);
		$subject 	= html_entity_decode($subject, ENT_QUOTES);

		$sendmsg 	= sprintf ( JText::_( 'CC_SEND_MSG' ),$name, $username ,$password_raw,$path);
		$sendmsg 	= html_entity_decode($sendmsg, ENT_QUOTES);

		JUtility::sendMail($mailfrom, $fromname, $email, $subject, $sendmsg ,1,$EmailCC,$EmailBCC);

	}
}
}
}
function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,3);
}
function genRandomSecretcode($length = 6)
{
	$salt = "0123456789";
	$len = strlen($salt);
	$makepass = '';
	$stat = @stat(__FILE__);
	if(empty($stat) || !is_array($stat)) $stat = array(php_uname());
	mt_srand(crc32(microtime() . implode('|', $stat)));
	for ($i = 0; $i < $length; $i ++) {
		$makepass .= $salt[mt_rand(0, $len -1)];
	}
	return $makepass;
}

$sql = "SELECT u.id,u.name,u.username,u.email FROM #__users AS u"
		. " LEFT JOIN #__ccinvoices_users AS iu ON iu.user_id = u.id "
		. " WHERE iu.contact_id = ".$contact_id
		;
$db->setQuery($sql);
$userAssined = $db->loadObjectList();

ob_start();
?>
<table cellpadding="0" cellspacing="0" width="100%" border="0" class="adminlist">
	<thead>
		<tr>
			<th width="28%" align="center">
				<?php echo JText::_('CC_NAME'); ?>
			</th>
			<th width="28%" align="center">
				<?php echo JText::_('CC_USER_NAME'); ?>
			</th>
			<th  align="center">
				<?php echo JText::_('CC_EMAIL'); ?>
			</th>
			<th width="8%" align="center">&nbsp;
			</th>
		</tr>
	</thead>
	<?php
	$k = 0;
	$db =& JFactory::getDBO();
	for ($i=0, $n=count( $rows ); $i < $n; $i++)
	{
		$row = &$rows[$i];
		$count_user = "";
		if($contact_id == '')
		{
			$sql = "SELECT count(*) FROM #__ccinvoices_users where user_id =".$row->id;
		}else
		{
			$sql = "SELECT count(*) FROM #__ccinvoices_users where user_id =".$row->id." AND contact_id =".$contact_id;
		}
		$db->setQuery($sql);
		$count_user = $db->loadResult();

	?>
	<tr class="<?php echo "row$k"; ?>">
		<td>
			<?php echo $row->name; ?>
		</td>
		<td>
			<?php echo $row->username; ?>
		</td>
		<td>
			<?php echo $row->email; ?>
		</td>
		<td>
			<?php if ($count_user == 0) { ?>
			<a href="javascript::void(0);" onclick="addUser(this.form,'<?php echo $row->id; ?>','<?php echo $contact_id; ?>','addUser')" ><img src="<?php echo JURI::base(); ?>images/user_add.png" width="16" height="16"></a>
			<?php }else { ?>
			<a href="javascript::void(0);" onclick="addUser(this.form,'<?php echo $row->id; ?>','<?php echo $contact_id; ?>','delUser')" ><img  src="<?php echo JURI::base(); ?>images/user_delete.png" width="16" height="16"></a>
			<?php } ?>
		</td>
	</tr>
	<?php
		$k = 1 - $k;
	}
	?>
</table>
<?php
$output1 = ob_get_contents();
ob_end_clean();
ob_start();
?>
<p style="margin:0px;padding:0px;">
	<?php
	$userAssined_tmp = "<table cellspacing='0' cellpadding='0'>";
	for ($k=0, $n=count($userAssined); $k < $n; $k++)
	{
		$userAssined_row = $userAssined[$k];
		$del_tmp = "'delUser'";
		$userAssined_tmp .= '<tr><td style="margin:0px;padding:0px;text-align:justify;"><a href="javascript::void(0);" title="'.$userAssined_row->username.','.$userAssined_row->email.'" onclick="addUser(this.form,'.$userAssined_row->id.','.$contact_id.','.$del_tmp.');">'.$userAssined_row->name.'&nbsp;<img style="margin:2px;" src="'.JURI::base().'images/user_delete.png" width="16" height="16">,</a></td></tr>';
	}
	$userAssined_tmp.="</table>";
	echo $userAssined_tmp;
?>
</p>
<?php
$output2 = ob_get_contents();
ob_end_clean();
echo $output1."|".$output2."|".$task;
?>