<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
class ccInvoicesControllerassigncontacts extends JController
{
    function __construct()
    {
        parent::__construct();
    }

    function display()
    {
		JRequest::setVar('view', 'assigncontacts');
		parent::display();
    }

}

?>