<?php
/**
* @package	 ccInvoices
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009-2010 Chill Creations
* @copyright Copyright (C) 2002-2009  Nicola Asuni - Tecnick.com S.r.l.
* @license	 GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of TCPDF.

This program is free software; you can redistribute it and/or

modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

Copyright (C) 2002-2009  Nicola Asuni - Tecnick.com S.r.l.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
class ccInvoicesControllerInvoices extends JController
{
    function __construct()
    {
		//Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'invoices');
		}
        $this->item_type = 'Invoices';
        parent::__construct();
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'add',	'edit' );
		$this->registerTask( 'new',	'edit' );
		$this->registerTask( 'apply', 'save' );
    }

    function edit()
	{
		JRequest::setVar( 'view', 'invoices');
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}

	function save()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$model = $this->getModel('invoices');
        $post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
        $resend	= JRequest::getVar( 'resend','');
        $send	= JRequest::getVar( 'send','');
		$reminder	= JRequest::getVar( 'reminder','');
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

        if ($id=$model->store())
        {
        	$msg = JText::_( 'CC_INVOICE_SAVED_SUCC' );
        }
        else
        {
        	$msg = JText::_( 'CC_INVOICE_SAVED_UNSUCC' );
        }
        if($this->_task == 'apply')
        {
        	$link 	= 'index.php?option=com_ccinvoices&controller=invoices&task=edit&cid[]='. $id;
        }
        else
        {
        	$link = 'index.php?option=com_ccinvoices&controller=invoices';
        }
        if($id > 0)
        {
			$query = 'SELECT custom_invoice_number,status from #__ccinvoices_invoices where id ='.$id;
			$db->setQuery($query);
			$row_inv = $db->loadObject();
			$custom_invoice_number = $row_inv->custom_invoice_number;
			$inv_status = $row_inv->status;
	 		$invoice_format = $config->invoice_format;
			if($invoice_format != "" && $inv_status != "1")
			{
				$query = 'SELECT i.number,c.contact_number from #__ccinvoices_invoices AS i'
						. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id '
						. ' where i.id ='.$id;
				$db->setQuery($query);
				$invRow = $db->loadObject();
				$custom_inv =  sprintf("%04d", $invRow->number);
				$inv_format["invoicenumber"] = $custom_inv;
				$inv_format["contactnumber"] = $invRow->contact_number;
				if($this->versionCompare()=="1.5")
				{
				$inv_format["dd"] = JHTML::_('date',  time(), "%d");
				$inv_format["mm"] = JHTML::_('date', time(), "%m");
				$inv_format["yy"] = JHTML::_('date',  time(), "%y");
				$inv_format["yyyy"] = JHTML::_('date',  time(), "%Y");
				}
				else
				{
					$inv_format["dd"] = JHTML::_('date',  time(), "d");
					$inv_format["mm"] = JHTML::_('date', time(), "m");
					$inv_format["yy"] = JHTML::_('date',  time(), "y");
					$inv_format["yyyy"] = JHTML::_('date',  time(), "Y");
				}
				foreach($inv_format as $inv_formats=>$value)
				{
					$find = "[".$inv_formats."]";
					$replace = $value;
					$invoice_format = str_replace($find,$replace,$invoice_format);
				}
				$sql = "UPDATE #__ccinvoices_invoices SET 	custom_invoice_number= '".$invoice_format."' WHERE id=".$id;
				$db->setQuery($sql);
				$db->query();
			}
			$file_path = $this->createInvoice($id);
			$this->sendEmail($reminder,$resend,$send,$file_path,$id);
        }

        $this->setRedirect( $link ,$msg);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function invoicesendsubject($reminder,$number)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT default_email_sub,rem_email_sub FROM #__ccinvoices_configuration WHERE id = 1 LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

   	    //$reminder	= JRequest::getVar( 'reminder');
		$op_val['invoice_number'] = $number;
       	if($reminder == '1')
		{
			if($config->rem_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEREMINDER_SUB' );
		}else
		{
				$invoicesend_sub = $config->rem_email_sub;
			}
		}else
		{
			if($config->default_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEEMAIL_SUB' );
			}else
			{
				$invoicesend_sub = $config->default_email_sub;
			}
		}
		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$invoicesend_sub = str_replace($find,$replace,$invoicesend_sub);
		}
		return $invoicesend_sub;
	}

	function sendEmail($reminder,$resend,$send,$file_path,$id)
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$model = $this->getModel('invoices');
		$query = 'SELECT i.*,c.name,c.contact,c.email'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

		if($send != '' || $reminder !='' || $resend !='')
		{
			$invoice_send_date	= date("Y-m-d");
			$sql = "UPDATE #__ccinvoices_invoices SET invoice_sent_date = '".$invoice_send_date."' WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();

			$sql = "UPDATE #__ccinvoices_invoices SET communication=1 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
		}

		//$MailFrom	= $mainframe->getCfg('mailfrom');
		//$FromName	= $mainframe->getCfg('fromname');
		$MailFrom	= $config->company_email;
		$FromName	= $config->user_company;
		if($config->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC = $config->email_cc;
		}
		if($config->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC = $config->email_bcc;
		}
		$invoicesendsubject	= 	$this->invoicesendsubject($reminder,$rows->number);
		$outputArr["invoice_number"] = $model->getInvoiceNumberFormat($rows->number);
		$front_end_url = "index.php?option=com_ccinvoices&view=ccinvoices";
		$pay_invoice_link="index.php?option=com_ccinvoices&task=paymentOverview&id=".MD5($id);
		//GETTING PAYMENT METHOD NAMES
		$pluginnames=$this->getPaymentMethodsName();
		if($reminder == '1' )
		{
			$outputArr["payment_methods"]=$pluginnames;
			$invoicesendmsg = $config->default_email_rem;
			$outputArr["contact_name"] = $rows->contact;
			$outputArr["name"] =  $rows->name;
			$outputArr["invoice_last_send_date"] = $model->dateChangeFormat($rows->invoice_sent_date,$config->date_format);
			$outputArr["user_name"] = $config->user_name;
			$outputArr["company_name"] = $config->user_company;
			$outputArr["invoice_due_date"] = $model->dateChangeFormat($rows->duedate,$config->date_format);
			$outputArr["invoice_total"] = $model->changeCurrencyFormat($rows->total);
			$outputArr["invoice_note"] = $rows->note;
			$outputArr["all_invoices_link"] = "<a href='".JURI::root().$front_end_url."' target='_blank'>".JText::_("CC_WEBSITE_LINK")."</a>";
			$outputArr["pay_invoice_link"] = "<a href='".JURI::root().$pay_invoice_link."' target='_blank'>".JText::_("CC_PAY_INVOICE_LINK")."</a>";
			foreach($outputArr as $outputArrs=>$value)
			{
				$find = "{".$outputArrs."}";
				$replace = $value;
				$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
			}
			$invoicesendmsg = $model->convertImgTags($invoicesendmsg);
			$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
			JUtility::sendMail($MailFrom, $FromName, $rows->email, $invoicesendsubject, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);
		}else if($resend == '1')
		{
			$outputArr["payment_methods"]=$pluginnames;
			$invoicesendmsg = $config->default_email;
			$outputArr["contact_name"] =  $rows->contact;
			$outputArr["name"] =  $rows->name;
			$outputArr["invoice_due_date"] = $model->dateChangeFormat($rows->duedate,$config->date_format);
			$outputArr["user_name"] = $config->user_name;
			$outputArr["company_name"] = $config->user_company;
			$outputArr["invoice_due_date"] = $model->dateChangeFormat($rows->duedate,$config->date_format);
			$outputArr["invoice_total"] = $model->changeCurrencyFormat($rows->total);
			$outputArr["invoice_note"] = $rows->note;
			$outputArr["all_invoices_link"] = "<a href='".JURI::root().$front_end_url."' target='_blank'>".JText::_("CC_WEBSITE_LINK")."</a>";
			$outputArr["pay_invoice_link"] = "<a href='".JURI::root().$pay_invoice_link."' target='_blank' >".JText::_("CC_PAY_INVOICE_LINK")."</a>";
			foreach($outputArr as $outputArrs=>$value)
			{
				$find = "{".$outputArrs."}";
				$replace = $value;
				$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
			}
			$invoicesendmsg = $model->convertImgTags($invoicesendmsg);
	    	$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
		JUtility::sendMail($MailFrom, $FromName, $rows->email, $invoicesendsubject, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);
		}else if($send == '1')
		{
			$outputArr["payment_methods"]=$pluginnames;
			$invoicesendmsg = $config->default_email;
			$outputArr["contact_name"] =  $rows->contact;
			$outputArr["name"] =  $rows->name;
			$outputArr["invoice_due_date"] = $model->dateChangeFormat($rows->duedate,$config->date_format);
			$outputArr["user_name"] = $config->user_name;
			$outputArr["company_name"] = $config->user_company;
			$outputArr["invoice_due_date"] = $model->dateChangeFormat($rows->duedate,$config->date_format);
			$outputArr["invoice_total"] = $model->changeCurrencyFormat($rows->total);
			$outputArr["invoice_note"] = $rows->note;
			$outputArr["all_invoices_link"] = "<a href='".JURI::root().$front_end_url."' target='_blank'>".JText::_("CC_WEBSITE_LINK")."</a>";
			$outputArr["pay_invoice_link"] = "<a href='".JURI::root().$pay_invoice_link."' target='_blank'>".JText::_("CC_PAY_INVOICE_LINK")."</a>";
			foreach($outputArr as $outputArrs=>$value)
			{
				$find = "{".$outputArrs."}";
				$replace = $value;
				$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
			}
			$invoicesendmsg = $model->convertImgTags($invoicesendmsg);
	    	$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
	    	JUtility::sendMail($MailFrom, $FromName, $rows->email, $invoicesendsubject, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);
		}
	}
	function getPaymentMethodsName()
	{
		$plugins_name=& JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher =& JDispatcher::getInstance();
		$methodnames = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$paymentmethodname="";
		if(count($methodnames)>0)
		{
			$i=0;
			foreach($methodnames as $pluginname)
			{
				if($i==count($methodnames)-2)
				{
					$paymentmethodname.=$pluginname["payment_method_note"]." or ";
				}
				else
				{
					$paymentmethodname.=$pluginname["payment_method_note"].", ";
				}
				$i=$i+1;
			}
		}
		$paymentmethodname=substr($paymentmethodname,0,strlen($paymentmethodname)-2);
		return $paymentmethodname;
	}
	function createInvoice($id)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS.'tcpdf.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS."config".DS."lang".DS.'eng.php');
        $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('David');
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords('Invoice');
        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('times', '', 8);
        $pdf->AddPage();
        $model = $this->getModel('invoices');
		$template=$model->gettemplatelayout($id);
        $v=$pdf->writeHTML($template, true, false, false, false, '');
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		if($conf->invoice_format != "")
		{
		$file_name = $model->getInvoiceNumberFormat($invRow->number).".pdf";
		}else
		{
			$file_name = $invRow->number.".pdf";
		}
        $file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.$file_name;
        $pdf->Output($file_path, 'F');
		return $file_path;
	}
	function downloadInvoice()
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$model = $this->getModel('invoices');
		$file_path = $this->createInvoice($id);
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		if($conf->invoice_format != "")
		{
		$file_name = $model->getInvoiceNumberFormat($invRow->number).".pdf";
		}else
		{
			$file_name = $invRow->number.".pdf";
		}
		define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'pdf' => 'application/pdf',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$fname\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}
	}
	function getAdditionalColumns(){
		$db =& JFactory::getDBO();
		$sql = "DESCRIBE #__users ";
		$db->setQuery($sql);
		$result = $db->loadAssocList();
		$array_all_columns = array();
		foreach($result as $key=>$value){
			$array_all_columns[] = $value['Field'];
		}
		return $array_all_columns;
	}
	// return all types by users
	function getUserType(){
	    $db =& JFactory::getDBO();
		$sql = "SELECT DISTINCT usertype
		        FROM #__users
                WHERE usertype <> ''";
		$db->setQuery($sql);
		$result = $db->loadAssocList();
		return $result;
	}
	function convertTextcharToUppercase($line)
	{
		$tmparray="";
		$newstr="";
		$tmparray=explode(",",$line);
		foreach($tmparray as $tmpstr)
		{
			$newstr.=strtoupper(substr($tmpstr,0,1)).substr($tmpstr,1).",";
		}
		return $newstr;
	}
	function batchDownloadCSV()
	{

		$id = JRequest::getInt("id","");
		$oid = JRequest::getVar('cid', array(0), 'method', 'array');
		JArrayHelper::toInteger($oid, array(0));
		sort($oid ,$sort_flags = SORT_NUMERIC);
		$database =& JFactory::getDBO();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
		$database->setQuery($query);
		$invRow = $database->loadObject();
		if(count($oid) == 1)
		{
			$file_name = "invoice_".$invRow->number.".csv";
		}else if(count($oid) > 1)
		{
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
			$database->setQuery($query);
			$first_inv_number= $database->loadResult();
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[count($oid)-1]." LIMIT 1";
			$database->setQuery($query);
			$last_inv_number= $database->loadResult();
			$file_name = "invoice_batch_".$first_inv_number."_".$last_inv_number.".csv";
		}

		$file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.$file_name;
/*		$list = array (
		    'aaa,bbb,ccc,dddd',
		    '123,456,789',
		    '"aaa","bbb"'
		);
*/
		$fp = fopen($file_path, 'w');
		$query = "SELECT name,id FROM  #__users ORDER BY id";
		$database->setquery($query);
		$rows = $database->loadObjectList();

		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 2 LIMIT 1";
	    $database->setQuery($query);
		$template = strip_tags($database->loadResult());
		/*
		$template = str_replace("\r\n","",$template);
		$template = str_replace('"','\"',$template);
		echo $template;exit;
*/
        $tmp_x = 0;
        $model = $this->getModel('invoices');
        $newline="";
        $kp=0;
        $tempid="";
        $countchar="";
        $i=0;
        $tempnum="";
        $countchar[-1]="0";
        $tempnum=1;
		foreach($oid as $id)
		{
			$query	= "SELECT * FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
			$database->setQuery($query);
			$rows= $database->loadObject();
			$countchar[$i]=substr_count($rows->pname,"|");
			if($countchar[$i]>$countchar[$i-1])
			{
				$tempnum=$countchar[$i];
				$tempid=$id;
			}
			else
			{
				$tempid=$id;
			}
			$i++;
		}

		list($line,$template,$productarray) = $model->gettemplateCSV($tempid,"0");
		$template=strip_tags($template);
		$order = array("\r\n", "\n", "\r","{","}");
		$line=strip_tags(str_replace($order, "", $line));
		$line=strip_tags(str_replace("_", " ", $line));
		$line=$this->convertTextcharToUppercase($line);
		fputcsv($fp, @split(',', $line));
		foreach($oid as $id)
		{
			list($line,$template,$productarray) = $model->gettemplateCSV($id,$tempnum);
			$order   = array("\r\n", "\n", "\r");
			$replace = '';
			$line = strip_tags(str_replace($order, $replace, $line));
			$line=str_replace(",","|",$line);
			$line=str_replace("`",",",$line);
			$linenew=array();
			$linenew[]=explode("|",$line);
			$finalarray="";
			$pos="";
			foreach($linenew as $record)
			{
				for($i=0;$i<=count($record)-1;$i++)
				{
					$tmp=strpos($record[$i],"{");
					$tmp1=strpos($record[$i],"}");
					if(trim($tmp)!="" AND trim($tmp1)!="")
					{
						$pos=$i;
						break;
					}
				}
			}
			$k=0;
			if(trim($pos)=="")
			{
				foreach($linenew as $record)
				{
					fputcsv($fp, $record);
				}
			}
			else
			{

				foreach($productarray as $product)
				{
					for($f=0;$f<=count($linenew[0])-1;$f++)
					{
						if($f>=0 AND $f<$pos)
						{
							$finalarray[$k][$f]=$linenew[0][$f];
						}
						elseif($f>=$pos AND $f<($pos+count($product)))
						{
							for($t=0;$t<=count($product)-1;$t++)
							{
								$product[$t]=str_replace("`",",",$product[$t]);
								$finalarray[$k][$t+$pos]=$product[$t];
							}
							$f=$t+$pos-1;
						}
						else
						{
							$finalarray[$k][$f]=$linenew[0][$f];
						}
					}
					$k++;
				}
				foreach($finalarray as $record)
				{
					fputcsv($fp, $record);
				}
			}
		}
		fclose($fp);
     	define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'csv' => 'application/csv',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$fname\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}



	}


	function batchDownloadPDF()
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$oid = JRequest::getVar('cid', array(0), 'method', 'array');
		JArrayHelper::toInteger($oid, array(0));
		sort($oid ,$sort_flags = SORT_NUMERIC);
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS.'tcpdf.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS."config".DS."lang".DS.'eng.php');
		$model = $this->getModel('invoices');
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
	    $pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor('David');
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords('Invoice');
        // set default header data
        //$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING);
        // set header and footer fonts
        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        // set default monospaced font
        //$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);
        //set margins
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
        //set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
        //set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
        $pdf->SetFont('times', '', 8);
        $model = $this->getModel('invoices');

		foreach($oid as $id)
		{
			$template = '';
			$pdf->AddPage();
			$template=$model->gettemplatelayout($id);
	        $pdf->writeHTML($template, true, false, false, false, '');
			$pdf->lastPage();
		}

		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		if(count($oid) == 1)
		{
			$file_name = "invoice_".$invRow->number.".pdf";
		}else if(count($oid) > 1)
		{
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
			$db->setQuery($query);
			$first_inv_number= $db->loadResult();
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[count($oid)-1]." LIMIT 1";
			$db->setQuery($query);
			$last_inv_number= $db->loadResult();
			$file_name = "invoice_batch_".$first_inv_number."_".$last_inv_number.".pdf";
		}
/*
		if($conf->invoice_format != "")
		{
			$file_name = $model->getInvoiceNumberFormat($invRow->number).".pdf";
		}else
		{
			$file_name = $invRow->number.".pdf";
		}
*/
        $file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.$file_name;
        $pdf->Output($file_path, 'F');

		define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'pdf' => 'application/pdf',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$fname\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}
	}
	function deleteInvoice()
		{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$sql = "DELETE FROM #__ccinvoices_invoices WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();
		$msg = JText::_("CC_INVOICE_DELETED_SUCCESS");
		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$this->setRedirect( $link ,$msg);
		}

	function remPayInvoice()
		{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();

		if(strtotime(date("%y-%m-%d",time())) > strtotime($invRow->duedate))
		{
			$sql = "UPDATE #__ccinvoices_invoices SET status=3 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
			$msg = JText::_("CC_INVOICE_STATUS_LATE");
		}else
		{
			$sql = "UPDATE #__ccinvoices_invoices SET status=2 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
			$msg = JText::_("CC_INVOICE_STATUS_OPEN");
		}

		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$this->setRedirect( $link ,$msg);
	}
	function payInvoice()
        {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$send = "0";
		$resend = "0";
		$reminder = "0";
		$sql = "UPDATE #__ccinvoices_invoices SET status=4 WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();
		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$msg = JText::_("CC_INVOICE_PAID_SUCCESS");
		$this->setRedirect( $link ,$msg);
        }

	function invoiceRem()
        {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$send = "0";
		$resend = "0";
		$reminder = "1";
		$file_path = $this->createInvoice($id);
		$this->sendEmail($reminder,$resend,$send,$file_path,$id);
		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$msg = JText::_("CC_SEND_INVOICE_REM_TO_CONTACT");
		$this->setRedirect( $link ,$msg);
        }

	function reSendInvoice()
        {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$send = "0";
		$resend = "1";
		$reminder = "0";
		$file_path = $this->createInvoice($id);
		$this->sendEmail($reminder,$resend,$send,$file_path,$id);
		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$msg = JText::_("CC_RESEND_INVOICE_TO_CONTACT");
		$this->setRedirect( $link ,$msg);
        }

	function viewInvoice()
        {
		$id = JRequest::getInt("id","");
		$model = $this->getModel('invoices');
		$template=$model->gettemplatelayout($id);
		echo $template;
        }

	function sendInvoice()
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$send = "1";
		$resend = "0";
		$reminder = "0";
		$file_path = $this->createInvoice($id);
		//echo $reminder.",".$send;exit;
		$this->sendEmail($reminder,$resend,$send,$file_path,$id);
		$link = "index.php?option=com_ccinvoices&controller=invoices";
		$msg = JText::_("CC_SEND_INVOICE_TO_CONTACT");
        $this->setRedirect( $link ,$msg);
	}

	function searchcontactname()
	{
       JRequest::setVar( 'view', 'invoices' );
       JRequest::setVar( 'layout', 'invoice_contact' );
       parent::display();
       exit;
	}
	function getinvoicecontact()
	{
   		JRequest::setVar( 'view', 'invoices' );
    	JRequest::setVar( 'layout', 'getinvoicecontact' );
    	parent::display();
    	exit;
	}
	function updateinvoicecontact()
	{
	   	$model = $this->getModel('invoices');
	   	$updateinvoicecontact=$model->updateinvoicecontact();
		exit;
	}

   	function copy()
	{
		// Check for request forgeries

		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect( 'index.php?option=com_ccinvoices' );
		$cid	= JRequest::getVar( 'cid', null, 'post', 'array' );
		$db		=& JFactory::getDBO();
		JTable :: addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_ccinvoices' . DS . 'tables');
		$query = 'SELECT default_due_days from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
		$db->setQuery($query);
		$confRow = $db->loadObject();

		$table	=& JTable::getInstance('invoices', 'Table');
		$user	= &JFactory::getUser();
		$n		= count( $cid );
	   	$model = $this->getModel('invoices');
		if ($n > 0)
		{
			foreach ($cid as $id)
			{
				if ($table->load( (int)$id ))
				{
					$table->id			= 0;
					//$table->number			= 'Copy of ' . $table->number;
					$table->number			= 0;
//					$table->amount			= $table->amount;
//					$table->client		= $table->client;
					$table->tax			= $table->tax;
					$table->status 			= 1;
					$table->numbercheck 			= $table->numbercheck;
					$table->invoice_sent_date 			= '0000-00-00';
					$table->note 			= $table->note;
					$table->contact_id 			= $table->contact_id;
				//	$table->custom_invoice_number 			= $table->custom_invoice_number;
					$table->custom_invoice_number 			= $model->getInvoiceNumberFormat("0");
					$table->invoice_date	= date("Y-m-d");
					$defDueDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") + $confRow->default_due_days, date("Y")));
					$table->duedate = $defDueDate;
					$table->communication = 0;
					if (!$table->store())
					{
						return JError::raiseWarning( $table->getError() );
					}
				}
				else
				{
					return JError::raiseWarning( 500, $table->getError() );
				}
			}
		}
		else
		{
			return JError::raiseWarning( 500, JText::_( 'CC_NO_ITEMS_SELECTED' ) );
		}
		$this->setMessage( JText::sprintf(  JText::_( 'CC_ITEM_COPIED' ) , $n ) );
	}

	function remove()
	{
		// get the model
		$model = $this->getModel('invoices');
		if($model->delete())
		{
			$msg = JText::_( 'CC_INVOICE_DETAILS_DELETED' );
		}
		else
		{
			$msg = JText::_( 'CC_ERROR_INVOICE_DELET' );
		}
		$this->setRedirect( 'index.php?option=com_ccinvoices' ,$msg);
	}

	function invoiceforcontact()
	{
		JRequest::setVar( 'view', 'invoices' );
    	JRequest::setVar( 'layout', 'contactform' );
    	parent::display();
	}
	function printInv()
	{
		JRequest::setVar( 'view', 'print');
		parent::display();
	}
}

class JHTMLInput
{
	function checkbox($name,$value,$checked='checked')
	{
		$html = "<input type=\"checkbox\" name=\"" . $name . "\" value=\"" . $value . "\"  id=\"" . $name . "\"  onClick=\"DoTheCheck('$name')\" $checked	/>";
		return $html;
	}
}

?>