<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
class ccInvoicesControllercontacts extends JController
{
    function __construct()
    {
        //Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'contacts');
        }
        $this->item_type = 'contacts';
        parent::__construct();
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'add',	'edit' );
		$this->registerTask( 'new',	'edit' );
		$this->registerTask( 'apply', 'save' );
    }

	function edit()
	{
		JRequest::setVar( 'view', 'contacts');
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}

	function save()
	{
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$model = $this->getModel('contacts');
		if ($id=$model->store())
		{
			$msg = JText::_( 'CC_CONTACT_SAVED_SUCC' );
		}else
		{
			$msg = JText::_( 'CC_CONTACT_SAVED_UNSUCC' );
		}
		if($post['task'] == 'apply')
		{
			$link 	= 'index.php?option=com_ccinvoices&controller=contacts&task=edit&cid[]='. $id;
		}else
		{
			$link = 'index.php?option=com_ccinvoices&controller=contacts';
		}
		$this->setRedirect($link, $msg);
	}

	function remove()
	{
		// get the model
		$model = $this->getModel('contacts');

		list($boolval,$cantdelmsg)=$model->delete();

		if($boolval)
		{
			if($cantdelmsg=="1")
			{
				JError::raiseWarning(100, JText::_('CC_CONTACTS_CANT_DELETED'));
				$this->setRedirect( 'index.php?option=com_ccinvoices&controller=contacts' );
			}
			else
		{
			$msg = JText::_( 'CC_CONTACTS_DELETED' );
				$this->setRedirect( 'index.php?option=com_ccinvoices&controller=contacts', $msg );
			}
		}
		else
		{
			$msg = JText::_( 'ERROR_CONTACTS_DELETED' );
			$this->setRedirect( 'index.php?option=com_ccinvoices&controller=contacts', $msg );
		}
		// redirect
	}
	function modelassigncontact()
	{
		JRequest::setVar( 'view', 'contacts');
		JRequest::setVar( 'layout', 'assigncontact'  );
		parent::display();
	}
}
?>