<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.controller' );
class ccInvoicesControllerPaymentoptions extends JController
{
    function __construct()
    {
        //Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'paymentoptions');
        }
        $this->item_type = 'Paymentoptions';
        parent::__construct();
    }
}
?>