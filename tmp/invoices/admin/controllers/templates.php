<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport( 'joomla.application.component.controller' );
class ccInvoicesControllerTemplates extends JController
{
    function __construct()
    {
        parent::__construct();
		$this->registerTask( 'apply',       'save' );
    }

   	function display()
	{
		JRequest::setVar( 'view', 'templates');
		parent::display();
	}

	function cancel()
	{
		// set user message redirect
		$msg = JText::_( 'CC_TEMPLATE_CANCELED' );
		$this->setRedirect( 'index.php?option=com_ccinvoices&controller=templates', $msg );
	}
	function save()
	{
		$post			= JRequest::get('post', JREQUEST_ALLOWRAW);
		$model = $this->getModel('templates');
		if ($cid = $model->store())	{
		$msg = JText::_( 'CC_TEMPLATE_SAVED' );
		} else
		{
			$msg = JText::_( 'CC_TEMPLATE_SAVED_FAILED' );
	}

		if($post['task'] == 'apply')	{
			$cid 		= JRequest::getInt( 'id');
			$link 	= 'index.php?option=com_ccinvoices&controller=templates&task=edit&cid[]='.$cid;
		} else {
			$link = 'index.php?option=com_ccinvoices&controller=templates';
		}
		$this->setRedirect($link, $msg);
	}

	function edit()
	{
		JRequest::setVar( 'view', 'templates');
		JRequest::setVar( 'layout', 'form');
		parent::display();
	}

}
?>

