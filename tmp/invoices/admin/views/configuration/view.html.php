<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');

class ccInvoicesViewConfiguration extends JView
{
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function display($tpl = null)
    {
		$model = & $this->getModel();
		$db =& JFactory::getDBO();
		$model->setId(1);
		$row =& $this->get('Data');
		$query = 'SELECT max(number) from #__ccinvoices_invoices where reset_inv = "" LIMIT 1 ';
		$db->setQuery($query);
		$invoice_format = "";
		$invoice_id_max = $db->loadResult();
		if($row->invoice_format != "")
		{
			$custom_inv =  sprintf("%04d", $invoice_id_max + 1);
			$inv_format["invoicenumber"] = $custom_inv ;
				if($this->versionCompare()=="1.5")
				{
			$inv_format["dd"] = JHTML::_('date',  time(), "%d");
			$inv_format["mm"] = JHTML::_('date',  time(), "%m");
			$inv_format["yy"] = JHTML::_('date',  time(), "%y");
			$inv_format["yyyy"] = JHTML::_('date',  time(), "%Y");
				}
				else
				{
					$inv_format["dd"] = JHTML::_('date',  time(), "d");
					$inv_format["mm"] = JHTML::_('date', time(), "m");
					$inv_format["yy"] = JHTML::_('date',  time(), "y");
					$inv_format["yyyy"] = JHTML::_('date',  time(), "Y");
				}
			$invoice_format = $row->invoice_format;
			foreach($inv_format as $inv_formats=>$value)
			{
				$find = "[".$inv_formats."]";
				$replace = $value;
				$invoice_format = str_replace($find,$replace,$invoice_format);
			}
		}else
		{
			$custom_inv =  sprintf("%04d", $invoice_id_max + 1);
			$invoice_format = $custom_inv ;
		}
		$next_invo_no = $invoice_id_max +1;
		if($row->invoice_start > $next_invo_no)
		{
			$next_invo_no = $row->invoice_start;
		}else
		{
			$query = 'SELECT count(id) from #__ccinvoices_invoices  where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($invoice_count == "0")
			{
				if($row->invoice_start != '')
				{
					$next_invo_no = $row->invoice_start;
				}
			}
		}
		/*if($row->default_email_sub == "")
		{
			$row->default_email_sub = JText::_( 'CC_INVOICEEMAIL_SUB' );
		}
		if($row->rem_email_sub == "")
		{
			$row->rem_email_sub = JText::_( 'CC_INVOICEREMINDER_SUB' );
		}*/

	    JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
		JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).': <small><small> '. JText::_( 'CC_CONFIGURATION' ).' </small></small>', 'ccinvoices.png' );
		JToolBarHelper::apply();
		JToolBarHelper::save();
				/* Component Footer Information Starts*/
		$file = JPATH_COMPONENT_ADMINISTRATOR.DS.'install.xml';
		$xml = JFactory::getXMLParser('Simple');
		$xml->loadFile($file);
		$xml = $xml->document;
		$c_version = $xml->version[0]->data();
		$c_name = $xml->name[0]->data();
		//echo $c_version.$c_name;exit;
		/* Check for New Version */
		$myReadAccess= new versionRead('http://www.chillcreations.com/versionnumbers.txt');
		if($data = $myReadAccess->getFileContents()) {
			$pieces = explode("\n", $data);
			foreach($pieces as $piece)
			{
				$small_pieces[] = explode(",", $piece);
			}
			$versionContent = "";
 			foreach( $small_pieces as $small_piece)
			{
  				if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) != 'none') {
					$versionContent ="<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a>";
					$versionContent .="<br/>".$small_piece[3]."</div>";
  				}
				else if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) == 'none') {
					$versionContent = "<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a></div>";
				}
			}
		}
		if(isset($versionContent) && $versionContent != "") {
			$this->assignRef('versionContent',	$versionContent);
		}
		$this->assignRef('version',		$c_version);
		$this->assignRef('name',		$c_name);
		/* Component Footer Information End*/
		$this->assignRef("row",$row);
		$this->assignRef('invoice_format',		$invoice_format);
		$this->assignRef('next_invo_no',		$next_invo_no);
        parent::display($tpl);
    }
}
?>