<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');

class ccInvoicesViewAssignusers extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db =& JFactory::getDBO();
	    $document =& JFactory::getDocument();
		$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/ccinvoice.css' );
		$contact_id = JRequest::getInt("contact_id","");
		$sql = "SELECT * FROM #__users ORDER BY  registerDate DESC LIMIT 10";
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$sql = "SELECT u.id,u.name,u.username,u.email FROM #__users AS u"
			. " LEFT JOIN #__ccinvoices_users AS iu ON iu.user_id = u.id "
			. " WHERE iu.contact_id = ".$contact_id
			;
		$db->setQuery($sql);
		$userAssined = $db->loadObjectList();

		$sql = "SELECT * FROM #__ccinvoices_contacts WHERE id =".$contact_id." LIMIT 1";
		$db->setQuery($sql);
		$contactDetails = $db->loadObject();

		if($this->versionCompare()!="1.5")
		{
			$query = 'SELECT id as value,title AS text'
				. ' FROM #__usergroups'
				. ' WHERE title != "ROOT"'
				. ' AND title != "USERS"'
			;
		}
		else
		{
			$query = 'SELECT id as value,name AS text'
				. ' FROM #__core_acl_aro_groups'
				. ' WHERE name != "ROOT"'
				. ' AND name != "USERS"'
			;
		}

		$db->setQuery( $query);
		$types[] 		= JHTML::_('select.option',  '0', '- '. JText::_( 'CC_SELECT_GROUP' ) .' -' );
		foreach( $db->loadObjectList() as $rec )
		{
			$types[] = JHTML::_('select.option',  $rec->value, JText::_( $rec->text ) );
		}
		$lists['type'] 	= JHTML::_('select.genericlist',   $types, 'usergroup', 'class="inputbox" size="1" ', 'value', 'text', "0" );

				/* Component Footer Information Starts*/
		$file = JPATH_COMPONENT_ADMINISTRATOR.DS.'install.xml';
		$xml = JFactory::getXMLParser('Simple');
		$xml->loadFile($file);
		$xml = $xml->document;
		$c_version = $xml->version[0]->data();
		$c_name = $xml->name[0]->data();
		//echo $c_version.$c_name;exit;
		/* Check for New Version */
		$myReadAccess= new versionRead('http://www.chillcreations.com/versionnumbers.txt');
		if($data = $myReadAccess->getFileContents()) {
			$pieces = explode("\n", $data);
			foreach($pieces as $piece)
			{
				$small_pieces[] = explode(",", $piece);
			}
			$versionContent = "";
 			foreach( $small_pieces as $small_piece)
			{
  				if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) != 'none') {
					$versionContent ="<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a>";
					$versionContent .="<br/>".$small_piece[3]."</div>";
  				}
				else if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) == 'none') {
					$versionContent = "<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a></div>";
				}
			}
		}
		if(isset($versionContent) && $versionContent != "") {
			$this->assignRef('versionContent',	$versionContent);
		}
		$this->assignRef('version',		$c_version);
		$this->assignRef('name',		$c_name);
		/* Component Footer Information End*/
		$this->assignRef("rows",$rows);
		$this->assignRef("userAssined",$userAssined);
		$this->assignRef("contact_id",$contact_id);
		$this->assignRef("contactDetails",$contactDetails);
		$this->assignRef("lists",$lists);
		$this->assignRef("jversion",$this->versionCompare());
		parent::display($tpl);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
}
?>