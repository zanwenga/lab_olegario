<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');

class ccInvoicesViewInvoices extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db =& JFactory::getDBO();
		$model = & $this->getModel();
		jimport('joomla.html.pagination');
		if($this->_layout == 'form' || $this->_layout == 'contactform' )
		{
			$invoiceRow		= $this->get('invoices');

			$sql = "SELECT * FROM #__ccinvoices_contacts where id = ".$invoiceRow->contact_id." LIMIT 1 ";
			$db->setQuery($sql);
			$contactsRow =  $db->loadObject();

			require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
			$confModel =new ccInvoicesModelConfiguration();
			$confModel->setId(1);
			$confRow = $confModel->getData();

			if($invoiceRow->invoice_date == '')
			{
				$invoiceRow->invoice_date = date("Y-m-d", time());
			}
			if($invoiceRow->id != '')
			{

				$selectDefaultTax = explode("|",$invoiceRow->tax);
			}else
			{
				if($confRow->default_tax != '')
				{
				$selectDefaultTax[0] = $confRow->default_tax;
			}
				else
				{
					$selectDefaultTax[0] = "";
				}
			}
			if($invoiceRow->id != '')
			{
				$defDueDate = $invoiceRow->duedate;

			}else
			{
				$defDueDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") + $confRow->default_due_days, date("Y")));
			}

			if($invoiceRow->id != '')
			{
				$defNote = $invoiceRow->note;
			}else
			{
				$defNote = $confRow->default_note;
			}

			$arrTaxVal = array();
			if($confRow->tax != '')
			{
				jimport('joomla.utilities.arrayhelper');
				$splitTax = explode(";",$confRow->tax);
				if(count($splitTax) > 0)
				{
					for($i=0;$i<count($splitTax);$i++)
					{
						$tmp['value'] = $splitTax[$i] ;
						$arrTaxVal[$i] = JArrayHelper::toObject($tmp);
					}
				}
			}

			$query	= "SELECT max(contact_number) FROM #__ccinvoices_contacts";
		    $db->setQuery($query);
		    $contact_id1 = $db->loadResult();
			$contact_number = $contact_id1 + 1 ;


			$invoicetax = $arrTaxVal;
            $text = $invoiceRow->id ? JText::_( 'CC_EDIT' ) : JText::_( 'CC_NEW' );
            JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).': <small><small> ' . $text.' </small></small>', 'ccinvoices.png' );
			//JToolBarHelper::title(   JText::_( 'CC_Invoice' ), 'invoice' );
			JToolBarHelper::apply();
			JToolBarHelper::save();
			JToolBarHelper::cancel();
			//$invoicetax=$model->invoicetax();
			$sql = "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = '' ";
			$db->setQuery($sql);
			$nextInvoieNo = $db->loadResult();
			if($nextInvoieNo == '')
			{
				$nextInvoieNo = "Empty";
			}
			$this->assignRef('nextInvoieNo',		$nextInvoieNo);
			$this->assignRef('contact_number',		$contact_number);
			$this->assignRef('invoicetax',		$invoicetax);
			$this->assignRef('invoiceRow',		$invoiceRow);
			$this->assignRef('contactsRow',		$contactsRow);
			$this->assignRef('selectDefaultTax',		$selectDefaultTax);
			$this->assignRef('defDueDate',		$defDueDate);
			$this->assignRef('defNote',		$defNote);
		    $clientid=JRequest::getVar('clientid');
		    $contactinvoice=$model->contactinvoice($clientid);
		    $this->assignRef('contactinvoice',		$contactinvoice);
		    $invoicenumbercheck=$model->invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}
		    $this->assignRef('numbercheck',		$new_inv_id1);
		    $lastinvoicenumber=$model->lastinvoicenumber();
		    $this->assignRef('lastinvoicenumber',		$lastinvoicenumber);
            $status[] = JHTML::_('select.option',  '1',  JText::_( 'CC_CONCEPT'));
            $status[] = JHTML::_('select.option',  '2', JText::_( 'CC_OPEN'));
            $status[] = JHTML::_('select.option',  '3', JText::_( 'CC_LATE'));
            $status[] = JHTML::_('select.option',  '4', JText::_( 'CC_PAID') );
            $this->assignRef('status',	$status);
		}else if($this->_layout == 'invoice_contact')
		{
        	$contactname = JRequest::getVar('contactname');
        	$contactdetails=$model->contactsearch($contactname);
			$this->assignRef('contactdetails',		$contactdetails);
		}else if($this->_layout =='getinvoicecontact')
		{
			$contactid = JRequest::getInt('contactid');
        	$value=$model->getinvoicecontact($contactid);
			$this->assignRef('getinvoicecontact',		$value);
		}else
		{
			JHTML::_('behavior.calendar');
			$clientid = JRequest::getInt("clientid","");
            JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ) . ': <small><small> ' .  JText::_( 'CC_INVOICES_INVOICES' ).' </small></small>', 'ccinvoices.png' );
			JToolBarHelper::addNewX();
			JToolBarHelper::customX( 'copy', 'copy.png', 'copy_f2.png', JText::_( 'CC_INVOICES_TOOLBAR_COPY') );
			JToolBarHelper::editListX();
			JToolBarHelper::deleteListX();

			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');
			$rows		= $this->get('data');
			$pagination		= $this->get('pagination');


			$sql = "SELECT currency_symbol,symbol_display,cformat FROM #__ccinvoices_configuration where id = 1 LIMIT 1 ";
			$db->setQuery($sql);
			$row_conf =  $db->loadObject();
			$currency_symbol = $row_conf->currency_symbol;
			$symbol_display = $row_conf->symbol_display;
			// table ordering
			$lists['order_Dir']	= $sortOrder;
			$lists['order']		= $sortColumn;
			$lists['search']		= $filter;
			$pageNav=$pagination;
			// search filter

            $status[] = JHTML::_('select.option',  '1',  JText::_( 'CC_CONCEPT'));
            $status[] = JHTML::_('select.option',  '2', JText::_( 'CC_OPEN'));
            $status[] = JHTML::_('select.option',  '3', JText::_( 'CC_LATE'));
            $status[] = JHTML::_('select.option',  '4', JText::_( 'CC_PAID') );
            $this->assignRef('status',	$status);
			$this->assignRef('items',		$rows);
			$this->assignRef('pageNav',		$pageNav);
			$this->assignRef('clientid',		$clientid);
			$this->assignRef('lists',		$lists);
			$this->assignRef('currency_symbol',		$currency_symbol);
			$this->assignRef('symbol_display',		$symbol_display);
			$this->assignRef('cformat',		$row_conf->cformat);
		}
		$sql = "SELECT 	date_format FROM #__ccinvoices_configuration WHERE id=1 ";
		$db->setQuery($sql);
		$date_format = $db->loadResult();

		/* Component Footer Information Starts*/
		$file = JPATH_COMPONENT_ADMINISTRATOR.DS.'install.xml';
		$xml = JFactory::getXMLParser('Simple');
		$xml->loadFile($file);
		$xml = $xml->document;
		$c_version = $xml->version[0]->data();
		$c_name = $xml->name[0]->data();
		//echo $c_version.$c_name;exit;
		/* Check for New Version */
		$myReadAccess= new versionRead('http://www.chillcreations.com/versionnumbers.txt');
		if($data = $myReadAccess->getFileContents()) {
			$pieces = explode("\n", $data);
			foreach($pieces as $piece)
			{
				$small_pieces[] = explode(",", $piece);
			}
			$versionContent = "";
 			foreach( $small_pieces as $small_piece)
			{
  				if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) != 'none') {
					$versionContent ="<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a>";
					$versionContent .="<br/>".$small_piece[3]."</div>";
  				}
				else if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) == 'none') {
					$versionContent = "<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a></div>";
				}
			}
		}
		if(isset($versionContent) && $versionContent != "") {
			$this->assignRef('versionContent',	$versionContent);
		}
		$this->assignRef('version',		$c_version);
		$this->assignRef('name',		$c_name);
		$this->assignRef('date_format',$date_format);
		parent::display($tpl);
	}
}
?>