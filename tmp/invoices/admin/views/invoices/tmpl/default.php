<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
defined('_JEXEC') or die('Restricted access');
?>
<style>
.adminlist
{
	font-size:14px;
}

</style>
<script language="javascript">
function createFilter(filter)
{
	document.getElementById('filter').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter').value = '';
	submitform();
}
function deleteAction(id,status)
{
	document.adminForm.task.value = "deleteInvoice";
	document.adminForm.id.value = id;
	if (confirm ("<?php echo JText::_( 'CC_DELETE_CONFIRM_MSG1' ); ?> "+status+"<?php echo JText::_( 'CC_DELETE_CONFIRM_MSG2' ); ?>"))
	{
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}

}
</script>
<script language="javascript" type="text/javascript">
<!--
function submitbutton(pressbutton)
{
	if(pressbutton == "reset_inv")
	{
		if (confirm ("<?php echo JText::_( 'CC_RESET_INV_MSG' ); ?>"))
		{
			document.adminForm.task.value = pressbutton;
			submitform( pressbutton );
		}else
		{
			document.adminForm.task.value = '';
		}
	}else
	{
		document.adminForm.task.value = pressbutton;
		submitform( pressbutton );
	}
}
function exportPDF()
{
	if(document.adminForm.boxchecked.value != 0)
	{
		document.adminForm.task.value = 'batchDownloadPDF';
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}else
	{
		alert("<?php echo JText::_( 'CC_PLEASE_CHOOSE_INVOICE' ); ?>");
	}
}
function exportCSV()
{
	if(document.adminForm.boxchecked.value != 0)
	{
		document.adminForm.task.value = 'batchDownloadCSV';
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}else
	{
		alert("<?php echo JText::_( 'CC_PLEASE_CHOOSE_INVOICE' ); ?>");
	}
}
//-->
</script>
<form action="index.php" method="post" name="adminForm">

<table width="100%">
	<tr>
		<td align="left" width="100%">
			<?php echo JText::_( 'CC_FILTER' ); ?>:
			<input type="text" name="invoice_filter" id="invoice_filter" onchange="createFilter(document.getElementById('invoice_filter').value);" class="text_area" value="<?php echo $this->lists['search'];?>"/>
			<input type="button" value="<?php echo JText::_('CC_GO'); ?>" onclick="createFilter(document.getElementById('invoice_filter').value);return false;" />
			<input type="button" onclick="eraseFilter();" value="<?php echo JText::_('CC_FILTERRESET'); ?> " />
		</td>
		<td align="right">
			<div class="batch_export">
				<?php echo JText::_( 'CC_BATCH_LABEL' ); ?>
				<a href="javascript::void(0);" onclick="exportPDF();" style="text-decoration:underline;"><?php echo JText::_("CC_LABEL_EXPORT_TO_PDF"); ?></a>
				<?php echo JText::_('CC_LABEL_EXPORT_TXT'); ?>
				<a href="javascript::void(0);" onclick="exportCSV();" style="text-decoration: underline;"><?php echo JText::_("CC_LABEL_EXPORT_TO_CSV"); ?></a>
			</div>
		</td>
	</tr>
</table>



<table class="adminlist" width="100%" border="0">
<thead>
	<tr>
		<th width="1%" align="center">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->items ); ?>);" />
		</th>
		<th nowrap="nowrap" align="center" width="17%">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_INVOICES_ID' ), 's.number', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="13%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_CLIENT' ), 'b.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="10%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_CONTACT' ), 'b.name', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_DATE' ), 's.invoice_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="10%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_AMOUNT' ), 's.total', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>

		<th width="6%" align="center">
			<?php echo JHTML::_('grid.sort',   JText::_( 'CC_INVOICES_STATUS' ), 's.status', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th width="25%" align="center">
			<?php echo JText::_( 'CC_INVOICES_ACTION' ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="9">
			<?php echo $this->pageNav->getListFooter(); ?>
		</td>
	</tr>
</tfoot>
<?php
$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
$k = 0;
for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );
	$link 		= JRoute::_( 'index.php?option=com_ccinvoices&controller=invoices&task=edit&cid[]='. $row->id );
	$link_print_inv = JRoute::_( 'index.php?option=com_ccinvoices&controller=invoices&task=printInv&id='. $row->id.'&tmpl=component' );
	$link1 		= JRoute::_( 'index.php?option=com_ccinvoices&controller=contacts&task=edit&cid[]='. $row->contact_id );
	if($row->invoice_date =='0000-00-00' || $row->invoice_date == '')
	{
		$datevalue='';
	} else if($row->invoice_date != '0000-00-00')
	{
		$datevalue=JHTML::_('date',  strtotime($row->invoice_date), $this->date_format);
	}
	if(strtotime(date('Y-m-d',time()))	 >  strtotime($row->duedate))
	{
		$database = JFactory::getDBO();
		$sql = "UPDATE #__ccinvoices_invoices SET status=3 WHERE status=2 AND id =".$row->id;
		$database->setQuery($sql);
		$database->query();

		if($row->status == "2")
		{
			$row->status = "3";
		}
	}
	?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>">
			<?php
			if($row->custom_invoice_number != '')
			{
				echo $row->custom_invoice_number;
			}else
			{
				echo $row->number;
			}

			 ?></a>
		</td>
		<td align="center">

			<a href="<?php echo $link1; ?>"><?php echo $row->name; ?></a>
		</td>
		<td align="center">
			<a href="<?php echo $link1; ?>"><?php echo $row->contact; ?></a>
		</td>

		<td align="center">
			<?php  echo $datevalue; ?>
		</td>
		<td align="center">
			<?php
			if($this->symbol_display == '1')
			{
				if($this->cformat == 0)
				{
					echo $this->currency_symbol.number_format($row->total, 2, '.', ',');
				}else if($this->cformat == 1)
				{
					echo $this->currency_symbol.number_format($row->total, 2, ',', '.');
				}else if($this->cformat == 2)
				{
					echo $this->currency_symbol.number_format($row->total, 2, '.', ' ');
				}

			}else
			{
				if($this->cformat == 0)
				{
					echo number_format($row->total, 2, '.', ',').$this->currency_symbol;
				}else if($this->cformat == 1)
				{
					echo number_format($row->total, 2, ',', '.').$this->currency_symbol;
				}else if($this->cformat == 2)
				{
					echo number_format($row->total, 2, '.', ' ').$this->currency_symbol;
				}
			}
			?>
		</td>
		<td align="center">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<?php
			if($row->status == 1)
			{
				$color = "border:solid 1px #818283;background-color:#E3E4E4;color:#818283;text-transform:uppercase;";
			}else if($row->status == 2)
			{
				$color = "border:solid 1px #1BC721;text-transform:uppercase;background-color:#F3FFEE;color:#006C09;";
			}else if($row->status == 3)
			{
				$color = "border:solid 1px #FF7570;text-transform:uppercase;background-color:#FFF6F6;color:#E62C2F;";
			}else if($row->status == 4)
			{
				$color = "border:solid 1px #7DAEE3;text-transform:uppercase;background-color:#DCEBFC;color:#006DCF;";
			}
			?>
				<td style="<?php echo $color; ?>" align="center">
			<?php
				$select_id = '';
				$temp = $this->status;
				$select_id = $row->status - 1;
				echo $temp[$select_id]->text;
			?>
		</td>
			</tr>
			</table>
		</td>
		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" >
				<tr>
					<td style="border:none;" align="center">
						<?php if($row->status == "1") { ?>
						<div ><img title="<?php echo JText::_('CC_TITLEDRAFT'); ?>" src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/send-inactive.png" ></div>
						<?php }else if($row->status == "2") {  ?>
							<?php if($row->invoice_sent_date == "0000-00-00") { ?>
								<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=sendInvoice&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/send.png" title="<?php echo JText::_("CC_SEND_INVOICE"); ?>"></a></div>
							<?php }else { ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=reSendInvoice&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
							<?php }?>
						<?php }else if($row->status == "3") {  ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=invoiceRem&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/reminder.png" title="<?php echo JText::_("CC_REMINDER_INVOICE"); ?>"></a></div>
						<?php } else if($row->status == "4") {  ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=reSendInvoice&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
						<?php }	?>
					</td>
					<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}"  href="index.php?option=com_ccinvoices&controller=assignusers&contact_id=<?php echo $row->contact_id; ?>&tmpl=component"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/manage_users.png"  title="<?php echo JText::_("CC_MANAGE_USERS"); ?>"></a></div>
					</td>
					<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" class="modal"  rel="{handler: 'iframe', size: {x: 700, y: 500}}"  href="index.php?option=com_ccinvoices&controller=invoices&task=viewInvoice&id=<?php echo $row->id; ?>&tmpl=component"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/view.png"  title="<?php echo JText::_("CC_QUICK_VIEW"); ?>"></a></div>
					</td>
					<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" href="<?php echo $link; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/edit.png"  title="<?php echo JText::_("CC_EDIT"); ?>"></a></div>
					</td>
					<td style="border:none;" align="center">
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=downloadInvoice&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/download.png" title="<?php echo JText::_("CC_DOWNLOAD_PRINT"); ?>"></a></div>
					</td>
					<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" href="javascript:void window.open('<?php echo $link_print_inv; ?>', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no');" ><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/print.png"  title="<?php echo JText::_("CC_PRINT_INV"); ?>"></a></div>
					</td>
					<td style="border:none;" align="center">
						<?php
						$db = JFactory::getDBO();
						$sql = "SELECT id,method FROM  #__ccinvoices_payment WHERE status='1' AND inv_id =".$row->number;
						$db->setQuery($sql);
						$count_inv = $db->loadObject();
						if($row->status !="4" ) { ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=payInvoice&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/payment.png" title="<?php echo JText::_("CC_SET_STATUS_PAID"); ?>"></a></div>
						<?php }else if($row->status == "4") {
							$msg="";
							if(count($count_inv)>0)
							{
								$msg=sprintf(JText::_("CC_REMOVE_PAYMENT_METHOD"),$count_inv->method);
							}
							else
							{
								$msg=JText::_("CC_REMOVE_PAYMENT_ADMIN");
							}
							?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=remPayInvoice&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/remove_payment.png"  title="<?php echo $msg ?>"></a></div>
						<?php } ?>
					</td>
					<td style="border:none;" align="center">
						<div ><a style="text-decoration:none;" href="javascript:void(0)" onclick="deleteAction('<?php echo  $row->id; ?>','<?php echo $temp[$select_id]->text; ?>');"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/inv_delete.png" title="<?php echo sprintf ( JText::_('CC_REMOVE_INV'), $temp[$select_id]->text); ?>"></a></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$k = 1 - $k;
}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />

<input type="hidden" name="clientid" value="<?php echo $this->clientid; ?>" />
<input type="hidden" name="id" value="0" />
<input type="hidden" name="controller" value="invoices" />
<input type="hidden" name="filter_order" value="<?php echo $this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $this->lists['order_Dir']; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>

<script type="text/javascript">
<!--
if(document.getElementById('system-message'))
{
	var tagg = document.getElementsByTagName('dd');
	var tagcount = tagg.length ;
	if(tagcount > 1)
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
			var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
 			if (ieversion>=7)
 			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '152px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '144px';
		}
	} else
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
	 		var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			if (ieversion>=7)
			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '102px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '94px';
		}
	}
}
//-->
</script>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>