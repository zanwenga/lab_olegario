<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
?>
<form action="index.php" method="post" name="adminForm">
<table class="adminlist">
<thead>
					<tr>
		<th width="1%" align="center">
			#
		</th>
		<th width="1%" align="center">
			<input type="checkbox" name="toggle" value="" onclick="checkAll(<?php echo count( $this->rows ); ?>);" />
		</th>
		<th  style="text-align:center;">
			<?php echo JText::_("CC_TEMPLATE_TITLE"); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JText::_("CC_TEMPLATE_LAST_EDIT_BY"); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JText::_("CC_TEMPLATE_EDITED_ON"); ?>
		</th>
	</tr>
</thead>
								<?php
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
	$row = &$this->rows[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );
	$link 		= JRoute::_( 'index.php?option=com_ccinvoices&controller=templates&task=edit&&cid[]='. $row->id );
	$z = $i + 1;
								?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<?php echo $z; ?>
						</td>
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
					</td>
		<td align="center">
			<?php echo $row->edit_by; ?>
		</td>
		<td align="center">

			<?php
				if($this->jversion!="1.5")
			 		echo JHTML::_('date',  $row->edit_date , "d.m.y");
			 	else
			 		echo JHTML::_('date',  $row->edit_date , "%d.%m.%y");
			 ?>
		</td>
	</tr>
<?php
	$k = 1 - $k;
	}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="templates" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>
