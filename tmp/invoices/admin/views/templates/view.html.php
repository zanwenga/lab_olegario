<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport( 'joomla.application.component.view');
class ccInvoicesViewTemplates extends JView
{
	function display($tpl = null)
	{
		global $mainframe, $option;
		$db =& JFactory::getDBO();
		$user = JFactory::getUser();
		$user_name = $user->get("name");

		if($this->_layout == 'form' )
		{
			$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
			JArrayHelper::toInteger($cid, array(0));
			JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).": <small><small> ".JText::_( 'CC_INVOICES_TEMPLATE' ).' </small></small>', 'ccinvoices.png' );
			JToolBarHelper::save();
			JToolBarHelper::apply();
			JToolBarHelper::cancel();
			$query = "SELECT * FROM #__ccinvoices_configuration WHERE id=1 LIMIT 1";
			$db->setQuery( $query);
			$confrow= $db->loadObject();

			if($confrow->default_email_sub == "")
			{
				$confrow->default_email_sub = JText::_( 'CC_INVOICEEMAIL_SUB' );
			}
			if($confrow->rem_email_sub == "")
			{
				$confrow->rem_email_sub = JText::_( 'CC_INVOICEREMINDER_SUB' );
			}

			$query = "SELECT * FROM #__ccinvoices_templates WHERE id = ".$cid[0]." LIMIT 1";
			$db->setQuery( $query);
			$row= $db->loadObject();


			$this->assignRef('confrow',$confrow);
			$this->assignRef('row',	$row);
			$this->assignRef('edit_by',	$user_name);
		}else
		{
            JHTML::stylesheet('style.css', JURI::base() . 'components/com_ccinvoices/assets/css/');
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).": <small><small> ".JText::_( 'CC_INVOICES_TEMPLATES' ).' </small></small>', 'ccinvoices.png' );
			JToolBarHelper::editListX();
			$query = "SELECT *  FROM #__ccinvoices_templates";
			$db->setQuery( $query);
			$rows= $db->loadObjectList();
			$this->assignRef('jversion',$this->versionCompare());
			$this->assignRef('rows',	$rows);
		}

				/* Component Footer Information Starts*/
		$file = JPATH_COMPONENT_ADMINISTRATOR.DS.'install.xml';
		$xml = JFactory::getXMLParser('Simple');
		$xml->loadFile($file);
		$xml = $xml->document;
		$c_version = $xml->version[0]->data();
		$c_name = $xml->name[0]->data();
		//echo $c_version.$c_name;exit;
		/* Check for New Version */
		$myReadAccess= new versionRead('http://www.chillcreations.com/versionnumbers.txt');
		if($data = $myReadAccess->getFileContents()) {
			$pieces = explode("\n", $data);
			foreach($pieces as $piece)
			{
				$small_pieces[] = explode(",", $piece);
			}
			$versionContent = "";
 			foreach( $small_pieces as $small_piece)
			{
  				if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) != 'none') {
					$versionContent ="<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a>";
					$versionContent .="<br/>".$small_piece[3]."</div>";
  				}
				else if ($small_piece[0] == $c_name && $small_piece[1] > $c_version && trim($small_piece[3]) == 'none') {
					$versionContent = "<div style='font-weight:bold;text-align:center;color:#FF0000;'><a style='color:#FF0000;' href='".$small_piece[2]."' target='_blank'>". JText::_( 'ID_WARNING' ) . " " .JText::_( 'ID_NEW_VERSION' ) . " " . $small_piece[1]. " " .JText::_( 'ID_AVAIALBLE_DOWNLOAD' ) ."</a></div>";
				}
			}
		}
		if(isset($versionContent) && $versionContent != "") {
			$this->assignRef('versionContent',	$versionContent);
		}
		$this->assignRef('version',		$c_version);
		$this->assignRef('name',		$c_name);
		/* Component Footer Information End*/
		parent::display($tpl);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
}
?>