CREATE TABLE IF NOT EXISTS `#__ccinvoices_invoices` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `number` int(11) NOT NULL,
  `invoice_date` date NOT NULL,
  `status` int(11) NOT NULL,
  `duedate` varchar(100) NOT NULL,
  `numbercheck` int(10) NOT NULL,
  `invoice_sent_date` date NOT NULL,
  `communication` int(2) NOT NULL,
  `discount` varchar(25) NOT NULL,
  `subtotal` varchar(25) NOT NULL,
  `totaltax` varchar(25) NOT NULL,
  `total` varchar(25) NOT NULL,
  `quantity` mediumtext NOT NULL,
  `pname` mediumtext NOT NULL,
  `price` mediumtext NOT NULL,
  `tax` mediumtext NOT NULL,
  `note` mediumtext NOT NULL,
  `contact_id` int(11) NOT NULL,
  `custom_invoice_number` varchar(250) NOT NULL,
  `reset_inv` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__ccinvoices_contacts` (
  `id` int(11) NOT NULL auto_increment,
  `name` varchar(255) NOT NULL,
  `contact` varchar(255) NOT NULL,
  `contact_number` varchar(30) NOT NULL,
  `address` mediumtext NOT NULL,
  `email` varchar(255) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;


CREATE TABLE IF NOT EXISTS `#__ccinvoices_templates` (
  `id` int(11) NOT NULL auto_increment,
  `title` varchar(200) NOT NULL,  
  `invoice_template` longtext NOT NULL,
  `edit_by` varchar(200) NOT NULL,
  `edit_date` int(11) NOT NULL,  
  PRIMARY KEY  (`id`)
)  ENGINE=MyISAM  DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__ccinvoices_payment_options` (
  `id` int(11) NOT NULL auto_increment,
  PRIMARY KEY  (`id`)
)  ENGINE=MyISAM  DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__ccinvoices_configuration` (
  `id` int(11) NOT NULL auto_increment,
  `user_name` varchar(50) NOT NULL,  
  `invoice_format` varchar(250) NOT NULL,
  `invoice_start` varchar(30) NOT NULL,
  `date_format` varchar(50) NOT NULL,
  `default_due_days` varchar(30) NOT NULL,
  `default_tax` varchar(10) NOT NULL,
  `tax` varchar(30) NOT NULL,
  `currency_symbol` varchar(10) NOT NULL,
  `user_company` varchar(70) NOT NULL,
  `company_email` varchar(70) NOT NULL,
  `company_phone` varchar(70) NOT NULL,
  `company_url` varchar(100) NOT NULL,
  `logo` varchar(50) NOT NULL,
  `company_address` varchar(150) NOT NULL,
  `other_details` varchar(150) NOT NULL,
  `default_note` longtext NOT NULL,
  `default_email_sub` varchar(250) NOT NULL,
  `rem_email_sub` varchar(250) NOT NULL,
  `default_email` longtext NOT NULL,
  `default_email_rem` longtext NOT NULL,
  `symbol_display` tinyint(2) NOT NULL,
  `cformat` tinyint(2) NOT NULL,
  `email_cc` varchar(100) NOT NULL,
  `email_bcc` varchar(100) NOT NULL,  
  `tax_id` varchar(50) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8; 

CREATE TABLE IF NOT EXISTS `#__ccinvoices_payment` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `inv_id` int(11) NOT NULL,
  `method` varchar(100) NOT NULL,
  `transaction_id` varchar(100) NOT NULL,
  `pdate` varchar(30) NOT NULL,
  `status` char(2) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;

CREATE TABLE IF NOT EXISTS `#__ccinvoices_users` (
  `id` int(11) unsigned NOT NULL auto_increment,
  `user_id` int(11) NOT NULL,
  `contact_id` int(11) NOT NULL,
  PRIMARY KEY  (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8;