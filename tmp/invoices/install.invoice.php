<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

function com_install()
{


	jimport( 'joomla.filesystem.folder' );
	jimport('joomla.filesystem.file');
$jversion = new JVersion();
$current_version =  $jversion->getShortVersion();
$current_version=substr($current_version,0,3);

$plugin_tablename="";
if($current_version=="1.5")
{
	$plugin_tablename="#__plugins";	
	$plugin_tab_fields="( `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`)";
}
else
{
	$plugin_tablename="#__extensions";
	$plugin_tab_fields="(`type`, `name`, `element`, `folder`,`enabled`, `access`,`protected`, `ordering`, `state`, `client_id`, `checked_out`, `checked_out_time`, `params`,`custom_data`,`system_data`)";
}

	$db			=& JFactory::getDBO();
	$query = "SELECT count(*) FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	$confCount = $db->loadResult();
	if($current_version=="1.5")
		$dateformat="%m-%d-%y";
	else
		$dateformat="m-d-y";
		
	if(!$confCount)
	{
		$query = "INSERT INTO `#__ccinvoices_configuration` (`id`, `tax`, `user_name`, `user_company`, `date_format`,`default_note`, `default_email`, `default_email_rem`) VALUES (1, '', '', '', '".$dateformat."','<p>Please pay the invoice before {invoice_due_date} via {payment_methods}.</p>', '<p>Dear {contact_name},<br/><br/>Thank you for your business! Attached you will find the invoice for our products and/or services.<br/><br/>Please pay the invoice before {invoice_due_date} via {payment_methods}. Go to {pay_invoice_link} to select a payment method and start your payment.<br/><br/>{all_invoices_link}.<br/><br/>Kind Regards,<br/>{user_name}<br/>{company_name}.</p>', '<p>Dear {contact_name},<br/><br/>Unfortunately, we have not received payment for the invoice we have sent you previously on {invoice_last_send_date}. We kindly request that you make the payment as soon as possible.<br/><br/>If you go to {pay_invoice_link} you can select one of our payment methods ({payment_methods}) and start your payment.<br/><br/>{all_invoices_link}.<br/><br/>Kind regards,<br/>{user_name}<br/>{company_name}.</p>');";
		$db->setQuery( $query);
	    $db->query();
	}

	if(!JFolder::exists(JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment')) 
	{	
	
		if($current_version=="1.5")
		{
		//--moliideal
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoiceMollieIDEAL'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__plugins` ( `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES ('iDEAL Payment', 'ccinvoiceMollieIDEAL', 'ccinvoices_payment', 0, 1, 0, 0, 0, 0, '0000-00-00 00:00:00', '');";
				$db->setQuery( $sql);
				$db->query();
			}

		//--paypal
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoicePaypal'");
			if(!$db->loadResult())
			{		
				$sql="INSERT INTO `#__plugins` ( `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES ('Paypal Payment', 'ccinvoicePaypal', 'ccinvoices_payment', 0, 2, 0, 0, 0, 0, '0000-00-00 00:00:00', '');";
				$db->setQuery( $sql);
				$db->query();
			}

			//--offline
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoiceOffline'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__plugins` ( `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`) VALUES ('Offline Payment', 'ccinvoiceOffline', 'ccinvoices_payment', 0, 3, 0, 0, 0, 0, '0000-00-00 00:00:00', '');";
				$db->setQuery( $sql);
				$db->query();		
			}

			$sourcefile = JPATH_SITE.DS.'components'.DS.'com_ccinvoices'.DS.'j15'.DS.'ccinvoices_payment';
			$destinationfile = JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			JFolder::move($sourcefile, $destinationfile);					
			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment');
			}				
		}
		else
		{
			JFolder::move(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment', JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment');		
			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15');
			}						
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceMollieIDEAL'");
			if(!$db->loadResult())
			{			
				
				$query = "INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices iDEAL Payment', 'plugin', 'ccinvoiceMollieIDEAL', 'ccinvoices_payment', 0, 0, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices iDEAL payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices iDEAL payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),('ccInvoices Paypal Payment', 'plugin', 'ccinvoicePaypal', 'ccinvoices_payment', 0, 0, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices Paypal payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices Paypal payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),('ccInvoices Offline Payment', 'plugin', 'ccinvoiceOffline', 'ccinvoices_payment', 0, 0, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices Offline Payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices Offline payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0);";
				$db->setQuery( $query );
				$db->query();		
			}
		}
		
	}	
	else
	{
	
		if($current_version=="1.5")
		{	
			$destinationfile = JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			if(JFolder::exists($destinationfile)) {
				JFolder::delete($destinationfile);
			}				
			$sourcefile = JPATH_SITE.DS.'components'.DS.'com_ccinvoices'.DS.'j15'.DS.'ccinvoices_payment';
			$destinationfile = JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			JFolder::move($sourcefile, $destinationfile);		

			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment');
			}		
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoiceMollieIDEAL'");
			if($db->loadResult())
			{
				$sql="UPDATE `#__plugins` SET  `ordering` =  '1' WHERE element='ccinvoiceMollieIDEAL'";
				$db->setQuery( $sql);
				$db->query();
			}
			//--paypal
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoicePaypal'");
			if($db->loadResult())
			{
				$sql="UPDATE `#__plugins` SET  `ordering` =  '2' WHERE element='ccinvoicePaypal'";
				$db->setQuery( $sql);
				$db->query();
			}
			//--offline
			$db->setQuery("SELECT count(*) FROM  #__plugins WHERE element='ccinvoiceOffline'");
			if($db->loadResult())
			{
				$sql="UPDATE `#__plugins` SET  `ordering` =  '3' WHERE element='ccinvoiceOffline'";
				$db->setQuery( $sql);
				$db->query();
			}			
		}
		else
		{
			
			$destinationfile = JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			
			if(JFolder::exists($destinationfile)) {			
				JFolder::delete($destinationfile);
			}
			
			JFolder::move(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment', JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment');		
			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15');
			}			
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceMollieIDEAL'");
			if(!$db->loadResult())
			{			
				
				$query = "INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices iDEAL Payment', 'plugin', 'ccinvoiceMollieIDEAL', 'ccinvoices_payment', 0, 1, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices iDEAL payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices Paypal payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),('Paypal Payment', 'plugin', 'ccinvoicePaypal', 'ccinvoices_payment', 0, 1, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices Paypal payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices Paypal payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),('ccInvoices Offline Payment', 'plugin', 'ccinvoiceOffline', 'ccinvoices_payment', 0, 1, 1, 1, '{\"legacy\":true,\"name\":\"ccInvoices Offline payment\",\"type\":\"plugin\",\"creationDate\":\"May 26, 2011\",\"author\":\"Chill Creations\",\"copyright\":\"[2011] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.4.1\",\"description\":\"ccInvoices Paypal payment\",\"group\":\"\"}', '', '', '', 0, '0000-00-00 00:00:00', 3, 0);";
				$db->setQuery( $query );
				$db->query();		
			}
			
		}
	}
	
	

	
//==============

	
	
	$query = "SELECT contact_number FROM #__ccinvoices_contacts";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE  #__ccinvoices_contacts  ADD  `contact_number` INT( 11 ) NOT NULL";
		$db->setQuery( $query );
		$db->query();
	}
	
	$query = "SELECT symbol_display FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE  #__ccinvoices_configuration  ADD  `symbol_display` tinyint( 2 ) NOT NULL";
		$db->setQuery( $query );
		$db->query();
	}
	$query = "SELECT cformat FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `cformat` tinyint(2) NOT NULL  AFTER `symbol_display`";
		$db->setQuery( $query );
		$db->query();
	}
	$query = "SELECT email_cc FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `email_cc` varchar(100) NOT NULL  AFTER `cformat`";
		$db->setQuery( $query );
		$db->query();
		
		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `email_bcc` varchar(100) NOT NULL  AFTER `email_cc`";
		$db->setQuery( $query );
		$db->query();
		
		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `default_email_sub` varchar(250) NOT NULL  AFTER `default_note`";
		$db->setQuery( $query );
		$db->query();	

		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `rem_email_sub` varchar(250) NOT NULL  AFTER `default_email_sub`";
		$db->setQuery( $query );
		$db->query();
	
	}	
	$query = "SELECT tax_id FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `tax_id` varchar(50) NOT NULL  AFTER `email_bcc`";
		$db->setQuery( $query );
		$db->query();	
	}	
	
	
	$query = "SELECT reset_inv FROM #__ccinvoices_invoices";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `reset_inv` char(2) NOT NULL  AFTER `custom_invoice_number`";
		$db->setQuery( $query );
		$db->query();
	}

	$query = "SELECT ordering FROM #__ccinvoices_invoices";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_invoices` ADD   `ordering` int(10) NOT NULL AFTER `reset_inv`";
		$db->setQuery( $query );
		$db->query();
	}
	
	
	$query = "SELECT title FROM #__ccinvoices_templates";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_templates` ADD `title` varchar(200) NOT NULL  AFTER `id`";
		$db->setQuery( $query );
		$db->query();
		
		$query = "ALTER TABLE `#__ccinvoices_templates` ADD `edit_by` varchar(200) NOT NULL  AFTER `invoice_template`";
		$db->setQuery( $query );
		$db->query();
		
		$query = "ALTER TABLE `#__ccinvoices_templates` ADD `edit_date` int(11) NOT NULL  AFTER `edit_by`";
		$db->setQuery( $query );
		$db->query();		
	}

	
	
	
	$query = "SELECT user_company FROM #__ccinvoices_configuration";
	$db->setQuery( $query );
	if($db->query())
	{
		$query = "ALTER TABLE  #__ccinvoices_configuration  CHANGE  `user_company` `user_company` VARCHAR(70) NOT NULL";
		$db->setQuery( $query );
		$db->query();
	}	
	
	$query = "SELECT contact_number FROM #__ccinvoices_contacts";
	$db->setQuery( $query );
	if($db->query())
	{
		$query = "ALTER TABLE  #__ccinvoices_contacts  CHANGE  `contact_number` `contact_number` VARCHAR(30) NOT NULL";
		$db->setQuery( $query );
		$db->query();
	}	

	$query = "SELECT ordering FROM #__ccinvoices_contacts";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_contacts` ADD   `ordering` int(10) NOT NULL AFTER `email`";
		$db->setQuery( $query );
		$db->query();
	}	

	$query = "SELECT total_id FROM #__ccinvoices_invoices";
	$db->setQuery( $query );
	if($db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_invoices` DROP `total_id` , DROP `items_id`";
		$db->setQuery( $query );
		$db->query();
	}	
	
	$query = "SELECT quantity FROM #__ccinvoices_invoices";
	$db->setQuery( $query );
	if(!$db->query())
	{
		$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `tax` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `price` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `pname` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `quantity` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `total`  VARCHAR( 25 ) NOT NULL  AFTER `communication`, ADD `totaltax` VARCHAR( 25 ) NOT NULL  AFTER `communication`, ADD `subtotal` VARCHAR( 25 ) NOT NULL  AFTER `communication` , ADD `discount` VARCHAR( 25 ) NOT NULL  AFTER `communication`";
		$db->setQuery( $query );
		$db->query();
	}

	$query = "SELECT pname FROM #__ccinvoices_items";
	$db->setQuery( $query );
	if($db->query())
	{
		$query = "DROP TABLE `#__ccinvoices_items`";
		$db->setQuery( $query );
		$db->query();
	}	
	
	$query = "SELECT totaltax FROM #__ccinvoices_total";
	$db->setQuery( $query );
	if($db->query())
	{
		$query = "DROP TABLE `#__ccinvoices_total`";
		$db->setQuery( $query );
		$db->query();
	}	
	
	$query = "SELECT count(*) FROM #__ccinvoices_templates";
	$db->setQuery( $query );
	$temCount = $db->loadResult();
	if(!$temCount)
	{
		$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`, `invoice_template`) VALUES (1,'Invoice PDF', '<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"100%\" align=\"left\">\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"100%\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" align=\"left\" valign=\"top\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Company Name:</td>\r\n<td align=\"left\">{company_name}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">URL:</td>\r\n<td align=\"left\">{company_url}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Phone:</td>\r\n<td align=\"left\">{company_phone}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">E-mail:</td>\r\n<td align=\"left\">{company_email}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Address:</td>\r\n<td align=\"left\">{company_address}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width=\"50%\" align=\"center\" valign=\"middle\">{logo}</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"100%\" align=\"left\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td width=\"50%\" align=\"left\" valign=\"top\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td style=\"background-color:#D6D6D6;\" colspan=\"2\" align=\"left\">\r\n<h4 style=\"margin:0px;\">Customer Information</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Name:</td>\r\n<td align=\"left\">{contact_name}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Contact Number:</td>\r\n<td align=\"left\">{contact_number}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Contact:</td>\r\n<td align=\"left\">{contact}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Email:</td>\r\n<td align=\"left\">{contact_email}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Address:</td>\r\n<td align=\"left\">{contact_address}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n<td width=\"50%\" align=\"left\" valign=\"top\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td style=\"background-color:#D6D6D6;\" colspan=\"2\" align=\"left\">\r\n<h4 style=\"margin:0px;\">Invoice Information</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Invoice Number:</td>\r\n<td align=\"left\">{invoice_number}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Invoice Date:</td>\r\n<td align=\"left\">{invoice_date}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Due Date:</td>\r\n<td align=\"left\">{invoice_due_date}</td>\r\n</tr>\r\n<tr>\r\n<td width=\"50%\" align=\"left\">Send Date:</td>\r\n<td align=\"left\">{invoice_sent_date}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color:#D6D6D6;\" colspan=\"2\" align=\"left\">\r\n<h4 style=\"margin:0px;\">Order Items</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"100%\" align=\"left\">\r\n<table border=\"0\" cellspacing=\"0\" cellpadding=\"2\" width=\"100%\">\r\n<tbody>\r\n<tr>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">#</td>\r\n<td width=\"50%\" align=\"left\" valign=\"top\">Name</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">Price</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">Product Tax</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">Tax %</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">Sub Total</td>\r\n</tr>\r\n<tr>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">{item_quantity}</td>\r\n<td width=\"50%\" align=\"left\" valign=\"top\">{item_name}</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">{item_amount}</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">{product_tax}</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">{tax_percentage}</td>\r\n<td width=\"10%\" align=\"left\" valign=\"top\">{item_total_incl_tax}</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"5\" width=\"90%\" align=\"right\" valign=\"top\">Discount :</td>\r\n<td align=\"left\" width=\"10%\" valign=\"top\">{invoice_discount}</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"5\" width=\"90%\" align=\"right\" valign=\"top\">Subtotal :</td>\r\n<td align=\"left\" width=\"10%\" valign=\"top\">{invoice_subtotal}</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"5\"  width=\"90%\" align=\"right\" valign=\"top\">Tax total :</td>\r\n<td align=\"left\" width=\"10%\" valign=\"top\">{invoice_tax}</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"5\"  width=\"90%\" align=\"right\" valign=\"top\">Total :</td>\r\n<td align=\"left\" width=\"10%\" valign=\"top\">{invoice_total}</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td style=\"background-color:#D6D6D6;\" colspan=\"2\" align=\"left\">\r\n<h4 style=\"margin:0px;\">Invoice Note</h4>\r\n</td>\r\n</tr>\r\n<tr>\r\n<td colspan=\"2\" width=\"100%\" align=\"left\">\r\n<table style=\"width: 100%;\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\">\r\n<tbody>\r\n<tr>\r\n<td>{invoice_note}<br /></td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>\r\n</td>\r\n</tr>\r\n</tbody>\r\n</table>'),(2, 'Invoice export CSV', '{invoice_number},{invoice_date},{contact_name},{contact_number},{contact},{contact_address},{contact_email},{item_quantity}, {item_name}, {item_price}, {item_tax},{invoice_subtotal},{invoice_tax},{invoice_total}');";
		$db->setQuery( $query );
		$db->query();
	}else
	{
	
		$query = 'SELECT title FROM #__ccinvoices_templates where id = 1 LIMIT 1';
		$db->setQuery( $query );
		$title= $db->loadResult();
		if($title == '')
		{
			$sql = "UPDATE #__ccinvoices_templates SET title='Invoice PDF' WHERE id = 1";
			$db->setQuery($sql);
			$db->query();				
		}
		$query = 'SELECT count(*) FROM #__ccinvoices_templates where id = 2';
		$db->setQuery( $query );
		$count_tID= $db->loadResult();	
		if($count_tID == 0)
		{
			$sql = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`, `invoice_template`) VALUES (2, 'Invoice export CSV', '{invoice_number},{invoice_date},{contact_name},{contact_number},\r\n{contact},{invoice_number},{invoice_date},{contact_name},{contact_number},{contact}');";
			$db->setQuery($sql);
			$db->query();				
		}		
	
	}
	
	
	$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice note'";
	$db->setQuery( $query );
	$temCount = $db->loadResult();
	if(!$temCount)
	{
		$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (3,'Invoice note');";
		$db->setQuery( $query );
		$db->query();
	}
	$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice e-mail'";
	$db->setQuery( $query );
	$temCount = $db->loadResult();
	if(!$temCount)
	{
		$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (4,'Invoice e-mail');";
		$db->setQuery( $query );
		$db->query();
	}
	$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice e-mail reminder'";
	$db->setQuery( $query );
	$temCount = $db->loadResult();
	if(!$temCount)
	{
		$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (5,'Invoice e-mail reminder');";
		$db->setQuery( $query );
		$db->query();
	}
	
	$keyword1 = ",";
	$query = 'SELECT quantity,pname,price,tax,id FROM #__ccinvoices_invoices where lower(concat_ws(" ",quantity,pname,price,tax)) like "%'.$keyword1.'%"';
	$db->setQuery( $query );
	$affRows = $db->loadObjectList();
	foreach($affRows as $affRow)
	{
		$quantity = str_replace(",", "|", $affRow->quantity);
		$pname = str_replace(",", "|", $affRow->pname);
		$price = str_replace(",", "|", $affRow->price);
		$tax = str_replace(",", "|", $affRow->tax);
		$sql = "UPDATE #__ccinvoices_invoices SET quantity='".$quantity."',pname='".$pname."',price='".$price."',tax='".$tax."' WHERE id =".$affRow->id;
		$db->setQuery($sql);
		$db->query();
	}	
	
	
}
?>
