<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport( 'joomla.application.component.view' );
jimport('joomla.html.pagination');

class  ccinvoicesViewccinvoices  extends JView
{
	function display($tpl = null)
	{
		global $mainframe;
		$db =& JFactory::getDBO();
		$user	=& JFactory::getUser();
		$action=JRequest::getVar('action','');
		$ptype=JRequest::getVar('ptype','');

		$filter					= JRequest::getVar('jwd_filter');
		$sortColumn		= JRequest::getVar('filter_order','ordering');
		$sortOrder		= JRequest::getVar('filter_order_Dir','asc');
		$rows		= $this->get('data');
		$pagination		= $this->get('pagination');



		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
		$showPaymentIcon = 0;
		$paymentIconImg = "";
		$plugins_name=& JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher =& JDispatcher::getInstance();
		$paymentIcons = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$plugin_infos = JPluginHelper::getPlugin('ccinvoices_payment', $plugin=null);
		$i = 0;
		$k = 0;

		foreach($paymentIcons as $paymentIcon)
		{
			if($this->versionCompare()=="1.5")
				$paymentIconPath = JURI::root()."plugins/ccinvoices_payment/".$plugin_infos[$i]->name."/images/".$paymentIcon["icon"];
			else
				$paymentIconPath = JURI::root()."plugins/ccinvoices_payment/".$plugin_infos[$i]->name."/".$plugin_infos[$i]->name."/images/".$paymentIcon["icon"];
			if($plugin_infos[$i]->name != "ccinvoiceOffline")
			{
				if($k < 3)
				{
					$paymentIconImg .= "<img src='".$paymentIconPath."' title='".$paymentIcon["payment_method"]."'/>&nbsp;";
					$k++;
				}
			}
			$i++;
		}
		$i = 0;
		foreach($paymentIcons as $paymentIcon)
		{
			if($plugin_infos[$i]->name != "ccinvoiceOffline")
			{
				$showPaymentIcon = 1;
			}
			$i++;
		}
		$PaymentMethodMessage="";
		if($action=="showresult")
		{
			$id = JRequest::getVar("id","");
			$sql = "SELECT id,number,invoice_date,duedate FROM #__ccinvoices_invoices WHERE MD5(id) ='".$id."' LIMIT 1";
			$db->setQuery($sql);
			$row = $db->loadObject();
			$inv_id="";
			$inv_id[0] = $row->id;
			$PaymentMethodMessage = $dispatcher->trigger( 'onAfterSuccessfulPayment',array($inv_id));
			$message=$this->getPaymentPluginStatus($PaymentMethodMessage,$plugin_infos,$ptype);
		}
		$lists['order_Dir']	= $sortOrder;
		$lists['order']		= $sortColumn;
		$status[1] = JText::_( 'CC_CONCEPT');
        $status[2] = JText::_( 'CC_OPEN');
        $status[3] = JText::_( 'CC_LATE');
        $status[4] = JText::_( 'CC_PAID');
        $this->assignRef('status',	$status);
		$this->assignRef('lists',  $lists);
		$this->assignRef('currency_symbol',  $config->currency_symbol);
		$this->assignRef('symbol_display',	$config->symbol_display);
		$this->assignRef('cformat',		$config->cformat);
		$this->assignRef('date_format',  $config->date_format);
		$this->assignRef('showPaymentIcon',  $showPaymentIcon);
		$this->assignRef('paymentIconImg',  $paymentIconImg);
		$this->assignRef("message",$message);
		$this->assignRef("rows",$rows);
		parent::display($tpl);
	}
	function getPaymentPluginStatus($PaymentMethodMessage,$plugin_infos,$ptype)
	{
		$i = 0;
		foreach($PaymentMethodMessage as $PaymentMethodMessages)
		{

			if($plugin_infos[$i]->name == $ptype)
			{
				return $PaymentMethodMessages;
			}
			$i++;
		}
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
}