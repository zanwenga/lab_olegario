<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
// Getting menu id(Itemid)
$itemid = JRequest::getVar('Itemid');
?>
<style type="text/css">
tr, td {
    border: 0 solid #DDDDDD;
}
#tdRecStyle1 a:hover
{
	text-decoration: none;
	background-color: #F4F9FE;
}

</style>
<?php
if($this->message!="")
{
	?>
	<table style="width:100%;">
		<tr>
			<td align="center" width="100%">
				<table cellpadding="0" cellspacing="10" style="width:80%;border: 1px solid rgb(0, 0, 0);background-color:#FFEEED;border-color:#FFB5B3;">
					<tr>
						<td width="16"><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/cross.png"></td>
						<td align="left" >
							<b color="#4A4646">
							<?php
								echo $this->message;
							?>
							</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
<?php
}
?>
<table cellpadding="0" style="margin-top:-20px;" cellspacing="0" width="98%" border="0">
	<tr>
		<td>
			<h2 style="font-size:2.1em;line-height:30px;"><?php echo sprintf(JText::_("CC_PAY_INVOICE"),$this->num); ?></h2>
		</td>
	</tr>
	<tr>
		<td>
			<p style="text-align:justify;"><?php echo sprintf(JText::_("CC_PAYMENT_OVERVIEW_PAGE_MSG"),$this->user_company,$this->invoice_date,$this->duedate); ?></p>
		</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" width="100%" border="0">
				<?php

					$i = 0;
					foreach($this->PaymentMethodDetails AS $pminfo)
					{
						if(versionCompare()=="1.5")
							$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->plugin_info[$i]->name."/images/".$pminfo["logo"];
						else
							$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->plugin_info[$i]->name."/".$this->plugin_info[$i]->name."/images/".$pminfo["logo"];

						?>
							<tr>
								<td id="tdRecStyle1" style="border:solid 1px #e5eff8;background-color:#f4f9fe;">
									<?php echo $pminfo; ?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?php
						$i++;
					}
				?>
			</table>
		</td>
	</tr>
</table>

<?
function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,3);
}
?>