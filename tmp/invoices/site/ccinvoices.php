<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

$controllerName = 'ccinvoices';
require_once( JPATH_COMPONENT.DS.'controllers'.DS.$controllerName.'.php' );
$controllerName = 'ccinvoicesController'.$controllerName;

// Create the controller
$controller = new $controllerName();

// Perform the Request task
$controller->execute( JRequest::getCmd('task') );

// Redirect if set by the controller
$controller->redirect();

?>