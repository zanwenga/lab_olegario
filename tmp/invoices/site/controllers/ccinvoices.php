<?php
/**
* @package	 ccInvoice
* @author    Chill Creations <info@chillcreations.com>
* @link      http://www.chillcreations.com
* @copyright Copyright (C) 2009 - 2010 Chill Creations
* @license	 GNU/GPL, see LICENSE.php for full license.
**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.controller');

class ccinvoicesControllerccinvoices extends JController
{
	function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	function display()
	{
		$user	=& JFactory::getUser();
		if ( $user->get('guest')) {
			$url = base64_encode("index.php?option=com_ccinvoices&view=ccinvoices");
			if($this->versionCompare()=="1.5")
			$link = "index.php?option=com_user&view=login&return=".$url;
			else
				$link = "index.php?option=com_users&view=login&return=".$url;
			$this->setRedirect( $link);
		}
		JRequest::setVar( 'view', 'ccinvoices');
		parent::display();
	}
	function processPayment()
	{
		JRequest::setVar( 'view', 'payment' );
		JRequest::setVar( 'layout', 'process' );

		parent::display();
	}

	function paymentOverview()
	{
		/*$user	=& JFactory::getUser();
		$id = JRequest::getVar("id");
		if ( $user->get('guest')) {
			$url = base64_encode("index.php?option=com_ccinvoices&task=paymentOverview&id=".$id);
			$link = "index.php?option=com_user&view=login&return=".$url;
			$this->setRedirect( $link);
		}*/
		JRequest::setVar( 'view', 'payment');
		parent::display();
	}
	function paymentReturnUrl()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$inv_id = JRequest::getVar("id","0");

		$sql = "SELECT count(*) FROM #__ccinvoices_payment WHERE status=1 AND MD5(inv_id) ='".$inv_id."'";
		$db->setQuery($sql);
		$count_inv=$db->loadResult();
		if($count_inv > 0)
		{
        	JRequest::setVar( 'view', 'ccinvoices' );
		}
		else
		{
			JRequest::setVar( 'view', 'payment' );
		}
		parent::display();
	}
	function download()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","0");
		$user	=& JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id;
		$db->setQuery($sql);
		$count = $db->loadResult();
		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			$model   = $this->getModel('ccinvoices');
			$template = $model->getTemplateLayout($id);
			$file_path = $model->createInvoicePDF($template,$id);
			$model->downloadInvoice($file_path,$id);

		}
	}
	function viewInv()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","0");
		$user	=& JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id;
		$db->setQuery($sql);
		$count = $db->loadResult();
		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			$model   = $this->getModel('ccinvoices');
			$template = $model->getTemplateLayout($id);
			if($this->versionCompare()=="1.5")
			{
			?>
				<style type="text/css">
					.contentpane
					{
						font-family:Arial,Helvetica,sans-serif;
						font-size:11px;
						margin:10px;
						padding:0 0 1px;
						color:#000000;
					}
				</style>
			<?php
			}
			else
			{
			?>
				<style type="text/css">
					.contentpane
					{
						font-family:Arial,Helvetica,sans-serif;
						font-size:14px;
						margin:10px;
						padding:0 0 1px;
						color:#000000;
					}
					tr, td {
    				border: 0 solid #DDDDDD;
					}
				</style>
			<?php
			}
			echo $template;

		}
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function emailInv()
	{
		global $mainframe;
		$app = JFactory::getApplication();
		$sitename= $app->getCfg('sitename');
		//$sitename = $mainframe->getCfg( 'sitename' );
		$db = JFactory::getDBO();
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		if($conf->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC = $conf->email_cc;
		}
		if($conf->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC = $conf->email_bcc;
		}
		//$MailFrom	= $mainframe->getCfg('mailfrom');
		//$FromName	= $mainframe->getCfg('fromname');
		$MailFrom	= $conf->company_email;
		$FromName	= $conf->user_company;
		$confcom_username	= $conf->user_name;
		$id = JRequest::getInt("id","0");
		$invoice_send_date	= date("Y-m-d");
		$sql = "UPDATE #__ccinvoices_invoices SET invoice_sent_date = '".$invoice_send_date."' WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();
		$itemid = JRequest::getVar("Itemid","");
		$user	=& JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id;
		$db->setQuery($sql);
		$count = $db->loadResult();
		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			$model   = $this->getModel('ccinvoices');
			$template = $model->getTemplateLayout($id);
			$file_path = $model->createInvoicePDF($template,$id);
			$query	= "SELECT number  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
			$db->setQuery($query);
			$invNumber = $db->loadResult();
			$invoicesend_sub= JText::_( 'CC_EMAIL_INVOICE' );
			$email_subject = sprintf ( JText::_('CC_EMAIL_SUBJECT'), $model->getInvoiceNumberFormat($invNumber,$conf->invoice_format));
			$message = sprintf ( JText::_('CC_EMAIL_MSG'), $user->get('username'),$sitename,$confcom_username,$FromName);
			$message = html_entity_decode($message, ENT_QUOTES);
			JUtility::sendMail($MailFrom, $FromName, $user->get('email'), $email_subject, $message ,1,$EmailCC,$EmailBCC, $file_path);
			$msg = JText::_("CC_INVOICE_SEND_SUCCESS");
			$link = "index.php?option=com_ccinvoices&Itemid=".$itemid;
			$this->setRedirect( $link ,$msg);
		}
	}
}
?>