<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class invoiceControlleradmin extends JControllerLegacy {


	function __construct(){
        parent::__construct();
    }

	public function index(){
		
		$model = $this->getModel('admin');
		$company = $model->getCompany();
		$view = $this->getView('adminPanel', 'html');
		$view->datos = $company;
        $view->display();
	}

	public function saveCompany(){
		$data = array(
			'name' 		=> JRequest::getString('name'),
			'address' 	=> JRequest::getString('address'),
			'cif' 		=> JRequest::getString('cif'),
			'city' 		=> JRequest::getString('city'),
			'region' 	=> JRequest::getString('region'),
			'pc' 		=> JRequest::getString('pc'),
			'country' 	=> JRequest::getString('country'),
			'phone1' 	=> JRequest::getString('phone1'),
			'phone2' 	=> JRequest::getString('phone2'),
			'fax' 		=> JRequest::getString('fax'),
			'email1' 	=> JRequest::getString('email1'),
			'email2'	=> JRequest::getString('email2'),
		);
		$model = $this->getModel('admin');
		$model->updateCompany($data);
		echo json_encode(array('response'=>true));
	}
}