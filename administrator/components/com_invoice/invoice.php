<?php

defined('_JEXEC') or die('Restricted access');

if(!defined('DS')){
	define('DS', DIRECTORY_SEPARATOR);
}

require_once(JPATH_COMPONENT . DS .'controllers'. DS .'controllerAdmin.php');

$controller = new invoiceControllerAdmin( );
$task = JFactory::getApplication()->input->get('task');
if($task == ""){
	$task = 'index';
}
$controller->execute($task);