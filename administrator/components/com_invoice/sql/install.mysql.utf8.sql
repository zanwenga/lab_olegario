/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `#__invcmp_company`
--

DROP TABLE IF EXISTS `#__invcmp_company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_company` (
  `id_company` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `cif` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `pc` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email1` varchar(200) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `#__invcmp_company`
--

LOCK TABLES `#__invcmp_company` WRITE;
/*!40000 ALTER TABLE `#__invcmp_company` DISABLE KEYS */;
INSERT INTO `#__invcmp_company` VALUES (1,'DEFAULT S.A.','C/DEFAULT, 1, 2C','Z-111111111','DEFAULT','DEFAULT','11111','DEFAULT','999 999 999','666 666 666','777 777 777','email1@email.com','email2@email.com');
/*!40000 ALTER TABLE `#__invcmp_company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `#__invcmp_customer`
--

DROP TABLE IF EXISTS `#__invcmp_customer`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_customer` (
  `id_customer` int(7) NOT NULL AUTO_INCREMENT,
  `name` varchar(200) DEFAULT NULL,
  `cif` varchar(45) DEFAULT NULL,
  `address` varchar(200) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  `region` varchar(45) DEFAULT NULL,
  `pc` varchar(45) DEFAULT NULL,
  `country` varchar(45) DEFAULT NULL,
  `contact_name` varchar(200) DEFAULT NULL,
  `phone1` varchar(45) DEFAULT NULL,
  `phone2` varchar(45) DEFAULT NULL,
  `fax` varchar(45) DEFAULT NULL,
  `email1` varchar(200) DEFAULT NULL,
  `email2` varchar(200) DEFAULT NULL,
  `observations` varchar(1000) DEFAULT NULL,
  `idExt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_customer`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `#__invcmp_invoice`
--

DROP TABLE IF EXISTS `#__invcmp_invoice`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_invoice` (
  `id_invoice` int(7) NOT NULL AUTO_INCREMENT,
  `total_nt` float DEFAULT NULL,
  `total_wt` float DEFAULT NULL,
  `idExt` varchar(45) DEFAULT NULL,
  `id_company` int(7) NOT NULL,
  `id_customer` int(7) NOT NULL,
  `id_status_invoice` int(7) NOT NULL,
  `date_payment` date DEFAULT NULL,
  `date_emision` date DEFAULT NULL,
  `date_create` date DEFAULT NULL,
  `id_tax` int(7) NOT NULL,
  PRIMARY KEY (`id_invoice`,`id_company`,`id_customer`,`id_status_invoice`,`id_tax`),
  KEY `fk_invcmp_invoice_invcmp_company1_idx` (`id_company`),
  KEY `fk_invcmp_invoice_invcmp_customer1_idx` (`id_customer`),
  KEY `fk_invcmp_invoice_invcmp_invoice_status1_idx` (`id_status_invoice`),
  KEY `fk_invcmp_invoice_invcmp_invoice_tax1_idx` (`id_tax`),
  CONSTRAINT `fk_invcmp_invoice_invcmp_company1` FOREIGN KEY (`id_company`) REFERENCES `#__invcmp_company` (`id_company`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invcmp_invoice_invcmp_customer1` FOREIGN KEY (`id_customer`) REFERENCES `#__invcmp_customer` (`id_customer`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invcmp_invoice_invcmp_invoice_status1` FOREIGN KEY (`id_status_invoice`) REFERENCES `#__invcmp_invoice_status` (`id_status_invoice`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_invcmp_invoice_invcmp_invoice_tax1` FOREIGN KEY (`id_tax`) REFERENCES `#__invcmp_tax` (`id_tax`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `#__invcmp_invoice_detail`
--

DROP TABLE IF EXISTS `#__invcmp_invoice_detail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_invoice_detail` (
  `id_detail` int(7) NOT NULL AUTO_INCREMENT,
  `description` varchar(200) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `id_invoice` int(7) NOT NULL,
  `id_product` int(7) NOT NULL,
  `num_products` int(4) DEFAULT '1',
  PRIMARY KEY (`id_detail`,`id_invoice`,`id_product`),
  KEY `fk_invcmp_invoice_detail_invcmp_invoice_idx` (`id_invoice`),
  KEY `fk_invcmp_invoice_detail_invcmp_product1_idx` (`id_product`),
  CONSTRAINT `fk_invcmp_invoice_detail_invcmp_invoice` FOREIGN KEY (`id_invoice`) REFERENCES `#__invcmp_invoice` (`id_invoice`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_invcmp_invoice_detail_invcmp_product1` FOREIGN KEY (`id_product`) REFERENCES `#__invcmp_product` (`id_product`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `#__invcmp_invoice_status`
--

DROP TABLE IF EXISTS `#__invcmp_invoice_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_invoice_status` (
  `id_status_invoice` int(7) NOT NULL AUTO_INCREMENT,
  `detail` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id_status_invoice`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `#__invcmp_invoice_status`
--

LOCK TABLES `#__invcmp_invoice_status` WRITE;
/*!40000 ALTER TABLE `#__invcmp_invoice_status` DISABLE KEYS */;
INSERT INTO `#__invcmp_invoice_status` VALUES (1,'Borrador'),(2,'Enviada'),(3,'Pagada');
/*!40000 ALTER TABLE `#__invcmp_invoice_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `#__invcmp_product`
--

DROP TABLE IF EXISTS `#__invcmp_product`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_product` (
  `id_product` int(7) NOT NULL AUTO_INCREMENT,
  `stock` int(7) DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  `price` float DEFAULT NULL,
  `idExt` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_product`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `#__invcmp_tax`
--

DROP TABLE IF EXISTS `#__invcmp_tax`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `#__invcmp_tax` (
  `id_tax` int(7) NOT NULL AUTO_INCREMENT,
  `tax` float DEFAULT NULL,
  `description` varchar(200) DEFAULT NULL,
  PRIMARY KEY (`id_tax`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `#__invcmp_tax`
--

LOCK TABLES `#__invcmp_tax` WRITE;
/*!40000 ALTER TABLE `#__invcmp_tax` DISABLE KEYS */;
INSERT INTO `#__invcmp_tax` VALUES (1,0.21,'IVA 21%'),(2,0.1,'IVA 10%');
/*!40000 ALTER TABLE `#__invcmp_tax` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;