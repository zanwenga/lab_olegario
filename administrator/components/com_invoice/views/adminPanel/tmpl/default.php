<form id="adminForm">
    
    <div class="form-horizontal">
        <fieldset class="adminform">
            <legend>DATOS DE LA EMPRESA FACTURADORA:</legend>
            <div class="row-fluid info-message" style="display:none;">
	            <div class="alert alert-info" role="info">
				  	Datos guardados con éxito
				</div>
			</div>
            <div class="row-fluid">
            	<button id="saveButton" style="float:right;" class="btn btn-default">Guardar</button>
            </div>
            <div class="row-fluid">
                <div class="span6">
                    <div class="control-group">
                        <div class="control-label">Nombre de la compañía</div>
                        <div class="controls"><input type="text" name="name"  value="<?php echo $this->datos['name'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Cif</div>
                        <div class="controls"><input type="text" name="cif" value="<?php echo $this->datos['cif'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Dirección</div>
                        <div class="controls"><input type="text" name="address" value="<?php echo $this->datos['address'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Ciudad</div>
                        <div class="controls"><input type="text" name="city" value="<?php echo $this->datos['city'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">CP</div>
                        <div class="controls"><input type="text" name="pc" value="<?php echo $this->datos['pc'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Provincia</div>
                        <div class="controls"><input type="text" name="region" value="<?php echo $this->datos['region'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">País</div>
                        <div class="controls"><input type="text" name="country" value="<?php echo $this->datos['country'];?>"></div>
                    </div>
                </div>
                <div class="span6">
                    <div class="control-group">
                        <div class="control-label">Teléfono 1</div>
                        <div class="controls"><input type="text" name="phone1" value="<?php echo $this->datos['phone1'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Teléfono 2</div>
                        <div class="controls"><input type="text" name="phone2" value="<?php echo $this->datos['phone2'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Fax</div>
                        <div class="controls"><input type="text" name="fax" value="<?php echo $this->datos['fax'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Email 1</div>
                        <div class="controls"><input type="text" name="email1" value="<?php echo $this->datos['email1'];?>"></div>
                    </div>
                    <div class="control-group">
                        <div class="control-label">Email 2</div>
                        <div class="controls"><input type="text" name="email2" value="<?php echo $this->datos['email2'];?>"></div>
                    </div>
                <div>
            <div>
        </fieldset>
    <div>
    <span id="path" style="display:none;"><?php echo JRoute::_('index.php?option=com_invoice&task=saveCompany&format=raw', true);?></span>
</form>
<script>
jQuery(function($){
	
	$('#saveButton').click(function(e){
		e.preventDefault();
		var url 	= $('#path').text();
		var data 	= getFormData();
		$.post(url, data, function(res){
			var res = JSON.parse(res);
			$('.info-message').toggle(100);
			setTimeout(function(){
				$('.info-message').toggle(100);
			},3000);
		});
	});


	var getFormData = function(){
		var array = $('#adminForm').serializeArray();
		var data = {};
		$.each(array, function(index,element){
			data[element.name] = element.value;
		});
		return data;
	}
});
</script>
