<?php

defined('_JEXEC') or die('Invalid access');
jimport('joomla.application.component.model');

class invoiceModeladmin extends JModelLegacy {
	
	/* COMPANY */
	function getCompany(){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_company' );
        $query->where('id_company = 1');
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	}

	function updateCompany($data){
		$data['id_company'] = 1;
		$row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->updateObject('#__invcmp_company', $row, 'id_company');
        return $result;
	}
}