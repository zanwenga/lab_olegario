<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
@define( 'DS', DIRECTORY_SEPARATOR );
/**
 * Script file of HelloWorld component
 */
class com_ccinvoicesInstallerScript
{
	function install($parent) 
	{
		$msg='<b>ccInvoices has been installed successfully.</b>';
		JFactory::getApplication()->enqueueMessage($msg);

	}
	function uninstall($parent) 
	{
		jimport( 'joomla.filesystem.folder' );
		jimport('joomla.filesystem.file');	
		$db = JFactory::getDBO();
		
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		$current_version=substr($current_version,0,3);


		$sql="DELETE FROM `#__assets` WHERE name='com_ccinvoices'";
		$db->setQuery( $sql);
		$db->query();	


		if (JFolder::exists(JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment'))
		{
			$destinationfile=JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			JFolder::delete($destinationfile);
			if($current_version=="1.5")
			{
				$db->setQuery("SELECT count(*) FROM  #__plugins WHERE folder='ccinvoices_payment'");
				if($db->loadResult())
				{
					$sql="DELETE FROM `#__plugins` WHERE folder='ccinvoices_payment'";
					$db->setQuery( $sql);
					$db->query();			
				}
			}
			else
			{
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE folder='ccinvoices_payment'");
				if($db->loadResult())
				{
					$sql="DELETE FROM `#__extensions` WHERE folder='ccinvoices_payment'";
					$db->setQuery( $sql);
					$db->query();			
				}		
			}
		}
		// Message area
		$msg='<b>ccInvoices has been uninstalled successfully.</b>';
		JFactory::getApplication()->enqueueMessage($msg);		
	}
	function update($parent) 
	{
		$msg='<b>ccInvoices has been updated successfully.</b>';
		JFactory::getApplication()->enqueueMessage($msg);
	}

	
	function preflight($type, $parent) 
	{	
		
		
	}

   
	function postflight($type, $parent) 
	{

		jimport( 'joomla.filesystem.folder' );
		jimport('joomla.filesystem.file');
		
		
		$dir=JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'assets';
		if (is_dir($dir)) {
			if ($dh = opendir($dir)) {
				while (($file = readdir($dh)) !== false) {
					if(substr($file,strlen($file)-4)==".pdf" or substr($file,strlen($file)-4)==".csv")
					{
						JFile::delete(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'assets'.DS.$file);
					}
				}
				closedir($dh);
			}
		}

		$dir=JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'assets'.DS.'logo';
		if(is_dir($dir)) 
		{
			$path2 = JPath::clean(JPATH_ROOT.DS."media".DS."com_ccinvoices");
			JFolder::create($path2);	
			JFolder::move($dir,JPATH_ROOT.DS.'media'.DS.'com_ccinvoices'.DS.'logo');
		}	
		//JFolder::move($sourcefile, $destinationfile);
		
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		$current_version=substr($current_version,0,3);

		$plugin_tablename="";
		if($current_version=="1.5")
		{
			$plugin_tablename="#__plugins";	
			$plugin_tab_fields="( `name`, `element`, `folder`, `access`, `ordering`, `published`, `iscore`, `client_id`, `checked_out`, `checked_out_time`, `params`)";
		}
		else
		{
			$plugin_tablename="#__extensions";
			$plugin_tab_fields="(`type`, `name`, `element`, `folder`,`enabled`, `access`,`protected`, `ordering`, `state`, `client_id`, `checked_out`, `checked_out_time`, `params`,`custom_data`,`system_data`)";
		}

		$db	= JFactory::getDBO();
		$query = "SELECT count(*) FROM #__ccinvoices_configuration";
		$db->setQuery( $query );
		$confCount = $db->loadResult();
		$dateformat="%m-%d-%y";
			
		if(!$confCount)
		{
			$query = "INSERT INTO `#__ccinvoices_configuration` (`id`, `tax`, `user_name`, `user_company`, `date_format`,`default_note`, `default_email`, `default_email_rem`) VALUES (1, '', '', '', '".$dateformat."','<p>Please pay the invoice before {invoice_due_date} via {payment_methods}.</p>', '<p>Dear {contact_name},<br/><br/>Thank you for your business! Attached you will find the invoice for our products and/or services.<br/><br/>Please pay the invoice before {invoice_due_date} via {payment_methods}. Go to {pay_invoice_link} to select a payment method and start your payment.<br/><br/>{all_invoices_link}.<br/><br/>Kind Regards,<br/>{user_name}<br/>{company_name}.</p>', '<p>Dear {contact_name},<br/><br/>Unfortunately, we have not received payment for the invoice we have sent you previously on {invoice_last_send_date}. We kindly request that you make the payment as soon as possible.<br/><br/>If you go to {pay_invoice_link} you can select one of our payment methods ({payment_methods}) and start your payment.<br/><br/>{all_invoices_link}.<br/><br/>Kind regards,<br/>{user_name}<br/>{company_name}.</p>');";
			$db->setQuery( $query);
			$db->query();
		}
		//$ToCheckOutParams='{"acc_no":"","test":"true","currency_code":""}';
		//$paypalParams='{"email_id":"","test":"1","currency_code":""}';
		//$molliParams='{"partner_id":"","test":"1"}';
		
		$manifest_cache_mollie='{\"legacy\":true,\"name\":\"ccInvoices - Payment - iDEAL\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"Chill Creations\",\"copyright\":\"[2013] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.7.0\",\"description\":\"ccInvoices iDEAL payment\",\"group\":\"\"}';

		$manifest_cache_paypal='{\"legacy\":true,\"name\":\"ccInvoices - Payment - Paypal\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"Chill Creations\",\"copyright\":\"[2013] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.7.0\",\"description\":\"ccInvoices Paypal payment\",\"group\":\"\"}';	

		$manifest_cache_2checkout='{\"legacy\":true,\"name\":\"ccInvoices - Payment - 2Checkout\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"Chill Creations\",\"copyright\":\"[2013] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.7.0\",\"description\":\"ccInvoices 2Checkout payment\",\"group\":\"\"}';	

		$manifest_cache_AuthoRize='{\"legacy\":true,\"name\":\"ccInvoices - Payment - AuthorizeDotnet\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"Chill Creations\",\"copyright\":\"[2013] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.7.0\",\"description\":\"ccInvoices - Payment - AuthorizeDotnet\",\"group\":\"\"}';	
		
		$manifest_cache_offline='{\"legacy\":true,\"name\":\"ccInvoices - Payment - Offline\",\"type\":\"plugin\",\"creationDate\":\"August 2012\",\"author\":\"Chill Creations\",\"copyright\":\"[2013] Chill Creations\",\"authorEmail\":\"info@chillcreations.com\",\"authorUrl\":\"http:\\/\\/www.chillcreations.com\",\"version\":\"1.7.0\",\"description\":\"ccInvoices Offline payment\",\"group\":\"\"}';		
		
		if(!JFolder::exists(JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment')) 
		{	
			JFolder::move(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment', JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment');		
			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15');
			}					

			//--moliideal,paypal,offline
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceMollieIDEAL'");
			if(!$db->loadResult())
			{			
				
				$query = "INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - iDEAL', 'plugin', 'ccinvoiceMollieIDEAL', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_mollie."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),('ccInvoices - Payment - Paypal', 'plugin', 'ccinvoicePaypal', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_paypal."', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),('ccInvoices - Payment - Offline', 'plugin', 'ccinvoiceOffline', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_offline."', '', '', '', 0, '0000-00-00 00:00:00', 3, 0);";
				$db->setQuery( $query );
				$db->query();		
			}
			
			//--2Checkout
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoice2Checkout'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - 2Checkout', 'plugin', 'ccinvoice2Checkout', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_2checkout."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0);";
				$db->setQuery( $sql);
				$db->query();
			}	
			
			//--Authorizedotnet
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceAuthorizedotnet'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - AuthorizeDotnet', 'plugin', 'ccinvoiceAuthorizedotnet', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_AuthoRize."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0);";
				$db->setQuery( $sql);
				$db->query();
			}								
			
		}	
		else
		{
		
			$destinationfile = JPATH_SITE.DS.'plugins'.DS.'ccinvoices_payment';
			
			if(JFolder::exists($destinationfile)) {			
				JFolder::delete($destinationfile);
			}
			
			JFolder::move(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'ccinvoices_payment', JPATH_ROOT.DS.'plugins'.DS.'ccinvoices_payment');		
			if(JFolder::exists(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15')) {
				JFolder::delete(JPATH_ROOT.DS.'components'.DS.'com_ccinvoices'.DS.'j15');
			}			
			
			//--moliideal,paypal,offline
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceMollieIDEAL'");
			if(!$db->loadResult())
			{			
				
				$query = "INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - iDEAL', 'plugin', 'ccinvoiceMollieIDEAL', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_mollie."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0),('ccInvoices - Payment - Paypal', 'plugin', 'ccinvoicePaypal', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_paypal."', '', '', '', 0, '0000-00-00 00:00:00', 2, 0),('ccInvoices - Payment - Offline', 'plugin', 'ccinvoiceOffline', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_offline."', '', '', '', 0, '0000-00-00 00:00:00', 3, 0);";
				$db->setQuery( $query );
				$db->query();
			}
			else
			{
				//--mollee
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceMollieIDEAL' AND type='plugin'");
				if($db->loadResult())
				{
					$sql="UPDATE `#__extensions` SET name='ccInvoices - Payment - iDEAL',`ordering` =  '1',`manifest_cache`='".$manifest_cache_mollie."' WHERE element='ccinvoiceMollieIDEAL' AND type='plugin'";
					$db->setQuery( $sql);
					$db->query();
				}
				//--paypal
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoicePaypal' AND type='plugin'");
				if($db->loadResult())
				{
					$sql="UPDATE `#__extensions` SET name='ccInvoices - Payment - Paypal',`ordering` =  '2',`manifest_cache`='".$manifest_cache_paypal."' WHERE element='ccinvoicePaypal' AND type='plugin'";
					$db->setQuery( $sql);
					$db->query();
				}				
				
				//--offline
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceOffline' AND type='plugin'");
				if($db->loadResult())
				{
					$sql="UPDATE `#__extensions` SET name='ccInvoices - Payment - Offline',`ordering` =  '5',`manifest_cache`='".$manifest_cache_offline."' WHERE element='ccinvoiceOffline' AND type='plugin'";
					$db->setQuery( $sql);
					$db->query();
				}						
			}
			
			//--2Checkout
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoice2Checkout'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - 2Checkout', 'plugin', 'ccinvoice2Checkout', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_2checkout."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0);";
				$db->setQuery( $sql);
				$db->query();
			}	
			else
			{
				//--2check
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoice2Checkout'");
				if($db->loadResult())
				{
					$sql="UPDATE `#__extensions` SET name='ccInvoices - Payment - 2Checkout',`ordering` =  '3',`manifest_cache`='".$manifest_cache_2checkout."' WHERE element='ccinvoice2Checkout'";
					$db->setQuery( $sql);
					$db->query();
				}					
			}

			//--Authorizedotnet
			$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceAuthorizedotnet'");
			if(!$db->loadResult())
			{
				$sql="INSERT INTO `#__extensions` ( `name`, `type`, `element`, `folder`, `client_id`, `enabled`, `access`, `protected`, `manifest_cache`, `params`, `custom_data`, `system_data`, `checked_out`, `checked_out_time`, `ordering`, `state`) VALUES ('ccInvoices - Payment - AuthorizeDotnet ', 'plugin', 'ccinvoiceAuthorizedotnet', 'ccinvoices_payment', 0, 0, 1, 0, '".$manifest_cache_AuthoRize."', '', '', '', 0, '0000-00-00 00:00:00', 1, 0);";
				$db->setQuery( $sql);
				$db->query();
			}				
			else
			{
				
				$db->setQuery("SELECT count(*) FROM  #__extensions WHERE element='ccinvoiceAuthorizedotnet'");
				if($db->loadResult())
				{
					$sql="UPDATE `#__extensions` SET name='ccInvoices - Payment - AuthorizeDotnet ',`ordering` =  '4',`manifest_cache`='".$manifest_cache_AuthoRize."' WHERE element='ccinvoiceAuthorizedotnet'";
					$db->setQuery( $sql);
					$db->query();
				}					
			}					
		}
		
		

		
	//==============			
		if($this->checkFieldExist("#__ccinvoices_contacts","contact_number"))
		{
			$query = "ALTER TABLE  #__ccinvoices_contacts  ADD  `contact_number` INT( 11 ) NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}
		
		if($this->checkFieldExist("#__ccinvoices_configuration","symbol_display"))
		{
			$query = "ALTER TABLE  #__ccinvoices_configuration  ADD  `symbol_display` tinyint( 2 ) NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}

		if($this->checkFieldExist("#__ccinvoices_configuration","cformat"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `cformat` tinyint(2) NOT NULL  AFTER `symbol_display`";
			$db->setQuery( $query );
			$db->query();
		}

		if($this->checkFieldExist("#__ccinvoices_configuration","email_cc"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `email_cc` varchar(100) NOT NULL  AFTER `cformat`";
			$db->setQuery( $query );
			$db->query();
			
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `email_bcc` varchar(100) NOT NULL  AFTER `email_cc`";
			$db->setQuery( $query );
			$db->query();
			
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `default_email_sub` varchar(250) NOT NULL  AFTER `default_note`";
			$db->setQuery( $query );
			$db->query();	

			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `rem_email_sub` varchar(250) NOT NULL  AFTER `default_email_sub`";
			$db->setQuery( $query );
			$db->query();
		
		}	

		if($this->checkFieldExist("#__ccinvoices_configuration","tax_id"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `tax_id` varchar(50) NOT NULL  AFTER `email_bcc`";
			$db->setQuery( $query );
			$db->query();	
		}	
		
		if($this->checkFieldExist("#__ccinvoices_configuration","newusermail_sub"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `newusermail_sub` varchar(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();	
			
			$query = "UPDATE `#__ccinvoices_configuration` SET newusermail_sub='Account Details for {contact} at {sitename}'";
			$db->setQuery( $query );
			$db->query();			
		}
				
		if($this->checkFieldExist("#__ccinvoices_invoices","reset_inv"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `reset_inv` char(2) NOT NULL  AFTER `custom_invoice_number`";
			$db->setQuery( $query );
			$db->query();
		}

		if($this->checkFieldExist("#__ccinvoices_invoices","ordering"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` ADD   `ordering` int(10) NOT NULL AFTER `reset_inv`";
			$db->setQuery( $query );
			$db->query();
		}
				
		if($this->checkFieldExist("#__ccinvoices_templates","title"))
		{
			$query = "ALTER TABLE `#__ccinvoices_templates` ADD `title` varchar(200) NOT NULL  AFTER `id`";
			$db->setQuery( $query );
			$db->query();
			
			$query = "ALTER TABLE `#__ccinvoices_templates` ADD `edit_by` varchar(200) NOT NULL  AFTER `invoice_template`";
			$db->setQuery( $query );
			$db->query();
			
			$query = "ALTER TABLE `#__ccinvoices_templates` ADD `edit_date` int(11) NOT NULL  AFTER `edit_by`";
			$db->setQuery( $query );
			$db->query();		
		}

		if(!$this->checkFieldExist("#__ccinvoices_configuration","user_company"))
		{
			$query = "ALTER TABLE  #__ccinvoices_configuration  CHANGE  `user_company` `user_company` VARCHAR(70) NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}	
		
		if(!$this->checkFieldExist("#__ccinvoices_contacts","contact_number"))
		{
			$query = "ALTER TABLE  #__ccinvoices_contacts  CHANGE  `contact_number` `contact_number` VARCHAR(30) NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}	

		if($this->checkFieldExist("#__ccinvoices_contacts","ordering"))
		{
			$query = "ALTER TABLE `#__ccinvoices_contacts` ADD   `ordering` int(10) NOT NULL AFTER `email`";
			$db->setQuery( $query );
			$db->query();
		}	

		if($this->checkFieldExist("#__ccinvoices_contacts","tax_id"))
		{
			$query = "ALTER TABLE `#__ccinvoices_contacts` ADD   `tax_id` VARCHAR( 30 ) NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}

		if(!$this->checkFieldExist("#__ccinvoices_invoices","total_id"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` DROP `total_id` , DROP `items_id`";
			$db->setQuery( $query );
			$db->query();
		}	
				
		if($this->checkFieldExist("#__ccinvoices_invoices","quantity"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `tax` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `price` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `pname` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `quantity` MEDIUMTEXT NOT NULL  AFTER `communication`, ADD `total`  VARCHAR( 25 ) NOT NULL  AFTER `communication`, ADD `totaltax` VARCHAR( 25 ) NOT NULL  AFTER `communication`, ADD `subtotal` VARCHAR( 25 ) NOT NULL  AFTER `communication` , ADD `discount` VARCHAR( 25 ) NOT NULL  AFTER `communication`";
			$db->setQuery( $query );
			$db->query();
		}

		if(!$this->checkFieldExist("#__ccinvoices_items","pname"))
		{
			$query = "DROP TABLE `#__ccinvoices_items`";
			$db->setQuery( $query );
			$db->query();
		}	
			
		if($this->checkFieldExist("#__ccinvoices_configuration","pdf_layout"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdf_layout` TINYINT( 1 ) NOT NULL ";
			$db->setQuery( $query );
			$db->query();
		}	 	
		
		$query = "SELECT count(*) FROM #__ccinvoices_templates";
		$db->setQuery( $query );
		$temCount = $db->loadResult();
		
		$PDFTemp='<table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td align="left" width="100%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td width="100%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td align="left" valign="top" width="50%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td align="left" width="50%">Company name:</td><td align="left">{company_name}</td></tr><tr><td align="left" width="50%">URL:</td><td align="left">{company_url}</td></tr><tr><td align="left" width="50%">Phone:</td><td align="left">{company_phone}</td></tr><tr><td align="left" width="50%">E-mail:</td><td align="left">{company_email}</td></tr><tr><td align="left" width="50%">Tax ID:</td><td align="left"><span>{company_tax_id}</span></td></tr><tr><td align="left" width="50%">Address:</td><td align="left">{company_address}</td></tr></tbody></table></td><td align="center" valign="middle" width="50%">{logo}</td></tr><tr><td colspan="2" align="left" width="100%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td align="left" valign="top" width="50%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td style="background-color: #d6d6d6;" colspan="2" align="left"><h4 style="margin: 0px;">Customer Information</h4></td></tr><tr><td align="left" width="50%">Name:</td><td align="left">{contact_name}</td></tr><tr><td align="left" width="50%">Contact number:</td><td align="left">{contact_number}</td></tr><tr><td align="left" width="50%">Contact person:</td><td align="left">{contact_person}</td></tr><tr><td align="left" width="50%">Email:</td><td align="left">{contact_email}</td></tr><tr><td align="left" width="50%">Tax ID:</td><td align="left">{contact_tax_id}</td></tr><tr><td align="left" width="50%">Address:</td><td align="left">{contact_address}</td></tr></tbody></table></td><td align="left" valign="top" width="50%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td style="background-color: #d6d6d6;" colspan="2" align="left"><h4 style="margin: 0px;">Invoice Information</h4></td></tr><tr><td align="left" width="50%">Invoice number:</td><td align="left">{invoice_number}</td></tr><tr><td align="left" width="50%">Invoice status:</td><td align="left"><span>{invoice_status}</span></td></tr><tr><td align="left" width="50%">Invoice date:</td><td align="left">{invoice_date}</td></tr><tr><td align="left" width="50%">Due date:</td><td align="left">{invoice_due_date}</td></tr><tr><td align="left" width="50%">Send date:</td><td align="left">{invoice_sent_date}</td></tr><tr><td align="left" width="50%">Paid date:</td><td align="left">{paid_date}</td></tr></tbody></table></td></tr></tbody></table></td></tr><tr><td style="background-color: #d6d6d6;" colspan="2" align="left"><h4 style="margin: 0px;">Order Items</h4></td></tr><tr><td colspan="2" align="left" width="100%"><table style="width: 100%;" border="0" cellpadding="0" cellspacing="0"><tbody><tr><td align="left" valign="top" width="10%">#</td><td align="left" valign="top" width="50%">Name</td><td align="left" valign="top" width="10%">Price</td><td align="left" valign="top" width="10%">Tax</td><td align="left" valign="top" width="10%">Tax %</td><td align="left" valign="top" width="10%">Subtotal</td></tr><tr><td colspan="6" align="left" valign="top" width="100%">{template_items}</td></tr><tr><td colspan="5" align="right" valign="top" width="90%">Discount:</td><td align="left" valign="top" width="10%">{invoice_discount}</td></tr><tr><td colspan="5" align="right" valign="top" width="90%">Subtotal:</td><td align="left" valign="top" width="10%">{invoice_subtotal}</td></tr><tr><td colspan="5" align="right" valign="top" width="90%">Tax:</td><td align="left" valign="top" width="10%"><span>{invoice_tax_total}</span></td></tr><tr><td colspan="5" align="right" valign="top" width="90%">Total:</td><td align="left" valign="top" width="10%">{invoice_total}</td></tr></tbody></table></td></tr><tr><td style="background-color: #d6d6d6;" colspan="2" align="left"><h4 style="margin: 0px;">Invoice Note</h4></td></tr><tr><td colspan="2" align="left" width="100%"><table style="width: 100%;" border="0" cellpadding="2" cellspacing="0"><tbody><tr><td>{invoice_note}</td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>';

		if(!$temCount)
		{
			$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`, `invoice_template`) VALUES (1,'Invoice PDF', '".$PDFTemp."'),(2, 'Invoice export CSV', '{invoice_number},{invoice_date},{contact_name},{contact_number},{contact},{contact_address},{contact_taxid},{contact_email},{item_quantity}, {item_name}, {item_price_excl_tax}, {item_tax_percentage},{invoice_subtotal_excl_discount},{invoice_tax_total},{invoice_total_incl_tax}');";
			$db->setQuery( $query );
			$db->query();
			
			$query = "UPDATE `#__ccinvoices_configuration` SET pdf_layout=1";
			$db->setQuery( $query );
			$db->query();	

		}else
		{

			if($this->checkFieldExist("#__ccinvoices_configuration","items_template"))
			{
				$query = "UPDATE `#__ccinvoices_configuration` SET pdf_layout=0";
				$db->setQuery( $query );
				$db->query();		
			}
			
			$query = 'SELECT title FROM #__ccinvoices_templates where id = 1 LIMIT 1';
			$db->setQuery( $query );
			$title= $db->loadResult();
			if($title == '')
			{
				$sql = "UPDATE #__ccinvoices_templates SET title='Invoice PDF' WHERE id = 1";
				$db->setQuery($sql);
				$db->query();				
			}
			$query = 'SELECT count(*) FROM #__ccinvoices_templates where id = 2';
			$db->setQuery( $query );
			$count_tID= $db->loadResult();	
			if($count_tID == 0)
			{
				$sql = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`, `invoice_template`) VALUES (2, 'Invoice export CSV', '{invoice_number},{invoice_date},{contact_name},{contact_number},\r\n{contact},{invoice_number},{invoice_date},{contact_name},{contact_number},{contact}');";
				$db->setQuery($sql);
				$db->query();				
			}		
		
		}
		
		
		$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice note'";
		$db->setQuery( $query );
		$temCount = $db->loadResult();
		if(!$temCount)
		{
			$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (3,'Invoice note');";
			$db->setQuery( $query );
			$db->query();
		}
		$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice e-mail'";
		$db->setQuery( $query );
		$temCount = $db->loadResult();
		if(!$temCount)
		{
			$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (4,'Invoice e-mail');";
			$db->setQuery( $query );
			$db->query();
		}
		$query = "SELECT count(*) FROM #__ccinvoices_templates WHERE title='Invoice e-mail reminder'";
		$db->setQuery( $query );
		$temCount = $db->loadResult();
		if(!$temCount)
		{
			$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`) VALUES (5,'Invoice e-mail reminder');";
			$db->setQuery( $query );
			$db->query();
		}
		
		$query = 'SELECT count(*) FROM #__ccinvoices_templates where id = 6';
		$db->setQuery( $query );
		$count_tID= $db->loadResult();	
		if(!$count_tID)
		{
			$query = "INSERT INTO `#__ccinvoices_templates` (`id`,`title`,`invoice_template`) VALUES (6,'New user e-mail','<p>Hello {contact},<br /> <br /> We have created an account for you on {sitename}, so you can use the below details to login and view the invoices for {contact_name}:<br /> <br /> Username: {contact_username}<br /> Passord: {password}<br /> <br /> Kind regards<br /> {user_name}<br /> {company_name}</p>');";
			$db->setQuery( $query );
			$db->query();
		}




		
		//configuration date issue solved.
		
		$sql = "SELECT date_format FROM #__ccinvoices_configuration WHERE id = 1 LIMIT 1";
		$db->setQuery($sql);
		$dtformat = $db->loadResult();

		$flag="0";
		$newstr="";
		if($dtformat!="")
		{
			for($i=0;$i<=strlen($dtformat)-1;$i++)
			{
				if(trim(substr($dtformat,$i,1))=="%")
				{
					$flag="1";

				}
			}
		}
		if($flag=="0")
		{
			for($i=0;$i<=strlen($dtformat)-1;$i++)
			{
				if((ord(substr($dtformat,$i,1))>=65 AND ord(substr($dtformat,$i,1))<=90) OR (ord(substr($dtformat,$i,1))>=97 AND ord(substr($dtformat,$i,1))<=122))
				{
					$newstr.="%".substr($dtformat,$i,1);
				}
				else
				{
					$newstr.=substr($dtformat,$i,1);
				}

			}
			$sql = "UPDATE #__ccinvoices_configuration SET date_format='".$newstr."' WHERE id = 1";
			$db->setQuery($sql);
			$db->query();
		}		
		

		//New ccinvoices_invoices fields

		//this is for change "|" to "|#$|"
		
		if($this->checkFieldExist("#__ccinvoices_invoices","item_id"))
		{
			$keyword1 = "|";
			$query = 'SELECT quantity,pname,price,tax,id FROM #__ccinvoices_invoices where lower(concat_ws(" ",quantity,pname,price,tax)) like "%'.$keyword1.'%"';
			$db->setQuery( $query );
			$affRows = $db->loadObjectList();
			foreach($affRows as $affRow)
			{
				$quantity = str_replace("|", "|#$|", $affRow->quantity);
				$pname = str_replace("|", "|#$|", $affRow->pname);
				$price = str_replace("|", "|#$|", $affRow->price);
				$tax = str_replace("|", "|#$|", $affRow->tax);
				$sql = "UPDATE #__ccinvoices_invoices SET quantity='".$quantity."',pname='".$pname."',price='".$price."',tax='".$tax."' WHERE id =".$affRow->id;
				$db->setQuery($sql);
				$db->query();
			}		
		}	
		
		
		if($this->checkFieldExist("#__ccinvoices_invoices","item_id"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `item_id` MEDIUMTEXT NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}		
		
		if($this->checkFieldExist("#__ccinvoices_invoices","item_description"))
		{
			$query = "ALTER TABLE `#__ccinvoices_invoices` ADD `item_description` MEDIUMTEXT NOT NULL";
			$db->setQuery( $query );
			$db->query();
		}	
		
		
		//Item template in configuration
		if($this->checkFieldExist("#__ccinvoices_configuration","items_template"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `items_template` LONGTEXT NOT NULL";
			$db->setQuery( $query );
			$db->query();

			$PDFTemp='<table style="width: 100%; margin: 0px 0px 0px 0px !important;" border="0" cellspacing="0" cellpadding="0">
					<tbody>
					<tr>
					<td align="left" valign="top" width="10%">{item_quantity}</td>
					<td align="left" valign="top" width="50%">{item_name}</td>
					<td align="left" valign="top" width="10%">{item_price_excl_tax}</td>
					<td align="left" valign="top" width="10%">{item_tax_amount}</td>
					<td align="left" valign="top" width="10%">{item_tax_percentage}</td>
					<td align="left" valign="top" width="10%">{item_total_incl_tax}</td>
					</tr>
					</tbody>
					</table>';			
			$query = "UPDATE `#__ccinvoices_configuration` SET items_template='".$PDFTemp."'";
			$db->setQuery( $query );
			$db->query();		
		}	

			
		
		
		
		
		//PDF settings field creating code
			if($this->checkFieldExist("#__ccinvoices_configuration","invoice_mode"))
			{
				$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `invoice_mode` TINYINT( 2 ) NOT NULL ,
							ADD `invoice_top_margin` VARCHAR( 255 ) NOT NULL ,
							ADD `invoice_bottom_margin` VARCHAR( 255 ) NOT NULL ,
							ADD `invoice_left_margin` VARCHAR( 255 ) NOT NULL ,
							ADD `invoice_right_margin` VARCHAR( 255 ) NOT NULL ,
							ADD `pdffontsize_invoice` VARCHAR( 5 ) NOT NULL ,
							ADD `fontname_invoice` TINYINT( 2 ) NOT NULL";
				$db->setQuery( $query );
				$db->query();

				$query ="UPDATE #__ccinvoices_configuration SET invoice_mode = 0 where id = 1";
				$db->setQuery( $query);
				$db->query();			
				$query ="UPDATE #__ccinvoices_configuration SET invoice_top_margin = '15' where id = 1";
				$db->setQuery( $query);
				$db->query();			
				$query ="UPDATE #__ccinvoices_configuration SET invoice_bottom_margin = '10' where id = 1";
				$db->setQuery( $query);
				$db->query();			
				$query ="UPDATE #__ccinvoices_configuration SET invoice_left_margin = '15' where id = 1";
				$db->setQuery( $query);
				$db->query();			
				$query ="UPDATE #__ccinvoices_configuration SET invoice_right_margin = '15' where id = 1";
				$db->setQuery( $query);
				$db->query();		
				$query = "UPDATE  #__ccinvoices_configuration SET pdffontsize_invoice='8' WHERE id=1";
				$db->setQuery( $query );
				$db->query();		
				$query = "UPDATE  #__ccinvoices_configuration SET fontname_invoice=4 WHERE id=1";
				$db->setQuery( $query );
				$db->query();				
			}	
			
			

		$create_item_table="CREATE TABLE IF NOT EXISTS `#__ccinvoices_items` (
			`id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
			`item_id` VARCHAR( 10 ) NOT NULL ,
			`item_quantity` VARCHAR( 10 ) NOT NULL ,
			`item_name` VARCHAR( 100 ) NOT NULL ,
			`item_description` VARCHAR( 350 ) NOT NULL ,
			`item_price_excl_tax` DOUBLE NOT NULL ,
			`item_tax_percentage` DOUBLE NOT NULL ,
			`ordering` INT( 11 ) NOT NULL,
			PRIMARY KEY ( `id` ) 
			) ;	";		
		$db->setQuery( $create_item_table );
		$db->query();	

		//Drop table #__ccinvoices_payment_options
		$query = "DROP TABLE IF EXISTS `#__ccinvoices_payment_options`";
		$db->setQuery( $query );
		$db->query();	
		
		
		//Background image in PDF fields

		if($this->checkFieldExist("#__ccinvoices_configuration","backgimage"))
		{
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `backgimage` tinyint( 2 ) NOT NULL";
			$db->setQuery( $query );
			$db->query();
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdfimagexpos` VARCHAR(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdfimageypos` VARCHAR(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdfimagewidth` VARCHAR(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdfimageheight` VARCHAR(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();
			$query = "ALTER TABLE `#__ccinvoices_configuration` ADD `pdfimagefile` VARCHAR(255) NOT NULL";
			$db->setQuery( $query );
			$db->query();

			$query = "UPDATE `#__ccinvoices_configuration` SET `backgimage`=0 WHERE id=1";
			$db->setQuery( $query );
			$db->query();			
			$query = "UPDATE `#__ccinvoices_configuration` SET `pdfimagexpos`='0' WHERE id=1";
			$db->setQuery( $query );
			$db->query();			
			$query = "UPDATE `#__ccinvoices_configuration` SET `pdfimageypos`='0' WHERE id=1";
			$db->setQuery( $query );
			$db->query();			
			$query = "UPDATE `#__ccinvoices_configuration` SET `pdfimagewidth`='200' WHERE id=1";
			$db->setQuery( $query );
			$db->query();			
			$query = "UPDATE `#__ccinvoices_configuration` SET `pdfimageheight`='130' WHERE id=1";
			$db->setQuery( $query );
			$db->query();			
		}			
	}
	
	function checkFieldExist($tableName,$fieldName)
	{
		$db	= JFactory::getDBO();
		$query = "SHOW COLUMNS FROM ".$tableName."";
		$db->setQuery( $query );
		$rows=$db->loadObjectList();

		$FieldFlag=true;
		if(count($rows))
		{
			foreach($rows as $fields)
			{
				if(trim($fields->Field)==trim($fieldName))
				{
					$FieldFlag=false;
				}
			}
		}
		return $FieldFlag;
	}		
}
?>
