<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );

require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'controllers'.DS.'invoices.php' );
global $mainframe;
jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
define('JPATH_COMPONENT', JPATH_BASE . '/components/');
jimport('joomla.mail.helper');

$mainframe = JFactory::getApplication('administrator');

$language = JFactory::getLanguage();
$language->load('com_ccinvoices', JPATH_ADMINISTRATOR, 'en-GB', true);
$language->load('com_ccinvoices', JPATH_ADMINISTRATOR, null, true);

$db = JFactory::getDBO();
$invoice_id = JRequest::getVar("inv_id","");
$status = JRequest::getVar("selectedst","");

function invoicenumbercheck()
{
	$db 	=JFactory::getDBO();
	$query	= "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = ''";
    $db->setQuery($query);
    $invoicenumbercheck = $db->loadResult();
    return $invoicenumbercheck;
}
	$query	= "SELECT status FROM #__ccinvoices_invoices WHERE id=".$invoice_id."";
    $db->setQuery($query);
    $pre_status = $db->loadResult();

	if(trim($pre_status)=="4")
	{
		if($status!=$pre_status)
		{
			$sql = "DELETE FROM #__ccinvoices_payment WHERE inv_id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}
	}

$query = "UPDATE #__ccinvoices_invoices SET status=".$status." WHERE id=".$invoice_id."";
$db->setQuery( $query );
$db->query();

		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

		$query = 'SELECT * from #__ccinvoices_invoices where id ='.$invoice_id;
		$db->setQuery($query);
		$row_inv = $db->loadObject();
		$row=$row_inv;


		$new_inv_id1="0";
		if($row->status == 1)
		{
			$new_inv_id1= "0";
			$sql = "UPDATE #__ccinvoices_invoices SET number= ".$new_inv_id1." WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}else if($row->status != 1 && $row->number == 0)
		{
			$query = 'SELECT invoice_start from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
			$db->setQuery($query);
			$confRow = $db->loadObject();
		    $invoicenumbercheck=invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}
			$sql = "UPDATE #__ccinvoices_invoices SET number= ".$new_inv_id1." WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}





		$custom_invoice_number = $row_inv->custom_invoice_number;
		$inv_status = $row_inv->status;
 		$invoice_format = $config->invoice_format;

		if($invoice_format != "" && $inv_status != "1")
		{
			$query = 'SELECT i.number,c.contact_number from #__ccinvoices_invoices AS i'
					. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id '
					. ' where i.id ='.$invoice_id;
			$db->setQuery($query);
			$invRow = $db->loadObject();
			$custom_inv =  sprintf("%04d", $invRow->number);
			$inv_format["invoicenumber"] = $custom_inv;
			$inv_format["contactnumber"] = $invRow->contact_number;
			$inv_format["dy"] = date("z") + 1;
			$inv_format["dd"] = date("d",time());
			$inv_format["mm"] = date( "m",time());
			$inv_format["yy"] = date("y",time());
			$inv_format["yyyy"] = date("Y",time());


			foreach($inv_format as $inv_formats=>$value)
			{
				$find = "[".$inv_formats."]";
				$replace = $value;
				$invoice_format = str_replace($find,$replace,$invoice_format);
			}

			$sql = "UPDATE #__ccinvoices_invoices SET custom_invoice_number= '".$invoice_format."' WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();

		}
		else
		{
			$sql = "UPDATE #__ccinvoices_invoices SET custom_invoice_number= '' WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}

$notify = JRequest::getVar("notify","");
JRequest::setVar("ajaxvarset",'1');
JRequest::setVar("filenm",'1');


if($notify=="Y")
{
	JRequest::setVar("id",$invoice_id);
	if($status=="2")
	{
		ccInvoicesControllerInvoices::sendInvoice();
	}
	if($status=="3")
	{
		ccInvoicesControllerInvoices::invoiceRem();
	}
}


?>