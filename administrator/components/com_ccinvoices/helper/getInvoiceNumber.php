<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,1);
}
if(versionCompare()>=3)
{
	$className='JControllerLegacy';
}
else
{
	jimport('joomla.application.component.controller');
	$className='JController';
}

//Controllers
if(versionCompare()>=3)
{
	class CCINVOISController extends JControllerLegacy{}
}
else
{
	class CCINVOISController extends JController{}
}
require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'controllers'.DS.'invoices.php' );
global $mainframe;
jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
define('JPATH_COMPONENT', JPATH_BASE . '/components/');
jimport('joomla.mail.helper');

$mainframe = JFactory::getApplication('administrator');
$lang = JFactory::getLanguage();
$lang->load('com_ccinvoices',JPATH_BASE.DS."administrator");

$db = JFactory::getDBO();
$invoice_id = JRequest::getVar("inv_id","");


	$query = 'SELECT custom_invoice_number,number from #__ccinvoices_invoices where id ='.$invoice_id;
	$db->setQuery($query);
	$row_inv = $db->loadObject();

	$invno='';
	if($row_inv->custom_invoice_number != '' AND $row_inv->custom_invoice_number != '0')
	{
		$invno= $row_inv->custom_invoice_number;
	}else
	{
		$invno=$row_inv->number;
	}
	echo $invno;

?>