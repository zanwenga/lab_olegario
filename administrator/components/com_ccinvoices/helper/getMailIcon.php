<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'controllers'.DS.'invoices.php' );
global $mainframe;
jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
define('JPATH_COMPONENT', JPATH_BASE . '/components/');
jimport('joomla.mail.helper');

$mainframe = JFactory::getApplication('administrator');
$lang = JFactory::getLanguage();
$lang->load('com_ccinvoices',JPATH_BASE.DS."administrator");

$db = JFactory::getDBO();
$invoice_id = JRequest::getVar("inv_id","");


$query = 'SELECT * from #__ccinvoices_invoices where id ='.$invoice_id;
$db->setQuery($query);
$row_inv = $db->loadObject();
$row=$row_inv;
//helper//components/com_ccinvoices/
 $lnk=JURI::base();
$lnk=str_replace("helper/","",$lnk);
?>

<?php if($row->status == "1") { ?>
	<div ><img title="<?php echo JText::_('CC_TITLEDRAFT'); ?>" src="<?php echo $lnk;?>assets/images/send-inactive.png" ></div>
<?php }else if($row->status == "2") {  ?>
	<?php if($row->invoice_sent_date == "0000-00-00") { ?>
		<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=sendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo $lnk;?>assets/images/send.png" title="<?php echo JText::_("CC_SEND_INVOICE"); ?>"></a></div>
	<?php }else { ?>
		<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=reSendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo $lnk;?>assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
	<?php }?>
<?php }else if($row->status == "3") {  ?>
	<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=invoiceRem&filenm=1&id=<?php echo $row->id; ?>"><img src="<?php echo $lnk;?>assets/images/reminder.png" title="<?php echo JText::_("CC_REMINDER_INVOICE"); ?>"></a></div>
<?php } else if($row->status == "4") {  ?>
	<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&controller=invoices&task=reSendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo $lnk;?>assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
<?php }	?>

