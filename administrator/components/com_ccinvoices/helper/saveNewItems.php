<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );
require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'models'.DS.'items.php' );

global $mainframe;
jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
define('JPATH_COMPONENT', JPATH_BASE . '/components/');
jimport('joomla.mail.helper');

$mainframe = JFactory::getApplication('administrator');
$lang = JFactory::getLanguage();
$lang->load('com_ccinvoices',JPATH_BASE.DS."administrator");

$db = JFactory::getDBO();
$itemid = JRequest::getVar("itemid","");
$itemqty = JRequest::getVar("itemqty","");
$itemname = JRequest::getVar("itemname","");
$itemdesc = JRequest::getVar("itemdesc","");
$itemprice = JRequest::getVar("itemprice","");
$itemtax = JRequest::getVar("itemtax","");

jimport('joomla.filter.input');

class Tableitems extends JTable
{
	var $id = null;
	var $item_id = null;
	var $item_quantity = null;
	var $item_name = null;
	var $item_description = null;
	var $item_price_excl_tax = null;
	var $item_tax_percentage = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_items', 'id', $db);
	}

	function check()
	{
		return true;
	}
}

JRequest::setVar('item_id',$itemid,'POST');
JRequest::setVar('item_quantity',$itemqty,'POST');
JRequest::setVar('item_name',$itemname,'POST');
JRequest::setVar('item_description',$itemdesc,'POST');
JRequest::setVar('item_price_excl_tax',$itemprice,'POST');
JRequest::setVar('item_tax_percentage',$itemtax,'POST');

echo ccInvoicesModelitems::store();
?>