<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
define( '_JEXEC', 1 );
chdir("../../../../");
getcwd();
define('JPATH_BASE', getcwd() );
define('DS', DIRECTORY_SEPARATOR);
require_once( JPATH_BASE .DS.'includes'.DS.'defines.php' );
require_once( JPATH_BASE .DS.'includes'.DS.'framework.php' );

jimport("joomla.user.helper");
jimport("joomla.utilities.utility");
define('JPATH_COMPONENT', JPATH_BASE . '/components/');
$mainframe = JFactory::getApplication('administrator');

$lang = JFactory::getLanguage();
$lang->load('com_ccinvoices',JPATH_BASE.DS."administrator");

$my = JFactory::getUser();
$db = JFactory::getDBO();
$user_id = JRequest::getInt("user_id","0");
$contact_id = JRequest::getInt("contact_id","0");
$task = JRequest::getVar("task","");
$filter = JRequest::getVar("filter","");
$path_new = JURI::root();
$path = str_replace("/components/com_ccinvoices/helper/", "", $path_new);

$where = array();
$sql1 = '';

if($my->id == "0")
{
	exit;
}

if($filter != '')
{
	$where[] = " name LIKE  '%".$filter."' OR username LIKE '%".$filter."%' OR email LIKE '%".$filter."%'";
}
$where		= count( $where ) ? ' WHERE ' . implode( ' AND ', $where ) : '';
if($task == "addUser")
{
	$sql = "INSERT INTO #__ccinvoices_users (`user_id`,`contact_id`) values ('".$user_id."','".$contact_id."')";
	$db->setQuery($sql);
	$db->query();

	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "delUser")
{
	$sql = "DELETE FROM #__ccinvoices_users where user_id = '".$user_id."' AND contact_id = '".$contact_id."'";
	$db->setQuery($sql);
	$db->query();

	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "")
{
	$sql = "SELECT * FROM #__users "
			. $where
			." ORDER BY  registerDate DESC LIMIT 10";
	$db->setQuery($sql);
	$rows = $db->loadObjectList();
}else if($task == "createUser")
{


	$usergroup = JRequest::getInt("usergroup","");
	$name =  JRequest::getVar("name","");
	$username = JRequest::getVar("username","");
	$email = JRequest::getVar("email","");
	$joomlaversion=JRequest::getVar("jversion","");
	$password_raw = genRandomSecretcode();
	$salt = JUserHelper::genRandomPassword(32);
    $crypt = JUserHelper::getCryptedPassword($password_raw, $salt);
    $registerDate = date('Y-m-d H:M:S');
    $password = $crypt.':'.$salt;

	$block = '0';
	$gid = JRequest::getInt("usergroup","");
	$section_value = 'users';
	$order_value = '0';
	$hidden = '0';
	$msg = '';

	$query = 'SELECT count(*)  FROM #__users where username = "'.$username.'" AND block=1 LIMIT 1';
	$db->setQuery( $query );
	$count_user = $db->loadResult();



	if(trim($email)!="")
	{
	if($username != '')
	{
		if($count_user == 0)
	{

		$query = 'INSERT INTO #__users ( name, username, email , password ,  block, registerDate)' .
				' VALUES ( "'.$name.'", "'.$username.'" ,"'.$email.'" ,"'.$password.'" ,"'.$block.'" ,"'.$registerDate.'" )'
			;
		$db->setQuery( $query );
		if(!$db->query()) {
			JError::raiseError( 500, $db->stderror() );
		}
		$user_id = $db->insertid();

		$query = 'INSERT INTO #__user_usergroup_map ( user_id, group_id)' .
				' VALUES ( '.$user_id.', '.$gid.' )'
			;

		$db->setQuery( $query );
		if(!$db->query()) {
			JError::raiseError( 500, $db->stderror() );
		}
		$sql = "INSERT INTO #__ccinvoices_users (`user_id`,`contact_id`) values ('".$user_id."','".$contact_id."')";
		$db->setQuery($sql);
		$db->query();


		$sql = "SELECT * FROM #__users "
				. $where
				." ORDER BY  registerDate DESC LIMIT 10";
		$db->setQuery($sql);
		$rows = $db->loadObjectList();

		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
		if($config->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC = $config->email_cc;
		}
		if($config->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC = $config->email_bcc;
		}
		$mailfrom 		= $mainframe->getCfg( 'mailfrom' );
		$fromname 		= $mainframe->getCfg( 'fromname' );
		$sitename 		= $mainframe->getCfg( 'sitename' );
		$siteURL		= JURI::root();


//start new user mail code
		$sql = "SELECT invoice_template FROM #__ccinvoices_templates WHERE id = 6  LIMIT 1";
		$db->setQuery($sql);
		$mailbody = $db->loadResult();
		$subject = $config->newusermail_sub;

		$sql = "SELECT * FROM #__ccinvoices_contacts WHERE id = ".$contact_id."  LIMIT 1";
		$db->setQuery($sql);
		$contact_rows = $db->loadObject();

		$outputArr["contact"]=$name;
		$outputArr["sitename"] = "<a href='".$path."' target='_blank'>".$sitename."</a>";
		$outputArr["contact_name"]= $contact_rows->name;
		$outputArr["contact_username"] =  $username;
		$outputArr["password"] = $password_raw;
		$outputArr["user_name"] = $config->user_name;
		$outputArr["company_name"] = $config->user_company;
		//subject
		foreach($outputArr as $outputArrs=>$value)
		{
			$find = "{".$outputArrs."}";
			$replace = $value;
			$subject = str_replace($find,$replace,$subject);
		}
		$subject=strip_tags($subject);
		//body
		foreach($outputArr as $outputArrs=>$value)
		{
			$find = "{".$outputArrs."}";
			$replace = $value;
			$mailbody = str_replace($find,$replace,$mailbody);
		}

		$subject 	= html_entity_decode($subject, ENT_QUOTES);
		$mailbody 	= html_entity_decode($mailbody, ENT_QUOTES);

		JFactory::getMailer()->sendMail($mailfrom, $fromname, $email, $subject, $mailbody ,1,$EmailCC,$EmailBCC);
//======================

	}
}
}
}
function versionCompareUsers()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,1);
}
function genRandomSecretcode($length = 6)
{
	$salt = "0123456789";
	$len = strlen($salt);
	$makepass = '';
	$stat = @stat(__FILE__);
	if(empty($stat) || !is_array($stat)) $stat = array(php_uname());
	mt_srand(crc32(microtime() . implode('|', $stat)));
	for ($i = 0; $i < $length; $i ++) {
		$makepass .= $salt[mt_rand(0, $len -1)];
	}
	return $makepass;
}

$sql = "SELECT u.id,u.name,u.username,u.email FROM #__users AS u"
		. " LEFT JOIN #__ccinvoices_users AS iu ON iu.user_id = u.id "
		. " WHERE iu.contact_id = ".$contact_id
		;
$db->setQuery($sql);
$userAssined = $db->loadObjectList();

ob_start();
if(versionCompareUsers()>=3)
{
	require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'helper'.DS.'userlist.php' );
}
else
{
	require_once( JPATH_BASE .DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'helper'.DS.'userlist25.php' );
}
?>