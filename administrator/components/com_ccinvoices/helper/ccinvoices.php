<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// no direct access
defined('_JEXEC') or die();
jimport('joomla.access.access');

 abstract class ccinvoicesHelper
 {

	public static function addSubmenu($submenu)
	{
		$view=JRequest::getCmd('view', 'invoices');
		if(versionCompare()>=3)
		{
			$tabbingName="JHtmlSidebar";
			JHtmlSidebar::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices&view=invoices', $submenu == 'invoices');
			JHtmlSidebar::addEntry(JText::_('CC_INVOICES_ITEMS'), 'index.php?option=com_ccinvoices&view=items', $submenu == 'items');
			JHtmlSidebar::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&view=contacts', $submenu == 'contacts');
			JHtmlSidebar::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&view=templates', $submenu == 'templates');
			JHtmlSidebar::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&view=configuration&layout=form', $submenu == 'configuration');
		}
		else
		{
			$tabbingName="JSubMenuHelper";
			JSubMenuHelper::addEntry(JText::_('CC_INVOICES_INVOICES'), 'index.php?option=com_ccinvoices&view=invoices', $submenu == 'invoices');
			JSubMenuHelper::addEntry(JText::_('CC_INVOICES_ITEMS'), 'index.php?option=com_ccinvoices&view=items', $submenu == 'items');
			JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONTACTS'), 'index.php?option=com_ccinvoices&view=contacts', $submenu == 'contacts');
			JSubMenuHelper::addEntry(JText::_('CC_INVOICES_TEMPLATES'), 'index.php?option=com_ccinvoices&view=templates', $submenu == 'templates');
			JSubMenuHelper::addEntry(JText::_('CC_INVOICES_CONFIGURATION'), 'index.php?option=com_ccinvoices&view=configuration&layout=form', $submenu == 'configuration');
		}
		//
		//JSubMenuHelper


		// set some global property
		$document = JFactory::getDocument();

		if ($submenu == 'categories')
		{
			$document->setTitle(JText::_('JE_ADMINISTRATION_CATEGORIES'));
		}
	}
	/**
	 * Get the actions
	 */

	public static function getActions()
	{

		 $user	= JFactory::getUser();
		 $result	= new JObject;

		if (empty($messageId)) {
			$assetName = 'com_ccinvoices';
		}
		else {
			$assetName = 'com_ccinvoices.message.'.(int) $messageId;
		}

		$actions = array('core.admin','core.manage','ccinvoices.create','ccinvoices.edit','ccinvoices.edit.state','ccinvoices.delete','ccinvoices.copy','ccinvoices.config','ccinvoices.view.invoices','ccinvoices.view.contacts','ccinvoices.view.ccitems','ccinvoices.view.templates','ccinvoices.view.configurations');

		foreach ($actions as $action) {
			$result->set($action,	$user->authorise($action, $assetName));
		}

		return $result;



	}
 }
?>
