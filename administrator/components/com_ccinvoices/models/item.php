<?php
/**
* @package	ccHelpdesk
* @version 1.0.0
* @author          Chill Creations <info@chillcreations.com>
* @link              http://www.chillcreations.com

* @copyright	Copyright (C) 2012 Chill Creations
* @license	        GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License.
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
**/
defined('_JEXEC') or die('DIRECT ACCESS NOT ALLOWED');

jimport('joomla.application.component.modeladmin');

class ccinvoicesModelitem extends JModelAdmin
{

	protected function allowEdit($data = array(), $key = 'id')
	{
		// Check specific edit permission then general edit permission.
		return JFactory::getUser()->authorise('ccinvoices.edit', 'com_ccinvoices.field.'.((int) isset($data[$key]) ? $data[$key] : 0)) or parent::allowEdit($data, $key);
	}
	public function getTable($type = 'items', $prefix = 'Table', $config = array())
	{

		return JTable::getInstance($type, $prefix, $config);
	}
	public function getScript()
	{
		//return 'administrator/components/com_helloworld/models/forms/student.js';
	}
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_ccinvoices.field', 'field', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		return $form;
	}
	protected function loadFormData()
	{

		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_ccinvoices.edit.field.data', array());

		if (empty($data))
		{
			$data = $this->getItem();
		}
		return $data;
	}

}

?>


