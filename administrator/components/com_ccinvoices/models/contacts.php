<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.modellist');

class ccInvoicesModelContacts extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'c.id',
				'name', 'c.name',
				'contact', 'c.contact',
				'contact_number', 'c.contact_number',
				'address', 'c.address',
				'email', 'c.email',
				'tax_id', 'c.tax_id',
				'ordering', 'c.ordering',
			);
		}

		parent::__construct($config);
	}


	protected function getListQuery()
	{
		$search	= $this->getState('filter.search');
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		// Select the required fields from the table.

		$query->select(
			$this->getState(
				'list.select',
				'c.id, c.name, c.contact, c.contact_number, c.address, c.email' .
				', c.tax_id, c.ordering'
			)
		);


		$query->from($db->quoteName('#__ccinvoices_contacts').' AS c');

		if (!empty($search))
		{
			$query->where("c.name LIKE '%$search%' OR LOWER(c.contact) LIKE '%".$search."%' OR email LIKE '%".$search."%'");
		}


		$orderCol	= $this->state->get('list.ordering', 'c.ordering');
		$orderDirn	= $this->state->get('list.direction', 'asc');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.'.$layout;
		}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		// List state informatioc.
		parent::populateState('c.ordering', 'asc');
	}

	function getContacts()
	{
		$cid = JRequest::getVar('cid');
		if(JRequest::getInt('id'))
		{
			$cid 		= JRequest::getVar( 'cid', array(JRequest::getInt('id')), '', 'array' );
		}
		if(is_array($cid)) $cid = intval($cid[0]);
		$row=  JTable::getInstance('contacts','Table');
		$row->load($cid);
		return $row;
	}

   	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
	// save
	function store()
	{
		$db			= JFactory::getDBO();
		$row = $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$tsk=$post['task'];
		$tsk=str_replace("contacts.","",$tsk);
		if($tsk=='save2copy')
		{
			$post['contact_id']=0;
		}
		if (!$row->bind($post)) {
			$this->setError($row->getError());
			return 0;
		}
		$row->id = $post["contact_id"];
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row = $this->getTable();
		$db 	= JFactory::getDBO();
		$cannotdelcontactmsg=0;
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
			    $query	= "SELECT count(*) FROM #__ccinvoices_invoices where contact_id =".$cid."  LIMIT 1";
			    $db->setQuery($query);
			    $rows=$db->loadResult();
			    if($rows<=0)
			    {
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
			    else
			    {
					$cannotdelcontactmsg="1";
			    }
			}
		}
		return array(true,$cannotdelcontactmsg);
	}
	function getLastContactId()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT contact_number FROM #__ccinvoices_contacts order by id desc";
        $db->setQuery($query);
        $row = $db->loadObject();
        if(count($row))
        	return $row->contact_number;
	}
}
?>