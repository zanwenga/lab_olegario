<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.modellist');

class ccInvoicesModelTemplates extends JModelList
{

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;
	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function _buildQuery()
	{
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();

		$filter				= JRequest::getVar('invoice_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccinvoices.templates.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccinvoices.templates.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccinvoices.templates.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccinvoices.templates.sortOrder',$sortOrder);



		//$this->_query="SELECT s.*,b.name AS name,b.contact AS contact " .
		//		"FROM #__ccinvoices_invoices AS s LEFT JOIN #__ccinvoices_contacts " .
		//		"AS b ON b.id = s.contact_id WHERE custom_invoice_number " .
		//		"LIKE '%".$db->getEscaped($filter)."%' ORDER BY $sortColumn $sortOrder";
	}
	public function getTable($type = 'templates', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	function __construct()
	{
		parent::__construct();

		$this->_buildQuery();
		$mainframe = JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('com_ccinvoices.templates.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_ccinvoices.templates.limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('com_ccinvoices.templates.limit', $limit);
		$this->setState('com_ccinvoices.templates.limitstart', $limitstart);
	}
	function getData()
	{
		if (empty($this->_data))
			$this->_data=$this->_getList($this->_query,$this->getState('com_ccinvoices.templates.limitstart'), $this->getState('com_ccinvoices.templates.limit'));
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getListCount($this->_query);
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccinvoices.templates.limitstart'), $this->getState('com_ccinvoices.templates.limit'));
		}
		return $this->_pagination;
	}
	function getInvoices()
	{
		$cid = JRequest::getVar('cid');
		if(is_array($cid)) $cid = intval($cid[0]);
		$row=  JTable::getInstance('templates','Table');
		$row->load($cid);
		return $row;
	}

	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	// save

	function store()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$row = $this->getTable();
		// Bind the form fields
		$post			= JRequest::get('post', JREQUEST_ALLOWRAW);
		$selectedid = JRequest::getInt( 'selected_id', '0', 'post', '' );

		//new code to save conf. data

		if($selectedid=="1")
		{
			$query = "UPDATE #__ccinvoices_configuration SET items_template=".$db->Quote($post['items_template'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$this->save_pdfsettings($post);
		}
		elseif($selectedid=="3")
		{
			$query = "UPDATE #__ccinvoices_configuration SET default_note=".$db->Quote($post['default_note'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

		}
		elseif($selectedid=="4")
		{
			$query = "UPDATE #__ccinvoices_configuration SET default_email_sub=".$db->Quote($post['default_email_sub']).",default_email=".$db->Quote($post['default_email'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

		}
		elseif($selectedid=="5")
		{
			$query = "UPDATE #__ccinvoices_configuration SET rem_email_sub=".$db->Quote($post['rem_email_sub']).",default_email_rem=".$db->Quote($post['default_email_rem'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
		}
		elseif($selectedid=="6")
		{
			$query = "UPDATE #__ccinvoices_configuration SET newusermail_sub=".$db->Quote($post['newusermail_sub'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
		}


		if (!$row->bind($post)) {
			return 0;
		}
		// Make sure data is valid
		if (!$row->check()) {
			return 0;
		}
		if (!$row->store())	{
			return 0;
		}
		return $row->id;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row = $this->getTable();
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}
	function save_pdfsettings($post)
	{
		$db = JFactory::getDBO();
		$cid=$post['id'];
		if($cid=="1")
		{

			$query = "UPDATE #__ccinvoices_configuration SET invoice_mode=".$db->Quote($post['invoice_mode'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET invoice_top_margin=".$db->Quote($post['invoice_top_margin'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET invoice_bottom_margin=".$db->Quote($post['invoice_bottom_margin'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET invoice_left_margin=".$db->Quote($post['invoice_left_margin'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET invoice_right_margin=".$db->Quote($post['invoice_right_margin'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

			$query = "UPDATE #__ccinvoices_configuration SET pdffontsize_invoice=".$db->Quote($post['pdffontsize_invoice'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET fontname_invoice=".$db->Quote($post['fontname_invoice'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

			$query = "UPDATE #__ccinvoices_configuration SET pdf_layout=".$db->Quote($post['pdf_layout'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}
			$query = "UPDATE #__ccinvoices_configuration SET backgimage=".$db->Quote($post['backgimage'])." WHERE id=1";
			$db->setQuery( $query );
			if(!$db->query()) {
				JError::raiseError( 500, $db->stderror() );
			}

			if($post['backgimage']=="1")
			{
				$query = "UPDATE #__ccinvoices_configuration SET pdfimagexpos=".$db->Quote($post['pdfimagexpos'])." WHERE id=1";
				$db->setQuery( $query );
				if(!$db->query()) {
					JError::raiseError( 500, $db->stderror() );
				}
				$query = "UPDATE #__ccinvoices_configuration SET pdfimageypos=".$db->Quote($post['pdfimageypos'])." WHERE id=1";
				$db->setQuery( $query );
				if(!$db->query()) {
					JError::raiseError( 500, $db->stderror() );
				}
				$query = "UPDATE #__ccinvoices_configuration SET pdfimagewidth=".$db->Quote($post['pdfimagewidth'])." WHERE id=1";
				$db->setQuery( $query );
				if(!$db->query()) {
					JError::raiseError( 500, $db->stderror() );
				}
				$query = "UPDATE #__ccinvoices_configuration SET pdfimageheight=".$db->Quote($post['pdfimageheight'])." WHERE id=1";
				$db->setQuery( $query );
				if(!$db->query()) {
					JError::raiseError( 500, $db->stderror() );
				}
				$logo = JRequest::getVar('pdfimagefile', null, 'files', 'array' );
				$img_path =	JPATH_ROOT.DS."media".DS."com_ccinvoices".DS."logo".DS;
				if($logo['name'] != '')
				{
					jimport('joomla.filesystem.file');
					$tmp_file_name = JFile::makeSafe(time().".".JFile::getExt($logo['name']));
					if (!JFile::upload($logo['tmp_name'], $img_path.$tmp_file_name))
					{
						return 0;
					}else
					{
						$row->logo = $tmp_file_name;
					}
					$query = "UPDATE #__ccinvoices_configuration SET pdfimagefile=".$db->Quote($tmp_file_name)." WHERE id=1";
					$db->setQuery( $query );
					if(!$db->query()) {
						JError::raiseError( 500, $db->stderror() );
					}
				}
			}
		}



	}
}
?>