<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

// Import Joomla! libraries
jimport('joomla.application.component.modellist');

class ccInvoicesModelitems extends JModelList
{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 'it.id',
				'name', 'it.item_id',
				'contact', 'it.item_quantity',
				'contact_number', 'it.item_name',
				'address', 'it.item_description',
				'email', 'it.item_price_excl_tax',
				'tax_id', 'it.item_tax_percentage',
				'ordering', 'it.ordering',
			);
		}

		parent::__construct($config);
	}


	protected function getListQuery()
	{
		$search	= $this->getState('filter.search');
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		// Select the required fields from the table.

		$query->select(
			$this->getState(
				'list.select',
				'it.id, it.item_id, it.item_quantity, it.item_name, it.item_description, it.item_price_excl_tax' .
				', it.item_tax_percentage, it.ordering'
			)
		);


		$query->from($db->quoteName('#__ccinvoices_items').' AS it');

		if (!empty($search))
		{
			$query->where("it.item_name LIKE '%$search%' OR it.item_id LIKE '%".$search."%' OR it.item_description LIKE '%".$search."%'");
		}


		$orderCol	= $this->state->get('list.ordering', 'it.ordering');
		$orderDirn	= $this->state->get('list.direction', 'asc');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.'.$layout;
		}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		// List state informatioc.
		parent::populateState('it.ordering', 'asc');
	}
	function getsingleItems()
	{
		$cid = JRequest::getVar('cid');
		if(JRequest::getInt('id'))
		{
			$cid 		= JRequest::getVar( 'cid', array(JRequest::getInt('id')), '', 'array' );
		}
		if(is_array($cid)) $cid = intval($cid[0]);
		$row=  JTable::getInstance('items','Table');
		$row->load($cid);
		return $row;
	}
   	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
	// save
	function store()
	{
		$db			= JFactory::getDBO();
		$row = ccInvoicesModelitems::getTable();

		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );

		require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
		$confModel =new ccInvoicesModelConfiguration();
		$confModel->setId(1);
		$confRow = $confModel->getData();
		if($confRow->cformat=="1")
		{
			$post['item_price_excl_tax']=str_replace(",",".",$post['item_price_excl_tax']);
			$post['item_tax_percentage']=str_replace(",",".",$post['item_tax_percentage']);
		}
		$tsk=$post['task'];
		$tsk=str_replace("items.","",$tsk);
		if($tsk=='save2copy')
		{
			$post['id']=0;
		}
		if (!$row->bind($post)) {
			$this->setError($row->getError());
			return 0;
		}

		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}
	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row = $this->getTable();
		$db 	= JFactory::getDBO();
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}
	function getLastItemId()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT item_id FROM #__ccinvoices_items order by id desc";
        $db->setQuery($query);
        $row = $db->loadObject();
        if(count($row))
        	return $row->item_id;
	}
	public function saveorder($idArray = null, $lft_array = null)
	{
		// Get an instance of the table object.
		$table = ccInvoicesModelitems::getTable();

		if (!$table->saveorder($idArray, $lft_array))
		{
			$this->setError($table->getError());
			return false;
		}

		// Clear the cache
		$this->cleanCache();

		return true;
	}
}
?>