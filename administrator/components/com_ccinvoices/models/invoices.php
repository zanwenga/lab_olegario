<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
jimport('joomla.application.component.modellist');


class ccInvoicesModelInvoices extends JModelList
{

	public function __construct($config = array())
	{
		if (empty($config['filter_fields'])) {
			$config['filter_fields'] = array(
				'id', 's.id',
				'number', 's.number',
				'name', 's.name',
				'invoice_date', 's.invoice_date',
				'total', 's.total',
				'status', 's.status',
				'name', 'b.name',
				'contact', 'b.contact',
				'ordering', 's.ordering',
			);
		}

		parent::__construct($config);
	}
	public function saveorder($idArray = null, $lft_array = null)
	{
		// Get an instance of the table object.
		$table = $this->getTable();

		if (!$table->saveorder($idArray, $lft_array))
		{
			$this->setError($table->getError());
			return false;
		}

		// Clear the cache
		$this->cleanCache();

		return true;
	}

	protected function getListQuery()
	{
		$mainframe = JFactory::getApplication();
		$filter	= $this->getState('filter.search');
		$sts	= $this->getState('filter.status1');

		 $clientid = JRequest::getInt("clientid","");

		$db = $this->getDbo();
		$query = $db->getQuery(true);

		$where = "";
		$where.= '(custom_invoice_number LIKE "%'.$filter.'%" ' .
				'OR LOWER(b.name) LIKE '.$db->Quote( '%'.$filter.'%', false ).'' .
				'OR LOWER(b.contact) LIKE '.$db->Quote( '%'.$filter.'%', false ).'' .
				'OR LOWER(s.number) LIKE '.$db->Quote( '%'.$filter.'%', false ).')';

		if($clientid !="")
		{
			$where.= "AND s.contact_id=".$clientid;
		}

		if(($sts>-1 AND $sts!=""))
		{
			$where.= " AND s.status=".$sts;
		}

		// Create a new query object.



		$query->select(
			$this->getState(
				'list.select',
				's.*'
			)
		);

		$query->from($db->quoteName('#__ccinvoices_invoices').' AS s');

		$query->select('b.name AS name,b.contact AS contact');
		$query->join( 'LEFT', '`#__ccinvoices_contacts` AS b ON b.id = s.contact_id' );

		if (!empty($filter) OR $clientid !="" OR ($sts>-1 AND $sts!=""))
		{
			$query->where($where);
		}

		$orderCol	= $this->state->get('list.ordering', 's.id');
		$orderDirn	= $this->state->get('list.direction', 'desc');
		$query->order($db->escape($orderCol.' '.$orderDirn));
		return $query;
	}
	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.'.$layout;
		}

		$search = $this->getUserStateFromRequest($this->context.'.filter.search', 'filter_search');
		$this->setState('filter.search', $search);

		$search = $this->getUserStateFromRequest($this->context.'.filter.status1', 'status1');
		$this->setState('filter.status1', $search);

		// List state information.
		parent::populateState('s.id', 'desc');
	}


	public function getTable($type = 'invoices', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	function gettemplatelayout($id)
	{
			$path=JPATH_SITE.DS."administrator".DS."components".DS."com_ccinvoices".DS."controllers".DS.'invoices.php';
			require_once($path);
			return $template=ccInvoicesControllerInvoices::gettemplatelayout($id);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}

	function getInvoices()
	{
		$cid = JRequest::getVar('cid');
		if(JRequest::getInt('id'))
		{
			$cid 		= JRequest::getVar( 'cid', array(JRequest::getInt('id')), '', 'array' );
		}
		if(is_array($cid)) $cid = intval($cid[0]);
		$row=  JTable::getInstance('invoices','Table');
		$row->load($cid);
		return $row;
	}

    function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

    function contactsearch($contactname)
    {
		$db 	=JFactory::getDBO();
		$query	= "SELECT id,name FROM #__ccinvoices_contacts WHERE  name like'%$contactname%'";
        $db->setQuery($query);
        $contactsearch = $db->loadObjectlist();
        return $contactsearch;
    }
	function getLastContactId()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT contact_number FROM #__ccinvoices_contacts order by id desc";
        $db->setQuery($query);
        $row = $db->loadObject();
        if(count($row))
        	return $row->contact_number;
	}
    function getinvoicecontact($contactid)
    {
    	$db 	= JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_contacts WHERE  id='$contactid'";
        $db->setQuery($query);
        $value = $db->loadObject();
        return $value;
    }

    function updateinvoicecontact()
    {
		$post['id']=JRequest::getInt('cc_id');
		$post['address']=JRequest::getVar('cc_address', '', 'get', 'string', JREQUEST_ALLOWRAW);
		$post['address'] = str_replace("|", "\n",$post['address']);
		$post['contact']=JRequest::getVar('cc_contact');
		$post['email']=JRequest::getVar('cc_email');
		$post['name']=JRequest::getVar('cc_name');
		$post['tax_id']=JRequest::getVar('tax_id');
		$post['contact_number']=JRequest::getVar('contact_number');
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$row = JTable::getInstance('contacts', 'Table');
		// exit;
		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		//echo $row->id;
		echo $row->id;
    }

	function invoicetax()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT value FROM #__tax";
	    $db->setQuery($query);
		$value[] = JHTML::_('select.option',  '0', '- '. JText::_( 'CC_SELECT_STATE' ) .' -' );
		$value = array_merge($value, $db->loadObjectList());
        return $value;
	}

    function store()
	{
		$db			= JFactory::getDBO();
        $resend	= JRequest::getVar( 'resend','');
        $send	= JRequest::getVar( 'send','');
        $reminder	= JRequest::getVar( 'reminder','');
        $id	= JRequest::getInt( 'id');
		$row = $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$tsk=$post['task'];
		$tsk=str_replace("invoices.","",$tsk);
		if($tsk=='save2copy')
		{
			$post['id']=0;
			$post['number']=0;
		}
		$invoicenumber=$post['number'];
		$previousinvoiceno=$post['numbercheck'];
		if($previousinvoiceno == $invoicenumber )
		{
            $post['numbercheck'] =0;
		}else
		{
			 $post['numbercheck'] =$previousinvoiceno;
		}
		if($id =='0' && $send !='' )
		{
               $post['invoice_sent_date']=date("Y-m-d");
               $post['status']='2';
               $post['communication']='1';
		}else if($reminder)
		{
			$post['communication']='3';
		}else
		{
			$post['communication']=$post['communication'];
		}
		$contact_id = $this->savecontactus();

		$item_id=$post['item_id'];
	    $quantity=$post['quantity'];
		$pname=$post['pname'];
		$item_description=$post['item_description'];
		$amount=$post['price'];
		$tax=$post['tax'];
		$itemid='';
		$quant='';
		$pnames='';
		$itemdesc='';
		$amounts='';
	    $taxes='';
		for($i=0;$i<count($quantity);$i++)
		{
			$itemid.=$item_id[$i];
			$quant.= $quantity[$i];
			$pnames.= $pname[$i];
			$itemdesc.=$item_description[$i];
			$amounts.= $amount[$i];
			$taxes.= $tax[$i];
			if($i != (count($quantity)-1))
			{
				$itemid.='|#$|';
				$quant .='|#$|';
				$pnames .='|#$|';
				$itemdesc .='|#$|';
				$amounts .='|#$|';
				$taxes .='|#$|';
			}
		}
		$post['item_id'] =$itemid;
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['item_description'] =$itemdesc;

		require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
		$confModel =new ccInvoicesModelConfiguration();
		$confModel->setId(1);
		$confRow = $confModel->getData();
		if($confRow->cformat=="1")
		{
			$post['price']=str_replace(",",".",$amounts);
			$post['tax'] =str_replace(",",".",$taxes);
			$post['subtotal']=str_replace(",",".",$post['subtotal']);
			$post['discount']=str_replace(",",".",$post['discount']);
			$post['totaltax']=str_replace(",",".",$post['totaltax']);
			$post['total']=str_replace(",",".",$post['total']);
			$post['quantity'] =str_replace(",",".",$post['quantity']);;
		}
		else
		{
			$post['price'] =$amounts;
			$post['tax'] =$taxes;
		}



		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$config = $db->loadObject();

		if (!$row->bind($post)) {
			$this->setError($row->getError());
			return 0;
		}


		if($post['id'])
		{
			$query	= "SELECT status FROM #__ccinvoices_invoices WHERE id=".$post['id']."";
		    $db->setQuery($query);
		    $pre_status = $db->loadResult();

			if(trim($pre_status)=="4")
			{

				if($post['status']!=$pre_status)
				{
					$sql = "DELETE FROM #__ccinvoices_payment WHERE inv_id=".$post['id'];
					$db->setQuery($sql);
					$db->query();
				}
			}
		}

		if($row->status != 1)
		    if(trim($config->invoice_format)=="")
				$row->custom_invoice_number="";
		if($row->status == 1)
		{
			$row->number = "";
		}else if($row->status != 1 && $row->number == 0)
		{
			$query = 'SELECT invoice_start from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
			$db->setQuery($query);
			$confRow = $db->loadObject();
		    $invoicenumbercheck=$this->invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}
			$row->number = $new_inv_id1;
		}
	//	$row->custom_invoice_number = $invoice_format;
		$row->contact_id = $contact_id;
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}

		// $insertid=$db->insertid();
		return $row->id;
	}

	function savecontactus()
	{
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$row = JTable::getInstance('contacts', 'Table');
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$post['id']=$post['contact_id'];

		// $post['invoiceid']=$invoiceid;
		// exit;
		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}

	function delete()
	{
		$cids = JRequest::getVar( 'cid', array(0), 'post', 'array' );
		$row = $this->getTable();
		JTable::addIncludePath(JPATH_ADMINISTRATOR.DS.'components'.DS.'com_ccinvoices'.DS.'tables');
		$contctrow = JTable::getInstance('contacts', 'Table');
		if (count( $cids ))
		{
			foreach($cids as $cid)
			{
				if (!$row->delete( $cid ))
				{
					return false;
				}
			}
		}
		return true;
	}


	function contactinvoice($clientid)
	{
      	$db 	= JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_contacts WHERE  id='$clientid'";
        $db->setQuery($query);
        $contactinvoice = $db->loadObject();
        return $contactinvoice;
	}

	function invoicenumbercheck()
	{
   		$db 	= JFactory::getDBO();
		$query	= "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = ''";
        $db->setQuery($query);
        $invoicenumbercheck = $db->loadResult();
        return $invoicenumbercheck;
   }

	function invoicenumbercheck1($invoicenumbercheck)
	{
   		$y=$invoicenumbercheck;
   	    for ($i = $y; $i <= $y; $i++)
   	    {
	        $db 	= JFactory::getDBO();
			$query	= "SELECT number FROM #__ccinvoices_invoices where number='$y'";
	        $db->setQuery($query);
	        $invoicenumbercheck1 = $db->loadResult();
	   	    if($invoicenumbercheck1)
	   	    {
	   	    	$y++;
	   	    }else
	   	    {
				return $y;
	   	    	break;
	   	    }
	   }
	}

	function lastinvoicenumber()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT * FROM #__ccinvoices_invoices  where reset_inv = '' ORDER BY id DESC Limit 1";
        $db->setQuery($query);
        $lastinvoicenumber = $db->loadObject();
        return $lastinvoicenumber;
	}




	function gettemplateCSV($id,$tempnum)
	{
		$db 	= JFactory::getDBO();
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 2 LIMIT 1";
	    $db->setQuery($query);
	    $template=array();
		$template = strip_tags($db->loadResult());
		//print_r($template);exit;
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id=".$id." LIMIT 1";
	    $db->setQuery($query);
		$invRow =$db->loadObject();

		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$query = 'SELECT i.*,c.tax_id,c.name,c.contact,c.address,c.email,c.contact_number'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();

		$itemid= explode("|#$|",$rows->item_id);
        $quantity= explode("|#$|",$rows->quantity);
		$pname=explode("|#$|",$rows->pname);
		$itemdesc= explode("|#$|",$rows->item_description);
		$amount=explode("|#$|",$rows->price);
		$tax=explode("|#$|",$rows->tax);

		$itemids="";
		$quant='';
		$pnames='';
		$itemdescs="";
		$amounts='';
        $taxes='';
        $subTotal='';
        $tax_percent = '';
        $breaka="<br/>";
        $price = '';
		$totalPrice = '';
		$excl_tax = '';
		$item_total_excl_tax = '';
		$invoice_total_excl_tax = '';
		$amt_tmp1 = '';
		$subto1 = '';
		$invoice_subtotal = '';
		$product_total_price = '';
		$inv_subtotal_amount = '';
		$op_val['product_total_price'] = '';
		$op_val['invoice_subtotal_amount'] = '';
		$order_tax_total_split_tmp = '';
		$tax_rate_values = array();
		$itemPriceIncTax = array();
		$inv_tax_total = '';
		$productstr="";
		$invoice_discount_price = 0;
		for($i=0;$i<count($quantity);$i++)
		{
			//$invoice_sub_tot=
			$quant.= $quantity[$i]."<br/>";
			$itemids.=$itemid[$i]."<br/>";
			$pnames.= $pname[$i]."<br/>";
			$itemdescs.=$itemdesc[$i]."<br/>";

			//Discount Calc Starts

			//Discount Calc Ends
			$amounts.= $amount[$i]."<br/>";
			$product_total_price += $amount[$i] * $quantity[$i];
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$inv_subtotal_amount += $disc_amt* $quantity[$i];

			}else
			{
				$inv_subtotal_amount += $amount[$i] * $quantity[$i];
			}
            $discount_price = "0";
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$amt_tmp1 = $disc_amt * $tax[$i] / 100;
				$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}else
			{
				$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
				$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}

			$amt_tmp = $amount[$i] * $tax[$i] / 100;
			$itemPriceIncTax[$i]=$amount[$i]+$amt_tmp;
			$taxes.= $amt_tmp."<br/>";
			$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
			$invoice_subtotal += $subto;
			$subTotal.= $subto."<br/>";
			//{item_total_excl_tax}
			$excl_tax = $amount[$i]* $quantity[$i];
			$invoice_total_excl_tax += $excl_tax;
			$item_total_excl_tax.= $excl_tax."<br/>";
			if($tax[$i] == '')
			{
				$tax_percent .= "0%<br/>";
			}else
			{
			$tax_percent .= $tax[$i]."%<br/>";
			}

			$tmp = '';
			$tax_rate = $tax[$i];
			$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
			$inv_tax_total +=$product_tax_amt;
			if(count($tax_rate_values) == 0)
			{
				$tax_rate_values[$tax_rate] = $product_tax_amt;
			}else
			{
				if (array_key_exists($tax_rate, $tax_rate_values))
				{
					$tmp = $tax_rate_values[$tax_rate];
				    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
				}else
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}
			}
		}

		if($tempnum!="0")
		{
			$tempst="";
			$str1="";
			$it_id=strpos($template,"item_id");
			$item_nm=strpos($template,"item_name");
			$item_dec=strpos($template,"item_description");
			$item_qty=strpos($template,"item_quantity");
			$item_amt=strpos($template,"{item_price}");

			$item_price_incl_tax=strpos($template,"{item_price_incl_tax}");


			if(!$item_amt)
			{
				$item_amt= strpos($template,"item_amount");
				if(!$item_amt)
					$item_amt= strpos($template,"item_price_excl_tax");
			}
			else
			{
				$template=str_replace("{item_price}","{item_amount}",$template);
				$template=str_replace("{item_price}","{item_price_excl_tax}",$template);
			}

			$item_tax= strpos($template,"{item_tax}");
			if(!$item_tax)
				$item_tax= strpos($template,"item_tax_percentage");
			else
				$template=str_replace("{item_tax}","{item_tax_percentage}",$template);



			if(trim($it_id)!="")
				{
				$tmpar[$it_id]="item_id";
				$tmpar1[$it_id]=$itemid;
				}
			if(trim($item_qty)!="")
				{
				$tmpar[$item_qty]="item_quantity";
				$tmpar1[$item_qty]=$quantity;
				}
			if(trim($item_nm)!="")
				{
				$tmpar[$item_nm]="item_name";
				$tmpar1[$item_nm]=$pname;
				}
			if(trim($item_dec)!="")
				{
				$tmpar[$item_dec]="item_description";
				$tmpar1[$item_dec]=$itemdesc;
				}
			if(trim($item_amt)!="")
				{
				$tmpar[$item_amt]="item_amount";
				$tmpar1[$item_amt]=$amount;
				}
			if(trim($item_tax)!="")
				{
				$tmpar[$item_tax]="item_tax_percentage";
				$tmpar1[$item_tax]=$tax;
				}
			if(trim($item_price_incl_tax)!="")
				{
				$tmpar[$item_price_incl_tax]="item_price_incl_tax";
				$tmpar1[$item_price_incl_tax]=$itemPriceIncTax;
				}



			ksort($tmpar);
			ksort($tmpar1);

			$dynamicfield=0;
			foreach($tmpar as $tmpar=>$value)
			{
				$str1[$dynamicfield]=$value;
				$op_val[$value][$dynamicfield] = $tmpar1[$tmpar];
				$dynamicfield=$dynamicfield+1;
				}
			if(trim($item_qty)!="" OR trim($item_nm)!="" OR trim($item_amt)!="" OR trim($item_tax)!="" OR trim($item_dec)!="" OR trim($it_id)!="")
			{
			}
			else
			{
				$op_val['tax_percentage'] = $tax_percent;
				$op_val['item_tax_percentage'] = $tax_percent;
			}
		}

		ksort($tax_rate_values);
		foreach($tax_rate_values as $key=>$value)
		{
			$order_tax_total_split_tmp .= $value."&nbsp;(".$key."%)&nbsp;<br/>";
		}
	    //{order_tax_total_split}
	    $orderitem_tax_total = $inv_tax_total;

	    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
	    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
	    $order_tax_total_split .= $order_tax_total_split_tmp;
	    $op_val['tax_total_split'] = $order_tax_total_split;
	    $op_val['invoice_tax_total_split'] = $order_tax_total_split;
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['price'] =$amounts;
		$post['tax'] =$taxes;
        /* contact values */
		$op_val['invoice_note'] = $rows->note;
		//GETTING PAYMENT METHOD NAMES
		$paymentmethodnames=$this->getPaymentMethodsName();
		$op_val['payment_methods']= $paymentmethodnames;
        $op_val['contact_name'] = $rows->name;
        $op_val['contact_number'] = $rows->contact_number;
		$op_val['contact'] = $rows->contact;
		$op_val['contact_person'] = $rows->contact;

		$contact_address = str_replace("\n", "<br>", $rows->address);
		$contact_address=str_replace(",",'`',$contact_address);
		$op_val['contact_address'] = $contact_address;
		$op_val['contact_taxid']= $rows->tax_id;
		$op_val['contact_tax_id']= $rows->tax_id;
		$op_val['contact_email'] = $rows->email;
	    $op_val['invoice_number'] = $this->getInvoiceNumberFormat($invRow->id);
		$op_val['invoice_date'] = $this->dateChangeFormat($rows->invoice_date,$conf->date_format);
		$op_val['invoice_due_date'] = $this->dateChangeFormat( $rows->duedate,$conf->date_format);
		//Not sent yet
		if(strtotime($invRow->invoice_sent_date) == '' OR trim($invRow->invoice_sent_date)=="0000-00-00" OR trim($invRow->invoice_sent_date)=="11-30-99" OR trim($invRow->invoice_sent_date)=="30-11-99")
		{
			$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
		}
		else
		{
		$op_val['invoice_sent_date'] = $this->dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
		}

		if($rows->discount == '' OR $rows->discount == '0')
		{
			$op_val['invoice_discount'] = '';
			$op_val['invoice_discount_new']='';
			$op_val['invoice_discount_excl_tax']='';
			$op_val['invoice_subtotal_with_discount']=$product_total_price;
			$op_val['invoice_subtotal_incl_discount']=$product_total_price;
		}else
		{
			$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
			$op_val['invoice_discount_new']=($product_total_price*$rows->discount)/100;
			$op_val['invoice_discount_excl_tax']=($product_total_price*$rows->discount)/100;
			$op_val['invoice_subtotal_with_discount']=$product_total_price-($product_total_price*$rows->discount)/100;
			$op_val['invoice_subtotal_incl_discount']=$product_total_price-($product_total_price*$rows->discount)/100;
		}
		$op_val['product_total_price'] = $product_total_price;
		$op_val['invoice_subtotal_amount'] = $inv_subtotal_amount;
		$invoice_discount_price=$invoice_subtotal;
		$op_val['invoice_discount_price']=number_format($invoice_discount_price * $rows->discount /100,2);
		$op_val['invoice_discount_amount']=number_format($invoice_discount_price * $rows->discount /100,2);

		$op_val['invoice_tax'] = $rows->totaltax;
		$op_val['invoice_tax_total'] = $rows->totaltax;
		$op_val['invoice_total'] = '';
		$op_val['invoice_total_excl_tax'] = '';
		$invoice_total=str_replace(",",'`',$rows->total);
		$op_val['invoice_total'] = $rows->total;
	    $op_val['invoice_total_excl_tax'] = $invoice_total_excl_tax;
	    $op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];
		$op_val['item_total_excl_tax'] = $item_total_excl_tax;
		$op_val['item_total_incl_tax'] = $subTotal;
		$invoice_subtotal=str_replace(",",'`',$invoice_subtotal);
		$op_val['invoice_subtotal'] = $invoice_subtotal;
		$op_val['invoice_subtotal_excl_discount'] = $invoice_subtotal;
		//$op_val['invoice_subtotal'] = $this->changeCurrencyFormat($invoice_subtotal);

		$op_val['product_tax'] = $taxes;
		$op_val['item_tax_amount'] = $taxes;
		$op_val['user_name'] = $conf->user_name;
		$op_val['company_name'] = $conf->user_company;
		$op_val['company_email'] = $conf->company_email;
		$op_val['company_phone'] = $conf->company_phone;
		$company_address = str_replace("\n", "<br/>", $conf->company_address);
		$op_val['company_address'] = $company_address;
		$other_details = str_replace("\n", "<br/>", $conf->other_details);
		$op_val['company_details'] = $other_details;
		$op_val['company_url'] = $conf->company_url;
		$op_val['tax_id'] = $conf->tax_id;
		$op_val['company_tax_id'] = $conf->tax_id;
		if($conf->logo != "")
		{
		$op_val['logo'] = '<img src="'.JURI::root()."administrator/components/com_ccinvoices/assets/logo/".$conf->logo.'"/>' ;
		}else
		{
			$op_val['logo'] = '';
		}
		$template = $this->convertImgTags($template);
		$templatereturn=$template;
		$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );
		//Get Custom Tags From The Plugin
		$val["id"] = $id;
		$options = array($val);
		JPluginHelper::importPlugin( 'ccinvoicetags' );
		$dispatcher = JDispatcher::getInstance();
		$customTag = $dispatcher->trigger( '_getCustomTags',$options);
		$temparry="";
		foreach ($customTag as $customTags)
		{
			foreach ($customTags as $customTagName=>$value)
			{
				$op_val[$customTagName] = $value;
			}
		}
		if($tempnum!=0)
		{
			$ki=0;
			foreach($op_val as $op_vals=>$value)
			{
				if(is_array($value))
				{
					$arrval[]=$value[$ki];
					$arrkey[]="{".$op_vals."}";
					$ki=$ki+1;
				}
			}


			$booleanvar=0;
			foreach($op_val as $op_vals=>$value)
			{
				if(is_array($value) )
				{
					if($booleanvar=="0")
					{
						$tempval="";
						$tempkey="";
						for($i=0;$i<=count($arrval[0])-1;$i++)
						{
							for($j=0;$j<=count($arrkey)-1;$j++)
							{
								$arrval[$j][$i]=str_replace(",",'`',$arrval[$j][$i]);
								$temparry[$i][$j]=$arrval[$j][$i];
								$tempval.=$arrval[$j][$i].",";
								$tempkey=$arrkey[$j];
							}
						}
						$booleanvar=1;
					}
				}
				else
				{
					$find = "{".$op_vals."}";
					$value=str_replace(",",'`',$value);
					$replace = $value;
					$template = str_replace($find,$replace,$template);
					$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
				}
			}
		}
		return array($template,$templatereturn,$temparry);
	}
	function getPaymentMethodsName()
	{
		$plugins_name= JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher = JDispatcher::getInstance();
		$methodnames = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$paymentmethodname="";
		if(count($methodnames)>0)
		{
			$i=0;
			foreach($methodnames as $pluginname)
			{
				if($i==count($methodnames)-2)
				{
					$paymentmethodname.=$pluginname["payment_method_note"]." or ";
				}
				else
				{
					$paymentmethodname.=$pluginname["payment_method_note"].", ";
				}
				$i=$i+1;
			}
		}
		$paymentmethodname=substr($paymentmethodname,0,strlen($paymentmethodname)-2);
		return $paymentmethodname;
	}
	function changeCurrencyFormat($cur_val)
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT cformat,currency_symbol,symbol_display FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$format = $conf->cformat;
		$tmp = '';
		$price_amount = "";
		if($format == 0)
		{
			$tmp = number_format($cur_val, 2, '.', ',');
		}else if($format == 1)
		{
			$tmp = number_format($cur_val, 2, ',', '.');
		}else if($format == 2)
		{
			$tmp = number_format($cur_val, 2, '.', ' ');
		}else if($format == 3)
		{
			$tmp = number_format($cur_val, 2, ".", "'");
		}
		if($conf->symbol_display == '1')
		{
			$price_amount = $conf->currency_symbol.$tmp;
		}else
		{
			$price_amount = $tmp.$conf->currency_symbol;
		}
		return $price_amount;
	}
	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}

	function invoicesendmessage()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$FromName	= $mainframe->getCfg('fromname');
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$contactus = $post['contact'];
		$invoice_number = $post['number'];
		$user_name = $post['name'];
		//$user_company = date("Y-m-d");
		$reminder	= JRequest::getVar( 'reminder');
		$resend	= JRequest::getVar( 'resend');
		$sql = "SELECT invoice_sent_date FROM #__ccinvoices_invoices WHERE number=".$invoice_number." LIMIT 1";
		$db->setQuery($sql);
		$invoice_sent_date = $db->loadResult();
		return ;
	}

	function invoicesendsubject()
	{
		$db = JFactory::getDBO();
		$sql = "SELECT default_email_sub,rem_email_sub FROM #__ccinvoices_configuration WHERE id = 1 LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
   		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
   	    $reminder	= JRequest::getVar( 'reminder');
		$op_val['invoice_number'] = $post['number'];

       	if($reminder == '1')
		{
			if($config->rem_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEREMINDER_SUB' );
		}else
		{
				$invoicesend_sub = $config->rem_email_sub;
			}
		}else
		{
			if($config->default_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEEMAIL_SUB' );
			}else
			{
				$invoicesend_sub = $config->default_email_sub;
			}
		}
		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$invoicesend_sub = str_replace($find,$replace,$invoicesend_sub);
		}
		return $invoicesend_sub;
	}
	function dateChangeFormat($date_temp,$date_format)
	{
		if(strtotime($date_temp) != '')
		{
		return strftime($date_format,strtotime($date_temp));
		}else
		{
			return;
	}
	}

	function getInvoiceNumberFormat($invNum)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$conf = $db->loadObject();
		$invoice_format = $conf->invoice_format;
		$query = 'SELECT custom_invoice_number,number from #__ccinvoices_invoices WHERE id='.$invNum.' LIMIT 1 ';
		$db->setQuery($query);
		$rowobj = $db->loadObject();

		$custom_invoice_number=$rowobj->custom_invoice_number;
		if($rowobj->custom_invoice_number == '' || $rowobj->custom_invoice_number == '0')
		{
			$custom_invoice_number = $rowobj->number;
		}
		return $custom_invoice_number;
	}

	function TinyMCELoad()
	{
		$_basePath = 'media/editors/tinymce/jscripts/tiny_mce';
		$app		= JFactory::getApplication();
		$language	= JFactory::getLanguage();

		$mode	= 1;
		$theme	= 'simple';
		$skin	=  '0';

		switch ($skin)
  		{
			case '3':
				$skin = "skin : \"o2k7\", skin_variant : \"black\",";
				break;

			case '2':
				$skin = "skin : \"o2k7\", skin_variant : \"silver\",";
				break;

			case '1':
				$skin = "skin : \"o2k7\",";
				break;
			case '0':
			default:
				$skin = "skin : \"default\",";
		}

		$compressed		= 0;
		$cleanup_startup	= 0;
		$cleanup_save		=  2;
		$entity_encoding	= 'raw';

		if ($cleanup_startup) {
			$cleanup_startup = 'true';
		}
		else {
			$cleanup_startup = 'false';
		}

		switch ($cleanup_save)
		{
			case '0':
				// Never clean up on save.
				$cleanup = 'false';
				break;

			case '1':
				// Clean up front end edits only.
				if ($app->isadmin()) {
					$cleanup = 'false';
				}
				else {
					$cleanup = 'true';
				}

				break;

			default:
				// Always clean up on save
				$cleanup = 'true';
		}

		$langMode			=  0;
		$langPrefix			= 'en';

		if ($langMode) {
			$langPrefix = substr($language->getTag(), 0, strpos($language->getTag(), '-'));
		}

		if ($language->isRTL()) {
			$text_direction = 'rtl';
		}
		else {
			$text_direction = 'ltr';
		}

		$use_content_css	=  1;
		$content_css_custom	= '';

		/*
		 * Lets get the default template for the site application
		 */
		$db		= JFactory::getDBO();
		$query	= $db->getQuery(true);

		$query->select('template');
		$query->from('#__template_styles');
		$query->where('client_id=0 AND home=1');

		$db->setQuery( $query );
		$template = $db->loadResult();

		$content_css = '';

		$templates_path = JPATH_SITE.DS.'templates';
		// loading of css file for 'styles' dropdown
		if ( $content_css_custom ) {
			// If URL, just pass it to $content_css
			if (strpos( $content_css_custom, 'http' ) !==false) {
				$content_css = 'content_css : "'. $content_css_custom .'",';
			}
			// If it is not a URL, assume it is a file name in the current template folder
			else {
				$content_css = 'content_css : "'. JURI::root() .'templates/'. $template . '/css/'. $content_css_custom .'",';

				// Issue warning notice if the file is not found (but pass name to $content_css anyway to avoid TinyMCE error
				if (!file_exists($templates_path.DS.$template.DS.'css'.DS.$content_css_custom)) {
					$msg = sprintf (JText::_('PLG_TINY_ERR_CUSTOMCSSFILENOTPRESENT'), $content_css_custom);
					JError::raiseNotice('SOME_ERROR_CODE', $msg);
				}
			}
		}
		else {
			// process when use_content_css is Yes and no custom file given
			if ($use_content_css) {
				// first check templates folder for default template
				// if no editor.css file in templates folder, check system template folder
				if (!file_exists($templates_path.DS.$template.DS.'css'.DS.'editor.css')) {
					$template = 'system';

					// if no editor.css file in system folder, show alert
					if (!file_exists($templates_path.DS.'system'.DS.'css'.DS.'editor.css')) {
						JError::raiseNotice('SOME_ERROR_CODE', JText::_('PLG_TINY_ERR_EDITORCSSFILENOTPRESENT'));
					}
					else {
						$content_css = 'content_css : "' . JURI::root() .'templates/system/css/editor.css",';
					}
				}
				else {
					$content_css = 'content_css : "' . JURI::root() .'templates/'. $template . '/css/editor.css",';
				}
			}
		}

		$relative_urls		= '1';

		if ($relative_urls) {
			// relative
			$relative_urls = "true";
		}
		else {
			// absolute
			$relative_urls = "false";
		}

		$newlines			= 0;

		if ($newlines) {
			// br
			$forcenewline = "force_br_newlines : true, force_p_newlines : false, forced_root_block : '',";
		}
		else {
			// p
			$forcenewline = "force_br_newlines : false, force_p_newlines : true, forced_root_block : 'p',";
		}

		$invalid_elements	= 'script,applet,iframe';
		$extended_elements	= '';


		// theme_advanced_* settings
		$toolbar			= 'top';
		$toolbar_align		= 'left';
		$html_height		= '550';
		$html_width			= '750';
		$resizing			= 'true';
		$resize_horizontal	= 'false';
		$element_path = '';

		if (1) {
			$element_path = "theme_advanced_statusbar_location : \"bottom\", theme_advanced_path : true";
		}
		else {
			$element_path = "theme_advanced_statusbar_location : \"none\", theme_advanced_path : false";
		}

		$buttons1_add_before = $buttons1_add = array();
		$buttons2_add_before = $buttons2_add = array();
		$buttons3_add_before = $buttons3_add = array();
		$buttons4 = array();
		$plugins	= array();

		if ($extended_elements != "") {
			$elements	= explode(',', $extended_elements);
		}

		// Initial values for buttons
		array_push($buttons4,'cut','copy','paste');
		// array_push($buttons4,'|');

		// Plugins

		// fonts
		$fonts =  1;

		if ($fonts) {
			$buttons1_add[]	= 'fontselect,fontsizeselect';
		}

		// paste
		$paste =  1;

		if ($paste) {
			$plugins[]	= 'paste';
			$buttons4[]	= 'pastetext';
			$buttons4[]	= 'pasteword';
			$buttons4[]	= 'selectall,|';
		}

		// search & replace
		$searchreplace		=   1;

		if ($searchreplace) {
			$plugins[]	= 'searchreplace';
			$buttons2_add_before[]	= 'search,replace,|';
		}

		// insert date and/or time plugin
		$insertdate			=  1;
		$format_date		= '%Y-%m-%d';
		$inserttime			= 1;
		$format_time		= '%H:%M:%S';

		if ($insertdate or $inserttime) {
			$plugins[]	= 'insertdatetime';
			if ($insertdate) {
				$buttons2_add[]	= 'insertdate';
			}

			if ($inserttime) {
				$buttons2_add[]	= 'inserttime';
			}
		}

		// colors
		$colors =  1;

		if ($colors) {
			$buttons2_add[]	= 'forecolor,backcolor';
		}

		// table
		$table = 1;

		if ($table) {
			$plugins[]	= 'table';
			$buttons3_add_before[]	= 'tablecontrols';
		}

		// emotions
		$smilies = 1;

		if ($smilies) {
			$plugins[]	= 'emotions';
			$buttons3_add[]	= 'emotions';
		}

		//media plugin
		$media = 1;

		if ($media) {
			$plugins[] = 'media';
			$buttons3_add[] = 'media';
		}

		// horizontal line
		$hr = 1;

		if ($hr) {
			$plugins[]	= 'advhr';
			$elements[] = 'hr[id|title|alt|class|width|size|noshade|style]';
			$buttons3_add[]	= 'advhr';
		}
		else {
			$elements[] = 'hr[id|class|title|alt]';
		}

		// rtl/ltr buttons
		$directionality	= 1;

		if ($directionality) {
			$plugins[] = 'directionality';
			$buttons3_add[] = 'ltr,rtl';
		}

		// fullscreen
		$fullscreen	= 1;

		if ($fullscreen) {
			$plugins[]	= 'fullscreen';
			$buttons2_add[]	= 'fullscreen';
		}

		// layer
		$layer = 1;

		if ($layer) {
			$plugins[]	= 'layer';
			$buttons4[]	= 'insertlayer';
			$buttons4[]	= 'moveforward';
			$buttons4[]	= 'movebackward';
			$buttons4[]	= 'absolute';
		}

		// style
		$style = 1;

		if ($style) {
			$plugins[]	= 'style';
			$buttons4[]	= 'styleprops';
		}

		// XHTMLxtras
		$xhtmlxtras	= 1;

		if ($xhtmlxtras) {
			$plugins[]	= 'xhtmlxtras';
			$buttons4[]	= 'cite,abbr,acronym,ins,del,attribs';
		}

		// visualchars
		$visualchars = 1;

		if ($visualchars) {
			$plugins[]	= 'visualchars';
			$buttons4[]	= 'visualchars';
		}

		// non-breaking
		$nonbreaking = 1;

		if ($nonbreaking) {
			$plugins[]	= 'nonbreaking';
			$buttons4[]	= 'nonbreaking';
		}

		// blockquote
		$blockquote	= 1;

		if ( $blockquote ) {
			$buttons4[] = 'blockquote';
		}

		// wordcount
		$wordcount	= 1;

		if ( $wordcount ) {
			$plugins[] = 'wordcount';
		}

		// template
		$template = 1;

		if ($template) {
			$plugins[]	= 'template';
			$buttons4[]	= 'template';
		}

		// advimage
		$advimage = 1;

		if ($advimage) {
			$plugins[]	= 'advimage';
			$elements[]	= 'img[class|src|border=0|alt|title|hspace|vspace|width|height|align|onmouseover|onmouseout|name|style]';
		}

		// advlink
		$advlink	= 1;

		if ($advlink) {
			$plugins[]	= 'advlink';
			$elements[]	= 'a[id|class|name|href|target|title|onclick|rel|style]';
		}

		//advlist
		$advlist	= 1;

		if ($advlist) {
			$plugins[]	= 'advlist';
		}

		// autosave
		$autosave = 1;

		if ($autosave) {
			$plugins[]	= 'autosave';
		}

		// context menu
		$contextmenu = 1;

		if ($contextmenu) {
			$plugins[]	= 'contextmenu';
		}

		// inline popups
		$inlinepopups			= 1;

		if ($inlinepopups) {
			$plugins[]	= 'inlinepopups';
			$dialog_type = "dialog_type : \"modal\",";
		}
		else {
			$dialog_type = "";
		}

		$custom_plugin = '';

		if ($custom_plugin != "") {
			$plugins[] = $custom_plugin;
		}

		$custom_button = '';

		if ($custom_button != "") {
			$buttons4[] = $custom_button;
		}

		// Prepare config variables
		$buttons1_add_before = implode(',', $buttons1_add_before);
		$buttons2_add_before = implode(',', $buttons2_add_before);
		$buttons3_add_before = implode(',', $buttons3_add_before);
		$buttons1_add = implode(',', $buttons1_add);
		$buttons2_add = implode(',', $buttons2_add);
		$buttons3_add = implode(',', $buttons3_add);
		$buttons4 = implode(',', $buttons4);
		$plugins = implode(',', $plugins);
		$elements = implode(',', $elements);


		switch ($mode)
		{
			case 0: /* Simple mode*/
				if ($compressed) {
					$load = "\t<script type=\"text/javascript\" src=\"".
							JURI::root().$this->_basePath.
							"/tiny_mce_gzip.js\"></script>\n";
					$load .= "\t<script type=\"text/javascript\">
					tinyMCE_GZ.init({
					themes : \"$theme[$mode]\",
					languages : \"". $langPrefix . "\"
				});
				</script>";
				}
				else {
					$load = "\t<script type=\"text/javascript\" src=\"".
							JURI::root().$this->_basePath.
							"/tiny_mce.js\"></script>\n";
				}

				$return = $load .
				"\t<script type=\"text/javascript\">
				tinyMCE.init({
					// General
					directionality: \"$text_direction\",
					editor_selector : \"mce_editable\",
					language : \"". $langPrefix . "\",
					mode : \"specific_textareas\",
					$skin
					theme : \"$theme[$mode]\",
					// Cleanup/Output
					inline_styles : true,
					gecko_spellcheck : true,
					cleanup : $cleanup,
					cleanup_on_startup : $cleanup_startup,
					entity_encoding : \"$entity_encoding\",
					$forcenewline
					// URL
					relative_urls : $relative_urls,
					remove_script_host : false,
					// Layout
					$content_css
					document_base_url : \"". JURI::root() ."\"
				});
				</script>";
				break;

			case 1: /* Advanced mode*/

				if ($compressed) {
					$load = "\t<script type=\"text/javascript\" src=\"".
							JURI::root().$this->_basePath.
							"/tiny_mce_gzip.js\"></script>\n";
					$load .= "\t<script type=\"text/javascript\">
						tinyMCE_GZ.init({
						themes : \"$theme[$mode]\",
						languages : \"". $langPrefix . "\"
					});
				</script>";
				}
				else {
					$load = "\t<script type=\"text/javascript\" src=\"".
							JURI::root().$_basePath.
							"/tiny_mce.js\"></script>\n";
				}

				$return = $load .
				"\t<script type=\"text/javascript\">
				tinyMCE.init({
					// General
					directionality: \"$text_direction\",
					editor_selector : \"mce_editable\",
					language : \"". $langPrefix . "\",
					mode : \"specific_textareas\",
					$skin
					theme : \"simple\",
					// Cleanup/Output
					inline_styles : true,
					gecko_spellcheck : true,
					cleanup : $cleanup,
					cleanup_on_startup : $cleanup_startup,
					entity_encoding : \"$entity_encoding\",
					extended_valid_elements : \"$elements\",
					$forcenewline
					invalid_elements : \"$invalid_elements\",
					// URL
					relative_urls : $relative_urls,
					remove_script_host : false,
					document_base_url : \"". JURI::root() ."\",
					// Layout
					$content_css
					// Advanced theme
					theme_advanced_toolbar_location : \"$toolbar\",
					theme_advanced_toolbar_align : \"$toolbar_align\",
					theme_advanced_source_editor_height : \"$html_height\",
					theme_advanced_source_editor_width : \"$html_width\",
					theme_advanced_resizing : $resizing,
					theme_advanced_resize_horizontal : $resize_horizontal,
					$element_path
				});
				</script>";

				break;

			case 2: /* Extended mode*/
				if ($compressed) {
					$load = "\t<script type=\"text/javascript\" src=\"".
							JURI::root().$this->_basePath.
							"/tiny_mce_gzip.js\"></script>\n";
					$load .= "\t<script type=\"text/javascript\">
				tinyMCE_GZ.init({
					themes : \"$theme[$mode]\",
					plugins : \"$plugins\",
					languages : \"". $langPrefix . "\"
				});
				</script>";
				}
				else {
				$load = "\t<script type=\"text/javascript\" src=\"".
						JURI::root().$this->_basePath.
						"/tiny_mce.js\"></script>\n";
				}

				$return = $load .
				"\t<script type=\"text/javascript\">
				tinyMCE.init({
					// General
					$dialog_type
					directionality: \"$text_direction\",
					editor_selector : \"mce_editable\",
					language : \"". $langPrefix . "\",
					mode : \"specific_textareas\",
					plugins : \"$plugins\",
					$skin
					theme : \"$theme[$mode]\",
					// Cleanup/Output
					inline_styles : true,
					gecko_spellcheck : true,
					cleanup : $cleanup,
					cleanup_on_startup : $cleanup_startup,
					entity_encoding : \"$entity_encoding\",
					extended_valid_elements : \"$elements\",
					$forcenewline
					invalid_elements : \"$invalid_elements\",
					// URL
					relative_urls : $relative_urls,
					remove_script_host : false,
					document_base_url : \"". JURI::root() ."\",
					//Templates
					template_external_list_url :  \"". JURI::root() ."media/editors/tinymce/templates/template_list.js\",
					// Layout
					$content_css
					// Advanced theme
					theme_advanced_toolbar_location : \"$toolbar\",
					theme_advanced_toolbar_align : \"$toolbar_align\",
					theme_advanced_source_editor_height : \"$html_height\",
					theme_advanced_source_editor_width : \"$html_width\",
					theme_advanced_resizing : $resizing,
					theme_advanced_resize_horizontal : $resize_horizontal,
					$element_path,
					theme_advanced_buttons1_add_before : \"$buttons1_add_before\",
					theme_advanced_buttons2_add_before : \"$buttons2_add_before\",
					theme_advanced_buttons3_add_before : \"$buttons3_add_before\",
					theme_advanced_buttons1_add : \"$buttons1_add\",
					theme_advanced_buttons2_add : \"$buttons2_add\",
					theme_advanced_buttons3_add : \"$buttons3_add\",
					theme_advanced_buttons4 : \"$buttons4\",
					plugin_insertdate_dateFormat : \"$format_date\",
					plugin_insertdate_timeFormat : \"$format_time\",
					fullscreen_settings : {
						theme_advanced_path_location : \"top\"
					}
				});
				</script>";
				break;
		}
		return $return;
	}
	function getLastItemId()
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT item_id FROM #__ccinvoices_items order by id desc";
        $db->setQuery($query);
        $row = $db->loadObject();
        if(count($row))
        	return $row->item_id;
	}
}
?>