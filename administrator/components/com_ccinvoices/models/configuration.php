<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries
jimport('joomla.application.component.modeladmin');

class ccInvoicesModelConfiguration extends JModelAdmin
{
    function __construct()
    {
		parent::__construct();
		$array = JRequest::getVar('id',  0, '', 'array');
		$this->setId((int)$array[0]);
    }
   	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}
   	function &getData()
	{
		$row = $this->getTable();
		$row->load( $this->_id );
		return $row;
	}
	public function getTable($type = 'configuration', $prefix = 'Table', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}
	// save
	function store()
	{
		$db	=  JFactory::getDBO();
		$row = $this->getTable();
		// Bind the form fields
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );


		$logo = JRequest::getVar('logo', null, 'files', 'array' );
		$img_path =	JPATH_ROOT.DS."media".DS."com_ccinvoices".DS."logo".DS;

		$default_note = $this->convertImgTags(@$post["default_note"]);

		if (!$row->bind($post))
		{
			$this->setError($row->getError());
			return 0;
		}
		$row->default_note = $default_note;

		if($logo['name'] != '')
		{
			jimport('joomla.filesystem.file');
			$tmp_file_name = JFile::makeSafe(time().".".JFile::getExt($logo['name']));
			if (!JFile::upload($logo['tmp_name'], $img_path.$tmp_file_name))
			{
				return 0;
			}else
			{
				$row->logo = $tmp_file_name;
			}
		}
		// Make sure data is valid
		if (!$row->check())
		{
			$this->setError($row->getError());
			return 0;
		}
		// Store it
		if (!$row->store())
		{
			$this->setError($row->getError());
			return 0;
		}
		return $row->id;
	}

	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}
	public function getForm($data = array(), $loadData = true)
	{
		// Get the form.
		$form = $this->loadForm('com_ccinvoices.banner', 'banner', array('control' => 'jform', 'load_data' => $loadData));
		if (empty($form))
		{
			return false;
		}

		// Determine correct permissions to check.
		if ($this->getState('banner.id'))
		{
			// Existing record. Can only edit in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.edit');
		}
		else
		{
			// New record. Can only create in selected categories.
			$form->setFieldAttribute('catid', 'action', 'core.create');
		}

		// Modify the form based on access controls.
		if (!$this->canEditState((object) $data))
		{
			// Disable fields for display.
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');
			$form->setFieldAttribute('state', 'disabled', 'true');
			$form->setFieldAttribute('sticky', 'disabled', 'true');

			// Disable fields while saving.
			// The controller has already verified this is a record you can edit.
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
			$form->setFieldAttribute('state', 'filter', 'unset');
			$form->setFieldAttribute('sticky', 'filter', 'unset');
		}

		return $form;
	}

	/**
	 * Method to get the data that should be injected in the form.
	 *
	 * @return  mixed  The data for the form.
	 *
	 * @since   1.6
	 */
	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$data = JFactory::getApplication()->getUserState('com_ccinvoices.edit.banner.data', array());

		if (empty($data))
		{
			$data = $this->getItem();

			// Prime some default values.
			if ($this->getState('banner.id') == 0)
			{
				$app = JFactory::getApplication();
				$data->set('catid', JRequest::getInt('catid', $app->getUserState('com_ccinvoices.banners.filter.category_id')));
			}
		}

		return $data;
	}
}
?>