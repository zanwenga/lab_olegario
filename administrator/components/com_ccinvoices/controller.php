<?php
/**
* @package    [ccNewsletter]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2008 - 2013] Chill Creations
* @copyright     Copyright (C) [Copyright 2010 Elodig- All rights reserved]
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccNewsletter].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

ccNewsletter elodig hack
-------
Author: Elodig
Copyright: Copyright (C) Copyright 2010 Elodig- All rights reserved

**/
defined('_JEXEC') or die;

/**
 * Plugins master display controller.
 *
 * @package     Joomla.Administrator
 * @subpackage  com_plugins
 * @since       1.5
 */

class ccinvoicesController extends CCINVOISController
{
	function display($cachable = false, $urlparams = false)
	{
		require_once JPATH_COMPONENT.'/helper/ccinvoices.php';
		// Load the submenu.

		ccinvoicesHelper::addSubmenu(JRequest::getCmd('view', 'invoices'));
		$view   = JRequest::getCmd('view', 'invoices');
		JRequest::setVar('view',$view);
		parent::display();
	}
}
