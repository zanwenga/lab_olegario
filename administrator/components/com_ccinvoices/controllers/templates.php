<?php

/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

class ccInvoicesControllertemplates extends CCINVOISController
{
    function __construct()
    {
        parent::__construct();
		$this->registerTask( 'apply',       'save' );
    }

   	function display($cachable = false, $urlparams = false)
	{
		JRequest::setVar( 'view', 'templates');
		parent::display();
	}

	function cancel()
	{
		// set user message redirect
		$msg = JText::_( 'CC_TEMPLATE_CANCELED' );
		$this->setRedirect( 'index.php?option=com_ccinvoices&view=templates', $msg );
	}
	function save()
	{
		$post			= JRequest::get('post', JREQUEST_ALLOWRAW);
		$model = $this->getModel('templates');
		if ($cid = $model->store())	{
		$msg = JText::_( 'CC_TEMPLATE_SAVED' );
		} else
		{
			$msg = JText::_( 'CC_TEMPLATE_SAVED_FAILED' );
		}
		$tsk=$post['task'];
		$tsk=str_replace("templates.","",$tsk);
		if($tsk == 'apply')	{
			$cid 		= JRequest::getInt( 'id');
			$link 	= 'index.php?option=com_ccinvoices&task=templates.edit&cid[]='.$cid;
		} else {
			$link = 'index.php?option=com_ccinvoices&view=templates';
		}
		$this->setRedirect($link, $msg);
	}

	function edit()
	{
		JRequest::setVar( 'view', 'templates');
		JRequest::setVar( 'layout', 'form');
		parent::display();
	}

}
?>

