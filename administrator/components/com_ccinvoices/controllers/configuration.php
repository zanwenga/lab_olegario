<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

class ccInvoicesControllerConfiguration extends CCINVOISController
{
	function __construct()
    {
        //Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'configuration');
        }
        $this->item_type = 'Configuration';
        parent::__construct();
		$this->registerTask( 'apply', 'save' );
    }
	function reset_inv()
	{
		$db = JFactory::getDBO();
		$sql = "UPDATE #__ccinvoices_invoices SET reset_inv = 1";
		$db->setQuery($sql);
		if($db->query())
		{
			$msg = JText::_("CC_INVOICE_RESET_SUCC");
		}else
		{
			$msg = JText::_("CC_INVOICE_RESET_FAILD");
		}

		$this->setRedirect( 'index.php?option=com_ccinvoices' ,$msg);
	}
	function save()
	{
		$post	= JRequest::get( 'post' );
		$model = $this->getModel('configuration');
		if ($model->store())
		{
			$msg = JText::_( 'CC_CONFIG_SAVED_SUCCESS' );
		}else {
			$msg = JText::_( 'CC_CONFIG_SAVED_ERROR' );
		}
		$tsk=$post['task'];
		$tsk=str_replace("configuration.","",$tsk);
		if($tsk == 'apply')
		{
			//http://localhost/JOOMLA3xtns/ccInvoices%5BJ3%5D/administrator/index.php?option=com_ccinvoices&view=configuration&layout=form
			$link 	= 'index.php?option=com_ccinvoices&view=configuration';
		}else
		{
			$link = 'index.php?option=com_ccinvoices';
		}
		$this->setRedirect($link, $msg);
	}
	function cancel()
	{
		$this->setRedirect("index.php?option=com_ccinvoices&view=invoices");
	}
}
?>