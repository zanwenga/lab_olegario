<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

class ccInvoicesControlleritems extends CCINVOISController
{
    function __construct()
    {
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'items');
        }
        $this->item_type = 'items';
        parent::__construct();
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'add',	'edit' );
		$this->registerTask( 'new',	'edit' );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'save2new', 'save' );
		$this->registerTask( 'save2copy', 'save' );
		$this->registerTask( 'trash', 'remove' );
    }

	function edit()
	{
		JRequest::setVar( 'view', 'items');
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}
	function modelassignItem()
	{
		JRequest::setVar( 'view', 'items');
		parent::display();
	}
	function save()
	{
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$model = $this->getModel('items');

		if ($id=$model->store())
		{
			$msg = JText::_( 'CC_ITEMS_SAVED_SUCC' );
		}else
		{
			$msg = JText::_( 'CC_ITEMS_SAVED_UNSUCC' );
		}
		$tsk=$post['task'];
		$tsk=str_replace("items.","",$tsk);

		if($tsk == 'apply')
		{
			$link 	= 'index.php?option=com_ccinvoices&task=items.edit&cid[]='. $id;
		}
		elseif($tsk=='save2new')
		{
			$link 	= 'index.php?option=com_ccinvoices&task=items.edit';
		}
		elseif($tsk=='save2copy')
		{
			$msg = JText::_( 'CC_CONTACT_SAVED_SUCC');
			$link 	= 'index.php?option=com_ccinvoices&task=items.edit&cid[]='. $id;
		}
		else
		{
			$link = 'index.php?option=com_ccinvoices&view=items';
		}
		$this->setRedirect($link, $msg);
	}
	function copy()
	{
		// get the model
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$cid	= JRequest::getVar( 'cid', null, 'post', 'array' );
		$db = JFactory::getDBO();
		foreach($cid as $id)
		{
			$query = "SELECT * FROM #__ccinvoices_items WHERE id=".$id."";
			$db->setQuery( $query );
			$rows = $db->loadObject();

			$query = "INSERT INTO #__ccinvoices_items ( `item_id`,`item_quantity`, `item_name`, `item_description`, `item_price_excl_tax`,`item_tax_percentage`) VALUES (  ".$db->Quote($rows->item_id).",".$db->Quote($rows->item_quantity).", ".$db->Quote($rows->item_name).",".$db->Quote($rows->item_description).", ".$db->Quote($rows->item_price_excl_tax).", ".$db->Quote($rows->item_tax_percentage).")";
			$db->setQuery( $query );
			$db->query();
		}

		$msg = JText::_( 'CC_ITEMS_COPYED');
		$link = "index.php?option=com_ccinvoices&view=items";
		$this->setRedirect($link, $msg);
	}
	function remove()
	{
		// get the model
		$model = $this->getModel('items');
		if($model->delete())
		{
			$msg = JText::_( 'CC_ITEMS_DELETED' );
			$this->setRedirect( 'index.php?option=com_ccinvoices&view=items', $msg );
		}
		else
		{
			$msg = JText::_( 'ERROR_ITEMS_DELETED' );
			$this->setRedirect( 'index.php?option=com_ccinvoices&view=items', $msg );
		}
	}
	function cancel()
	{
		$link = 'index.php?option=com_ccinvoices&view=items';
		$this->setRedirect($link);
	}
	public function saveOrderAjax()
	{
		// Get the input
		$pks = $this->input->post->get('cid', array(), 'array');
		$order = $this->input->post->get('order', array(), 'array');

		// Sanitize the input
		JArrayHelper::toInteger($pks);
		JArrayHelper::toInteger($order);

		// Get the model
		$model = $this->getModel('item');

		// Save the ordering
		$return = $model->saveorder($pks, $order);

		if ($return)
		{
			echo "1";
		}

		// Close the application
		JFactory::getApplication()->close();
	}
}
?>