<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

class ccInvoicesControllercontacts extends CCINVOISController
{
    function __construct()
    {
        //Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'contacts');
        }
        $this->item_type = 'contacts';
        parent::__construct();
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'add',	'edit' );
		$this->registerTask( 'new',	'edit' );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'save2new', 'save' );
		$this->registerTask( 'save2copy', 'save' );
		$this->registerTask( 'trash', 'remove' );

    }

	function edit()
	{
		JRequest::setVar( 'view', 'contacts');
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}
	function cancel()
	{
		$link = 'index.php?option=com_ccinvoices&view=contacts';
		$this->setRedirect($link);
	}
	function save()
	{
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$model = $this->getModel('contacts');

		if ($id=$model->store())
		{
			$msg = JText::_( 'CC_CONTACT_SAVED_SUCC' );
		}
		else
		{
			$msg = JText::_( 'CC_CONTACT_SAVED_UNSUCC' );
		}

		$tsk=$post['task'];
		$tsk=str_replace("contacts.","",$tsk);
		if($tsk == 'apply')
		{
			$link 	= 'index.php?option=com_ccinvoices&task=contacts.edit&cid[]='. $id;
		}
        elseif($tsk == 'save2new')
        {
			$msg = JText::_( 'CC_CONTACT_SAVED_SUCC' );
			$link = 'index.php?option=com_ccinvoices&task=contacts.edit';
        }
        elseif($tsk=='save2copy')
        {
			$msg = JText::_( 'CC_CONTACT_SAVED_SUCC');
			$link = "index.php?option=com_ccinvoices&task=contacts.edit&cid[]=".$id."";
        }
		else
		{
			$link = 'index.php?option=com_ccinvoices&view=contacts';
		}
		$this->setRedirect($link, $msg);
	}
	function copy()
	{
		// get the model
		JRequest::checkToken() or jexit( 'Invalid Token' );
				$cid	= JRequest::getVar( 'cid', null, 'post', 'array' );


		$db = JFactory::getDBO();

		foreach($cid as $id)
		{
			$query = "SELECT * FROM #__ccinvoices_contacts WHERE id=".$id."";
			$db->setQuery( $query );
			$rows = $db->loadObject();



			$query = "INSERT INTO #__ccinvoices_contacts ( `name`, `contact`, `contact_number`, `address`, `email`,`tax_id`) VALUES (  ".$db->Quote($rows->name).", ".$db->Quote($rows->contact).",".$db->Quote($rows->contact_number).", ".$db->Quote($rows->address).", ".$db->Quote($rows->email).", ".$db->Quote($rows->tax_id).")";
			$db->setQuery( $query );
			$db->query();
		}
		$msg = JText::_( 'CC_CONTACT_SAVED_SUCC');
		$link = "index.php?option=com_ccinvoices&view=contacts";
		$this->setRedirect($link, $msg);
	}
	function remove()
	{
		// get the model
		$model = $this->getModel('contacts');

		list($boolval,$cantdelmsg)=$model->delete();

		if($boolval)
		{
			if($cantdelmsg=="1")
			{
				JError::raiseWarning(100, JText::_('CC_CONTACTS_CANT_DELETED'));
				$this->setRedirect( 'index.php?option=com_ccinvoices&view=contacts' );
			}
			else
		{
			$msg = JText::_( 'CC_CONTACTS_DELETED' );
				$this->setRedirect( 'index.php?option=com_ccinvoices&view=contacts', $msg );
			}
		}
		else
		{
			$msg = JText::_( 'ERROR_CONTACTS_DELETED' );
			$this->setRedirect( 'index.php?option=com_ccinvoices&view=contacts', $msg );
		}
		// redirect
	}
	function modelassigncontact()
	{
		JRequest::setVar( 'view', 'contacts');
		parent::display();
	}
}
?>