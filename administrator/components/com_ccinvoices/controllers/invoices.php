<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
function versionCompareOnly()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,1);
}
//Controllers
if(versionCompareOnly()>=3)
{
	class CCINVOISINVController extends JControllerLegacy{}
}
else
{
	jimport('joomla.application.component.controller');
	class CCINVOISINVController extends JController{}
}
class ccInvoicesControllerInvoices extends CCINVOISINVController
{
    function __construct()
    {
		//Get View
        if(JRequest::getCmd('view') == '')
        {
            JRequest::setVar('view', 'invoices');
		}
        $this->item_type = 'Invoices';
        parent::__construct();
		$this->registerTask( 'unpublish',	'publish' );
		$this->registerTask( 'add',	'edit' );
		$this->registerTask( 'new',	'edit' );
		$this->registerTask( 'apply', 'save' );
		$this->registerTask( 'save2new', 'save' );
		$this->registerTask( 'save2copy', 'save' );
		$this->registerTask( 'trash', 'remove' );
    }
	public function saveOrderAjax()
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		// Get the arrays from the Request
		$pks   = $this->input->post->get('cid', null, 'array');
		$order = $this->input->post->get('order', null, 'array');
		$originalOrder = explode(',', $this->input->getString('original_order_values'));

		// Make sure something has changed
		if (!($order === $originalOrder)) {
			// Get the model
			$model = $this->getModel();
			// Save the ordering
			$return = $model->saveorder($pks, $order);
			if ($return)
			{
				echo "1";
			}
		}
		// Close the application
		JFactory::getApplication()->close();
	}
	function invoicenumbercheck()
	{
   		$db 	= JFactory::getDBO();
		$query	= "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = ''";
        $db->setQuery($query);
        $invoicenumbercheck = $db->loadResult();
        return $invoicenumbercheck;
   }
	function cancel()
	{
		$link = 'index.php?option=com_ccinvoices&view=invoices';
		$this->setRedirect($link);
	}
	function updateStatusFromCol()
	{
		$db = JFactory::getDBO();
		$invoice_id = JRequest::getVar("inv_id","");
		$status = JRequest::getVar("selectedst","");

		$query	= "SELECT status FROM #__ccinvoices_invoices WHERE id=".$invoice_id."";
	    $db->setQuery($query);
	    $pre_status = $db->loadResult();

		if(trim($pre_status)=="4")
		{
			if($status!=$pre_status)
			{
				$sql = "DELETE FROM #__ccinvoices_payment WHERE inv_id=".$invoice_id;
				$db->setQuery($sql);
				$db->query();
			}
		}

		$query = "UPDATE #__ccinvoices_invoices SET status=".$status." WHERE id=".$invoice_id."";
		$db->setQuery( $query );
		$db->query();

		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

		$query = 'SELECT * from #__ccinvoices_invoices where id ='.$invoice_id;
		$db->setQuery($query);
		$row_inv = $db->loadObject();
		$row=$row_inv;


		$new_inv_id1="0";
		if($row->status == 1)
		{
			$new_inv_id1= "0";
			$sql = "UPDATE #__ccinvoices_invoices SET number= ".$new_inv_id1." WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}else if($row->status != 1 && $row->number == 0)
		{
			$query = 'SELECT invoice_start from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
			$db->setQuery($query);
			$confRow = $db->loadObject();
		    $invoicenumbercheck=ccInvoicesControllerInvoices::invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}
			$sql = "UPDATE #__ccinvoices_invoices SET number= ".$new_inv_id1." WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}





		$custom_invoice_number = $row_inv->custom_invoice_number;
		$inv_status = $row_inv->status;
 		$invoice_format = $config->invoice_format;

		if($invoice_format != "" && $inv_status != "1")
		{
			$query = 'SELECT i.number,c.contact_number from #__ccinvoices_invoices AS i'
					. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id '
					. ' where i.id ='.$invoice_id;
			$db->setQuery($query);
			$invRow = $db->loadObject();
			$custom_inv =  sprintf("%04d", $invRow->number);
			$inv_format["invoicenumber"] = $custom_inv;
			$inv_format["contactnumber"] = $invRow->contact_number;
			$inv_format["dy"] = date("z") + 1;
			$inv_format["dd"] = date("d",time());
			$inv_format["mm"] = date( "m",time());
			$inv_format["yy"] = date("y",time());
			$inv_format["yyyy"] = date("Y",time());


			foreach($inv_format as $inv_formats=>$value)
			{
				$find = "[".$inv_formats."]";
				$replace = $value;
				$invoice_format = str_replace($find,$replace,$invoice_format);
			}

			$sql = "UPDATE #__ccinvoices_invoices SET custom_invoice_number= '".$invoice_format."' WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();

		}
		else
		{
			$sql = "UPDATE #__ccinvoices_invoices SET custom_invoice_number= '' WHERE id=".$invoice_id;
			$db->setQuery($sql);
			$db->query();
		}

		$notify = JRequest::getVar("notify","");
		JRequest::setVar("ajaxvarset",'1');
		JRequest::setVar("filenm",'1');


		if($notify=="Y")
		{
			JRequest::setVar("id",$invoice_id);
			if($status=="2")
			{
				ccInvoicesControllerInvoices::sendInvoice();
			}
			if($status=="3")
			{
				ccInvoicesControllerInvoices::invoiceRem();
			}
		}
	}
    function edit()
	{
		JRequest::setVar( 'view', 'invoices');
		JRequest::setVar( 'layout', 'form'  );
		JRequest::setVar('hidemainmenu', 1);
		parent::display();
	}

	function save()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$model = $this->getModel('invoices');
        $post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
        $resend	= JRequest::getVar( 'resend','');
        $send	= JRequest::getVar( 'send','');
		$reminder	= JRequest::getVar( 'reminder','');
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

        if ($id=$model->store())
        {
        	$msg = JText::_( 'CC_INVOICE_SAVED_SUCC' );
        }
        else
        {
        	$msg = JText::_( 'CC_INVOICE_SAVED_UNSUCC' );
        }
		$tsk=$post['task'];
		$tsk=str_replace("invoices.","",$tsk);
        if($tsk == 'apply')
        {
        	$link 	= 'index.php?option=com_ccinvoices&task=invoices.edit&cid[]='. $id;
        }
        elseif($tsk == 'save2new')
        {
        	$link = 'index.php?option=com_ccinvoices&task=invoices.edit';
        }
        elseif($tsk=='save2copy')
        {
			$msg = JText::_( 'CC_ITEM_COPIED');
			$link 	= 'index.php?option=com_ccinvoices&task=invoices.edit&cid[]='. $id;
        }
        else
        {
        	$link = 'index.php?option=com_ccinvoices&view=invoices';
        }
        if($id > 0)
        {
			$query = 'SELECT custom_invoice_number,status from #__ccinvoices_invoices where id ='.$id;
			$db->setQuery($query);
			$row_inv = $db->loadObject();
			$custom_invoice_number = $row_inv->custom_invoice_number;
			$inv_status = $row_inv->status;
	 		$invoice_format = $config->invoice_format;
			if($invoice_format != "" && $inv_status != "1")
			{
				$query = 'SELECT i.number,c.contact_number from #__ccinvoices_invoices AS i'
						. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id '
						. ' where i.id ='.$id;
				$db->setQuery($query);
				$invRow = $db->loadObject();
				$custom_inv =  sprintf("%04d", $invRow->number);
				$inv_format["invoicenumber"] = $custom_inv;
				$inv_format["contactnumber"] = $invRow->contact_number;
				$inv_format["dy"] = date("z") + 1;
				$inv_format["dd"] = date("d",time());
				$inv_format["mm"] = date( "m",time());
				$inv_format["yy"] = date("y",time());
				$inv_format["yyyy"] = date("Y",time());
				foreach($inv_format as $inv_formats=>$value)
				{
					$find = "[".$inv_formats."]";
					$replace = $value;
					$invoice_format = str_replace($find,$replace,$invoice_format);
				}
				$sql = "UPDATE #__ccinvoices_invoices SET custom_invoice_number= '".$invoice_format."' WHERE id=".$id;
				$db->setQuery($sql);
				$db->query();
			}
			if($reminder=="1" OR $resend=="1" OR $send=="1")
			{
				JRequest::setVar('filenm','1');
			}
			$file_path = $this->createInvoice($id);
			$this->sendEmail($reminder,$resend,$send,$file_path,$id);
			if($reminder=="1" OR $resend=="1" OR $send=="1")
			{
				if($file_path!="")
				{
					jimport('joomla.filesystem.file');
					if(JFile::exists($file_path)) {
						JFile::delete($file_path);
					}
				}
			}
        }

        $this->setRedirect( $link ,$msg);
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function invoicesendsubject($reminder,$number)
	{
		$db = JFactory::getDBO();
		$sql = "SELECT default_email_sub,rem_email_sub FROM #__ccinvoices_configuration WHERE id = 1 LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

   	    //$reminder	= JRequest::getVar( 'reminder');
		$op_val['invoice_number'] = $number;
       	if($reminder == '1')
		{
			if($config->rem_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEREMINDER_SUB' );
		}else
		{
				$invoicesend_sub = $config->rem_email_sub;
			}
		}else
		{
			if($config->default_email_sub == '')
			{
			$invoicesend_sub= JText::_( 'CC_INVOICEEMAIL_SUB' );
			}else
			{
				$invoicesend_sub = $config->default_email_sub;
			}
		}
		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$invoicesend_sub = str_replace($find,$replace,$invoicesend_sub);
		}
		return $invoicesend_sub;
	}

	function sendEmail($reminder,$resend,$send,$file_path,$id)
	{
		global $mainframe;
		$db = JFactory::getDBO();
		//$model = $this->getModel('invoices');
		$query = 'SELECT i.*,c.tax_id,c.name,c.contact,c.email'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();

		if($send != '' || $reminder !='' || $resend !='')
		{
			$invoice_send_date	= date("Y-m-d");
			$sql = "UPDATE #__ccinvoices_invoices SET invoice_sent_date = '".$invoice_send_date."' WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();

			$sql = "UPDATE #__ccinvoices_invoices SET communication=1 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
		}

		//$MailFrom	= $mainframe->getCfg('mailfrom');
		//$FromName	= $mainframe->getCfg('fromname');
		$MailFrom	= $config->company_email;
		$FromName	= $config->user_company;
		if($config->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC = explode(",",$config->email_cc);
		}
		if($config->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC = explode(",",$config->email_bcc);
		}

		if($reminder == '1')
		{
			if($config->rem_email_sub == '')
			{
				$invoicesend_sub= JText::_( 'CC_INVOICEREMINDER_SUB' );
			}
			else
			{
					$invoicesend_sub = $config->rem_email_sub;
			}
		}
		else
		{
			if($config->default_email_sub == '')
			{
				$invoicesend_sub= JText::_( 'CC_INVOICEEMAIL_SUB' );
			}
			else
			{
				$invoicesend_sub = $config->default_email_sub;
			}
		}

		//GETTING PAYMENT METHOD NAMES
		$pluginnames=ccInvoicesControllerInvoices::getPaymentMethodsName();
		$emails = explode(",",$rows->email);
		if($reminder == '1')
		{
			$invoicesend_sub=ccInvoicesControllerInvoices::gettemplatelayout($id,5,$invoicesend_sub);
			$invoicesendmsg=ccInvoicesControllerInvoices::gettemplatelayout($id,5,$config->default_email_rem);
			$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
			JFactory::getMailer()->sendMail($MailFrom, $FromName,$emails, $invoicesend_sub, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);

		}else if($resend == '1')
		{
			$invoicesend_sub=ccInvoicesControllerInvoices::gettemplatelayout($id,4,$invoicesend_sub);
			$invoicesendmsg=ccInvoicesControllerInvoices::gettemplatelayout($id,4,$config->default_email);
			$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
			JFactory::getMailer()->sendMail($MailFrom, $FromName,$emails, $invoicesend_sub, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);
		}else if($send == '1')
		{
			$invoicesend_sub=ccInvoicesControllerInvoices::gettemplatelayout($id,4,$invoicesend_sub);
			$invoicesendmsg=ccInvoicesControllerInvoices::gettemplatelayout($id,4,$config->default_email);
			$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);
			JFactory::getMailer()->sendMail($MailFrom, $FromName,$emails, $invoicesend_sub, $invoicesendmsg ,1,$EmailCC,$EmailBCC,$file_path);
		}
	}
	function getPaymentMethodsName()
	{
		$plugins_name= JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher = JDispatcher::getInstance();
		$methodnames = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$paymentmethodname="";
		if(count($methodnames)>0)
		{
			$i=0;
			foreach($methodnames as $pluginname)
			{
				if($i==count($methodnames)-2)
				{
					$paymentmethodname.=$pluginname["payment_method_note"]." or ";
				}
				else
				{
					$paymentmethodname.=$pluginname["payment_method_note"].", ";
				}
				$i=$i+1;
			}
		}
		$paymentmethodname=substr($paymentmethodname,0,strlen($paymentmethodname)-2);
		return $paymentmethodname;
	}
	function createInvoice($id)
	{

		$mainframe = JFactory::getApplication();
		$sitename=$mainframe->getCfg("sitename");
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
        require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS."assets".DS."tcpdf".DS.'tcpdf.php');
        require_once(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS."assets".DS."tcpdf".DS."config".DS."lang".DS.'eng.php');


        $pdf = new ccInvoicesTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($sitename);
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords('Invoice');

		$query = "SELECT *  FROM #__ccinvoices_configuration WHERE id = 1";
		$db->setQuery( $query);
		$confrows = $db->loadObject();

		$topmargin=$confrows->invoice_top_margin;
		$leftmargin=$confrows->invoice_left_margin;
		$rightmargin=$confrows->invoice_right_margin;
		$bottommargin=$confrows->invoice_bottom_margin;
		$invoicemode=$confrows->invoice_mode;

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
		$pdf->SetMargins($leftmargin, $topmargin, $rightmargin);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
		$pdf->SetAutoPageBreak(TRUE,$bottommargin);
		//set auto page breaks
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$l = '';
		$pdf->setLanguageArray($l);

		if($confrows->fontname_invoice=="1")
			$pdf->SetFont('times', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="2")
			$pdf->SetFont('arialunicid0', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="3")
			$pdf->SetFont('Verdana', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="4")
			$pdf->SetFont('helvetica', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="5")
			$pdf->SetFont('FreeSans', '', $confrows->pdffontsize_invoice);

		if($invoicemode=="0")
			$pdf->AddPage('P', 'A4');
		elseif($invoicemode=="1")
			$pdf->AddPage('L', 'A4');
		else
			$pdf->AddPage();

        //Background image process
        if($confrows->backgimage=="1")
        {
        	$backImgPath=JPATH_ROOT.DS.'media'.DS.'com_ccinvoices'.DS.'logo'.DS.$confrows->pdfimagefile;
			$pdf->Image($backImgPath, $confrows->pdfimagexpos, $confrows->pdfimageypos, $confrows->pdfimagewidth, $confrows->pdfimageheight, '', '', '', false, 300, '', false, false, 0);
        }
		$template=ccInvoicesControllerInvoices::gettemplatelayout($id);

        $v=$pdf->writeHTML($template, true, false, false, false, '');

		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if($conf->invoice_format != "")
		{
		$file_name = trim(ccInvoicesControllerInvoices::getInvoiceNumberFormat($invRow->id))."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}else
		{
			$file_name = $invRow->number."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}
		$filenm=JRequest::getVar('filenm','0');
		if($filenm=="1")
		{
			if($conf->invoice_format != "")
			{
				$file_name = ccInvoicesControllerInvoices::getInvoiceNumberFormat($invRow->id).".pdf";
			}else
			{
				$file_name = $invRow->number.".pdf";
			}
		}
        $file_path = JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS."assets".DS.'files'.DS.'pdf'.DS.$file_name;
        $pdf->Output($file_path, 'F');
		return $file_path;
	}

	function downloadInvoice()
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$model = $this->getModel('invoices');
		$file_path = $this->createInvoice($id);
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if($conf->invoice_format != "")
		{
		$file_name = trim(ccInvoicesControllerInvoices::getInvoiceNumberFormat($invRow->id))."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}else
		{
			$file_name = $invRow->number."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}

		$sourcefile=JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."pdf".DS.$file_name;
		$filedsdf = fopen($sourcefile,"r");
		$cont=file_get_contents($sourcefile);
		$charfound=strrpos($file_name,"_");
		$file_n=substr($file_name,0,$charfound).substr($file_name,strlen($file_name)-4);

		define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."pdf".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'pdf' => 'application/pdf',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$file_n\"");
		echo $cont;
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}
	}
	function getAdditionalColumns(){
		$db = JFactory::getDBO();
		$sql = "DESCRIBE #__users ";
		$db->setQuery($sql);
		$result = $db->loadAssocList();
		$array_all_columns = array();
		foreach($result as $key=>$value){
			$array_all_columns[] = $value['Field'];
		}
		return $array_all_columns;
	}
	// return all types by users
	function getUserType(){
	    $db = JFactory::getDBO();
		$sql = "SELECT DISTINCT usertype
		        FROM #__users
                WHERE usertype <> ''";
		$db->setQuery($sql);
		$result = $db->loadAssocList();
		return $result;
	}
	function convertTextcharToUppercase($line)
	{
		$tmparray="";
		$newstr="";
		$tmparray=explode(",",$line);
		foreach($tmparray as $tmpstr)
		{
			$newstr.=strtoupper(substr($tmpstr,0,1)).substr($tmpstr,1).",";
		}
		return $newstr;
	}
	function batchDownloadCSV()
	{

		$id = JRequest::getInt("id","");
		$oid = JRequest::getVar('cid', array(0), 'method', 'array');
		JArrayHelper::toInteger($oid, array(0));
		sort($oid ,$sort_flags = SORT_NUMERIC);
		$database = JFactory::getDBO();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
		$database->setQuery($query);
		$invRow = $database->loadObject();
		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if(count($oid) == 1)
		{
			$file_name = "invoice_".$invRow->number."_".strtotime(date("d-M-Y")).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".csv";
		}else if(count($oid) > 1)
		{
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
			$database->setQuery($query);
			$first_inv_number= $database->loadResult();
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[count($oid)-1]." LIMIT 1";
			$database->setQuery($query);
			$last_inv_number= $database->loadResult();
			$file_name = "invoice_batch_".$first_inv_number."_".$last_inv_number."_".strtotime(date("d-M-Y")).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".csv";
		}

		$file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.'files'.DS.'csv'.DS.$file_name;
/*		$list = array (
		    'aaa,bbb,ccc,dddd',
		    '123,456,789',
		    '"aaa","bbb"'
		);
*/
		$fp = fopen($file_path, 'w');
		$query = "SELECT name,id FROM  #__users ORDER BY id";
		$database->setquery($query);
		$rows = $database->loadObjectList();

		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 2 LIMIT 1";
	    $database->setQuery($query);
		$template = strip_tags($database->loadResult());
		/*
		$template = str_replace("\r\n","",$template);
		$template = str_replace('"','\"',$template);
		echo $template;exit;
*/
        $tmp_x = 0;
        $model = $this->getModel('invoices');
        $newline="";
        $kp=0;
        $tempid="";
        $countchar="";
        $i=0;
        $tempnum="";
        $countchar[-1]="0";
        $tempnum=1;
		foreach($oid as $id)
		{
			$query	= "SELECT * FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
			$database->setQuery($query);
			$rows= $database->loadObject();
			$countchar[$i]=substr_count($rows->pname,"|");
			if($countchar[$i]>$countchar[$i-1])
			{
				$tempnum=$countchar[$i];
				$tempid=$id;
			}
			else
			{
				$tempid=$id;
			}
			$i++;
		}

		list($line,$template,$productarray) = $model->gettemplateCSV($tempid,"0");
		$template=strip_tags($template);
		$order = array("\r\n", "\n", "\r","{","}");
		$line=strip_tags(str_replace($order, "", $line));
		$line=strip_tags(str_replace("_", " ", $line));
		$line=$this->convertTextcharToUppercase($line);
		fputcsv($fp, @split(',', $line));
		foreach($oid as $id)
		{
			list($line,$template,$productarray) = $model->gettemplateCSV($id,$tempnum);
			$order   = array("\r\n", "\n", "\r");
			$replace = '';
			$line = strip_tags(str_replace($order, $replace, $line));
			$line=str_replace(",","|",$line);
			$line=str_replace("`",",",$line);

			$linenew=array();
			$linenew[]=explode("|",$line);
			$finalarray="";
			$pos="";
			foreach($linenew as $record)
			{
				for($i=0;$i<=count($record)-1;$i++)
				{
					$tmp=strpos($record[$i],"{");
					$tmp1=strpos($record[$i],"}");
					if(trim($tmp)!="" AND trim($tmp1)!="")
					{
						$pos=$i;
						break;
					}
				}
			}
			$k=0;
			if(trim($pos)=="")
			{

				foreach($linenew as $record)
				{
					fputcsv($fp, $record);
				}
			}
			else
			{

				foreach($productarray as $product)
				{
					for($f=0;$f<=count($linenew[0])-1;$f++)
					{
						if($f>=0 AND $f<$pos)
						{
							$finalarray[$k][$f]=$linenew[0][$f];
						}
						elseif($f>=$pos AND $f<($pos+count($product)))
						{
							for($t=0;$t<=count($product)-1;$t++)
							{
								$product[$t]=str_replace("`",",",$product[$t]);
								$finalarray[$k][$t+$pos]=$product[$t];
							}
							$f=$t+$pos-1;
						}
						else
						{
							$finalarray[$k][$f]=$linenew[0][$f];
						}
					}
					$k++;
				}
				foreach($finalarray as $record)
				{
					fputcsv($fp, $record);
				}
			}
		}
		fclose($fp);

		$sourcefile=JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."csv".DS.$file_name;
		$cont="";
		$filedsdf = fopen($sourcefile,"r");
		$cont=file_get_contents($sourcefile);
		$charfound=strrpos($file_name,"_");
		$file_n=substr($file_name,0,$charfound).substr($file_name,strlen($file_name)-4);
		fclose($filedsdf);

     	define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."csv".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'csv' => 'application/csv',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$file_n\"");
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		@unlink(JPATH_ROOT.DS.'administrator'.DS.'components'.DS.'com_ccinvoices'.DS.'assets'.DS.'files'.DS.'csv'.DS.$file_name);
		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}



	}


	function batchDownloadPDF()
	{
		$mainframe = JFactory::getApplication();
		$sitename=$mainframe->getCfg("sitename");
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$oid = JRequest::getVar('cid', array(0), 'method', 'array');
		JArrayHelper::toInteger($oid, array(0));
		sort($oid ,$sort_flags = SORT_NUMERIC);
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS.'tcpdf.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS."config".DS."lang".DS.'eng.php');
		$model = $this->getModel('invoices');
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
	    $pdf = new ccInvoicesTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($sitename);
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords('Invoice');

		$query = "SELECT *  FROM #__ccinvoices_configuration WHERE id = 1";
		$db->setQuery( $query);
		$confrows = $db->loadObject();

		$topmargin=$confrows->invoice_top_margin;
		$leftmargin=$confrows->invoice_left_margin;
		$rightmargin=$confrows->invoice_right_margin;
		$bottommargin=$confrows->invoice_bottom_margin;
		$invoicemode=$confrows->invoice_mode;

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
		$pdf->SetMargins($leftmargin, $topmargin, $rightmargin);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE,$bottommargin);
		//set auto page breaks
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$l = '';
		$pdf->setLanguageArray($l);

		if($confrows->fontname_invoice=="1")
			$pdf->SetFont('times', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="2")
			$pdf->SetFont('arialunicid0', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="3")
			$pdf->SetFont('Verdana', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="4")
			$pdf->SetFont('helvetica', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="5")
			$pdf->SetFont('FreeSans', '', $confrows->pdffontsize_invoice);
        $model = $this->getModel('invoices');

		foreach($oid as $id)
		{
			$template = '';
			if($invoicemode=="0")
				$pdf->AddPage('P', 'A4');
			elseif($invoicemode=="1")
				$pdf->AddPage('L', 'A4');
			else
				$pdf->AddPage();
	        //Background image process
	        if($confrows->backgimage=="1")
	        {
	        	$backImgPath=JPATH_ROOT.DS.'media'.DS.'com_ccinvoices'.DS.'logo'.DS.$confrows->pdfimagefile;
				$pdf->Image($backImgPath, $confrows->pdfimagexpos, $confrows->pdfimageypos, $confrows->pdfimagewidth, $confrows->pdfimageheight, '', '', '', false, 300, '', false, false, 0);
	        }
			$template=ccInvoicesControllerInvoices::gettemplatelayout($id);
	        $pdf->writeHTML($template, true, false, false, false, '');
			$pdf->lastPage();
		}

		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		$file_n="";
		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if(count($oid) == 1)
		{
			$file_name = "invoice_".$invRow->number."_".strtotime(date("d-M-Y")).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}else if(count($oid) > 1)
		{
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[0]." LIMIT 1";
			$db->setQuery($query);
			$first_inv_number= $db->loadResult();
			$query	= "SELECT number FROM #__ccinvoices_invoices where id = ".$oid[count($oid)-1]." LIMIT 1";
			$db->setQuery($query);
			$last_inv_number= $db->loadResult();

			$file_name = "invoice_batch_".$first_inv_number."_".$last_inv_number."_".strtotime(date("d-M-Y")).ord(base64_encode("pdf")).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}

/*
		if($conf->invoice_format != "")
		{
			$file_name = $model->getInvoiceNumberFormat($invRow->number).".pdf";
		}else
		{
			$file_name = $invRow->number.".pdf";
		}
*/
        $file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.'files'.DS.'pdf'.DS.$file_name;
        $pdf->Output($file_path, 'F');


		$sourcefile=JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."pdf".DS.$file_name;
		$filedsdf = fopen($sourcefile,"r");
		$cont=file_get_contents($sourcefile);
		$charfound=strrpos($file_name,"_");
		$file_n=substr($file_name,0,$charfound).substr($file_name,strlen($file_name)-4);


		define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."pdf".DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'pdf' => 'application/pdf',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$file_n\"");
		echo $cont;
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}
	}
	function deleteInvoice()
		{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");

		$sql = "DELETE FROM #__ccinvoices_invoices WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();

		$msg = JText::_("CC_INVOICE_DELETED_SUCCESS");
		$link = "index.php?option=com_ccinvoices&view=invoices";
		$this->setRedirect( $link ,$msg);
		}

	function remPayInvoice()
		{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();

		if(strtotime(date("%y-%m-%d",time())) > strtotime($invRow->duedate))
		{
			$sql = "UPDATE #__ccinvoices_invoices SET status=3 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
			$msg = JText::_("CC_INVOICE_STATUS_LATE");
		}else
		{
			$sql = "UPDATE #__ccinvoices_invoices SET status=2 WHERE id=".$id;
			$db->setQuery($sql);
			$db->query();
			$msg = JText::_("CC_INVOICE_STATUS_OPEN");
		}

		$link = "index.php?option=com_ccinvoices&view=invoices";
		$this->setRedirect( $link ,$msg);
	}
	function payInvoice()
        {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$send = "0";
		$resend = "0";
		$reminder = "0";
		$sql = "UPDATE #__ccinvoices_invoices SET status=4 WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();
		$link = "index.php?option=com_ccinvoices&view=invoices";
		$msg = JText::_("CC_INVOICE_PAID_SUCCESS");
		$this->setRedirect( $link ,$msg);
        }

	function invoiceRem()
    {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");

		$ajaxvarset =JRequest::getInt("ajaxvarset","0");
		$send = "0";
		$resend = "0";
		$reminder = "1";
		$file_path = ccInvoicesControllerInvoices::createInvoice($id);
		ccInvoicesControllerInvoices::sendEmail($reminder,$resend,$send,$file_path,$id);
		if($file_path!="")
		{
			jimport('joomla.filesystem.file');
			if(JFile::exists($file_path)) {
				JFile::delete($file_path);
			}
		}
		$link = "index.php?option=com_ccinvoices&view=invoices";
		$msg = JText::_("CC_SEND_INVOICE_REM_TO_CONTACT");
		if($ajaxvarset!="1")
		{
			ccInvoicesControllerInvoices::setRedirect( $link ,$msg);
		}
    }

	function reSendInvoice()
        {
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");
		$ajaxvarset =JRequest::getInt("ajaxvarset","0");
		$send = "0";
		$resend = "1";
		$reminder = "0";

		$file_path = ccInvoicesControllerInvoices::createInvoice($id);
		ccInvoicesControllerInvoices::sendEmail($reminder,$resend,$send,$file_path,$id);
		if($file_path!="")
		{
			jimport('joomla.filesystem.file');
			if(JFile::exists($file_path)) {
				JFile::delete($file_path);
			}
		}
		$link = "index.php?option=com_ccinvoices&view=invoices";
		$msg = JText::_("CC_RESEND_INVOICE_TO_CONTACT");
		if($ajaxvarset!="1")
		{
			ccInvoicesControllerInvoices::setRedirect( $link ,$msg);
		}
        }

	function viewInvoice()
        {
        	?>
				<script language="javascript">
				function actioncolor(tmp)
				{
					document.getElementById(tmp).style.color='blue';
				}
				function actioncolor_change(tmp)
				{
					document.getElementById(tmp).style.color='gray';

				}
				</script>
			<?php
		$id = JRequest::getInt("id","");
		$db = JFactory::getDBO();

		//next invoice
		$query = "SELECT id FROM #__ccinvoices_invoices WHERE id >".$id." order by id asc LIMIT 1";
		$db->setQuery( $query );
		$next_order = $db->loadResult();
		//==========================
		//previous invoice
		$query = "SELECT id FROM #__ccinvoices_invoices WHERE id <".$id." order by id desc LIMIT 1";
		$db->setQuery( $query );
		$prev_order = $db->loadResult();
		//==========================
		?>
			<table border="0" width="100%">
				<tr>
					<td>
		<?php
		$link_prev_order = "index.php?option=com_ccinvoices&task=invoices.viewInvoice&id=".$prev_order."&tmpl=component";
		if(trim($prev_order)!="")
		{
			$prev_invNumber=ccInvoicesControllerInvoices::getInvoiceNumberFormat($prev_order);
			?>
				<div  style="width:250px;display:block;" ><a title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>" class="modal"  href="<?php echo $link_prev_order; ?>" rel="{handler:'iframe',size:{x:window.getSize().scrollSize.x-80, y: window.getSize().size.y-80}, onShow:$('sbox-window').setStyles({'padding': 0})}" style="color:gray;font-size:12px;" ><div id="divblok1" onmouseover="actioncolor(this.id)" onmousemove="actioncolor(this.id)" onmouseout="actioncolor_change(this.id)"><?php echo sprintf (  JText::_('CC_PREV_ORDER'),$prev_invNumber); ?></div></a></div>
			<?php
		}
		?>
				</td>
				<td>
		<?php
		$link_next_order = "index.php?option=com_ccinvoices&task=invoices.viewInvoice&id=".$next_order."&tmpl=component";
		if(trim($next_order)!="")
		{
			$next_invNumber=ccInvoicesControllerInvoices::getInvoiceNumberFormat($next_order);
			?>
				<div style="float:right;display:block;" ><a title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>" class="modal"  href="<?php echo $link_next_order; ?>" rel="{handler:'iframe',size:{x:window.getSize().scrollSize.x-80, y: window.getSize().size.y-80}, onShow:$('sbox-window').setStyles({'padding': 0})}" style="color:gray;font-size:12px;" ><div id="divblok2" onmouseover="actioncolor(this.id)" onmousemove="actioncolor(this.id)" onmouseout="actioncolor_change(this.id)"><?php echo sprintf (  JText::_('CC_NEXT_ORDER'),$next_invNumber); ?></div></a></div>
			<?php
		}
		?>
					</td>
				</tr>
			</table>
		<?php
		$model = $this->getModel('invoices');
		$template=ccInvoicesControllerInvoices::gettemplatelayout($id);
			if($template == "0")
			{
				echo JText::_("CC_NOT_AUTHO");
				return;
			}
		echo $template;
        }

	function sendInvoice()
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","");

		$ajaxvarset =JRequest::getInt("ajaxvarset","0");

		$send = "1";
		$resend = "0";
		$reminder = "0";
		$file_path = ccInvoicesControllerInvoices::createInvoice($id);

		//echo $reminder.",".$send;exit;
		ccInvoicesControllerInvoices::sendEmail($reminder,$resend,$send,$file_path,$id);
		if($file_path!="")
		{
			jimport('joomla.filesystem.file');
			if(JFile::exists($file_path)) {
				JFile::delete($file_path);
			}
		}
		$link = "index.php?option=com_ccinvoices&view=invoices";
		$msg = JText::_("CC_SEND_INVOICE_TO_CONTACT");
		if($ajaxvarset!="1")
		{
        	ccInvoicesControllerInvoices::setRedirect( $link ,$msg);
		}
	}

	function searchcontactname()
	{
       JRequest::setVar( 'view', 'invoices' );
       JRequest::setVar( 'layout', 'invoice_contact' );
       parent::display();
       exit;
	}
	function getinvoicecontact()
	{
   		JRequest::setVar( 'view', 'invoices' );
    	JRequest::setVar( 'layout', 'getinvoicecontact' );
    	parent::display();
    	exit;
	}
	function updateinvoicecontact()
	{
	   	$model = $this->getModel('invoices');
	   	$updateinvoicecontact=$model->updateinvoicecontact();
		exit;
	}

   	function copy()
	{
		// Check for request forgeries
		JRequest::checkToken() or jexit( 'Invalid Token' );
		$this->setRedirect( 'index.php?option=com_ccinvoices' );
		$cid	= JRequest::getVar( 'cid', null, 'post', 'array' );
		$db		= JFactory::getDBO();
		JTable :: addIncludePath(JPATH_ADMINISTRATOR . DS . 'components' . DS . 'com_ccinvoices' . DS . 'tables');
		$query = 'SELECT default_due_days from #__ccinvoices_configuration where id = 1 LIMIT 1 ';
		$db->setQuery($query);
		$confRow = $db->loadObject();

		$table	= JTable::getInstance('invoices', 'Table');
		$user	= JFactory::getUser();
		$n		= count( $cid );
	   	$model = $this->getModel('invoices');
		if ($n > 0)
		{
			foreach ($cid as $id)
			{
				if ($table->load( (int)$id ))
				{
					$table->id			= 0;
					//$table->number			= 'Copy of ' . $table->number;
					$table->number			= 0;
//					$table->amount			= $table->amount;
//					$table->client		= $table->client;
					$table->tax			= $table->tax;
					$table->status 			= 1;
					$table->numbercheck 			= $table->numbercheck;
					$table->invoice_sent_date 			= '0000-00-00';
					$table->note 			= $table->note;
					$table->contact_id 			= $table->contact_id;
				//	$table->custom_invoice_number 			= $table->custom_invoice_number;
					$table->custom_invoice_number 			= $model->getInvoiceNumberFormat("0");
					$table->invoice_date	= date("Y-m-d");
					$defDueDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") + $confRow->default_due_days, date("Y")));
					$table->duedate = $defDueDate;
					$table->communication = 0;
					if (!$table->store())
					{
						return JError::raiseWarning( $table->getError() );
					}
				}
				else
				{
					return JError::raiseWarning( 500, $table->getError() );
				}
			}
		}
		else
		{
			return JError::raiseWarning( 500, JText::_( 'CC_NO_ITEMS_SELECTED' ) );
		}
		$this->setMessage( JText::sprintf(  JText::_( 'CC_ITEM_COPIED' ) , $n ) );
	}

	function remove()
	{
		// get the model
		$model = $this->getModel('invoices');
		if($model->delete())
		{
			$msg = JText::_( 'CC_INVOICE_DETAILS_DELETED' );
		}
		else
		{
			$msg = JText::_( 'CC_ERROR_INVOICE_DELET' );
		}
		$this->setRedirect( 'index.php?option=com_ccinvoices' ,$msg);
	}

	function invoiceforcontact()
	{
		JRequest::setVar( 'view', 'invoices' );
    	JRequest::setVar( 'layout', 'contactform' );
    	parent::display();
	}
	function printInv()
	{
		JRequest::setVar( 'view', 'print');
		parent::display();
	}
	function getItemsTemplate()
	{
		$db = JFactory::getDBO();
		$query = 'SELECT items_template from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		return $db->loadResult();
	}
	function getPDFLayout()
	{
		$db = JFactory::getDBO();
		$query = 'SELECT pdf_layout from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		if(trim($db->loadResult())=="1")
		{
			return true;
		}
		else
			return false;
	}
	function gettemplatelayout($id,$templateid='',$subOrBody='')
	{

		jimport("joomla.filesystem.file");
		$db 	= JFactory::getDBO();
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$template='';
		if($templateid=='')
		{
			$query	= "SELECT invoice_template FROM #__ccinvoices_templates where id = 1 LIMIT 1";
		    $db->setQuery($query);
			$template =$db->loadResult();
		}
		else
		{
			$template=$subOrBody;
		}
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id=".$id." LIMIT 1";
	    $db->setQuery($query);
		$invRow =$db->loadObject();
		if(count($invRow)<=0)
		{
			return 0;
		}
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();

		$query = 'SELECT i.*,c.tax_id,c.name,c.contact,c.address,c.email,c.contact_number'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
		$itemid= explode("|#$|",$rows->item_id);
        $quantity= explode("|#$|",$rows->quantity);
		$pname=explode("|#$|",$rows->pname);
		$itemdesc= explode("|#$|",$rows->item_description);
		$amount=explode("|#$|",$rows->price);
		$tax=explode("|#$|",$rows->tax);
		$itemids="";
		$quant='';
		$pnames='';
		$itemdescs="";
		$amounts='';
        $taxes='';
        $subTotal='';
        $tax_percent = '';
        $breaka="<br/>";
        $price = '';
		$totalPrice = '';
		$excl_tax = '';
		$item_total_excl_tax = '';
		$invoice_total_excl_tax = '';
		$amt_tmp1 = '';
		$subto1 = '';
		$invoice_subtotal = '';
		$product_total_price = '';
		$inv_subtotal_amount = '';
		$op_val['product_total_price'] = '';
		$op_val['invoice_subtotal_amount'] = '';
		$order_tax_total_split_tmp = '';
		$tax_rate_values = array();
		$itemPriceIncTax = array();
		$itPriceIncTax='';
		$inv_tax_total = '';
        $invoice_discount_price = 0;
        $taskIDNT=JRequest::getVar('task','');
		$generatedItemTemplate='';

		if(ccInvoicesControllerInvoices::getPDFLayout())
		{
			for($i=0;$i<count($quantity);$i++)
			{
				$quant= $quantity[$i];
				$itemids=@$itemid[$i];
				$pnames=$pname[$i];
				$itemdescs=@$itemdesc[$i];

				//Discount Calc Ends
				$amounts= ccInvoicesControllerInvoices::changeCurrencyFormat($amount[$i]);
				$product_total_price += $amount[$i] * $quantity[$i];
				if($rows->discount > 0)
				{
					$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
					$inv_subtotal_amount += $disc_amt* $quantity[$i];

				}else
				{
					$inv_subtotal_amount += $amount[$i] * $quantity[$i];
				}
				/*$price += $amount[$i] * $quantity[$i];
				$price_tmp = $amount[$i] * $quantity[$i];
				$totalPrice += $price_tmp * $tax[$i] / 100;
	            */
	            $discount_price = "0";
				if($rows->discount > 0)
				{
					$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
					$amt_tmp1 = $disc_amt * $tax[$i] / 100;
					$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
					$discount_price += $subto1;
				}else
				{
					$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
					$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
					$discount_price += $subto1;
				}

				$amt_tmp = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100;
				$itemPriceIncTax[$i]=$amount[$i]+$amt_tmp;
				$itPriceIncTax=ccInvoicesControllerInvoices::changeCurrencyFormat($itemPriceIncTax[$i]);
				$taxes= ccInvoicesControllerInvoices::changeCurrencyFormat($amt_tmp);
				$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
				$invoice_subtotal += $subto;
				$subTotal= ccInvoicesControllerInvoices::changeCurrencyFormat($subto);
				//{item_total_excl_tax}
				$excl_tax = $amount[$i]* $quantity[$i];
				$invoice_total_excl_tax += $excl_tax;
				$item_total_excl_tax= ccInvoicesControllerInvoices::changeCurrencyFormat($excl_tax);
				if($tax[$i] == '')
				{
					$tax_percent = "0%";
				}else
				{
					$tax_percent = $tax[$i]."%";
				}

				$tmp = '';
				$tax_rate = $tax[$i];
				$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
				$inv_tax_total +=$product_tax_amt;
				if(count($tax_rate_values) == 0)
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}else
				{
					if (array_key_exists($tax_rate, $tax_rate_values))
					{
						$tmp = $tax_rate_values[$tax_rate];
					    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
					}else
					{
						$tax_rate_values[$tax_rate] = $product_tax_amt;
					}
				}

				$generatedItemTemplate.=ccInvoicesControllerInvoices::getItemsTemplate();

				$op_Itval['item_id']=$itemids;
		       	$op_Itval['item_quantity'] = $quant;
			    $op_Itval['item_name'] = $pnames;
			    $op_Itval['item_description']=$itemdescs;
				$op_Itval['item_amount'] = $amounts;
				$op_Itval['item_price_excl_tax'] = $amounts;
				$op_Itval['item_price_incl_tax'] = $itPriceIncTax;
				$op_Itval['product_tax'] = $taxes;

				$op_Itval['item_tax_amount'] = $taxes;
				$op_Itval['item_tax'] = $taxes;
				$op_Itval['item_total_incl_tax'] = $subTotal;
				$op_Itval['item_total_excl_tax'] = $item_total_excl_tax;
				$op_Itval['tax_percentage'] = $tax_percent;
				$op_Itval['item_tax_percentage'] = $tax_percent;

				foreach($op_Itval as $op_vals=>$value)
				{
					$find = "{".$op_vals."}";
					$replace = $value;
					$generatedItemTemplate = str_replace(trim($find),trim($replace),$generatedItemTemplate);
				}
			}



			ksort($tax_rate_values);
			foreach($tax_rate_values as $key=>$value)
			{
				$order_tax_total_split_tmp .= ccInvoicesControllerInvoices::changeCurrencyFormat($value)."&nbsp;(".$key."%)&nbsp;<br/>";
			}
		    //{order_tax_total_split}
		    $orderitem_tax_total = ccInvoicesControllerInvoices::changeCurrencyFormat($inv_tax_total);
		    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
		    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
		    $order_tax_total_split .= $order_tax_total_split_tmp;
		    $op_val['tax_total_split'] = $order_tax_total_split;
		    $op_val['invoice_tax_total_split'] = $order_tax_total_split;
			//print_r ($tax_rate_values);exit;
			$post['quantity'] =$quant;
			$post['pname'] =$pnames;
			$post['price'] =$amounts;
			$post['tax'] =$taxes;
	        /* contact values */
			$op_val['invoice_note'] = $rows->note;
			//GETTING PAYMENT METHOD NAMES
			$paymentmethodnames=ccInvoicesControllerInvoices::getPaymentMethodsName();

			$op_val['payment_methods']= $paymentmethodnames;
	        $op_val['contact_name'] = $rows->name;
	        $op_val['contact_number'] = $rows->contact_number;
			$op_val['contact'] = $rows->contact;
			$op_val['contact_person'] = $rows->contact;
			$contact_address = str_replace("\n", "<br/>", $rows->address);
			$op_val['contact_address'] = $contact_address;

			$status_text="";

			if($rows->status=="1")
				$status_text=JText::_( 'CC_CONCEPT');
			if($rows->status=="2")
				 $status_text=JText::_( 'CC_OPEN');
			if($rows->status=="3")
				 $status_text=JText::_( 'CC_LATE');
			if($rows->status=="4")
				 $status_text=JText::_( 'CC_PAID');

			$op_val['invoice_status']=$status_text;

			$op_val['contact_taxid']= $rows->tax_id;
			$op_val['contact_tax_id']= $rows->tax_id;


			$op_val['contact_email'] = $rows->email;

		    $op_val['invoice_number'] = ccInvoicesControllerInvoices::getInvoiceNumberFormat($rows->id);

			$op_val['invoice_date'] = ccInvoicesControllerInvoices::dateChangeFormat($rows->invoice_date,$conf->date_format);
			$op_val['invoice_due_date'] = ccInvoicesControllerInvoices::dateChangeFormat( $rows->duedate,$conf->date_format);
			if(strtotime($invRow->invoice_sent_date) == '' OR trim($invRow->invoice_sent_date)=="0000-00-00" OR trim($invRow->invoice_sent_date)=="11-30-99" OR trim($invRow->invoice_sent_date)=="30-11-99")
			{
				$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
			}
			else
			{
			$op_val['invoice_sent_date'] = ccInvoicesControllerInvoices::dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
			}





			if($rows->discount == '' OR $rows->discount == '0')
			{
				$op_val['invoice_discount'] = '';
				$op_val['invoice_discount_new']='';
				$op_val['invoice_discount_excl_tax']='';
				$op_val['invoice_subtotal_with_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
				$op_val['invoice_subtotal_incl_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
			}else
			{

				$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
				$op_val['invoice_discount_new']=ccInvoicesControllerInvoices::changeCurrencyFormat(($product_total_price*$rows->discount)/100);
				$op_val['invoice_discount_excl_tax']=ccInvoicesControllerInvoices::changeCurrencyFormat(($product_total_price*$rows->discount)/100);
				$op_val['invoice_subtotal_with_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price-($product_total_price*$rows->discount)/100);
				$op_val['invoice_subtotal_incl_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price-($product_total_price*$rows->discount)/100);
			}
			$op_val['product_total_price'] = ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
			$op_val['invoice_subtotal_amount'] = ccInvoicesControllerInvoices::changeCurrencyFormat($inv_subtotal_amount);
			$invoice_discount_price=$invoice_subtotal;
			$op_val['invoice_discount_price']=number_format($invoice_discount_price * $rows->discount /100,2);
			$op_val['invoice_discount_amount']=number_format($invoice_discount_price * $rows->discount /100,2);

			$op_val['invoice_tax'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->totaltax);
			$op_val['invoice_tax_total'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->totaltax);

			$op_val['invoice_total'] = '';
			$op_val['invoice_total_excl_tax'] = '';
			$op_val['invoice_total'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->total);
		    $op_val['invoice_total_excl_tax'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_total_excl_tax);
		    $op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];


			$op_val['invoice_subtotal'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_subtotal);
			$op_val['invoice_subtotal_excl_discount'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_subtotal);



			$op_val['user_name'] = $conf->user_name;
		//	$op_val['user_email'] =  $conf->user_email;
			$op_val['company_name'] = $conf->user_company;
			$op_val['company_email'] = $conf->company_email;
			$op_val['company_phone'] = $conf->company_phone;





			$company_address = str_replace("\n", "<br/>", $conf->company_address);
			$op_val['company_address'] = $company_address;
			$other_details = str_replace("\n", "<br/>", $conf->other_details);
			$op_val['company_details'] = $other_details;
			$op_val['company_url'] = $conf->company_url;
			$op_val['tax_id'] = $conf->tax_id;
			$op_val['company_tax_id'] = $conf->tax_id;
			$logo_path_check =  JPATH_SITE.DS."media".DS."com_ccinvoices".DS."logo".DS.$conf->logo;
			if($conf->logo != "")
			{
				if(JFile::exists($logo_path_check))
				{
			$op_val['logo'] = '<img src="'.JURI::root()."media/com_ccinvoices/logo/".$conf->logo.'"/>' ;
			}else
			{
					$op_val['logo'] = "";
				}
			}else
			{
				$op_val['logo'] = '';
			}
			$template = ccInvoicesControllerInvoices::convertImgTags($template);
			$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );

			$op_val['template_items'] = $generatedItemTemplate;
		}
		else
		{
			for($i=0;$i<count($quantity);$i++)
			{
				$breakVariable = "";
				$produtStringLen = strlen($pname[$i]);

				if($taskIDNT=="downloadInvoice")
				{
					$dividedVal =  73;
				}
				elseif($taskIDNT=="printInv")
				{
					$dividedVal =  62;
				}
				else
				{
					$dividedVal =  94;
				}

				$noOrBr = $produtStringLen / $dividedVal;

				$capCount=0;
				for($t=0;$t<=strlen($pname[$i])-1;$t++)
				{
					if(ord(substr($pname[$i],$t,1))>=65 AND ord(substr($pname[$i],$t,1))<=90)
					{
						$capCount++;
					}
				}

				if($taskIDNT=="downloadInvoice")
				{
					if($capCount>=10 AND strlen($pname[$i])>=100 AND strlen($pname[$i])<=150)
					{
						$noOrBr=ceil($noOrBr)+1;
					}
					else
					{
						$noOrBr=ceil($noOrBr);
					}
				}
				else
				{
					$noOrBr=ceil($noOrBr);
				}

				for($k=1;$k<=$noOrBr;$k++)
				{
					$breakVariable = $breakVariable."<br/>";
				}

				if($breakVariable=="")
					$breakVariable="<br/>";

				$quant.= $quantity[$i].$breakVariable;
				$itemids.=@$itemid[$i].$breakVariable;
				$pnames.=$pname[$i]."<br/>";
				$itemdescs.=@$itemdesc[$i].$breakVariable;

				//Discount Calc Ends
				$amounts.= ccInvoicesControllerInvoices::changeCurrencyFormat($amount[$i]).$breakVariable;
				$product_total_price += $amount[$i] * $quantity[$i];
				if($rows->discount > 0)
				{
					$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
					$inv_subtotal_amount += $disc_amt* $quantity[$i];

				}else
				{
					$inv_subtotal_amount += $amount[$i] * $quantity[$i];
				}
				/*$price += $amount[$i] * $quantity[$i];
				$price_tmp = $amount[$i] * $quantity[$i];
				$totalPrice += $price_tmp * $tax[$i] / 100;
	            */
	            $discount_price = "0";
				if($rows->discount > 0)
				{
					$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
					$amt_tmp1 = $disc_amt * $tax[$i] / 100;
					$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
					$discount_price += $subto1;
				}else
				{
					$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
					$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
					$discount_price += $subto1;
				}

				$amt_tmp = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100;
				$itemPriceIncTax[$i]=$amount[$i]+$amt_tmp;
				$itPriceIncTax.=ccInvoicesControllerInvoices::changeCurrencyFormat($itemPriceIncTax[$i]).$breakVariable;
				$taxes.= ccInvoicesControllerInvoices::changeCurrencyFormat($amt_tmp).$breakVariable;
				$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
				$invoice_subtotal += $subto;
				$subTotal.= ccInvoicesControllerInvoices::changeCurrencyFormat($subto).$breakVariable;
				//{item_total_excl_tax}
				$excl_tax = $amount[$i]* $quantity[$i];
				$invoice_total_excl_tax += $excl_tax;
				$item_total_excl_tax.= ccInvoicesControllerInvoices::changeCurrencyFormat($excl_tax).$breakVariable;
				if($tax[$i] == '')
				{
					$tax_percent .= "0%".$breakVariable;
				}else
				{
					$tax_percent .= $tax[$i]."%".$breakVariable;
				}

				$tmp = '';
				$tax_rate = $tax[$i];
				$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
				$inv_tax_total +=$product_tax_amt;
				if(count($tax_rate_values) == 0)
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}else
				{
					if (array_key_exists($tax_rate, $tax_rate_values))
					{
						$tmp = $tax_rate_values[$tax_rate];
					    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
					}else
					{
						$tax_rate_values[$tax_rate] = $product_tax_amt;
					}
				}
			}
			ksort($tax_rate_values);
			foreach($tax_rate_values as $key=>$value)
			{
				$order_tax_total_split_tmp .= ccInvoicesControllerInvoices::changeCurrencyFormat($value)."&nbsp;(".$key."%)&nbsp;<br/>";
			}
		    //{order_tax_total_split}
		    $orderitem_tax_total = ccInvoicesControllerInvoices::changeCurrencyFormat($inv_tax_total);
		    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
		    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
		    $order_tax_total_split .= $order_tax_total_split_tmp;
		    $op_val['tax_total_split'] = $order_tax_total_split;
		    $op_val['invoice_tax_total_split'] = $order_tax_total_split;
			//print_r ($tax_rate_values);exit;
			$post['quantity'] =$quant;
			$post['pname'] =$pnames;
			$post['price'] =$amounts;
			$post['tax'] =$taxes;
	        /* contact values */
			$op_val['invoice_note'] = $rows->note;
			//GETTING PAYMENT METHOD NAMES
			$paymentmethodnames=ccInvoicesControllerInvoices::getPaymentMethodsName();
			$op_val['payment_methods']= $paymentmethodnames;
	        $op_val['contact_name'] = $rows->name;
	        $op_val['contact_number'] = $rows->contact_number;
			$op_val['contact'] = $rows->contact;
			$op_val['contact_person'] = $rows->contact;
			$contact_address = str_replace("\n", "<br/>", $rows->address);
			$op_val['contact_address'] = $contact_address;

			$status_text="";

			if($rows->status=="1")
				$status_text=JText::_( 'CC_CONCEPT');
			if($rows->status=="2")
				 $status_text=JText::_( 'CC_OPEN');
			if($rows->status=="3")
				 $status_text=JText::_( 'CC_LATE');
			if($rows->status=="4")
				 $status_text=JText::_( 'CC_PAID');

			$op_val['invoice_status']=$status_text;

			$op_val['contact_taxid']= $rows->tax_id;
			$op_val['contact_tax_id']= $rows->tax_id;


			$op_val['contact_email'] = $rows->email;
		    $op_val['invoice_number'] = ccInvoicesControllerInvoices::getInvoiceNumberFormat($rows->id);
			$op_val['invoice_date'] = ccInvoicesControllerInvoices::dateChangeFormat($rows->invoice_date,$conf->date_format);
			$op_val['invoice_due_date'] = ccInvoicesControllerInvoices::dateChangeFormat( $rows->duedate,$conf->date_format);
			if(strtotime($invRow->invoice_sent_date) == '' OR trim($invRow->invoice_sent_date)=="0000-00-00" OR trim($invRow->invoice_sent_date)=="11-30-99" OR trim($invRow->invoice_sent_date)=="30-11-99")
			{
				$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
			}
			else
			{
			$op_val['invoice_sent_date'] = ccInvoicesControllerInvoices::dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
			}
			$op_val['item_id']=$itemids;
	       	$op_val['item_quantity'] = $quant;
		    $op_val['item_name'] = $pnames;
		    $op_val['item_description']=$itemdescs;
			$op_val['item_amount'] = $amounts;
			$op_val['item_price_excl_tax'] = $amounts;
			$op_val['item_price_incl_tax'] = $itPriceIncTax;


			$op_val['item_tax'] = $taxes;
			if($rows->discount == '' OR $rows->discount == '0')
			{
				$op_val['invoice_discount'] = '';
				$op_val['invoice_discount_new']='';
				$op_val['invoice_discount_excl_tax']='';
				$op_val['invoice_subtotal_with_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
				$op_val['invoice_subtotal_incl_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
			}else
			{

				$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
				$op_val['invoice_discount_new']=ccInvoicesControllerInvoices::changeCurrencyFormat(($product_total_price*$rows->discount)/100);
				$op_val['invoice_discount_excl_tax']=ccInvoicesControllerInvoices::changeCurrencyFormat(($product_total_price*$rows->discount)/100);
				$op_val['invoice_subtotal_with_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price-($product_total_price*$rows->discount)/100);
				$op_val['invoice_subtotal_incl_discount']=ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price-($product_total_price*$rows->discount)/100);
			}
			$op_val['product_total_price'] = ccInvoicesControllerInvoices::changeCurrencyFormat($product_total_price);
			$op_val['invoice_subtotal_amount'] = ccInvoicesControllerInvoices::changeCurrencyFormat($inv_subtotal_amount);
			$invoice_discount_price=$invoice_subtotal;
			$op_val['invoice_discount_price']=number_format($invoice_discount_price * $rows->discount /100,2);
			$op_val['invoice_discount_amount']=number_format($invoice_discount_price * $rows->discount /100,2);

			$op_val['invoice_tax'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->totaltax);
			$op_val['invoice_tax_total'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->totaltax);
			$op_val['tax_percentage'] = $tax_percent;
			$op_val['item_tax_percentage'] = $tax_percent;
			$op_val['invoice_total'] = '';
			$op_val['invoice_total_excl_tax'] = '';
			$op_val['invoice_total'] = ccInvoicesControllerInvoices::changeCurrencyFormat($rows->total);
		    $op_val['invoice_total_excl_tax'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_total_excl_tax);
		    $op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];
			$op_val['item_total_excl_tax'] = $item_total_excl_tax;
			$op_val['item_total_incl_tax'] = $subTotal;
			$op_val['invoice_subtotal'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_subtotal);
			$op_val['invoice_subtotal_excl_discount'] = ccInvoicesControllerInvoices::changeCurrencyFormat($invoice_subtotal);

			$op_val['product_tax'] = $taxes;
			$op_val['item_tax_amount'] = $taxes;
			$op_val['user_name'] = $conf->user_name;
		//	$op_val['user_email'] =  $conf->user_email;
			$op_val['company_name'] = $conf->user_company;
			$op_val['company_email'] = $conf->company_email;
			$op_val['company_phone'] = $conf->company_phone;





			$company_address = str_replace("\n", "<br/>", $conf->company_address);
			$op_val['company_address'] = $company_address;
			$other_details = str_replace("\n", "<br/>", $conf->other_details);
			$op_val['company_details'] = $other_details;
			$op_val['company_url'] = $conf->company_url;
			$op_val['tax_id'] = $conf->tax_id;
			$op_val['company_tax_id'] = $conf->tax_id;
			$logo_path_check =  JPATH_SITE.DS."media".DS."com_ccinvoices".DS."logo".DS.$conf->logo;
			if($conf->logo != "")
			{
				if(JFile::exists($logo_path_check))
				{
			$op_val['logo'] = '<img src="'.JURI::root()."media/com_ccinvoices/logo/".$conf->logo.'"/>' ;
			}else
			{
					$op_val['logo'] = "";
				}
			}else
			{
				$op_val['logo'] = '';
			}
			$template = ccInvoicesControllerInvoices::convertImgTags($template);
			$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );

		}

/*
 * Mail template tags
 */


		$front_end_url = JURI::root()."index.php?option=com_ccinvoices&view=ccinvoices";
		$pay_invoice_link=JURI::root()."index.php?option=com_ccinvoices&task=paymentOverview&id=".MD5($id);
		$front_end_url=str_replace("components/com_ccinvoices/helper/","",$front_end_url);
		$pay_invoice_link=str_replace("components/com_ccinvoices/helper/","",$pay_invoice_link);
		$op_val["all_invoices_link"] = "<a href='".$front_end_url."' target='_blank'>".JText::_("CC_WEBSITE_LINK")."</a>";
		$op_val["pay_invoice_link"] = "<a href='".$pay_invoice_link."' target='_blank'>".JText::_("CC_PAY_INVOICE_LINK")."</a>";
		$op_val["invoice_last_send_date"] = ccInvoicesControllerInvoices::dateChangeFormat($rows->invoice_sent_date,$conf->date_format);
		$op_val["name"] =  $rows->name;

		@$payRow=@ccInvoicesControllerInvoices::getPaymentRecord($rows->id);

		if(@trim($payRow->pdate)!="")
		{
			$op_val['paid_date'] = strftime($conf->date_format,$payRow->pdate);
		}
		else
		{
			$op_val['paid_date'] = "";
		}


		//Get Custom Tags From The Plugin
		$val["id"] = $id;
		$options = array( $val);
		JPluginHelper::importPlugin( 'ccinvoicetags' );
		$dispatcher = JDispatcher::getInstance();
		$customTag = $dispatcher->trigger( '_getCustomTags',$options);
		foreach ($customTag as $customTags)
		{
			foreach ($customTags as $customTagName=>$value)
			{
				$op_val[$customTagName] = preg_replace('/<br(.*?)\/>/', '', $value);
			}
		}
		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$template = str_replace($find,$replace,$template);
			$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
		}
		return $template;
	}
	function getPaymentRecord($invId)
	{
		$db = JFactory::getDBO();
		$query = "SELECT * from #__ccinvoices_payment WHERE inv_id=$invId";
		$db->setQuery($query);
		return $db->loadObject();
	}
	function getInvoiceNumberFormat($invNum)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$conf = $db->loadObject();
		$invoice_format = $conf->invoice_format;
		$query = 'SELECT custom_invoice_number,number from #__ccinvoices_invoices WHERE id='.$invNum.' LIMIT 1 ';
		$db->setQuery($query);
		$rowobj = $db->loadObject();

		$custom_invoice_number=$rowobj->custom_invoice_number;
		if($rowobj->custom_invoice_number == '' || $rowobj->custom_invoice_number == '0')
		{
			$custom_invoice_number = $rowobj->number;
		}
		return $custom_invoice_number;
	}
	function changeCurrencyFormat($cur_val)
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT cformat,currency_symbol,symbol_display FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$format = $conf->cformat;
		$tmp = '';
		$price_amount = "";
		if($format == 0)
		{
			$tmp = @number_format(@$cur_val, 2, '.', ',');
		}else if($format == 1)
		{
			$tmp = @number_format(@$cur_val, 2, ',', '.');
		}else if($format == 2)
		{
			$tmp = @number_format(@$cur_val, 2, '.', ' ');
		}else if($format == 3)
		{
			$tmp = @number_format(@$cur_val, 2, ".", "'");
		}

		if($conf->symbol_display == '1')
		{
			$price_amount = $conf->currency_symbol.$tmp;
		}else
		{
			$price_amount = $tmp.$conf->currency_symbol;
		}
		return $price_amount;
	}
	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}
	function dateChangeFormat($date_temp,$date_format)
	{
		if(strtotime($date_temp) != '')
		{
			return strftime($date_format,strtotime($date_temp));
		}else
		{
			return;
		}
	}
}

class JHTMLInput
{
	function checkbox($name,$value,$checked='checked')
	{
		$html = "<input type=\"checkbox\" name=\"" . $name . "\" value=\"" . $value . "\"  id=\"" . $name . "\"  onClick=\"DoTheCheck('$name')\" $checked	/>";
		return $html;
	}
}

?>