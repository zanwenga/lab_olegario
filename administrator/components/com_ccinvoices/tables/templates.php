<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class Tabletemplates extends JTable
{
	var $id = null;
	var $invoice_template =null;
	var $title =null;
	var $edit_by  =null;
	var $edit_date  =null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_templates', 'id', $db);
	}

	function check()
	{
		return true;
	}
}
?>