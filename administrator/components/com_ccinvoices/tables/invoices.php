<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Include library dependencies
jimport('joomla.filter.input');

class Tableinvoices extends JTable
{

	var $id = null;
	var $number = null;
	var $invoice_date = null;
	var $status= null;
	var $duedate = null;
	var $note = null;
	var $numbercheck=null;
	var $invoice_sent_date = null;
	var $custom_invoice_number = null;
	var $communication = null;
	var $discount = null;
	var $subtotal = null;
	var $totaltax = null;
	var $total = null;
	var $item_id = null;
	var $quantity= null;
	var $pname=null;
	var $item_description = null;
	var $price = null;
	var $tax=null;
	var $contact_id = null;

	function __construct(& $db)
	{
		parent::__construct('#__ccinvoices_invoices', 'id', $db);
	}

	function check()
	{
		return true;
	}
}
?>