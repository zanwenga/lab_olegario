<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

class ccInvoicesViewTemplates extends CCINVOISViews
{
	function display($tpl = null)
	{
		global $mainframe, $option;
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
		$user_name = $user->get("name");

		// Access check.
		if (!JFactory::getUser()->authorise('ccinvoices.view.templates', 'com_ccinvoices'))
		{
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}


		if($this->_layout == 'form' )
		{
			require_once JPATH_COMPONENT.'/helper/ccinvoices.php';
			$canDo = ccinvoicesHelper::getActions();

			if (!JFactory::getUser()->authorise('ccinvoices.edit', 'com_ccinvoices'))
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}
$cid 		= JRequest::getVar( 'cid', array(0), '', 'array' );
			if(versionCompare()>=3)
			{
				if(JRequest::getInt('id'))
				{
					$cid 		= JRequest::getVar( 'cid', array(JRequest::getInt('id')), '', 'array' );
				}
			}
			JArrayHelper::toInteger($cid, array(0));
			JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).":  ".JText::_( 'CC_INVOICES_TEMPLATE' ).' ', 'ccinvoices.png' );
			JToolBarHelper::apply('templates.apply',JText::_( 'CCINVOICES_TEMPLATES_APPLY_TOOL' ));
			JToolBarHelper::save('templates.save',JText::_( 'CCINVOICES_TEMPLATES_SAVE_TOOL' ));
			JToolBarHelper::cancel('templates.cancel',JText::_( 'CCINVOICES_TEMPLATES_CLOSE_TOOL' ));
			$query = "SELECT * FROM #__ccinvoices_configuration WHERE id=1 LIMIT 1";
			$db->setQuery( $query);
			$confrow= $db->loadObject();

			if($confrow->default_email_sub == "")
			{
				$confrow->default_email_sub = JText::_( 'CC_INVOICEEMAIL_SUB' );
			}
			if($confrow->rem_email_sub == "")
			{
				$confrow->rem_email_sub = JText::_( 'CC_INVOICEREMINDER_SUB' );
			}

			$query = "SELECT * FROM #__ccinvoices_templates WHERE id = ".$cid[0]." LIMIT 1";
			$db->setQuery( $query);
			$row= $db->loadObject();

	        $pagestyle[] = JHTML::_('select.option',  '0', JText::_('CCINVOICES_PORTRAIT'));
	        $pagestyle[] = JHTML::_('select.option',  '1',  JText::_('CCINVOICES_LANDSCAPE'));

	        $fontname[] = JHTML::_('select.option',  '1',  JText::_('CCINVOICES_FONT_NAME_1'));
	        $fontname[] = JHTML::_('select.option',  '2', JText::_('CCINVOICES_FONT_NAME_2'));
	        $fontname[] = JHTML::_('select.option',  '3', JText::_('CCINVOICES_FONT_NAME_3'));
	        $fontname[] = JHTML::_('select.option',  '4', JText::_('CCINVOICES_FONT_NAME_4'));
	        $fontname[] = JHTML::_('select.option',  '5', JText::_('CCINVOICES_FONT_NAME_5'));

	        $pdf_layout[] = JHTML::_('select.option',  '0', JText::_('CCINVOICES_OLDLAYOUT'));
	        $pdf_layout[] = JHTML::_('select.option',  '1',  JText::_('CCINVOICES_NEWLAYOUT'));



			$this->assignRef('pagestyle',$pagestyle);
			$this->assignRef('fontname',	$fontname);
			$this->assignRef('pdf_layout',	$pdf_layout);


			$this->assignRef('confrow',$confrow);
			$this->assignRef('confrows',$confrow);
			$this->assignRef('row',	$row);
			$this->assignRef('edit_by',	$user_name);
			if(versionCompare()>=3)
			{
				$this->_layout='form';
			}
			else
			{
				$this->_layout='form25';
			}
		}else
		{



			$query = "SELECT *  FROM #__ccinvoices_templates order by id";
			$db->setQuery( $query);
			$rows= $db->loadObjectList();

			$query = "SELECT * FROM #__ccinvoices_configuration WHERE id=1 LIMIT 1";
			$db->setQuery( $query);
			$confrow= $db->loadObject();

			$this->assignRef('confrow',$confrow);

			$this->jversion='';
			$this->assignRef('rows',	$rows);
			$this->addToolBar();
			if(versionCompare()>=3)
			{
				$this->sidebar = JHtmlSidebar::render();
				$this->_layout='default';
			}
			else
			{
				$this->_layout='default25';
			}
		}
		$this->getVersionInfo();
		$this->addCss();
		parent::display($tpl);
	}
	function addToolBar()
	{
		JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).":  ".JText::_( 'CC_INVOICES_TEMPLATES' ).' ', 'ccinvoices.png' );
		$editpermission="1";
		$canDo = ccinvoicesHelper::getActions();
		if ($canDo->get('ccinvoices.edit'))
			JToolbarHelper::editList('templates.edit',JText::_( 'CCINVOICES_TEMPLATES_EDIT_TOOL' ));
		if ($canDo->get('core.admin')) {
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_ccinvoices','500','800', JText::_( 'CCINVOICES_PERMISSION_TOOL_LBL' ));
		}
		$editpermission=$canDo->get('ccinvoices.edit');
		$this->editpermission=$editpermission;
	}
	function getVersionInfo()
	{

		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';

		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);

		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
	function addCss()
	{
	    $document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	}
}
?>