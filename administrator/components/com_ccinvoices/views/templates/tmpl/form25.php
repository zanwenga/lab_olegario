<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
$editor		= JFactory::getEditor();
?>
<script language="javascript" type="text/javascript">
function submitbutton(pressbutton)
{
	var form = document.adminForm;
	try
	{
		if ( form.invoice_template.value == "" ) {
			alert("<?php echo JText::_( 'CC_TEMPLATE_EMPTY', true ); ?>");
		} else {

			<?php
				if($this->row->id=="1")
				{
					echo $editor->save( 'invoice_template');
				}
			?>
			submitform(pressbutton);
		}
	}
	catch(e)
	{
		submitform(pressbutton);
	}
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<table width="100%"  cellpadding="0" cellspacing="0" border="0" >

	<tr>
		<td valign="top">
				<table cellpadding="0" cellspacing="0" border="0" width="100%">
					<tr>
						<td>
							<input type="hidden" name="selected_id" value="<?php echo $this->row->id; ?>">
						</td>
					</tr>
					<?php
						if($this->row->id=="1")
						{
						?>

					<tr>
						<td align="left" >
							<input type="hidden" name="title" value="<?php echo $this->row->title;?>" size="40" maxlength="150" />
						</td>
					</tr>
					<tr>
						<td colspan="2" align="left" valign="top">
							<fieldset class="adminform" style="width:auto;padding:10px;">
							<legend><?php echo $this->row->title; ?></legend>
							<table width="100%" cellpadding="0" cellspacing="0">
								<tr>
									<td><a href="#" style="font-size:20px;text-decoration:none !important;"> <?php echo JText::_('CCINVOICES_TEMPLATE_HEADER');?></a></td>
								</tr>
								<tr>
									<td>
										<?php
										if($this->row->id=="1")
										{
											?>

											<?php
											echo $editor->display( 'invoice_template', $this->row->invoice_template , '100%', '600', '25', '10',true ,null) ;
										?>
										<?php
										}else{
										?>
										<?php
										}
										?>
									</td>
								</tr>
								<?php if($this->row->id=="1"){	?>
									<tr>
										<td>&nbsp;</td>
									</tr>
									<tr>
										<td><a href="#" style="font-size:20px;text-decoration:none !important;"> <?php echo JText::_('CCINVOICES_TEMPLATE_ITEMS');?></a></td>
									</tr>
									<tr>
										<td colspan="2">
										<?php
											$visBle='';
											if($this->confrows->pdf_layout=="1")
												$visBle='display:block;';
											else
												$visBle='display:none;';

										?>
										<?php
										echo $editor->display( 'items_template', $this->confrow->items_template , '100%', '100', '25', '10',true ,null) ;
										?>
										</td>
									</tr>
								<?php } ?>
							</table>
							</fieldset>
						</td>
					</tr>

					<?php } ?>

					<?php
						if($this->row->id=="2")
						{
						?>
					<tr>
						<td colspan="2" align="left" valign="top">
							<fieldset class="adminform" id="ccinvoices_invoicecsv" >
							<legend><?php echo $this->row->title; ?></legend>
							<textarea name="invoice_template" id="invoice_template" style="width:100%;" rows="5" cols="80"><?php echo $this->row->invoice_template; ?></textarea>
						</td>
					</tr>
					<?php } ?>


					<?php
						if($this->row->id>2)
						{
						?>
					<tr>
						<td colspan="2" valign="top">
								<!--default note-->
							<?php
								if($this->row->id=="3")
								{
								?>
								<fieldset class="adminform" style="padding:10px;">
									<legend><?php echo $this->row->title; ?></legend>
									<table cellpadding="3" cellspacing="0" width="100%" border="0">
										<tr>
											<td align="left" valign="top" >
												&nbsp;
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												 <?php 	echo $editor->display( 'default_note', $this->confrow->default_note , '100%', '200', '25', '10' ); ?>
											</td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>

								<?php
								if($this->row->id=="4")
								{
								?>
								<!--default email-->
								<fieldset class="adminform"  style="padding:10px;">
									<legend><?php echo $this->row->title; ?></legend>
										<table cellpadding="0" cellspacing="0" width="100%" border="0">
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_SUB' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<input type="text" name="default_email_sub" value="<?php echo $this->confrow->default_email_sub; ?>" size="60"/>

												</td>
											</tr>
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_MSG' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<?php 	echo $editor->display( 'default_email', $this->confrow->default_email , '100%', '350', '25', '10' ); ?>
												</br></br>
												</td>
											</tr>
										</table>
								</fieldset>
								<?php } ?>
								<?php
								if($this->row->id=="5")
								{
								?>
								<!--default email reminder-->
								<fieldset class="adminform"  style="padding:10px;">
									<legend><?php echo $this->row->title; ?></legend>
									<table cellpadding="3" cellspacing="0" width="100%" border="0">
										<tr>
											<td align="left" valign="top" >
												<?php echo JText::_( 'CC_EMAIL_SUB' ); ?>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
											<input type="text" name="rem_email_sub" value="<?php echo $this->confrow->rem_email_sub; ?>" size="60"/>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top" >
												<?php echo JText::_( 'CC_EMAIL_MSG' ); ?>
											</td>
										</tr>
										<tr>
											<td align="left" valign="top">
												<?php 	echo $editor->display( 'default_email_rem', $this->confrow->default_email_rem , '100%', '350', '25', '10' ); ?>
											</br></br>
											</td>
										</tr>
									</table>
								</fieldset>
								<?php } ?>
								<?php
								if($this->row->id=="6")
								{
								?>
								<!--default email-->
								<fieldset class="adminform"  style="padding:10px;">
									<legend><?php echo $this->row->title; ?></legend>
										<table cellpadding="0" cellspacing="0" width="100%" border="0">
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_SUB' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<input type="text" name="newusermail_sub" value="<?php echo $this->confrow->newusermail_sub; ?>" size="60"/>

												</td>
											</tr>
											<tr>
												<td align="left" valign="top" >
													<?php echo JText::_( 'CC_EMAIL_MSG' ); ?>
												</td>
											</tr>
											<tr>
												<td align="left" valign="top">
													<?php 	echo $editor->display( 'invoice_template', $this->row->invoice_template , '100%', '350', '25', '10' ); ?>
												</br></br>
												</td>
											</tr>
										</table>
								</fieldset>
								<?php } ?>
						</td>
					</tr>
					<?php
						}
					?>
				</table>
		</td>
		<td width="200" valign="top" style="padding-top:4px;" >
			<table cellpadding="0" cellspacing="0" border="0" width="100%">
				<tr>
					<td align="left" valign="top">
					<?php
						JHTML::_('behavior.tooltip');
						jimport('joomla.html.pane');
						// TODO: allowAllClose should default true in J!1.6, so remove the array when it does.
						$tabs = JPane::getInstance('Tabs');

						$pane1 = JPane::getInstance('sliders', array('allowAllClose' => true));
						if($this->row->id=="6")
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							".JText::_( 'CC_NEWUSER_EMAIL_TAGS' )."
							</table>";
						}
						elseif($this->row->id=="1" OR $this->row->id=="3" OR $this->row->id=="4" OR $this->row->id=="5")
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							{contact_name}<br/>{user_name}<br/>
							{contact_number}<br/>
							{contact_person}<br/>
 							{contact_address}<br/>
 							{contact_tax_id}<br/>
							{contact_email}<br/><br/>
							{invoice_number}<br/>
							{invoice_status}<br/>
							{invoice_date}<br/>
							{invoice_due_date}<br/>
							{invoice_last_send_date}<br/>
							{paid_date}<br/>
							{invoice_sent_date}<br/>
							{invoice_note}<br/>
							{payment_methods}<br/><br/>
							{template_items}<br/>
							{item_id}<br/>
							{item_quantity}<br/>
							{item_name}<br/>
							{item_description}<br/>
							{item_price_excl_tax}<br/>
							{item_price_incl_tax}<br/>
							{item_tax_percentage}<br/>
							{item_tax_amount}<br/>
							{item_total_incl_tax}<br/>
							{item_total_excl_tax}<br/><br/>
							{invoice_discount}<br/>
							{invoice_discount_excl_tax}<br/>
							{invoice_subtotal_incl_discount}<br/>
							{invoice_subtotal_excl_discount}<br/>
							{invoice_tax_total}<br/>{invoice_total_excl_tax}<br/>{invoice_total_incl_tax}<br/>{invoice_tax_total_split}<br/><br/>
							{company_name}<br/>
							{company_email}<br/>
							{company_phone}<br/>
							{company_address}<br/>
							{company_details}<br/>
							{company_url}<br/>
							{company_tax_id}<br/>
							{pay_invoice_link}<br/>
							{all_invoices_link}<br/>
							</table>";
						}
						elseif($this->row->id=="2")
						{
							$shop_info = "<table width='200'><tr><td style='padding:5px;' >
							{contact_name}<br/>
							{contact_number}<br/>
							{contact_person}<br/>
 							{contact_address}<br/>
 							{contact_tax_id}<br/>
							{contact_email}<br/><br/>
							{invoice_number}<br/>
							{invoice_status}<br/>
							{invoice_date}<br/>
							{invoice_due_date}<br/>
							{invoice_sent_date}<br/>
							{invoice_note}<br/>
							{payment_methods}<br/><br/>
							{item_id}<br/>
							{item_quantity}<br/>
							{item_name}<br/>
							{item_description}<br/>
							{item_price_excl_tax}<br/>
							{item_price_incl_tax}<br/>
							{item_tax_percentage}<br/>
							{item_tax_amount}<br/>
							{item_total_incl_tax}<br/>
							{item_total_excl_tax}<br/><br/>
							{invoice_discount}<br/>
							{invoice_discount_excl_tax}<br/>
							{invoice_subtotal_incl_discount}<br/>
							{invoice_subtotal_excl_discount}<br/>
							{invoice_tax_total}<br/>{invoice_total_excl_tax}<br/>{invoice_total_incl_tax}<br/>{invoice_tax_total_split}<br/><br/>
							{company_name}<br/>
							{company_email}<br/>
							{company_phone}<br/>
							{company_address}<br/>
							{company_details}<br/>
							{company_url}<br/>
							{company_tax_id}<br/>
							</table>";
						}
					if($this->row->id=="1")
					{
						echo $tabs->startPane('myTabs');
							echo $tabs->startPanel(JText::_('CCINVOICES_TEMPLATE_TAB'), 'order');
								echo $pane1->startPane("menu-pane11");
								echo $pane1->startPanel(JText :: _('CC_TEMP_TAGS'), "param-page");
			                    echo $shop_info;
								echo $pane1->endPanel();
							echo $tabs->endPanel();

							echo $tabs->startPanel(JText::_('CCINVOICES_PDF_SET_TAB'), 'order');
								$html='';
								if($this->row->id=="1")
								{
										if($this->confrows->backgimage == "1")
										{
											$selectBEF = "checked";
											$selectAFT = "";
											$displayImgSet='display:block;';

										}else
										{
											$selectBEF = "";
											$selectAFT = "checked";
											$displayImgSet='display:none;';
										}
										$allImgTag="";
										?>
										<?php if($this->confrows->pdfimagefile != '') {
											$pt=JURI::root().'media/com_ccinvoices/logo/'.$this->confrows->pdfimagefile;
											$allImgTag="<img src='$pt' height='100' style='width:100px;'>";
										 } ?>
										<?php
										$html="<table border='0' width='200' cellspacing='0' cellpadding='0'> <tr><td valign='top'><table border='0' cellspacing='0' cellpadding='0'><tr><td>
											<fieldset style='width:180px;background-color:white;' ><legend>".JText::_( 'CCINVOICES_PDF_INVOICE_SETTINGS' )."</legend>
											<table cellspacing='0' cellpadding='0'><tr><td valign='top' class='ccinvoices_drop_lbl' >".JText::_('CCINVOICES_PDF_INVOICE_MODE')."</td>
											<td>".JHTML::_('select.genericlist',   $this->pagestyle, 'invoice_mode', 'class="inputbox ccinvoices_drop_self" size="1"" ', 'value', 'text', $this->confrows->invoice_mode)."</td></tr>
													<tr><td>".JText::_('CCINVOICES_PDF_INVOICE_TOP_MARGIN')."</td><td><input type='text' name='invoice_top_margin' value='".$this->confrows->invoice_top_margin."'></td></tr><tr><td>".JText::_('CCINVOICES_PDF_INVOICE_BOTTOM_MARGIN')."</td><td><input type='text' name='invoice_bottom_margin' value='".$this->confrows->invoice_bottom_margin."'></td></tr>
													<tr><td>".JText::_('CCINVOICES_PDF_INVOICE_LEFT_MARGIN')."</td><td><input type='text' name='invoice_left_margin' value='".$this->confrows->invoice_left_margin."'></td></tr><tr><td>".JText::_('CCINVOICES_PDF_INVOICE_RIGHT_MARGIN')."</td><td><input type='text' name='invoice_right_margin' value='".$this->confrows->invoice_right_margin."'></td></tr>
													<tr><td>".JText::_( 'CCINVOICES_FONT_SIZE' )."</td><td><input type='text' name='pdffontsize_invoice' id='pdffontsize_invoice' value='".$this->confrows->pdffontsize_invoice."' size='5' maxlength='40'/></td></tr>
													<tr><td align='left' valign='top' class='ccinvoices_drop_lbl'>".JText::_( 'CCINVOICES_SHOW_INVOICE_FONT_TYPE' )."</td><td align='left' >".JHTML::_('select.genericlist',   $this->fontname, 'fontname_invoice', 'class"inputbox ccinvoices_drop_self" size="1" style="margin-bottom: 0px !important;"', 'value', 'text', $this->confrows->fontname_invoice )."</td></tr>
													<tr><td align='left' valign='top' class='ccinvoices_drop_lbl'>".JText::_( 'CCINVOICES_PDFLAYOUTS' )."</td><td align='left' >".JHTML::_('select.genericlist',   $this->pdf_layout, 'pdf_layout', 'class="inputbox ccinvoices_drop_self" size="1" ', 'value', 'text', $this->confrows->pdf_layout )."</td></tr>
													<tr><td align='left' valign='top' class='ccinvoices_drop_lbl'>".JText::_( 'CCINVOICES_PDFIMAGESTATE' )."</td><td align='left' >
														<table>
															<tr>
															<td><input type='radio' onchange='showBackImageSet(this.value)' id='backgimage' name='backgimage' value='1' ".$selectBEF." /></td>
															<td>".JText::_( 'CCINVOICES_PDFIMAGESTATE_TEXT_YES' )."&nbsp;</td>
															<td><input type='radio' onchange='showBackImageSet(this.value)' id='backgimage' name='backgimage' value='0' ".$selectAFT." /></td>
															<td>".JText::_( 'CCINVOICES_PDFIMAGESTATE_TEXT_NO' )."</td>
															</tr>
														</table>
													</td></tr>
													<tr>
														<td colspan='2'  align='left' valign='top' class='ccinvoices_drop_lbl'>
															<table width='100%' id='backgrountbl_id' style='$displayImgSet'>
																<tr>
																	<td>
																		".JText::_( 'CCINVOICES_PDFIMAGE_XPOS_LBL' )."
																	</td>
																	<td>
																		<input type='text' name='pdfimagexpos' value='".$this->confrows->pdfimagexpos."'>
																	</td>
																</tr>
																<tr>
																	<td>
																		".JText::_( 'CCINVOICES_PDFIMAGE_YPOS_LBL' )."
																	</td>
																	<td>
																		<input type='text' name='pdfimageypos' value='".$this->confrows->pdfimageypos."'>
																	</td>
																</tr>
																<tr>
																	<td>
																		".JText::_( 'CCINVOICES_PDFIMAGE_WIDTH_LBL' )."
																	</td>
																	<td>
																		<input type='text' name='pdfimagewidth' value='".$this->confrows->pdfimagewidth."'>
																	</td>
																</tr>
																<tr>
																	<td>
																		".JText::_('CCINVOICES_PDFIMAGE_HEIGHT_LBL')."
																	</td>
																	<td>
																		<input type='text' name='pdfimageheight' value='".$this->confrows->pdfimageheight."'>
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		".JText::_('CCINVOICES_PDFIMAGE_BROWSE_LBL')."
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>
																		<input type='file' name='pdfimagefile'>
																	</td>
																</tr>
																<tr>
																	<td colspan='2'>".$allImgTag."</td>
																</tr>
															</table>
														</td>
													</tr>
													</table>
											</fieldset></td></tr>
											</table></td></tr></table>";
								}
								echo $html;
							echo $tabs->endPanel();
						echo $tabs->endPane();
					}
					else
					{
							echo $pane1->startPane("menu-pane11");
							echo $pane1->startPanel(JText :: _('CC_TEMP_TAGS'), "param-page");
		                    echo $shop_info;
							echo $pane1->endPanel();
					}
						?>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="controller" value="templates">
<input type="hidden" name="id" value="<?php echo $this->row->id; ?>">
<input type="hidden" name="edit_by" value="<?php echo $this->edit_by; ?>">
<input type="hidden" name="edit_date" value="<?php echo time(); ?>">
<input type="hidden" name="task" value="">
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<script language="JavaScript" type="text/javascript">
function showBackImageSet(val)
{
	if(val==1)
	{
		document.getElementById('backgrountbl_id').style.display='block';
		document.getElementById('backgrountbl_id').style.width='100%';
	}
	else
	{
		document.getElementById('backgrountbl_id').style.display='none';
	}
}
</script>
<table width="100%">
	<tr>
		<td align="center">
		<?php
			if (isset($this->versionContent))
			{
				echo $this->versionContent;
			}
		?>
		</td>
	</tr>
</table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>
<style type="text/css">
#editor-xtd-buttons
{
	display:none !important;
}
.toggle-editor
{
	display:none !important;
}
</style>