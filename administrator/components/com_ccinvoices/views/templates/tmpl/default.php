<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
?>
<style>
.adminlist
{
	font-size:11px;
}
</style>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<table class="table table-striped">
<thead>
					<tr>
		<th width="1%" align="center">
			#
		</th>
		<th width="1%" align="center">
			<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
		</th>
		<th  style="text-align:left;">
			<?php echo JText::_("CC_TEMPLATE_TITLE"); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JText::_("CC_TEMPLATE_LAST_EDIT_BY"); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JText::_("CC_TEMPLATE_EDITED_ON"); ?>
		</th>
	</tr>
</thead>
								<?php
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
	$row = &$this->rows[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );
	if($this->editpermission=="1")
		$link 		= JRoute::_( 'index.php?option=com_ccinvoices&task=templates.edit&&cid[]='. $row->id );
	else
		$link 		= "#";
	$z = $i + 1;
								?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<?php echo $z; ?>
						</td>
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="left" class="nowrap has-context">
			<div class="pull-left">
				<a href="<?php echo $link; ?>"><?php echo $row->title; ?></a>
			</div>
			<div class="pull-left">
				<?php
					// Create dropdown items
					JHtml::_('dropdown.edit', $row->id, 'templates.');

					// Render dropdown list
					echo JHtml::_('dropdown.render');
				?>
			</div>
		</td>
		<td align="center">
			<?php echo $row->edit_by; ?>
		</td>
		<td align="center">

			<?php
				echo strftime($this->confrow->date_format,$row->edit_date);
			 ?>
		</td>
	</tr>
<?php
	$k = 1 - $k;
	}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="controller" value="templates" />
<input type="hidden" name="view" value="templates" />
<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>
