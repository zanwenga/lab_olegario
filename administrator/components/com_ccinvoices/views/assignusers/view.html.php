<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

class ccInvoicesViewAssignusers extends CCINVOISViews
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = JFactory::getDBO();
	    $document = JFactory::getDocument();
		$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/css/ccinvoice.css' );
		$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/css/style.css' );
		$contact_id = JRequest::getInt("contact_id","");
		$sql = "SELECT * FROM #__users ORDER BY  registerDate DESC LIMIT 10";
		$db->setQuery($sql);
		$rows = $db->loadObjectList();
		$sql = "SELECT u.id,u.name,u.username,u.email FROM #__users AS u"
			. " LEFT JOIN #__ccinvoices_users AS iu ON iu.user_id = u.id "
			. " WHERE iu.contact_id = ".$contact_id
			;
		$db->setQuery($sql);
		$userAssined = $db->loadObjectList();

		$sql = "SELECT * FROM #__ccinvoices_contacts WHERE id =".$contact_id." LIMIT 1";
		$db->setQuery($sql);
		$contactDetails = $db->loadObject();

		$user	= JFactory::getUser();
		$sql = "SELECT * FROM #__user_usergroup_map WHERE user_id =".$user->get('id')." order by group_id desc LIMIT 1";
		$db->setQuery($sql);
		$newdata = $db->loadObject();
		$query = "SELECT id as value,title AS text FROM #__usergroups WHERE id <=".$newdata->group_id." ";
		$db->setQuery( $query);
		$types[] 		= JHTML::_('select.option',  '0', '- '. JText::_( 'CC_SELECT_GROUP' ) .' -' );
		foreach( $db->loadObjectList() as $rec )
		{
			$types[] = JHTML::_('select.option',  $rec->value, JText::_( $rec->text ) );
		}
		$lists['type'] 	= JHTML::_('select.genericlist',   $types, 'usergroup', 'class="inputbox" size="1" ', 'value', 'text', "0" );


		/* Component Footer Information End*/
		$this->assignRef("rows",$rows);
		$this->assignRef("userAssined",$userAssined);
		$this->assignRef("contact_id",$contact_id);
		$this->contactDetails=$contactDetails;

		$this->assignRef("lists",$lists);

		if(versionCompare()>=3)
		{
			$this->_layout='default';
		}
		else
		{
			$this->_layout='default25';
		}
		$this->getVersionInfo();
		parent::display($tpl);
	}
	function getVersionInfo()
	{
		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';
		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);
		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
}
?>