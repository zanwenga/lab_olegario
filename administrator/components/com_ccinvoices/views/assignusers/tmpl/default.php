<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
?>
<script language ="javascript">
function copyEmailName()
{
	document.adminForm.name.value = document.adminForm.email.value;
}
function copyEmailUsername()
{
	document.adminForm.username.value = document.adminForm.email.value;
}
</script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/checkEmail.js"></script>
<script src="<?php echo JURI::base();?>components/com_ccinvoices/assets/js/addUser.js"></script>

<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php



$my 	=  JFactory::getUser ();
?>
<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
		<td width="70%" valign="top" style="padding-right:10px;">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#page-site" data-toggle="tab"><?php echo JText::_('CC_CREATE_NEW_USER');?></a></li>
				<li><a href="#page-system" data-toggle="tab"><?php echo JText::_('CC_ADD_EXISTING_USER');?></a></li>
			</ul>
			<div id="config-document" class="tab-content">
				<div id="page-site" class="tab-pane active">
					<div class="row-fluid">
						<div class="span50">
							<table cellpadding="4" cellspacing="0"  border="0">
								<tr>
									<td colspan="3">
										<?php echo JText::_("CC_NEW_USER_DESC"); ?>
									</td>
								</tr>
								<tr>
									<td colspan="3">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td width="15%">
										<?php echo JText::_("CC_EMAIL"); ?>
									</td>
									<td  width="33%">
										<input type="text" name="email" id="email" onkeyup="checkEmail();" size="30" value="" maxlength="60">
										<input type="hidden" name="email_checker" id="email_checker" value="0" />
										<input type="hidden" name="emailMSGval" id="emailMSGval" value="<?php echo JText::_("CC_EMAILEXISTMSG"); ?>" />
										<input type="hidden" name="emailMSG_EMPTY" id="emailMSG_EMPTY" value="<?php echo JText::_("CC_EMAIL_EMPTY"); ?>" />
										<input type="hidden" name="emailMSG_INVALID" id="emailMSG_INVALID" value="<?php echo JText::_("CC_EMAIL_NOT_VALID"); ?>" />
										<input type="hidden" name="nameEmptyMSG" id="nameEmptyMSG" value="<?php echo JText::_("CC_NAME_EMPTY"); ?>" />
										<input type="hidden" name="usernameEmptyMSG" id="usernameEmptyMSG" value="<?php echo JText::_("CC_USERNAME_EMPTY"); ?>" />
										<input type="hidden" name="usergroupEmptyMSG" id="usergroupEmptyMSG" value="<?php echo JText::_("CC_USERGROUP_EMPTY"); ?>" />
										<input type="hidden" name="jversion" id="jversion" value="0" />
									</td>
									<td id="emailMSG">
											&nbsp;
									</td>
								</tr>
								<tr>
									<td>
										<?php echo JText::_("CC_NAME"); ?>
									</td>
									<td>
										<input type="text" name="name" size="30" value="" maxlength="60">
									</td>
									<td align="left">
										<a href="javascript:void(0);" onclick="copyEmailName();"><img  src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/copy_email.png" width="16" height="16">&nbsp;<?php echo JText::_("CC_COPY_EMAIL"); ?></a>
									</td>
								</tr>
								<tr>
									<td>
										<?php echo JText::_("CC_USER_NAME"); ?>
									</td>
									<td>
										<input type="text" name="username" size="30" value="" maxlength="60">
									</td>
									<td align="left">
										<a href="javascript:void(0);" onclick="copyEmailUsername();"><img  src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/copy_email.png" width="16" height="16">&nbsp;<?php echo JText::_("CC_COPY_EMAIL"); ?></a>
									</td>
								</tr>
								<tr>
									<td>
										<?php echo JText::_("CC_GROUP"); ?>
									</td>
									<td>
										<?php echo $this->lists['type']; ?>
									</td>
									<td>
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										&nbsp;
									</td>
								</tr>
								<tr>
									<td colspan="3" align="center">
										<table>
										<tr><td>
										<img  src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/user_add.png" width="16" height="16">
										</td>
										<td><input class="btn btn-primary" type="button" name="createUser" value="<?php echo JText::_("CC_CREATE_USER_CONTACT"); ?>" onclick ="addUser(this.form,'','<?php echo $this->contact_id; ?>','createUser');" >
										</td>
										</table>
										<span id="op_area" style="display:none;">
											<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/loader.gif" width="16" height="16">
										</span>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</div>
				<div id="page-system" class="tab-pane">
					<div class="row-fluid">
						<div class="span12">
							<table cellpadding="0" cellspacing="0" width="100%" border="0">
											<tr>
												<td>
													<div id="filter-bar" class="btn-toolbar">
														<div class="filter-search btn-group pull-left">
															<?php echo JText::_('CC_SEARCH_FOR_A_USER'); ?>&nbsp;<input type="text" name="filter" value="">
														</div>
														<div class="btn-group hidden-phone">
															<button class="btn tip" type="button" onclick="addUser(this.form,'','<?php echo $this->contact_id; ?>','');" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-remove"></i></button>
															<button class="btn tip" type="button" onclick="document.adminForm.filter.value = '';addUser(this.form,'','<?php echo $this->contact_id; ?>','');" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-search"></i></button>
														</div>
													</div>

												</td>
											</tr>
											<tr>
												<td>
													&nbsp;
												</td>
											</tr>
										</table>
									<div id="userListArea">
										<table cellpadding="0" cellspacing="0" width="100%" border="0" class="table table-striped">
											<thead>
												<tr>
													<th width="28%" align="center">
														<?php echo JText::_('CC_NAME'); ?>
													</th>
													<th width="28%" align="center">
														<?php echo JText::_('CC_USER_NAME'); ?>
													</th>
													<th  align="center">
														<?php echo JText::_('CC_EMAIL'); ?>
													</th>
													<th width="8%" align="center">&nbsp;
													</th>
												</tr>
											</thead>
											<?php
											$k = 0;
											$db = JFactory::getDBO();
											for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
											{
												$row = &$this->rows[$i];
												$count_user = "";
												if($this->contact_id == '')
												{
													$sql = "SELECT count(*) FROM #__ccinvoices_users where user_id =".$row->id;
												}else
												{
													$sql = "SELECT count(*) FROM #__ccinvoices_users where user_id =".$row->id." AND contact_id =".$this->contact_id;
												}
												$db->setQuery($sql);
												$count_user = $db->loadResult();

											?>
											<tr class="<?php echo "row$k"; ?>">
												<td>
													<?php echo $row->name; ?>
												</td>
												<td>
													<?php echo $row->username; ?>
												</td>
												<td>
													<?php echo $row->email; ?>
												</td>
												<td>
													<?php if ($count_user == 0) { ?>
													<a href="javascript::void(0);" onclick="addUser(this.form,'<?php echo $row->id; ?>','<?php echo $this->contact_id; ?>','addUser')" ><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/user_add.png" width="16" height="16"></a>
													<?php }else { ?>
													<a href="javascript::void(0);" onclick="addUser(this.form,'<?php echo $row->id; ?>','<?php echo $this->contact_id; ?>','delUser')" ><img  src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/user_delete.png" width="16" height="16"></a>
													<?php } ?>
												</td>
											</tr>
									<?php
										$k = 1 - $k;
									}
									?>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>



		<?php

		$contactDetails = $this->contactDetails;
		?>
		</td>
		<td valign="top" >
			<table cellpadding="0" cellspacing="0" width="100%" border="0" style="margin-top:26px;">
				<tr>
					<td width="100%">
						<fieldset  >
							<legend><?php echo JText::_( 'CC_CONTACT_DETAILS' ); ?></legend>
							<table cellpadding="0" cellspacing="0" width="100%">
								<tr>
									<td>
										<?php echo JText::_("CC_INVOICE_NAME"); ?>:
									</td>
									<td>
									<?php
											if(strlen(@$contactDetails->name) > 25 )
											{
												echo substr(@$contactDetails->name, 0, 25)."...";
											}else
											{
												echo @$contactDetails->name;
											}
									?>
									</td>
								</tr>
								<tr>
									<td>
										<?php echo JText::_("CC_INVOICES_CONTACTS"); ?>:
									</td>
									<td>
									<?php
											if(strlen(@$contactDetails->contact) > 25 )
											{
												echo substr(@$contactDetails->contact, 0, 25)."...";
											}else
											{
												echo @$contactDetails->contact;
											}
									?>
									</td>
								</tr>
								<tr>
									<td>
										<?php echo JText::_("CC_EMAIL"); ?>:
									</td>
									<td>
									<?php
											if(strlen(@$contactDetails->email) > 25 )
											{
												echo substr(@$contactDetails->email, 0, 25)."...";
											}else
											{
												echo @$contactDetails->email;
											}
									?>
									</td>
								</tr>
								<tr>
									<td valign="top">
										<?php echo JText::_("CC_INVOICE_ADDRESS"); ?>:
									</td>
									<td>
										<?php
										$addStr = "";
										$addrSplit = explode("\n",@$contactDetails->address);
										for($i=0;$i<count($addrSplit);$i++)
										{
											if(strlen($addrSplit[$i]) > 25 )
											{
												$addStr .= substr($addrSplit[$i], 0, 25)."...\n";
											}else
											{
												$addStr .= $addrSplit[$i]."\n";
											}
										}

										$address_tmp = str_replace("\n", "<br/>", $addStr);
										echo $address_tmp;
										?>
									</td>
								</tr>

							</table>

						</fieldset>
					</td>
				</tr>
				<tr>
					<td width="100%">
						<fieldset  >
							<legend><?php echo JText::_( 'CC_USER_ASSIGNED_TO_CONTACTS' ); ?></legend>
							<div id = "userAssined">
							<p class="cchelpdesk_assinguserlist">
									<?php
									$userAssined_tmp = "<table cellspacing='0' cellpadding='0' class='table table-striped'>";
									for ($k=0, $n=count( $this->userAssined ); $k < $n; $k++)
									{
										$userAssined = &$this->userAssined[$k];
										$del_tmp = "'delUser'";
										$userAssined_tmp .= '<tr><td class="cchelpdesk_assinguserlist"><a href="javascript::void(0);" title="'.$userAssined->username.','.$userAssined->email.'" onclick="addUser(this.form,'.$userAssined->id.','.$this->contact_id.','.$del_tmp.')">'.$userAssined->name.'&nbsp;<img style="margin:2px;" src="'.JURI::root().'/administrator/components/com_ccinvoices/assets/images/user_delete.png" width="16" height="16"></a>,</td></tr>';
									}
									$userAssined_tmp.="</table>";
									echo $userAssined_tmp;
									?>
								</p>
							</div>
						</fieldset>
					</td>
				</tr>
			</table>
		</td>
	</tr>
</table>

<input type="hidden" name="path" value="<?php echo JURI::base();?>">
<input type="hidden" name="root_path" value="<?php echo JURI::root();?>">
</form>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>