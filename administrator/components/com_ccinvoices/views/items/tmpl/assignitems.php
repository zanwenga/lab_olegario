<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>
<script language="javascript" type="text/javascript">
function createFilter(filter)
{
	document.getElementById('filter_search').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter_search').value = '';
	submitform();
}
</script>
<script language ="javascript">



</script>


<style>
.adminlist
{
	font-size:11px;
}
.adminlist p
{
	margin:0px;
	padding:0px;
}
</style>
<script language="JavaScript" type="text/javascript">
  var commasOrDot= <?php echo $this->confRow->cformat;?>;
</script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/invoice-contactc-list.js"></script>
<form action="index.php" method="post" name="adminForm" id="adminForm">

<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
		<td>
		<ul class="nav nav-tabs">
			<li class="active"><a href="#page-site" data-toggle="tab"><?php echo JText::_('CC_ADD_EXISTING_ITEM');?></a></li>
		</ul>
	<table>
	<tr>
		<td align="left" width="100%">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="" />
				</div>
				<div class="btn-group hidden-phone">
					<button class="btn tip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn tip" type="button" onclick="document.id('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
				</div>
			</div>
		</td>
	</tr>
</table>
<table class="table table-striped">
<thead>
	<tr>
		<th width="3%" nowrap="nowrap" align="center">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_ITEM_ID' ), 'it.item_id', $listDirn, $listOrder ); ?>
		</th>
		<th width="3%" nowrap="nowrap" align="center">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_INVOICES_ITEM_QTY' ), 'it.item_quantity', $listDirn, $listOrder ); ?>
		</th>
		<th width="20%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_NAME' ), 'it.item_name', $listDirn, $listOrder ); ?>
		</th>
		<th width="35%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_DESC' ), 'it.item_description', $listDirn, $listOrder ); ?>
		</th>
		<th width="8%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_PRICE' ), 'it.item_price_excl_tax', $listDirn, $listOrder ); ?>
		</th>
        <th width="5%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_TAX' ), 'it.item_tax_percentage', $listDirn, $listOrder ); ?>
		</th>
        <th width="12%" align="center">
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="9">
			<?php
				echo $this->pagination->getLimitBox();
				echo $this->pagination->getListFooter();
			?>
		</td>
	</tr>
</tfoot>
<?php
$k = 0;
for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );


	?>
	<tr class="<?php echo "row$k"; ?>" >
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')"><?php echo $row->item_id; ?></a>
			<input type="hidden" id="item_id<?php echo $row->id; ?>" value="<?php echo $row->item_id; ?>" />
		</td>
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')"><?php echo $row->item_quantity; ?></a>
			<input type="hidden" id="item_quantity<?php echo $row->id; ?>" value="<?php echo $row->item_quantity; ?>" />
		</td>
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')">

			<?php
				if(strlen($row->item_name)>40)
				{
					echo substr($row->item_name,0,40)."....";
				}
				else
				{
					echo $row->item_name;
				}
			 ?>
			 </a>
			 <input type="hidden" id="item_name<?php echo $row->id; ?>" value="<?php echo $row->item_name; ?>" />
		</td>
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')">
			<?php
				if(strlen($row->item_description)>105)
				{
					echo substr($row->item_description,0,105)."....";
				}
				else
				{
					echo $row->item_description;
				}
			 ?>
			 </a>
			 <input type="hidden" id="item_description<?php echo $row->id; ?>" value="<?php echo $row->item_description; ?>" />
		</td>
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')">
			<?php
			if($this->row_conf->symbol_display == '1')
			{
				if($this->row_conf->cformat == 0)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ',');
				}else if($this->row_conf->cformat == 1)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, ',', '.');
				}else if($this->row_conf->cformat == 2)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ' ');
				}

			}else
			{
				if($this->row_conf->cformat == 0)
				{
					echo number_format($row->item_price_excl_tax, 2, '.', ',').$this->row_conf->currency_symbol;
				}else if($this->row_conf->cformat == 1)
				{
					echo number_format($row->item_price_excl_tax, 2, ',', '.').$this->row_conf->currency_symbol;
				}else if($this->row_conf->cformat == 2)
				{
					echo number_format($row->item_price_excl_tax, 2, '.', ' ').$this->row_conf->currency_symbol;
				}
			}
			?>
			</a>
			<input type="hidden" id="item_price_excl_tax<?php echo $row->id; ?>" value="<?php echo $row->item_price_excl_tax; ?>" />
		</td>
		<td align="center">
			<a href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')">
			<?php
				echo $row->item_tax_percentage." %";
			?>
			</a>
			<input type="hidden" id="item_tax_percentage<?php echo $row->id; ?>" value="<?php echo $row->item_tax_percentage; ?>" />
		</td>
		<td align="center">
			<a id="sbox-btn-close" href="javascript::void(0);" style="color:#666666;" onclick="addItem(this.form,'<?php echo $row->id; ?>')" ><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/user_add.png" width="16" height="16">&nbsp;<?php echo JText::_('CCINVOICES_ADD_EXT_ITEMTO_INV');?></a>
		</td>
	</tr>
	<?php
	$k = 1 - $k;
}
?>
</table>

		</td>
	</tr>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="modelassignItem" />
<input type="hidden" name="boxchecked"  id="boxchecked" value="0" />
<input type="hidden" name="controller" value="items" />
<input type="hidden" name="view" value="items" />
<input type="hidden" name="tmpl" value="component">
<input type="hidden" name="layout" value="assignitems">
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</form>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
