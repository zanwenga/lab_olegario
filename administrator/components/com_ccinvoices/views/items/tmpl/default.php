<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$saveOrder	= $listOrder == 'ordering';
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

if ($saveOrder)
{
	$saveOrderingUrl = 'index.php?option=com_ccinvoices&task=items.saveOrderAjax&tmpl=component';
	JHtml::_('sortablelist.sortable', 'articleList', 'adminForm', strtolower($listDirn), $saveOrderingUrl);
}
?>
<script language="javascript" type="text/javascript">
function createFilter(filter)
{
	document.getElementById('filter_search').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter_search').value = '';
	submitform();
}
</script>


<style>
.adminlist
{
	font-size:11px;
}
.adminlist p
{
	margin:0px;
	padding:0px;
}
</style>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<table>
	<tr>
		<td align="left" width="100%">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" placeholder="<?php echo JText::_('CCINVOICES_ITEM_SEARCH_BOX_PLACEHOLDER_LBL'); ?>" class="input-medium search-query" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="" />
				</div>
				<div class="btn-group hidden-phone">
					<button class="btn tip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn tip" type="button" onclick="document.id('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
				</div>
			</div>
		</td>
	</tr>
</table>
<table class="table table-striped" id="articleList">
<thead>
	<tr>
		<th width="1%" class="nowrap center hidden-phone">
			<?php echo JHtml::_('grid.sort', '<i class="icon-menu-2"></i>', 'ordering', $listDirn, $listOrder, null, 'asc', 'JGRID_HEADING_ORDERING'); ?>
		</th>
		<th width="1%" align="left">
			<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
		</th>
		<th width="3%" nowrap="nowrap" align="center">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_ITEM_ID' ), 'it.item_id', $listDirn, $listOrder ); ?>
		</th>
		<th width="2%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_QTY' ), 'it.item_quantity', $listDirn, $listOrder ); ?>
		</th>
		<th width="20%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_NAME' ), 'it.item_name', $listDirn, $listOrder ); ?>
		</th>
		<th width="40%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_DESC' ), 'it.item_description', $listDirn, $listOrder ); ?>
		</th>
		<th width="8%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_PRICE' ), 'it.item_price_excl_tax', $listDirn, $listOrder ); ?>
		</th>
		<th width="8%" align="center">
			<?php echo JText::_( 'CCINVOICES_ITEM_PRICE_INC_TAX' ); ?>
		</th>
        <th width="5%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ITEM_TAX' ), 'it.item_tax_percentage', $listDirn, $listOrder ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="10">
			<?php
				echo $this->pagination->getLimitBox();
				echo $this->pagination->getListFooter();
			?>
		</td>
	</tr>
</tfoot>
<?php
$k = 0;

for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$ordering	= $listOrder == 'ordering';
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );

	if($this->editpermission=="1")
		$link 		= JRoute::_( 'index.php?option=com_ccinvoices&task=items.edit&cid[]='. $row->id );
	else
		$link 		= "#";
		$canChange=1;
		$saveOrder=1;
	?>
	<tr class="<?php echo "row$k"; ?>" >
	<td class="order nowrap center hidden-phone">
	<?php if ($canChange) :
		$disableClassName = '';
		$disabledLabel	  = '';
		if (!$saveOrder) :
			$disabledLabel    = JText::_('JORDERINGDISABLED');
			$disableClassName = 'inactive tip-top';
		endif; ?>
		<span class="sortable-handler hasTooltip <?php echo $disableClassName?>" title="<?php echo $disabledLabel?>">
			<i class="icon-menu"></i>
		</span>
		<input type="text" style="display:none;" name="order[]" size="5"
			value="<?php echo $row->ordering;?>" class="width-20 text-area-order " />
	<?php else : ?>
		<span class="sortable-handler inactive" >
			<i class="icon-menu"></i>
		</span>
	<?php endif; ?>
	</td>
	<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>"><?php echo $row->item_id; ?></a>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>"><?php echo $row->item_quantity; ?></a>
		</td>
		<td align="center" class="nowrap has-context">
			<div class="pull-left">
				<a href="<?php echo $link; ?>">
					<?php
						if(strlen($row->item_name)>40)
						{
							echo substr($row->item_name,0,40)."....";
						}
						else
						{
							echo $row->item_name;
						}
					 ?>
				 </a>
			</div>
			<div class="pull-left">
				<?php
					// Create dropdown items
					JHtml::_('dropdown.edit', $row->id, 'items.');
					JHtml::_('dropdown.divider');
					JHtml::_('dropdown.trash', 'cb' . $i, 'items.');


					// Render dropdown list
					echo JHtml::_('dropdown.render');
					?>
			</div>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>">
			<?php
				if(strlen($row->item_description)>105)
				{
					echo substr($row->item_description,0,105)."....";
				}
				else
				{
					echo $row->item_description;
				}
			 ?>
			 </a>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>">
			<?php
			if($this->row_conf->symbol_display == '1')
			{
				if($this->row_conf->cformat == 0)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ',');
				}else if($this->row_conf->cformat == 1)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, ',', '.');
				}else if($this->row_conf->cformat == 2)
				{
					echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ' ');
				}

			}else
			{
				if($this->row_conf->cformat == 0)
				{
					echo number_format($row->item_price_excl_tax, 2, '.', ',').$this->row_conf->currency_symbol;
				}else if($this->row_conf->cformat == 1)
				{
					echo number_format($row->item_price_excl_tax, 2, ',', '.').$this->row_conf->currency_symbol;
				}else if($this->row_conf->cformat == 2)
				{
					echo number_format($row->item_price_excl_tax, 2, '.', ' ').$this->row_conf->currency_symbol;
				}
			}
			?>
			</a>
		</td>
		<td align="center">
			<?php
				if(!$row->item_tax_percentage)
				{
					if($this->row_conf->symbol_display == '1')
					{
						if($this->row_conf->cformat == 0)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ',');
						}else if($this->row_conf->cformat == 1)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, ',', '.');
						}else if($this->row_conf->cformat == 2)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax, 2, '.', ' ');
						}

					}else
					{
						if($this->row_conf->cformat == 0)
						{
							echo number_format($row->item_price_excl_tax, 2, '.', ',').$this->row_conf->currency_symbol;
						}else if($this->row_conf->cformat == 1)
						{
							echo number_format($row->item_price_excl_tax, 2, ',', '.').$this->row_conf->currency_symbol;
						}else if($this->row_conf->cformat == 2)
						{
							echo number_format($row->item_price_excl_tax, 2, '.', ' ').$this->row_conf->currency_symbol;
						}
					}
				}
				else
				{

					if($this->row_conf->symbol_display == '1')
					{
						if($this->row_conf->cformat == 0)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, '.', ',');
						}else if($this->row_conf->cformat == 1)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, ',', '.');
						}else if($this->row_conf->cformat == 2)
						{
							echo $this->row_conf->currency_symbol.number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, '.', ' ');
						}

					}else
					{
						if($this->row_conf->cformat == 0)
						{
							echo number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, '.', ',').$this->row_conf->currency_symbol;
						}else if($this->row_conf->cformat == 1)
						{
							echo number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, ',', '.').$this->row_conf->currency_symbol;
						}else if($this->row_conf->cformat == 2)
						{
							echo number_format($row->item_price_excl_tax+(($row->item_price_excl_tax*$row->item_tax_percentage)/100), 2, '.', ' ').$this->row_conf->currency_symbol;
						}
					}
				}
			?>
		</td>
		<td align="center">
			<a href="<?php echo $link; ?>">
			<?php
				echo $row->item_tax_percentage." %";
			?>
			</a>
		</td>
	</tr>
	<?php
	$k = 1 - $k;
}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked"  id="boxchecked" value="0" />
<input type="hidden" name="controller" value="items" />
<input type="hidden" name="view" value="items" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>