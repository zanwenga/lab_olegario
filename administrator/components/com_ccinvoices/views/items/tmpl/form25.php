<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');

JHTML::_('behavior.tooltip');
$newtaxarra=explode(";",$this->row_conf->tax);

if(count($newtaxarra)>0)
{

	$i=1;
	$default_tax="";
	$boxid="";
	foreach($newtaxarra as $val)
	{
		$tax[] = JHTML::_('select.option', $val, $val);
		if(trim($this->row->id)=="")
		{
			if($val==$this->row_conf->default_tax)
			{
				$default_tax=$val;
				$boxid=$val;
			}
		}


		if($this->confRow->cformat!="1")
		{
			if(@$this->row->item_tax_percentage==$val)
			{
				$boxid=$val;
			}
		}
		else
		{
			if(@str_replace(".",",",$this->row->item_tax_percentage)==$val)
			{
				$boxid=$val;
			}
		}

		$i=$i+1;
	}
}

?>
<script type="text/javascript">
    window.addEvent('domready', function(){
	   //do your tips stuff in here...
	   var zoomTip = new Tips($$('.hasTip3'), {
	      className: 'custom3', //this is the prefix for the CSS class
	      offsets: {
	  		'x': 20,       //default is 16
	  		'y': 30        //default is 16
              },
	      initialize:function(){
	         this.fx = new Fx.Style(this.toolTip, 'opacity',
	        		 {duration: 1000, wait: false}).set(0);
	      },
	      onShow: function(toolTip) {
	         this.fx.start(0,.8);
	      },
	      onHide: function(toolTip) {
	         this.fx.start(.8,0);
	      }
	   });
	});
</script>
<script language="javascript">
<?php if($this->confRow->cformat!="1"){ ?>

function isNNumeric(valuee)
{
	if(isNaN(valuee))
	{
		document.getElementById('item_price_excl_tax').value='';
	}
}
<?php } ?>

Joomla.submitbutton = function(task)
{

	var form =document.adminForm;
	if (task =='items.cancel')
	{
		Joomla.submitform(task);
		return;
	}
	if (form.item_name.value == "")
	{
    	alert( "<?php echo JText::_( 'CC_ENTER_ITEM_NAME'); ?>" );
        form.email.focus();
	}else if (form.item_price_excl_tax.value == "")
	{
    	alert('<?php echo JText::_("CC_ENTER_ITEM_PRICE"); ?>');
        form.email.focus();
	}else
	{
		Joomla.submitform(task);
	}
}

function removeQty()
{
	document.getElementById('item_quantity').value='';
}

</script>

<style>
#headder
{
	background-color:#F4F4F4;
	padding-left:5px;
	font-weight:bold;
}
#subitems
{
	background-color:#ffffff;
	padding-left:5px;
}
</style>

<table cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" width="100%">
<table cellpadding="0" cellspacing="0" border="0" border="0">
<tr><td align="center" width="100%">
<form action="index.php" method="post" name="adminForm" id="adminForm" autocomplete="off">
	<div>
		<fieldset class="adminform" style="padding:10px;">
		<legend><?php echo JText::_( 'CC_INVOICES_ITEM_FIELDSET' ); ?></legend>
			<table class="admintable" cellspacing="0" cellpadding="0">
				<tr>
					<td >
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td id="headder" width="35">
									<div class="hasTip" title="<?php echo JText::_( 'CC_INVOICES_ITEM_ID_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_INVOICES_ITEM_ID' ) ?></div>
								</td>
								<td id="headder" width="56">
									<?php echo JText::_( 'CC_INVOICES_ITEM_QTY' ); ?>
								</td>
								<td id="headder">
									<?php echo JText::_( 'CC_INVOICES_ITEM_NAME' ); ?>
								</td>
							</tr>
						</table>
					</td>
					<td id="headder">
						<?php echo JText::_( 'CC_INVOICES_ITEM_DESC' ); ?>
					</td>
					<td id="headder">
						<?php echo JText::_( 'CC_INVOICES_ITEM_PRICE' ); ?>
					</td>
					<td id="headder">
						<?php echo JText::_( 'CC_INVOICES_ITEM_TAX' ); ?>
					</td>
				</tr>
				<tr>
					<td colspan="4">
  						<hr style="border:1px solid #e4e4e2 !important;">
					</td>
				</tr>
				<tr >
					<td id="subitems" valign="top">
						<table cellspacing="0" cellpadding="0">
							<tr>
								<td valign="top" >
									<input type="text" name="item_id" value="<?php echo @$this->row->item_id;?>" size="2" maxlength="3">
								</td>
								<td width="55" valign="top" style="padding-left:5px;">
									<input type="text" name="item_quantity" id="item_quantity" value="<?php if($this->row->id==""){ echo "1"; }else{ echo $this->row->item_quantity;}?>" onclick="removeQty()" size="2" maxlength="10">
								</td>
								<td valign="top" style="padding-left:5px;">
									<input type="text" name="item_name" value="<?php echo @$this->row->item_name;?>" size="45" maxlength="100">
								</td>
							</tr>
						</table>
						<br>
						<?php echo sprintf(JText::_('CC_INVOICES_ITEM_LAST_ID'),$this->lastitemid); ?>
					</td>
					<td id="subitems" valign="top" >
						<textarea name="item_description" cols="2" style="font-size: 1.08em;width:200px;" rows="3" maxlength="350"><?php echo @$this->row->item_description;?></textarea>
					</td>
					<td id="subitems" valign="top">
						<input type="text" name="item_price_excl_tax" id="item_price_excl_tax"  onkeyup="isNNumeric(this.value)" size="20" value="<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",@$this->row->item_price_excl_tax); }else{ echo str_replace(",",".",@$this->row->item_price_excl_tax); } ;?>">
					</td>
					<td id="subitems" valign="top">
						<?php
							if($this->confRow->cformat=="1"){ $boxid= str_replace(".",",",$boxid); }else{ $boxid= str_replace(",",".",$boxid); }
						?>
						<?php echo JHTML::_('select.genericlist',   $tax, 'item_tax_percentage', '', 'value', 'text',$boxid); ?>
					</td>
				</tr>
			</table>
		</fieldset>
	</div>
	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid[]" value="" />
	<input type="hidden" name="option" value="com_ccinvoices" />
	<input type="hidden" name="controller" value="items" />
	<input type="hidden" name="task" value="" />
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
</td></tr></table></td></tr></table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>