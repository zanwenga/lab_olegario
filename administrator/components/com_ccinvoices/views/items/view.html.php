<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

class ccInvoicesViewitems extends CCINVOISViews
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = JFactory::getDBO();

		// Access check.
		if (!JFactory::getUser()->authorise('ccinvoices.view.ccitems', 'com_ccinvoices'))
		{
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}

		if($this->_layout == 'form')
		{
			if (!JFactory::getUser()->authorise('ccinvoices.edit', 'com_ccinvoices'))
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}

			$this->addToolBarForm();
			if(versionCompare()>=3)
			{
				$this->_layout='form';
			}
			else
			{
				$this->_layout='form25';
			}
		}
		elseif($this->_layout == 'assignitems' || $this->_layout=='assignitems25')
		{
			$document = JFactory::getDocument();
			$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/css/ccinvoice.css' );
			$inv_id = JRequest::getInt("inv_id","");
			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');
			$this->items      = $this->get('Items');
			$this->pagination = $this->get('Pagination');
			$this->state      = $this->get('State');

			require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
			$confModel =new ccInvoicesModelConfiguration();
			$confModel->setId(1);
			$confRow = $confModel->getData();
			$this->confRow=$confRow;
			$this->assignRef("inv_id",$inv_id);
		}
		else
		{
			JHTML::_('behavior.calendar');

			$document = JFactory::getDocument();
			$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/css/style.css' );
			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');

			$this->items      = $this->get('Items');
			$this->pagination = $this->get('Pagination');
			$this->state      = $this->get('State');

			// Check for errors.
			if (count($errors = $this->get('Errors')))
			{
				JError::raiseError(500, implode("\n", $errors));
				return false;
			}

			if(versionCompare()>=3)
			{
				$this->sidebar = JHtmlSidebar::render();
				$this->_layout='default';
			}
			else
			{
				$this->_layout='default25';
			}
			$this->addToolBar();
		}
		$sql = "SELECT currency_symbol,symbol_display,cformat,tax,default_tax FROM #__ccinvoices_configuration where id = 1 LIMIT 1 ";
		$db->setQuery($sql);
		$row_conf =  $db->loadObject();
		$this->assignRef('row_conf',$row_conf);
		$this->getVersionInfo();

		parent::display($tpl);
	}
	function addToolBarForm()
	{
			$row		= $this->get('singleItems');


			$text = $row->id ? JText::_( 'CC_EDIT' ) : JText::_( 'CC_NEW' );

			JToolBarHelper::title( JText::_( 'CC_INVOICES' ).': ' . $text.' ', 'ccinvoices.png' );
			JToolBarHelper::apply('items.apply',JText::_( 'CCINVOICES_ITEMS_APPLY_TOOL' ));
			JToolBarHelper::save('items.save',JText::_( 'CCINVOICES_ITEMS_SAVE_TOOL' ));
			JToolBarHelper::custom( 'items.save2new', 'save-new.png', 'new_f2.png',JText::_( 'CCINVOICES_ITEMS_SAVENEW_TOOL' ), false,  false );
			if($row->id)
				JToolBarHelper::custom( 'items.save2copy', 'save-copy.png', 'copy_f2.png',JText::_( 'CCINVOICES_ITEMS_SAVECOPY_TOOL' ), false,  false );
			JToolBarHelper::cancel('items.cancel',JText::_( 'CCINVOICES_ITEMS_CLOSE_TOOL' ));

			$this->lastitemid=$this->getModel()->getLastItemId();
			$this->assignRef('row',			$row);

			require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
			$confModel =new ccInvoicesModelConfiguration();
			$confModel->setId(1);
			$confRow = $confModel->getData();
			$this->confRow=$confRow;
	}
	function getVersionInfo()
	{

		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';

		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);

		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
	function addToolBar()
	{
		JToolBarHelper::title(   JText::_( 'CC_INVOICES' ) . ':  ' .  JText::_( 'CC_ITEM' ).' ', 'ccinvoices.png' );
		$editpermission="1";
		$canDo = ccinvoicesHelper::getActions();
		if ($canDo->get('ccinvoices.create'))
			JToolBarHelper::addNew('items.add',JText::_( 'CCINVOICES_ITEMS_NEW_TOOL' ));
		if ($canDo->get('ccinvoices.edit'))
			JToolbarHelper::editList('items.edit',JText::_( 'CCINVOICES_ITEMS_EDIT_TOOL' ));
		if ($canDo->get('ccinvoices.copy'))
		{
			JToolBarHelper::custom( 'items.copy', 'copy.png', 'copy_f2.png',  JText::_( 'CC_INVOICES_TOOLBAR_COPY') , true );
		}
		if ($canDo->get('ccinvoices.delete'))
			JToolbarHelper::deleteList('', 'items.remove', 'CCINVOICES_ITEMS_REMOVE_TOOL');
		if ($canDo->get('core.admin')) {
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_ccinvoices','500','800', JText::_( 'CCINVOICES_PERMISSION_TOOL_LBL' ));
		}
		$editpermission=$canDo->get('ccinvoices.edit');
		$this->assignRef('editpermission',		$editpermission);
	}

}
?>