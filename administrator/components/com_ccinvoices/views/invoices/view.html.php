<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

class ccInvoicesViewInvoices extends CCINVOISViews
{
	function display($tpl = null)
	{
		$mainframe = JFactory::getApplication();

		// Access check.
		if (!JFactory::getUser()->authorise('ccinvoices.view.invoices', 'com_ccinvoices'))
		{
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}

		$db = JFactory::getDBO();
		$model =  $this->getModel();
		jimport('joomla.html.pagination');
		if($this->_layout == 'form' || $this->_layout == 'contactform' )
		{
			require_once JPATH_COMPONENT.'/helper/ccinvoices.php';
			$editstatus="1";

			$canDo = ccinvoicesHelper::getActions();
			$editstatus=$canDo->get('ccinvoices.edit.state');
			$taskk=JRequest::getVar('task','');

			if (!JFactory::getUser()->authorise('ccinvoices.edit', 'com_ccinvoices') AND $taskk!="add")
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}

			$invoiceRow		= $this->get('invoices');

			$contactsRow=array();
			if($invoiceRow->contact_id!="")
			{
				$sql = "SELECT * FROM #__ccinvoices_contacts where id = ".$invoiceRow->contact_id." LIMIT 1 ";
				$db->setQuery($sql);
				$contactsRow =  $db->loadObject();
			}

			require_once(JPATH_COMPONENT.DS.'models'.DS.'configuration.php');
			$confModel =new ccInvoicesModelConfiguration();
			$confModel->setId(1);
			$confRow = $confModel->getData();

			if($invoiceRow->invoice_date == '')
			{
				$invoiceRow->invoice_date = date("Y-m-d", time());
			}
			if($invoiceRow->id != '')
			{

				$selectDefaultTax = explode("|",$invoiceRow->tax);
			}else
			{
				if($confRow->default_tax != '')
				{
				$selectDefaultTax[0] = $confRow->default_tax;
			}
				else
				{
					$selectDefaultTax[0] = "";
				}
			}
			if($invoiceRow->id != '')
			{
				$defDueDate = $invoiceRow->duedate;

			}else
			{
				$defDueDate = date("Y-m-d", mktime(0, 0, 0, date("m") , date("d") + $confRow->default_due_days, date("Y")));
			}

			if($invoiceRow->id != '')
			{
				$defNote = $invoiceRow->note;
			}else
			{
				$defNote = $confRow->default_note;
			}

			$arrTaxVal = array();
			if($confRow->tax != '')
			{
				jimport('joomla.utilities.arrayhelper');
				$splitTax = explode(";",$confRow->tax);
				if(count($splitTax) > 0)
				{
					for($i=0;$i<count($splitTax);$i++)
					{
						$tmp['value'] = $splitTax[$i] ;
						$arrTaxVal[$i] = JArrayHelper::toObject($tmp);
					}
				}
			}

			$query	= "SELECT max(contact_number) FROM #__ccinvoices_contacts";
		    $db->setQuery($query);
		    $contact_id1 = $db->loadResult();
			$contact_number = $contact_id1 + 1 ;


			$invoicetax = $arrTaxVal;



			//$invoicetax=$model->invoicetax();
			$sql = "SELECT max(number) FROM #__ccinvoices_invoices where reset_inv = '' ";
			$db->setQuery($sql);
			$nextInvoieNo = $db->loadResult();
			if($nextInvoieNo == '')
			{
				$nextInvoieNo = "Empty";
			}

			$this->assignRef('editstatus',		$editstatus);

			$this->assignRef('nextInvoieNo',		$nextInvoieNo);
			$this->assignRef('contact_number',		$contact_number);
			$this->assignRef('invoicetax',		$invoicetax);
			$this->assignRef('invoiceRow',		$invoiceRow);
			$this->assignRef('contactsRow',		$contactsRow);
			$this->assignRef('selectDefaultTax',		$selectDefaultTax);
			$this->assignRef('defDueDate',		$defDueDate);
			$this->assignRef('defNote',		$defNote);
			$this->lastcontactid=$this->getModel()->getLastContactId();
		    $clientid=JRequest::getVar('clientid');
		    $contactinvoice=$model->contactinvoice($clientid);
		    $this->assignRef('contactinvoice',		$contactinvoice);
		    $invoicenumbercheck=$model->invoicenumbercheck();
            $invoicenoval=$invoicenumbercheck+1;
		   // $invoicenumbercheck1=$model->invoicenumbercheck1($invoicenoval);
		    $invoicenumbercheck1=$invoicenoval;

			$query = 'SELECT count(number) from #__ccinvoices_invoices where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($confRow->invoice_start > $invoicenumbercheck1)
			{
				$new_inv_id1 = $confRow->invoice_start;
			}else
			{
				if($invoice_count == "0")
				{
					if($confRow->invoice_start == '')
					{
						$new_inv_id1 = 1;
					}else
					{
					$new_inv_id1 = $confRow->invoice_start;
					}
				}else
				{
					$new_inv_id1 = $invoicenumbercheck1;
				}
			}

		    $this->assignRef('numbercheck',		$new_inv_id1);
		    $lastinvoicenumber=$model->lastinvoicenumber();
		    $this->lastitemid=$this->getModel()->getLastItemId();

		    $this->assignRef('lastinvoicenumber',		$lastinvoicenumber);
            $status[] = JHTML::_('select.option',  '1',  JText::_( 'CC_CONCEPT'));
            $status[] = JHTML::_('select.option',  '2', JText::_( 'CC_OPEN'));
            $status[] = JHTML::_('select.option',  '3', JText::_( 'CC_LATE'));
            $status[] = JHTML::_('select.option',  '4', JText::_( 'CC_PAID') );
            $this->assignRef('status',	$status);

			$this->confRow=$confRow;
			if(versionCompare()>=3)
			{
				$this->_layout='form';
			}
			else
			{
				$this->_layout='form25';
			}
			$this->addToolBarForm($invoiceRow);
			$this->addCss();
		}
		else if($this->_layout == 'invoice_contact')
		{
        	$contactname = JRequest::getVar('contactname');
        	$contactdetails=$model->contactsearch($contactname);
			$this->assignRef('contactdetails',		$contactdetails);
		}else if($this->_layout =='getinvoicecontact')
		{
			$contactid = JRequest::getInt('contactid');
        	$value=$model->getinvoicecontact($contactid);
			$this->assignRef('getinvoicecontact',		$value);
		}else
		{
			$this->items      = $this->get('Items');
			$this->pagination = $this->get('Pagination');
			$this->state      = $this->get('State');


			// Check for errors.
			if (count($errors = $this->get('Errors')))
			{
				JError::raiseError(500, implode("\n", $errors));
				return false;
			}

			JHTML::_('behavior.calendar');
			$clientid = JRequest::getInt("clientid","");



			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','id');
			$sortOrder		= JRequest::getVar('filter_order_Dir','desc');
			$rows		= $this->get('data');



			$sql = "SELECT currency_symbol,symbol_display,cformat FROM #__ccinvoices_configuration where id = 1 LIMIT 1 ";
			$db->setQuery($sql);
			$row_conf =  $db->loadObject();
			$currency_symbol = $row_conf->currency_symbol;
			$symbol_display = $row_conf->symbol_display;
			// table ordering


			// search filter
            $status[] = JHTML::_('select.option',  '1',  JText::_( 'CC_CONCEPT'));
            $status[] = JHTML::_('select.option',  '2', JText::_( 'CC_OPEN'));
            $status[] = JHTML::_('select.option',  '3', JText::_( 'CC_LATE'));
            $status[] = JHTML::_('select.option',  '4', JText::_( 'CC_PAID') );
            $this->assignRef('status',	$status);


			if(versionCompare()>=3)
			{

			}
			else
			{
				$status1[] = JHTML::_('select.option',  '0',  JText::_( 'CC_SELECT'));
			}
            $status1[] = JHTML::_('select.option',  '1',  JText::_( 'CC_CONCEPT'));
            $status1[] = JHTML::_('select.option',  '2', JText::_( 'CC_OPEN'));
            $status1[] = JHTML::_('select.option',  '3', JText::_( 'CC_LATE'));
            $status1[] = JHTML::_('select.option',  '4', JText::_( 'CC_PAID') );
            $status1[] = JHTML::_('select.option',  '-1', JText::_( 'CCINVOICES_STATUS_ALL') );


            $this->assignRef('status1',	$status1);
			$sts=$mainframe->getUserStateFromRequest('com_ccinvoices.invoices.status1', 'status1', 0, 'int');
			$this->assignRef('Newstatus',	$sts);

			$this->assignRef('clientid',		$clientid);
			$this->assignRef('currency_symbol',		$currency_symbol);
			$this->assignRef('symbol_display',		$symbol_display);
			$this->assignRef('cformat',		$row_conf->cformat);
			if(versionCompare()>=3)
			{
				foreach ($this->items as &$item) {
					$this->ordering[$item->id][] = $item->id;
				}
				$this->addFilter();
				$this->sidebar = JHtmlSidebar::render();
				$this->_layout='default';
			}
			else
			{
				$this->_layout='default25';
			}
			$this->addToolBar();
			$this->addCssDefa();
		}
		$sql = "SELECT 	date_format FROM #__ccinvoices_configuration WHERE id=1 ";
		$db->setQuery($sql);
		$date_format = $db->loadResult();


		$this->getVersionInfo();


		$this->assignRef('date_format',$date_format);
		parent::display($tpl);
	}
	function addToolBar()
	{
		JToolBarHelper::title(JText::_( 'CC_INVOICES' ) . ':  ' .  JText::_( 'CC_INVOICES_INVOICES' ).' ', 'ccinvoices.png' );
		$editpermission="1";
		$editstatus="1";

		$canDo = ccinvoicesHelper::getActions();
		$editstatus=$canDo->get('ccinvoices.edit.state');
		if ($canDo->get('ccinvoices.create'))
		{
			//JToolBarHelper::addNewX('add',JText::_( 'CCINVOICES_INVOICES_NEW_TOOL' ));
			JToolBarHelper::addNew('invoices.add',JText::_( 'CCINVOICES_INVOICES_NEW_TOOL' ));
		}
		$editpermission=$canDo->get('ccinvoices.edit');
		if ($canDo->get('ccinvoices.edit'))
		{
			//JToolBarHelper::editListX('edit',JText::_( 'CCINVOICES_INVOICES_EDIT_TOOL' ));
			JToolbarHelper::editList('invoices.edit');
			JToolBarHelper::custom( 'invoices.copy', 'copy.png', 'copy_f2.png',  JText::_( 'CC_INVOICES_TOOLBAR_COPY') , true );
		}
		if ($canDo->get('ccinvoices.delete'))
		{
			JToolbarHelper::deleteList('', 'invoices.remove', 'CCINVOICES_INVOICES_REMOVE_TOOL');
		}
		if ($canDo->get('core.admin')) {
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_ccinvoices','500','800', JText::_( 'CCINVOICES_PERMISSION_TOOL_LBL' ));
		}
		$this->assignRef('editstatus',		$editstatus);
		$this->assignRef('editpermission',		$editpermission);
	}
	function addToolBarForm($invoiceRow)
	{
        $text = $invoiceRow->id ? JText::_( 'CC_EDIT' ) : JText::_( 'CC_NEW' );
        JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).':  ' . $text.' ', 'ccinvoices.png' );
		JToolBarHelper::apply('invoices.apply',JText::_( 'CCINVOICES_INVOICES_APPLY_TOOL' ));
		JToolBarHelper::save('invoices.save',JText::_( 'CCINVOICES_INVOICES_SAVE_TOOL' ));
		JToolBarHelper::custom( 'invoices.save2new', 'save-new.png', 'new_f2.png', JText::_( 'CCINVOICES_INVOICES_SAVENEW_TOOL' ), false,  false );
		if($invoiceRow->id)
			JToolBarHelper::custom( 'invoices.save2copy', 'save-copy.png', 'copy_f2.png', JText::_( 'CCINVOICES_INVOICES_SAVECOPY_TOOL' ), false,  false );
		JToolBarHelper::cancel('invoices.cancel',JText::_( 'CCINVOICES_INVOICES_CLOSE_TOOL' ));
	}
	function getVersionInfo()
	{

		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';

		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);

		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
	function addCss()
	{
	    $document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/main.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/thickbox.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/highslide.css');
	}
	function addCssDefa()
	{
	    $document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/thickbox.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/highslide.css');
	}
	function addFilter()
	{
		JHtmlSidebar::addFilter(
			JText::_('CCINVOICES_INVOICES_SELECT_STATUS'),
			'status1',
			JHtml::_('select.options',  $this->status1, 'value', 'text', $this->state->get('filter.status1'))
		);
	}
}
?>