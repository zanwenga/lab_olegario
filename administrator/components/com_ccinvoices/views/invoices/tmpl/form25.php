<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');

$jversion = new JVersion();
$current_version =  $jversion->getShortVersion();
 if(substr($current_version,0,3)!="1.5")
 {
	echo $this->getModel()->TinyMCELoad();
 }
?>
<script language="JavaScript" type="text/javascript">
  var commasOrDot= <?php echo $this->confRow->cformat;?>;
  <?php if(versionCompare()>=3){ ?>
  	var delbtnJM=3;
  <?php }else{ ?>
  	var delbtnJM=2;
  <?php } ?>
</script>

<?php

$invRow = $this->invoiceRow;
$contactsRow = $this->contactsRow;
$item_id=explode("|#$|",$invRow->item_id);
$quantityname=explode("|#$|",$invRow->quantity);
$productname=explode("|#$|",$invRow->pname);
$item_description=explode("|#$|",$invRow->item_description);
$productprice=explode("|#$|",$invRow->price);
$taxvalue=explode("|#$|",$invRow->tax);

$count=count($quantityname);

$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
?>
<style>
table#myTable tr td
{
	text-align:left;
}
</style>
<script language="javascript">
 var itemIdToolTip="<?php echo sprintf(JText::_('CC_INVOICES_ITEM_LAST_ID'),$this->lastitemid) ?>";

function submitbutton(pressbutton)
{
	if (pressbutton == 'cancel') {
		submitform( pressbutton );
		return;
	}
	if(document.getElementById('contact_id').value=="")
	{
		alert("<?php echo JText::_("CC_MSG_PLEASE_SELECT_CONTACT"); ?>");
		return false;
	}
	else if(document.getElementById("contact_name").value =="")
	{
		alert("<?php echo JText::_("CC_MSG_ENTER_COMPANY_NAME"); ?>");
		return;
	}
	var table12 = document.getElementById('myTable');
	var itemqtyid=table12.rows[3].cells[1].firstChild.getAttribute('id');
	var itemnmid=table12.rows[3].cells[2].firstChild.getAttribute('id');
	var itempriceid=table12.rows[3].cells[4].firstChild.getAttribute('id');

	if(document.getElementById(itemqtyid).value =="" || document.getElementById(itemnmid).value =="" || document.getElementById(itempriceid).value =="")
	{
		alert("<?php echo JText::_("CC_MSG_PLEASE_ADD_ITEMS"); ?>");
		return;
	}else
	{
		submitform( pressbutton );
		return;
	}
}

Joomla.submitbutton = function(task)
{

	if (task =='invoices.cancel')
	{
		Joomla.submitform(task);
		return;
	}

	if(document.getElementById('contact_id').value=="")
	{
		alert("<?php echo JText::_("CC_MSG_PLEASE_SELECT_CONTACT"); ?>");
		return false;
	}
	else if(document.getElementById("contact_name").value =="")
	{
		alert("<?php echo JText::_("CC_MSG_ENTER_COMPANY_NAME"); ?>");
		return;
	}

	var table12 = document.getElementById('myTable');
	var itemqtyid=table12.rows[3].cells[1].firstChild.getAttribute('id');
	var itemnmid=table12.rows[3].cells[2].firstChild.getAttribute('id');
	var itempriceid=table12.rows[3].cells[4].firstChild.getAttribute('id');

	if(document.getElementById(itemqtyid).value =="" || document.getElementById(itemnmid).value =="" || document.getElementById(itempriceid).value =="")
	{
		alert("<?php echo JText::_("CC_MSG_PLEASE_ADD_ITEMS"); ?>");
		return;
	}else
	{
		Joomla.submitform(task);
		return;
	}
}


function DoTheCheck(value)
{
	if(value == 'send')
	{
		document.adminForm.resend.checked = false ;
		document.adminForm.reminder.checked = false ;
	}
	if(value == 'resend')
	{
		document.adminForm.send.checked = false ;
		document.adminForm.reminder.checked = false ;
	}
	if(value == 'reminder')
	{
		document.adminForm.send.checked = false ;
		document.adminForm.resend.checked = false ;
	}
}
function changeThisUrl()
{
	cont_id_tmp = document.getElementById("contact_id_url");
	temp = cont_id_tmp.href;
	if((temp.split('index.php').length -1) > 0)
	{
		cont_id_tmp.className = "modal";
		cont_id_tmp.rel = "{handler: 'iframe', size: {x: 680, y: 500}}"
	}else
	{
		cont_id_tmp.rel = "test";
		cont_id_tmp.className = "test"
		alert("<?php echo JText::_( 'CC_CHOOSE_CONTACT' ); ?>");
	}

}
function clearContact()
{
	document.adminForm.name.value = "";
	document.adminForm.contact_hidden.value = "";
	document.adminForm.contact_id.value = "";
	document.adminForm.contactname.value = "";
	document.adminForm.contact.value = "";
	document.adminForm.contact_number.value = "";
	document.adminForm.address.value = "";
	document.adminForm.cc_email.value = "";
	document.adminForm.cc_tax_id.value = "";

 	document.getElementById('contact_txt').innerHTML =  document.adminForm.add_con_msg.value;
 	cont_id_tmp = document.getElementById("contact_id_url");
 	cont_id_tmp.href = "javascript::void(0);";
	cont_id_tmp.rel = "test";
	cont_id_tmp.className = "test"
}
</script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>plugins/editors/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
 	editor_selector : "mceEditor",
	theme : "simple"
});

function checkinvoicestatus()
{
	if(document.getElementById("status").value!=1)
	{
		try {
			document.getElementById("send_to_contact1").style.display='block';
		} catch(e){}
		try {
			document.getElementById("send_to_contactbox").style.display='block';
		} catch(e){}

		document.getElementById("send_to_contact").style.display='block';
		document.getElementById("number").style.background="white";
		document.getElementById("number").disabled=false;
		document.getElementById("number").value=document.getElementById("hidnumber").value;
	}
	else
	{
		try {
			document.getElementById("send_to_contact1").style.display='none';
		} catch(e){}
		try {
			document.getElementById("send_to_contactbox").style.display='none';
		} catch(e){}
		document.getElementById("send_to_contact").style.display='none';
		document.getElementById("number").style.background="#EAEAEA";
		document.getElementById("number").value=0;
		document.getElementById("number").disabled=true;
		try {
			document.adminForm.send.checked = false ;
		} catch(e){}
		try {
			document.adminForm.resend.checked = false ;
		} catch(e){}
		try {
			document.adminForm.reminder.checked = false ;
		} catch(e){}
	}
	if(document.getElementById("status").value=="2")
	{
		document.getElementById("imagenotpaid").style.display='none';
	}
	else
	{
		document.getElementById("imagenotpaid").style.display='none';
	}
}

</script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/invoice-contactc-list.js"></script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/changeInvoiceStatus.js"></script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/getPaymentStatus.js"></script>

<?php JHTML::_('behavior.tooltip');
 ?>


<form action="index.php" method="post" name="adminForm" autocomplete="off">
<div class="clr"></div>
<table cellpadding="0" cellspacing="" width="100%"><tr><td align="center" width="100%">
<table cellpadding="0" cellspacing="0" border="0" width="62%">
	<tr>
		<td align="center" colspan="2" >
			<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contactbox">
			<table width="100%">
<?php
if($invRow->status!="1")
{
	if($invRow->communication)
	{
	?>
		<tr>
			<td align="center" colspan="2" ><div class="ccinvioce_communication"><?php echo JText::_( 'CC_INVOICE_EDIT_MAIL_MSG' ); ?></div></td>
		</tr>
	<?php
	}
}
?>
			</table>
			</span>
		</td>
	</tr>

	<tr>
		<td width="100%" colspan="2">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>
<td width="48%" valign="top" >
			<fieldset class="adminform" style="padding:10px;">
			<legend><?php echo JText::_( 'CC_INVOICE_DETAILS' ); ?></legend>
			<table class="admintable" cellspacing="1" border="0">
			<tr>
				<td width="220px"  class="key" style="font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_NUMBER' ); ?>
				</td>
				<td>
				<?php
					if($invRow->id == 0)
				    {
				?>
						<input class="text_area" type="text" name="number" id="number" size="25" style="background-color:#EAEAEA;" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>" disabled />

				        <br/>
				        <input class="text_area" type="hidden" name="hidnumber" id="hidnumber" size="25" style="background-color:#EAEAEA;" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>" disabled />
				        <input class="text_area" type="hidden" name="numbercheck" size="25" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>"  />
				       <?php echo JText::_( 'CC_LAST_INVOICE_ID' ); ?>
				        <?php
				        	echo $this->nextInvoieNo;
					}else
					{
						?>
						<input class="text_area" type="text" name="number" style="<?php echo ($invRow->status=="1")?'background-color:#EAEAEA;':''; ?>" id="number" size="25" maxlength="256" value="<?php  echo $invRow->number ?>"  <?php echo ($invRow->status=="1")?'disabled':''; ?>/>
						<br/>
						<input class="text_area" type="hidden" name="hidnumber" id="hidnumber" size="25" style="background-color:#EAEAEA;" maxlength="256" value="<?php echo $invRow->number; ?>" disabled />
						<input class="text_area" type="hidden" name="numbercheck" size="25" maxlength="256" value="<?php  echo $invRow->numbercheck ;?>"  />
						<?php echo JText::_( 'CC_LAST_INVOICE_ID' ); ?>
						<?php
							echo $this->nextInvoieNo;
					}
						?>
				</td>
				<Td></td>
			</tr>
			<tr>
				<td width="220px"  class="key" style="font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_DATE' ); ?>
				</td>
				<td>
					<?php echo JHTML::_('calendar', $invRow->invoice_date, 'invoice_date', 'invoice_date', '%Y-%m-%d'); ?>
				</td>
				<Td></td>
			</tr>
			<tr>
				<td width="220px"  class="key" style="font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_DUEDATE' ); ?>
				</td>
				<td>
					<input type="text" value="<?php echo $this->defDueDate; ?>" name="duedate" >
				</td>
				<Td></td>
			</tr>
			<tr>
				<td width="150px"  class="key" style="font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_STATUS' ); ?>
				</td>
				<td>
					<table>
						<tr>
							<td>

							<?php
								if($this->editstatus=="0")
								{
							 		echo JHTML::_('select.genericlist',   $this->status, 'status', 'class="inputbox" size="1"  onchange="checkinvoicestatus()" disabled="disabled" ', 'value', 'text', $invRow->status );
								}
								else
								{
									echo JHTML::_('select.genericlist',   $this->status, 'status', 'class="inputbox" size="1"  onchange="checkinvoicestatus()"  ', 'value', 'text', $invRow->status );
								}
							 ?></td>
							<td>
								<?php
									if($invRow->id!="0")
									{
								?>
								<span id="imagenotpaid" style="<?php echo ($invRow->status==2)?'display:none':'display:none'; ?>" onmouseover="getPaymentStatus()" onmouseout="removestatusimage()" onclick="updateInvoiceStatus(),getPaymentStatus()">
									<img id="imgid" src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/payment.png" title="">
								</span>
								<?php
									}
								?>
								<input type="hidden" name="imagealttext" id="imagealttext" value="<?php echo JText::_( 'CC_SET_STATUS_PAID' ); ?>">
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2">
					<input type="hidden" name"ajaxdisplay" id="ajaxdisplay" value="">
				</td>
			</tr>
			<tr>
				<td colspan="2" align="left">
					<div class="ccinvoices_imgaltertext" id="imgaltertext" align="left"></div>
				</td>
			</tr>
			<?php
	 		if(!$invRow->communication)
			{
			?>
			<tr>
				<td id="tmpid" align="right" colspan="2">
					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact">
						<table><tr><td><input id="send" type="checkbox" onclick="DoTheCheck('send')" value="1" name="send"></td><td>  <?php echo JText::_( 'CC_SEND_INVOICE_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php } ?>
			<?php
	 		if($invRow->communication)
			{
			?>
			<tr>
	       			<td >&nbsp;</td>
					<td align="left" >
					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact">
					<table><tr><td><input id="resend" type="checkbox" onclick="DoTheCheck('resend')" value="1" name="resend"></td><td>  <?php echo JText::_( 'CC_RESEND_INVOICE_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php
	 		if($invRow->status != '4')
			{
			?>
	       	<tr>
	       			<td >&nbsp;</td>
					<td align="left" >
					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact1">
					<table><tr><td><input id="reminder" type="checkbox" onclick="DoTheCheck('reminder')" value="1" name="reminder"></td><td>  <?php echo JText::_( 'CC_SEND_INVOICE_REM_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php } ?>

			<?php } ?>
			</table>
			</fieldset>
		</td>
		<td width="52%" >
			<fieldset class="adminform"  style="padding:10px;">
			<legend><?php echo JText::_( 'CC_INVOICES_CONTACTS' ); ?></legend>
			<table  class="admintable" width="100%">
			<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" border="0">
						<tr>
							<td width="43%" style="padding-right:2px;">
								<a class="modal"   rel="{handler: 'iframe', size: {x: 950, y: 500}}" id="existing_contact_url" href="index.php?option=com_ccinvoices&task=contacts.modelassigncontact&layout=assigncontact25&contact_id=<?php if(@$contactsRow->id==""){ echo @$this->contactinvoice->id; }else{ echo @$contactsRow->id; } ?>&tmpl=component" >
									<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td width="14" align="right" class="ccinvoices_edit_btn_left">
									<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/zoom.png" width="14" height="16">
									</td><td class="ccinvoices_edit_btn_right">
									<?php echo JText::_( 'CC_EXISTING_CONTACTS' ); ?>
									</td></tr></table>
								</a>
							</td>
							<td width="43%" style="padding-right:2px;">
								<a href="javascript:void(0);" onclick="updatecontact()" >
									<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" class="ccinvoices_edit_btn_left">
									<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/add.png" width="16" height="16">
									</td><td class="ccinvoices_edit_btn_right">
											<span id="contact_txt">
												<?php
												if( @$contactsRow->id != '' OR @$this->contactinvoice->id!='')
												{
													echo JText::_("CC_UPDATE_CONTACT");
												}else
												{
												 	echo JText::_("CC_CREATE_CONTACT");
												}
												?>
											</span>
									</td></tr></table>
								</a>
							</td>
							<td width="15%">
								<a class="modal"   rel="{handler: 'iframe', size: {x: screen.width-100, y: screen.height-200}}" id="existing_contact_url" href="index.php?option=com_ccinvoices&controller=contacts&task=modelassigncontact&layout=assigncontact&contact_id=<?php if(@$contactsRow->id==""){ echo @$this->contactinvoice->id; }else{ echo @$contactsRow->id; } ?>&tmpl=component" >
									<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" class="ccinvoices_edit_btn_left">
									<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/manage_users.png" width="16" height="16">
									</td><td class="ccinvoices_edit_btn_right">
											<?php
											$clid=JRequest::getVar('clientid','');
											 if(@$contactsRow->id == '' AND $clid==""){ ?>
											<a class="modal"  rel="{handler: 'iframe', size: {x: 950, y: 500}}" id="contact_id_url" onclick="changeThisUrl();" href="javascript:void(0);" ><?php echo JText::_("CC_MANAGE_USERS"); ?></a>
											<?php }else { ?>
											<a class="modal"  rel="{handler: 'iframe', size: {x: 950, y: 500}}" id="contact_id_url" href="index.php?option=com_ccinvoices&view=assignusers&layout=default25&contact_id=<?php if(@$contactsRow->id==""){ echo @$this->contactinvoice->id; }else{ echo @$contactsRow->id; } ?>&tmpl=component" ><?php echo JText::_("CC_MANAGE_USERS"); ?></a>
											<?php }?>
									</td></tr></table>
								</a>
							</td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td colspan="2" style="color: !important;border:1px solid #e4e4e2 !important;"></td>
			</tr>
			<tr>
				<td colspan="2" style="padding-top:5px;"><hr style="border:1px solid #e4e4e2 !important;"></td>
			</tr>
			<tr>
				<td width="220px" align="right" class="key" style="padding-right:10px;font-weight:bold;">
					<div class="hasTip" title="<?php echo JText::_( 'CC_INVOICE_NAME_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_INVOICE_NAME' ) ?></div>
				</td>
				<td>
		           <input type="text" size="40" id="contact_name" name="name" value="<?php if(@$contactsRow->name==""){ echo @$this->contactinvoice->name; }else{ echo @$contactsRow->name; } ?>" />
				   <input type="hidden" id="contact_hidden" name="contact_hidden" value="<?php if(@$contactsRow->id==""){ echo @$this->contactinvoice->id; }else{ echo @$contactsRow->id; } ?>" />
				   <input type="hidden" id="contact_id" name="contact_id" value="<?php if(@$contactsRow->id==""){ echo @$this->contactinvoice->id; }else{ echo @$contactsRow->id; } ?>" />
				   <input type="hidden" id="contactname" name="contactname" value="<?php if(@$contactsRow->name==""){ echo @$this->contactinvoice->name; }else{ echo @$contactsRow->name; } ?>" />
				</td>
			</tr>
			<tr>
				<td width="220px" align="right" class="key" style="padding-right:10px;font-weight:bold;">
						<div class="hasTip" title="<?php echo JText::_( 'CC_INVOICES_CONTACT_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_INVOICES_CONTACT' ) ?></div>
				</td>
				<td>
					<input class="text_area" type="text" name="contact" id="cc_contact" size="40" maxlength="256" value="<?php if(@$contactsRow->contact==""){ echo @$this->contactinvoice->contact; }else{ echo @$contactsRow->contact; } ?>"  />
				</td>
			</tr>
			<tr>
				<td width="100" align="right" class="key" style="padding-right:10px;font-weight:bold;">
					<div class="hasTip" title="<?php echo JText::_( 'CC_CONTACT_NUMBER_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_CONTACT_NUMBER' ); ?></div>

				</td>
				<td>
					<table cellspacing="0" cellpadding="0">
						<tr>
							<td>
	 				 			<input type="text" size="10" id="contact_number" name="contact_number" value="<?php if(@$contactsRow->contact_number==""){ echo @$this->contactinvoice->contact_number; }else{ echo @$contactsRow->contact_number; } ?>">
	 				 		</td>
	 				 	</tr>
						<tr>
							<td>
	 				 			<?php echo sprintf(JText::_( 'CC_INVOICES_LAST_CONTACT_NUMBER' ),$this->lastcontactid); ?>
	 				 		</td>
	 				 	</tr>
	 				 </table>
				</td>
			</tr>
			<tr>
				<td width="220px" align="right" class="key" style="padding-right:10px;font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_ADDRESS' ); ?>
				</td>
				<td>
					<textarea class="text_area"  name="address" id="cc_address" cols="28" style="font-size: 0.909em;width:190px;" rows="5" ><?php if(@$contactsRow->address==""){ echo @$this->contactinvoice->address; }else{ echo @$contactsRow->address; } ?></textarea>
				</td>
			</tr>
			<tr>
				<td width="100" align="right"  style="padding-right:10px;font-weight:bold;">
						<?php echo JText::_( 'CC_CONTACT_TAX_ID' ); ?>
				</td>
				<td>
					<input class="text_area" type="text" name="cc_tax_id" id="cc_tax_id" size="30" maxlength="30" value="<?php if(@$contactsRow->tax_id==""){ echo @$this->contactinvoice->tax_id; }else{ echo @$contactsRow->tax_id; } ?>"  />
				</td>
			</tr>
			<tr>
				<td width="220px" align="right" class="key" style="padding-right:10px;font-weight:bold;">
						<?php echo JText::_( 'CC_EMAIL' ); ?>
				</td>
				<td>
					<input class="text_area" type="text" name="cc_email" id="cc_email" size="40" maxlength="256" value="<?php if(@$contactsRow->email==""){ echo @$this->contactinvoice->email; }else{ echo @$contactsRow->email; } ?>"  />
				</td>

			</tr>
			<tr>
				<td colspan="2"><hr style="border:1px solid #e4e4e2 !important;"></td>
			</tr>
			<tr>
				<td colspan="2" width="100%" align="center">
					<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tr>
				  			<td width="100%" align="right" style="padding:5px 10px 0px 10px;">
								<table cellpadding="0" cellspacing="0" width="100%" border="0">
									<tr>
										<td width="48%" align="center">

											<div style="align:left;display:none;" id="contactajaxbox">
												<table><tr><td>
												<img id="tickimg" src="<?php echo JURI::root()."administrator/components/com_ccinvoices/assets/images/tick.png"; ?>">
												</td><td>
												&nbsp;&nbsp;
												<?php echo JText::_("CC_EDIT_CONTACT_LABEL"); ?>
												</td></tr></table>
											</div>
										</td>
										<td width="52%" align="right" valign="middle">
											<a href="javascript:void(0);" onclick="clearContact()" >
												<table width="70%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" class="ccinvoices_edit_btn_left">
												<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/delete.png">
												</td><td class="ccinvoices_edit_btn_right">
													<span id="clear_txt">
														<?php
															if( @$contactsRow->id != '')
															{
																echo JText::_("CC_CLEAR_CONTACT");
															}else
															{
															 	echo JText::_("CC_CLEAR_CONTACT");
															}
														?>
													</span>
												</td></tr></table>
											</a>
										</td>
										<!--<td width="50%" align="right" valign="middle">
											<div>
											<table border="0" cellspacing="0" cellpadding="0"><tr><td>
											<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/delete.png">
											</td><td >
											&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearContact()" >
											<span id="clear_txt">
											<?php
											if( @$contactsRow->id != '')
											{
												echo JText::_("CC_CLEAR_CONTACT");
											}else
											{
											 	echo JText::_("CC_CLEAR_CONTACT");
											}
											?>
											</span></a>
											</td></tr></table>
											</div>
										</td>-->
									</tr>
								</table>
	  			</td>
			</tr>
			</table>
				</td>
			</tr>
			</table>
			</fieldset>
		</td>
	</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
			<fieldset class="adminform" style="padding:5px;">
			 <legend><?php echo JText::_( 'CC_INVOICE_ITEMS' ); ?></legend>
				<table  cellpadding="0" cellspacing="0" width="100%">
						<tr>
							<td>
								<table class="admintable">
									<tr>
										<Td>
											<a class="modal"   rel="{handler: 'iframe', size: {x: 900, y: 500}}" href="index.php?option=com_ccinvoices&task=items.modelassignItem&layout=assignitems25&tmpl=component" >
												<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" class="ccinvoices_edit_btn_left">
												<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/zoom.png"  height="16">
												</td><td class="ccinvoices_edit_btn_right">
												<?php echo JText::_( 'CC_INVOICE_ADD_EXIST_ITEMS' ); ?>
												</td></tr></table>
											</a>
										</td>
										<td>
											&nbsp;&nbsp;
										</td>
										<Td>
											<a href="javascript:void(0);" onclick="insRow1(document.getElementById('myTable').rows.length-3,'1')">
												<table class="buttoncl"><Tr><td align="right" class="ccinvoices_edit_btn_left">
												<input type="button"  class="itemadd"  value="">
												</td><td class="ccinvoices_edit_btn_right">
												<?php echo JText::_( 'CC_INVOICE_ADDITEMS' ); ?>
												</td></tr></table>
											</a>
										</td>
									</tr>
								</table>
							</td>
							<input type="hidden" name="totalitems" id="totalitems" value="<?php echo $count;?>" >
						</tr>

						<tr>
						<td >
<style>
#headder
{
	background-color:#F4F4F4;
	padding-left:5px;
	font-weight:bold;
}
#subitems0
{
	background-color:#ffffff;
	padding-left:5px;
}
#subitems1
{
	background-color:#F4F4F4;
	padding-left:5px;
}
</style>
							<table class="admintable" id="myTable" width="100%">
								<tr>
									<td colspan="7" style="padding-top:5px;"><hr style="border:1px solid #e4e4e2 !important;"></td>
								</tr>
			  					<tr align="left" >
			  						<td style="padding-left:5px;"><div class="hasTip" title="<?php echo JText::_( 'CC_INVOICES_ITEM_ID_TOOLTIP' ) ?>"><b><?php echo JText::_( 'CC_INVOICES_ITEM_ID' ); ?></b></div></td>
									<td style="padding-left:5px;"><b><?php echo JText::_( 'CC_QTY' ); ?></b></td>
									<td style="padding-left:5px;"><b><?php echo JText::_( 'CC_ITEM_NAME_LBL' ); ?></b></td>
									<td style="padding-left:5px;"><b><?php echo JText::_( 'CC_INVOICES_ITEM_DESC' ); ?></b></td>
			                        <td style="padding-left:5px;"><b><?php echo JText::_( 'CC_PRICE' ); ?></b></td>
			                        <td style="padding-left:5px;"><b><?php echo JText::_( 'CC_TAX' ); ?></b></td>
			                       <!--<td><b><?php echo JText::_( 'CC_ADD' ); ?></b></td>-->
			                        <td style="padding-left:5px;">&nbsp;</td>
			                    </tr>
								<tr>
									<td colspan="7" style="padding-bottom:5px;"><hr style="border:1px solid #e4e4e2 !important;"></td>
								</tr>
			                  		<?php

			                  		if(!empty($invRow->quantity))
			                  		{

			                  		}
			                  		else
			                  		{
			                  			$idvalue="my1Div";
			                  			$boxid='my1DivSel';
			                  			$quantityid='my1DivFT';
			                  			$priceid='my1DivST';
			                  		}
			                  		$tm="0";

						 for($i=0;$i<$count;$i++){

						 	$j=1;
						 	$j=$i+$j;
						 	$idvalue="my".$j."Div";
						 	$boxid="my".$j."DivSel";
						 	$quantityid="my".$j."DivFT";
			                $priceid="my".$j."DivST";
			                $productid="my".$j."Product";
			                $Descriptionid="my".$j."Description";
			                $Itemids="my".$j."ItemId";
			                $ItemQTYid="my".$j."Itemqty";
			                $myDivDel="my".$j."DivDel";
			                $mysubDivDel="my".$j."subDivDel";
			            ?>
			                    <tr id="<?php echo $idvalue?>" >
			                    		<td  id="subitems<?php echo $tm;?>" valign="top">
			                    			<div class="hasTip" title="<?php echo sprintf(JText::_('CC_INVOICES_ITEM_LAST_ID'),$this->lastitemid) ?>">
			                    				<input type='text' size="2" value="<?php echo @$item_id[$i]; ?>"  name="item_id[]" id=<?php echo $Itemids;?>>
			                    			</div>
			                    		</td>
			                           <td id="subitems<?php echo $tm;?>" valign="top"><input id="<?php echo $ItemQTYid;?>" type='text' size="2" onclick="removeQty(this.id)" onkeyup="total2(this.value,'<?php echo $quantityid ?>')" value="<?php if($this->confRow->cformat=="1"){ if($invRow->id==""){ echo "1"; }else{ echo str_replace(".",",",$quantityname[$i]); } }else{ if($invRow->id==""){ echo "1"; }else{ echo str_replace(",",".",$quantityname[$i]); } } ;?>" id="<?php echo $quantityid ?>"  name="quantity[]"></td>
			                           <td id="subitems<?php echo $tm;?>" valign="top"><input type='text' size="35" value="<?php echo $productname[$i]; ?>" id="<?php echo $productid; ?>"  name="pname[]" ></td>
			                           <td id="subitems<?php echo $tm;?>" valign="top"><textarea cols="2" style="font-size: 0.909em;width:250px;" rows="3" maxlength="350" name="item_description[]" id="<?php echo $Descriptionid;?>"><?php echo @$item_description[$i]; ?></textarea></td>
			                           <td id="subitems<?php echo $tm;?>" valign="top"><input type='text'  value='<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",$productprice[$i]); }else{ echo str_replace(",",".",$productprice[$i]); } ;?>' name="price[]" id="<?php echo $priceid ?>" onkeyup="total2(this.value,'<?php echo $priceid ?>',event)"></td>
			                           <td id="subitems<?php echo $tm;?>" valign="top"><?php echo JHTML::_("select.genericlist", $this->invoicetax, "tax[]","onchange=total3()", "value", "value",  $taxvalue[$i],$boxid);?></td>
									   <!--<td valign="top"><input type="button"  class="itemadd" onclick="insRow1('<?php echo $idvalue ?>')" value=""></td>-->
									   <td id="subitems<?php echo $tm;?>" valign="top" style="padding-top:4px;">
									   		<table border="0" id="<?php echo $mysubDivDel;?>">
									   			<tr>
									   				<td>
														<a href="javascript:void(0);"  onclick="deleteRow('<?php echo $idvalue ?>')">
															<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" style="border-top:solid 1px #c0c0c0;border-bottom:solid 1px #c0c0c0;border-left:solid 1px #c0c0c0;">
																<input id="<?php echo $myDivDel;?>" type="button" class="itemdelete" value="">
															</td><td style="border-top:solid 1px #c0c0c0;border-bottom:solid 1px #c0c0c0;border-right:solid 1px #c0c0c0;">
																<?php echo JText::_( 'CC_DELETE' ); ?>
															</td></tr></table>
														</a>
									   				</td>
									   			</tr>
									   			<?php
									   				if($invRow->id==""){
									   			?>
									   			<!--<tr id="mysubtr<?php echo $j;?>">
									   				<td style="padding-top:5px;">
														<a href="javascript:void(0);"  onclick="saveAsNewItem('<?php echo $j ?>','mysubtr<?php echo $j;?>')">
															<table width="100%" class="buttoncl" border="0" cellspacing="0" cellpadding="0"><Tr><td align="right" style="border-top:solid 1px #c0c0c0;border-bottom:solid 1px #c0c0c0;border-left:solid 1px #c0c0c0;">
																<input type="button" class="itemsaveas" onclick=""  value="">
															</td><td style="border-top:solid 1px #c0c0c0;border-bottom:solid 1px #c0c0c0;border-right:solid 1px #c0c0c0;">
																<?php echo JText::_( 'CC_SAVE_AS_NEW' ); ?>
															</td></tr></table>
														</a>
									   				</td>
									   			</tr>-->
									   			<?php
									   				}
									   			?>
									   		</table>
									   </td>
								</tr>
						<?php
								if($tm=="0")
								{
									$tm=1;
								}else{
									$tm=0;
								}
						 }
						?>
			             </table>
			</fieldset>
			</td> </tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="60%" valign="top">
			<fieldset class="adminform" >
				<legend><?php echo JText::_( 'CC_INVOICE_NOTE' ); ?></legend>
					<table class="admintable" width="50%">
					<tr>
						<td valign="top" >
								<?php
									$jversion = new JVersion();
									$current_version =  $jversion->getShortVersion();
								 if(substr($current_version,0,3)!="1.5"){ ?>
									<textarea class="mce_editable" name="note" id="cc_note" cols="62" rows="5"><?php echo $this->defNote;?></textarea>
								<?php }else{ ?>
									<textarea class="mceEditor"  name="note" id="cc_note" cols="62" rows="5" ><?php echo $this->defNote;?></textarea>
								<?php } ?>
						</td>
					</tr>
			</table>
			</fieldset>
		</td>
		<td width="40%" valign="top">
		<fieldset class="adminform" style="padding:10px;">
			<legend><?php echo JText::_( 'CC_INVOICE_TOTALS' ); ?></legend>
				<table class="admintable" cellspacing="1">
				<tr>
					<td width="220px"  class="key">
						<label style="font-weight:bold;">
							<?php echo JText::_( 'CC_INVOICE_DISCOUNT' ); ?>
						</label>
					</td>
					<td>
						<input class="text_area" type="text" name="discount"  onkeyup="total3()" id="discount" size="40" maxlength="256" value="<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",$invRow->discount); }else{ echo str_replace(",",".",$invRow->discount); } ;?>"  />
					</td>
				</tr>
				<tr>
					<td width="220px"  class="key">
						<label style="font-weight:bold;">
							<?php echo JText::_( 'CC_INVOICE_SUBTOTAL' ); ?>
						</label>
					</td>
					<td>
						<input type="text" size="40" id="total1" name='subtotal' value='<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",$invRow->subtotal); }else{ echo str_replace(",",".",$invRow->subtotal); } ;?>'>
					</td>
				</tr>
				<tr>
					<td width="220px"  class="key">
						<label style="font-weight:bold;">
							<?php echo JText::_( 'CC_INVOICE_TAX' ); ?>
						</label>
					</td>
					<td>
						<input class="text_area" type="text" name="totaltax" id="tax" size="40" maxlength="256" value="<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",$invRow->totaltax); }else{ echo str_replace(",",".",$invRow->totaltax); } ;?>"  />
					</td>
				</tr>
				<tr>
					<td width="220px"  class="key">
						<label style="font-weight:bold;">
							<?php echo JText::_( 'CC_INVOICE_TOTALS' ); ?>
						</label>
					</td>
					<td>
						<input class="text_area" type="text" name="total" id="total" size="40" maxlength="256" value="<?php if($this->confRow->cformat=="1"){ echo str_replace(".",",",$invRow->total); }else{ echo str_replace(",",".",$invRow->total); } ;?>"  />
					</td>
				</tr>

		</table>
		<br/>
		</fieldset>
		</td>
	</tr>
</table>
<?php
$rowstyleval=0;
if($count%2==0)
{
	$rowstyleval=1;
}
else
{
	$rowstyleval=0;
}

?>
<input type="hidden" name="id" id="id" value="<?php echo $invRow->id ?>" />
<input type="hidden" name="cid[]" value="<?php echo $invRow->id ?>" />
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="controller" value="invoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="rowstyleval" id="rowstyleval" value="<?php echo $rowstyleval;?>" />
<input type="hidden" name="path" id="path" value="<?php echo JURI::root();?>" />
<input type="hidden" name="pathadmin" id="pathadmin" value="<?php echo JURI::root();?>" />
<input type="hidden" value="<?php print_r($this->invoicetax) ?>" name="invoicetax" id="invoicetax">
<input type="hidden" name="invoice_sent_date" value="<?php echo $invRow->invoice_sent_date; ?>" />
<input type="hidden" name="cnumber" value="<?php echo $this->contact_number;?>"/>
<input type="hidden" name="communication" value="<?php echo $invRow->communication; ?>" />
<input type="hidden" name="add_con_msg" value="<?php echo JText::_("CC_CREATE_CONTACT"); ?>"/>
<input type="hidden" name="update_con_msg" value="<?php echo JText::_("CC_UPDATE_CONTACT"); ?>"/>
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</td></tr></table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>