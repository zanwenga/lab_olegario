<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
$values=$this->getinvoicecontact;
?>
<input  type="hidden" id="invoice_contact" value="<?php if( isset($values->contact)) { echo $values->contact;  } ?>">
<input  type="hidden" id="invoice_address" value="<?php if( isset($values->address))  { echo $values->address ; }?>">
<input  type="hidden" id="invoice_email" value="<?php if( isset($values->email))  { echo $values->email ; }?>">
<input  type="hidden" id="contact_number_cc" value="<?php if( isset($values->contact_number))  { echo $values->contact_number ; }?>">
