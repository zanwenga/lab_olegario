<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));


					?>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/highslide-with-html.js"></script>
<script type="text/javascript">
    hs.graphicsDir = '<?php echo JURI::base() ?>components/com_ccinvoices/assets/graphics/';
    hs.outlineType = 'rounded-white';
    hs.outlineWhileAnimating = true;
</script>
<script type="text/javascript">
	var sendText1="<?php echo JText::_('CC_SEND_NOTIFY_EMAIL1');?>";
	var sendText2="<?php echo JText::_('CC_SEND_NOTIFY_EMAIL2');?>";
</script>
<?php
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');
?>

<style>
.adminlist
{
	font-size:11px;
}
p {
    font-size: 1em;
    margin: 0 0 0 0;
    padding: 0;
}
</style>
<script language="javascript">
function createFilter(filter)
{
	document.getElementById('filter_search').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter_search').value = '';
	submitform();
}
function deleteAction(id,status)
{
	document.adminForm.task.value = "deleteInvoice";
	document.adminForm.id.value = id;
	if (confirm ("<?php echo JText::_( 'CC_DELETE_CONFIRM_MSG1' ); ?> "+status+"<?php echo JText::_( 'CC_DELETE_CONFIRM_MSG2' ); ?>"))
	{
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}

}

function onlyClose()
{
	var totalCnt;

	totalCnt=document.getElementById('totalrows').value;

	for(i=0;i<totalCnt;i++)
	{
		tmpidd=document.getElementById('cancelbtnNew'+i).value;
		document.getElementById('cancelbtn'+tmpidd).click();
	}

}

var opened_clsed;
opened_clsed=false;
function tempfunction(tmp,id)
{

	document.getElementById(tmp).click();
	wrapperIdName=id;
	var selected_status =document.getElementById('selectedstatus'+id).value;
	opened_clsed="true";
	if(selected_status=="1")
	{
		document.getElementById('top_status_id1'+id).style.display="block";
		document.getElementById('top_status_id2'+id).style.display="none";
		document.getElementById('top_status_id3'+id).style.display="none";
		document.getElementById('top_status_id4'+id).style.display="none";

		document.getElementById('top1_status_id1'+id).style.display="none";
		document.getElementById('top1_status_id2'+id).style.display="block";
		document.getElementById('top1_status_id3'+id).style.display="block";
		document.getElementById('top1_status_id4'+id).style.display="block";


	}
	if(selected_status=="2")
	{
		document.getElementById('top_status_id1'+id).style.display="none";
		document.getElementById('top_status_id2'+id).style.display="block";
		document.getElementById('top_status_id3'+id).style.display="none";
		document.getElementById('top_status_id4'+id).style.display="none";

		document.getElementById('top1_status_id1'+id).style.display="block";
		document.getElementById('top1_status_id2'+id).style.display="none";
		document.getElementById('top1_status_id3'+id).style.display="block";
		document.getElementById('top1_status_id4'+id).style.display="block";
	}
	if(selected_status=="3")
	{
		document.getElementById('top_status_id1'+id).style.display="none";
		document.getElementById('top_status_id2'+id).style.display="none";
		document.getElementById('top_status_id3'+id).style.display="block";
		document.getElementById('top_status_id4'+id).style.display="none";

		document.getElementById('top1_status_id1'+id).style.display="block";
		document.getElementById('top1_status_id2'+id).style.display="block";
		document.getElementById('top1_status_id3'+id).style.display="none";
		document.getElementById('top1_status_id4'+id).style.display="block";
	}
	if(selected_status=="4")
	{
		document.getElementById('top_status_id1'+id).style.display="none";
		document.getElementById('top_status_id2'+id).style.display="none";
		document.getElementById('top_status_id3'+id).style.display="none";
		document.getElementById('top_status_id4'+id).style.display="block";

		document.getElementById('top1_status_id1'+id).style.display="block";
		document.getElementById('top1_status_id2'+id).style.display="block";
		document.getElementById('top1_status_id3'+id).style.display="block";
		document.getElementById('top1_status_id4'+id).style.display="none";
	}


}


function borderwidth(statusid,tdid1,tdid2,tdid3,tdid4,cutdid,invid)
{

	if(statusid=="2")
	{
		document.getElementById('notifycustomerText'+invid).innerHTML=sendText1;
	}
	if(statusid=="3")
	{
		document.getElementById('notifycustomerText'+invid).innerHTML=sendText2;
	}
	if(statusid=="2" || statusid=="3")
	{
		document.getElementById('notifycustomer'+invid).style.display='block';
		document.getElementById('sendNTEmail'+invid).checked=true;
	}
	else
	{
		document.getElementById('notifycustomer'+invid).style.display='none';
		document.getElementById('sendNTEmail'+invid).checked=false;
	}
	if(cutdid+invid==tdid1)
	{

		document.getElementById('top1_status_id1'+invid).style.border='solid 2px';
		document.getElementById('top1_status_id2'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id3'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id4'+invid).style.border='solid 1px';
	}
	if(cutdid+invid==tdid2)
	{
		document.getElementById('top1_status_id1'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id2'+invid).style.border='solid 2px';
		document.getElementById('top1_status_id3'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id4'+invid).style.border='solid 1px';
	}
	if(cutdid+invid==tdid3)
	{
		document.getElementById('top1_status_id1'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id2'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id3'+invid).style.border='solid 2px';
		document.getElementById('top1_status_id4'+invid).style.border='solid 1px';
	}
	if(cutdid+invid==tdid4)
	{
		document.getElementById('top1_status_id1'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id2'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id3'+invid).style.border='solid 1px';
		document.getElementById('top1_status_id4'+invid).style.border='solid 2px';
	}
	document.getElementById('selectedstatus'+invid).value=statusid;

}

function GetXmlHttpObject()
{
var xmlHttp=null;
try
{
// Firefox, Opera 8.0+, Safari
xmlHttp=new XMLHttpRequest();
}
catch (e)
{
// Internet Explorer
try
{
xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");
}
catch (e)
{
xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");
}
}
return xmlHttp;
}


</script>

<script language="javascript" type="text/javascript">
var xmlHttpgetInvoiceNo
var invLinkId;
var inVNoForpopup;
var popupHeader="<?php echo sprintf(JText::_('CC_INVOICES_ST_INVOICE_NUMBER'),'');?>";

function getInvoiceNumber(id,dispId)
{

		var path = document.adminForm.pathadmin.value;
		invLinkId=dispId;
		inVNoForpopup='invoicenoForPopup'+id;
		var variables="inv_id="+id;
		var url=path+"administrator/components/com_ccinvoices/helper/getInvoiceNumber.php?";
		url=url+variables;

		xmlHttpgetInvoiceNo=GetXmlHttpObject();
		if (xmlHttpgetInvoiceNo==null)
		{
				alert ("Your browser does not support AJAX!");
				return;
		}
		xmlHttpgetInvoiceNo.onreadystatechange=stateChanged_getINVNO;
		xmlHttpgetInvoiceNo.open("GET",url,true);
		xmlHttpgetInvoiceNo.send(null);
}
function stateChanged_getINVNO()
{
	if (xmlHttpgetInvoiceNo.readyState==4)
	{
		if (xmlHttpgetInvoiceNo.status == 200)
		{
			var response = xmlHttpgetInvoiceNo.responseText;
			document.getElementById(invLinkId).innerHTML=response;
			document.getElementById(inVNoForpopup).innerHTML=popupHeader+" "+response;
		}
	}
}
</script>


<script language="javascript" type="text/javascript">
var xmlHttpgetMailIcon
var mailTdid;
function getMailIcon(id,dispId)
{

		var path = document.adminForm.pathadmin.value;
		mailTdid=dispId;
		var variables="inv_id="+id;
		var url=path+"administrator/components/com_ccinvoices/helper/getMailIcon.php?";
		url=url+variables;

		xmlHttpgetMailIcon=GetXmlHttpObject();
		if (xmlHttpgetMailIcon==null)
		{
				alert ("Your browser does not support AJAX!");
				return;
		}
		xmlHttpgetMailIcon.onreadystatechange=stateChanged_getMailIcon;
		xmlHttpgetMailIcon.open("GET",url,true);
		xmlHttpgetMailIcon.send(null);
}
function stateChanged_getMailIcon()
{
	if (xmlHttpgetMailIcon.readyState==4)
	{
		if (xmlHttpgetMailIcon.status == 200)
		{
			var response = xmlHttpgetMailIcon.responseText;

			document.getElementById(mailTdid).innerHTML=response;

		}
	}
}
</script>
<script language="javascript" type="text/javascript">
<!--
function submitbutton(pressbutton)
{
	if(pressbutton == "reset_inv")
	{
		if (confirm ("<?php echo JText::_( 'CC_RESET_INV_MSG' ); ?>"))
		{
			document.adminForm.task.value = pressbutton;
			submitform( pressbutton );
		}else
		{
			document.adminForm.task.value = '';
		}
	}else
	{
		document.adminForm.task.value = pressbutton;
		submitform( pressbutton );
	}
}
function exportPDF()
{
	if(document.adminForm.boxchecked.value != 0)
	{
		document.adminForm.task.value = 'invoices.batchDownloadPDF';
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}else
	{
		alert("<?php echo JText::_( 'CC_PLEASE_CHOOSE_INVOICE' ); ?>");
	}
}
function exportCSV()
{
	if(document.adminForm.boxchecked.value != 0)
	{
		document.adminForm.task.value = 'invoices.batchDownloadCSV';
		document.adminForm.submit();
		document.adminForm.task.value = '';
	}else
	{
		alert("<?php echo JText::_( 'CC_PLEASE_CHOOSE_INVOICE' ); ?>");
	}
}
//-->
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<table width="100%">
	<tr>

		<td align="left" width="100%">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" placeholder="<?php echo JText::_('CCINVOICES_SEARCH_BOX_PLACEHOLDER_LBL'); ?>" class="input-medium search-query" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="" />
				</div>
				<div class="btn-group hidden-phone">
					<button class="btn tip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn tip" type="button" onclick="document.id('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
				</div>
			</div>
		</td>
		<td></td>
		<td align="right">
			<div style="width: 300px;">
                <?php echo JText::_( 'CC_BATCH_LABEL' ); ?>
                <a href="javascript::void(0);" class="btn btn-small" onclick="exportPDF();" ><?php echo JText::_("CC_LABEL_EXPORT_TO_PDF"); ?></a>
                <?php echo JText::_('CC_LABEL_EXPORT_TXT'); ?>
                <a href="javascript::void(0);" class="btn btn-small" onclick="exportCSV();" ><?php echo JText::_("CC_LABEL_EXPORT_TO_CSV"); ?></a>
            </div>
		</td>
	</tr>
</table>



<table class="table table-striped" width="100%" border="0" >
<thead>
	<tr>
		<th width="1%" align="center">
			<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
		</th>
		<th nowrap="nowrap" align="center" width="17%">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_INVOICES_ID' ), 's.number', $listDirn, $listOrder ); ?>
		</th>
		<th width="13%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_CLIENT' ), 'b.name', $listDirn, $listOrder ); ?>
		</th>
		<th width="10%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_CONTACT' ), 'b.name', $listDirn, $listOrder ); ?>
		</th>
		<th align="center" width="15%">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_DATE' ), 's.invoice_date', $listDirn, $listOrder ); ?>
		</th>
		<th width="10%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICES_AMOUNT' ), 's.total', $listDirn, $listOrder ); ?>
		</th>

		<th width="6%" align="center">
			<?php echo JHTML::_('grid.sort',   JText::_( 'CC_INVOICES_STATUS' ), 's.status', $listDirn, $listOrder ); ?>
		</th>
		<th width="12%" align="center">
			<?php echo JText::_( 'CC_INVOICES_ACTION' ); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="9">
				<?php
				echo $this->pagination->getLimitBox();
				echo $this->pagination->getListFooter();
				?>
		</td>
	</tr>
</tfoot>
<?php
$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
$k = 0;

for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );
	if($this->editpermission=="1")
		$link 		= JRoute::_( 'index.php?option=com_ccinvoices&task=invoices.edit&cid[]='. $row->id );
	else
		$link 		= "#";
	$link_print_inv = JRoute::_( 'index.php?option=com_ccinvoices&task=invoices.printInv&id='. $row->id.'&tmpl=component' );
	$link1 		= JRoute::_( 'index.php?option=com_ccinvoices&task=contacts.edit&cid[]='. $row->contact_id );
	if($row->invoice_date =='0000-00-00' || $row->invoice_date == '')
	{
		$datevalue='';
	} else if($row->invoice_date != '0000-00-00')
	{
		$datevalue=strftime($this->date_format, strtotime($row->invoice_date));
	}
	if(strtotime(date('Y-m-d',time()))	 >  strtotime($row->duedate))
	{
		$database = JFactory::getDBO();
		$sql = "UPDATE #__ccinvoices_invoices SET status=3 WHERE status=2 AND id =".$row->id;
		$database->setQuery($sql);
		$database->query();

		if($row->status == "2")
		{
			$row->status = "3";
		}
	}
	?>
	<tr class="<?php echo "row$k"; ?>" >
		<td align="center">
			<?php echo $checked; ?>
		</td>

		<td align="center" class="nowrap has-context">
			<div class="pull-left">
				<a href="<?php echo $link; ?>" id="invNumberdisplay<?php echo $row->id;?>">
					<?php
					if($row->custom_invoice_number != '')
					{
						echo $row->custom_invoice_number;
					}else
					{
						echo $row->number;
					}
					$invno='';
					if($row->custom_invoice_number != '')
					{
						$invno= $row->custom_invoice_number;
					}else
					{
						$invno=$row->number;
					}
				 	?>
				</a>
			</div>
			<div class="pull-left">
				<?php
					// Create dropdown items
					JHtml::_('dropdown.edit', $row->id, 'invoices.');
					JHtml::_('dropdown.divider');
					JHtml::_('dropdown.trash', 'cb' . $i, 'invoices.');

					// Render dropdown list
					echo JHtml::_('dropdown.render');
					?>
			</div>
		</td>
		<td align="center">

			<a href="<?php echo $link1; ?>"><?php echo $row->name; ?></a>
		</td>
		<td align="center">
			<a href="<?php echo $link1; ?>"><?php echo $row->contact; ?></a>
		</td>

		<td align="center">
			<?php  echo $datevalue; ?>
		</td>
		<td align="center">
			<?php
			if($this->symbol_display == '1')
			{
				if($this->cformat == 0)
				{
					echo @$this->currency_symbol.@number_format(@$row->total, 2, '.', ',');
				}else if($this->cformat == 1)
				{
					echo @$this->currency_symbol.@number_format(@$row->total, 2, ',', '.');
				}else if($this->cformat == 2)
				{
					echo @$this->currency_symbol.@number_format(@$row->total, 2, '.', ' ');
				}else if(@$this->cformat == 3)
				{
					echo @$this->currency_symbol.@number_format(@$row->total, 2, ".", "'");
				}
			}else
			{
				if($this->cformat == 0)
				{
					echo @number_format(@$row->total, 2, '.', ',').@$this->currency_symbol;
				}else if($this->cformat == 1)
				{
					echo @number_format(@$row->total, 2, ',', '.').@$this->currency_symbol;
				}else if($this->cformat == 2)
				{
					echo @number_format(@$row->total, 2, '.', ' ').@$this->currency_symbol;
				}else if($this->cformat == 3)
				{
					echo @number_format(@$row->total, 2, ".", "'").@$this->currency_symbol;
				}


			}
			?>
		</td>
		<td align="center" style="padding-left:10px;padding-right:10px;">

				<p style="height:1px;"><a href="#" onclick="return hs.htmlExpand(this, { contentId: 'highslide-html<?php echo $row->id;?>' } )" class="highslide">
					<input style="visibility:hidden;" type="button" name="status_btn_<?php echo $row->id;?>" id="status_btn_<?php echo $row->id;?>" />
				</a></p>
				<?php
				if($this->editstatus)
				{
				?>
				<table cellpadding="0" cellspacing="0" width="100%"   onclick="tempfunction('status_btn_<?php echo $row->id;?>','<?php echo $row->id;?>')">
				<?php
				}else
				{
					?>
				<table cellpadding="0" cellspacing="0" width="100%">
					<?php
				}
				?>
					<tr>
						<?php
							if($row->status == 1)
							{
								$color = "border:solid 1px #818283;text-align:center;background-color:#E3E4E4;color:#818283;text-transform:uppercase;";
							}else if($row->status == 2)
							{
								$color = "border:solid 1px #1BC721;text-align:center;text-transform:uppercase;background-color:#F3FFEE;color:#006C09;";
							}else if($row->status == 3)
							{
								$color = "border:solid 1px #FF7570;text-align:center;text-transform:uppercase;background-color:#FFF6F6;color:#E62C2F;";
							}else if($row->status == 4)
							{
								$color = "border:solid 1px #7DAEE3;text-align:center;text-transform:uppercase;background-color:#DCEBFC;color:#006DCF;";
							}
						?>
						<?php
								$color1[0] = "text-align:center;border:solid 1px #818283;background-color:#E3E4E4;color:#818283;text-transform:uppercase;";
								$color1[1] = "text-align:center;border:solid 1px #1BC721;text-transform:uppercase;background-color:#F3FFEE;color:#006C09;";
								$color1[2] = "text-align:center;border:solid 1px #FF7570;text-transform:uppercase;background-color:#FFF6F6;color:#E62C2F;";
								$color1[3] = "text-align:center;border:solid 1px #7DAEE3;text-transform:uppercase;background-color:#DCEBFC;color:#006DCF;";
						?>
						<script language="JavaScript" type="text/javascript">
						var tmpcheckVarrr;
						tmpcheckVarrr=true
  function ttttFucn()
  {
	tmpcheckVarrr=false;
  }
  function ttttFucn1()
  {
	tmpcheckVarrr=true;
  }
</script>

						<td id="current_status<?php echo $row->id?>" style="<?php echo $color; ?>" onmouseout="ttttFucn1()" onmousemove="ttttFucn()">
						<input type="hidden" name="cancelbtnNew<?php echo $i?>" id="cancelbtnNew<?php echo $i?>" value="<?php echo $row->id?>" />
							<?php
								$select_id = '';
								$temp = $this->status;
								$select_id = $row->status - 1;
								echo $temp[$select_id]->text;
								$tmparrr=array();
								if($row->status=="1")
								{
									$tmparrr=array(2,3,4);
								}
								if($row->status=="2")
								{
									$tmparrr=array(1,3,4);
								}
								if($row->status=="3")
								{
									$tmparrr=array(1,2,4);
								}
								if($row->status=="4")
								{
									$tmparrr=array(1,2,3);
								}
							?>
						</td>
						<!-- dynamic status-->
							<td style="height:14px;display:none;<?php echo $color1[0]; ?>" id="td_status_id1<?php echo $row->id?>"  >
								<?php
									echo $temp[0]->text;
								?>

							</td>

							<td style="height:14px;display:none;<?php echo $color1[1]; ?>" id="td_status_id2<?php echo $row->id?>"  >
								<?php
									echo $temp[1]->text;
								?>
							</td>

							<td style="height:14px;display:none;<?php echo $color1[2]; ?>" id="td_status_id3<?php echo $row->id?>"  >
								<?php
									echo $temp[2]->text;
								?>
							</td>
							<td style="height:14px;display:none;<?php echo $color1[3]; ?>" id="td_status_id4<?php echo $row->id?>"  >
								<?php
									echo $temp[3]->text;
								?>
							</td>
						<!-- dynamic status-->
							<td id="updateingDiv_<?php echo $row->id?>" align="center" style="border-top: 0px solid #DDDDDD !important;<?php if($k=="1"){ echo "background-color:white !important;"; } ?>height:14px;display:none;">
								<img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/loader.gif">
							</td>
							<td id="updatedDiv_<?php echo $row->id?>" align="center" style="border-top: 0px solid #DDDDDD !important;<?php if($k=="1"){ echo "background-color:white !important;"; } ?>height:14px;display:none;">
								<img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/ticknew.png">
							</td>
					</tr>
				</table>
				</a></p>

			<div style="width:150px;height:220px;" class="highslide-html-content" id="highslide-html<?php echo $row->id;?>" >

				<table border="0" >
					<tr>
						<td colspan="4">
							<table cellsapcing="0" cellpadding="0" width="100%">
								<tr>
									<td style="color:#666666;" width="80%" id="invoicenoForPopup<?php echo $row->id?>">
										<?php if($row->status!=1){ ?>
											<?php echo sprintf(JText::_('CC_INVOICES_ST_INVOICE_NUMBER'),$invno); ?>
										<?php } ?>
									</td>
									<td width="20%" onclick="return hs.close(this)" class="ccinvoices_invlist_stauts">
										X
									</td>
								</tr>
								<tr>
									<td colspan="2"><hr/></td>
								</tr>
							</table>

						</td>
					</tr>
					<Tr>
						<!--<td style="<?php echo $color; ?>" align="center">
							<?php
								echo $temp[$select_id]->text;
							?>
						</td>-->
						<!-- dynamic current status-->
							<td style="display:none;<?php echo $color1[0]; ?>" id="top_status_id1<?php echo $row->id?>"  align="center">
								<?php
									echo $temp[0]->text;
								?>

							</td>

							<td style="display:none;<?php echo $color1[1]; ?>" id="top_status_id2<?php echo $row->id?>"  align="center">
								<?php
									echo $temp[1]->text;
								?>
							</td>

							<td style="display:none;<?php echo $color1[2]; ?>" id="top_status_id3<?php echo $row->id?>"  align="center">
								<?php
									echo $temp[2]->text;
								?>
							</td>

							<td style="display:none;<?php echo $color1[3]; ?>" id="top_status_id4<?php echo $row->id?>"  align="center">
								<?php
									echo $temp[3]->text;
								?>
							</td>
						<!-- dynamic status-->
					</Tr>
					<Tr>
						<td style="color:#666666;">
							<?php echo JText::_('CC_CHANGE_STAT_TO');?>
							<input type="hidden" name="selectedstatus<?php echo $row->id?>" id="selectedstatus<?php echo $row->id?>" value="<?php echo $row->status?>">
							<input type="button" class="ccinvoice_state_change" onclick="return hs.close(this)" name="cancelbtn<?php echo $row->id?>" id="cancelbtn<?php echo $row->id?>" value="Cancel" />
						</td>
					</Tr>
					<Tr>
						<!--<td style="<?php echo $color1[$tmparrr[0]-1]; ?>" id="td_stat_id<?php echo $tmparrr[0].$row->id?>" onclick="borderwidth(<?php echo $tmparrr[0];?>,<?php echo $tmparrr[0].$row->id?>,<?php echo $tmparrr[1].$row->id?>,<?php echo $tmparrr[2].$row->id?>,<?php echo $tmparrr[0].$row->id?>,<?php echo $row->id?>)" align="center">
							<?php
								echo $temp[$tmparrr[0]-1]->text;
							?>
						</td>-->
						<!-- dynamic current status top1-->
							<td style="cursor: pointer;display:none;<?php echo $color1[0]; ?>" id="top1_status_id1<?php echo $row->id?>" onclick="borderwidth('1','top1_status_id1<?php echo $row->id?>','top1_status_id2<?php echo $row->id?>','top1_status_id3<?php echo $row->id?>','top1_status_id4<?php echo $row->id?>','top1_status_id1','<?php echo $row->id?>')"  align="center">
								<?php
									echo $temp[0]->text;
								?>
							</td>
					</Tr>
					<Tr>
							<td style="cursor: pointer;display:none;<?php echo $color1[1]; ?>" id="top1_status_id2<?php echo $row->id?>" onclick="borderwidth('2','top1_status_id1<?php echo $row->id?>','top1_status_id2<?php echo $row->id?>','top1_status_id3<?php echo $row->id?>','top1_status_id4<?php echo $row->id?>','top1_status_id2','<?php echo $row->id?>')" align="center">
								<?php
									echo $temp[1]->text;
								?>
							</td>
					</Tr>
					<Tr>
							<td style="cursor: pointer;display:none;<?php echo $color1[2]; ?>" id="top1_status_id3<?php echo $row->id?>" onclick="borderwidth('3','top1_status_id1<?php echo $row->id?>','top1_status_id2<?php echo $row->id?>','top1_status_id3<?php echo $row->id?>','top1_status_id4<?php echo $row->id?>','top1_status_id3','<?php echo $row->id?>')" align="center">
								<?php
									echo $temp[2]->text;
								?>
							</td>
					</Tr>
					<Tr>
							<td style="cursor: pointer;display:none;<?php echo $color1[3]; ?>" id="top1_status_id4<?php echo $row->id?>" onclick="borderwidth('4','top1_status_id1<?php echo $row->id?>','top1_status_id2<?php echo $row->id?>','top1_status_id3<?php echo $row->id?>','top1_status_id4<?php echo $row->id?>','top1_status_id4','<?php echo $row->id?>')" align="center">
								<?php
									echo $temp[3]->text;
								?>
							</td>
						<!-- dynamic status-->
					</Tr>
					<tr height="20">
						<td style="color:#666666;display:none;" id="notifycustomer<?php echo $row->id?>">
							<table>
								<tr>
									<Td>
										<input type="checkbox" id="sendNTEmail<?php echo $row->id?>" name="sendNTEmail<?php echo $row->id?>" />
									</td>
									<td>
										<div id="notifycustomerText<?php echo $row->id?>"></div>
									</td>
								</tr>
							</table>
						</td>
					</tr>
					<Tr>
						<td align="center" onclick="updatestatus<?php echo $row->id?>()">

							<a href="javascript:void(0);" >
								<img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/update.png" >
							</a>
						</td>
					</tr>
				</table>
			</div>
		</td>

<script language="javascript" type="text/javascript">
var xmlHttpUpdateStatus
function updatestatus<?php echo $row->id?>()
{
		chek=document.getElementById('sendNTEmail<?php echo$row->id?>').checked;
		notify="Y"
		if(chek==true)
		{
			notify="Y"
		}
		if(chek==false)
		{
			notify="N"
		}

		try
		{
			document.getElementById('current_status<?php echo$row->id?>').style.display="none";
		}
		catch (e)
		{
		}
		try
		{
			document.getElementById('td_status_id1<?php echo$row->id?>').style.display="none";

		}
		catch (e)
		{
		}

		try
		{
			document.getElementById('td_status_id2<?php echo$row->id?>').style.display="none";

		}
		catch (e)
		{
		}

		try
		{
			document.getElementById('td_status_id3<?php echo$row->id?>').style.display="none";

		}
		catch (e)
		{
		}

		try
		{
			document.getElementById('td_status_id4<?php echo$row->id?>').style.display="none";

		}
		catch (e)
		{

		}

		document.getElementById('current_status<?php echo$row->id?>').style.display="none";
		document.getElementById('updateingDiv_<?php echo$row->id?>').style.display="block";

		document.getElementById('cancelbtn'+<?php echo $row->id?>).click();

		var path = document.adminForm.pathadmin.value;
		var selected_status =document.getElementById('selectedstatus<?php echo $row->id?>').value;



		var variables="inv_id="+<?php echo $row->id?>+"&selectedst="+selected_status+"&notify="+notify;
		var url=path+"administrator/index.php?option=com_ccinvoices&task=invoices.updateStatusFromCol&";
		url=url+variables;

		xmlHttpUpdateStatus=GetXmlHttpObject();
		if (xmlHttpUpdateStatus==null)
		{
				alert ("Your browser does not support AJAX!");
				return;
		}
		xmlHttpUpdateStatus.onreadystatechange=stateChanged_statusUpdate<?php echo$row->id?>;
		xmlHttpUpdateStatus.open("GET",url,true);
		xmlHttpUpdateStatus.send(null);
}
function stateChanged_statusUpdate<?php echo $row->id?>()
{
	if (xmlHttpUpdateStatus.readyState==4)
	{
		if (xmlHttpUpdateStatus.status == 200)
		{
			var response = xmlHttpUpdateStatus.responseText;
			setTimeout('setDIVs_New<?php echo$row->id?>()',1000);

			statusidTmp=document.getElementById('selectedstatus<?php echo$row->id?>').value
			if(statusidTmp!=1)
			{
				document.getElementById('invoicenoForPopup<?php echo$row->id?>').style.display='block';
			}
			else
			{
				document.getElementById('invoicenoForPopup<?php echo$row->id?>').style.display='none';
			}
		}
	}
}
function setDIVs_New<?php echo$row->id?>()
{
	getInvoiceNumber("<?php echo$row->id?>","invNumberdisplay<?php echo $row->id;?>");
	getMailIcon("<?php echo$row->id?>","mailTd<?php echo $row->id;?>");
	document.getElementById('updateingDiv_<?php echo$row->id?>').style.display="none";
	document.getElementById('updatedDiv_<?php echo$row->id?>').style.display="block";
	setTimeout('setDIVs1<?=$row->id?>()',1500);
}
function setDIVs1<?php echo$row->id?>()
{

	var selected_status =document.getElementById('selectedstatus<?php echo $row->id?>').value;
	document.getElementById('updatedDiv_<?php echo$row->id?>').style.display="none";
	document.getElementById('td_status_id'+selected_status+'<?php echo$row->id?>').style.display="block";

	clearTimeout();
}
</script>

		<td>
			<table cellpadding="0" cellspacing="0" border="0" width="100%" >
				<tr>
					<td style="<?php if($k=="1"){ echo "background-color:white !important;"; } ?>border:none;" align="center" id="mailTd<?php echo$row->id?>">
						<?php if($row->status == "1") { ?>
						<div ><img title="<?php echo JText::_('CC_TITLEDRAFT'); ?>" src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/send-inactive.png" ></div>
						<?php }else if($row->status == "2") {  ?>
							<?php if($row->invoice_sent_date == "0000-00-00") { ?>
								<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.sendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/send.png" title="<?php echo JText::_("CC_SEND_INVOICE"); ?>"></a></div>
							<?php }else { ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.reSendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
							<?php }?>
						<?php }else if($row->status == "3" AND $row->communication=="1") {  ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.invoiceRem&filenm=1&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/reminder.png" title="<?php echo JText::_("CC_REMINDER_INVOICE"); ?>"></a></div>
						<?php } else if($row->status == "4" AND $row->communication=="1") {  ?>
						<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.reSendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/resend.png" title="<?php echo JText::_("CC_RESEND_INVOICE"); ?>"></a></div>
						<?php }else{ ?>
							<div ><a style="text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.sendInvoice&filenm=1&id=<?php echo  $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/send.png" title="<?php echo JText::_("CC_SEND_INVOICE"); ?>"></a></div>
						<?php } ?>
					</td>
					<!--<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}"  href="index.php?option=com_ccinvoices&controller=assignusers&contact_id=<?php echo $row->contact_id; ?>&tmpl=component"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/manage_users.png"  title="<?php echo JText::_("CC_MANAGE_USERS"); ?>"></a></div>
					</td>-->

					<td style="<?php if($k=="1"){ echo "background-color:white !important;"; } ?>border:none;" align="center">
						<div><a  style="text-decoration:none;" class="modal" rel="{handler: 'iframe', size: {x: 950, y: 500}}" href="index.php?option=com_ccinvoices&task=invoices.viewInvoice&id=<?php echo $row->id; ?>&tmpl=component"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/view.png"  title="<?php echo JText::_("CC_QUICK_VIEW"); ?>"></a></div>
					</td>
					<!--<td style="border:none;" align="center">
						<div ><a  style="text-decoration:none;" href="<?php echo $link; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/edit.png"  title="<?php echo JText::_("CC_EDIT"); ?>"></a></div>
					</td>-->
					<td style="<?php if($k=="1"){ echo "background-color:white !important;"; } ?>border:none;" align="center">
						<div ><a style="<?php if($k=="1"){ echo "background-color:white !important;"; } ?>text-decoration:none;" href="index.php?option=com_ccinvoices&task=invoices.downloadInvoice&id=<?php echo $row->id; ?>"><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/download.png" title="<?php echo JText::_("CC_DOWNLOAD_PRINT"); ?>"></a></div>
					</td>
					<td style="<?php if($k=="1"){ echo "background-color:white !important;"; } ?>border:none;" align="center">
						<div ><a  style="text-decoration:none;" href="javascript:void window.open('<?php echo $link_print_inv; ?>', 'win2', 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=710,height=500,directories=no,location=no');" ><img src="<?php echo JURI::base();?>/components/com_ccinvoices/assets/images/print.png"  title="<?php echo JText::_("CC_PRINT_INV"); ?>"></a></div>
					</td>
				</tr>
			</table>
		</td>
	</tr>
	<?php
	$k = 1 - $k;

}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked" value="0" />
<input type="hidden" name="path" id="pathadmin" value="<?php echo JURI::root();?>" />
<input type="hidden" name="clientid" value="<?php echo $this->clientid; ?>" />
<input type="hidden" name="id" value="0" />
<input type="hidden" name="totalrows" id="totalrows" value="<?php echo $i; ?>" />
<input type="hidden" name="counttot" id="counttot" value="<?php echo $i;?>" />
<input type="hidden" name="view" value="invoices" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>

<script type="text/javascript">
<!--
if(document.getElementById('system-message'))
{
	var tagg = document.getElementsByTagName('dd');
	var tagcount = tagg.length ;
	if(tagcount > 1)
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
			var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
 			if (ieversion>=7)
 			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '152px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '144px';
		}
	} else
	{
		if (/MSIE (\d+\.\d+);/.test(navigator.userAgent))
		{ //test for MSIE x.x;
	 		var ieversion=new Number(RegExp.$1) // capture x.x portion and store as a number
			if (ieversion>=7)
			{
				document.getElementById('ajaxsearchbox').style.position = 'relative';
				document.getElementById('ajaxsearchbox').style.bottom = '102px';
			}
		} else
		{
			document.getElementById('ajaxsearchbox').style.position = 'relative';
			document.getElementById('ajaxsearchbox').style.bottom = '94px';
		}
	}
}
//-->
</script>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>
<script type="text/javascript">
document.onmousedown=check

function check(e)
{
	var target = (e && e.target) || (event && event.srcElement);
	tmpMostVar=checkParent(target);

	if(tmpMostVar)
	{
		onlyClose();
	}
}

function checkParent(t)
{
	var tmpVaar;
	tmpVaar=true;
	while(t.parentNode)
	{
		if(t==document.getElementById('highslide-html'+wrapperIdName)){

			return false;
		}
		t=t.parentNode
	}

	return true;
}
</script>
