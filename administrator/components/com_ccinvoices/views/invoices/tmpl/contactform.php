<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
$invRow = $this->invoiceRow;
$contactsRow = $this->contactsRow;
$quantityname=explode("|",$invRow->quantity);
$productname=explode("|",$invRow->pname);
$productprice=explode("|",$invRow->price);
$taxvalue=explode("|",$invRow->tax);
$count=count($quantityname);

$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
?>
<style>
table#myTable tr td
{
	text-align:left;
}
</style>
<script language="javascript">
function changeThisUrl()
{
	cont_id_tmp = document.getElementById("contact_id_url");
	temp = cont_id_tmp.href;
	if((temp.split('index.php').length -1) > 0)
	{
		cont_id_tmp.className = "modal";
		cont_id_tmp.rel = "{handler: 'iframe', size: {x: 680, y: 500}}"
	}else
	{
		cont_id_tmp.rel = "test";
		cont_id_tmp.className = "test"
		alert("<?php echo JText::_( 'CC_CHOOSE_CONTACT' ); ?>");
	}

}
function clearContact()
{
	document.adminForm.name.value = "";
	document.adminForm.client.value = "";
	document.adminForm.contact_hidden.value = "";
	document.adminForm.contact_id.value = "";
	document.adminForm.contactname.value = "";

	document.adminForm.contact.value = "";
	document.adminForm.contact_number.value = "";
	document.adminForm.address.value = "";
	document.adminForm.email.value = "";
 	document.getElementById('contact_txt').innerHTML =  document.adminForm.add_con_msg.value;
 	cont_id_tmp = document.getElementById("contact_id_url");
 	cont_id_tmp.href = "javascript::void(0);";
	cont_id_tmp.rel = "test";
	cont_id_tmp.className = "test"
}
</script>
<script type="text/javascript" src="<?php echo JURI::root(); ?>plugins/editors/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
 	editor_selector : "mceEditor",
	theme : "simple"
});
function submitbutton(pressbutton)
{
	if (pressbutton == 'cancel') {
		submitform( pressbutton );
		return;
	}
	if(document.getElementById("my1DivFT").value =="" || document.getElementById("my1Product").value =="" || document.getElementById("my1DivST").value =="")
	{
		alert("<?php echo JText::_("CC_MSG_PLEASE_ADD_ITEMS"); ?>");
		return;
	}else
	{
		submitform( pressbutton );
		return;
	}
}
function checkinvoicestatus()
{
	if(document.getElementById("status").value!=1)
	{
		document.getElementById("send_to_contact").style.display='block';
		document.getElementById("number").style.background="white";
		document.getElementById("number").disabled=false;
		document.getElementById("number").value=document.getElementById("hidnumber").value;
	}
	else
	{
		document.getElementById("send_to_contact").style.display='none';
		document.getElementById("number").style.background="#EAEAEA";
		document.getElementById("number").value=0;
		document.getElementById("number").disabled=true;
	}
}

</script>
<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/invoice-contactc-list.js"></script>
<form action="index.php" method="post" name="adminForm" autocomplete="off">
<div class="clr"></div>
<table cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" width="100%">
<table cellpadding="0" cellspacing="0" border="0" width="700">
	<tr>
		<td width="100%" colspan="2">
			<table cellpadding="0" cellspacing="0" width="100%">
				<tr>

<td width="48%" valign="top">
		<fieldset class="adminform" >
		<legend><?php echo JText::_( 'CC_INVOICE_DETAILS' ); ?></legend>
		<table class="admintable" cellspacing="1" width="100%">
		<tr>
			<td class="key">
				<label style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_NUMBER' ); ?>
				</label>
			</td>
			<td>
		    	<input class="text_area" type="text" name="number" id="number" size="25" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>" disabled />
		        <br/>
		        <input class="text_area" type="hidden" name="hidnumber" style="background-color:#EAEAEA;" id="hidnumber" size="25" style="background-color:#EAEAEA;" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>" disabled />
		        <input class="text_area" type="hidden" name="numbercheck" size="25" maxlength="256" value="<?php  $tot= $this->numbercheck; echo $tot;?>"  />
				<?php echo JText::_("CC_LAST_INVOICE_ID"); ?><?php  if(isset($this->lastinvoicenumber->number)) { echo $this->lastinvoicenumber->number;};?>
			</td>
		</tr>
		<tr>
			<td   class="key">
				<label style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_DATE' ); ?>
				</label>
			</td>
			<td>
				<?php echo JHTML::_('calendar', $invRow->invoice_date, 'invoice_date', 'invoice_date', '%Y-%m-%d'); ?>
			</td>
		</tr>
		<tr>
			<td   class="key">
				<label style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_DUEDATE' ); ?>
				</label>
			</td>
			<td>
				<input type="text" value="<?php echo $this->defDueDate; ?>" name="duedate" >
			</td>
		</tr>
		<tr>
				<td class="key" style="font-weight:bold;">
						<?php echo JText::_( 'CC_INVOICE_STATUS' ); ?>
				</td>
				<td>
					<?php  echo JHTML::_('select.genericlist',   $this->status, 'status', 'class="inputbox" size="1" onchange="checkinvoicestatus()"', 'value', 'text', $invRow->status ); ?>
				</td>
			</tr>
<?php
	 		if(!$invRow->communication)
			{
			?>
			<tr>
				<td id="tmpid" align="right" colspan="2">

					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact">
						<table><tr><td><?php  echo JHTML::_('input.checkbox', 'send', '1',''); ?></td><td>  <?php echo JText::_( 'CC_SEND_INVOICE_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php } ?>
			<?php
	 		if($invRow->communication)
			{
			?>
			<tr>
					<td align="right" colspan="2">
					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact">
					<table><tr><td><?php  echo JHTML::_('input.checkbox', 'resend', '1',''); ?></td><td>  <?php echo JText::_( 'CC_RESEND_INVOICE_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php
	 		if($invRow->status != '4')
			{
			?>
	       	<tr>
					<td align="right" colspan="2">
					<span style="<?php echo ($invRow->status=='1' or $invRow->status=='')?'display:none':'display:block'; ?>" id="send_to_contact1">
					<table><tr><td><?php  echo JHTML::_('input.checkbox', 'reminder', '1',''); ?></td><td>  <?php echo JText::_( 'CC_SEND_INVOICE_REM_TO_CONTACT' ); ?></td></tr></table>
					</span>
				</td>
			</tr>
			<?php } ?>

			<?php } ?>
		</table>
		</fieldset>
	</td>
	<td width="52%" >
		<fieldset class="adminform"  >
		<legend><?php echo JText::_( 'CC_INVOICES_CONTACTS' ); ?></legend>
		<table class="admintable" width="100%">
		<tr>
				<td colspan="2">
					<table cellpadding="0" cellspacing="0" border="0" width="100%" border="0">
						<tr>
							<td width="33%" align="left" valign="bottom">
								<table cellpadding="0" cellspacing="0" width="100%" border="0">
									<tr>
										<td align="right" width="16">
											<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/zoom.png" width="16" height="16">
										</td>
										<td valign="middle">
											<a class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}" id="existing_contact_url" href="index.php?option=com_ccinvoices&controller=contacts&task=modelassigncontact&layout=assigncontact&contact_id=<?php echo $contactsRow->id; ?>&tmpl=component" ><?php echo JText::_("CC_EXISTING_CONTACTS"); ?></a>
										</td>
									</tr>
								</table>
							</td>
							<td width="33%"  valign="bottom">
								<table cellpadding="0" cellspacing="0" width="100%" border="0">
									<tr>
										<td align="right" width="16">
											<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/add.png" width="16" height="16">
										</td>
										<td align="left" valign="middle">
											<a href="javascript::void(0);" onclick="updatecontact()" ><span id="contact_txt">
											<?php
											if( $this->contactinvoice->id != '')
											{
												echo JText::_("CC_UPDATE_CONTACT");
											}else
											{
											 	echo JText::_("CC_CREATE_CONTACT");
											}
											?>
											</span></a>
										</td>
									</tr>
								</table>
							</td>
							<td width="33%" align="right" valign="bottom">
								<table cellpadding="0" cellspacing="0" width="100%" border="0">
									<tr>
										<td align="right" width="16">
											<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/manage_users.png" width="16" height="16">
										</td>
										<td align="left" valign="middle">
											<?php if($this->contactinvoice->id == ''){ ?>
											<a class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}" id="contact_id_url" onclick="changeThisUrl();" href="javascript:void(0);" ><?php echo JText::_("CC_MANAGE_USERS"); ?></a>
											<?php }else { ?>
											<a class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}" id="contact_id_url" href="index.php?option=com_ccinvoices&controller=assignusers&contact_id=<?php echo $this->contactinvoice->id; ?>&tmpl=component" ><?php echo JText::_("CC_MANAGE_USERS"); ?></a>

											<?php }?>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
				</td>
			</tr>
		<tr>
			<td   class="key" style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_NAME' ); ?>
			</td>
			<td>
	           <input type="text" size="40" id="contact_name" name="client" value="<?php echo $this->contactinvoice->name;?>">
			   <input type="hidden" id="contact_hidden" name="contact_hidden" value="<?php echo $this->contactinvoice->id;?>">
			   <input type="hidden" id="contact_id" name="contact_id" value="<?php echo $this->contactinvoice->id;?>">
			   <input type="hidden" id="contactname" name="contactname" value="<?php echo $this->contactinvoice->name;?>">
			</td>
		</tr>
		<tr>
			<td  class="key" style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICES_CONTACT' ); ?>
			</td>
			<td>
				<input class="text_area" type="text" name="contact" id="cc_contact" size="40" maxlength="256" value="<?php if (isset($this->contactinvoice->contact)) { echo $this->contactinvoice->contact; };?>"  />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" style="font-weight:bold;">
					<?php echo JText::_( 'CC_CONTACT_NUMBER' ); ?>
			</td>
			<td>
	 			 <input type="text" size="10" id="contact_number" name="contact_number" value="<?php echo $this->contactinvoice->contact_number;?>">
			</td>
		</tr>
		<tr>
			<td  class="key" style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_ADDRESS' ); ?>
			</td>
			<td>
				<textarea class="text_area"  name="address" id="cc_address" style="width:190px;" cols="28" rows="5" ><?php if (isset($this->contactinvoice->address)) { echo $this->contactinvoice->address; };?></textarea>
			</td>
		</tr>
		<tr>
			<td   class="key" style="font-weight:bold;">
					<?php echo JText::_( 'CC_INVOICE_STATUS' ); ?>
			</td>
			<td>
				<input class="text_area" type="text" name="email" id="cc_email" size="40" maxlength="256" value="<?php if (isset($this->contactinvoice->email)) { echo $this->contactinvoice->email; };?>"  />
			</td>
		</tr>
		<tr>
				<td colspan="2" width="100%" align="right">
					<table cellpadding="0" cellspacing="0" width="100%" border="0">
						<tr>
				  			<td width="100%" align="right" style="padding:0px 10px 0px 10px;">
								<table cellpadding="0" cellspacing="0" width="100%" border="0">
									<tr>
										<td width="50%" align="center">
											<div style="align:left;display:none;" id="contactajaxbox"><img id="tickimg" src="<?php echo JURI::root()."administrator/images/tick.png"; ?>">&nbsp;&nbsp;<?php echo JText::_("CC_EDIT_CONTACT_LABEL"); ?></div>
										</td>
										<td width="50%" align="center" valign="middle">
											<div ><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/delete.png" width="16" height="16">&nbsp;&nbsp;<a href="javascript:void(0);" onclick="clearContact();" ><span id="clear_txt">
											<?php
												echo JText::_("CC_CLEAR_CONTACT");
											?>
											</span></a></div>
										</td>
									</tr>
								</table>
							</td>
						</tr>
					</table>
					<div id="contactajaxbox"></div>
			</td>
		</tr>
		</table>
		</fieldset>
		</td>
	</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td width="100%" colspan="2">
			<fieldset class="adminform" >
			<legend><?php echo JText::_( 'CC_INVOICE_ITEMS' ); ?></legend>
			<table class="admintable" cellpadding="0" cellspacing="1" width="100%">
			<tr>
				<td>
					<table id="myTable" border="0" width="100%">
  						<tr align="left">
							<th><?php echo JText::_( 'CC_QTY' ); ?></th>
							<th><?php echo JText::_( 'CC_ITEM' ); ?></th>
	                        <th><?php echo JText::_( 'CC_PRICE' ); ?></th>
	                        <th><?php echo JText::_( 'CC_TAX' ); ?></th>
	                        <th><?php echo JText::_( 'CC_ADD' ); ?></th>
	                        <th><?php echo JText::_( 'CC_DELETE' ); ?></th>
						</tr>
						<?php
						if(!empty($invRow->quantity))
                  		{
                  		}
                  		else
                  		{
                  			$idvalue="my1Div";
                  			$boxid='my1DivSel';
                  			$quantityid='my1DivFT';
                  			$priceid='my1DivST';
                  		}
						for($i=0;$i<$count;$i++)
						{
						 	$j=1;
						 	$j=$i+$j;
						 	$idvalue="my".$j."Div";
						 	$boxid="my".$j."DivSel";
						 	$quantityid="my".$j."DivFT";
			                $priceid="my".$j."DivST";
			     			$productid="my".$j."Product";
			            ?>
						<tr id="<?php echo $idvalue?>">
                        	<td><input type='text' size="6" onkeyup="total2(this.value,'<?php echo $quantityid ?>')" value="<?php echo $quantityname[$i]; ?>" id="<?php echo $quantityid ?>"  name="quantity[]"  ></td>
                        	<td><input type='text' size="45" value="" id="<?php echo $productid; ?>"   name="pname[]" ></td>
                        	<td><input type='text'  value=''   name="price[]" id="<?php echo $priceid ?>" onkeyup="total2(this.value,'<?php echo $priceid ?>')"></td>
                        	<td><?php echo JHTML::_("select.genericlist", $this->invoicetax, "tax[]","onchange=total3()", "value", "value", $this->selectDefaultTax,$boxid);?></td>
                        	<td><input type="button"  class="itemadd" onclick="insRow1('<?php echo $idvalue ?>')" value=""></td>
                        	<td>  <input type="button" class="itemdelete" onclick="deleteRow('<?php echo $idvalue ?>')" value=""> </td>
						</tr>
						<?php
							}
						?>
					</table>
				</td>
			</tr>
			</table>
			</fieldset>
		</td>
	</tr>
	<tr>
		<td width="60%" >
			<fieldset class="adminform" >
				<legend><?php echo JText::_( 'CC_INVOICE_NOTE' ); ?></legend>
					<table class="admintable" width="50%">
					<tr>
						<td valign="top" >
							 <textarea class="mceEditor"  name="note" id="cc_note" cols="62" rows="5"><?php echo  $this->defNote;?></textarea>
						</td>
					</tr>
			</table>
			</fieldset>
		</td>
		<td width="40%" >
			<fieldset class="adminform"  >
				<legend><?php echo JText::_( 'CC_INVOICE_TOTALS' ); ?></legend>
					<table class="admintable" cellspacing="1">
					<tr>
						<td   class="key">
							<label style="font-weight:bold;">
								<?php echo JText::_( 'CC_INVOICE_DISCOUNT' ); ?>
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="discount"  onkeyup=total3() id="discount" size="40" maxlength="256" value="<?php echo $invRow->discount;?>"  />
						</td>
					</tr>
					<tr>
						<td   class="key">
							<label style="font-weight:bold;">
								<?php echo JText::_( 'CC_INVOICE_SUBTOTAL' ); ?>
							</label>
						</td>
						<td>
							<input type="text" id="total1" name='subtotal' size="40" value='<?php echo $invRow->subtotal;?>'>
						</td>
					</tr>
					<tr>
						<td   class="key">
							<label style="font-weight:bold;">
								<?php echo JText::_( 'CC_INVOICE_TAX' ); ?>
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="totaltax" id="tax" size="40" maxlength="256" value="<?php echo $invRow->totaltax;?>"  />
						</td>
					</tr>
					<tr>
						<td   class="key">
							<label style="font-weight:bold;">
								<?php echo JText::_( 'CC_INVOICE_TOTALS' ); ?>
							</label>
						</td>
						<td>
							<input class="text_area" type="text" name="total" id="total" size="40" maxlength="256" value="<?php echo $invRow->total;?>"  />
						</td>
					</tr>
			</table>
			</fieldset>
		</td>
	</tr>
</table>
<input type="hidden" name="id" value="<?php echo $invRow->id ?>" />
<input type="hidden" name="cid[]" value="<?php echo $invRow->id ?>" />
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="controller" value="invoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="path" id="path" value="<?php echo JURI::root();?>" />
<input type="hidden" value="<?php print_r($this->invoicetax) ?>" name="invoicetax" id="invoicetax">
<input type="hidden" name="cnumber" value="<?php echo $this->contact_number;?>"/>
<input type="hidden" name="add_con_msg" value="<?php echo JText::_("CC_CREATE_CONTACT"); ?>"/>
<input type="hidden" name="update_con_msg" value="<?php echo JText::_("CC_UPDATE_CONTACT"); ?>"/>
<?php echo JHTML::_( 'form.token' ); ?>
</form>
</td></tr></table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>