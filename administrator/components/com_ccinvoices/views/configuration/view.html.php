<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries


class ccInvoicesViewConfiguration extends CCINVOISViews
{
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function display($tpl = null)
    {
		$model =  $this->getModel();
		$db = JFactory::getDBO();
		$model->setId(1);
		$row = $this->get('Data');
		$query = 'SELECT max(number) from #__ccinvoices_invoices where reset_inv = "" LIMIT 1 ';
		$db->setQuery($query);
		$invoice_format = "";
		$invoice_id_max = $db->loadResult();
		if($row->invoice_format != "")
		{
			$custom_inv =  sprintf("%04d", $invoice_id_max + 1);
			$inv_format["invoicenumber"] = $custom_inv ;
			$inv_format["dy"] = date("z") + 1;
			$inv_format["dd"] = date("d",time());
			$inv_format["mm"] = date("m",time());
			$inv_format["yy"] = date("y",time());
			$inv_format["yyyy"] = date("Y",time());
			$invoice_format = $row->invoice_format;
			foreach($inv_format as $inv_formats=>$value)
			{
				$find = "[".$inv_formats."]";
				$replace = $value;
				$invoice_format = str_replace($find,$replace,$invoice_format);
			}
		}else
		{
			$custom_inv =  sprintf("%04d", $invoice_id_max + 1);
			$invoice_format = $custom_inv ;
		}
		$next_invo_no = $invoice_id_max +1;
		if($row->invoice_start > $next_invo_no)
		{
			$next_invo_no = $row->invoice_start;
		}else
		{
			$query = 'SELECT count(id) from #__ccinvoices_invoices  where reset_inv = "" ';
			$db->setQuery($query);
			$invoice_count = $db->loadResult();
			if($invoice_count == "0")
			{
				if($row->invoice_start != '')
				{
					$next_invo_no = $row->invoice_start;
				}
			}
		}

		$this->getVersionInfo();
		$this->addToolBar();
		$this->addCss();

		if(versionCompare()>=3)
		{
			$this->sidebar = JHtmlSidebar::render();
			$this->_layout='default';
		}
		else
		{
			$this->_layout='default25';
		}

		$this->assignRef("row",$row);
		$this->assignRef('invoice_format',		$invoice_format);
		$this->assignRef('next_invo_no',		$next_invo_no);
        parent::display($tpl);
    }
    function addToolBar()
    {
		JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).': '. JText::_( 'CC_CONFIGURATION' ).' ', 'ccinvoices.png' );
		$canDo = ccinvoicesHelper::getActions();
		if (!JFactory::getUser()->authorise('ccinvoices.view.configurations', 'com_ccinvoices'))
		{
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}
		if ($canDo->get('ccinvoices.edit'))
		{
			JToolbarHelper::apply('configuration.apply',JText::_( 'CCINVOICES_CONFIG_APPLY_TOOL' ));
			JToolBarHelper::save('configuration.save',JText::_( 'CCINVOICES_CONFIG_SAVE_TOOL' ));
		}
		if ($canDo->get('core.admin'))
		{
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_ccinvoices','500','800', JText::_( 'CCINVOICES_PERMISSION_TOOL_LBL' ));
			JToolBarHelper::divider();
		}
		JToolBarHelper::cancel('configuration.cancel',JText::_( 'CCINVOICES_CONFIG_CLOSE_TOOL' ));

    }
	function getVersionInfo()
	{
		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';
		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);
		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
	function addCss()
	{
	    $document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	}
}
?>