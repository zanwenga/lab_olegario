<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');

JHtml::_('behavior.framework');
JHtml::_('behavior.tooltip');
JHtml::_('behavior.formvalidation');
JHtml::_('behavior.modal');
JHtml::_('formbehavior.chosen', 'select');
$row = $this->row;
?>
<script type="text/javascript" src="<?php echo JURI::root(); ?>plugins/editors/tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
tinyMCE.init({
	mode : "textareas",
 	editor_selector : "mceEditor",
	theme : "simple"
});

function submitbutton(pressbutton)
{
	if(pressbutton == "configuration.reset_inv")
	{
		if (confirm ("<?php echo JText::_( 'CC_RESET_INV_MSG' ); ?>"))
		{
			document.adminForm.task.value = pressbutton;
			submitform( pressbutton );
		}else
		{
			document.adminForm.task.value = '';
		}
	}else
	{
		document.adminForm.task.value = pressbutton;
		submitform( pressbutton );
	}
}
function allowedFileExtension()
{
	var selected_filename=document.getElementById('logo').value;

	var lastIndx=selected_filename.lastIndexOf(".");
	var selected_extn=selected_filename.substring(lastIndx+1);
	if(selected_extn.toLowerCase()=='jpg' || selected_extn.toLowerCase()=='jpeg' || selected_extn.toLowerCase()=='gif')
	{

	}
	else
	{
		selected_filename=document.getElementById('logo').value='';
		alert('Logo uploader allowed only JPG or GIF images!');
	}
}
</script>
<form action="index.php" method="post" name="adminForm" id="adminForm" enctype="multipart/form-data">
<?php if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<table cellpadding="0" cellspacing="0" border="0" width="100%">
	<tr>
		<td width="70%" valign="top">
		<!-- invoice settings starting-->
			<fieldset   style="padding:10px;background-color:white;">
				<legend><?php echo JText::_( 'CC_INVOICE_SETTINGS' ); ?></legend>
	<table cellpadding="3" cellspacing="0" width="100%" border="0">
		<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_HEADING1' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_INVOICENUMBER_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_CONTACTNUMBER_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_DAY_OF_YEAR_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_DATE_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_MONTH_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_YEAR1_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_YEAR2_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_SYMBOL_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<?php echo JText::_( 'CC_CONFIG_SHOPNAME_DESC' ); ?>
						</td>
					</tr>
					<tr>
						<td align="left" >
							<table cellpadding="0" cellspacing="4" border="0" width="100%">
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_CONFIG_INVOICE_FORMAT_LABEL' ); ?>
									</td>
									<td>
										<input type="text" name="invoice_format" value="<?php echo $row->invoice_format; ?>" size="30"/><br/>

									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<?php
											if($row->invoice_format != '')
											{
												$label1 = sprintf ( JText::_( 'CC_CONFIG_INVOICE_FORMAT_LABEL1' ), $this->invoice_format);
												echo $label1;
											}else
											{
												$label2 = sprintf ( JText::_( 'CC_CONFIG_INVOICE_FORMAT_LABEL1' ), $this->invoice_format);
												echo $label2;
											}
										 ?>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_CONFIG_INVOICE_START_LABEL' ); ?>
									</td>
									<td>
										<input type="text" name="invoice_start" value="<?php echo $row->invoice_start; ?>" size="30"/><br/>

									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<div style="float:left;padding-right:10px;">
										<?php
											$custom_inv =  $this->invoice_format;
											$label1 = sprintf ( JText::_( 'CC_CONFIG_INVOICE_START_LABEL1' ), $custom_inv);
											echo $label1;
										?>
										</div>
										<div style="float:none;">
										<?php $setpath=JURI::root()."administrator/components/com_ccinvoices/assets/images/error_delete.png"; ?>
										<a href="#" onclick="javascript: submitbutton('configuration.reset_inv')"><img style="margin:0px;" src="<?php echo $setpath; ?>">&nbsp;&nbsp;<?php  echo JText::_( 'CC_CONFIG_INVOICE_RESET_LABEL1' ); ?></a>
										</div>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_CONFIG_DATE_FORMAT_LABEL' ); ?>
									</td>
									<td>
										<input type="text" name="date_format" value="<?php echo $row->date_format; ?>" size="30"/><?php

										 ?>

									</td>
								</tr>
								<tr>
									<td></td>
									<td>
										<?php
											if($row->date_format != '')
											{
												$label1 = sprintf ( JText::_( 'CC_CONFIG_DATE_FORMAT_LABEL1' ), strftime($row->date_format, time()));
										 	?>
												<a href="http://php.net/manual/en/function.strftime.php" target="_blank"><?php echo $label1; ?></a>
											<?php
											}else
											{
												$label2 = sprintf ( JText::_( 'CC_CONFIG_DATE_FORMAT_LABEL1' ),strftime( "%m-%d-%y", time()));
										 	?>
												<a href="http://php.net/manual/en/function.strftime.php" target="_blank"><?php echo $label2; ?></a>
											<?php
											}
										 ?>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_DEFAULT_DUE_DAYS' ); ?>
									</td>
									<td>
										<input type="text" name="default_due_days" value="<?php echo $row->default_due_days; ?>" size="30"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
				</fieldset>
<!-- invoice settings ending-->
<!-- tax settings starting-->
			<fieldset   style="padding:10px;background-color:white;">
				<legend><?php echo JText::_( 'CC_TAX_SETTINGS' ); ?></legend>
					<table cellpadding="3" cellspacing="0" width="100%" border="0">
						<tr>
							<td align="left" >
							<table cellpadding="0" cellspacing="4" border="0" width="100%">
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_DEFAULT_TAX' ); ?>
									</td>
									<td>
										<input type="text" name="default_tax" value="<?php echo $row->default_tax; ?>" size="30"/>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_TAX_VALUES' ); ?>
									</td>
									<td align="left">
										<input type="text" name="tax" value="<?php echo $row->tax; ?>" size="30"/>
									</td>
								</tr>
								<tr>
									<td>&nbsp;</td>
									<td><?php echo JText::_( 'CC_TAX_EXAMPLE' ); ?></td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
				</fieldset>
<!-- tax settings ending-->
<!-- currency settings starting-->
			<fieldset  style="padding:10px;background-color:white;" >
				<legend><?php echo JText::_( 'CC_CURRENCY_SETTINGS' ); ?></legend>
					<table cellpadding="3" cellspacing="0" width="100%" border="0">
						<tr>
							<td align="left" >
							<table cellpadding="0" cellspacing="4" border="0" width="100%">

								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_CURRENCY_SYMBOL' ); ?>
									</td>
									<td>
										<div class="ccinvoices_conf_currency1">
											<input type="text" name="currency_symbol" value="<?php echo htmlentities($row->currency_symbol); ?>" size="10"/>
										</div>
										<div style="float:left;margin-top:8px;">
											<?php echo JText::_( 'CC_CURRENCY_SYMBOL_BACK' ); ?>
										</div>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_DISPLAYCURRENCY_SYMBOL' ); ?>
									</td>
									<td>
									<?php
										if($row->symbol_display == "1")
										{
											$selectBEF = "checked";
											$selectAFT = "";

										}else
										{
											$selectBEF = "";
											$selectAFT = "checked";
										}
									?>
										<table>
											<tr>
											<td><input type="radio" name="symbol_display" value="1" <?php echo $selectBEF; ?> /></td>
											<td> <?php echo JText::_( 'CC_SYMBOL_TEXT_1' ); ?>&nbsp;</td>
											<td><input type="radio" name="symbol_display" value="0" <?php echo $selectAFT; ?> /></td>
											<td> <?php echo JText::_( 'CC_SYMBOL_TEXT_2' ); ?></td>
											</tr>
										</table>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_DISPLAYCURRENCY_FORMAT' ); ?>
									</td>
									<td>
									<?php
							            $cformat[] = JHTML::_('select.option',  '0',  JText::_( '1,000,000.00'));
							            $cformat[] = JHTML::_('select.option',  '1', JText::_( '1.000.000,00'));
							            $cformat[] = JHTML::_('select.option',  '2', JText::_( '1 000 000.00'));
							            $cformat[] = JHTML::_('select.option',  '3', JText::_( "1'000'000.00"));
							            echo JHTML::_('select.genericlist',  $cformat, 'cformat', 'class="inputbox" size="1" ', 'value', 'text', $row->cformat );
									?>
									</td>
								</tr>
							</table>
							</td>
						</tr>
					</table>
				</fieldset>
<!-- currency settings ending-->

<!-- email settings starting-->
			<fieldset  style="padding:10px;background-color:white;" >
				<legend><?php echo JText::_( 'CC_COMMUNICATION_SETTINGS' ); ?></legend>
					<table cellpadding="3" cellspacing="0" width="100%" border="0">
						<tr>
							<td align="left" >
							<table cellpadding="0" cellspacing="4" border="0" width="100%">
								<tr>
									<td align="left" width="20%" valign="top" colspan="2"><?php echo JText::_( 'CC_CONFIGURATION_EMAIL_SETTING_MSG' ); ?></td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_EMAIL_CC' ); ?>
									</td>
									<td>
										<input type="text" name="email_cc" value="<?php echo $row->email_cc; ?>" size="30"/>
									</td>
								</tr>
								<tr>
									<td align="left" width="20%" >
										<?php echo JText::_( 'CC_EMAIL_BCC' ); ?>
									</td>
									<td>
										<input type="text" name="email_bcc" value="<?php echo $row->email_bcc; ?>" size="30"/>
									</td>
								</tr>
							</table>
						</td>
					</tr>
				</table>
			</fieldset>
		</td>
		<td width="30%" valign="top">

			<fieldset  style="padding:10px;background-color:white;" >
				<legend><?php echo JText::_( 'CC_COMPANY_DETAILS' ); ?></legend>
				<table cellpadding="3" cellspacing="0"  width="100%" border="0">
					<tr>
						<td  colspan="2" align="left"  >
							<?php echo JText::_("CC_COMPANY_DETAILS_DESC"); ?>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="left" valign="top" >
							&nbsp;
						</td>
					</tr>
					<tr>
						<td width="25%" align="left"  >
							<?php echo JText::_("CC_USER_NAME"); ?>
			</td>
			<td width="45%" align="left">
							<input type="text" name="user_name" value="<?php echo $row->user_name; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
							<?php echo JText::_("CC_USER_COMPANY"); ?>
			</td>
			<td align="left">
							<input type="text" name="user_company" value="<?php echo $row->user_company; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
				<?php echo JText::_("CC_COMPANY_EMAIL"); ?>
			</td>
			<td align="left">
				<input type="text" name="company_email" value="<?php echo $row->company_email; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
				<?php echo JText::_("CC_COMPANY_PHONE"); ?>
			</td>
			<td align="left">
				<input type="text" name="company_phone" value="<?php echo $row->company_phone; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
				<?php echo JText::_("CC_COMPANY_URL"); ?>
			</td>
			<td align="left">
				<input type="text" name="company_url" value="<?php echo $row->company_url; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
				<?php echo JText::_("CC_TAX_ID"); ?>
			</td>
			<td align="left">
				<input type="text" name="tax_id" value="<?php echo $row->tax_id; ?>" />
			</td>
		</tr>
		<tr>
						<td width="25%" align="left"  >
				<?php echo JText::_("CC_COMPANY_ADDRESS"); ?>
			</td>
			<td align="left">
				<textarea cols="10" rows="4" style="width:250px;" name="company_address"><?php echo $row->company_address; ?></textarea>
			</td>
		</tr>
					<tr>
						<td width="25%" align="left"  >
							<?php echo JText::_("CC_OTHER_DETAILS"); ?>
						</td>
						<td align="left">
							<textarea cols="10" style="width:250px;" rows="4" name="other_details"><?php echo $row->other_details; ?></textarea>
						</td>
					</tr>
	</table>
	</fieldset>
			<fieldset  style="padding:10px;background-color:white;" >
				<legend><?php echo JText::_( 'CC_COMPANY_LOGO' ); ?></legend>
				<table cellpadding="3" cellspacing="0" width="100%" border="0">
					<tr>
						<td colspan="2" align="left"  >
							<?php echo JText::_("CC_COMPANY_LOGO_DESC"); ?>
						</td>
					</tr>
					<tr>
						<td colspan="2" align="left"  >
							<input class="input-small" type="file" name="logo" id="logo" onchange="allowedFileExtension();">&nbsp;<input class="btn btn-primary"type="button" onclick="document.adminForm.task.value='configuration.apply';document.adminForm.submit();" name="upload_logo" value="<?php echo JText::_("CC_UPLOAD_LOGO"); ?>" />
						</td>
					</tr>
					<tr>
						<td colspan="2" align="center" >
							<?php if($row->logo != '') { ?>
								<img src="<?php echo JURI::root(); ?>media/com_ccinvoices/logo/<?php echo $row->logo; ?>">
							<?php } ?>

						</td>
					</tr>
				</table>
			</fieldset>
		</td>
	</tr>
</table>
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="path" value="<?php echo JURI::base();?>">
<input type="hidden" name="controller" value="configuration">
<input type="hidden" name="task" value="">
<input type="hidden" name="id" value="1">
<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>
<table width="100%"><tr><td align="center">

<?php

	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>