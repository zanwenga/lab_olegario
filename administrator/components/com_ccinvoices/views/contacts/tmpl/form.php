<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');


$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.modal', $params);
?>
<?php JHTML::_('behavior.tooltip');
 ?>
<script type="text/javascript">
    window.addEvent('domready', function(){
	   //do your tips stuff in here...
	   var zoomTip = new Tips($$('.hasTip3'), {
	      className: 'custom3', //this is the prefix for the CSS class
	      offsets: {
	  		'x': 20,       //default is 16
	  		'y': 30        //default is 16
              },
	      initialize:function(){
	         this.fx = new Fx.Style(this.toolTip, 'opacity',
	        		 {duration: 1000, wait: false}).set(0);
	      },
	      onShow: function(toolTip) {
	         this.fx.start(0,.8);
	      },
	      onHide: function(toolTip) {
	         this.fx.start(.8,0);
	      }
	   });
	});
</script>

<script language="javascript">
function submitbutton(pressbutton)
{
	var reg = /^([A-Za-z0-9_\-\.])+\@([A-Za-z0-9_\-\.])+\.([A-Za-z]{2,4})$/;
	var form =document.adminForm;
	if (pressbutton =='cancel')
	{
		submitform(pressbutton );
		return;
	}
	if(document.getElementById("contact_name").value =="")
	{
		alert("<?php echo JText::_("CC_MSG_ENTER_COMPANY_NAME"); ?>");
		return;
	}
	else if (form.name.value == "")
	{
		alert( "<?php echo JText::_( 'CC_ENTER_NAME'); ?>" );
		form.name.focus();
	}else if (form.email.value == "")
	{
    	alert( "<?php echo JText::_( 'CC_ENTER_EMAIL'); ?>" );
        form.email.focus();
	}else if(reg.test(form.email.value) == false)
	{
    	alert('<?php echo JText::_("CC_ENTER_VALID_EMAIL"); ?>');
        form.email.focus();
	}else
	{
		submitform(pressbutton );
	}
}
function changeThisUrl()
{
	cont_id_tmp = document.getElementById("contact_id_url");
	temp = cont_id_tmp.href;
	if((temp.split('index.php').length -1) > 0)
	{
		cont_id_tmp.className = "modal";
		cont_id_tmp.rel = "{handler: 'iframe', size: {x: 680, y: 500}}"
	}else
	{
		cont_id_tmp.rel = "test";
		cont_id_tmp.className = "test"
		alert("<?php echo JText::_( 'CC_CHOOSE_CONTACT' ); ?>");
	}

}
</script>

<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/invoice-contactc-list.js"></script>
<table cellpadding="0" cellspacing="0" width="100%"><tr><td align="center" width="100%">
<table cellpadding="0" cellspacing="0" border="0" width="400" border="0">
<tr><td align="center" width="100%">
<form action="index.php" method="post" name="adminForm" id="adminForm" autocomplete="off">
	<div>
		<fieldset style="padding:10px;">
		<legend><?php echo JText::_( 'CC_INVOICES_CONTACTS' ); ?></legend>
		<table width="100%" border="0">
		<tr>
			<td width="100" align="right" class="key" valign="top">
				<div class="hasTip" title="<?php echo JText::_( 'CC_INVOICE_NAME_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_INVOICE_NAME' ) ?></div>
			</td>
			<td>
	           <input type="text" size="40" id="contact_name" name="name" value="<?php echo $this->row->name;?>" >
			   <input type="hidden" id="contact_hidden" name="contact_name_id" value="">
			   <input type="hidden" id="contact_id" name="contact_id" value="<?php echo $this->row->id;?>">
			   <!--<input type="hidden" id="contactname" name="name" value="<?php echo $this->row->name;?>">-->
				<span id="contactajaxbox"> </span>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" valign="top">
					<div class="hasTip" title="<?php echo JText::_( 'CC_INVOICES_CONTACT_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_INVOICES_CONTACT' ) ?></div>
			</td>
			<td>
				<input class="text_area" type="text" name="contact" id="cc_contact" size="40" maxlength="256" value="<?php echo $this->row->contact;?>"  />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" valign="top">
					<div class="hasTip" title="<?php echo JText::_( 'CC_CONTACT_NUMBER_TOOLTIP' ) ?>"><?php echo JText::_( 'CC_CONTACT_NUMBER' ); ?></div>
			</td>
			<td>
				<table cellspacing="0" cellpadding="0">
					<tr>
						<td>
 				 			<input type="text" size="10" id="contact_number" name="contact_number" value="<?php echo $this->row->contact_number;?>">
 				 		</td>
 				 	</tr>
					<tr>
						<td>
 				 			<?php echo sprintf(JText::_( 'CC_INVOICES_LAST_CONTACT_NUMBER' ),$this->lastcontactid); ?>
 				 		</td>
 				 	</tr>
 				 </table>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" valign="top">
					<?php echo JText::_( 'CC_INVOICE_ADDRESS' ); ?>
			</td>
			<td>
				<textarea class="text_area"  name="address" id="cc_address" style="width:190px;" cols="28" rows="5" ><?php echo $this->row->address;?></textarea>
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" valign="top">
					<?php echo JText::_( 'CC_CONTACT_TAX_ID' ); ?>
			</td>
			<td>
				<input class="text_area" type="text" name="tax_id" id="tax_id" size="30" maxlength="30" value="<?php echo $this->row->tax_id;?>"  />
			</td>
		</tr>
		<tr>
			<td width="100" align="right" class="key" valign="top">
					<?php echo JText::_( 'CC_EMAIL' ); ?>
			</td>
			<td>
				<input class="text_area" type="text" name="email" id="cc_email" size="40" maxlength="256" value="<?php echo $this->row->email;?>"  />
			</td>
		</tr>
		<tr>
			<td colspan="2">&nbsp;</td>
		</tr>
		<tr>
			<td colspan="2">
				<table cellpadding="0" cellspacing="0" border="0" width="100%" border="0">
					<tr>
		<?php
		if( $this->row->id)
		{
		?>
						<td align="right" width="33%">
							<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td align="right" width="16">
							<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/add.png" width="16" height="16">
						</td>
						<td align="left" valign="middle">
							<a href="index.php?option=com_ccinvoices&task=invoices.edit&clientid=<?php echo $this->row->id ?>"><?php echo JText::_("CC_NEW_INVOICE"); ?></a>
							<!--<a href="index.php?option=com_ccinvoices&controller=invoices&task=invoiceforcontact&clientid=<?php echo $this->row->id ?>"><?php echo JText::_("CC_NEW_INVOICE"); ?></a>-->
						</td>
					</tr>
				</table>
			</td>
			<td align="right" width="40%">
				<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td align="right" width="16">
							<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/view.png" width="16" height="16">
						</td>
						<td align="left" valign="middle">
							<a href="index.php?option=com_ccinvoices&view=invoices&clientid=<?php echo $this->row->id ?>"><?php echo JText::_("CC_VIEW_ALL_INVOICE"); ?></a>
						</td>
					</tr>
				</table>
			</td>
				<?php
				}else {
				?>
					<td width="33%">&nbsp;</td><td width="33%">&nbsp;</td>
		<?php
		}
		?>
						<td  align="right"  width="33%">
							<table cellpadding="0" cellspacing="0" width="100%" border="0">
					<tr>
						<td align="right" width="16">
							<?php if($this->row->id != ''){ ?>
								<img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/manage_users.png" width="16" height="16">
							<?php }?>
						</td>
						<td align="left" valign="middle">
							<?php if($this->row->id == ''){ ?>
							<?php }else { ?>
								<a class="modal"  rel="{handler: 'iframe', size: {x: 900, y: 500}}" id="contact_id_url" href="index.php?option=com_ccinvoices&view=assignusers&contact_id=<?php echo $this->row->id ; ?>&tmpl=component" ><?php echo JText::_("CC_MANAGE_USERS"); ?></a>
							<?php }?>
						</td>
					</tr>
				</table>
			</td>
		</tr>
				</table>
			</td>
		</tr>


		</table>
		</fieldset>
	</div>
	<input type="hidden" name="id" value="<?php echo $this->row->id ?>" />
	<input type="hidden" name="cid[]" value="" />
	<input type="hidden" name="option" value="com_ccinvoices" />
	<input type="hidden" name="controller" value="contacts" />
	<input type="hidden" name="task" value="" />
	<input type="hidden" name="cnumber" value="<?php echo $this->contact_number;?>"/>
    <input type="hidden" name="path" id="path" value="<?php echo JURI::root();?>" />
    <input type="hidden" id="contact_txt" value="">
	<input type="hidden" name="add_con_msg" value="<?php echo JText::_("CC_CREATE_CONTACT"); ?>"/>
	<input type="hidden" name="update_con_msg" value="<?php echo JText::_("CC_UPDATE_CONTACT"); ?>"/>
	<?php echo JHTML::_( 'form.token' ); ?>
</form>
</td></tr></table></td></tr></table>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>