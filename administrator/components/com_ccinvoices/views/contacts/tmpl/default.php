<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');

$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('dropdown.init');
JHtml::_('formbehavior.chosen', 'select');

?>
<script language="javascript" type="text/javascript">
function createFilter(filter)
{
	document.getElementById('filter').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter').value = '';
	submitform();
}
</script>
<script type="text/javascript">
function newinvoice()
{
	var chks = document.getElementsByName('cid[]');
	var hasChecked = false;
	var blocklist=document.getElementById('boxchecked').value;
	if(blocklist == '0')
	{
		window.location='index.php?option=com_ccinvoices&controller=invoices&task=invoiceforcontact';
	}else
	{
		for (var i = 0; i < chks.length; i++)
		{
			if (chks[i].checked)
			{
				var idid=chks[i].value;
				window.location='index.php?option=com_ccinvoices&controller=invoices&task=invoiceforcontact&clientid='+idid;
			}
		}
	}
	return true;
}
</script>

<style>
.adminlist
{
	font-size:11px;
}
.adminlist p
{
	margin:0px;
	padding:0px;
}
</style>
<form action="index.php" method="post" name="adminForm" id="adminForm">
<?php if(!empty( $this->sidebar)): ?>
	<div id="j-sidebar-container" class="span2">
		<?php echo $this->sidebar; ?>
	</div>
	<div id="j-main-container" class="span10">
<?php else : ?>
	<div id="j-main-container">
<?php endif;?>
<table>
	<tr>
		<td align="left" width="100%">
			<div id="filter-bar" class="btn-toolbar">
				<div class="filter-search btn-group pull-left">
					<input type="text" placeholder="<?php echo JText::_('CCINVOICES_CONTACT_SEARCH_BOX_PLACEHOLDER_LBL'); ?>" class="input-medium search-query" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="" />
				</div>
				<div class="btn-group hidden-phone">
					<button class="btn tip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
					<button class="btn tip" type="button" onclick="document.id('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
				</div>
			</div>
		</td>
	</tr>
</table>
<table class="table table-striped">
<thead>
	<tr>
		<th width="1%" align="left">
			<input type="checkbox" name="checkall-toggle" value="" onclick="Joomla.checkAll(this)" />
		</th>
		<th width="20%" nowrap="nowrap" align="center">
			<?php echo JHTML::_('grid.sort', JText::_( 'CC_NAME' ), 'c.name', $listDirn, $listOrder); ?>
		</th>
		<th width="10%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ID' ), 'c.contact_number', $listDirn, $listOrder); ?>
		</th>
		<th align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_CONTACT_HEAD' ), 'c.contact', $listDirn, $listOrder); ?>
		</th>
		<th width="22%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_EMAIL' ), 'c.email', $listDirn, $listOrder); ?>
		</th>
        <th width="20%" align="center">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_ADDRESS' ), 'c.address', $listDirn, $listOrder); ?>
		</th>
	</tr>
</thead>
<tfoot>
	<tr>
		<td colspan="9">
			<?php
				echo $this->pagination->getLimitBox();
				echo $this->pagination->getListFooter();
			?>
		</td>
	</tr>
</tfoot>
<?php
$k = 0;
for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
	$checked 	= JHTML::_('grid.id',   $i, $row->id );

	if($this->editpermission=="1")
		$link 		= JRoute::_( 'index.php?option=com_ccinvoices&task=contacts.edit&cid[]='. $row->id );
	else
		$link 		= "#";
	?>
	<tr class="<?php echo "row$k"; ?>">
		<td align="center">
			<?php echo $checked; ?>
		</td>
		<td align="center" class="nowrap has-context">

			<div class="pull-left">
				<a href="<?php echo $link; ?>"><?php echo $row->name; ?></a>
			</div>
			<div class="pull-left">
				<?php
					// Create dropdown items
					JHtml::_('dropdown.edit', $row->id, 'contacts.');
					JHtml::_('dropdown.divider');
					JHtml::_('dropdown.trash', 'cb' . $i, 'contacts.');


					// Render dropdown list
					echo JHtml::_('dropdown.render');
					?>
			</div>
		</td>
		<td align="center">
			<?php echo $row->contact_number; ?>
		</td>
		<td align="center">
			<?php echo $row->contact; ?>
		</td>
		<td align="center">
			<?php echo $row->email; ?>
		</td>
		<td align="center">
			<?php echo substr($row->address, 0, 15); ?>
		</td>
	</tr>
	<?php
	$k = 1 - $k;
}
?>
</table>
<input type="hidden" name="option" value="com_ccinvoices" />
<input type="hidden" name="task" value="" />
<input type="hidden" name="boxchecked"  id="boxchecked" value="0" />
<input type="hidden" name="controller" value="contacts" />
<input type="hidden" name="view" value="contacts" />
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="invoice_filter" value="" id="filter" />
<?php echo JHTML::_( 'form.token' ); ?>
</div>
</form>

<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<p class="copyright" style="text-align:center;" >
<?php echo JText::_($this->name); ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>