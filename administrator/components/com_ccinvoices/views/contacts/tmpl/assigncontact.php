<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
defined('_JEXEC') or die('Restricted access');
JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
?>
<script language="javascript" type="text/javascript">
function createFilter(filter)
{
	document.getElementById('filter').value = filter;
	submitform();
}

function eraseFilter()
{
	document.getElementById('filter').value = '';
	submitform();
}
</script>
<script language ="javascript">


function addContact(form,cid)
{

	window.parent.document.getElementById('contact_name').value = document.getElementById('name'+cid).value;
	window.parent.document.getElementById('cc_contact').value = document.getElementById('contact'+cid).value;
	window.parent.document.getElementById('contact_number').value = document.getElementById('contact_number'+cid).value;
	window.parent.document.getElementById('cc_email').value = document.getElementById('email'+cid).value;
	window.parent.document.getElementById('cc_address').value = document.getElementById('ccaddress'+cid).innerHTML;
	window.parent.document.getElementById('contact_id').value = document.getElementById('cid'+cid).value;
	window.parent.document.getElementById('contact_hidden').value = document.getElementById('cid'+cid).value;
	window.parent.document.getElementById('cc_tax_id').value = document.getElementById('tax_id'+cid).value;
	window.parent.document.getElementById('contact_txt').innerHTML =  document.adminForm.update_con_msg.value;

	cont_id_tmp = window.parent.document.getElementById("contact_id_url");
	cont_id_tmp.href = "index.php?option=com_ccinvoices&view=assignusers&contact_id="+cid+"&tmpl=component";
	cont_id_tmp.className = "modal";
	cont_id_tmp.rel = "{handler: 'iframe', size: {x: 680, y: 500}}"

	window.parent.SqueezeBox.close();
}
</script>

<form action="index.php" method="post" name="adminForm">
<?php

$my =  JFactory::getUser ();
?>

<table cellpadding="0" cellspacing="0" width="100%" border="0">
	<tr>
		<td width="70%" valign="top">
			<ul class="nav nav-tabs">
				<li class="active"><a href="#page-site" data-toggle="tab"><?php echo JText::_('CC_ADD_EXISTING_CONTACT');?></a></li>
			</ul>
			<table cellpadding="0" cellspacing="0" width="100%" border="0">
				<tr>
					<td>
						<div id="filter-bar" class="btn-toolbar">
							<div class="filter-search btn-group pull-left">
								<input type="text" name="filter_search" id="filter_search" value="<?php echo $this->escape($this->state->get('filter.search')); ?>" title="" />
							</div>
							<div class="btn-group hidden-phone">
								<button class="btn tip" type="submit" title="<?php echo JText::_('JSEARCH_FILTER_SUBMIT'); ?>"><i class="icon-search"></i></button>
								<button class="btn tip" type="button" onclick="document.id('filter_search').value='';this.form.submit();" title="<?php echo JText::_('JSEARCH_FILTER_CLEAR'); ?>"><i class="icon-remove"></i></button>
							</div>
						</div>
					</td>
				</tr>
			</table>
			<div id="userListArea">
			<table cellpadding="0" style="margin-top:3px;" cellspacing="0" width="100%" border="0" class="table table-striped">
			<thead>
				<tr align="center">
					<th width="12%" align="center">
						<?php echo JHTML::_('grid.sort', JText::_( 'CC_CONTACT_NUMBER' ),'c.name', $listDirn, $listOrder); ?>
					</th>
					<th width="15%" align="center">
						<?php echo JHTML::_('grid.sort', JText::_( 'CC_NAME' ), 'c.name', $listDirn, $listOrder ); ?>
					</th>
					<th width="15%" align="center">
						<?php echo JHTML::_('grid.sort', JText::_( 'CC_CONTACT' ), 'c.contact', $listDirn, $listOrder); ?>
					</th>
					<th  align="center">
						<?php echo JHTML::_('grid.sort', JText::_( 'CC_ADDRESS' ), 'c.address', $listDirn, $listOrder ); ?>
					</th>
					<th  align="center" width="170px">
						<?php echo JHTML::_('grid.sort', JText::_( 'CC_EMAIL' ), 'c.email', $listDirn, $listOrder ); ?>
					</th>
					<th width="14%" align="center">&nbsp;
					</th>
				</tr>
			</thead>
<?php
$k = 0;
for ($i=0, $n=count( $this->items ); $i < $n; $i++)
{
	$row = &$this->items[$i];
?>
				<tr align="center" class="<?php echo "row$k"; ?>">
					<td align="center" >
						<a href="javascript::void(0);" style="color:#666666;" onclick="addContact(this.form,'<?php echo $row->id; ?>')" >
						<?php echo $row->contact_number; ?>
						</a>
						<input type="hidden" id="contact_number<?php echo $row->id; ?>" value="<?php echo $row->contact_number; ?>" />
					</td>
					<td align="center">
						<a href="javascript::void(0);" style="color:#666666;" onclick="addContact(this.form,'<?php echo $row->id; ?>')" >
						<?php echo $row->name; ?>
						</a>
						<input type="hidden" id="name<?php echo $row->id; ?>" value="<?php echo $row->name; ?>" />
					</td>
					<td align="center">
						<a href="javascript::void(0);" style="color:#666666;" onclick="addContact(this.form,'<?php echo $row->id; ?>')" >
						<?php echo $row->contact; ?>
						</a>
						<input type="hidden" id="contact<?php echo $row->id; ?>" value="<?php echo $row->contact; ?>" />
					</td>
					<td align="center" width="200px">
						<a href="javascript::void(0);" style="color:#666666;" onclick="addContact(this.form,'<?php echo $row->id; ?>')" >
						<?php
							 $count_str = "25";
							echo substr($row->address, 0, $count_str);
							?>
						</a>
						<div id="ccaddress<?php echo $row->id; ?>" style="visibility:collapse;height:8px;"><?php echo $row->address; ?></div>

					</td>
					<td align="center" width="170px">
						<a href="javascript::void(0);" style="color:#666666;" onclick="addContact(this.form,'<?php echo $row->id; ?>')" >
							<?php
								if(strlen($row->email)>30)
								{
									echo substr($row->email,0,30)."...";
								}
								else
								{
									echo $row->email;
								}
							?>
						</a>
						<input type="hidden" id="email<?php echo $row->id; ?>" value="<?php echo $row->email; ?>" />
						<input type="hidden" id="tax_id<?php echo $row->id; ?>" value="<?php echo $row->tax_id; ?>" />
						<input type="hidden" id="cid<?php echo $row->id; ?>" value="<?php echo $row->id; ?>" />
					</td>
					<td align="center">
						<a id="sbox-btn-close" href="javascript::void(0);" onclick="addContact(this.form,'<?php echo $row->id; ?>')" ><img src="<?php echo JURI::root(); ?>/administrator/components/com_ccinvoices/assets/images/user_add.png" width="16" height="16">&nbsp;Add existing contact to invoice</a>
					</td>
				</tr>
<?php
	$k = 1 - $k;
}
?>
			</table>
</div>
		<?php
		$contactDetails = $this->contactDetails;
		?>

		</td>
	</tr>
</table>


<input type="hidden" name="path" value="<?php echo JURI::base();?>">
<input type="hidden" name="root_path" value="<?php echo JURI::root();?>">
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="controller" value="contacts">
<input type="hidden" name="view" value="contacts">
<input type="hidden" name="layout" value="assigncontact">
<input type="hidden" name="task" value="modelassigncontact">
<input type="hidden" name="filter_order" value="<?php echo $listOrder; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo $listDirn; ?>" />
<input type="hidden" name="tmpl" value="component">
<input type="hidden" name="add_con_msg" value="<?php echo JText::_("CC_CREATE_CONTACT"); ?>"/>
<input type="hidden" name="update_con_msg" value="<?php echo JText::_("CC_UPDATE_CONTACT"); ?>"/>
<input type="hidden" name="invoice_filter" value="" id="filter" />
</form>
<table width="100%"><tr><td align="center">
<?php
	if (isset($this->versionContent)) {
		echo $this->versionContent;
	}
?>
</td></tr></table>
<!--
<p class="copyright" style="text-align:center;" >
<?php echo $this->name; ?>&nbsp;<?php echo $this->version; ?>. Copyright (C) 2006 - <?php echo $curYear = date('Y'); ?>  Chill Creations<br/>Joomla! component by <a href="http://www.chillcreations.com" target="_blank">Chill Creations</a>
</p>-->