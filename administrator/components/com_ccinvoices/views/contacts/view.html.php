<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// no direct access
defined('_JEXEC') or die('Restricted access');
// Import Joomla! libraries

class ccInvoicesViewcontacts extends CCINVOISViews
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$model =  $this->getModel();

		// Access check.
		if (!JFactory::getUser()->authorise('ccinvoices.view.contacts', 'com_ccinvoices'))
		{
			return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
		}

		if($this->_layout == 'form')
		{
			require_once JPATH_COMPONENT.'/helper/ccinvoices.php';
			$canDo = ccinvoicesHelper::getActions();

			// Access check.
			if (!JFactory::getUser()->authorise('ccinvoices.view.contacts', 'com_ccinvoices'))
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}

			if (!JFactory::getUser()->authorise('ccinvoices.edit', 'com_ccinvoices'))
			{
				return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
			}


			$query	= "SELECT max(contact_number) FROM #__ccinvoices_contacts";
	        $db->setQuery($query);
	        $contact_id1 = $db->loadResult();
	        $contact_number = $contact_id1 + 1 ;

			$this->addToolBarForm();

			$row		= $this->get('contacts');

			$this->assignRef('row',			$row);
			$this->assignRef('contact_number',		$contact_number);
			$this->lastcontactid=$this->getModel()->getLastContactId();

			if(versionCompare()>=3)
			{
				$this->_layout='form';
			}
			else
			{
				$this->_layout='form25';
			}
			$this->addFormCss();
		}
		elseif($this->_layout == 'assigncontact' || $this->_layout == 'assigncontact25')
		{
			$document = JFactory::getDocument();
			$document->addStylesheet( JURI::base() . 'components/com_ccinvoices/assets/css/ccinvoice.css' );
			$inv_id = JRequest::getInt("inv_id","");
			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');

			$this->items      = $this->get('Items');
			$this->pagination = $this->get('Pagination');
			$this->state      = $this->get('State');

			// Check for errors.
			if (count($errors = $this->get('Errors')))
			{
				JError::raiseError(500, implode("\n", $errors));
				return false;
			}

			$userAssined=array();
			if($inv_id!="")
			{
				$sql = "SELECT c.id,c.name,c.contact,c.contact_number,c.address,c.email  FROM #__ccinvoices_contacts AS c"
					. " LEFT JOIN #__ccinvoices_invoices AS i ON i.contact_id = c.id "
					. " WHERE iu.contact_id = ".$inv_id
					;
				$db->setQuery($sql);
				$userAssined = $db->loadObjectList();
			}

			$sql = "SELECT * FROM #__ccinvoices_contacts WHERE id =".$inv_id." LIMIT 1";
			$db->setQuery($sql);
			$contactDetails = $db->loadObject();

			$this->assignRef("userAssined",$userAssined);
			$this->assignRef("inv_id",$inv_id);
			$this->assignRef("contactDetails",$contactDetails);

		}
		else
		{
			JHTML::_('behavior.calendar');

			$filter					= JRequest::getVar('invoice_filter');
			$sortColumn		= JRequest::getVar('filter_order','ordering');
			$sortOrder		= JRequest::getVar('filter_order_Dir','asc');

			$this->items      = $this->get('Items');
			$this->pagination = $this->get('Pagination');
			$this->state      = $this->get('State');

			// Check for errors.
			if (count($errors = $this->get('Errors')))
			{
				JError::raiseError(500, implode("\n", $errors));
				return false;
			}

			if(versionCompare()>=3)
			{
				$this->sidebar = JHtmlSidebar::render();
				$this->_layout='default';
			}
			else
			{
				$this->_layout='default25';
			}
			$this->addToolBar();
		}

		$this->getVersionInfo();
		$this->addCss();

		parent::display($tpl);
	}
	function getVersionInfo()
	{

		$adminDir = JPATH_ADMINISTRATOR .'/components';
		$siteDir = JPATH_SITE .'/components';

		$file = $adminDir.'/com_ccinvoices/ccinvoices.xml';
		$contents = file_get_contents($file);
		$doc = new SimpleXmlElement($contents, LIBXML_NOCDATA);

		$this->assignRef('version',		$doc->version[0]);
		$this->assignRef('name',		$doc->name[0]);
	}
	function addToolBarForm()
	{
		$row		= $this->get('contacts');
		$text = $row->id ? JText::_( 'CC_EDIT' ) : JText::_( 'CC_NEW' );
		JToolBarHelper::title(   JText::_( 'CC_INVOICES' ).': ' . $text.' ', 'ccinvoices.png');
		JToolbarHelper::apply('contacts.apply',JText::_( 'CCINVOICES_CONTACTS_APPLY_TOOL' ));
		JToolBarHelper::save('contacts.save',JText::_( 'CCINVOICES_CONTACTS_SAVE_TOOL' ));
		JToolBarHelper::save2new('contacts.save2new',JText::_( 'CCINVOICES_CONTACTS_SAVENEW_TOOL' ));
		if($row->id)
			JToolBarHelper::save2copy('contacts.save2copy',JText::_( 'CCINVOICES_CONTACTS_SAVECOPY_TOOL' ));
		JToolBarHelper::cancel('contacts.cancel',JText::_( 'CCINVOICES_CONTACTS_CLOSE_TOOL' ));
	}
	function addToolBar()
	{
		JToolBarHelper::title(JText::_( 'CC_INVOICES' ) . ':  ' .  JText::_( 'CC_CONTACTS_HEADER' ).' ', 'ccinvoices.png' );
		$editpermission="1";
		$canDo = ccinvoicesHelper::getActions();
		if ($canDo->get('ccinvoices.create'))
			JToolBarHelper::addNew('contacts.add',JText::_( 'CCINVOICES_CONTACTS_NEW_TOOL' ));
		if ($canDo->get('ccinvoices.edit'))
			JToolbarHelper::editList('contacts.edit',JText::_( 'CCINVOICES_CONTACTS_EDIT_TOOL' ));
		if ($canDo->get('ccinvoices.copy'))
		{
			JToolBarHelper::custom( 'contacts.copy', 'copy.png', 'copy_f2.png',  JText::_( 'CC_INVOICES_TOOLBAR_COPY') , true );
		}
		if ($canDo->get('ccinvoices.delete'))
			JToolbarHelper::deleteList('', 'contacts.remove', 'CCINVOICES_CONTACTS_REMOVE_TOOL');
		if ($canDo->get('core.admin')) {
			JToolBarHelper::divider();
			JToolBarHelper::preferences('com_ccinvoices','500','800', JText::_( 'CCINVOICES_PERMISSION_TOOL_LBL' ));
		}
		$editpermission=$canDo->get('ccinvoices.edit');
		$this->assignRef('editpermission',		$editpermission);
	}
	function addCss()
	{
		$document = JFactory::getDocument();
		$document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	}
	function addFormCss()
	{
		$document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/main.css');
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/thickbox.css');
	}
}
?>