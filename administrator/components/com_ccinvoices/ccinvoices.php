<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

defined('_JEXEC') or die;
@define( 'DS', DIRECTORY_SEPARATOR );

$language = JFactory::getLanguage();
$language->load('com_ccinvoices', JPATH_ADMINISTRATOR, 'en-GB', true);
$language->load('com_ccinvoices', JPATH_ADMINISTRATOR, null, true);

function versionCompareForAlpha()
{
	$jversion = new JVersion();
	return $current_version =  $jversion->getShortVersion();
}

function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,1);
}

if(versionCompare()>=3)
{
	$className='JControllerLegacy';
}
else
{
	jimport('joomla.application.component.controller');
	$className='JController';
}
if (!JFactory::getUser()->authorise('core.manage', 'com_ccinvoices'))
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

//Controllers
if(versionCompare()>=3)
{
	class CCINVOISController extends JControllerLegacy{}
}
else
{
	class CCINVOISController extends JController{}
}

//Views
if(versionCompare()>=3)
{
	class CCINVOISViews extends JViewLegacy{}
}
else
{
	jimport( 'joomla.application.component.view' );
	class CCINVOISViews extends JView{}
}


if(versionCompare()>=3)
{
	$controller	= JControllerLegacy::getInstance('ccinvoices');
}
else
{
	$controller	= JController::getInstance('ccinvoices');
}
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();

?>