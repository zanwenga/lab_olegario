<?php
/**
* Form definition file for core Quotes feature
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

$form_def['form_type'] = 'QU';
$form_def['vendor_agnostic'] = false;
$form_def['check_duplicate_products'] = false;
$form_def['client_mapping_required'] = true;
$form_def['action'] = "quote_request";
$form_def['fe_action'] = "quotes";
$form_def['sub_action'] = "";
$form_def['class'] = $form_def['action'];
$form_def['lang_suffix'] = "_QUOTE";
$form_def['edit_link_prefix'] = nbf_cms::$interop->admin_page_prefix . "&action=quote_request&task=edit&";
$form_def['icon'] = sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, $form_def['action']);
$form_def['list_toolbar'] = "";
$form_def['edit_toolbar'] = "";
$form_def['suppressed'] = array();
$form_def['options_included'] = array();
$form_def['default'] = array();
//Options to suppress (these are options that have been in the core for some time and extensions will know about them)
$form_def['suppressed'][] = 'tab_order';
$form_def['suppressed'][] = 'details_gateway';
$form_def['suppressed'][] = 'details_email_pending_to_client';
$form_def['suppressed'][] = 'details_email_admin_pending';
$form_def['suppressed'][] = 'details_auto_handle_shipping';
$form_def['suppressed'][] = 'advanced_pre_calculate_code';
$form_def['suppressed'][] = 'advanced_post_calculate_code';
$form_def['suppressed'][] = 'advanced_order_creation_code';
$form_def['suppressed'][] = 'advanced_after_processing_code';
$form_def['suppressed'][] = 'editor_related_product';
$form_def['suppressed'][] = 'editor_order_value';
$form_def['suppressed'][] = 'editor_order_value';
//Options to include (these are new items added to core which extensions may not know about)
$form_def['options_included'][] = 'details_offline_payment_redirect';