<?php
/**
* Form definition file for core Orders feature
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

$form_def['form_type'] = 'OR';
$form_def['vendor_agnostic'] = false;
$form_def['check_duplicate_products'] = true;
$form_def['client_mapping_required'] = true;
$form_def['action'] = "orderforms";
$form_def['fe_action'] = "orders";
$form_def['sub_action'] = "";
$form_def['class'] = "forms";
$form_def['lang_suffix'] = "";
$form_def['edit_link_prefix'] = nbf_cms::$interop->admin_page_prefix . "&action=orderforms&task=edit&";
$form_def['icon'] = sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, "forms");
$form_def['list_toolbar'] = "";
$form_def['edit_toolbar'] = "";
$form_def['suppressed'] = array();
$form_def['options_included'] = array();
$form_def['default'] = array();
//Options to suppress (these are options that have been in the core for some time and extensions will know about them)
$form_def['suppressed'][] = 'details_quote_accept_redirect';
//Options to include (these are new items added to core which extensions may not know about)
$form_def['options_included'][] = 'details_offline_payment_redirect';