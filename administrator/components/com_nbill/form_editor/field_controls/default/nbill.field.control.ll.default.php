<?php
/**
* nBill Label Control Class file - for handling output and processing of labels on forms.
* @version 1
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

include_once(realpath(dirname(__FILE__)) . "/../custom/nbill.field.control.base.php");

/**
* Label
*
* @package nBill Framework
* @author Russell Walker
* @version 1.0
* @copyright (C) 2015 Netshine Software Limited
*/
class nbf_field_control_ll_default extends nbf_field_control
{
	/**
	* Labels are treated differently because they do not use a value property to set/unset
	*/
	public function __construct($form_id, $id)
	{
        parent::__construct($form_id, $id);
		$this->html_control_type = 'LL';
	}

	/**
	* Renders the control
	*/
	protected function _render_control($admin = false)
	{
		?><div id="ctl_<?php echo $this->id . $this->suffix; ?>" style="float:left;"><?php echo defined($this->value) ? constant($this->value) : $this->value; ?></div><?php
	}
}