<?php
/**
* Represents a Paypal preapproval invitation.
* @version 1
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nBillPaypalInvitation
{
    /** @var int **/
    public $id;
    /** @var int **/
    public $client_id;
    /** @var string **/
    public $first_name;
    /** @var string **/
    public $last_name;
    /** @var string **/
    public $email_address;
    /** @var string **/
    public $sent_to;
    /** @var string **/
    public $currency;
    /** @var string **/
    public $max_amount;
    /** @var int **/
    public $payment_count;
    /** @var string **/
    public $description;
    /** @var string **/
    public $token;
    /** @var \DateTime **/
    public $date_sent;
}