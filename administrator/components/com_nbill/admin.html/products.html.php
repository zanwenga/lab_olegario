<?php
/**
* HTML output for products
* @version 2
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nBillProducts
{
    protected static $custom_column_count = 0;

	public static function showProducts($rows, $pagination, $vendors, $categories, $attachments = array())
	{
        $vendor_col = false;
        /***** EXCLUDE FROM LITE START *****/
		?>
		<script type="text/javascript">
		function refresh_vendor()
		{
			//Show the appropriate categories depending on selected vendor
			var vendor_id = document.getElementById('vendor_filter').value;
            if (vendor_id == -999)
			{
				<?php
				foreach ($vendors as $vendor)
				{
					echo "document.getElementById('category_filter_" . $vendor->id . "').value = '-999';";
					echo "document.getElementById('category_filter_" . $vendor->id . "').style.display = 'none';";
				}
				?>
				document.getElementById('category_filter_label').style.display = 'none';
			}
			else
			{
				<?php
				foreach ($vendors as $vendor)
				{
					echo "document.getElementById('category_filter_" . $vendor->id . "').style.display = 'none';";
				}
				?>
				document.getElementById('category_filter_' + vendor_id).style.display = 'inline';
				document.getElementById('category_filter_label').style.display = 'inline';
			}
		}
		</script>
        <?php
        /***** EXCLUDE FROM LITE END *****/
        ?>
		<table class="adminheading" style="width:auto;">
		<tr class="nbill-admin-heading">
            <th <?php echo sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, "products"); ?>>
				<?php echo NBILL_BRANDING_NAME . NBILL_BRANDING_TRADEMARK_SYMBOL . ": " . NBILL_PRODUCTS_TITLE; ?>
			</th>
		</tr>
		</table>

		<div class="nbill-message-ie-padding-bug-fixer"></div>
		<?php if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		} ?>

		<form action="<?php echo nbf_cms::$interop->admin_page_prefix; ?>" method="post" name="adminForm" id="adminForm">
        <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
        <input type="hidden" name="action" value="products" />
        <input type="hidden" name="task" value="" />
        <input type="hidden" name="box_checked" value="0" />
        <input type="hidden" name="hidemainmenu" value="0">
        <input type="hidden" name="attachment_id" value="" />

		<p align="left"><?php echo NBILL_PRODUCTS_INTRO ?></p>

		<?php
			//Display filter dropdown if multi-company
			if (count($vendors) > 1)
			{
				echo "<p align=\"left\">" . NBILL_VENDOR_NAME . "&nbsp;";
				$selected_filter = $vendors[0]->id;
				if (nbf_common::nb_strlen(nbf_common::get_param($_POST,'vendor_filter')) > 0)
				{
					$selected_filter = nbf_common::get_param($_POST, 'vendor_filter');
				}
				$vendor_name = array();
				$vendor_name[] = nbf_html::list_option(-999, NBILL_ALL);
				foreach ($vendors as $vendor)
				{
					$vendor_name[] = nbf_html::list_option($vendor->id, $vendor->vendor_name);
				}
				echo nbf_html::select_list($vendor_name, "vendor_filter", 'id="vendor_filter" class="inputbox" onchange="document.adminForm.submit();"', $selected_filter );
			}
			else
			{
				echo "<input type=\"hidden\" name=\"vendor_filter\" id=\"vendor_filter\" value=\"" . $vendors[0]->id . "\" />";
				$_POST['vendor_filter'] = $vendors[0]->id;
			}

			//Display filter dropdown if there are categories
			//Create a dropdown of categories for each vendor - show/hide via javascript depending on vendor selected
			$cat_title_displayed = false;
			foreach ($vendors as $vendor)
			{
				if (count($vendors) < 2)
				{
					echo "<p align=\"left\">";
				}
				$cat_list = array();
				$cat_list[] = nbf_html::list_option(-999, NBILL_ALL);
				foreach ($categories[$vendor->id] as $cat_item)
				{
					$cat_list[] = nbf_html::list_option($cat_item['id'], $cat_item['name']);
				}
				$category_filter = -999;
				if (isset($_POST['category_filter_' . $vendor->id]))
				{
					$category_filter = nbf_common::get_param($_POST, 'category_filter_' . $vendor->id);
				}
				if (strtolower(nbf_version::$suffix) != 'lite' && count($cat_list) > 0) {
					if (!$cat_title_displayed) {
						if (count($vendors) > 0) {
							echo "&nbsp;&nbsp;&nbsp;<span id=\"category_filter_label\">" . NBILL_PRODUCT_CATS . "</span>&nbsp;";
						} else {
							echo "<p align=\"left\"><span id=\"category_filter_label\">" . NBILL_PRODUCT_CATS . "</span>&nbsp;";
						}
						$cat_title_displayed = true;
					}
					echo nbf_html::select_list($cat_list, "category_filter_" . $vendor->id, 'class="inputbox" id="category_filter_' . $vendor->id . '" onchange="document.adminForm.submit();"', $category_filter);
				}
			}

            if ($pagination->record_count > nbf_globals::$record_limit)
            {
                $csv_click = "if (confirm('" . sprintf(NBILL_CSV_EXPORT_LIMIT_WARNING, nbf_globals::$record_limit, nbf_globals::$record_limit, nbf_globals::$record_limit) . "')){document.getElementById('do_csv_download').value=1;document.adminForm.submit();document.getElementById('do_csv_download').value='';}return false;";
            }
            else
            {
                $csv_click = "document.getElementById('do_csv_download').value=1;document.adminForm.submit();document.getElementById('do_csv_download').value='';return false;";
            }
            ?>
            <input type="hidden" name="do_csv_download" id="do_csv_download" value="" />
            &nbsp;
            <span style="white-space:nowrap;"><a href="#" title="<?php echo NBILL_CSV_DOWNLOAD_LIST_DESC; ?>" onclick="<?php echo $csv_click; ?>"><img border="0" src="<?php echo nbf_cms::$interop->nbill_site_url_path ?>/images/icons/medium/csv.gif" alt="<?php echo NBILL_CSV_DOWNLOAD_LIST_DESC; ?>" style="vertical-align:middle" /></a>
            <strong><a href="#" title="<?php echo NBILL_CSV_DOWNLOAD_LIST_DESC; ?>" onclick="<?php echo $csv_click; ?>"><?php echo NBILL_CSV_DOWNLOAD; ?></a></strong></span>
            <?php

			if (count($vendors) > 0 || $cat_title_displayed)
			{
				echo "</p>";
			}
		?>

        <div class="rounded-table">
            <table class="adminlist">
            <tr class="nbill-admin-title-row">
                <th class="selector">
			    #
			    </th>
                <th class="selector">
                    <input type="checkbox" name="check_all" value="" onclick="for(var i=0; i<<?php echo count($rows); ?>;i++) {document.getElementById('cb' + i).checked=this.checked;} document.adminForm.box_checked.value=this.checked;" />
			    </th>
                <?php self::renderCustomColumn('id'); ?>
			    <th class="title">
				    <?php echo NBILL_PRODUCT_NAME; ?>
			    </th>
                <?php self::renderCustomColumn('name'); ?>
			    <th class="title">
				    <?php echo NBILL_PRODUCT_SKU; ?>
			    </th>
                <?php self::renderCustomColumn('sku'); ?>
                <?php if (strtolower(nbf_version::$suffix) != 'lite') { ?>
			    <th class="title">
				    <?php echo NBILL_PRODUCT_CATEGORY; ?>
			    </th>
                <?php self::renderCustomColumn('category'); ?>
			    <?php
                }
				    //Only show vendor name if more than one listed
				    if (count($vendors) > 1 && $selected_filter == -999)
				    {?>
					    <th class="title">
						    <?php echo NBILL_VENDOR_NAME; ?>
					    </th>

				    <?php }
			    ?>
                <?php self::renderCustomColumn('vendor'); ?>
		    </tr>
		    <?php
			    for ($i=0, $n=count( $rows ); $i < $n; $i++)
			    {
				    $row = &$rows[$i];
				    $link = nbf_cms::$interop->admin_page_prefix . "&action=products&task=edit&cid=$row->id";
				    echo "<tr>";
				    echo "<td class=\"selector\">";
				    echo $pagination->list_offset + $i + 1;
				    $checked = nbf_html::id_checkbox($i, $row->id);
				    echo "</td><td class=\"selector\">$checked</td>";
                    self::renderCustomColumn('id', $row);
				    echo "<td class=\"list-value\"><a href=\"$link\" title=\"" . NBILL_EDIT_PRODUCT . "\">" . $row->name . "</a>";
                    /***** EXCLUDE FROM LITE START *****/
                    if (file_exists(nbf_cms::$interop->nbill_admin_base_path . '/admin.proc/supporting_docs.php')) {
                    ?>
                    <div style="float:right"><a href="#" onclick="<?php if ($row->attachment_count){ ?>var att=document.getElementById('attachments_<?php echo $row->id; ?>');if(att.style.display=='none'){att.style.display='';}else{att.style.display='none';}<?php }else{ ?>window.open('<?php echo nbf_cms::$interop->admin_popup_page_prefix; ?>&hide_billing_menu=1&action=supporting_docs&use_stylesheet=1&show_toolbar=1&attach_to_type=PR&attach_to_id=<?php echo $row->id; ?>','','scrollbars=1,width=790,height=500');<?php } ?>return false;"><img border="0" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/icons/supporting_docs.gif" alt="<?php echo NBILL_ATTACHMENTS; ?>" style="vertical-align:middle;" /><?php if ($row->attachment_count) {echo " (" . $row->attachment_count . ")";} ?></a></div>
                    <div id="attachments_<?php echo $row->id; ?>" style="display:none;text-align:right;clear:both;">
                        <table cellpadding="3" cellspacing="0" border="0" style="margin-left:auto;margin-right:0px;">
                        <?php
                        foreach ($attachments as $attachment)
                        {
                            if ($attachment->associated_doc_id == $row->id)
                            {
                                ?>
                                <tr>
                                <td>
                                    <a href="<?php echo nbf_cms::$interop->admin_page_prefix; ?>&action=supporting_docs&task=download&file=<?php echo base64_encode($attachment->id); ?>"><img style="vertical-align:middle" border="0" alt="" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/file.png" />&nbsp;<?php echo $attachment->file_name; ?></a>
                                </td>
                                <td>
                                    <input type="button" class="button btn" value="<?php echo NBILL_DETACH; ?>" onclick="if(confirm('<?php echo NBILL_DETACH_SURE; ?>')){document.adminForm.attachment_id.value='<?php echo $attachment->id; ?>';document.adminForm.task.value='detach_file';document.adminForm.submit();}" />
                                </td>
                                <td>
                                    <input type="button" class="button btn" value="<?php echo NBILL_DELETE; ?>" onclick="if(confirm('<?php echo sprintf(NBILL_DELETE_FILE_SURE, $attachment->file_name); ?>')){document.adminForm.attachment_id.value='<?php echo $attachment->id; ?>';document.adminForm.task.value='delete_file';document.adminForm.submit();}" />
                                </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        <tr><td colspan="3">
                        <a href="#" onclick="window.open('<?php echo nbf_cms::$interop->admin_popup_page_prefix; ?>&hide_billing_menu=1&action=supporting_docs&use_stylesheet=1&show_toolbar=1&attach_to_type=PR&attach_to_id=<?php echo $row->id; ?>','','scrollbars=1,width=790,height=500');return false;"><img style="vertical-align:middle" border="0" alt="" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/icons/supporting_docs.gif" />&nbsp;<?php echo NBILL_NEW_ATTACHMENT; ?></a>
                        </td></tr>
                        </table>
                    </div>
                    <?php
                    }
                    /***** EXCLUDE FROM LITE END *****/
                    echo "</td>";
                    self::renderCustomColumn('name', $row);
				    echo "<td class=\"list-value\">" . $row->product_code . "</td>";
                    self::renderCustomColumn('sku', $row);

                    if (strtolower(nbf_version::$suffix) != 'lite') {
				        //Only show category name if more than one listed
				        $cat_col = false;
				        foreach ($categories[$row->vendor_id] as $category)
				        {
					        if ($category['id'] == $row->category)
					        {
						        echo "<td class=\"list-value\">" . $row->category_name . "</td>";
                                $cat_col = true;
						        break;
					        }
				        }
				        if (!$cat_col)
				        {
					        echo "<td>&nbsp;</td>";
				        }
                        self::renderCustomColumn('category', $row);
                    }

				    //Only show vendor name if more than one listed
				    $vendor_col = false;
				    if (count($vendors) > 1 && $selected_filter == -999)
				    {
					    foreach ($vendors as $vendor)
					    {
						    if ($vendor->id == $row->vendor_id)
						    {
							    echo "<td class=\"list-value\">" . $vendor->vendor_name . "</td>";

							    $vendor_col = true;
							    break;
						    }
					    }
				    }
                    self::renderCustomColumn('vendor', $row);
				    echo "</tr>";
			    }
		    ?>
		    <tr class="nbill_tr_no_highlight"><td colspan="<?php echo ($vendor_col ? 6 : 5) + self::$custom_column_count; ?>" class="nbill-page-nav-footer"><?php echo $pagination->render_page_footer(); ?></td></tr>
		    </table>
        </div>

		</form>
        <?php
        /***** EXCLUDE FROM LITE START *****/
        ?>
		<script type="text/javascript">
		refresh_vendor();
		</script>
        <?php
        /***** EXCLUDE FROM LITE END *****/
	}

    protected static function renderCustomColumn($column_name, $row = 'undefined')
    {
        $method = ($row == 'undefined') ? 'render_header' : 'render_row';
        if (file_exists(dirname(__FILE__) . "/custom_columns/products/after_$column_name.php")) {
            include_once(dirname(__FILE__) . "/custom_columns/products/after_$column_name.php");
            if (is_callable(array("nbill_admin_products_after_$column_name", $method))) {
                call_user_func(array("nbill_admin_products_after_$column_name", $method), $row);
                if ($method == 'render_header') {
                    self::$custom_column_count++;
                }
            }
        }
    }

    public static function downloadProductsCSV($vendors, $rows, $product_prices, $max_currencies_per_product, $currencies)
    {
        $selected_filter = $vendors[0]->id;
        if (nbf_common::nb_strlen(nbf_common::get_param($_POST,'vendor_filter')) > 0)
        {
            $selected_filter = nbf_common::get_param($_POST, 'vendor_filter');
        }

        if (count($vendors) > 1 && $selected_filter == -999)
        {
            echo NBILL_VENDOR_NAME . ",";
        }
        echo NBILL_ID . ",";
        echo NBILL_PRODUCT_CATEGORY . ",";
        echo NBILL_PRODUCT_SKU . ",";
        echo NBILL_NOMINAL_LEDGER_CODE . ",";
        echo NBILL_PRODUCT_NAME . ",";
        echo NBILL_PRODUCT_DESCRIPTION . ",";
        echo NBILL_IS_FREEBIE . ",";
        echo NBILL_IS_TAXABLE . ",";
        echo NBILL_REQUIRES_SHIPPING . ",";
        echo NBILL_SHIPPING_SERVICES . ",";
        echo NBILL_SHIPPING_UNITS . ",";
        echo NBILL_AUTO_FULFIL . ",";
        echo NBILL_IS_DOWNLOADABLE . ",";
        echo NBILL_DOWNLOAD_LOCATION . ",";
        echo NBILL_NO_OF_DAYS_AVAILABLE . ",";
        echo NBILL_DOWNLOAD_LINK_TEXT . ",";
        echo NBILL_DOWNLOAD_LOCATION_2 . ",";
        echo NBILL_DOWNLOAD_LINK_TEXT_2 . ",";
        echo NBILL_DOWNLOAD_LOCATION_3 . ",";
        echo NBILL_DOWNLOAD_LINK_TEXT_3 . ",";
        echo NBILL_EMAIL_DOWNLOADS . ",";
        echo NBILL_EMAIL_DOWNLOADS_MESSAGE . ",";
        echo NBILL_IS_USER_SUB . ",";
        echo NBILL_SUB_USER_GROUP . ",";
        if (nbf_cms::$interop->multi_user_group)
        {
            echo NBILL_MULTI_GROUP . ",";
        }
        echo NBILL_EXPIRY_LEVEL . ",";
        echo NBILL_EXPIRY_REDIRECT . ",";
        echo NBILL_ALLOW_GLOBAL_DISCOUNTS . ",";
        for ($i = 0; $i < $max_currencies_per_product; $i++)
        {
            echo NBILL_NET_PRICE_SETUP_FEE . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_ONE_OFF . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_WEEKLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_FOUR_WEEKLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_MONTHLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_QUARTERLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_SEMI_ANNUALLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_ANNUALLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_BIANNUALLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_FIVE_YEARLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
            echo NBILL_NET_PRICE_TEN_YEARLY . sprintf(NBILL_CSV_ITEM_CURRENCY, $currencies[$i]) . ",";
        }
        echo "\r\n";

        foreach ($rows as $row)
        {
            if (count($vendors) > 1 && $selected_filter == -999)
            {
                foreach ($vendors as $vendor)
                {
                    if ($vendor->id == $row->vendor_id)
                    {
                        echo "\"" . str_replace("\"", "\"\"", $vendor->vendor_name) . "\",";
                        break;
                    }
                }
            }
            echo $row->id . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->category_name) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->product_code) . "\",";
            echo $row->nominal_ledger_code . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->name) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->description) . "\",";
            echo ($row->is_freebie ? NBILL_YES : NBILL_NO) . ",";
            echo ($row->is_taxable ? NBILL_YES : NBILL_NO) . ",";
            echo ($row->requires_shipping ? NBILL_YES : NBILL_NO) . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->shipping_services) . "\",";
            echo format_number($row->shipping_units, 'editable_quantity') . ",";
            echo ($row->auto_fulfil_orders ? NBILL_YES : NBILL_NO) . ",";
            echo ($row->is_downloadable ? NBILL_YES : NBILL_NO) . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_location) . "\",";
            echo $row->no_of_days_available . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_link_text) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_location_2) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_link_text_2) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_location_3) . "\",";
            echo "\"" . str_replace("\"", "\"\"", $row->download_link_text_3) . "\",";
            echo ($row->email_downloads ? NBILL_YES : NBILL_NO) . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->email_downloads_message) . "\",";
            echo ($row->is_sub ? NBILL_YES : NBILL_NO) . ",";
            echo $row->user_group . ",";
            if (nbf_cms::$interop->multi_user_group)
            {
                echo ($row->multi_group ? NBILL_YES : NBILL_NO) . ",";
            }
            echo $row->expiry_level . ",";
            echo "\"" . str_replace("\"", "\"\"", $row->expiry_redirect) . "\",";
            echo ($row->allow_global_discounts ? NBILL_YES : NBILL_NO);
            $product_price_count = 0;
            for ($currency_index = 0; $currency_index < count($currencies); $currency_index++)
            {
                foreach ($product_prices as $product_price)
                {
                    if ($product_price->product_id == $row->id)
                    {
                        $product_price_count++;
                        if ($product_price->currency_code == $currencies[$currency_index])
                        {
                            echo "," . format_number($product_price->net_price_setup_fee, 'editable_currency');
                            echo "," . format_number($product_price->net_price_one_off, 'editable_currency');
                            echo "," . format_number($product_price->net_price_weekly, 'editable_currency');
                            echo "," . format_number($product_price->net_price_four_weekly, 'editable_currency');
                            echo "," . format_number($product_price->net_price_monthly, 'editable_currency');
                            echo "," . format_number($product_price->net_price_quarterly, 'editable_currency');
                            echo "," . format_number($product_price->net_price_semi_annually, 'editable_currency');
                            echo "," . format_number($product_price->net_price_annually, 'editable_currency');
                            echo "," . format_number($product_price->net_price_biannually, 'editable_currency');
                            echo "," . format_number($product_price->net_price_five_years, 'editable_currency');
                            echo "," . format_number($product_price->net_price_ten_years, 'editable_currency');
                        }
                    }
                }
            }
            for ($i = $product_price_count; $i < $max_currencies_per_product; $i++)
            {
                echo ",,,,,,,,,,,";
            }
            echo "\r\n";
        }
    }

	/**
	* Edit a product (or create a new one)
	*/
	public static function editProduct($product_id, $row, $vendors, $categories, $ledger, $shipping, $prices, $user_groups, $selected_cats = array(), $discounts = array(), $product_discounts = array(), $existing_orders = array(), $price_match_found = false, $user_sub_plugin_present = false, $use_posted_values, $attachments = array())
	{
        nbf_cms::$interop->init_editor();
		$currencies = nbf_xref::get_currencies();
		?>
		<script type="text/javascript" language="javascript">
        var update_existing = false;
		<?php nbf_html::add_js_validation_numeric(); ?>
		function nbill_submit_task(task_name)
		{
			var form = document.adminForm;
			if (task_name == 'cancel')
			{
				presubmit();
				form.task.value=task_name;
                form.submit();
				return;
			}

			//Validate fields
			if (form.name.value == "")
			{
				alert('<?php echo NBILL_PRODUCT_NAME_REQUIRED; ?>');
			}
			<?php
			foreach ($vendors as $vendor)
			{
				foreach ($currencies as $currency)
				{
					if (nbf_common::nb_strlen($currency['code']) > 0)
					{
						$suffix = $currency['code'] . "_" . $vendor->id; ?>
						else if (!IsNumeric(document.getElementById('net_price_one_off_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_ONE_OFF); ?>');
						}
                        /***** EXCLUDE FROM LITE START *****/
						else if (!IsNumeric(document.getElementById('net_price_weekly_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_WEEKLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_four_weekly_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_FOUR_WEEKLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_monthly_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_MONTHLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_quarterly_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_QUARTERLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_semi_annually_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_SEMI_ANNUALLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_annually_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_ANNUALLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_biannually_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_BIANNUALLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_five_years_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_FIVE_YEARLY); ?>');
						}
						else if (!IsNumeric(document.getElementById('net_price_ten_years_<?php echo $suffix; ?>').value, true))
						{
							alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NET_PRICE_TEN_YEARLY); ?>');
						}
                        /***** EXCLUDE FROM LITE END *****/
						<?php
					}
				}
			}
			?>
            /***** EXCLUDE FROM LITE START *****/
			else if (!IsNumeric(form.shipping_units.value, true))
			{
				alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_SHIPPING_UNITS); ?>');
			}
			else if (!IsNumeric(form.no_of_days_available.value, true))
			{
				alert('<?php echo sprintf(NBILL_NUMERIC_ONLY, NBILL_NO_OF_DAYS_AVAILABLE); ?>');
			}
            else if (form.is_downloadable.checked)
			{
				if (form.download_location.value == "" || form.download_link_text.value == "")
				{
					alert('<?php echo NBILL_DOWNLOAD_INFO_REQUIRED; ?>');
				}
                else
				{
                    <?php if (!$user_sub_plugin_present) { ?>
                    if (form.is_sub[1].checked)
					{
						alert('<?php echo NBILL_ENSURE_MAMBOT_PUBLISHED; ?>');
					}
                    <?php } ?>
					presubmit();
                    document.adminForm.task.value=task_name;
                    document.adminForm.submit();
				}
			}
            /***** EXCLUDE FROM LITE END *****/
            else
			{
                <?php
                /***** EXCLUDE FROM LITE START *****/
                if (!$user_sub_plugin_present) { ?>
				if (form.is_sub[1].checked)
				{
					alert('<?php echo NBILL_ENSURE_MAMBOT_PUBLISHED; ?>');
				}
                <?php }
                /***** EXCLUDE FROM LITE END *****/
                ?>
				presubmit();
				document.adminForm.task.value=task_name;
                document.adminForm.submit();
			}
		}

		function presubmit()
		{
            /***** EXCLUDE FROM LITE START *****/
			//Add any newly added discounts to the hidden field so we can pick them up on postback
			<?php foreach ($vendors as $vendor)
			{?>
				var discountCount_<?php echo $vendor->id; ?> = document.getElementById('discount_<?php echo $vendor->id; ?>_count').value;
				var discounts_<?php echo $vendor->id;?>;
				discounts_<?php echo $vendor->id; ?> = '';
				for (var i=0; i< discountCount_<?php echo $vendor->id; ?>; i++)
				{
					if (i > 0)
					{
						discounts_<?php echo $vendor->id; ?> += '|';  //new discount delimiter
					}
					discounts_<?php echo $vendor->id; ?> += document.getElementById('discount_<?php echo $vendor->id; ?>_' + i + '_id').value;
					discounts_<?php echo $vendor->id; ?> += '#' + document.getElementById('discount_<?php echo $vendor->id; ?>_' + i + '_priority').value;
					discounts_<?php echo $vendor->id; ?> += '#' + document.getElementById('discount_<?php echo $vendor->id; ?>_' + i + '_quantity').value;
                    discounts_<?php echo $vendor->id; ?> += '#' + getRadioButtonValue('discount_<?php echo $vendor->id; ?>_' + i + '_multiply');
                    discounts_<?php echo $vendor->id; ?> += '#' + document.getElementById('discount_<?php echo $vendor->id; ?>_' + i + '_offset').value;
				}
				document.getElementById('discount_added_items_<?php echo $vendor->id; ?>').value = discounts_<?php echo $vendor->id; ?>;
			<?php
			}
			?>

            //Check whether to update prices on existing orders
            if (update_existing)
            {
                update_existing = confirm('<?php echo NBILL_PRODUCT_UPDATE_EXISTING_ORDERS; ?>');
                document.getElementById('update_existing').value = update_existing;
            }
            /***** EXCLUDE FROM LITE END *****/
		}

        function check_existing_orders(vendor_id, pay_freq, currency, orig_amount, new_amount)
        {
            /***** EXCLUDE FROM LITE START *****/
            if (orig_amount != new_amount)
            {
                <?php
                foreach ($existing_orders as $existing_order)
                {?>
                    if (vendor_id == '<?php echo $existing_order->vendor_id; ?>' &&
                        pay_freq == '<?php echo $existing_order->payment_frequency; ?>' &&
                        currency == '<?php echo $existing_order->currency; ?>' &&
                        orig_amount == '<?php echo format_number($existing_order->net_price, 'editable_currency'); ?>')
                    {
                        update_existing = true;
                    }
                <?php } ?>
            }
            /***** EXCLUDE FROM LITE END *****/
        }

		function refresh_vendor()
		{
			//Show the appropriate nominal ledger codes depending on selected vendor
			var vendor_id = document.getElementById('vendor_id').value;
			<?php
			foreach ($vendors as $vendor)
			{
                /***** EXCLUDE FROM LITE START *****/
				echo "document.getElementById('category_" . $vendor->id . "').style.display = 'none';\n";
				echo "document.getElementById('ledger_" . $vendor->id . "').style.display = 'none';\n";
                echo "document.getElementById('shipping_" . $vendor->id . "').style.display = 'none';\n";
                echo "document.getElementById('product_discounts_" . $vendor->id . "').style.display = 'none';\n";
                /***** EXCLUDE FROM LITE END *****/
                echo "document.getElementById('prices_" . $vendor->id . "').style.display = 'none';\n";

			}
			?>
            /***** EXCLUDE FROM LITE START *****/
			document.getElementById('category_' + vendor_id).style.display = 'inline';
			document.getElementById('ledger_' + vendor_id).style.display = 'inline';
			document.getElementById('shipping_' + vendor_id).style.display = 'inline';
            /***** EXCLUDE FROM LITE END *****/
			document.getElementById('prices_' + vendor_id).style.display = 'block';
            switch (vendor_id)
			{
				<?php
                /***** EXCLUDE FROM LITE START *****/
				foreach ($vendors as $vendor)
				{
					echo "case '" . $vendor->id . "':\n";
					if (count(@$discounts[$vendor->id]) > 0)
					{
						echo "document.getElementById('product_discounts_" . $vendor->id . "').style.display = '';\n";
						echo "break;\n";
					}
				}
                /***** EXCLUDE FROM LITE END *****/
				?>
			}
		}

        /***** EXCLUDE FROM LITE START *****/
		function user_sub_changed()
		{
			if (document.adminForm.is_sub[1].checked)
			{
				document.getElementById('usersub').style.display = '';
			}
			else
			{
				document.getElementById('usersub').style.display = 'none';
			}
		}

		function add_product_discount()
		{
			//Get current vendor
			var vendor_id = document.getElementById('vendor_id').value;

			//Check that the values are valid
			if (document.getElementById('discount_' + vendor_id + '_new_id').value == 0)
			{
				alert('<?php echo NBILL_PLEASE_SELECT_PRODUCT_DISCOUNT; ?>');
				return;
			}

			//Check if this discount is already present (don't allow it twice)
			var discountTable = document.getElementById('discount_' + vendor_id + '_new_row').parentNode;
			for ( i=0; i < discountTable.childNodes.length; i++)
			{
				var elemId = discountTable.childNodes[i].id;
				if (elemId != null && elemId.length > 0 && elemId != 'discount_' + vendor_id + '_new_row')
				{
					if (elemId.indexOf('_') > 0)
					{
						var idParts = elemId.split('_');
						if (idParts.length > 2)
						{
							if (document.getElementById('discount_' + vendor_id + '_' + idParts[2] + '_row').style.display != 'none')
							{
								existingElem = document.getElementById('discount_' + vendor_id + '_' + idParts[2] + '_id');
								if (existingElem != null)
								{
									if (existingElem.value == document.getElementById('discount_' + vendor_id + '_new_id').options[document.getElementById('discount_' + vendor_id + '_new_id').selectedIndex].value)
									{
										alert('<?php echo NBILL_PRODUCT_DISCOUNT_DUPLICATION; ?>');
										return;
									}
								}
							}
						}
					}
				}
			}

			//Set default values if not set
			if (document.getElementById('discount_' + vendor_id + '_new_priority').value.length == 0)
			{
				document.getElementById('discount_' + vendor_id + '_new_priority').value = 1;
			}
			if (document.getElementById('discount_' + vendor_id + '_new_quantity').value.length == 0)
			{
				document.getElementById('discount_' + vendor_id + '_new_quantity').value = 1;
			}
            if (getRadioButtonValue('discount_' + vendor_id + '_new_multiply').length == 0)
            {
                document.getElementById('discount_' + vendor_id + '_new_multiply1').checked = true;
            }
            if (document.getElementById('discount_' + vendor_id + '_new_offset').value.length == 0)
            {
                document.getElementById('discount_' + vendor_id + '_new_offset').value = 0;
            }

			//Store the values from the 'new' line
			var new_id = document.getElementById('discount_' + vendor_id + '_new_id').selectedIndex;
			var new_discount = document.getElementById('discount_' + vendor_id + '_new_id').options[new_id].text;
			var new_discount_value = document.getElementById('discount_' + vendor_id + '_new_id').options[new_id].value;
			var new_priority = document.getElementById('discount_' + vendor_id + '_new_priority').value;
			var new_quantity = document.getElementById('discount_' + vendor_id + '_new_quantity').value;
            var new_multiply = getRadioButtonValue('discount_' + vendor_id + '_new_multiply');
            var new_offset = document.getElementById('discount_' + vendor_id + '_new_offset').value;

			//Reset 'new' line ready for next input
			document.getElementById('discount_' + vendor_id + '_new_id').selectedIndex = 0;
            document.getElementById('discount_' + vendor_id + '_new_priority').value = '1';
            document.getElementById('discount_' + vendor_id + '_new_quantity').value = '1';
            document.getElementById('discount_' + vendor_id + '_new_multiply1').checked = true;
            document.getElementById('discount_' + vendor_id + '_new_offset').value = '0';

			//Work out what number we are on
			var discountNo = (document.getElementById('discount_' + vendor_id + '_count').value * 1);
			document.getElementById('discount_' + vendor_id + '_count').value = (discountNo * 1) + 1;

			//Create a new row
			var newElement = document.createElement('tr');
			newElement.id = 'discount_' + vendor_id + '_' + discountNo + '_row';

			//First cell - discount id
			var firstCell = document.createElement('td');
			var discountId = document.createElement('input');
			discountId.setAttribute('type', 'hidden');
			discountId.setAttribute('value', new_discount_value);
			discountId.id = 'discount_' + vendor_id + '_' + discountNo + '_id';
			firstCell.appendChild(discountId);
            var discountText = document.createTextNode(new_discount);
            firstCell.appendChild(discountText);

            //Second cell - priority
            var secondCell = document.createElement('td');
            var priority = document.createElement('input');
            priority.style['width'] = '80px';
            priority.setAttribute('type', 'text');
            priority.id = 'discount_' + vendor_id + '_' + discountNo + '_priority';
            priority.value = new_priority;
            secondCell.appendChild(priority);

            //Third cell - quantity
            var thirdCell = document.createElement('td');
            var quantity = document.createElement('input');
            quantity.setAttribute('type', 'text');
            quantity.style['width'] = '80px';
            quantity.id = 'discount_' + vendor_id + '_' + discountNo + '_quantity';
            quantity.value = new_quantity;
            thirdCell.appendChild(quantity);

            //Fourth cell - multiply
            var fourthCell = document.createElement('td');
            var multiply = document.createElement('input');
            multiply.setAttribute('type', 'radio');
            multiply.setAttribute('name', 'discount_' + vendor_id + '_' + discountNo + '_multiply');
            multiply.id = 'discount_' + vendor_id + '_' + discountNo + '_multiply0';
            multiply.value = 0;
            if (new_multiply == 0)
            {
                multiply.checked = true;
            }
            var multiply_label = document.createElement('label');
            multiply_label.setAttribute('for', 'discount_' + vendor_id + '_' + discountNo + '_multiply0');
            multiply_label.innerHTML = '&nbsp;<?php echo NBILL_NO; ?>&nbsp;';
            var multiply2 = document.createElement('input');
            multiply2.setAttribute('type', 'radio');
            multiply2.setAttribute('name', 'discount_' + vendor_id + '_' + discountNo + '_multiply');
            multiply2.id = 'discount_' + vendor_id + '_' + discountNo + '_multiply1';
            multiply2.value = 1;
            if (new_multiply == 1)
            {
                multiply2.checked = true;
            }
            var multiply2_label = document.createElement('label');
            multiply2_label.setAttribute('for', 'discount_' + vendor_id + '_' + discountNo + '_multiply1');
            multiply2_label.innerHTML = '&nbsp;<?php echo NBILL_YES; ?>&nbsp;';

            fourthCell.appendChild(multiply);
            fourthCell.appendChild(multiply_label);
            fourthCell.appendChild(multiply2);
            fourthCell.appendChild(multiply2_label);

            //Fifth cell - quantity
            var fifthCell = document.createElement('td');
            var offset = document.createElement('input');
            offset.setAttribute('type', 'text');
            offset.style['width'] = '80px';
            offset.id = 'discount_' + vendor_id + '_' + discountNo + '_offset';
            offset.value = new_offset;
            fifthCell.appendChild(offset);

            //delete button
            var deleteButton = document.createElement('input');
            deleteButton.setAttribute('type', 'button');
            deleteButton.setAttribute('name', 'discount_' + vendor_id + '_' + discountNo + '_delete');
            deleteButton.id = 'discount_' + vendor_id + '_' + discountNo + '_delete';
            deleteButton.setAttribute('value', '<?php echo NBILL_DELETE_PRODUCT_DISCOUNT; ?>');

            addEvent( deleteButton, 'click', function(){ delete_discount(discountNo); } );

            fifthCell.appendChild(deleteButton);

            newElement.appendChild(firstCell);
            newElement.appendChild(secondCell);
            newElement.appendChild(thirdCell);
            newElement.appendChild(fourthCell);
            newElement.appendChild(fifthCell);

            //Work out where to put it
            var newRow = document.getElementById('discount_' + vendor_id + '_new_row');
            newRow.parentNode.insertBefore(newElement, newRow);
		}
        /***** EXCLUDE FROM LITE END *****/

		//This function only: modified version of code released under LESSER GPL
		//CC-GNU LGPL (see http://cn.creativecommons.org/licenses/LGPL/2.1/index.html)
		function addEvent(obj, type, fn)
		{
			if (obj.attachEvent)
			{
				obj['e'+type+fn] = fn;
				obj[type+fn] = function(){obj['e'+type+fn](window.event);}
				obj.attachEvent('on'+type, obj[type+fn]);
			}
			else
			{
				obj.addEventListener( type, fn, false );
			}
		}

        function getRadioButtonValue(GroupName)
        {
            var retVal = null;

            if (GroupName == null)
            {
                    return null;
            }
            var radio_inputs = document.getElementsByTagName('input');
            for (var i=0; i<radio_inputs.length; i++)
            {
                var input = radio_inputs[i];
                if (input.type == 'radio' && input.name == GroupName && input.checked)
                {
                    retVal = input.value;
                    break;
                }
            }
            return retVal;
        }

        /***** EXCLUDE FROM LITE START *****/
		function delete_discount(discountNo)
		{
			//Get current vendor
			var vendor_id = document.getElementById('vendor_id').value;

			delList = document.getElementById('discount_deleted_items_' + vendor_id);
			if (delList.value.length > 0)
			{
				delList.value += ",";
			}
			delList.value += discountNo;
			document.getElementById('discount_' + vendor_id + '_' + discountNo + '_row').style.display = 'none';
		}
        /***** EXCLUDE FROM LITE END *****/
		</script>

		<table class="adminheading" style="width:auto;">
		<tr class="nbill-admin-heading">
            <th <?php echo sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, "products"); ?>>
				<?php echo NBILL_BRANDING_NAME . NBILL_BRANDING_TRADEMARK_SYMBOL; ?>:
				<?php echo $row->id ? NBILL_EDIT_PRODUCT . " '" . $row->name . "'" : NBILL_NEW_PRODUCT; ?>
			</th>
		</tr>
		</table>

		<div class="nbill-message-ie-padding-bug-fixer"></div>
		<?php
		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		} ?>

		<form action="<?php echo nbf_cms::$interop->admin_page_prefix; ?>" method="post" name="adminForm" id="adminForm">
        <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
        <input type="hidden" name="action" value="products" />
        <input type="hidden" name="task" value="edit" />
        <input type="hidden" name="box_checked" value="0" />
        <input type="hidden" name="hidemainmenu" value="0">
		<input type="hidden" name="id" value="<?php echo $product_id;?>" />
        <input type="hidden" name="update_existing" id="update_existing" value="" />
		<?php nbf_html::add_filters(); ?>

        <?php
        $tab_settings = new nbf_tab_group();
        $tab_settings->start_tab_group("admin_settings");
        $tab_settings->add_tab_title("basic", NBILL_ADMIN_TAB_BASIC);
        if (strtolower(nbf_version::$suffix) != 'lite') {
            $tab_settings->add_tab_title("advanced", NBILL_ADMIN_TAB_ADVANCED);
        }
        ob_start();
        ?>

        <div class="rounded-table">
		    <table width="100%" border="0" cellspacing="0" cellpadding="3" class="adminform" id="nbill-admin-table-products">
		    <tr id="nbill-admin-tr-product-details">
			    <th colspan="2"><?php echo NBILL_PRODUCT_DETAILS; ?>
			    <?php
				    if (count($vendors) < 2)
				    {
					    echo "<input type=\"hidden\" name=\"vendor_id\" id=\"vendor_id\" value=\"" . $vendors[0]->id . "\" />";
					    $selected_vendor = $vendors[0]->id;
					    $_POST['vendor_id'] = $vendors[0]->id;
				    }
			    ?>
			    </th>
		    </tr>
		    <?php
			    if (count($vendors) > 1)
			    {?>
				    <tr id="nbill-admin-tr-vendor-name">
					    <td class="nbill-setting-caption">
						    <?php echo NBILL_VENDOR_NAME; ?>
					    </td>
					    <td class="nbill-setting-value">
						    <?php
							    $vendor_name = array();
							    foreach ($vendors as $vendor)
							    {
								    $vendor_name[] = nbf_html::list_option($vendor->id, $vendor->vendor_name);
							    }
							    if($row->id)
							    {
								    $selected_vendor = $row->vendor_id;
							    }
							    else
							    {
								    $selected_vendor = nbf_common::get_param($_POST, 'vendor_filter');
							    }
                                if ($selected_vendor < 1)
                                {
                                    $selected_vendor = @$vendors[0]->id;
                                }
							    echo nbf_html::select_list($vendor_name, "vendor_id", 'class="inputbox" id="vendor_id" onchange="refresh_vendor();"', $use_posted_values ? nbf_common::get_param($_POST, 'vendor_id', '', true) : $selected_vendor);
						    ?>
                            <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_ID, "vendor_id_help"); ?>
					    </td>
				    </tr>
			    <?php }
            /***** EXCLUDE FROM LITE START *****/
            ?>
		    <tr id="nbill-admin-tr-category">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_PRODUCT_CATEGORY; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php
					    //Create a dropdown of categories for each vendor - show/hide via javascript depending on vendor selected
					    foreach ($vendors as $vendor)
					    {
						    $cat_list = array();
						    foreach ($categories[$vendor->id] as $cat_item)
						    {
							    $cat_list[] = nbf_html::list_option($cat_item['id'], $cat_item['name']);
						    }
						    if($row->id)
						    {
							    $selected_cat = $row->category;
						    }
						    else
						    {
							    $selected_cat = $selected_cats[$vendor->id];
						    }
						    echo nbf_html::select_list($cat_list, "category_" . $vendor->id, 'class="inputbox" id="category_' . $vendor->id . '"', $use_posted_values ? nbf_common::get_param($_POST, 'category_' . $vendor->id, '', true) : $selected_cat);
					    }
				    ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_CATEGORY, "category_help"); ?>
			    </td>
		    </tr>

		    <tr id="nbill-admin-tr-ledger-code">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_NOMINAL_LEDGER_CODE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php
					    //Create a dropdown of ledger codes for each vendor - show/hide via javascript depending on vendor selected
					    foreach ($vendors as $vendor)
					    {
						    $ledger_list = array();
						    $ledger_list[] = nbf_html::list_option("-1", "-1 - " . NBILL_MISCELLANEOUS);
						    foreach ($ledger[$vendor->id] as $ledger_item)
						    {
							    if ($ledger_item->vendor_id == $vendor->id)
							    {
								    if ($ledger_item->code != -1 && $ledger_item->description != NBILL_MISCELLANEOUS)
								    {
									    $ledger_list[] = nbf_html::list_option($ledger_item->code, $ledger_item->code . " - " . $ledger_item->description);
								    }
							    }
						    }
						    if($row->id)
						    {
							    $selected_ledger = $row->nominal_ledger_code;
						    }
						    else
						    {
							    $selected_ledger = '';
						    }
						    echo nbf_html::select_list($ledger_list, "ledger_" . $vendor->id, 'class="inputbox" id="ledger_' . $vendor->id . '"', $use_posted_values ? nbf_common::get_param($_POST, 'ledger_' . $vendor->id, '', true) : $selected_ledger);
					    }
				    ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_NOMINAL_LEDGER_CODE, "ledger_help"); ?>
			    </td>
		    </tr>
            <?php /***** EXCLUDE FROM LITE END *****/ ?>

		    <tr id="nbill-admin-tr-sku">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_PRODUCT_SKU; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="product_code" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'product_code', '', true) : $row->product_code); ?>" class="inputbox" style="width:160px" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_SKU, "product_code_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-product-name">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_PRODUCT_NAME; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="name" id="name" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'name', '', true) : $row->name); ?>" class="inputbox" style="width:160px" />
				    <input type="hidden" name="old_product_name" id="old_product_name" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'name', '', true) : $row->name); ?>" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_NAME, "name_help"); ?>
			    </td>
		    </tr>
            <tr id="nbill-admin-tr-product-desc">
                <td class="nbill-setting-caption">
                    <?php echo NBILL_PRODUCT_DESCRIPTION; ?>
                </td>
                <td class="nbill-setting-value">
                    <?php echo nbf_cms::$interop->render_editor("description", "editor1", $use_posted_values ? nbf_common::get_param($_POST, 'description', '', true, false, true) : $row->description); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_HTML_DESCRIPTION, "description_help"); ?>
                </td>
            </tr>
            <!-- Custom Fields Placeholder -->
            <?php /***** EXCLUDE FROM LITE START *****/ ?>
		    <tr id="nbill-admin-tr-is-user-sub">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_IS_USER_SUB; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("is_sub", "onclick=\"user_sub_changed();\"", $use_posted_values ? nbf_common::get_param($_POST, 'is_sub', '', true) : $row->is_sub); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_IS_USER_SUB . " <strong>" . NBILL_NOTE_USER_SUB . "</strong>", "is_sub_help"); ?>
			    </td>
		    </tr>
		    <tr id="usersub"<?php if (!$row->is_sub) {echo " style=\"display:none;\"";} ?>>
			    <td colspan="2">
				    <div style="margin-left:50px;">
					    <table cellpadding="3" cellspacing="0" border="0" class="adminform" style="width:auto !important">
						    <tr>
							    <td class="nbill-setting-caption">
								    <?php echo NBILL_SUB_USER_GROUP; ?>
							    </td>
							    <td class="nbill-setting-value">
								    <?php
									    //Create a dropdown of access levels
									    $access_list = array();
                                        $group_id_col = nbf_cms::$interop->cms_database_enum->column_user_group_id;
                                        $group_name_col = nbf_cms::$interop->cms_database_enum->column_user_group_name;
                                        $first_level = 0;
                                        if (count($user_groups) > 0)
                                        {
                                            $first_level = intval($user_groups[0]->level);
                                        }
									    foreach ($user_groups as $user_group)
									    {
										    $access_list[] = nbf_html::list_option($user_group->$group_id_col, str_repeat("...", $user_group->level - $first_level) . $user_group->$group_name_col);
									    }
									    if($row->id)
									    {
										    $selected_access = $row->user_group;
									    }
									    else
									    {
										    $selected_access = '';
									    }
									    echo nbf_html::select_list($access_list, "user_group", 'class="inputbox" id="user_group"', $use_posted_values ? nbf_common::get_param($_POST, 'user_group', '', true) : $selected_access);
								    ?>
                                    <?php nbf_html::show_static_help(NBILL_INSTR_SUB_USER_GROUP, "user_group_help"); ?>
							    </td>
						    </tr>
                            <?php if (nbf_cms::$interop->multi_user_group)
                            { ?>
                                <tr>
                                    <td class="nbill-setting-caption">
                                        <?php echo NBILL_MULTI_GROUP; ?>
                                    </td>
                                    <td class="nbill-setting-value">
                                        <?php echo nbf_html::yes_or_no_options("multi_group", "", $use_posted_values ? nbf_common::get_param($_POST, 'multi_group', '', true) : $row->multi_group); ?>
                                        <?php nbf_html::show_static_help(NBILL_INSTR_MULTI_GROUP, "multi_group_help"); ?>
                                    </td>
                                </tr>
                            <?php } ?>
						    <tr>
							    <td class="nbill-setting-caption">
								    <?php echo NBILL_EXPIRY_LEVEL; ?>
							    </td>
							    <td class="nbill-setting-value">
								    <?php
									    $expiry_list = array();
									    $expiry_list[] = nbf_html::list_option(-1, NBILL_EXPIRY_DELETE);
									    $expiry_list[] = nbf_html::list_option(-2, NBILL_EXPIRY_BLOCK);
									    foreach ($user_groups as $user_group)
									    {
										    $expiry_list[] = nbf_html::list_option($user_group->$group_id_col, str_repeat("...", $user_group->level - $first_level) . sprintf(NBILL_EXPIRY_DOWNGRADE, $user_group->$group_name_col));
									    }
									    //Create a dropdown of access levels
									    if($row->expiry_level)
									    {
										    $selected_access = $row->expiry_level;
									    }
									    else
									    {
										    $selected_access = -2;
									    }
									    echo nbf_html::select_list($expiry_list, "expiry_level", 'class="inputbox" id="expiry_level"', $use_posted_values ? nbf_common::get_param($_POST, 'expiry_level', '', true) : $selected_access);
								    ?>
                                    <?php nbf_html::show_static_help(NBILL_INSTR_EXPIRY_LEVEL, "expiry_level_help"); ?>
							    </td>
						    </tr>
						    <tr>
							    <td class="nbill-setting-caption">
								    <?php echo NBILL_EXPIRY_REDIRECT;
								    $default_url = nbf_cms::$interop->site_page_prefix . "&action=subexpiry&task=message" . nbf_cms::$interop->site_page_suffix; ?>
							    </td>
							    <td class="nbill-setting-value">
								    <input type="radio" class="nbill_form_input" name="opt_expiry_redirect" id="opt_redirect_none" value="0" <?php ($use_posted_values && nbf_common::get_param($_POST, 'opt_expiry_redirect') === 0) || nbf_common::nb_strlen($row->expiry_redirect) == 0 ? "checked=\"checked\" " : ""; ?>/><label for="opt_redirect_none" class="nbill_form_label"><?php echo NBILL_REDIRECT_NONE; ?></label>
								    <br />
								    <input type="radio" class="nbill_form_input" name="opt_expiry_redirect" id="opt_redirect_default" value="1" <?php echo ($use_posted_values && nbf_common::get_param($_POST, 'opt_expiry_redirect') === 1) || (!$row->expiry_level || $row->expiry_redirect == urlencode($default_url)) ? "checked=\"checked\" " : ""; ?>/><label for="opt_redirect_default" class="nbill_form_label"><?php echo NBILL_REDIRECT_DEFAULT; ?></label>
								    <br />
								    <input type="radio" class="nbill_form_input" name="opt_expiry_redirect" id="opt_redirect_url" value="2" <?php if (($use_posted_values && nbf_common::get_param($_POST, 'opt_expiry_redirect') === 2) || (nbf_common::nb_strlen($row->expiry_redirect) > 0 && $row->expiry_redirect != urlencode($default_url))) {echo "checked=\"checked\" ";} ?>/><label for="opt_redirect_url" class="nbill_form_label"><?php echo NBILL_REDIRECT_URL; ?></label>
								    <br />
								    <input type="text" name="redirect_url" id="redirect_url" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'redirect_url', '', true) : ($row->expiry_redirect == urlencode($default_url) ? "" : urldecode($row->expiry_redirect)); ?>" style="width:160px;" />
                                    <?php nbf_html::show_static_help(NBILL_INSTR_EXPIRY_REDIRECT, "expiry_redirect_help"); ?>
							    </td>
					    </table>
				    </div>
			    </td>
		    </tr>
            <?php /***** EXCLUDE FROM LITE END *****/ ?>
		    <tr id="nbill-admin-tr-is-free">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_IS_FREEBIE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("is_freebie", ($price_match_found ? 'onchange="update_existing=true;"' : ''), $use_posted_values ? nbf_common::get_param($_POST, 'is_freebie', '', true) : $row->is_freebie); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_IS_FREEBIE, "is_freebie_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-prices">
			    <td colspan="2">
				    <?php
					    echo "<div style=\"margin-bottom:5px\">" . NBILL_PRODUCT_PRICE_INTRO . "</div>";

					    //New tab for each currency
					    foreach ($vendors as $vendor)
					    {
						    //echo "<table id=\"prices_" . $vendor->id . "\" width=\"100%\"><tr><td>";
                            echo "<div id=\"prices_" . $vendor->id . "\">";
						    $nbf_tab_currency = new nbf_tab_group();
						    $nbf_tab_currency->start_tab_group("currency_" . $vendor->id);
                            $price_list = $prices[$vendor->id];
						    foreach ($currencies as $currency)
                            {
                                if (nbf_common::nb_strlen(trim($currency['code'])) > 0)
                                {
                                    $suffix = $currency['code'] . "_" . $vendor->id;
                                    $nbf_tab_currency->add_tab_title($suffix, $currency['code']);
                                }
                            }

						    foreach ($currencies as $currency)
						    {
							    if (nbf_common::nb_strlen(trim($currency['code'])) > 0)
							    {
								    $price_list_record = null;
								    $setup_fee = format_number("0", 'editable_currency');
								    $one_off = format_number("0", 'editable_currency');
								    $weekly = format_number("0", 'editable_currency');
								    $four_weekly = format_number("0", 'editable_currency');
								    $monthly = format_number("0", 'editable_currency');
								    $quarterly = format_number("0", 'editable_currency');
								    $semiannually = format_number("0", 'editable_currency');
								    $annually = format_number("0", 'editable_currency');
								    $biannually = format_number("0", 'editable_currency');
								    $fiveyearly = format_number("0", 'editable_currency');
								    $tenyearly = format_number("0", 'editable_currency');
								    $suffix = $currency['code'] . "_" . $vendor->id;
                                    foreach ($price_list as $price_list_record)
								    {
									    if ($price_list_record->currency_code == $currency['code'])
									    {
										    $setup_fee = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_setup_fee_' . $suffix, '', true) : $price_list_record->net_price_setup_fee, 'editable_currency');
										    $one_off = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_one_off_' . $suffix, '', true) : $price_list_record->net_price_one_off, 'editable_currency');
										    $weekly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_weekly_' . $suffix, '', true) : $price_list_record->net_price_weekly, 'editable_currency');
										    $four_weekly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_four_weekly_' . $suffix, '', true) : $price_list_record->net_price_four_weekly, 'editable_currency');
										    $monthly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_monthly_' . $suffix, '', true) : $price_list_record->net_price_monthly, 'editable_currency');
										    $quarterly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_quarterly_' . $suffix, '', true) : $price_list_record->net_price_quarterly, 'editable_currency');
										    $semiannually = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_semi_annually_' . $suffix, '', true) : $price_list_record->net_price_semi_annually, 'editable_currency');
										    $annually = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_annually_' . $suffix, '', true) : $price_list_record->net_price_annually, 'editable_currency');
										    $biannually = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_biannually_' . $suffix, '', true) : $price_list_record->net_price_biannually, 'editable_currency');
										    $fiveyearly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_five_years_' . $suffix, '', true) : $price_list_record->net_price_five_years, 'editable_currency');
										    $tenyearly = format_number($use_posted_values ? nbf_common::get_param($_POST, 'net_price_ten_years_' . $suffix, '', true) : $price_list_record->net_price_ten_years, 'editable_currency');
										    break;
									    }
								    }
								    ob_start();
					                ?>
					 			    <br /><!--Line break needed for IE6-->

								    <table cellpadding="3" cellspacing="0" border="0" id="nbill-admin-table-product-prices-<?php echo $currency['code']; ?>">
									    <?php if (strtolower(nbf_version::$suffix) != 'lite') { ?>
                                        <tr id="nbill-admin-tr-price-setup-fee">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_SETUP_FEE; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_setup_fee_<?php echo $suffix; ?>" id="net_price_setup_fee_<?php echo $suffix; ?>" value="<?php echo $setup_fee; ?>" class="inputbox" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_SETUP_FEE, "net_price_setup_fee_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
                                        <?php } ?>
									    <tr id="nbill-admin-tr-price-one-off">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_ONE_OFF; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_one_off_<?php echo $suffix; ?>" id="net_price_one_off_<?php echo $suffix; ?>" value="<?php echo $one_off; ?>" class="inputbox" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_ONE_OFF, "net_price_one_off_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
                                        <?php /***** EXCLUDE FROM LITE START *****/ ?>
									    <tr id="nbill-admin-tr-price-weekly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_WEEKLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_weekly_<?php echo $suffix; ?>" id="net_price_weekly_<?php echo $suffix; ?>" value="<?php echo $weekly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'BB', '<?php echo $currency['code']; ?>', <?php echo $weekly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_WEEKLY, "net_price_weekly_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-four-weekly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_FOUR_WEEKLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_four_weekly_<?php echo $suffix; ?>" id="net_price_four_weekly_<?php echo $suffix; ?>" value="<?php echo $four_weekly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'BX', '<?php echo $currency['code']; ?>', <?php echo $four_weekly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_FOUR_WEEKLY, "net_price_four_weekly_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-monthly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_MONTHLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_monthly_<?php echo $suffix; ?>" id="net_price_monthly_<?php echo $suffix; ?>" value="<?php echo $monthly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'CC', '<?php echo $currency['code']; ?>', <?php echo $monthly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_MONTHLY, "net_price_monthly_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-quarterly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_QUARTERLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_quarterly_<?php echo $suffix; ?>" id="net_price_quarterly_<?php echo $suffix; ?>" value="<?php echo $quarterly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'DD', '<?php echo $currency['code']; ?>', <?php echo $quarterly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_QUARTERLY, "net_price_quarterly_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-semi-annually">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_SEMI_ANNUALLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_semi_annually_<?php echo $suffix; ?>" id="net_price_semi_annually_<?php echo $suffix; ?>" value="<?php echo $semiannually; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'DX', '<?php echo $currency['code']; ?>', <?php echo $semiannually; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_SEMI_ANNUALLY, "net_price_semi_annually_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-annually">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_ANNUALLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_annually_<?php echo $suffix; ?>" id="net_price_annually_<?php echo $suffix; ?>" value="<?php echo $annually; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'EE', '<?php echo $currency['code']; ?>', <?php echo $annually; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_ANNUALLY, "net_price_annually_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-biannually">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_BIANNUALLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_biannually_<?php echo $suffix; ?>" id="net_price_biannually_<?php echo $suffix; ?>" value="<?php echo $biannually; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'FF', '<?php echo $currency['code']; ?>', <?php echo $biannually; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_BIANNUALLY, "net_price_biannually_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-five-yearly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_FIVE_YEARLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_five_years_<?php echo $suffix; ?>" id="net_price_five_years_<?php echo $suffix; ?>" value="<?php echo $fiveyearly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'GG', '<?php echo $currency['code']; ?>', <?php echo $fiveyearly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_FIVE_YEARLY, "net_price_five_years_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
									    <tr id="nbill-admin-tr-price-ten-yearly">
										    <td class="nbill-setting-caption">
											    <?php echo NBILL_NET_PRICE_TEN_YEARLY; ?>
										    </td>
										    <td class="nbill-setting-value">
											    <input type="text" name="net_price_ten_years_<?php echo $suffix; ?>" id="net_price_ten_years_<?php echo $suffix; ?>" value="<?php echo $tenyearly; ?>" class="inputbox" onchange="check_existing_orders(<?php echo $vendor->id; ?>, 'HH', '<?php echo $currency['code']; ?>', <?php echo $tenyearly; ?>, this.value);" />
                                                <?php nbf_html::show_static_help(NBILL_INSTR_NET_PRICE_TEN_YEARLY, "net_price_ten_years_" . $suffix . "_help"); ?>
										    </td>
									    </tr>
                                        <?php /***** EXCLUDE FROM LITE END *****/ ?>
								    </table>
					                <?php
								    $nbf_tab_currency->add_tab_content($suffix, ob_get_clean());
							    }
						    }
						    $nbf_tab_currency->end_tab_group();
						    //echo "</td></tr></table>";
                            echo "</div>";
					    }?>
			    </td>
		    </tr>
            <?php if (strtolower(nbf_version::$suffix) != 'lite') { ?>
            <tr id="nbill-admin-tr-allow-freq-change">
                <td class="nbill-setting-caption">
                    <?php echo NBILL_PRODUCT_ALLOW_FREQ_CHANGE; ?>
                </td>
                <td class="nbill-setting-value">
                    <?php echo nbf_html::yes_or_no_options("allow_freq_change", "", $use_posted_values ? nbf_common::get_param($_POST, 'allow_freq_change', '', true) : $row->allow_freq_change); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_ALLOW_FREQ_CHANGE, "allow_freq_change_help"); ?>
                </td>
            </tr>
            <?php } ?>
		    <tr id="nbill-admin-tr-is-taxable">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_IS_TAXABLE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("is_taxable", ($price_match_found ? 'onchange="update_existing=true;"' : ''), $use_posted_values ? nbf_common::get_param($_POST, 'is_taxable', '', true) : $row->is_taxable); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_IS_TAXABLE, "is_taxable_help"); ?>
			    </td>
		    </tr>
            <tr id="nbill-admin-tr-electronic-delivery">
                <td class="nbill-setting-caption">
                    <?php echo NBILL_PRODUCT_ELECTRONIC_DELIVERY; ?>
                </td>
                <td class="nbill-setting-value">
                    <?php echo nbf_html::yes_or_no_options("electronic_delivery", ($price_match_found ? 'onchange="update_existing=true;"' : ''), $use_posted_values ? nbf_common::get_param($_POST, 'electronic_delivery', '', true) : $row->electronic_delivery); ?>
                    <input type="hidden" name="electronic_delivery_orig" id="electronic_delivery_orig" value="<?php echo $row->electronic_delivery; ?>" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_ELECTRONIC_DELIVERY, "electronic_delivery_help"); ?>
                </td>
            </tr>
            <?php /***** EXCLUDE FROM LITE START *****/ ?>
            <tr id="nbill-admin-tr-custom-tax-rate">
                <td class="nbill-setting-caption">
                    <?php echo NBILL_PRODUCT_CUSTOM_TAX_RATE; ?>
                </td>
                <td class="nbill-setting-value">
                    <input type="hidden" name="orig_custom_tax_rate" id="orig_custom_tax_rate" value="<?php echo format_number($use_posted_values ? nbf_common::get_param($_POST, 'orig_custom_tax_rate', '', true) : $row->custom_tax_rate ? $row->custom_tax_rate : '0.00', 'editable_tax_rate'); ?>" />
                    <input type="text" name="custom_tax_rate" id="custom_tax_rate" value="<?php echo format_number($use_posted_values ? nbf_common::get_param($_POST, 'custom_tax_rate', '', true) : $row->custom_tax_rate ? $row->custom_tax_rate : '0.00', 'editable_tax_rate'); ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_PRODUCT_CUSTOM_TAX_RATE, "custom_tax_rate_help"); ?>
                </td>
            </tr>
		    <tr id="nbill-admin-tr-requires-shipping">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_REQUIRES_SHIPPING; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("requires_shipping", ($price_match_found ? 'onchange="update_existing=true;"' : ''), $use_posted_values ? nbf_common::get_param($_POST, 'requires_shipping', '', true) : $row->requires_shipping); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_REQUIRES_SHIPPING, "requires_shipping_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-shipping-services">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_SHIPPING_SERVICES; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php
					    //Create a dropdown of shipping services for each vendor - show/hide via javascript depending on vendor selected
					    foreach ($vendors as $vendor)
					    {
						    $shipping_list = array();
						    $shipping_list[] = nbf_html::list_option("-1", NBILL_NOT_APPLICABLE);
						    foreach ($shipping[$vendor->id] as $shipping_item)
						    {
							    $shipping_list[] = nbf_html::list_option($shipping_item->id, $shipping_item->description);
						    }
						    if($row->id)
						    {
							    $selected_shipping = explode(",", $use_posted_values ? nbf_common::get_param($_POST, 'shipping_' . $vendor->id, '', true) : $row->shipping_services);
						    }
						    else
						    {
							    $selected_shipping = $use_posted_values ? nbf_common::get_param($_POST, 'shiping_' . $vendor->id, '', true) : array();
						    }
						    //Manually output select list to allow for multiple selections
						    echo "<select name=\"shipping_" . $vendor->id . "[]\" multiple=\"multiple\" class=\"inputbox\" id=\"shipping_" . $vendor->id . "\">";
						    foreach($shipping_list as $shipping_list_item)
						    {
							    echo "<option value=\"" . $shipping_list_item->value . "\"";
							    if (array_search($shipping_list_item->value, $selected_shipping) !== false)
							    {
								    echo " selected=\"selected\"";
							    }
							    echo ">" . $shipping_list_item->description . "</option>";
						    }
						    echo "</select>";
					    }
				    ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_SHIPPING_SERVICES_AVAILABLE, "shipping_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-shipping-units">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_SHIPPING_UNITS; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="shipping_units" value="<?php echo format_number($use_posted_values ? nbf_common::get_param($_POST, 'shipping_units', '', true) : $row->shipping_units, 'quantity'); ?>" class="inputbox"<?php echo $price_match_found ? ' onchange="update_existing=true;"' : ''; ?> />
                    <?php nbf_html::show_static_help(NBILL_INSTR_SHIPPING_UNITS, "shipping_units_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-auto-fulfil">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_AUTO_FULFIL; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("auto_fulfil_orders", "", $use_posted_values ? nbf_common::get_param($_POST, 'auto_fulfil_orders', '', true) : $row->auto_fulfil_orders); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_AUTO_FULFIL, "auto_fulfil_orders_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-is-downloadable">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_IS_DOWNLOADABLE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("is_downloadable", nbf_cms::$interop->demo_mode ? "disabled=\"disabled\"" : "", nbf_cms::$interop->demo_mode ? 0 : $use_posted_values ? nbf_common::get_param($_POST, 'is_downloadable', '', true) : $row->is_downloadable); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_IS_DOWNLOADABLE, "is_downloadable_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-location">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LOCATION; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_location" id="download_location" value="<?php echo nbf_cms::$interop->demo_mode ? "Disabled" : $use_posted_values ? nbf_common::get_param($_POST, 'download_location', '', true) : $row->download_location; ?>" <?php if (nbf_cms::$interop->demo_mode) {echo "disabled=\"disabled\" ";} ?>class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LOCATION . ". " . NBILL_PRODUCT_DOWNLOADABLE_TOKENS, "download_location_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-link-text">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LINK_TEXT; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_link_text" id="download_link_text" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'download_link_text', '', true) : $row->download_link_text); ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LINK_TEXT, "download_link_text_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-location-2">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LOCATION_2; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_location_2" id="download_location_2" value="<?php echo nbf_cms::$interop->demo_mode ? "Disabled" : $use_posted_values ? nbf_common::get_param($_POST, 'download_location_2', '', true) : $row->download_location_2; ?>" <?php if (nbf_cms::$interop->demo_mode) {echo "disabled=\"disabled\" ";} ?>class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LOCATION_2, "download_location_2_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-link-text-2">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LINK_TEXT_2; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_link_text_2" id="download_link_text_2" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'download_link_text_2', '', true) : $row->download_link_text_2); ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LINK_TEXT_2, "download_link_text_2_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-location-3">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LOCATION_3; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_location_3" id="download_location_3" value="<?php echo nbf_cms::$interop->demo_mode ? "Disabled" : $use_posted_values ? nbf_common::get_param($_POST, 'download_location_3', '', true) : $row->download_location_3; ?>" <?php if (nbf_cms::$interop->demo_mode) {echo "disabled=\"disabled\" ";} ?>class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LOCATION_3, "download_location_3_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-download-link-text-3">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_DOWNLOAD_LINK_TEXT_3; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="download_link_text_3" id="download_link_text_3" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'download_link_text_3', '', true) : $row->download_link_text_3); ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_DOWNLOAD_LINK_TEXT_3, "download_link_text_3_help"); ?>
			    </td>
		    </tr>
            <?php
            $extra_downloads = ($use_posted_values ? nbf_common::get_param($_POST, 'download_location_4', '', true) : $row->download_location_4);
            if (!$extra_downloads)
            { ?>
                <tr id="extra_downloads_link"><td colspan="2"><a href="#" onclick="for (var i=4;i<=10;i=i+1) {document.getElementById('extra_download_' + i + 'a').style.display='';document.getElementById('extra_download_' + i + 'b').style.display='';}document.getElementById('extra_downloads_link').style.display='none';return false;"><?php echo NBILL_PRODUCT_DOWNLOAD_MORE; ?></a></td></tr>
            <?php } ?>

            <?php for ($i = 4; $i <= 10; $i++) { ?>
            <tr id="extra_download_<?php echo $i; ?>a"<?php echo !$extra_downloads ? ' style="display:none;"' : ''; ?>>
                <td class="nbill-setting-caption">
                    <?php echo constant("NBILL_DOWNLOAD_LOCATION_$i"); ?>
                </td>
                <td class="nbill-setting-value">
                    <input type="text" name="download_location_<?php echo $i; ?>" id="download_location_<?php echo $i; ?>" value="<?php echo nbf_cms::$interop->demo_mode ? "Disabled" : $use_posted_values ? nbf_common::get_param($_POST, 'download_location_' . $i, '', true) : $row->{"download_location_$i"}; ?>" <?php if (nbf_cms::$interop->demo_mode) {echo "disabled=\"disabled\" ";} ?>class="inputbox" />
                    <?php nbf_html::show_static_help(constant("NBILL_INSTR_DOWNLOAD_LOCATION_$i"), "download_location_" . $i . "_help"); ?>
                </td>
            </tr>
            <tr id="extra_download_<?php echo $i; ?>b"<?php echo !$extra_downloads ? ' style="display:none;"' : ''; ?>>
                <td class="nbill-setting-caption">
                    <?php echo constant("NBILL_DOWNLOAD_LINK_TEXT_$i"); ?>
                </td>
                <td class="nbill-setting-value">
                    <input type="text" name="download_link_text_<?php echo $i; ?>" id="download_link_text_<?php echo $i; ?>" value="<?php echo str_replace("\"", "&quot;", $use_posted_values ? nbf_common::get_param($_POST, 'download_link_text_' . $i, '', true) : $row->{"download_link_text_$i"}); ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(constant("NBILL_INSTR_DOWNLOAD_LINK_TEXT_$i"), "download_link_text_" . $i . "_help"); ?>
                </td>
            </tr>
            <?php } ?>
		    <tr id="nbill-admin-tr-download-days-available">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_NO_OF_DAYS_AVAILABLE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <input type="text" name="no_of_days_available" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'no_of_days_available', '', true) : $row->no_of_days_available; ?>" class="inputbox" />
                    <?php nbf_html::show_static_help(NBILL_INSTR_NO_OF_DAYS_AVAILABLE, "no_of_days_available_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-email-downloads">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_EMAIL_DOWNLOADS; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <?php echo nbf_html::yes_or_no_options("email_downloads", nbf_cms::$interop->demo_mode ? "disabled=\"disabled\"" : "", nbf_cms::$interop->demo_mode ? 0 : $use_posted_values ? nbf_common::get_param($_POST, 'email_downloads', '', true) : $row->email_downloads); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_EMAIL_DOWNLOADS, "email_downloads_help"); ?>
			    </td>
		    </tr>
		    <tr id="nbill-admin-tr-email-downloads-message">
			    <td class="nbill-setting-caption">
				    <?php echo NBILL_EMAIL_DOWNLOADS_MESSAGE; ?>
			    </td>
			    <td class="nbill-setting-value">
				    <textarea name="email_downloads_message" class="code"><?php echo $use_posted_values ? nbf_common::get_param($_POST, 'email_downloads_message', '', true, false, true) : $row->email_downloads_message; ?></textarea>
                    <?php nbf_html::show_static_help(NBILL_INSTR_EMAIL_DOWNLOADS_MESSAGE, "email_downloads_message_help"); ?>
			    </td>
		    </tr>
            <?php /***** EXCLUDE FROM LITE END *****/ ?>
		    </table>
        </div>

        <?php
        $tab_settings->add_tab_content("basic", ob_get_clean());
        /***** EXCLUDE FROM LITE START *****/
        ob_start();
        ?>
        <div class="rounded-table">
            <table width="100%" border="0" cellspacing="0" cellpadding="3" class="adminform" id="nbill-admin-table-products-advanced">
            <tr>
                <th colspan="2"><?php echo NBILL_PRODUCT_DETAILS; ?></th>
            </tr>
            <tr id="nbill-admin-tr-allow-global-discounts">
                <td class="nbill-setting-caption">
                    <?php echo NBILL_ALLOW_GLOBAL_DISCOUNTS; ?>
                </td>
                <td class="nbill-setting-value">
                    <?php echo nbf_html::yes_or_no_options("allow_global_discounts", "", $use_posted_values ? nbf_common::get_param($_POST, 'allow_global_discounts', '', true) : $row->allow_global_discounts); ?>
                    <?php nbf_html::show_static_help(NBILL_INSTR_ALLOW_GLOBAL_DISCOUNTS, "allow_global_discounts_help"); ?>
                </td>
            </tr>
            <?php foreach ($vendors as $vendor)
            {
            ?>
                <tr id="product_discounts_<?php echo $vendor->id; ?>">
                    <td colspan="2">
                        <input type="hidden" name="discount_<?php echo $vendor->id; ?>_count" id="discount_<?php echo $vendor->id; ?>_count" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount' . $vendor->id . '_count', '', true) : count(@$product_discounts[$vendor->id]); ?>" />
                        <input type="hidden" name="discount_deleted_items_<?php echo $vendor->id; ?>" id="discount_deleted_items_<?php echo $vendor->id; ?>" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_deleted_items_' . $vendor->id, '', true) : ''; ?>" />
                        <input type="hidden" name="discount_added_items_<?php echo $vendor->id; ?>" id="discount_added_items_<?php echo $vendor->id; ?>" value = "<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_added_items_' . $vendor->id, '', true) : ''; ?>" />

                        <div class="rounded-table">
                            <table cellpadding="4" cellspacing="0" border="0" class="adminform" id="product_discount_<?php echo $vendor->id; ?>_table">
                                <tr><th><?php echo NBILL_PRODUCT_DISCOUNT_TITLE; ?></th></tr>
                                <tr><td><?php echo NBILL_PRODUCT_DISCOUNT_INTRO; ?></td></tr>
                            </table>

                            <table>
                                <tr>
                                    <td><strong><?php echo NBILL_PRODUCT_DISCOUNT;?></strong></td>
                                    <td><strong><?php echo NBILL_PRODUCT_DISCOUNT_PRIORITY; ?></strong></td>
                                    <td><strong><?php echo NBILL_PRODUCT_DISCOUNT_QTY; ?></strong></td>
                                    <td><strong><?php echo NBILL_PRODUCT_DISCOUNT_MULTIPLY; ?></strong></td>
                                    <td style="width:100%;"><strong><?php echo NBILL_PRODUCT_DISCOUNT_OFFSET; ?></strong></td>
                                </tr>
                                <?php
                                    //Display controls for each existing product discount
                                    $discount_no = 0;
                                    foreach ($product_discounts[$vendor->id] as $product_discount)
                                    { ?>
                                    <tr id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_row">
                                        <td>
                                            <input type="hidden" name="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_id" id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_id" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_' . $discount_no, '', true) : $product_discount->discount_id; ?>" />
                                            <?php
                                            foreach ($discounts[$vendor->id] as $discount)
                                            {
                                                if ($discount->id == $product_discount->discount_id)
                                                {
                                                    echo $discount->discount_name;
                                                    break;
                                                }
                                            }
                                            ?>
                                        </td>
                                        <td><input type="text" name="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_priority" id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_priority" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_' . $discount_no . '_priority', '', true) : $product_discount->priority; ?>" class="inputbox small-numeric" /></td>
                                        <td><input type="text" name="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_quantity" id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_quantity" value="<?php echo format_number($use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_' . $discount_no . '_quantity', '', true) : $product_discount->quantity_required, 'quantity'); ?>" class="inputbox small-numeric" /></td>
                                        <td><?php echo nbf_html::yes_or_no_options("discount_" . $vendor->id . "_" . $discount_no . "_multiply", "", $product_discount->multiply); ?></td>
                                        <td><input type="text" name="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_offset" id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_offset" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_' . $discount_no . '_offset', '', true) : $product_discount->offset; ?>" class="inputbox small-numeric" /><br />
                                        <input type="button" name="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_delete" id="discount_<?php echo $vendor->id; ?>_<?php echo $discount_no; ?>_delete" value="<?php echo NBILL_DELETE_PRODUCT_DISCOUNT; ?>" onclick="delete_discount('<?php echo $discount_no; ?>');" />
                                    </tr>
                                    <?php
                                        $discount_no++;
                                    }
                                    //Display extra row for new discount
                                ?>
                                <tr style="background-color: #ccffcc;" id="discount_<?php echo $vendor->id; ?>_new_row">
                                    <td>
                                    <?php
                                        $discount_list = array();
                                        $discount_list[] = nbf_html::list_option(0, NBILL_NOT_APPLICABLE);
                                        foreach ($discounts[$vendor->id] as $discount)
                                        {
                                            $discount_list[] = nbf_html::list_option($discount->id, $discount->discount_name);
                                        }
                                        echo nbf_html::select_list($discount_list, "discount_" . $vendor->id . "_new_id", 'class="inputbox squashable" id="discount_' . $vendor->id . '_new_id"', 'value', 'text');
                                    ?>
                                    </td>
                                    <td><input type="text" name="discount_<?php echo $vendor->id; ?>_new_priority" id="discount_<?php echo $vendor->id; ?>_new_priority" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_new_priority', '', true) : ''; ?>" class="inputbox small-numeric" /></td>
                                    <td><input type="text" name="discount_<?php echo $vendor->id; ?>_new_quantity" id="discount_<?php echo $vendor->id; ?>_new_quantity" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_new_quantity', '', true) : ''; ?>" class="inputbox small-numeric" /></td>
                                    <td><?php echo nbf_html::yes_or_no_options("discount_" . $vendor->id . "_new_multiply", "", $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_new_multiply', '', true) : "1"); ?></td>
                                    <td><input type="text" name="discount_<?php echo $vendor->id; ?>_new_offset" id="discount_<?php echo $vendor->id; ?>_new_offset" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'discount_' . $vendor->id . '_new_offset', '', true) : ''; ?>" class="inputbox small-numeric" /><br />
                                    <input type="button" name="add_<?php echo $vendor->id; ?>_new_discount" id="add_<?php echo $vendor->id; ?>_new_discount" value="<?php echo NBILL_ADD_PRODUCT_DISCOUNT; ?>" onclick="add_product_discount();" /></td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            <?php
            }
            ?>

            </table>
        </div>

        <?php
        $tab_settings->add_tab_content("advanced", ob_get_clean());
        /***** EXCLUDE FROM LITE END *****/
        $tab_settings->end_tab_group();
        ?>

        <?php
        /***** EXCLUDE FROM LITE START *****/
        if (file_exists(nbf_cms::$interop->nbill_admin_base_path . '/admin.proc/supporting_docs.php') &&$row->id)
        { ?>
            <div id="attachments_<?php echo $row->id; ?>" style="clear:both;">
                <input type="hidden" name="attachment_id" id="attachment_id" value="" />
                <input type="hidden" name="use_posted_values" value="" />
                <table cellpadding="3" cellspacing="0" border="0">
                <?php
                foreach ($attachments as $attachment)
                {
                    ?>
                    <tr class="nbill-admin-tr-attachment">
                    <td>
                        <a href="<?php echo nbf_cms::$interop->admin_page_prefix; ?>&action=supporting_docs&task=download&file=<?php echo base64_encode($attachment->id); ?>"><img style="vertical-align:middle" border="0" alt="" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/file.png" />&nbsp;<?php echo $attachment->file_name; ?></a>
                    </td>
                    <td>
                        <input type="button" class="button btn" value="<?php echo NBILL_DETACH; ?>" onclick="if(confirm('<?php echo NBILL_DETACH_SURE; ?>')){document.adminForm.attachment_id.value='<?php echo $attachment->id; ?>';document.adminForm.task.value='detach_file_edit';document.adminForm.submit();}" />
                    </td>
                    <td>
                        <input type="button" class="button btn" value="<?php echo NBILL_DELETE; ?>" onclick="if(confirm('<?php echo sprintf(NBILL_DELETE_FILE_SURE, $attachment->file_name); ?>')){document.adminForm.attachment_id.value='<?php echo $attachment->id; ?>';document.adminForm.task.value='delete_file_edit';document.adminForm.submit();}" />
                    </td>
                    </tr>
                    <?php
                }
                ?>
                <tr><td colspan="3">
                <a href="#" onclick="window.open('<?php echo nbf_cms::$interop->admin_popup_page_prefix; ?>&hide_billing_menu=1&action=supporting_docs&use_stylesheet=1&show_toolbar=1&attach_to_type=PR&attach_to_id=<?php echo $row->id; ?>','','scrollbars=1,width=790,height=500');return false;"><img style="vertical-align:middle" border="0" alt="" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/icons/supporting_docs.gif" />&nbsp;<?php echo NBILL_NEW_ATTACHMENT; ?></a>
                </td></tr>
                </table>
            </div>
        <?php }
        /***** EXCLUDE FROM LITE END *****/ ?>

		</form>
		<script type="text/javascript">
		refresh_vendor();
		</script>
		<?php
	}
}