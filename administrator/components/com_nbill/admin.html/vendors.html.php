<?php
/**
* HTML output for vendors
* @version 2
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nBillVendors
{
	public static function showVendors($rows, $pagination)
	{
		?>
		<table class="adminheading" style="width:auto;">
		<tr class="nbill-admin-heading">
            <th <?php echo sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, "vendors"); ?>>
				<?php echo NBILL_BRANDING_NAME . NBILL_BRANDING_TRADEMARK_SYMBOL . ": " . NBILL_VENDORS_TITLE; ?>
			</th>
		</tr>
		</table>

		<div class="nbill-message-ie-padding-bug-fixer"></div>
		<?php
		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		} ?>

		<form action="<?php echo nbf_cms::$interop->admin_page_prefix; ?>" method="post" name="adminForm" id="adminForm">
		<p align="left"><?php echo NBILL_VENDOR_INTRO; ?></p>

        <div class="rounded-table">
            <table class="adminlist">
            <tr class="nbill-admin-title-row">
                <th class="selector">
			    #
			    </th>
                <th class="selector">
                    <input type="checkbox" name="check_all" value="" onclick="for(var i=0; i<<?php echo count($rows); ?>;i++) {document.getElementById('cb' + i).checked=this.checked;} document.adminForm.box_checked.value=this.checked;" />
			    </th>
			    <th class="title">
				    <?php echo NBILL_VENDOR_NAME; ?>
			    </th>
			    <th class="title">
				    <?php echo NBILL_VENDOR_COUNTRY; ?>
			    </th>
			    <th class="title">
				    <?php echo NBILL_NEXT_NBILL_NO; ?>
			    </th>
		    </tr>
		    <?php
			    for ($i=0, $n=count( $rows ); $i < $n; $i++)
			    {
				    $row = &$rows[$i];
				    $link = nbf_cms::$interop->admin_page_prefix . "&action=vendors&task=edit&cid=$row->id";
				    echo "<tr>";
				    echo "<td class=\"selector\">";
				    echo $pagination->list_offset + $i + 1;
				    $checked = nbf_html::id_checkbox($i, $row->id);
				    echo "</td><td class=\"selector\">$checked</td>";
				    echo "<td class=\"list-value\"><a href=\"$link\" title=\"" . NBILL_EDIT_VENDOR . "\">" . $row->vendor_name . "</a></td>";
				    echo "<td class=\"list-value\">" . $row->vendor_country . "</td>";
				    echo "<td class=\"list-value\">" . $row->next_invoice_no . "</td>";
				    echo "</tr>";
			    }
		    ?>
		    <tr class="nbill_tr_no_highlight"><td colspan="5" class="nbill-page-nav-footer"><?php echo $pagination->render_page_footer(); ?></td></tr>
		    </table>
        </div>

		<input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
		<input type="hidden" name="action" value="vendors" />
		<input type="hidden" name="task" value="" />
		<input type="hidden" name="box_checked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0">
		</form>
		<?php
	}

	/**
	* Edit a vendor (or create a new one)
	*/
	public static function editVendor($vendor_id, $row, $country_codes, $currency_codes, $templates, $email_templates, $gateways, $use_posted_values = false, $master_connect = false, $master_vendors = array(), $test_connection = false)
	{
		nbf_cms::$interop->init_editor();
        include_once(nbf_cms::$interop->nbill_admin_base_path . "/framework/ajax/nbill.ajax.client.php");
		?>
		<script language="javascript" type="text/javascript">
		function nbill_submit_task(task_name)
		{
			var form = document.adminForm;
			if (task_name == 'cancel')
            {
				document.adminForm.task.value=task_name;
                document.adminForm.submit();
				return;
			}

			// do field validation
			if (form.vendor_name.value == "")
			{
				alert('<?php echo NBILL_VENDOR_NAME_REQUIRED; ?>');
			}
			else if (form.vendor_address.value == "")
			{
				alert('<?php echo NBILL_VENDOR_ADDRESS_REQUIRED; ?>');
			}
			else if (form.vendor_country.value == "")
			{
				alert('<?php echo NBILL_VENDOR_COUNTRY_REQUIRED; ?>');
			}
			else if (form.vendor_currency.value == "")
			{
				alert('<?php echo NBILL_VENDOR_CURRENCY_REQUIRED; ?>');
			}
			else if (form.admin_email == "")
			{
				alert('<?php echo NBILL_VENDOR_EMAIL_REQUIRED; ?>');
			}
			else
			{
				document.adminForm.task.value=task_name;
                document.adminForm.submit();
			}
		}

        //Need a few globals for asynchronous processing of master db update
        var abort = false;
        var maxdate = new Array();
        var records_inserted = 0;
        var records_updated = 0;
        var record_pointer = 0;
        var percentage = 0;
        var complete = 0;
        var message = '';
        var task_name = 'start';

        <?php
        echo get_prompt_js();
        ?>
        function sync_click()
        {
            if (document.getElementById('use_master_db1').checked)
            {
                if (confirm('<?php echo NBILL_SYNC_ARE_YOU_SURE; ?>'))
                {
                    IEprompt('<?php echo NBILL_SYNC_UP_TO;?>', '<?php echo nbf_common::nb_date("Y/m/d", nbf_common::nb_time()); ?>');
                }
            }
            else
            {
                alert('<?php echo str_replace("'", "\\'", NBILL_MASTER_DB_NOT_IN_USE); ?>');
            }
        }

        function promptCallback(date_entered)
        {
            records_inserted = 0;
            records_updated = 0;
            percentage = 0;
            record_pointer = 0;
            maxdate = date_entered.split('/');
            do_vendor_sync('<?php echo NBILL_SYNC_VENDOR; ?>');
        }

        /***** EXCLUDE FROM LITE START *****/
        function do_vendor_sync(task_title)
        {
            if (maxdate.length == 3)
            {
                if ((parseInt(maxdate[0], 10) + '').length == 4 && parseInt(maxdate[1], 10) > 0 && parseInt(maxdate[1], 10) < 13 && parseInt(maxdate[2], 10) > 0 && parseInt(maxdate[2], 10) < 32)
                {
                    IEprompt('', '', draw_progress_bar(percentage, task_title));
                    complete = 0;
                    message = '';
                    abort = false;
                    submit_ajax_request('sync_vendor', 'master_host=' + URLEncode(document.getElementById('master_host').value) + '&master_username=' + URLEncode(document.getElementById('master_username').value) + '&master_password=' + URLEncode(document.getElementById('master_password').value) + '&master_dbname=' + URLEncode(document.getElementById('master_dbname').value) + '&master_table_prefix=' + URLEncode(document.getElementById('master_table_prefix').value) + '&master_vendor_id=' + parseInt(document.getElementById('master_vendor_id').value) + '&vendor_id=' + <?php echo $vendor_id; ?> + '&maxdate_year=' + parseInt(maxdate[0], 10) + '&maxdate_month=' + parseInt(maxdate[1], 10) + '&maxdate_day=' + parseInt(maxdate[2], 10) + '&taskname=' + task_name + '&pointer=' + record_pointer, vendor_sync_ajax_callback, false);
                    return;
                }
            }
            alert('<?php echo sprintf(NBILL_INVALID_DATE_ENTERED, 'yyyy/mm/dd'); ?>');
            // clear out the dialog box
            _dialogPromptID.style.display='none';
            // clear out the screen
            _blackoutPromptID.style.display='none';
            //Show dropdowns
            IEshow_dropdowns();
        }

        function vendor_sync_ajax_callback(ajax_result)
        {
            ajax_result = ajax_result.split('#!#');
            if (ajax_result.length == 8)
            {
                message = urldecode(ajax_result[7]);
                if (message.length == 0)
                {
                    record_pointer = ajax_result[0];
                    task_name = ajax_result[1];
                    percentage = ajax_result[3];
                }
                complete = ajax_result[2];
                task_title = ajax_result[4];
                if (task_title.length == 0)
                {
                    task_title = '<?php echo NBILL_SYNC_VENDOR; ?>'
                }
                records_inserted += parseInt(ajax_result[5]);
                records_updated += parseInt(ajax_result[6]);
                if (message.length == 0 && !abort)
                {
                    if (complete == 0)
                    {
                        do_vendor_sync(task_title);
                        return;
                    }
                }
                else
                {
                    abort_sync();
                }
            }
            else
            {
                //Something went wrong!
                abort = true;
            }

            if (message.length == 0 && abort)
            {
                message = '<?php echo NBILL_VENDOR_SYNC_ABORTED; ?>'
                message += '<?php echo NBILL_SYNC_SUCCESS_RECORD_COUNT; ?>'.replace('%s1', records_inserted).replace('%s2', records_updated);
            }
            else
            {
                //Completed successfully
                IEprompt('', '', draw_progress_bar(100, task_title));
                if(message.length == 0)
                {
                    message = '<?php echo NBILL_SYNC_SUCCESS; ?>';
                }
                message += '\n\n<?php echo NBILL_SYNC_SUCCESS_RECORD_COUNT; ?>'.replace('%s1', records_inserted).replace('%s2', records_updated);
                document.getElementById('vendor_sync_retry').style.display = 'none';
            }
            if (message.length > 0)
            {
                alert(message);

                // clear out the dialog box
                _dialogPromptID.style.display='none';
                // clear out the screen
                _blackoutPromptID.style.display='none';
                //Show dropdowns
                IEshow_dropdowns();

                if (abort || complete == 0)
                {
                    document.getElementById('vendor_sync_retry').innerHTML = '<?php echo str_replace('>', '\\>', str_replace('<', '\\<', NBILL_VENDOR_SYNC_RETRY)); //Some external entity is inserting line breaks before all opening tags ?>'.replace('%s', 'do_vendor_sync(\'' + task_title + '\')');
                    document.getElementById('vendor_sync_retry').style.display = 'block';
                }
                return;
            }
        }

        function draw_progress_bar(percentage, task_name)
        {
            percentage = percentage > 100 ? 100 : percentage;
            percentage = percentage < 0 ? 0 : percentage;
            var html = '<div style="margin-top: 5px;width:300px;height:100px;text-align:center;padding:0px;margin-left:auto;margin-right:auto;">';
            html += '<p><strong>' + task_name + ' - ' + parseInt(percentage) + '%</strong></p>';
            html += '<div style="border:solid 1px #666666;background-color:#ffffff;width:200px;height:30px;margin-left:auto;margin-right:auto;text-align:left;">';
            html += '<div style="background-color:#000099;width:' + parseInt(percentage * 2) + 'px;height:30px;"></';
            html += 'div>';
            html += '<div style="margin-top: 10px;margin-left:auto;margin-right:auto;width:50px;"><input type="button" id="abort_vendor_sync" style="width:50px;" value="<?php echo NBILL_VENDOR_SYNC_ABORT; ?>" onclick="if (confirm(\'<?php echo NBILL_VENDOR_SYNC_ABORT_SURE; ?>\')){abort_sync();}" /></';
            html += 'div></';
            html += 'div></';
            html += 'div>';
            return html;
        }

        function abort_sync()
        {
            var innerhtml = '<div style="width:100%;height:100%;text-align:center;"><div style="margin:auto;width:auto;padding:10px;"><h3><?php echo NBILL_VENDOR_SYNC_ABORTING; ?></h3></';
            innerhtml += 'div></';
            innerhtml += 'div>';
            _dialogPromptID.innerHTML = innerhtml;
            abort = true;
        }
        /***** EXCLUDE FROM LITE END *****/

        <?php
        /*
        This copyright notice applies to the following URLEncode javascript function ONLY:
        Copyright (c) 2007 & 2008 cass-hacks.com

        Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

           1. Redistributions of source code must retain the above copyright notice, this list of conditions, and the following disclaimer.
           2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution, and in the same place and form as other copyright, license and disclaimer information.
           3. The end-user documentation included with the redistribution, if any, must include the following acknowledgment: "This product includes software developed by Cass-hacks.com (http://cass-hacks.com/) and its contributors", in the same place and form as other third-party acknowledgments. Alternately, this acknowledgment may appear in the software itself, in the same form and location as other such third-party acknowledgments.
           4. Except as contained in this notice, the name of Cass-hacks.com shall not be used in advertising or otherwise to promote the sale, use or other dealings in this Software without prior written authorization from Cass-hacks.com.

        THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE XFREE86 PROJECT, INC OR ITS CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
        */
        ?>
        function URLEncode (clearString)
        {
          var output = '';
          var x = 0;
          clearString = clearString.toString();
          var regex = /(^[a-zA-Z0-9_.]*)/;
          while (x < clearString.length) {
            var match = regex.exec(clearString.substr(x));
            if (match != null && match.length > 1 && match[1] != '') {
                output += match[1];
              x += match[1].length;
            } else {
              if (clearString[x] == ' ')
                output += '+';
              else {
                var charCode = clearString.charCodeAt(x);
                var hexVal = charCode.toString(16);
                output += '%' + ( hexVal.length < 2 ? '0' : '' ) + hexVal.toUpperCase();
              }
              x++;
            }
          }
          return output;
        }

        function urldecode (str)
        {
            // Decodes URL-encoded string
            //
            // version: 909.322
            // discuss at: http://phpjs.org/functions/urldecode
            // +   original by: Philip Peterson
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +      input by: AJ
            // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   improved by: Brett Zamir (http://brett-zamir.me)
            // +      input by: travc
            // +      input by: Brett Zamir (http://brett-zamir.me)
            // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
            // +   improved by: Lars Fischer
            // +      input by: Ratheous
            // +   improved by: Orlando
            // %        note 1: info on what encoding functions to use from: http://xkr.us/articles/javascript/encode-compare/
            // *     example 1: urldecode('Kevin+van+Zonneveld%21');
            // *     returns 1: 'Kevin van Zonneveld!'
            // *     example 2: urldecode('http%3A%2F%2Fkevin.vanzonneveld.net%2F');
            // *     returns 2: 'http://kevin.vanzonneveld.net/'
            // *     example 3: urldecode('http%3A%2F%2Fwww.google.nl%2Fsearch%3Fq%3Dphp.js%26ie%3Dutf-8%26oe%3Dutf-8%26aq%3Dt%26rls%3Dcom.ubuntu%3Aen-US%3Aunofficial%26client%3Dfirefox-a');
            // *     returns 3: 'http://www.google.nl/search?q=php.js&ie=utf-8&oe=utf-8&aq=t&rls=com.ubuntu:en-US:unofficial&client=firefox-a'

            var hash_map = {}, ret = str.toString(), unicodeStr='', hexEscStr='';

            var replacer = function (search, replace, str) {
                var tmp_arr = [];
                tmp_arr = str.split(search);
                return tmp_arr.join(replace);
            };

            // The hash_map is identical to the one in urlencode.
            hash_map["'"]   = '%27';
            hash_map['(']   = '%28';
            hash_map[')']   = '%29';
            hash_map['*']   = '%2A';
            hash_map['~']   = '%7E';
            hash_map['!']   = '%21';
            hash_map['%20'] = '+';
            hash_map['\u00DC'] = '%DC';
            hash_map['\u00FC'] = '%FC';
            hash_map['\u00C4'] = '%D4';
            hash_map['\u00E4'] = '%E4';
            hash_map['\u00D6'] = '%D6';
            hash_map['\u00F6'] = '%F6';
            hash_map['\u00DF'] = '%DF';
            hash_map['\u20AC'] = '%80';
            hash_map['\u0081'] = '%81';
            hash_map['\u201A'] = '%82';
            hash_map['\u0192'] = '%83';
            hash_map['\u201E'] = '%84';
            hash_map['\u2026'] = '%85';
            hash_map['\u2020'] = '%86';
            hash_map['\u2021'] = '%87';
            hash_map['\u02C6'] = '%88';
            hash_map['\u2030'] = '%89';
            hash_map['\u0160'] = '%8A';
            hash_map['\u2039'] = '%8B';
            hash_map['\u0152'] = '%8C';
            hash_map['\u008D'] = '%8D';
            hash_map['\u017D'] = '%8E';
            hash_map['\u008F'] = '%8F';
            hash_map['\u0090'] = '%90';
            hash_map['\u2018'] = '%91';
            hash_map['\u2019'] = '%92';
            hash_map['\u201C'] = '%93';
            hash_map['\u201D'] = '%94';
            hash_map['\u2022'] = '%95';
            hash_map['\u2013'] = '%96';
            hash_map['\u2014'] = '%97';
            hash_map['\u02DC'] = '%98';
            hash_map['\u2122'] = '%99';
            hash_map['\u0161'] = '%9A';
            hash_map['\u203A'] = '%9B';
            hash_map['\u0153'] = '%9C';
            hash_map['\u009D'] = '%9D';
            hash_map['\u017E'] = '%9E';
            hash_map['\u0178'] = '%9F';
            hash_map['\u00C6'] = '%C3%86';
            hash_map['\u00D8'] = '%C3%98';
            hash_map['\u00C5'] = '%C3%85';

            for (unicodeStr in hash_map) {
                hexEscStr = hash_map[unicodeStr]; // Switch order when decoding
                ret = replacer(hexEscStr, unicodeStr, ret); // Custom replace. No regexing
            }

            // End with decodeURIComponent, which most resembles PHP's encoding functions
            ret = decodeURIComponent(ret);

            return ret;
        }
		</script>

		<table class="adminheading" style="width:auto;">
		<tr class="nbill-admin-heading">
            <th <?php echo sprintf(NBILL_ADMIN_IMAGE, nbf_cms::$interop->nbill_site_url_path, "vendors"); ?>>
				<?php echo NBILL_BRANDING_NAME . NBILL_BRANDING_TRADEMARK_SYMBOL; ?>:
				<?php echo $row->id ? NBILL_EDIT_VENDOR . " '$row->vendor_name'" : NBILL_NEW_VENDOR; ?>
			</th>
		</tr>
		</table>

		<div class="nbill-message-ie-padding-bug-fixer"></div>
		<?php
		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>\n";
		} ?>
        <div id="vendor_sync_retry" style="display:none;" class="nbill-message"></div>
		<form enctype="multipart/form-data" action="<?php echo nbf_cms::$interop->admin_page_prefix; ?>" method="post" name="adminForm" id="adminForm">
		<input type="hidden" name="MAX_FILE_SIZE" value="30720" />
		<input type="hidden" name="id" value="<?php echo $vendor_id;?>" />
		<?php nbf_html::add_filters(); ?>

        <?php
        $tab_settings = new nbf_tab_group();
        $tab_settings->start_tab_group("admin_settings");
        $tab_settings->add_tab_title("basic", NBILL_ADMIN_TAB_BASIC);
        $tab_settings->add_tab_title("advanced", NBILL_ADMIN_TAB_ADVANCED);
        ob_start();
        ?>

        <div class="rounded-table">
		<table width="100%" border="0" cellspacing="0" cellpadding="3" class="adminform" id="nbill-admin-table-vendor">
        <?php if ($row->invoice_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_INVOICE_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-invoice-no" onclick="nbill_submit_task('unlock-invoice');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($row->order_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_ORDER_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-order-no" onclick="nbill_submit_task('unlock-order');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($row->receipt_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_RECEIPT_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-receipt-no" onclick="nbill_submit_task('unlock-receipt');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($row->payment_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_PAYMENT_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-payment-no" onclick="nbill_submit_task('unlock-payment');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($row->credit_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_CREDIT_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-credit-no" onclick="nbill_submit_task('unlock-credit');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
        <?php if ($row->quote_no_locked) { ?>
        <tr>
            <td colspan="2">
                <br /><strong>
                <?php echo NBILL_QUOTE_NO_LOCKED; ?></strong>&nbsp;<input type="submit" name="unlock-quote-no" onclick="nbill_submit_task('unlock-quote');return false;" value="<?php echo NBILL_UNLOCK; ?>" />
            </td>
        </tr>
        <?php } ?>
		<tr>
			<th colspan="2"><?php echo NBILL_VENDOR_DETAILS; ?></th>
		</tr>
		<tr id="nbill-admin-tr-vendor-name">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_NAME; ?>
			</td>
			<td class="nbill-setting-value">
				<input type="text" name="vendor_name" id="vendor_name" value="<?php echo $use_posted_values ? str_replace("\"", "&quot;", nbf_common::get_param($_POST,'vendor_name', null, true)) : str_replace("\"", "&quot;", $row->vendor_name); ?>" class="inputbox" style="width:160px" />
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_NAME, "vendor_name_help"); ?>
			</td>
		</tr>
        <!-- Custom Fields Placeholder -->
        <tr id="nbill-admin-tr-admin-email">
            <td class="nbill-setting-caption">
                <?php echo NBILL_ADMIN_EMAIL; ?>
            </td>
            <td class="nbill-setting-value">
                <input type="text" name="admin_email" id="admin_email" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST,'admin_email', null, true) : $row->admin_email; ?>" class="inputbox" style="width:160px" />
                <?php nbf_html::show_static_help(NBILL_INSTR_ADMIN_EMAIL, "admin_email_help"); ?>
            </td>
        </tr>

		<tr id="nbill-admin-tr-vendor-logo">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_LOGO; ?>
			</td>
			<td class="nbill-setting-value">
				<input type="file" name="vendor_logo" id="vendor_logo" value="" class="inputbox" style="width:160px"<?php if (nbf_cms::$interop->demo_mode) {echo ' disabled="disabled"';} ?> />
				<?php
                $vendor_logo_file = nbf_cms::$interop->nbill_fe_base_path . "/images/vendors/" . $vendor_id . ".png";
                if (!file_exists($vendor_logo_file))
                {
                    $vendor_logo_file = nbf_cms::$interop->nbill_fe_base_path . "/images/vendors/" . $vendor_id . ".gif";
                }
				if (file_exists($vendor_logo_file))
				{
					//Have to use a table below because DIVs do not autosize according to their content
					?>
					<input type="hidden" name="delete_vendor_logo" id="delete_vendor_logo" value="" />
					<br /><table cellpadding="0" cellspacing="0" border="0"><tr><td style="padding: 5px; border:solid 1px #dddddd; background-color: #ffffff;"><img id="logo_preview" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/vendors/<?php echo basename($vendor_logo_file); ?>" alt="<?php echo NBILL_VENDOR_LOGO; ?>" /></td><td><input type="button" name="delete_logo" id="delete_logo" class="button btn" value="<?php echo NBILL_DELETE_LOGO; ?>" onclick="document.getElementById('logo_preview').style.display='none';document.getElementById('delete_logo').style.display='none';document.getElementById('delete_vendor_logo').value=1;"<?php if (nbf_cms::$interop->demo_mode) {echo ' disabled="disabled"';} ?> /></td></tr></table>
				<?php }
				?>
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_LOGO, "vendor_logo_help"); ?>
			</td>
		</tr>
		<tr id="nbill-admin-tr-vendor-address">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_ADDRESS; ?>
			</td>
			<td class="nbill-setting-value">
				<textarea name="vendor_address" id="vendor_address" class="inputbox" rows="4" cols="20"><?php echo $use_posted_values ? nbf_common::get_param($_POST,'vendor_address', "", true) : trim($row->vendor_address); ?></textarea>
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_ADDRESS, "vendor_address_help"); ?>
			</td>
		</tr>
		<tr id="nbill-admin-tr-vendor-country">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_COUNTRY; ?>
			</td>
			<td class="nbill-setting-value">
				<?php
					$vendor_country = array();
					foreach ($country_codes as $country_code)
					{
						$vendor_country[] = nbf_html::list_option($country_code['code'], $country_code['description']);
					}
					echo nbf_html::select_list($vendor_country, "vendor_country", 'class="inputbox" id="vendor_country"', $use_posted_values ? nbf_common::get_param($_POST,'vendor_country', null, true) : $row->vendor_country );
				?>
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_COUNTRY, "vendor_country_help"); ?>
			</td>
		</tr>
		<tr id="nbill-admin-tr-vendor-currency">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_CURRENCY; ?>
			</td>
			<td class="nbill-setting-value">
				<?php
					$vendor_currency = array();
					foreach ($currency_codes as $currency_code)
					{
						$vendor_currency[] = nbf_html::list_option($currency_code['code'], $currency_code['description']);
					}
					echo nbf_html::select_list($vendor_currency, "vendor_currency", 'id="vendor_currency" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'vendor_currency', null, true) : $row->vendor_currency );
				?>
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_CURRENCY, "vendor_currency_help"); ?>
			</td>
		</tr>
		<tr id="nbill-admin-tr-default-vendor">
			<td class="nbill-setting-caption">
				<?php echo NBILL_VENDOR_DEFAULT; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_html::yes_or_no_options("default_vendor", "", $use_posted_values ? nbf_common::get_param($_POST, 'default_vendor', null, true) : $row->default_vendor); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_VENDOR_DEFAULT, "default_vendor_help"); ?>
			</td>
		</tr>

        <tr id="nbill-admin-tr-tax-reference-no">
            <td class="nbill-setting-caption">
                <?php echo NBILL_TAX_REFERENCE_NO; ?>
            </td>
            <td class="nbill-setting-value">
                <input type="text" name="tax_reference_no" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST, 'tax_reference_no', null, true) : $row->tax_reference_no; ?>" class="inputbox" />
                <?php nbf_html::show_static_help(NBILL_INSTR_TAX_REFERENCE_NO, "tax_reference_no_help"); ?>
            </td>
        </tr>

        <tr id="nbill-admin-tr-default-gateway">
            <td class="nbill-setting-caption">
                <?php echo NBILL_DEFAULT_GATEWAY; ?>
            </td>
            <td class="nbill-setting-value">
                <?php
                    $gateway_list = array();
                    $gateway_list[] = nbf_html::list_option(-1, NBILL_NOT_APPLICABLE);
                    foreach ($gateways as $gateway)
                    {
                        $gateway_list[] = nbf_html::list_option($gateway->gateway_id, $gateway->display_name);
                    }
                    echo nbf_html::select_list($gateway_list, "default_gateway", 'id="default_gateway" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'default_gateway', null, true) : $row->default_gateway );
                ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_DEFAULT_GATEWAY, "default_gateway_help"); ?>
            </td>
        </tr>
		<tr id="nbill-admin-tr-next-invoice-no">
			<td class="nbill-setting-caption">
				<?php echo NBILL_NEXT_NBILL_NO; ?>
			</td>
			<td class="nbill-setting-value">
				<?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_invoice_no', null, true) : $row->next_invoice_no;
				if (nbf_common::nb_strlen($nextno) == 0)
				{
					$nextno = "0001";
				}?>
				<input type="text" name="next_invoice_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_invoice_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_INVOICE_NO, "next_invoice_no_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
		<tr id="nbill-admin-tr-next-order-no">
			<td class="nbill-setting-caption">
				<?php echo NBILL_NEXT_ORDER_NO; ?>
			</td>
			<td class="nbill-setting-value">
				<?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_order_no', null, true) : $row->next_order_no;
				if (nbf_common::nb_strlen($nextno) == 0)
				{
					$nextno = "0001";
				}?>
				<input type="text" name="next_order_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_order_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_ORDER_NO, "next_order_no_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
		<tr id="nbill-admin-tr-next-receipt-no">
			<td class="nbill-setting-caption">
				<?php echo NBILL_NEXT_RECEIPT_NO; ?>
			</td>
			<td class="nbill-setting-value">
				<?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_receipt_no', null, true) : $row->next_receipt_no;
				if (nbf_common::nb_strlen($nextno) == 0)
				{
					$nextno = "0001";
				}?>
				<input type="text" name="next_receipt_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_receipt_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_RECEIPT_NO, "next_receipt_no_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-next-payment-no">
			<td class="nbill-setting-caption">
				<?php echo NBILL_NEXT_PAYMENT_NO; ?>
			</td>
			<td class="nbill-setting-value">
				<?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_payment_no', null, true) : $row->next_payment_no;
				if (nbf_common::nb_strlen($nextno) == 0)
				{
					$nextno = "0001";
				}?>
				<input type="text" name="next_payment_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_payment_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_PAYMENT_NO, "next_payment_no_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
        <tr id="nbill-admin-tr-next-credit-no">
			<td class="nbill-setting-caption">
				<?php echo NBILL_NEXT_CREDIT_NO; ?>
			</td>
			<td class="nbill-setting-value">
				<?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_credit_no', null, true) : $row->next_credit_no;
				if (nbf_common::nb_strlen($nextno) == 0)
				{
					$nextno = "CR-0001";
				}?>
				<input type="text" name="next_credit_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_credit_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_CREDIT_NO, "next_credit_no_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-next-quote-no">
            <td class="nbill-setting-caption">
                <?php echo NBILL_NEXT_QUOTE_NO; ?>
            </td>
            <td class="nbill-setting-value">
                <?php $nextno = $use_posted_values ? nbf_common::get_param($_POST,'next_quote_no', null, true) : $row->next_quote_no;
                if (nbf_common::nb_strlen($nextno) == 0)
                {
                    $nextno = "QU-0001";
                }?>
                <input type="text" name="next_quote_no" value="<?php echo $nextno; ?>" class="inputbox" />
                <input type="hidden" name="next_quote_no_orig" value="<?php echo $nextno; ?>" />
                <?php nbf_html::show_static_help(NBILL_INSTR_NEXT_QUOTE_NO, "next_quote_no_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
        </table>
        </div>
        <?php
        $tab_settings->add_tab_content("basic", ob_get_clean());
        ob_start();
        ?>

        <div class="rounded-table">
        <table width="100%" border="0" cellspacing="0" cellpadding="3" class="adminform" id="nbill-admin-table-vendors-advanced">
        <tr>
            <th colspan="2"><?php echo NBILL_VENDOR_DETAILS; ?></th>
        </tr>
        <tr id="nbill-admin-tr-vendor-templates">
            <td colspan="2">
                <div style="padding: 5px; border: solid 1px #999999; background-color:#fff;margin:3px;">
                    <strong><?php echo NBILL_TEMPLATES_TITLE; ?></strong>
                    <p><?php echo sprintf(NBILL_TEMPLATES_INTRO, '<span class="word-breakable inline-block">' . nbf_cms::$interop->nbill_fe_base_path . DIRECTORY_SEPARATOR . "templates" . DIRECTORY_SEPARATOR . '</span>', '<span class="word-breakable inline-block">' . nbf_cms::$interop->nbill_fe_base_path . DIRECTORY_SEPARATOR . "email_templates" . DIRECTORY_SEPARATOR) . '</span>'; ?></p>
                    <table cellpadding="3" cellspacing="0" border="0" id="nbill-admin-table-vendor-templates">
                        <tr id="nbill-admin-tr-invoice-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_INVOICE_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                    $template_list = array();
                                    foreach ($templates as $template)
                                    {
                                        if (substr($template, 0, 7) != 'credit_' && substr($template, 0, 6) != 'quote_' && substr($template, 0, 9) != 'delivery_')
                                        {
                                            $template_list[] = nbf_html::list_option($template, $template);
                                        }
                                    }
                                    echo nbf_html::select_list($template_list, "invoice_template_name", 'id="invoice_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'invoice_template_name', null, true) : $row->invoice_template_name );
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_TEMPLATE, "invoice_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_INVOICE_EMAIL_TEMPLATE;
                                $email_template_list = array();
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 13) != 'credit_email_'
                                        && substr($email_template, 0, 12) != 'order_email_'
                                        && substr($email_template, 0, 14) != 'pending_email_'
                                        && substr($email_template, 0, 12) != 'quote_email_'
                                        && substr($email_template, 0, 20) != 'quote_request_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php echo nbf_html::select_list($email_template_list, "invoice_email_template_name", 'id="invoice_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'invoice_email_template_name', null, true) : $row->invoice_email_template_name ); ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_EMAIL_TEMPLATE, "invoice_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-credit-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_CREDIT_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $template_list = array();
                                foreach ($templates as $template)
                                {
                                    if (substr($template, 0, 8) != 'invoice_' && substr($template, 0, 6) != 'quote_' && substr($template, 0, 9) != 'delivery_')
                                    {
                                        $template_list[] = nbf_html::list_option($template, $template);
                                    }
                                }
                                echo nbf_html::select_list($template_list, "credit_template_name", 'id="credit_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'credit_template_name', null, true) : $row->credit_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_CREDIT_TEMPLATE, "credit_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-credit-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_CREDIT_EMAIL_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $email_template_list = array();
                                $email_template_list[] = nbf_html::list_option("", NBILL_NOT_APPLICABLE);
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 14) != 'invoice_email_'
                                        && substr($email_template, 0, 12) != 'order_email_'
                                        && substr($email_template, 0, 14) != 'pending_email_'
                                        && substr($email_template, 0, 12) != 'quote_email_'
                                        && substr($email_template, 0, 20) != 'quote_request_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                echo nbf_html::select_list($email_template_list, "credit_email_template_name", 'id="credit_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'credit_email_template_name', null, true) : $row->credit_email_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_CREDIT_EMAIL_TEMPLATE, "credit_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <?php /***** EXCLUDE FROM LITE START *****/ ?>
                        <tr id="nbill-admin-tr-quote-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_QUOTE_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $template_list = array();
                                foreach ($templates as $template)
                                {
                                    if (substr($template, 0, 7) != 'credit_' && substr($template, 0, 8) != 'invoice_' && substr($template, 0, 9) != 'delivery_')
                                    {
                                        $template_list[] = nbf_html::list_option($template, $template);
                                    }
                                }
                                echo nbf_html::select_list($template_list, "quote_template_name", 'id="quote_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'quote_template_name', null, true) : $row->quote_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_QUOTE_TEMPLATE, "quote_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-quote-request-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_QRC_EMAIL_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $email_template_list = array();
                                $email_template_list[] = nbf_html::list_option("", NBILL_NOT_APPLICABLE);
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 13) != 'credit_email_'
                                        && substr($email_template, 0, 12) != 'order_email_'
                                        && substr($email_template, 0, 14) != 'pending_email_'
                                        && substr($email_template, 0, 12) != 'quote_email_'
                                        && substr($email_template, 0, 14) != 'invoice_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                echo nbf_html::select_list($email_template_list, "qrc_email_template_name", 'id="qrc_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'qrc_email_template_name', null, true) : $row->qrc_email_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_QRC_EMAIL_TEMPLATE, "qrc_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-quote-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_QUOTE_EMAIL_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $email_template_list = array();
                                $email_template_list[] = nbf_html::list_option("", NBILL_NOT_APPLICABLE);
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 13) != 'credit_email_'
                                        && substr($email_template, 0, 12) != 'order_email_'
                                        && substr($email_template, 0, 14) != 'pending_email_'
                                        && substr($email_template, 0, 14) != 'invoice_email_'
                                        && substr($email_template, 0, 20) != 'quote_request_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                echo nbf_html::select_list($email_template_list, "quote_email_template_name", 'id="quote_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'quote_email_template_name', null, true) : $row->quote_email_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_QUOTE_EMAIL_TEMPLATE, "quote_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-pending-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_PENDING_EMAIL_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $email_template_list = array();
                                $email_template_list[] = nbf_html::list_option("", NBILL_NOT_APPLICABLE);
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 13) != 'credit_email_'
                                        && substr($email_template, 0, 12) != 'order_email_'
                                        && substr($email_template, 0, 14) != 'invoice_email_'
                                        && substr($email_template, 0, 12) != 'quote_email_'
                                        && substr($email_template, 0, 20) != 'quote_request_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                echo nbf_html::select_list($email_template_list, "pending_email_template_name", 'id="pending_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'pending_email_template_name', null, true) : $row->pending_email_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_PENDING_EMAIL_TEMPLATE, "pending_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <tr id="nbill-admin-tr-order-email-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_ORDER_EMAIL_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                $email_template_list = array();
                                $email_template_list[] = nbf_html::list_option("", NBILL_NOT_APPLICABLE);
                                foreach ($email_templates as $email_template)
                                {
                                    if (substr($email_template, 0, 13) != 'credit_email_'
                                        && substr($email_template, 0, 14) != 'invoice_email_'
                                        && substr($email_template, 0, 14) != 'pending_email_'
                                        && substr($email_template, 0, 12) != 'quote_email_'
                                        && substr($email_template, 0, 20) != 'quote_request_email_')
                                    {
                                        $email_template_list[] = nbf_html::list_option($email_template, $email_template);
                                    }
                                }
                                echo nbf_html::select_list($email_template_list, "order_email_template_name", 'id="order_email_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'order_email_template_name', null, true) : $row->order_email_template_name);
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_ORDER_EMAIL_TEMPLATE, "order_email_template_name_help"); ?>
                            </td>
                        </tr>
                        <?php /***** EXCLUDE FROM LITE END *****/ ?>
                        <tr id="nbill-admin-tr-delivery-template">
                            <td class="nbill-setting-caption">
                                <?php echo NBILL_DELIVERY_TEMPLATE; ?>
                            </td>
                            <td class="nbill-setting-value">
                                <?php
                                    $template_list = array();
                                    foreach ($templates as $template)
                                    {
                                        if (substr($template, 0, 7) != 'credit_' && substr($template, 0, 6) != 'quote_' && substr($template, 0, 8) != 'invoice_')
                                        {
                                            $template_list[] = nbf_html::list_option($template, $template);
                                        }
                                    }
                                    echo nbf_html::select_list($template_list, "delivery_template_name", 'id="delivery_template_name" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'delivery_template_name', null, true) : $row->delivery_template_name );
                                ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_DELIVERY_TEMPLATE, "invoice_delivery_name_help"); ?>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        <tr id="nbill-admin-tr-add-remittance">
            <td class="nbill-setting-caption">
                <?php echo NBILL_ADD_REMITTANCE; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_html::yes_or_no_options("show_remittance", "", $use_posted_values ? nbf_common::get_param($_POST, 'show_remittance', null, true) : $row->show_remittance); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_ADD_REMITTANCE, "show_remittance_help"); ?>
            </td>
        </tr>
        <tr id="nbill-admin-tr-add-paylink">
            <td class="nbill-setting-caption">
                <?php echo NBILL_ADD_PAYLINK; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_html::yes_or_no_options("show_paylink", "", $use_posted_values ? nbf_common::get_param($_POST, 'show_paylink', null, true) : $row->show_paylink); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_ADD_PAYLINK, "show_paylink_help"); ?>
            </td>
        </tr>
        <tr id="nbill-admin-tr-paper-size">
            <td class="nbill-setting-caption">
                <?php echo NBILL_PAPER_SIZE; ?>
            </td>
            <td class="nbill-setting-value">
                <input type="text" name="paper_size" value="<?php echo $use_posted_values ? nbf_common::get_param($_POST,'paper_size', null, true) : $row->paper_size; ?>" class="inputbox" style="width:160px" />
                <?php nbf_html::show_static_help(NBILL_INSTR_PAPER_SIZE, "paper_size_help"); ?>
            </td>
        </tr>
        <tr id="nbill-admin-tr-auto-create-income">
            <td class="nbill-setting-caption">
                <?php echo NBILL_AUTO_CREATE_INCOME; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_html::yes_or_no_options("auto_create_income", "", $use_posted_values ? nbf_common::get_param($_POST, 'auto_create_income', null, true) : $row->auto_create_income); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_AUTO_CREATE_INCOME, "auto_create_income_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-default-quote-intro">
            <td class="nbill-setting-caption">
                <?php echo NBILL_QUOTE_DEFAULT_INTRO; ?>
            </td>
            <td class="nbill-setting-value">
                <?php
                echo nbf_cms::$interop->render_editor("quote_default_intro", "editor_quote_intro", $use_posted_values ? nbf_common::get_param($_POST,'quote_default_intro', null, true) : $row->quote_default_intro); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_QUOTE_DEFAULT_INTRO, "quote_default_intro_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
		<tr id="nbill-admin-tr-default-pay-instr">
			<td class="nbill-setting-caption">
				<?php echo NBILL_DEFAULT_PAYMENT_INSTR; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_cms::$interop->render_editor("payment_instructions", "editor1", $use_posted_values ? nbf_common::get_param($_POST,'payment_instructions', null, true) : $row->payment_instructions); ?>
				<?php nbf_html::show_static_help(NBILL_INSTR_DEFAULT_PAYMENT_INSTR, "payment_instructions_help"); ?>
			</td>
		</tr>
        <tr id="nbill-admin-tr-invoice-pay-instr">
            <td class="nbill-setting-caption">
                <?php echo NBILL_INVOICE_PAY_INST; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_cms::$interop->render_editor("invoice_offline_pay_inst", "editor_invoice_payinst", $use_posted_values ? nbf_common::get_param($_POST,'invoice_offline_pay_inst', null, true) : $row->invoice_offline_pay_inst); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_PAY_INST, "invoice_offline_pay_inst_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-quote-pay-instr">
            <td class="nbill-setting-caption">
                <?php echo NBILL_QUOTE_PAY_INST; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_cms::$interop->render_editor("quote_offline_pay_inst", "editor_quote_payinst", $use_posted_values ? nbf_common::get_param($_POST,'quote_offline_pay_inst', null, true) : $row->quote_offline_pay_inst); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_QUOTE_PAY_INST, "quote_payinst_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
		<tr id="nbill-admin-tr-default-small-print">
			<td class="nbill-setting-caption">
				<?php echo NBILL_DEFAULT_SMALL_PRINT; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_cms::$interop->render_editor("small_print", "editor2", $use_posted_values ? nbf_common::get_param($_POST,'small_print', null, true) : $row->small_print); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_DEFAULT_SMALL_PRINT, "small_print_help"); ?>
			</td>
		</tr>
		<tr id="nbill-admin-tr-credit-small-print">
			<td class="nbill-setting-caption">
				<?php echo NBILL_CREDIT_SMALL_PRINT; ?>
			</td>
			<td class="nbill-setting-value">
                <?php echo nbf_cms::$interop->render_editor("credit_small_print", "editor3", $use_posted_values ? nbf_common::get_param($_POST,'credit_small_print', null, true) : $row->credit_small_print); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_SMALL_PRINT_CR, "credit_small_print_help"); ?>
			</td>
		</tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-quote-small-print">
            <td class="nbill-setting-caption">
                <?php echo NBILL_QUOTE_SMALL_PRINT; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_cms::$interop->render_editor("quote_small_print", "editor4", $use_posted_values ? nbf_common::get_param($_POST,'quote_small_print', null, true) : $row->quote_small_print); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_SMALL_PRINT_QU, "quote_small_print_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE END *****/ ?>
        <tr id="nbill-admin-tr-delivery-small-print">
            <td class="nbill-setting-caption">
                <?php echo NBILL_DELIVERY_SMALL_PRINT; ?>
            </td>
            <td class="nbill-setting-value">
                <?php echo nbf_cms::$interop->render_editor("delivery_small_print", "editor5", $use_posted_values ? nbf_common::get_param($_POST,'delivery_small_print', null, true) : $row->delivery_small_print); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_INVOICE_SMALL_PRINT_DE, "delivery_small_print_help"); ?>
            </td>
        </tr>
        <?php /***** EXCLUDE FROM LITE START *****/ ?>
        <tr id="nbill-admin-tr-suppress-receipt-nos">
			<td class="nbill-setting-caption">
				<?php echo NBILL_SUPPRESS_RECEIPT_NOS; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_html::yes_or_no_options("suppress_receipt_nos", "", $use_posted_values ? nbf_common::get_param($_POST, 'suppress_receipt_nos', null, true) : $row->suppress_receipt_nos); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_SUPPRESS_RECEIPT_NOS, "suppress_receipt_nos_help"); ?>
			</td>
		</tr>
        <tr id="nbill-admin-tr-suppress-payment-nos">
			<td class="nbill-setting-caption">
				<?php echo NBILL_SUPPRESS_PAYMENT_NOS; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_html::yes_or_no_options("suppress_payment_nos", "", $use_posted_values ? nbf_common::get_param($_POST, 'suppress_payment_nos', null, true) : $row->suppress_payment_nos); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_SUPPRESS_PAYMENT_NOS, "suppress_payment_nos_help"); ?>
			</td>
		</tr>
        <tr id="nbill-admin-tr-suppress-generation-buttons">
			<td class="nbill-setting-caption">
				<?php echo NBILL_SUPPRESS_GENERATION_BUTTONS; ?>
			</td>
			<td class="nbill-setting-value">
				<?php echo nbf_html::yes_or_no_options("suppress_generation_buttons", "", $use_posted_values ? nbf_common::get_param($_POST, 'suppress_generation_buttons', null, true) : $row->suppress_generation_buttons); ?>
                <?php nbf_html::show_static_help(NBILL_INSTR_SUPPRESS_GENERATION_BUTTONS, "suppress_generation_buttons_help"); ?>
			</td>
		</tr>
		<?php /***** EXCLUDE FROM LITE END *****/ ?>
        </table>
        </div>

		<br />
        <?php
        /***** EXCLUDE FROM LITE START *****/
        if (!nbf_cms::$interop->demo_mode)
        { ?>
            <div class="rounded-table">
                <table width="100%" border="0" cellspacing="0" cellpadding="5" class="adminform" id="nbill-admin-table-master-db">
		        <tr><th colspan="2"><?php echo NBILL_MASTER_DB_SECTION; ?></th></tr>
		        <tr><th colspan="2"><?php echo NBILL_MASTER_DB_INTRO; ?></th></tr>
		        <?php
		        $use_master_db = $use_posted_values ? nbf_common::get_param($_POST, 'use_master_db', null, true) : $row->use_master_db;
                $master_host = $use_posted_values ? nbf_common::get_param($_POST, 'master_host', null, true) : $row->master_host;
		        $master_username = $use_posted_values ? nbf_common::get_param($_POST, 'master_username', null, true) : $row->master_username;
		        $master_password = $use_posted_values ? nbf_common::get_param($_POST, 'master_password', null, true) : $row->master_password;
		        $master_dbname = $use_posted_values ? nbf_common::get_param($_POST, 'master_dbname', null, true) : $row->master_dbname;
		        $master_table_prefix = $use_posted_values ? nbf_common::get_param($_POST, 'master_table_prefix', null, true) : $row->master_table_prefix;
		        $master_vendor_id = $use_posted_values ? nbf_common::get_param($_POST, 'master_vendor_id', null, true) : $row->master_vendor_id;
		        ?>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_USE_MASTER_DB; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <?php echo nbf_html::yes_or_no_options("use_master_db", "", $use_master_db); ?>
                        <?php nbf_html::show_static_help(NBILL_INSTR_USE_MASTER_DB, "use_master_db_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_MASTER_DB_HOST; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <input type="text" name="master_host" id="master_host" value="<?php echo $master_host; ?>" class="inputbox" style="width:160px" />
                        <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_DB_HOST, "master_host_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_MASTER_USERNAME; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <input type="text" name="master_username" id="master_username" value="<?php echo $master_username; ?>" class="inputbox" style="width:160px" />
                        <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_USERNAME, "master_username_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_MASTER_PASSWORD; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <input type="text" name="master_password" id="master_password" value="<?php echo $master_password; ?>" class="inputbox" style="width:160px" />
                        <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_PASSWORD, "master_password_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_MASTER_DB_NAME; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <input type="text" name="master_dbname" id="master_dbname" value="<?php echo $master_dbname; ?>" class="inputbox" style="width:160px" />
                        <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_DB_NAME, "master_dbname_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <td class="nbill-setting-caption">
				        <?php echo NBILL_MASTER_TABLE_PREFIX; ?>
			        </td>
			        <td class="nbill-setting-value">
				        <input type="text" name="master_table_prefix" id="master_table_prefix" value="<?php echo $master_table_prefix; ?>" class="inputbox" style="width:160px" />
                        <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_TABLE_PREFIX, "master_table_prefix_help"); ?>
			        </td>
		        </tr>
		        <tr>
			        <?php
			        if ($use_master_db || $test_connection)
			        {
				        if (!$master_connect)
				        {
					        //Could not connect to master database
					        ?>
					        <td>&nbsp;</td><td><span style="color:#ff0000;"><strong><?php echo NBILL_MASTER_DB_CANNOT_CONNECT; ?></strong></span></td>
					        </tr><tr><td>&nbsp;</td><td>
						        <input type="button" name="test_connection" id="test_connection" onclick="nbill_submit_task('test_connection')" class="button btn" value="<?php echo NBILL_MASTER_DB_TEST; ?>" />
					        </td>
					        <?php
				        }
				        else
				        {
					        if ($test_connection)
					        {?>
						        </tr><tr>
						        <td>&nbsp;<script type="text/javascript">window.setTimeout(function(){window.location.hash='nbill_master_db';}, 750);</script></td>
						        <td><span style="color:#009900;"><strong><?php echo NBILL_MASTER_DB_TEST_SUCCESS; ?></strong></span></td>
						        </tr>
						        <tr>
						        <td>&nbsp;</td><td><input type="button" name="test_connection" id="test_connection" onclick="nbill_submit_task('test_connection');" class="button btn" value="<?php echo NBILL_MASTER_DB_TEST; ?>" /></td>
						        </tr>
						        <tr>
					        <?php
					        }
					        //Show master vendor list, followed by sync button
					        ?>
					        <td><?php echo NBILL_MASTER_VENDOR; ?></td><td>
					            <?php
					            $master_vendor_list = array();
					            foreach ($master_vendors as $master_vendor)
					            {
						            $master_vendor_list[] = nbf_html::list_option($master_vendor->id, $master_vendor->vendor_name);
					            }
					            echo nbf_html::select_list($master_vendor_list, "master_vendor_id", 'id="master_vendor_id" class="inputbox"', $use_posted_values ? nbf_common::get_param($_POST,'master_vendor_id', null, true) : $row->master_vendor_id );
					            ?>
                                <?php nbf_html::show_static_help(NBILL_INSTR_MASTER_VENDOR, "master_vendor_id_help"); ?>
					        </td>
					        </tr>
					        <tr>
					        <?php if (!$test_connection)
					        {?>
						        <td>&nbsp;</td>
						        <td>
							        <input type="button" name="synchronise" id="synchronise" onclick="sync_click();" value="<?php echo NBILL_SYNCHRONISE; ?>" />
                                    <?php nbf_html::show_static_help(NBILL_INSTR_SYNCHRONISE, "synchronise_help"); ?>
						        </td>
					        <?php
					        }
				        }
			        }
			        else
			        {
				        //Show 'test connection' button ?>
				        <td>&nbsp;</td><td><input type="button" name="test_connection" id="test_connection" onclick="nbill_submit_task('test_connection');" class="button btn" value="<?php echo NBILL_MASTER_DB_TEST; ?>" /></td>
				        <?php
			        }
		        ?>
		        </tr>
		        </table>
            </div>
        <?php }
        /***** EXCLUDE FROM LITE END *****/

        $tab_settings->add_tab_content("advanced", ob_get_clean());
        $tab_settings->end_tab_group();
        ?>
        <a name="nbill_master_db" />
		<input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
		<input type="hidden" name="action" value="vendors" />
		<input type="hidden" name="task" value="edit" />
		<input type="hidden" name="box_checked" value="0" />
		<input type="hidden" name="hidemainmenu" value="0">

		</form>
		<?php
	}
}