<?php
/**
* Entry point into both front-end and back-end (done this way for cross compatability with older versions of Joomla/Mambo)
* @version 3
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

/***** EXCLUDE FROM GPL START *****/if (function_exists("ioncube_loader_version") || function_exists("ioncube_loader_iversion")) {/***** EXCLUDE FROM GPL END *****/
    if (file_exists(realpath(dirname(__FILE__)) . "/site.nbill.php")) {
        include(realpath(dirname(__FILE__)) . "/site.nbill.php");
    } else if (file_exists(realpath(dirname(__FILE__)) . "/admin.nbill.php")) {
        include(realpath(dirname(__FILE__)) . "/admin.nbill.php");
    } else {
        die("Sorry, the component entry point file could not be found!");
    }
/***** EXCLUDE FROM GPL START *****/} else {
    function temp_error_handler($errno, $errstr){}
    set_error_handler("temp_error_handler");

    if (file_exists(realpath(dirname(__FILE__)) . "/admin.nbill.php")) {
        ?>
        An ionCube loader is required to use this billing system. There is no functioning ionCube loader present (if it was working before but suddenly stopped, it could be due to your php.ini file being changed, or your hosting company upgrading PHP to a new version but failing to update the loader). Please refer to the prerequisites page in the documentation for assistance, or run the ionCube loader wizard (which comes with the <a target="_blank" href="http://www.ioncube.com/loaders.php">ionCube loaders</a>).
        <?php
    } else {
        ?>
        An ionCube loader is required to use this billing system. There is no functioning ionCube loader present. Please contact a system administrator for assistance.
        <?php
    }
}/***** EXCLUDE FROM GPL END *****/