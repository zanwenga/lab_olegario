<?php
/**
* Default HTML output template for quote processing pages
* @version 1
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nbill_fe_quotes
{
    /**
    * Show list of quotes
    * @param array $rows
    * @param string $date_format
    */
    public static function show_quotes($rows, $first_description, $date_format)
    {
        ?>
        <div class="nbill-quote-list">
            <p><?php echo NBILL_FE_QUOTE_INTRO; ?></p>
            <?php
            self::_show_filters();
            ?>
            <table width="100%" class="contentpane category" id="nbill-quote-list-table" cellpadding="3" cellspacing="1" border="0" style="margin-top:5px;">
                <tr class="jlist-table nbill_list_tr_headings nbill_quotes">
                    <?php
                    self::render_column_header($rows, "quote_no", NBILL_FE_QUOTE_NUMBER, "white-space:nowrap;");
                    self::render_column_header($rows, "quote_date", NBILL_FE_QUOTE_DATE);
                    self::render_column_header($rows, "quote_first_item", NBILL_FE_QUOTE_FIRST_ITEM);
                    self::render_column_header($rows, "quote_net", NBILL_FE_QUOTE_TOTAL_NET, "text-align:right;white-space:nowrap;");
                    self::render_column_header($rows, "quote_gross", NBILL_FE_QUOTE_TOTAL_GROSS, "text-align:right;white-space:nowrap;");
                    self::render_column_header($rows, "quote_status", NBILL_FE_QUOTE_STATUS);
                    ?>
                </tr>
                <?php
                $rownumber = 2;
                for ($i=0, $n=count($rows); $i < $n; $i++)
                {
                    $row = &$rows[$i];
                    $rownumber = $rownumber == 1 ? 2 : 1;
                    $link = htmlentities(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . "&action=quotes&task=print&cid=$row->id" . nbf_cms::$interop->site_page_suffix);
                    $pdf_link = htmlentities(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . "&action=quotes&task=pdf&cid=$row->id" . nbf_cms::$interop->site_page_suffix);
                    ?>
                    <tr class="sectiontableentry<?php echo $rownumber; ?> cat-list-row<?php echo $rownumber; ?> nbill_list_tr_value" id="tr_quote_<?php echo $row->id; ?>">
                        <td>
                        <span style="white-space:nowrap;">
                        <a href="<?php echo nbf_cms::$interop->site_page_prefix; ?>&action=quotes&task=edit&cid=<?php echo intval($row->id); ?><?php echo nbf_cms::$interop->site_page_suffix; ?>" title="<?php echo NBILL_QUOTE_VIEW_DETAILS; ?>"><?php echo $row->document_no; ?></a>&nbsp;
                        <?php
                        if ($row->status != 'AA' && $row->status != 'BB')
                        {
                            if (nbf_frontend::get_display_option("html_preview_quote")) { ?>
                                <a target="_blank" href="<?php echo $link; ?>" onclick="window.open('<?php echo $link; ?>', '<?php echo uniqid(); ?>', 'width=700,height=500,resizable=yes,scrollbars=yes,toolbar=yes,location=no,directories=no,status=yes,menubar=yes,copyhistory=no');return false;"><img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=preview.gif" . nbf_cms::$interop->site_page_suffix; ?>" alt="<?php echo NBILL_HTML_QUOTE; ?>" border="0" /></a>
                                <?php
                            }
                            if (nbf_frontend::get_display_option("pdf_quote")) {
                                if (nbf_common::pdf_writer_available())
                                { ?>
                                    <a target="_blank" title="<?php echo NBILL_PDF_QUOTE; ?>" href="<?php echo $pdf_link; ?>" onclick="window.open('<?php echo $pdf_link; ?>', '<?php echo uniqid(); ?>', 'width=700,height=500,resizable=yes,scrollbars=yes,toolbar=yes,location=no,directories=no,status=yes,menubar=yes,copyhistory=no');return false;"><img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=pdf.png" . nbf_cms::$interop->site_page_suffix; ?>" alt="<?php echo NBILL_PDF_QUOTE; ?>" title="<?php echo NBILL_PDF_QUOTE; ?>" border="0" /></a>
                                <?php }
                            }
                        } ?>
                        </span>
                        </td>
                        <?php
                        if (is_callable(array("nbill_quote_after_quote_no", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_no", 'render_row'), $row);
                        }
                        $display_option = nbf_frontend::get_display_option("quote_date");
                        if ($display_option) { ?>
                            <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo nbf_common::nb_date($date_format, $row->document_date);?></td>
                            <?php
                        }
                        if (is_callable(array("nbill_quote_after_quote_date", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_date", 'render_row'), $row);
                        }
                        $display_option = nbf_frontend::get_display_option("quote_first_item");
                        if ($display_option)
                        {
                            $first_desc = "";
                            $section_found = false;
                            foreach ($first_description as $descriptions)
                            {
                                if ($descriptions->document_id == $row->document_id)
                                {
                                    if ($descriptions->section_name)
                                    {
                                        $first_desc = $descriptions->section_name;
                                        $section_found = true;
                                        break;
                                    }
                                }
                            }
                            if (!$section_found)
                            {
                                foreach ($first_description as $descriptions)
                                {
                                    if ($descriptions->document_id == $row->document_id)
                                    {
                                        $first_desc = $descriptions->product_description;
                                        break;
                                    }
                                }
                            }
                            ?>
                            <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $first_desc; ?></td>
                            <?php
                        }
                        if (is_callable(array("nbill_quote_after_quote_first_item", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_first_item", 'render_row'), $row);
                        }
                        $display_option = nbf_frontend::get_display_option("quote_net");
                        if ($display_option) { ?>
                            <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>" style="white-space:nowrap;text-align:right;"><?php echo $row->total_net;?></td>
                        <?php }
                        if (is_callable(array("nbill_quote_after_quote_net", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_net", 'render_row'), $row);
                        }
                        $display_option = nbf_frontend::get_display_option("quote_gross");
                        if ($display_option) { ?>
                            <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>" style="white-space:nowrap;text-align:right;"><?php echo $row->total_gross;?></td>
                        <?php }
                        if (is_callable(array("nbill_quote_after_quote_gross", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_gross", 'render_row'), $row);
                        }
                        $display_option = nbf_frontend::get_display_option("quote_status");
                        if ($display_option) { ?>
                            <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo @constant($row->quote_status); ?></td>
                        <?php }
                        if (is_callable(array("nbill_quote_after_quote_status", 'render_row')))
                        {
                            call_user_func(array("nbill_quote_after_quote_status", 'render_row'), $row);
                        } ?>
                    </tr>
                <?php } ?>
            </table>
        </div>
        <?php
    }

    public static function render_column_header($rows, $column_name, $header_text, $custom_style = "")
    {
        $display_option = nbf_frontend::get_display_option($column_name);
        if ($display_option)
        {?>
            <th class="sectiontableheader responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"<?php echo $custom_style ? ' style="' . $custom_style . '"' : ''; ?>><?php echo $header_text; ?></th>
        <?php }
        if (file_exists(dirname(__FILE__) . "/custom_columns/after_$column_name.php"))
        {
            include_once(dirname(__FILE__) . "/custom_columns/after_$column_name.php");
            if (is_callable(array("nbill_quote_after_$column_name", 'render_header')))
            {
                call_user_func(array("nbill_quote_after_$column_name", 'render_header'), $rows);
            }
        }
    }

    public static function edit_quote($row, $items, $date_format)
    {
        nbf_common::load_language("template.common");
        nbf_common::load_language("template.qu");
        ?>
        <script type="text/javascript">
            function corre_click()
            {
                var corre = document.getElementById('nbill_quote_correspondence');
                var btn = document.getElementById('nbill_quote_correspondence_button');
                if (corre.style.display == 'none')
                {
                    corre.style.display = '';
                    btn.innerHTML = '- <?php echo NBILL_FE_QUOTE_HIDE_CORRE; ?>';
                }
                else
                {
                    corre.style.display = 'none';
                    btn.innerHTML = '+ <?php echo NBILL_FE_QUOTE_SHOW_CORRE; ?>';
                }
            }
        </script>

        <h2 class="componentheading"><?php echo sprintf(NBILL_FE_QUOTE_TITLE, $row->document_no, nbf_common::nb_date($date_format, $row->document_date), @constant($row->quote_status)); ?></h2>

        <?php
        if ($row->status != 'AA' && $row->status != 'BB' && count($items) > 0)
        {
            if ($row->quote_intro)
            {
                echo '<div class="nbill_quote_intro">' . $row->quote_intro . '</div>';
            }

            //Show the items that make up the quote
            self::show_items($row, $items);

            //Show small print
            if (nbf_common::nb_strlen($row->small_print) > 0)
            { ?>
                <div id="nbill_quote_small_print">
                    <?php echo $row->small_print; ?>
                </div>
            <?php
            }
        }
        else
        {
            if ($row->status == 'AA' || $row->status == 'BB')
            {
                ?><br /><p><strong><?php echo $row->status == 'AA' ? NBILL_QUOTE_NOT_YET_AVAILABLE : NBILL_QUOTE_NOT_YET_AVAILABLE_ON_HOLD; ?></strong></p><br /><?php
            }
        }

        //Show correspondence and allow reply
        ?>
        <a name="nbill_quote_reply"></a>
        <div id="nbill_quote_correspondence_container" style="margin-bottom:7px;">
            <script type="text/javascript">
            document.write('<div id="nbill_quote_corre_button_container"><a href="#" id="nbill_quote_correspondence_button" onclick="corre_click();this.blur();return false;">+ <?php echo NBILL_FE_QUOTE_SHOW_CORRE; ?></a></div>');
            </script>
            <div id="nbill_quote_correspondence">
                <?php echo strlen(trim(strip_tags($row->correspondence))) > 0 ? $row->correspondence : NBILL_FE_QUOTE_NO_CORRE; ?>
            </div>
            <?php if (!nbf_common::get_param($_REQUEST, 'show_corre'))
            { ?>
            <script type="text/javascript">
                document.getElementById('nbill_quote_correspondence').style.display='none';
            </script>
            <?php } ?>
        </div>
        <div id="nbill_quote_reply_email">
            <p><?php echo NBILL_FE_QUOTE_REPLY_INTRO; ?></p>
            <?php
            self::show_email_form($row->email_address, sprintf(NBILL_FE_QUOTE_EMAIL_DEFAULT_SUBJECT, $row->document_no));
            ?>
        </div>
        <?php
    }

    public static function show_items($row, $items)
    {
        include_once(nbf_cms::$interop->nbill_admin_base_path . "/framework/classes/nbill.process.discount.class.php");
        $config = nBillConfigurationService::getInstance()->getConfig();
        $precision = $config->precision_currency_grand_total;

        //Check whether there are any shipping amounts
        $hide_unit_price = true;
        $hide_quantity = true;
        $hide_discount = true;
        $hide_net_price = true;
        $hide_tax = true;
        $hide_shipping = true;
        $hide_shipping_tax = true;
        foreach ($items as &$document_item)
        {
            if ($document_item->no_of_units > 1)
            {
                $hide_unit_price = false;
                $hide_quantity = false;
            }
            if ($document_item->discount_amount != 0)
            {
                $hide_discount = false;
            }
            if ($document_item->net_price_for_item != $document_item->gross_price_for_item)
            {
                $hide_net_price = false;
            }
            if ($document_item->tax_for_item != 0)
            {
                $hide_tax = false;
            }
            if ($document_item->shipping_for_item != 0)
            {
                $hide_shipping = false;
            }
            if ($document_item->tax_for_shipping != 0)
            {
                $hide_shipping_tax = false;
            }
        }
        ?>
        <script type="text/javascript">
        function format_currency(number, dec_places){
        //(c) Copyright 2008, Russell Walker, Netshine Software Limited. www.netshinesoftware.com
        if (typeof dec_places === 'undefined') {
            dec_places=2;
        }
        var new_number='';var i=0;var sign="";number=number.toString();number=number.replace(/^\s+|\s+$/g,'');if(number.charCodeAt(0)==45){sign='-';number=number.substr(1).replace(/^\s+|\s+$/g,'')}dec_places=dec_places*1;dec_point_pos=number.lastIndexOf(".");if(dec_point_pos==0){number="0"+number;dec_point_pos=1}if(dec_point_pos==-1||dec_point_pos==number.length-1){if(dec_places>0){new_number=number+".";for(i=0;i<dec_places;i++){new_number+="0"}if(new_number==0){sign=""}return sign+new_number}else{return sign+number}}var existing_places=(number.length-1)-dec_point_pos;if(existing_places==dec_places){return sign+number}if(existing_places<dec_places){new_number=number;for(i=existing_places;i<dec_places;i++){new_number+="0"}if(new_number==0){sign=""}return sign+new_number}var end_pos=(dec_point_pos*1)+dec_places;var round_up=false;if((number.charAt(end_pos+1)*1)>4){round_up=true}var digit_array=new Array();for(i=0;i<=end_pos;i++){digit_array[i]=number.charAt(i)}for(i=digit_array.length-1;i>=0;i--){if(digit_array[i]=="."){continue}if(round_up){digit_array[i]++;if(digit_array[i]<10){break}}else{break}}for(i=0;i<=end_pos;i++){if(digit_array[i]=="."||digit_array[i]<10||i==0){new_number+=digit_array[i]}else{new_number+="0"}}if(dec_places==0){new_number=new_number.replace(".","")}if(new_number==0){sign=""}return sign+new_number}

        function item_accept_click(item_id, elem)
        {
            chk_accept=document.getElementById('nbill_quote_accept_' + item_id);
            if(chk_accept.value=='1') {
                chk_accept.value='0';
                elem.title='<?php echo NBILL_QUOTE_CLICK_TO_ACCEPT; ?>';
                var img=document.getElementById('nbill_quote_accept_img_' + item_id);
                img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/cross.png" . nbf_cms::$interop->site_page_suffix; ?>';
                img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_NO ?>';
            } else {
                chk_accept.value='1';
                elem.title='<?php echo NBILL_QUOTE_CLICK_TO_REJECT; ?>';
                var img=document.getElementById('nbill_quote_accept_img_' + item_id);
                img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/tick.png" . nbf_cms::$interop->site_page_suffix; ?>';
                img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_YES; ?>';
            }

            <?php
            //Maintain atomic sections and update accepted totals
            $sections = array();
            $section = array();
            foreach ($items as $item) {
                $section[] = $item;
                if ($item->section_name) {
                    $sections[$item->id] = $section;
                    $section = array();
                }
            }
            ?>
            //Make sure atomic sections maintain the same status for all items
            var section_accepted_count = 0;
            var section_rejected_count = 0;
            var refresh_item = null;
            var refresh_img = null;
            switch (item_id) {
                <?php
                $refresh_icons = false;
                foreach ($sections as $key=>$value) {
                    $section_break_item = end($value);
                    reset($value);
                    $siblings = $value; //Make a shallow copy so the array pointers can remain intact
                    foreach ($value as $section_item) {
                        ?>
                        case <?php echo $section_item->id; ?>:
                            <?php
                            if ($section_break_item->section_quote_atomic) {
                                foreach ($siblings as $sibling_item) {
                                    ?>
                                    document.getElementById('nbill_quote_accept_<?php echo $sibling_item->id; ?>').value = chk_accept.value;
                                    <?php
                                    $refresh_icons = true;
                                }
                                if ($refresh_icons) {
                                    foreach ($siblings as $sibling_item) {
                                        ?>
                                        refresh_item = document.getElementById('nbill_quote_accept_<?php echo $sibling_item->id; ?>');
                                        refresh_img=document.getElementById('nbill_quote_accept_img_<?php echo $sibling_item->id; ?>');
                                        if (refresh_item.value == '0') {
                                            refresh_item.title='<?php echo NBILL_QUOTE_CLICK_TO_ACCEPT; ?>';
                                            refresh_img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/cross.png" . nbf_cms::$interop->site_page_suffix; ?>';
                                            refresh_img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_NO ?>';
                                        } else {
                                            refresh_item.title='<?php echo NBILL_QUOTE_CLICK_TO_REJECT; ?>';
                                            refresh_img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/tick.png" . nbf_cms::$interop->site_page_suffix; ?>';
                                            refresh_img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_YES; ?>';
                                        }
                                        <?php
                                    }
                                }
                            }

                            //If there is a section discount mark it as accepted/rejected/partial
                            foreach ($siblings as $sibling_item) {
                                ?>
                                if (document.getElementById('nbill_quote_accept_<?php echo $sibling_item->id; ?>').value != 0) {
                                    section_accepted_count++;
                                } else {
                                    section_rejected_count++;
                                }
                                <?php
                            }
                            $section_break_item = end($value);
                            reset($value);
                            $siblings = $value; //Make a shallow copy so the array pointers can remain intact
                            if ($section_break_item->section_discount_percent != 0) {
                                ?>
                                refresh_img = document.getElementById('nbill_section_discount_selector_<?php echo $section_break_item->id; ?>');
                                if (section_accepted_count > 0 && section_rejected_count == 0) {
                                    //Discount accepted
                                    refresh_img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/tick.png" . nbf_cms::$interop->site_page_suffix; ?>';
                                    refresh_img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_YES; ?>';
                                    refresh_img.title='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_YES; ?>';
                                } else if (section_accepted_count == 0) {
                                    //Discount rejected
                                    refresh_img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/cross.png" . nbf_cms::$interop->site_page_suffix; ?>';
                                    refresh_img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_NO ?>';
                                    refresh_img.title='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_NO ?>';
                                } else {
                                    //Discount partially accepted
                                    refresh_img.src='<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/partial.png" . nbf_cms::$interop->site_page_suffix; ?>';
                                    refresh_img.alt='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_PARTIAL; ?>';
                                    refresh_img.title='<?php echo NBILL_QUOTE_ITEM_ACCEPTED_PARTIAL; ?>';
                                }
                                <?php
                            }
                            ?>
                            break;
                            <?php
                    }
                }
                ?>
                default:
                    break;
            }

            var section_accepted_item_net = 0;
            var section_accepted_item_tax = 0;
            var section_accepted_item_shipping = 0;
            var section_accepted_item_shipping_tax = 0;
            var section_accepted_item_gross = 0;
            var accepted_item_net = new Object();
            var accepted_item_tax = new Object();
            var accepted_item_shipping = new Object();
            var accepted_item_shipping_tax = new Object();
            var accepted_item_gross = new Object();

            <?php
            function quote_item_copy($quote_items) {
                $newItems = array();
                foreach($quote_items as $key => $value) {
                    if(is_array($value)) $newItems[$key] = quote_item_copy($value);
                    elseif(is_object($value)) $newItems[$key] = clone $value;
                    else $newItems[$key] = $value;
                }
                return $newItems;
            }

            $accepted_items = quote_item_copy($items);
            nbf_discount::apply_section_discounts($accepted_items);
            foreach ($accepted_items as $accepted_item) {
                $accepted_summary_freq_code = defined("NBILL_PER_" . nbf_common::nb_strtoupper($accepted_item->quote_pay_freq)) ? nbf_common::nb_strtoupper($accepted_item->quote_pay_freq) : 'AA';
                ?>
                if (!accepted_item_net['<?php echo $accepted_summary_freq_code; ?>']){accepted_item_net['<?php echo $accepted_summary_freq_code; ?>'] = 0;}
                if (!accepted_item_tax['<?php echo $accepted_summary_freq_code; ?>']){accepted_item_tax['<?php echo $accepted_summary_freq_code; ?>'] = 0;}
                if (!accepted_item_shipping['<?php echo $accepted_summary_freq_code; ?>']){accepted_item_shipping['<?php echo $accepted_summary_freq_code; ?>'] = 0;}
                if (!accepted_item_shipping_tax['<?php echo $accepted_summary_freq_code; ?>']){accepted_item_shipping_tax['<?php echo $accepted_summary_freq_code; ?>'] = 0;}
                if (!accepted_item_gross['<?php echo $accepted_summary_freq_code; ?>']){accepted_item_gross['<?php echo $accepted_summary_freq_code; ?>'] = 0;}
                if (document.getElementById('nbill_quote_accept_<?php echo $accepted_item->id; ?>').value == '1') {
                    accepted_item_net['<?php echo $accepted_summary_freq_code; ?>'] += <?php echo $accepted_item->net_price_for_item; ?>;
                    accepted_item_tax['<?php echo $accepted_summary_freq_code; ?>'] += <?php echo $accepted_item->tax_for_item; ?>;
                    accepted_item_shipping['<?php echo $accepted_summary_freq_code; ?>'] += <?php echo $accepted_item->shipping_for_item; ?>;
                    accepted_item_shipping_tax['<?php echo $accepted_summary_freq_code; ?>'] += <?php echo $accepted_item->tax_for_shipping; ?>;
                    accepted_item_gross['<?php echo $accepted_summary_freq_code; ?>'] += <?php echo $accepted_item->gross_price_for_item; ?>;
                    section_accepted_item_net += <?php echo $accepted_item->net_price_for_item; ?>;
                    section_accepted_item_tax += <?php echo $accepted_item->tax_for_item; ?>;
                    section_accepted_item_shipping += <?php echo $accepted_item->shipping_for_item; ?>;
                    section_accepted_item_shipping_tax += <?php echo $accepted_item->tax_for_shipping; ?>;
                    section_accepted_item_gross += <?php echo $accepted_item->gross_price_for_item; ?>;
                }
                <?php
                if (strlen($accepted_item->section_name) > 0) {
                    $currency_pre = '';
                    $currency_post = '';
                    $dummy_currency = nbf_common::convertValueToCurrencyObject(100, $row->currency, true);
                    if (is_numeric(substr($dummy_currency->format(), 0, 1))) {
                        $currency_post = ' ' . $row->currency_symbol;
                    } else {
                        $currency_pre = $row->currency_symbol;
                    }
                    $precision = $dummy_currency->precision;
                    ?>
                    if (document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_net')) {
                        document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_net').innerHTML = '<?php echo $currency_pre; ?>' + format_currency(section_accepted_item_net, <?php echo $precision; ?>) + '<?php echo $currency_post; ?>';
                    }
                    if (document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_tax')) {
                        document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_tax').innerHTML = '<?php echo $currency_pre; ?>' + format_currency(section_accepted_item_tax, <?php echo $precision; ?>) + '<?php echo $currency_post; ?>';
                    }
                    if (document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_shipping')) {
                        document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_shipping').innerHTML = '<?php echo $currency_pre; ?>' + format_currency(section_accepted_item_shipping, <?php echo $precision; ?>) + '<?php echo $currency_post; ?>';
                    }
                    if (document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_shipping_tax')) {
                        document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_shipping_tax').innerHTML = '<?php echo $currency_pre; ?>' + format_currency(section_accepted_item_shipping_tax, <?php echo $precision; ?>) + '<?php echo $currency_post; ?>';
                    }
                    document.getElementById('nbill_section_<?php echo $accepted_item->id; ?>_accepted_summary_gross').innerHTML = '<?php echo $currency_pre; ?>' + format_currency(section_accepted_item_gross, <?php echo $precision; ?>) + '<?php echo $currency_post; ?>';

                    section_accepted_item_net = 0;
                    section_accepted_item_tax = 0;
                    section_accepted_item_shipping = 0;
                    section_accepted_item_shipping_tax = 0;
                    section_accepted_item_gross = 0;
                    <?php
                }
            }
            ?>
            var freq_code = 'AA';
            for(freq_code in accepted_item_gross) {
                if (document.getElementById('nbill_accepted_summary_net_' + freq_code)) {
                    document.getElementById('nbill_accepted_summary_net_' + freq_code).innerHTML = '<?php echo $row->currency_symbol; ?>' + format_currency(accepted_item_net[freq_code], <?php echo $precision; ?>);
                }
                if (document.getElementById('nbill_accepted_summary_tax_' + freq_code)) {
                    document.getElementById('nbill_accepted_summary_tax_' + freq_code).innerHTML = '<?php echo $row->currency_symbol; ?>' + format_currency(accepted_item_tax[freq_code], <?php echo $precision; ?>);
                }
                if (document.getElementById('nbill_accepted_summary_shipping_' + freq_code)) {
                    document.getElementById('nbill_accepted_summary_shipping_' + freq_code).innerHTML = '<?php echo $row->currency_symbol; ?>' + format_currency(accepted_item_shipping[freq_code], <?php echo $precision; ?>);
                }
                if (document.getElementById('nbill_accepted_summary_shipping_tax_' + freq_code)) {
                    document.getElementById('nbill_accepted_summary_shipping_tax_' + freq_code).innerHTML = '<?php echo $row->currency_symbol; ?>' + format_currency(accepted_item_shipping_tax[freq_code], <?php echo $precision; ?>);
                }
                document.getElementById('nbill_accepted_summary_gross_' + freq_code).innerHTML = '<?php echo $row->currency_symbol; ?>' + format_currency(accepted_item_gross[freq_code], <?php echo $precision; ?>);
            }
        }
        </script>

        <div id="nbill_quote_items">
        <!-- Preload images -->
        <span style="display:none">
            <img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/tick.png" . nbf_cms::$interop->site_page_suffix; ?>" alt="" />
            <img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/cross.png" . nbf_cms::$interop->site_page_suffix; ?>" alt="" />
            <img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/partial.png" . nbf_cms::$interop->site_page_suffix; ?>" alt="" />
        </span>
        <table cellpadding="5" cellspacing="0" border="0" width="100%" class="nbill-invoice-table nbill_quote_table">
            <tr class="line_item_headings">
                <th><?php echo NBILL_PRT_DESC; ?></th>
                <?php if (!$hide_unit_price) { ?><th class="numeric responsive-cell optional"><?php echo NBILL_PRT_UNIT_PRICE; ?></th><?php } ?>
                <?php if (!$hide_quantity) { ?><th class="numeric responsive-cell optional"><?php echo NBILL_PRT_QUANTITY; ?></th><?php } ?>
                <?php if (!$hide_discount) { ?><th class="numeric"><?php echo NBILL_PRT_DISCOUNT; ?></th><?php } ?>
                <?php if (!$hide_net_price) { ?><th class="numeric responsive-cell priority"><?php echo NBILL_PRT_NET_PRICE; ?></th><?php } ?>
                <?php if (!$hide_tax) { ?><th class="numeric responsive-cell priority"><?php if (nbf_common::nb_strlen($row->tax_abbreviation) == 0) {echo NBILL_PRT_VAT;} else {echo $row->tax_abbreviation;}; ?></th><?php } ?>
                <?php if (!$hide_shipping) { ?><th class="numeric"><?php echo NBILL_PRT_SHIPPING; ?></th><?php } ?>
                <?php if (!$hide_shipping_tax) { ?><th class="numeric"><?php echo sprintf(NBILL_PRT_SHIPPING_VAT, nbf_common::nb_strlen($row->tax_abbreviation) > 0 ? $row->tax_abbreviation : NBILL_PRT_VAT); ?></th><?php } ?>
                <th class="numeric"><?php echo NBILL_PRT_TOTAL; ?></th>
                <th style="text-align:center"><?php echo NBILL_QUOTE_ITEM_ACCEPTED_ACTION; ?></th>
            </tr>
            <?php
            $row_no = 0;
            $section_header_done = false;
            $this_item_no = -1;
            $sections_present = false;
            $section_items = array();
            $summary_net = array();
            $summary_tax = array();
            $summary_shipping = array();
            $summary_shipping_tax = array();
            $summary_gross = array();
            $accepted_summary_net = array();
            $accepted_summary_tax = array();
            $accepted_summary_shipping = array();
            $accepted_summary_shipping_tax = array();
            $accepted_summary_gross = array();
            $recurring_present = false;
            $one_off_present = false;
            $nothing_accepted_yet = true;
            $accept_checker_rendered = false;
            $this_section_atomic = false;

            //Separate items for payment frequency breakdown summary so we can take section discounts into account
            $summary_doc_items = quote_item_copy($items);
            nbf_discount::apply_section_discounts($summary_doc_items, $row->document_id);

            foreach ($items as &$document_item)
            {
                $this_item_no++;
                $this_section_item_count = 0;
                if (!$section_header_done)
                {
                    //If there is a section break on a later item, insert the header
                    for ($section_item = $this_item_no; $section_item < count($items); $section_item++)
                    {
                        if ($items[$section_item]->document_id == $row->document_id)
                        {
                            $this_section_item_count++;
                            if (strlen($items[$section_item]->section_name) > 0)
                            {
                                $sections_present = true;
                                $this_section_atomic = $items[$section_item]->section_quote_atomic;
                                ?>
                                <tr class="line_item_section_header">
                                    <th class="nbill-section-header" colspan="10">
                                        <?php echo $items[$section_item]->section_name; ?>
                                    </th>
                                </tr>
                                <?php
                                $section_header_done = true;
                                break;
                            }
                        }
                    }
                }
                if ($section_header_done)
                {
                    $section_items[] = $document_item;
                }
                if ($document_item->quote_item_accepted)
                {
                    $nothing_accepted_yet = false;
                }
                $row_no = $row_no == 1 ? 0 : 1;
                ?>
                <tr class="<?php echo "row$row_no"; ?> line_item_values">
                    <td>
                    <?php
                    if (nbf_common::nb_strlen($document_item->product_description) > 0)
                    {
                        echo strlen(trim(strip_tags($document_item->detailed_description))) > 0 ? "<strong>" . $document_item->product_description . "</strong><div>" . $document_item->detailed_description . "</div>" : $document_item->product_description;
                    }
                    else
                    {
                        echo strlen(trim(strip_tags($document_item->detailed_description))) > 0 ? '<div>' . $document_item->detailed_description . '</div>' : "&nbsp;";
                    } ?>
                    </td>
                    <?php if (!$hide_unit_price) { ?><td class="numeric responsive-cell optional"><?php echo nbf_common::convertValueToCurrencyObject($document_item->net_price_per_unit, $row->currency, true)->format(); ?></td><?php } ?>
                    <?php if (!$hide_quantity) { ?><td class="numeric responsive-cell optional"><?php echo nbf_common::convertValueToNumberObject($document_item->no_of_units, 'quantity')->format(); ?></td><?php } ?>
                    <?php if (!$hide_discount) { ?><td class="numeric"><?php echo nbf_common::convertValueToCurrencyObject($document_item->discount_amount, $row->currency, true)->format(); if (strlen($document_item->discount_description) > 0) {echo " (" . $document_item->discount_description . ")";} else {echo "&nbsp;";} ?></td><?php } ?>
                    <?php if (!$hide_net_price) { ?><td class="numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($document_item->net_price_for_item, $row->currency, true)->format(); ?></td><?php } ?>
                    <?php if (!$hide_tax) { ?><td class="numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($document_item->tax_for_item, $row->currency, true)->format();?></td><?php } ?>
                    <?php if (!$hide_shipping) { ?><td class="numeric"><?php echo nbf_common::convertValueToCurrencyObject($document_item->shipping_for_item, $row->currency, true)->format(); ?></td><?php } ?>
                    <?php if (!$hide_shipping_tax) { ?><td class="numeric"><?php echo nbf_common::convertValueToCurrencyObject($document_item->tax_for_shipping, $row->currency, true)->format(); ?></td><?php } ?>
                    <td class="numeric"><?php echo nbf_common::convertValueToCurrencyObject($document_item->gross_price_for_item, $row->currency, true)->format();
                    $summary_freq_code = defined("NBILL_PER_" . nbf_common::nb_strtoupper($document_item->quote_pay_freq)) ? nbf_common::nb_strtoupper($document_item->quote_pay_freq) : 'AA';

                    //Find equivalent summary doc item
                    foreach ($summary_doc_items as &$summary_doc_item)
                    {
                        if ($summary_doc_item->id == $document_item->id)
                        {
                            if (!isset($summary_net[$summary_freq_code])) {$summary_net[$summary_freq_code] = 0;}
                            if (!isset($summary_tax[$summary_freq_code])) {$summary_tax[$summary_freq_code] = 0;}
                            if (!isset($summary_shipping[$summary_freq_code])) {$summary_shipping[$summary_freq_code] = 0;}
                            if (!isset($summary_shipping_tax[$summary_freq_code])) {$summary_shipping_tax[$summary_freq_code] = 0;}
                            if (!isset($summary_gross[$summary_freq_code])) {$summary_gross[$summary_freq_code] = 0;}
                            if (!isset($accepted_summary_net[$summary_freq_code])) {$accepted_summary_net[$summary_freq_code] = 0;}
                            if (!isset($accepted_summary_tax[$summary_freq_code])) {$accepted_summary_tax[$summary_freq_code] = 0;}
                            if (!isset($accepted_summary_shipping[$summary_freq_code])) {$accepted_summary_shipping[$summary_freq_code] = 0;}
                            if (!isset($accepted_summary_shipping_tax[$summary_freq_code])) {$accepted_summary_shipping_tax[$summary_freq_code] = 0;}
                            if (!isset($accepted_summary_gross[$summary_freq_code])) {$accepted_summary_gross[$summary_freq_code] = 0;}
                            $summary_net[$summary_freq_code] = float_add($summary_net[$summary_freq_code], $summary_doc_item->net_price_for_item);
                            $summary_tax[$summary_freq_code] = float_add($summary_tax[$summary_freq_code], $summary_doc_item->tax_for_item);
                            $summary_shipping[$summary_freq_code] = float_add($summary_shipping[$summary_freq_code], $summary_doc_item->shipping_for_item);
                            $summary_shipping_tax[$summary_freq_code] = float_add($summary_shipping_tax[$summary_freq_code], $summary_doc_item->tax_for_shipping);
                            $summary_gross[$summary_freq_code] = float_add($summary_gross[$summary_freq_code], $summary_doc_item->gross_price_for_item);
                            if ($summary_doc_item->quote_item_accepted || $summary_doc_item->quote_awaiting_payment)
                            {
                                $accepted_summary_net[$summary_freq_code] = float_add($accepted_summary_net[$summary_freq_code], $summary_doc_item->net_price_for_item);
                                $accepted_summary_tax[$summary_freq_code] = float_add($accepted_summary_tax[$summary_freq_code], $summary_doc_item->tax_for_item);
                                $accepted_summary_shipping[$summary_freq_code] = float_add($accepted_summary_shipping[$summary_freq_code], $summary_doc_item->shipping_for_item);
                                $accepted_summary_shipping_tax[$summary_freq_code] = float_add($accepted_summary_shipping_tax[$summary_freq_code], $summary_doc_item->tax_for_shipping);
                                $accepted_summary_gross[$summary_freq_code] = float_add($accepted_summary_gross[$summary_freq_code], $summary_doc_item->gross_price_for_item);
                            }
                            if ($summary_freq_code == 'AA')
                            {
                                $one_off_present = true;
                            }
                            else
                            {
                                $recurring_present = true;
                                ?><br /><span style="white-space:nowrap"><?php echo constant("NBILL_PER_" . nbf_common::nb_strtoupper($summary_doc_item->quote_pay_freq)); ?></span><?php
                            }
                        }
                    }

                    $hide_checkbox = false;
                    if (!($this_section_atomic && $accept_checker_rendered))
                    {
                    ?>
                    </td>
                    <td style="text-align:center"<?php if ($this_section_atomic && !$accept_checker_rendered && $this_section_item_count > 1){echo ' rowspan="' . $this_section_item_count . '"';$accept_checker_rendered = true;} ?>>
                    <?php }
                    else
                    {
                        $hide_checkbox = true;
                    } ?>
                        <div style="<?php echo $hide_checkbox ? 'display:none;' : 'display:block;'; ?>">
                            <div id="nbill_quote_accept_checker_<?php echo $document_item->id; ?>" class="nbill_quote_checkbox" style="display:none;">
                            <input type="hidden" name="nbill_quote_accept_<?php echo $document_item->id; ?>" id="nbill_quote_accept_<?php echo $document_item->id; ?>" value="<?php echo $document_item->quote_item_accepted || $document_item->quote_mandatory || $document_item->quote_awaiting_payment ? '1' : '0'; ?>" />
                            <?php
                            if ((!nbf_frontend::get_display_option('quote_no_default_accept') || !$document_item->quote_item_accepted) && !$document_item->quote_mandatory) { ?><a class="nbill_quote_selector" href="#" onclick="item_accept_click(<?php echo $document_item->id; ?>, this);return false;" title="<?php echo $document_item->quote_item_accepted || $document_item->quote_awaiting_payment ? NBILL_QUOTE_CLICK_TO_REJECT : NBILL_QUOTE_CLICK_TO_ACCEPT ?>"><?php } ?>
                                <?php if ($document_item->quote_awaiting_payment)
                                { ?>
                                <img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/partial.png" . nbf_cms::$interop->site_page_suffix; ?>" alt="<?php echo NBILL_QUOTE_ITEM_ACCEPTED_YES; ?>" id="nbill_quote_accept_img_<?php echo $document_item->id; ?>" border="0" height="16" width="16" />
                                <?php }
                                else
                                { ?>
                                <img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=icons/" . ($document_item->quote_item_accepted || $document_item->quote_mandatory ? 'tick' : 'cross'); ?>.png<?php echo nbf_cms::$interop->site_page_suffix; ?>" alt="<?php echo $document_item->quote_item_accepted || $document_item->quote_mandatory ? NBILL_QUOTE_ITEM_ACCEPTED_YES : NBILL_QUOTE_ITEM_ACCEPTED_NO; ?>" id="nbill_quote_accept_img_<?php echo $document_item->id; ?>" border="0" height="16" width="16" />
                                <?php }
                                if ((!nbf_frontend::get_display_option('quote_no_default_accept') || !$document_item->quote_item_accepted) && !$document_item->quote_mandatory) { ?></a><?php }
                            ?>
                            </div>
                            <?php
                            if ($document_item->quote_mandatory) { echo " " . NBILL_FE_QUOTE_ITEM_MANDATORY; }
                            if ($document_item->quote_awaiting_payment) { echo " " . NBILL_QUOTE_ITEM_ACCEPTED_AWAITING; } ?>
                            <noscript><input type="checkbox" name="nbill_quote_accept_<?php echo $document_item->id; ?>"<?php if ($document_item->quote_item_accepted || $document_item->quote_mandatory || $document_item->quote_awaiting_payment) {echo ' checked="checked"';} if ($document_item->quote_mandatory) {echo ' disabled="disabled"';} ?> /><?php if ($document_item->quote_mandatory) { echo " " . NBILL_FE_QUOTE_ITEM_MANDATORY; } ?></noscript>
                            <script type="text/javascript">
                                document.getElementById('nbill_quote_accept_checker_<?php echo $document_item->id; ?>').style.display='';
                            </script>
                        </div>
                    </td>
                </tr>
                <?php
                if (strlen($document_item->section_name) > 0)
                {
                    //Work out section sub-totals (apply any discount if applicable)
                    $rejected_count = 0;
                    $accepted_count = 0;
                    $section_total_net = 0;
                    $section_total_tax = 0;
                    $section_total_shipping = 0;
                    $section_total_shipping_tax = 0;
                    $section_total_gross = 0;
                    $section_accepted_total_net = 0;
                    $section_accepted_total_tax = 0;
                    $section_accepted_total_shipping = 0;
                    $section_accepted_total_shipping_tax = 0;
                    $section_accepted_total_gross = 0;
                    $section_discount_net = 0;
                    $section_discount_tax = 0;
                    $section_discount_gross = 0;
                    foreach ($section_items as $section_item)
                    {
                        if ($section_item->quote_item_accepted)
                        {
                            $accepted_count++;
                        }
                        else
                        {
                            $rejected_count++;
                        }
                        $this_net = $section_item->net_price_for_item;
                        $this_tax = $section_item->tax_for_item;
                        $this_shipping = $section_item->shipping_for_item;
                        $this_shipping_tax = $section_item->tax_for_shipping;
                        $this_gross = $section_item->gross_price_for_item;
                        $this_discount_net = 0;
                        $this_discount_tax = 0;
                        $this_discount_gross = 0;
                        if ($document_item->section_discount_percent != 0)
                        {
                            $this_discount_net = ($this_net / 100) * $document_item->section_discount_percent;
                            $this_discount_tax = ($this_discount_net / 100) * $section_item->tax_rate_for_item;
                            $this_discount_gross = float_add($this_discount_net, $this_discount_tax);
                            $this_net = float_subtract($this_net, $this_discount_net);
                            $this_tax = float_subtract($this_tax, $this_discount_tax);
                            $this_gross = float_add(float_add($this_net, $this_tax), float_add($this_shipping, $this_shipping_tax));
                        }
                        $section_discount_net = float_add($section_discount_net, $this_discount_net);
                        $section_discount_tax = float_add($section_discount_tax, $this_discount_tax);
                        $section_discount_gross = float_add($section_discount_gross, $this_discount_gross);
                        $section_total_net = float_add($section_total_net, $this_net);
                        $section_total_tax = float_add($section_total_tax, $this_tax);
                        $section_total_shipping = float_add($section_total_shipping, $this_shipping);
                        $section_total_shipping_tax = float_add($section_total_shipping_tax, $this_shipping_tax);
                        $section_total_gross = float_add($section_total_gross, $this_gross);
                        if ($section_item->quote_item_accepted || $section_item->quote_awaiting_payment)
                        {
                            $section_accepted_total_net = float_add($section_accepted_total_net, $this_net);
                            $section_accepted_total_tax = float_add($section_accepted_total_tax, $this_tax);
                            $section_accepted_total_shipping = float_add($section_accepted_total_shipping, $this_shipping);
                            $section_accepted_total_shipping_tax = float_add($section_accepted_total_shipping_tax, $this_shipping_tax);
                            $section_accepted_total_gross = float_add($section_accepted_total_gross, $this_gross);
                        }
                    }
                    $section_discount_net = float_subtract(0, $section_discount_net);
                    $section_discount_tax = float_subtract(0, $section_discount_tax);
                    $section_discount_gross = float_subtract(0, $section_discount_gross);

                    if ($document_item->section_discount_percent != 0)
                    {
                        ?>
                        <tr class="line_item_section_discount">
                            <td><?php echo $document_item->section_discount_title ? $document_item->section_discount_title : NBILL_SECTION_DISCOUNT; ?></td>
                            <?php if (!$hide_unit_price) { ?><td class="numeric responsive-cell optional">&nbsp;</td><?php } ?>
                            <?php if (!$hide_quantity) { ?><td class="numeric responsive-cell optional">&nbsp;</td><?php } ?>
                            <?php if (!$hide_discount) { ?><td class="numeric">&nbsp;</td><?php } ?>
                            <?php if (!$hide_net_price) { ?><td class="numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($section_discount_net, $row->currency)->format(); ?></td><?php } ?>
                            <?php if (!$hide_tax) { ?><td class="numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($section_discount_tax, $row->currency, true)->format(); ?></td><?php } ?>
                            <?php if (!$hide_shipping) { ?><td class="numeric">&nbsp;</td><?php } ?>
                            <?php if (!$hide_shipping_tax) { ?><td class="numeric">&nbsp;</td><?php } ?>
                            <td class="numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_discount_gross, $row->currency, true)->format(); ?></td>
                            <?php $alt = $accepted_count > 0 ? ($rejected_count > 0 ? nbf_common::parse_translation(nbf_cms::$interop->language, "template.qu", 'NBILL_QUOTE_ITEM_ACCEPTED_PARTIAL') : nbf_common::parse_translation(nbf_cms::$interop->language, "template.qu", 'NBILL_QUOTE_ITEM_ACCEPTED_YES')) : nbf_common::parse_translation(nbf_cms::$interop->language, "template.qu", 'NBILL_QUOTE_ITEM_ACCEPTED_NO'); ?>
                            <td style="text-align:center;">
                                <img id="nbill_section_discount_selector_<?php echo $document_item->id; ?>" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/icons/<?php echo $accepted_count > 0 ? ($rejected_count > 0 ? 'partial' : 'tick') : 'cross'; ?>.png" alt="<?php echo $alt; ?>" title="<?php echo $alt; ?>" />
                            </td>
                        </tr>
                        <?php
                    }

                    $colspan = 4;
                    if ($hide_unit_price)    {$colspan--;}
                    if ($hide_quantity)    {$colspan--;}
                    if ($hide_discount)    {$colspan--;}
                    ?>
                    <!--<tr>
                        <td colspan="<?php echo $colspan; ?>">
                            <strong><?php echo $document_item->section_name; ?></strong>
                        </td>
                        <?php if (!$hide_net_price) { ?><td class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_total_net, $row->currency, true)->format();?></td><?php } ?>
                        <?php if (!$hide_tax) { ?><td class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_total_tax, $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping) { ?><td class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_total_shipping, $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping_tax) { ?><td class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_total_shipping_tax, $row->currency, true)->format(); ?></td><?php } ?>
                        <td class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($section_total_gross, $row->currency, true)->format(); ?></td>
                        <td class="total numeric">&nbsp;</td>
                    </tr>-->
                    <tr class="nbill_quote_accepted_tr">
                        <td colspan="<?php echo $colspan; ?>">
                            <strong><?php echo sprintf(NBILL_QUOTE_SECTION_ACCEPTED, $document_item->section_name); ?></strong>
                        </td>
                        <?php if (!$hide_net_price) { ?><td class="total numeric responsive-cell priority" id="nbill_section_<?php echo $document_item->id; ?>_accepted_summary_net"><?php echo nbf_common::convertValueToCurrencyObject($section_accepted_total_net, $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_tax) { ?><td class="total numeric responsive-cell priority" id="nbill_section_<?php echo $document_item->id; ?>_accepted_summary_tax"><?php echo nbf_common::convertValueToCurrencyObject($section_accepted_total_tax, $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping) { ?><td class="total numeric" id="nbill_section_<?php echo $document_item->id; ?>_accepted_summary_shipping"><?php echo nbf_common::convertValueToCurrencyObject($section_accepted_total_shipping, $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping_tax) { ?><td class="total numeric" id="nbill_section_<?php echo $document_item->id; ?>_accepted_summary_shipping_tax"><?php echo nbf_common::convertValueToCurrencyObject($section_accepted_total_shipping_tax, $row->currency, true)->format(); ?></td><?php } ?>
                        <td class="total numeric" id="nbill_section_<?php echo $document_item->id; ?>_accepted_summary_gross"><?php echo nbf_common::convertValueToCurrencyObject($section_accepted_total_gross, $row->currency, true)->format(); ?></td>
                        <td class="total numeric">&nbsp;</td>
                    </tr>
                    <?php
                    $section_header_done = false;
                    $section_items = array();
                    $accept_checker_rendered = false;
                }
            }

            if ($sections_present)
            {
                ?>
                <tr class="line_item_separator"><td colspan="10" style="border:none;">&nbsp;</td></tr>
                <?php
            }

            if ($recurring_present || $one_off_present)
            {
                $total_decision_rendered = false;
                foreach ($summary_gross as $key=>$value)
                {
                    ?>
                    <tr class="line_item_totals">
                        <?php $colspan = 4;
                        if ($hide_unit_price) {$colspan--;}
                        if ($hide_quantity) {$colspan--;}
                        if ($hide_discount) {$colspan--;}
                        ?>
                        <td class="total" colspan="<?php echo $colspan; ?>"><?php echo NBILL_PRT_TOTAL; if ($key != 'AA') {echo constant("NBILL_PER_" . nbf_common::nb_strtoupper($key));} else {if ($recurring_present) {echo NBILL_PRT_TOTAL_ONE_OFF;}} ?></td>
                        <?php if (!$hide_net_price) { ?><td align="right" class="total numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($summary_net[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_tax) { ?><td align="right" class="total numeric responsive-cell priority"><?php echo nbf_common::convertValueToCurrencyObject($summary_tax[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping) { ?><td align="right" class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($summary_shipping[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping_tax) { ?><td align="right" class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($summary_shipping_tax[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <td align="right" class="total numeric"><?php echo nbf_common::convertValueToCurrencyObject($value, $row->currency, true)->format(); ?></td>
                        <?php if (!$total_decision_rendered)
                        {
                            if ($row->status == 'DD')
                            {
                                //If all already accepted, don't bother asking
                                ?><td <?php if (count($summary_gross) > 1) {echo ' rowspan="' . count($summary_gross) . '"';} ?>>&nbsp;</td><?php
                                $total_decision_rendered = true;
                            }
                            else
                            {
                                ?><td class="total" style="text-align:center;"<?php if (count($summary_gross) > 1) {echo ' rowspan="' . count($summary_gross) . '"';} ?>>
                                    <?php if (count($items) > 1)
                                    {
                                        ?>
                                        <input type="submit" name="nbill_quote_accept_all" class="button btn nbill-button" value="<?php echo NBILL_QUOTE_ACCEPT_ALL; ?>"<?php if ($row->quote_show_warning){echo ' onclick="return confirm(\'' . NBILL_QUOTE_ACCEPT_ALL_WARNING . '\');"';} ?> />
                                        <?php
                                    }
                                    if ($nothing_accepted_yet && (count($items) > 1 || $items[0]->quote_mandatory))
                                    { ?>
                                        <input type="submit" name="nbill_quote_reject" class="button btn nbill-button" value="<?php echo count($items) > 1 ? NBILL_QUOTE_REJECT_ALL : NBILL_QUOTE_REJECT; ?>" onclick="return confirm('<?php echo NBILL_QUOTE_REJECT_WARNING; ?>');" />
                                    <?php }
                                    if ($row->quote_show_warning) { ?>
                                    <noscript><br /><?php echo NBILL_QUOTE_SUBMIT_WARNING; ?></noscript>
                                    <?php } ?>
                                </td>
                                <?php $total_decision_rendered = true;
                            }
                        } ?>
                    </tr><?php
                }
                ?>
                <tr class="line_item_separator"><td colspan="10" style="border:none;">&nbsp;</td></tr>
                <?php
                $accepted_decision_rendered = false;
                foreach ($accepted_summary_gross as $key=>$value)
                {
                    ?>
                    <tr class="nbill_quote_accepted_tr">
                        <?php $colspan = 4;
                        if ($hide_unit_price) {$colspan--;}
                        if ($hide_quantity) {$colspan--;}
                        if ($hide_discount) {$colspan--;}
                        ?>
                        <td class="total" colspan="<?php echo $colspan; ?>"><?php echo NBILL_QUOTE_ACCEPTED_TOTAL; if ($key != 'AA') {echo constant("NBILL_PER_" . nbf_common::nb_strtoupper($key));} else {if ($recurring_present) {echo NBILL_PRT_TOTAL_ONE_OFF;}} ?></td>
                        <?php if (!$hide_net_price) { ?><td align="right" class="total numeric responsive-cell priority" id="nbill_accepted_summary_net_<?php echo $key; ?>"><?php echo nbf_common::convertValueToCurrencyObject($accepted_summary_net[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_tax) { ?><td align="right" class="total numeric responsive-cell priority" id="nbill_accepted_summary_tax_<?php echo $key; ?>"><?php echo nbf_common::convertValueToCurrencyObject($accepted_summary_tax[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping) { ?><td align="right" class="total numeric" id="nbill_accepted_summary_shipping_<?php echo $key; ?>"><?php echo nbf_common::convertValueToCurrencyObject($accepted_summary_shipping[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <?php if (!$hide_shipping_tax) { ?><td align="right" class="total numeric" id="nbill_accepted_summary_shipping_tax_<?php echo $key; ?>"><?php echo nbf_common::convertValueToCurrencyObject($accepted_summary_shipping_tax[$key], $row->currency, true)->format(); ?></td><?php } ?>
                        <td align="right" class="total numeric" id="nbill_accepted_summary_gross_<?php echo $key; ?>"><?php echo nbf_common::convertValueToCurrencyObject($value, $row->currency, true)->format(); ?></td>
                        <?php if (!$accepted_decision_rendered)
                        {
                            if ($row->status == 'DD')
                            {
                                //If all already accepted, don't bother asking
                                ?><td <?php if (count($summary_gross) > 1) {echo ' rowspan="' . count($summary_gross) . '"';} ?>>&nbsp;</td><?php
                                $accepted_decision_rendered = true;
                            }
                            else
                            {
                                ?><td class="total" style="text-align:center;"<?php if (count($summary_gross) > 1) {echo ' rowspan="' . count($summary_gross) . '"';} ?>>
                                    <input type="submit" name="nbill_quote_submit" class="button btn nbill-button" value="<?php echo NBILL_QUOTE_ACCEPT_SELECTED; ?>" onclick="<?php $condition=array();foreach ($items as &$document_item){$condition[]="document.getElementById('nbill_quote_accept_" . $document_item->id . "').value == '1'";}?>if (<?php echo implode(" || ", $condition); ?>){<?php if ($row->quote_show_warning){echo 'return confirm(\'' . NBILL_QUOTE_SUBMIT_WARNING . '\');';} ?>} else {return confirm('<?php echo NBILL_QUOTE_REJECT_WARNING; ?>');}" />
                                    <?php if ($row->quote_show_warning) { ?>
                                    <noscript><br /><?php echo NBILL_QUOTE_SUBMIT_WARNING; ?></noscript>
                                    <?php } ?>
                                </td>
                                <?php $accepted_decision_rendered = true;
                            }
                        } ?>
                    </tr><?php
                }
            }
            else
            {
                ?>
                <tr class="line_items_empty">
                    <td colspan="2"><?php echo NBILL_QUOTE_NO_ITEMS; ?></td>
                </tr>
                <?php
            }
            ?>
        </table>
        </div>
        <?php
    }

    public static function show_email_form($from = "", $default_subject = "", $message = "", $cc = "", $bcc = "")
    {
        nbf_common::load_language("email");
        ?>
        <table cellpadding="3" cellspacing="0" border="0">
            <tr class="nbill_email_quote_row nbill_email_quote_from">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE_FROM; ?></td><td style="vertical-align:top"><input type="text" name="message_from" id="message_from" value="<?php echo $from; ?>" /></td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_to">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE_TO; ?></td><td style="vertical-align:top"><?php echo nbf_cms::$interop->site_name; ?></td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_cc">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE_CC; ?></td><td style="vertical-align:top"><input type="text" name="message_cc" id="message_cc" value="<?php echo is_array($cc) ? implode(";", $cc) : $cc ?>" /></td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_bcc">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE_BCC; ?></td><td style="vertical-align:top"><input type="text" name="message_bcc" id="message_bcc" value="<?php echo is_array($bcc) ? implode(";", $bcc) : $bcc ?>" /></td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_subject">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE_SUBJECT; ?></td><td style="vertical-align:top"><input type="text" name="message_subject" id="message_subject" value="<?php echo $default_subject; ?>" /></td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_message">
                <td style="vertical-align:top"><?php echo NBILL_EMAIL_MESSAGE; ?></td>
                <td style="vertical-align:top">
                    <textarea name="quote_email_message" id="quote_email_message" rows="8"><?php echo $message; ?></textarea>
                </td>
            </tr>
            <tr class="nbill_email_quote_row nbill_email_quote_action">
                <td colspan="2" align="right" style="text-align:right;vertical-align:top;">
                    <input type="submit" name="send_message" class="button btn nbill-button" value="<?php echo NBILL_EMAIL_SEND; ?>" style="font-size:10pt;font-weight:bold;" />
                    &nbsp;
                    <input type="submit" name="cancel_message" class="button btn nbill-button" value="<?php echo NBILL_EMAIL_CANCEL; ?>" style="font-size:10pt;font-weight:bold;" />
                </td>
            </tr>
        </table>
        <?php
    }

    public static function quote_rejected_successfully()
    {
        ?>
        <div class="nbill-message"><?php echo NBILL_QUOTE_REJECTED_SUCCESSFULLY; ?></div>
        <?php
    }

    public static function quote_part_rejected_successfully()
    {
        ?>
        <div class="nbill-message"><?php echo NBILL_QUOTE_PART_REJECTED_SUCCESSFULLY; ?></div>
        <?php
    }

    public static function quote_accepted_successfully($invoice_generated)
    {
        ?>
        <div class="nbill-message"><?php echo NBILL_QUOTE_ACCEPTED_SUCCESSFULLY; if ($invoice_generated) {echo NBILL_QUOTE_ACCEPTED_INVOICE_GENERATED;} ?></div>
        <?php
    }

    public static function quote_nothing_new_accepted()
    {
        ?>
        <div class="nbill-message"><?php echo NBILL_QUOTE_NOTHING_NEW; ?></div>
        <?php
    }

    public static function show_quote_payment_summary($quote_no, $select_gateway, $gateways, $default_gateway, $quote_summary_total, $voucher_available = false)
    {
        ?>
        <table cellpadding="3" cellspacing="0" border="0" class="nbill_summary_table nbill_quote_payment">
            <tr class="nbill_quote_payment_intro">
                <th colspan="2" class="nbill_summary_sub_heading"><?php echo NBILL_SUMMARY_QUOTE_DETAILS; ?></th>
            </tr>
            <tr class="nbill_quote_payment_row quote_payment_number">
                <td class="field-title"><?php echo NBILL_FE_QUOTE_NUMBER; ?></td>
                <td class="field-value"><?php echo $quote_no; ?></td>
            </tr>
            <?php
            echo $quote_summary_total; //This has been rendered by the summary table field control
            if ($select_gateway)
            {
                if (count($gateways) > 1)
                {
                    //Allow selection of payment gateway
                    ?>
                    <tr class="nbill_quote_payment_row quote_payment_gateway">
                        <td><?php echo NBILL_SELECT_GATEWAY; ?></td>
                        <td align="right" class="nbill-gateway-select">
                            <?php
                            $gateway_list = array();
                            $resubmit_on_select = false;
                            foreach($gateways as $gateway)
                            {
                                $gateway_list[] = nbf_html::list_option($gateway->code, $gateway->description);
                                if (@$gateway->fee_or_discount !== null)
                                {
                                    $resubmit_on_select = true;
                                }
                            }
                            echo nbf_html::select_list($gateway_list, "payment_gateway", 'id="payment_gateway" class="inputbox"' . ($resubmit_on_select ? ' onchange="document.select_gateway.submit();"' : ''), $default_gateway);
                            ?>
                        </td>
                    </tr>
                    <?php
                }
            }
            if ($voucher_available)
            {
                ?>
                <tr class="nbill_quote_payment_row quote_payment_discount">
                    <td><?php echo NBILL_FE_DOC_DISCOUNT_VOUCHER; ?></td>
                    <td>
                        <input type="text" name="nbill_document_voucher_code" id="nbill_document_voucher_code" value="" />
                        <input type="submit" class="button btn nbill-button" name="nbill_apply_voucher" value="<?php echo NBILL_FE_DOC_DISCOUNT_APPLY; ?>" />
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr class="nbill_quote_payment_row quote_payment_submit">
                <td colspan="2" align="right" class="nbill-quote_payment-submit" style="text-align:right">
                    <?php
                    if (!$voucher_available)
                    {
                        ?>
                        <input type="hidden" name="nbill_document_voucher_code" value="<?php echo nbf_common::get_param($_REQUEST, 'nbill_document_voucher_code'); ?>" />
                        <?php
                    }
                    ?>
                    <input type="submit" class="button btn nbill-button" name="nbill_submit_quote_payment_summary" id="nbill_submit_quote_payment_summary" value="<?php echo NBILL_SUBMIT; ?>" />
                </td>
            </tr>
        </table>
        <?php
    }

    /**
    * If there is a message to display, display it
    * @param mixed $message
    */
    public static function show_message($message)
    {
        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            ?><div class="nbill-message"><?php echo nbf_globals::$message; ?></div><?php
        }
        if (nbf_common::nb_strlen($message) > 0)
        {
            echo $message;
        }
    }

    /**
    * If there is a message to display, display it
    * @param mixed $message
    */
    public static function pay_offline($message)
    {
        if (nbf_common::nb_strlen($message) > 0)
        {
            echo $message;
        }
        else
        {
            echo NBILL_QUOTE_PAY_OFFLINE;
        }
    }

    /**
    * Displays the breadcrumb path, if the display options require it
    * @param string $form_title Title of current form, or nothing if we are on the list of forms
    */
    public static function show_pathway($editing_quote = false)
    {
        if (nbf_frontend::get_display_option("pathway"))
        {
            if ($editing_quote)
            {
                ?>
                <div class="pathway" style="margin-bottom:7px"><a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix); ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <a href="<?php echo nbf_cms::$interop->site_page_prefix ?>&action=quotes<?php echo nbf_cms::$interop->site_page_suffix; ?>"><?php echo NBILL_MY_QUOTES; ?></a> &gt; <?php echo NBILL_QUOTE_VIEW_DETAILS ?></div>
                <?php
            }
            else
            {
                ?>
                <div class="pathway" style="margin-bottom:7px"><a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix); ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <?php echo NBILL_MY_QUOTES; ?></div>
                <?php
            }
        }
    }

    protected static function _show_filters()
    {
        if (nbf_frontend::get_display_option("quote_date_range"))
        {
            echo "&nbsp;&nbsp;" . NBILL_DATE_RANGE; $cal_date_format = nbf_common::get_date_format(true); ?>
            <span style="white-space:nowrap"><input type="text" id="search_date_from" class="calendar-textbox" name="search_date_from" size="10" maxlength="19" value="<?php echo nbf_common::get_param($_REQUEST,'search_date_from'); ?>" <?php if (nbf_common::get_param($_POST,'all_outstanding')) {echo "disabled=\"disabled\"";} ?> />
            <input type="button" id="search_date_from_cal" name="search_date_from_cal" class="calendar-button button btn" value="..." onclick="displayCalendar(document.quotes.search_date_from,'<?php echo $cal_date_format; ?>',this);" <?php if (nbf_common::get_param($_POST,'all_outstanding')) {echo "disabled=\"disabled\"";} ?> /></span>
            <?php echo NBILL_TO; ?>
            <span style="white-space:nowrap"><input type="text" id="search_date_to" class="calendar-textbox" name="search_date_to" size="10" maxlength="19" value="<?php echo nbf_common::get_param($_REQUEST,'search_date_to'); ?>" <?php if (nbf_common::get_param($_POST,'all_outstanding')) {echo "disabled=\"disabled\"";} ?> />
            <input type="button" id="search_date_to_cal" name="search_date_to_cal" class="calendar-button button btn" value="..." onclick="displayCalendar(document.quotes.search_date_to,'<?php echo $cal_date_format; ?>',this);" <?php if (nbf_common::get_param($_POST,'all_outstanding')) {echo "disabled=\"disabled\"";} ?> /></span>

            <input type="submit" id="dosearch" name="dosearch" class="button btn nbill-button" value="<?php echo NBILL_GO; ?>" />
            <?php
        }
    }
}