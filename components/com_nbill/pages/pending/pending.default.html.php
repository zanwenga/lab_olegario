<?php
/**
* Default HTML output template for pending order processing pages
* @version 1
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nbill_fe_pending
{
    public static function show_order_cancellation($order)
    {
        ?>
        <table width="100%" class="contentpane nbill-cancel-order-table" cellpadding="3" cellspacing="1" border="0" style="margin-top:5px;">
            <tr class="cancel_tr cancel_intro"><td colspan="2" class="nbill-cancel-order-intro"><?php echo NBILL_CANCEL_ORDER_INTRO; ?><br /><hr /></td></tr>
            <tr class="cancel_tr cancel_order_no"><td><?php echo NBILL_ORDER_NO; ?></td><td><?php echo $order->order_no; ?></td></tr>
            <tr class="cancel_tr cancel_product"><td><?php echo NBILL_FE_PRODUCT; ?></td><td><?php echo $order->product_name; ?></td></tr>
            <?php if (nbf_common::nb_strlen($order->relating_to) > 0) { ?>
                <tr class="cancel_tr cancel_relating_to"><td><?php echo NBILL_RELATING_TO; ?></td><td><?php echo $order->relating_to; ?></td></tr>
            <?php } ?>
            <tr class="cancel_tr cancel_price"><td><?php echo NBILL_PRICE; ?></td><td><?php echo $order->total_gross;?></td>
            <tr class="cancel_tr cancel_reason"><td><?php echo NBILL_CANCELLATION_REASON; ?></td><td><textarea name="cancellation_reason" cols="30" rows="4"><?php echo stripslashes(nbf_common::get_param($_POST, 'cancellation_reason')); ?></textarea></td></tr>
            <tr class="cancel_tr cancel_confirm"><td colspan="2"><input type="checkbox" name="confirm" id="confirm"><label for="confirm"><?php echo NBILL_CONFIRM_CANCELLATION; ?></label>
            <tr class="cancel_tr cancel_navigate">
                <td><div style="float:left" class="nbill-cancel-order-back"><a href="<?php echo nbf_cms::$interop->site_page_prefix; ?>&action=orders&task=view<?php echo nbf_cms::$interop->site_page_suffix; ?>"><?php echo NBILL_RETURN_TO_ORDERS; ?></a></div></td>
                <td align="right" class="nbill-cancel-order-submit"><input type="submit" id="btn_delete_pending" class="button btn nbill-button" name="cancel_order" value="<?php echo NBILL_CANCEL_ORDER; ?>" /></td>
            </tr>
        </table>
        <?php
    }

    public static function delete_pending_are_you_sure()
    {
        ?>
        <p class="nbill-delete-pending-confirm"><?php echo NBILL_CONFIRM_PENDING_DELETE; ?></p>
        <table width="100%" class="contentpane nbill-cancel-order-table" cellpadding="3" cellspacing="1" border="0" style="margin-top:5px;">
            <tr class="cancel_tr cancel_pending_confirm">
                <td width="100%"><div style="float:left" class="nbill-delete-pending-back"><a href="<?php echo nbf_cms::$interop->site_page_prefix; ?>&action=orders&task=view<?php echo nbf_cms::$interop->site_page_suffix; ?>"><?php echo NBILL_RETURN_TO_ORDERS; ?></a></div></td>
                <td align="right" class="nbill-cancel-order-submit" style="text-align:right"><input type="submit" id="btn_confirm_delete_pending" class="button btn nbill-button" name="confirm" value="<?php echo NBILL_CANCEL_ORDER; ?>" /></td>
            </tr>
        </table>
        <?php
    }

    /**
    * Show list of pending/live orders
    * @param array $rows The live orders
    * @param string $date_format
    * @param array $pay_frequencies
    * @param array $downloadables
    * @param array $pending_orders The pending orders
    */
    public static function show_orders($rows, $date_format, $pay_frequencies, $downloadables, $pending_orders)
    {
        ?>
        <table width="100%" class="contentpane category" id="nbill-order-list-table" cellpadding="3" cellspacing="1" border="0" style="margin-top:5px;">
            <tr class="jlist-table nbill_list_tr_headings nbill_orders">
                <?php
                self::render_column_header($rows, "order_no", NBILL_ORDER_NO);
                self::render_column_header($rows, "order_date", NBILL_ORDER_DATE);
                self::render_column_header($rows, "expiry_date", NBILL_EXPIRY_DATE_FE);
                self::render_column_header($rows, "product", NBILL_FE_PRODUCT);
                self::render_column_header($rows, "relating_to", NBILL_FE_RELATING_TO);
                self::render_column_header($rows, "order_value", NBILL_PRICE);
                self::render_column_header($rows, "frequency", NBILL_PAY_FREQUENCY);
                self::render_column_header($rows, "order_status", NBILL_FE_ORDER_STATUS);
                if (count($downloadables) > 0)
                {
                    self::render_column_header($rows, "download", NBILL_DOWNLOAD);
                } ?>
            </tr>
            <?php

            $rownumber = 2;
            for ($i=0, $n=count( $pending_orders ); $i < $n; $i++)
            {
                $pending_order = $pending_orders[$i];
                $rownumber = $rownumber == 1 ? 2 : 1;
                ?>
                <tr class="sectiontableentry<?php echo $rownumber; ?> cat-list-row<?php echo $rownumber; ?> nbill_list_tr_value" id="tr_pending_order_<?php echo $pending_order->id; ?>">
                    <?php
                    $display_option = nbf_frontend::get_display_option("order_no");
                    if ($display_option) { ?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo NBILL_PENDING . "-" . $pending_order->id; ?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_no", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_no", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("order_date");
                    if ($display_option) { ?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo nbf_common::nb_date($date_format, $pending_order->timestamp); ?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_date", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_date", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("expiry_date");
                    if ($display_option) {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo "&nbsp"; ?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_expiry_date", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_expiry_date", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("product");
                    if ($display_option) { ?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $pending_order->form_name;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_product", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_product", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("relating_to");
                    if ($display_option) { ?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $pending_order->relating_to;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_relating_to", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_relating_to", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("order_value");
                    if ($display_option) {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $pending_order->total_gross;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_value", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_value", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("frequency");
                    if ($display_option) {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php foreach ($pay_frequencies as $pay_frequency)
                    {
                        if ($pay_frequency->code == $pending_order->payment_frequency)
                        {
                            echo $pay_frequency->description;
                            echo "&nbsp;&nbsp;<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=pending&task=delete&id=" . $pending_order->id . nbf_cms::$interop->site_page_suffix . "\">" . NBILL_CANCEL . "</a>";
                            break;
                        }
                    }?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_frequency", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_frequency", 'render_row'), $pending_order);
                    }
                    $display_option = nbf_frontend::get_display_option("order_status");
                    if ($display_option) { ?>
                    <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>">
                        <?php echo NBILL_PENDING;
                        if ($pending_order->total_gross->value > 0 && (nbf_frontend::get_display_option("pending_pay_link")))
                        {
                            echo "&nbsp;&nbsp;<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=pending&task=pay&id=" . $pending_order->id . "" . nbf_cms::$interop->site_page_suffix . "\" style=\"white-space:nowrap;\">" . NBILL_PENDING_ORDER_PAY_NOW . "</a>";
                        }
                        ?>
                    </td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_status", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_status", 'render_row'), $pending_order);
                    }
                    if (count($downloadables) > 0) {?><td>&nbsp;</td><?php }
                    if (is_callable(array("nbill_pending_after_download", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_download", 'render_row'), $pending_order);
                    } ?>
                </tr>
                <?php
            }
            for ($i=0, $n=count( $rows ); $i < $n; $i++)
            {
                $row = &$rows[$i];
                $rownumber = $rownumber == 1 ? 2 : 1;

                $download_link = array();
                $download_count = 0;
                if (array_key_exists($row->product_id, $downloadables))
                {
                    $download = $downloadables[$row->product_id];
                    if (is_array($download) && count($download) == 20)
                    {
                        $link_no = 0;
                        for ($j=0; $j<20; $j=$j+2)
                        {
                            $link_no++;
                            if (nbf_common::nb_strlen($download[$j]) > 0)
                            {
                                $download_link[] = "<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=download&id=$row->id&linkno=$link_no\">" . $download[$j + 1] . "</a>";
                                $download_count++;
                            }
                        }
                    }
                }
                ?>
                <tr class="sectiontableentry<?php echo $rownumber; ?> cat-list-row<?php echo $rownumber; ?> nbill_list_tr_value" id="tr_order_<?php echo $row->id; ?>">
                    <?php
                    $display_option = nbf_frontend::get_display_option("order_no");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>">
                        <a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=orders&task=detail&id=' . $row->id . nbf_cms::$interop->site_page_suffix); ?>">
                        <?php
                        echo $row->order_no;
                        ?></a><?php
                        $display_option = nbf_frontend::get_display_option("invoice_link");
                        if ($display_option)
                        {?>
                            <span class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>">&nbsp;
                                <a href="<?php $url = nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=invoices&task=view&order_id=' . $row->id . '&search_date_from=' . nbf_common::nb_date(nbf_common::get_date_format(), 0) . '' . nbf_cms::$interop->site_page_suffix); if (@$_SERVER['HTTPS'] || @$_SERVER['SERVER_PORT'] == 443) echo str_replace("http://", "https://", $url); else echo $url; ?>" title="<?php echo NBILL_VIEW_INVOICES_ALT; ?>"><img border="0" src="<?php echo nbf_cms::$interop->nbill_site_url_path; ?>/images/icons/invoices.gif" alt="<?php echo NBILL_VIEW_INVOICES; ?>" /></a>
                            </span><?php
                        } ?>
                        </td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_no", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_no", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("order_date");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo nbf_common::nb_date($date_format, $row->start_date);?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_date", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_date", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("expiry_date");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php if ($row->expiry_date) {echo nbf_common::nb_date($date_format, $row->expiry_date);} else {echo "&nbsp;";} ?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_expiry_date", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_expiry_date", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("product");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $row->product_name;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_product_name", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_product_name", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("relating_to");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $row->relating_to;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_relating_to", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_relating_to", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("order_value");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo $row->total_gross ;?></td>
                    <?php }
                    if (is_callable(array("nbill_pending_after_order_value", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_value", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("frequency");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php
                        foreach ($pay_frequencies as $pay_frequency)
                        {
                            if ($pay_frequency->code == $row->payment_frequency)
                            {
                                echo $pay_frequency->description;
                                if ($row->expiry_date > 0 && $row->expiry_date < nbf_common::nb_time())
                                {
                                    echo "<span style=\"color: #ff0000;\">";
                                    echo " (" . NBILL_EXPIRED . " " . nbf_common::nb_date($date_format, $row->expiry_date) . ")";
                                    echo "</span>";
                                }
                                else
                                {
                                    if ($row->next_due_date > 0 && $row->net_price > 0)
                                    {
                                        if (time() > $row->next_due_date)
                                        {
                                            echo "<span style=\"color: #ff0000;\">";
                                        }
                                        echo " (" . NBILL_DUE . " " . nbf_common::nb_date($date_format, $row->next_due_date) . ")";
                                        if (time() > $row->next_due_date)
                                        {
                                            echo "</span>";
                                        }
                                    }
                                }
                                if (nbf_frontend::get_display_option("allow_cancellation"))
                                {
                                    if ($row->order_status != 'EE' && $row->payment_frequency != 'AA' && $row->payment_frequency != 'XX' && ($row->auto_renew || !nbf_frontend::get_display_option("suppress_cancel_if_not_auto")))
                                    {
                                        echo " &nbsp;<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=orders&task=delete&id=" . $row->id . "" . nbf_cms::$interop->site_page_suffix . "\">" . NBILL_CANCEL . "</a>";
                                    }
                                }
                                $display_option = nbf_frontend::get_display_option("renew_link");
                                if ($display_option)
                                {
                                    if ($row->order_status != 'EE' || !nbf_frontend::get_display_option("suppress_renew_if_cancelled"))
                                    {
                                        if ((!$row->auto_renew || ($row->expiry_date > 0)) && $row->payment_frequency != 'AA' && $row->payment_frequency != 'XX' && $row->net_price > 0)
                                        {
                                            //Check how far in advance renewals can be made
                                            $advance = nbf_frontend::get_display_option("renew_link_advance_limit");
                                            $advance = $advance === false ? 1 : intval($advance);
                                            if ($row->auto_renew)
                                            {
                                                $due_date = $row->expiry_date;
                                            }
                                            else
                                            {
                                                $due_date = $row->next_due_date;
                                            }
                                            $max_due_date = nbf_common::nb_time();
                                            for ($j = 1; $j <= intval($advance); $j++)
                                            {
                                                $max_due_date = nbf_date::get_next_payment_date($row->start_date, $max_due_date, $row->payment_frequency, false);
                                            }
                                            if ($due_date <= $max_due_date)
                                            {
                                                ?><span class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><?php echo " &nbsp;<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=orders&task=renew&order_id=" . $row->id . "&show_due=1" . nbf_cms::$interop->site_page_suffix . "\">" . NBILL_RENEW . "</a>"; ?></span><?php
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                        }?>
                        </td>
                        <?php
                    }
                    if (is_callable(array("nbill_pending_after_frequency", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_frequency", 'render_row'), $row);
                    }
                    $display_option = nbf_frontend::get_display_option("order_status");
                    if ($display_option)
                    {?>
                        <td class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>">
                        <?php echo @constant($row->order_status_desc);
                        $display_option = nbf_frontend::get_display_option("parcel_tracking");
                        if ($display_option)
                        {
                            $tracking_url = generate_tracking_url($row->shipping_id, $row->parcel_tracking_id);
                            if ($tracking_url)
                            {
                                ?>
                                <span class="responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"><a target="_blank" href="<?php echo $tracking_url; ?>" title="<?php NBILL_TRACK_THIS_PARCEL; ?>"><?php echo NBILL_TRACKING; ?></a></span>
                                <?php
                            }
                        }
                        ?>
                        </td>
                        <?php
                    }
                    if (is_callable(array("nbill_pending_after_order_status", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_order_status", 'render_row'), $row);
                    }
                    if (count($downloadables) > 0)
                    {
                        ?><td><?php
                    }
                    if ($download_count > 0)
                    {
                        if ($download_count > 1)
                        {
                            ?><ul><li><?php
                        }
                        if ($row->order_status != 'EE')
                        {
                            echo $download_link[0];
                        }
                        else
                        {
                            echo "&nbsp;";
                        }
                        if ($download_count > 1)
                        {
                            ?></li><?php
                        }
                    }
                    for ($j=1; $j<$download_count; $j++)
                    {
                        ?><li><?php if ($row->order_status != 'EE') {echo $download_link[$j];} else {echo "&nbsp;";} ?></li><?php
                    }
                    if ($download_count > 1)
                    {
                        ?></ul><?php
                    }
                    if (count($downloadables) > 0)
                    {
                        ?></td><?php
                    }
                    if (is_callable(array("nbill_pending_after_download", 'render_row')))
                    {
                        call_user_func(array("nbill_pending_after_download", 'render_row'), $row);
                    }
                    ?>
                </tr><?php
            } ?>
        </table>
        <?php
    }

    public static function show_order_detail($order, $date_format, $field_labels = array(), $field_options = array(), $downloadables = array())
    {
        ?>
        <table cellpadding="0" cellspacing="0" border="0" class="nbill-order-details">
            <tr class="order_details order_details_heading">
                <th colspan="2" id="nbill_order_form_field_values">
                    <?php echo NBILL_ORDER_DETAILS; ?>
                </th>
            </tr>
            <tr class="order_details order_details_order_no">
                <td class="order_details_label"><?php echo NBILL_FE_ORDER_NO; ?></td>
                <td class="order_details_value"><?php echo $order->order_no; ?></td>
            </tr>
            <tr class="order_details order_details_product">
                <td class="order_details_label"><?php echo NBILL_FE_PRODUCT; ?></td>
                <td class="order_details_value"><?php echo $order->product_name; ?></td>
            </tr>
            <?php
            $download_link = array();
            $download_count = 0;
            foreach ($downloadables as $download)
            {
                if (is_array($download) && count($download) == 20) {
                    $link_no = 0;
                    for ($j=0; $j<20; $j=$j+2)
                    {
                        $link_no++;
                        if (strlen($download[$j]) > 0) {
                            $download_link[] = "<a href=\"" . nbf_cms::$interop->site_page_prefix . "&action=download&id=" . $order->id . "&linkno=$link_no\">" . $download[$j + 1] . "</a>";
                            $download_count++;
                        }
                    }
                }
            }
            if ($download_count > 0) {
                ?>
                <tr class="order_details order_details_downloads">
                    <td class="order_details_label"><?php echo NBILL_FE_DOWNLOADS; ?></td>
                    <td class="order_details_value">
                        <ul>
                            <?php foreach ($download_link as $link)
                            {
                                ?><li><?php if ($order->order_status != 'EE') {echo $link;} else {echo "&nbsp;";} ?></li><?php
                            } ?>
                        </ul>
                    </td>
                </tr>
                <?php
            }
            ?>
            <tr class="order_details order_details_price">
                <td class="order_details_label"><?php echo NBILL_PRICE; ?></td>
                <td class="order_details_value"><?php echo nbf_common::convertValueToCurrencyObject($order->net_price + $order->total_tax_amount + $order->total_shipping_price + $order->total_shipping_tax, $order->currency); ?></td>
            </tr>
            <tr class="order_details order_details_quantity">
                <td class="order_details_label"><?php echo NBILL_FE_QUANTITY; ?></td>
                <td class="order_details_value"><?php echo  nbf_common::convertValueToNumberObject($order->quantity, 'quantity'); ?></td>
            </tr>
            <tr class="order_details order_details_relating_to">
                <td class="order_details_label"><?php echo NBILL_RELATING_TO; ?></td>
                <td class="order_details_value"><?php echo $order->relating_to; ?></td>
            </tr>
            <tr class="order_details order_details_order_date">
                <td class="order_details_label"><?php echo NBILL_ORDER_DATE; ?></td>
                <td><?php echo date($date_format, $order->start_date); ?></td>
            </tr>
            <tr class="order_details order_details_pay_frequency">
                <td class="order_details_label"><?php echo NBILL_PAY_FREQUENCY; ?></td>
                <td class="order_details_value"><?php echo @constant($order->payment_frequency_desc) ? constant($order->payment_frequency_desc) : $order->payment_frequency_desc; ?></td>
            </tr>
            <tr class="order_details order_details_last_due_date">
                <td class="order_details_label"><?php echo NBILL_ORDER_LAST_DUE_DATE; ?></td>
                <td class="order_details_value"><?php echo date($date_format, $order->last_due_date); ?></td>
            </tr>
            <tr class="order_details order_details_next_due_date">
                <td class="order_details_label"><?php echo NBILL_ORDER_NEXT_DUE_DATE; ?></td>
                <td class="order_details_value"><?php echo date($date_format, $order->next_due_date); ?></td>
            </tr>
            <tr class="order_details order_details_status">
                <td class="order_details_label"><?php echo NBILL_FE_ORDER_STATUS; ?></td>
                <td class="order_details_value"><?php echo @constant($order->status_desc) ? constant($order->status_desc) : $order->status_desc; ?></td>
            </tr>
            <?php
            ob_start();
            $form_fields = explode("\n", $order->form_field_values);
            if (count($form_fields) > 0) {
                foreach ($form_fields as $form_field)
                {
                    $field_parts = explode("=", $form_field);
                    if (count($field_parts) == 2) {
                        if (substr($field_parts[0], 0, 15) != 'ctl_NBILL_CORE_' && substr($field_parts[0], 0, 8) != 'ctl_next') {
                            $field_name = trim(str_replace('ctl_', '', $field_parts[0]));
                            $field = isset($field_labels[$field_name]) ? $field_labels[$field_name] : null;
                            if ($field) {
                                if (substr($field['field_type'], 0, 1) == 'E') { //Checkbox
                                    $label = $field['checkbox_text'] ? $field['checkbox_text'] : str_replace('ctl_', '', $field_parts[0]);
                                } else {
                                    $label = $field['label'] ? $field['label'] : str_replace('ctl_', '', $field_parts[0]);
                                }
                                $value = trim($field_parts[1]);
                                if (strlen($label) > 0 && strlen($value) > 0) {
                                    foreach ($field_options as $field_option)
                                    {
                                        if ($field_option->field_id == $field['id'] && $field_option->option_value == $value) {
                                            $value = $field_option->option_description;
                                        }
                                    }
                                    ?>
                                    <tr class="order_details form_field" id="tr_form_field_<?php echo $field['id']; ?>">
                                        <td class="order_details_label"><?php echo $label; ?></td>
                                        <td class="order_details_value"><?php echo $value; ?></td>
                                    </tr>
                                    <?php
                                }
                            }
                        }
                    }
                }
            }
            $form_field_values = ob_get_clean();
            if (strlen($form_field_values) > 0) {
                ?>
                <tr class="order_details order_details_form_fields">
                    <th colspan="2" id="nbill_order_form_field_values">
                        <?php echo NBILL_FE_FORM_FIELD_VALUES; ?>
                    </th>
                </tr>
                <?php
                echo $form_field_values;
            }
            ?>
        </table>
        <?php
    }

    public static function render_column_header($rows, $column_name, $header_text, $custom_style = "")
    {
        $display_option = nbf_frontend::get_display_option($column_name);
        if ($display_option)
        {?>
            <th class="sectiontableheader responsive-cell<?php echo nbf_frontend::get_css_class_for_option($display_option); ?>"<?php echo $custom_style ? ' style="' . $custom_style . '"' : ''; ?>><?php echo $header_text; ?></th>
        <?php }
        if (file_exists(dirname(__FILE__) . "/custom_columns/after_$column_name.php"))
        {
            include_once(dirname(__FILE__) . "/custom_columns/after_$column_name.php");
            if (is_callable(array("nbill_pending_after_$column_name", 'render_header')))
            {
                call_user_func(array("nbill_pending_after_$column_name", 'render_header'), $rows);
            }
        }
    }

    /**
    * Displays the breadcrumb path, if the display options require it
    * @param string $form_title Title of current form, or nothing if we are on the list of forms
    */
    public static function show_pathway($cancellation = false, $order_details = false)
    {
        if (nbf_frontend::get_display_option("pathway"))
        {
            if ($cancellation)
            {
                ?><div class="pathway" style="margin-bottom:7px"><a href="<?php
                $main = nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix);
                echo $main; ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <a href="<?php echo nbf_cms::$interop->site_page_prefix . "&action=orders&task=view" . nbf_cms::$interop->site_page_suffix; ?>"><?php echo NBILL_MY_ORDERS; ?></a> &gt; <?php echo NBILL_CANCEL_ORDER; ?></div>
                <?php
            } else if ($order_details) {
                ?><div class="pathway" style="margin-bottom:7px"><a href="<?php
                $main = nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix);
                echo $main; ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <a href="<?php echo nbf_cms::$interop->site_page_prefix . "&action=orders&task=view" . nbf_cms::$interop->site_page_suffix; ?>"><?php echo NBILL_MY_ORDERS; ?></a> &gt; <?php echo NBILL_ORDER_DETAILS; ?></div>
                <?php
            } else {
                ?>
                <div class="pathway" style="margin-bottom:7px"><a href="<?php $main = nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix);
                echo $main; ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <?php echo NBILL_MY_ORDERS; ?></div>
                <?php
            }
        }
    }
}