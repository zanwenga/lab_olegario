<?php
/**
* Default HTML output template for order processing pages
* @version 1
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

class nbill_fe_orders
{
    /**
    * Display the list of order forms
    * @param mixed $forms
    */
    public static function show_form_list($forms, $quotes = false, $custom_link_text = null, $custom_intro = null, $custom_action = null, $custom_link_params = '')
    {
        static $order_forms_shown = false;

         //Add support for overlib
        nbf_html::include_overlib_js();

        if (!$quotes)
        {
            $order_forms_shown = true;
        }
        ?>
        <p class="nbill-new-order-intro"><?php echo $quotes ? ($order_forms_shown ? NBILL_OR_NEW_QUOTE_INTRO : NBILL_NEW_QUOTE_INTRO) : ($custom_intro ? $custom_intro : NBILL_NEW_ORDER_INTRO); ?></p>
        <table width="100%" class="contentpane nbill_form_list" cellpadding="3" cellspacing="1" border="0">
            <?php
            foreach ($forms as $form)
            {
                $available = true;
                //if ($form->always_show)
                //{
                    //Check whether this form is available
                    $null = null;
                    $available = nbf_frontend::form_allowed($form->id, $null);
                //}
                $link = nbf_cms::$interop->site_page_prefix . "&action=" . ($custom_action ? $custom_action : ($quotes ? "quotes" : "orders")) . "&task=order&cid=$form->id" . $custom_link_params . nbf_cms::$interop->site_page_suffix;
                $url = nbf_cms::$interop->process_url($link);
                if (@$_SERVER['HTTPS'] || @$_SERVER['SERVER_PORT'] == 443)
                {
                    $url = str_replace("http://", "https://", $url);
                }
                if ($available)
                {
                    ?>
                    <tr class="nbill_form_list_row">
                        <td><a href="<?php echo $url; ?>" title="<?php echo $custom_link_text ? $custom_link_text : NBILL_PLACE_ORDER; ?>"><?php echo $form->title; ?> </a></td>
                        <?php if (is_callable(array("nbill_order_after_title", 'render_row')))
                        {
                            call_user_func(array("nbill_order_after_title", 'render_row'), $form);
                        } ?>
                    </tr>
                    <?php
                }
                else if ($form->always_show)
                {
                    //Show greyed out, with hover text showing the unavailable message
                    ?>
                    <tr class="nbill_form_list_row nbill_form_list_row_disabled">
                        <td class="nbill-link-disabled"><?php echo '<a href="' . $url . '" onclick="overlibToggle = !overlibToggle;if (overlibToggle){this.onmouseover();}else{this.onmouseout();}return false;" onmouseover="return nbill_overlib(\'' . htmlentities(strlen($form->form_unavailable_message) > 0 ? nbf_common::parse_and_execute_code($form->form_unavailable_message, true) : NBILL_ERR_FORM_NOT_DEFINED) . '\');" onmouseout="return nbill_overlib_nd();">' . $form->title . '</a>'; ?></td>
                        <?php if (is_callable(array("nbill_order_after_title", 'render_row')))
                        {
                            call_user_func(array("nbill_order_after_title", 'render_row'), $form);
                        } ?>
                    </tr>
                    <?php
                }
            } ?>
        </table>
        <?php
    }

    /**
    * Display error message and login box if someone tries to acces a form that is not available to them
    */
    public static function form_not_allowed($custom_message)
    {
        if (nbf_common::nb_strlen($custom_message) == 0)
        {
            $custom_message = NBILL_ERR_FORM_NOT_DEFINED;
        }
        ?><p class="nbill_form_not_allowed_message"><?php echo $custom_message; ?></p><?php
        nbill_show_login_box(false, false);
    }

    /**
    * Render the order form
    * @param mixed $form Order form database object
    * @param mixed $page Page object for the page to be rendered
    * @param mixed $fields All of the fields for ALL pages on the form
    * @param mixed $field_options List options for radio list and dropdown list fields
    * @param mixed $sql_field_options List options loaded from database for radio list and dropdown list fields
    */
    public static function form_display($form_height, &$form, &$page, &$fields, &$field_options, &$sql_field_options, $show_previous, $extra_form_output = array())
    {
        nbf_html::include_overlib_js();
        self::_show_title($form->title);
        if (nbf_common::nb_strlen($page->intro) > 0)
        {
            ?>
            <div id="nbill-page-intro"><?php echo $page->intro; ?></div>
            <?php
        }
        if ($page->renderer == 2)
        { ?>
            <div class="billing-fields">
                <?php
                if (isset($extra_form_output[@$page->page_no]))
                {
                    if ($extra_form_output[$page->page_no]['before_fields'])
                    {
                        echo $extra_form_output[$page->page_no]['output'];
                    }
                }
                foreach ($fields as &$field)
                {
                    self::_render_field($page, $field, $field_options, $sql_field_options, "", $field->page_no == $page->page_no, false, true);
                }
                if (isset($extra_form_output[@$page->page_no]))
                {
                    if (!$extra_form_output[$page->page_no]['before_fields'])
                    {
                        echo $extra_form_output[$page->page_no]['output'];
                    }
                }
                ?>
                <div class="nbill-order-form-buttons">
                    <?php if ($show_previous) {
                        nbf_html::show_previous_button($form->id, $page, false, false, true);
                    } ?>
                    <?php nbf_html::show_next_button($form->id, $page, false, false, true); ?>
                </div>
                <div style="clear:both;"></div>
            </div>
            <?php
        }
        else if ($page->renderer == 1)
        {
            ?>
            <div class="billing-fields">
                <table class="contentpane nbill_form_table" cellpadding="3" cellspacing="1" border="0" style="margin-top:5px;width:auto;">
                    <?php
                    if (isset($extra_form_output[@$page->page_no]))
                    {
                        if ($extra_form_output[$page->page_no]['before_fields'])
                        {
                            echo $extra_form_output[$page->page_no]['output'];
                        }
                    }
                    foreach ($fields as &$field)
                    {
                        self::_render_field($page, $field, $field_options, $sql_field_options, "", $field->page_no == $page->page_no, true);
                    }
                    if (isset($extra_form_output[@$page->page_no]))
                    {
                        if (!$extra_form_output[$page->page_no]['before_fields'])
                        {
                            echo $extra_form_output[$page->page_no]['output'];
                        }
                    }
                    ?>
                    <tr class="nbill_form_prev_next_buttons">
                        <?php if ($show_previous)
                        { ?>
                            <td style="text-align:left;">
                                <?php nbf_html::show_previous_button($form->id, $page, false, true); ?>
                            </td>
                        <?php } ?>
                        <td<?php if (!$show_previous) {echo ' colspan="2"';} ?> style="text-align:right;">
                            <?php nbf_html::show_next_button($form->id, $page, false, true); ?>
                        </td>
                    </tr>
                </table>
            </div>
            <?php
        }
        else
        { ?>
            <!--[if lte IE 7]>
              <style type="text/css">
                #nbill_form_spacer {position:relative;}
                .nbill_field {width:100%;}
              </style>
            <![endif]-->

            <div id="nbill_form_spacer" style="height:<?php echo $form_height + 25; ?>px;min-width:<?php echo $page->min_width; ?>px;width:auto !important;width:<?php echo $page->min_width; ?>px;">
                <div id="nbill_form" style="position:relative;min-width:<?php echo $page->min_width; ?>px;width:auto !important;width:<?php echo $page->min_width; ?>px;">
                    <?php
                    //Add next button (add it first so that it becomes the default button when the enter key is pressed - in most browsers)
                    if (isset($extra_form_output[@$page->page_no]))
                    {
                        $y_pos = $page->next_y_pos;
                        $page->next_y_pos += $extra_form_output[@$page->page_no]['extra_height'];
                        if ($page->prev_y_pos == $y_pos)
                        {
                            $page->prev_y_pos = $page->next_y_pos;
                        }
                    }
                    nbf_html::show_next_button($form->id, $page);

                    //If there is any extra form output to show before the custom fields, show it now...
                    if (isset($extra_form_output) && count($extra_form_output) > 0)
                    {
                        foreach ($extra_form_output as $extra_page_no=>$page_output)
                        {
                            if (is_numeric($extra_page_no) && $page_output['before_fields'])
                            {
                                echo '<div id="extra_form_output_page_' . $extra_page_no . '"' . ($extra_page_no == $page->page_no ? '' : 'style="display:none;"') . '>';
                                echo $extra_form_output[$extra_page_no]['output'];
                                echo '</div>';
                                if ($extra_page_no == $page->page_no)
                                {
                                    //Move all other fields down
                                    foreach ($fields as &$other_field)
                                    {
                                        if ($other_field->page_no == $page->page_no)
                                        {
                                            $other_field->y_pos += $extra_form_output[@$page->page_no]['extra_height'];
                                        }
                                    }
                                }
                            }
                        }
                    }

                    foreach ($fields as $field)
                    {
                        self::_render_field($page, $field, $field_options, $sql_field_options, "", $field->page_no == $page->page_no);
                    }

                    //If there is any extra form output to show after the custom fields, show it now...
                    if (isset($extra_form_output) && count($extra_form_output) > 0)
                    {
                        foreach ($extra_form_output as $extra_page_no=>$page_output)
                        {
                            if (is_numeric($extra_page_no) && !$page_output['before_fields'])
                            {
                                echo '<div id="extra_form_output_page_' . $extra_page_no . '" style="' . ($extra_page_no == $page->page_no ? 'position:relative;top:' . $y_pos . 'px;' : 'display:none;') . '">';
                                echo $extra_form_output[$extra_page_no]['output'];
                                echo '</div>';
                            }
                        }
                    }

                    //Add previous button, if applicable
                    if ($show_previous)
                    {
                        nbf_html::show_previous_button($form->id, $page);
                    }
                    ?>
                </div>
            </div>
        <?php }
        if (nbf_common::nb_strlen($page->footer) > 0)
        {
            ?>
            <div id="nbill-page-footer" style="margin-bottom:25px;"><?php echo $page->footer; ?></div>
            <?php
        }
    }

    /**
    * If there is a message to display, display it (typically a success or failure message after renewing an order)
    * @param mixed $message
    */
    public static function show_message($message)
    {
        if (nbf_common::nb_strlen($message) > 0)
        {
            echo $message;
        }
        else
        {
            if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
            {
                ?><div class="nbill-message"><?php echo nbf_globals::$message; ?></div><?php
            }
        }
    }

    /**
    * Display the renewal confirmation screen
    * @param array $existing_orders The order record(s) as held in the database
    * @param array $orders The new orders array for the forthcoming payment
    * @param string $currency
    * @param string $order_summary_total The summary of totals as rendered by the summary table field control
    * @param string $date_format
    * @param array $gateways The available gateways for payment
    * @param string $default_gateway
    */
    public static function renewal_display(&$existing_orders, &$orders, &$currency, &$order_summary_total, &$date_format, $gateways, $default_gateway, $frequencies = array(), $current_frequency = "")
    {
        ?>
        <table cellpadding="3" cellspacing="0" border="0" class="nbill_summary_table nbill_order_renewal">
            <tr class="nbill_order_renewal_row nbill_order_number">
                <td class="field-title"><?php echo NBILL_ORDER_NUMBER; ?></td>
                <td class="field-value">
                <?php
                    $order_nos = "";
                    foreach ($existing_orders as $existing_order)
                    {
                        if (nbf_common::nb_strlen($order_nos) > 0)
                        {
                            $order_nos .= ", ";
                        }
                        $order_nos .= $existing_order->order_no;
                        if ($existing_order->next_due_date)
                        {
                            if (nbf_common::get_param($_REQUEST, 'show_due'))
                            {
                                if (time() > $existing_order->next_due_date)
                                {
                                    $order_nos .= "<span style=\"color: #ff0000;\">";
                                }
                                $order_nos .= " (" . NBILL_DUE . " " . nbf_common::nb_date($date_format, $existing_order->next_due_date);
                                if ($existing_order->expiry_date > 0 && $existing_order->expiry_date < nbf_common::nb_time())
                                {
                                    $order_nos .= ", " . NBILL_EXPIRED . " " . nbf_common::nb_date($date_format, $existing_order->expiry_date);
                                }
                                $order_nos .= ")";
                                if (time() > $existing_order->next_due_date)
                                {
                                    $order_nos .= "</span>";
                                }
                            }
                        }
                        else
                        {
                            if ($existing_order->expiry_date > 0 && $existing_order->expiry_date < nbf_common::nb_time())
                            {
                                $order_nos .= " (" . NBILL_EXPIRED . " " . nbf_common::nb_date($date_format, $existing_order->expiry_date) . ")";
                            }
                        }
                    }
                    echo $order_nos;
                ?>
                </td>
            </tr>
            <?php
            if ($frequencies && count($frequencies) > 1)
            {
                ?>
                <tr class="nbill_order_renewal_row nbill_order_renewal_freqency">
                    <td class="field-title"><?php echo NBILL_PAY_FREQUENCY; ?></td>
                    <td class="field-value">
                        <input type="hidden" name="orig_pay_freq" id="orig_pay_freq" value="<?php echo $current_frequency; ?>" />
                        <div id="current_pay_freq">
                        <?php
                        foreach($frequencies as $code=>$description)
                        {
                            if ($code == $current_frequency)
                            {
                                echo $description;
                                break;
                            }
                        }
                        ?>
                        <input type="button" id="change_pay_freq" class="button nbill-button" value="<?php echo NBILL_CHANGE; ?>" onclick="document.getElementById('current_pay_freq').style.display='none';document.getElementById('changed_pay_freq').style.display='';" />
                        </div>
                        <div id="changed_pay_freq" style="display:none;">
                        <?php
                        $frequency_list = array();
                        foreach($frequencies as $code=>$description)
                        {
                            $frequency_list[] = nbf_html::list_option($code, $description);
                        }
                        echo nbf_html::select_list($frequency_list, "payment_frequency", 'id="payment_frequency" class="inputbox nbill_control"', $current_frequency);
                        ?>
                        <input type="submit" name="change_freq" id="change_freq" value="<?php echo NBILL_UPDATE; ?>" class="button btn nbill-button" />
                        </div>
                    </td>
                <?php
            }

            echo $order_summary_total; //This has been rendered by the summary table field control
            if (nbf_frontend::get_display_option("gateway_choice_order"))
            {
                if (count($gateways) > 1)
                {
                    //Allow selection of payment gateway
                    ?>
                    <tr class="nbill_order_renewal_row nbill_order_renewal_gateway">
                        <td><?php echo NBILL_SELECT_GATEWAY; ?></td>
                        <td align="right" class="nbill-gateway-select">
                            <?php
                            $gateway_list = array();
                            foreach($gateways as $gateway)
                            {
                                $gateway_list[] = nbf_html::list_option($gateway->code, $gateway->description);
                            }
                            echo nbf_html::select_list($gateway_list, "payment_gateway", 'id="payment_gateway" class="inputbox"', $default_gateway);
                            ?>
                        </td>
                    </tr>
                    <?php
                }
                else
                {
                    ?>
                    <input type="hidden" name="payment_gateway" id="payment_gateway" value="<?php echo $default_gateway; ?>" />
                    <?php
                }
            }
            ?>
            <tr class="order_renewal_row order_renewal_submit">
                <td colspan="2" class="nbill-renew-submit">
                    <input type="submit" name="sumbit_renew" id="submit_renew" value="<?php echo NBILL_RENEW_SUBMIT; ?>" class="button btn nbill-button" />
                </td>
            </tr>
        </table>
        <?php
    }

    /**
    * Displays the breadcrumb path, if the display options require it
    * @param string $form_title Title of current form, or nothing if we are on the list of forms
    */
    public static function show_pathway($form_title = "", $renewal = false, $quotes = false, $custom_path = null)
    {
        if (nbf_frontend::get_display_option("pathway"))
        {
            if ($custom_path)
            {
                echo $custom_path;
            }
            else if (nbf_cms::$interop->user->id)
            {
                if ($quotes || nbf_common::get_param($_REQUEST, 'action') == "quotes")
                {
                    ?>
                    <div class="pathway" style="margin-bottom:7px"><a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix); ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <a href="<?php $url = nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=quotes' . nbf_cms::$interop->site_page_suffix); if (@$_SERVER['HTTPS'] || @$_SERVER['SERVER_PORT'] == 443) echo str_replace("http://", "https://", $url); else echo $url;?>"><?php echo NBILL_MY_QUOTES; ?></a> &gt;
                    <?php
                    if (nbf_common::nb_strlen($form_title) > 0)
                    {
                        ?> <a href="<?php echo nbf_cms::$interop->site_page_prefix . '&action=quotes&task=new' . nbf_cms::$interop->site_page_suffix; ?>"><?php
                    }
                    echo NBILL_FE_NEW_QUOTE;
                    if (nbf_common::nb_strlen($form_title) > 0)
                    {
                        ?></a> &gt; <?php echo $form_title;
                    }
                }
                else
                {
                    ?>
                    <div class="pathway" style="margin-bottom:7px"><a href="<?php
                        $main = nbf_cms::$interop->process_url(nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_page_prefix . nbf_cms::$interop->site_page_suffix);
                        echo $main; ?>"><?php echo NBILL_MAIN_MENU; ?></a> &gt; <a href="<?php $url = nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=orders&task=view' . nbf_cms::$interop->site_page_suffix); if (@$_SERVER['HTTPS'] || @$_SERVER['SERVER_PORT'] == 443) echo str_replace("http://", "https://", $url); else echo $url;?>"><?php echo NBILL_MY_ORDERS; ?></a> &gt;
                    <?php
                    if ($renewal)
                    {
                        echo NBILL_ORDER_RENEW_TITLE;
                    }
                    else
                    {
                        if (nbf_common::nb_strlen($form_title) > 0)
                        {
                            ?> <a href="<?php echo nbf_cms::$interop->site_page_prefix . '&action=orders&task=new' . nbf_cms::$interop->site_page_suffix; ?>"><?php
                        }
                        echo NBILL_FE_NEW_ORDER;
                        if (nbf_common::nb_strlen($form_title) > 0)
                        {
                            ?></a> &gt; <?php echo $form_title;
                        }
                    }
                }
            ?>
            </div>
            <?php
            }
        }
    }

    /**
    * If you don't want the form title shown, create an override for this function in the orders.custom.html.php file
    */
    protected static function _show_title($form_title)
    {
        ?><h2 class="componentheading nbill_form_title"><?php echo $form_title; ?></h2><?php
    }

    /**
    * Renders the field and returns the height allowance
    */
    protected static function _render_field(&$page, $field, &$field_options, &$sql_field_options, $suffix = "", $on_current_page = false, $table_renderer = false, $responsive_renderer=false)
    {
        if ($field->published == 1 || ($field->published == 2 && nbf_common::get_param($_REQUEST, 'nbill_entity_id')) || ($field->published == 3 && !nbf_common::get_param($_REQUEST, 'nbill_entity_id')))
        {
            $control =& nbf_form_fields::create_control($field, array_merge(array_filter($field_options[$field->id]), array_filter($sql_field_options[$field->id])) + $field_options[$field->id] + $sql_field_options[$field->id]);
            if ($responsive_renderer) {
                $control->renderer = 2;
                ?>
                <div class="nbill-field-responsive" style="<?php if (!$on_current_page || substr($field->field_type, 0, 1) == 'H') {echo 'display:none;';} else {echo 'left:' . $field->x_pos . 'px;';} if ($field->override_absolute) {echo 'top:' . $field->y_pos . 'px;position:absolute;'; } ?>">
                    <div id="row_<?php echo $field->name; ?>" class="nbill-field-container">
                        <?php if (!$field->merge_columns)
                        { ?>
                            <label class="field-title nbill-field-label" for="ctl_<?php echo $field->id; ?>" id="lbl_ctl_<?php echo $field->id; ?>" style="width:<?php echo $page->label_width; ?>px;"><?php echo $field->label ? ((defined(str_replace("* ", "", $field->label)) ? (nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . constant(str_replace("* ", "", $field->label)) : $field->label)) : "&nbsp;"; ?></label>
                        <?php } ?>
                        <div class="field-value">
                            <?php if ($field->merge_columns)
                            { ?>
                                <span id="lbl_ctl_<?php echo $field->id; ?>" class="nbill-field-label-merged" style="float:left"><?php echo $field->label ? ((defined(str_replace("* ", "", $field->label)) ? (nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . constant(str_replace("* ", "", $field->label)) : $field->label)) : "&nbsp;"; ?></span>
                            <?php } ?>
                            <div id="val_<?php echo $field->id; ?>" class="nbill_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                                <div id="pre_field_<?php echo $field->id; ?>" class="nbill-field-pre" style="display:inline;"><?php
                                //Execute any code needed to retrieve the pre_field value
                                echo nbf_common::parse_and_execute_code($field->pre_field);
                                ?></div>
                                <div id="rendered_control_<?php echo $field->id; ?>" class="nbill-field-control" style="display:inline;">
                                    <?php
                                    $control->render_control(false, $on_current_page);
                                    ?>
                                </div>
                                <div id="post_field_<?php echo $field->id; ?>" class="nbill-field-post" style="display:inline;"><?php
                                //Execute any code needed to retrieve the pre_field value
                                echo nbf_common::parse_and_execute_code($field->post_field);
                                ?></div>
                                <div id="help_<?php echo $field->id; ?>" class="nbill-field-help" style="display:inline;vertical-align:top;"><?php
                                if (nbf_common::nb_strlen($field->help_text) > 0)
                                {
                                    $field->help_text = nbf_common::parse_and_execute_code($field->help_text);
                                    nbf_html::show_overlib($field->help_text);
                                } ?></div>
                            </div>
                        </div>
                    </div>
                    <?php if ($field->confirmation)
                    {
                        ?>
                        <div id="row_<?php echo $field->name; ?>_confirm" class="nbill-field-container-confirm">
                            <?php if (!$field->merge_columns)
                            { ?>
                                <label class="field-title nbill-field-label" for="ctl_<?php echo $field->id; ?>_confirm" style="width:<?php echo $page->label_width; ?>px;">
                                    <span id="lbl_ctl_<?php echo $field->id; ?>_confirm"><?php echo $field->label ? sprintf((nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . NBILL_CONFIRM_LABEL, ((defined(str_replace("* ", "", $field->label)) ? constant(str_replace("* ", "", $field->label)) : str_replace("* ", "", $field->label)))) : sprintf(NBILL_CONFIRM_LABEL, "&nbsp;"); ?></span>
                                </label>
                            <?php } ?>
                            <div class="field-value">
                                <?php
                                if ($field->merge_columns)
                                { ?>
                                    <span id="lbl_ctl_<?php echo $field->id; ?>_confirm" class="nbill-field-label-merged"><?php echo $field->label ? sprintf((nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . NBILL_CONFIRM_LABEL, ((defined(str_replace("* ", "", $field->label)) ? constant(str_replace("* ", "", $field->label)) : str_replace("* ", "", $field->label)))) : sprintf(NBILL_CONFIRM_LABEL, "&nbsp;"); ?></span>
                                <?php } ?>
                                <div id="conf_val_<?php echo $field->id; ?>" class="nbill_value nbill_confirm_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                                    <?php
                                    $control->suffix = "_confirm";
                                    $control->value = nbf_common::get_param($_REQUEST, 'ctl_' . $field->name . '_confirm');
                                    $control->render_control(false, $on_current_page);
                                    ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <?php
            }
            else if ($table_renderer)
            {
                $control->renderer = 1;
                ?>
                <tr id="row_<?php echo $field->name; ?>"<?php if (!$on_current_page || substr($field->field_type, 0, 1) == 'H') {echo ' style="display:none;"';} ?> class="nbill-field-tr">
                    <?php if (!$field->merge_columns)
                    { ?>
                    <td class="field-title nbill-field-label-td" align="left">
                        <span id="lbl_ctl_<?php echo $field->id; ?>" class="nbill-field-label"><?php echo $field->label ? ((defined(str_replace("* ", "", $field->label)) ? (nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . constant(str_replace("* ", "", $field->label)) : $field->label)) : "&nbsp;"; ?></span>
                    </td>
                    <?php } ?>
                    <td class="field-value nbill-field-value-td" align="left"<?php if ($field->merge_columns) {echo ' colspan="2"';} ?>>
                        <?php if ($field->merge_columns)
                        { ?>
                            <span id="lbl_ctl_<?php echo $field->id; ?>" style="float:left" class="nbill-field-label-merged"><?php echo $field->label ? ((defined(str_replace("* ", "", $field->label)) ? (nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . constant(str_replace("* ", "", $field->label)) : $field->label)) : "&nbsp;"; ?></span>
                        <?php } ?>
                        <div id="val_<?php echo $field->id; ?>" class="nbill_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                            <div id="pre_field_<?php echo $field->id; ?>" class="nbill-field-pre" style="display:inline;"><?php
                            //Execute any code needed to retrieve the pre_field value
                            echo nbf_common::parse_and_execute_code($field->pre_field);
                            ?></div>
                            <div id="rendered_control_<?php echo $field->id; ?>" class="nbill-field-control" style="display:inline;">
                                <?php
                                $control->render_control(false, $on_current_page);
                                ?>
                            </div>
                            <div id="post_field_<?php echo $field->id; ?>" class="nbill-field-post" style="display:inline;"><?php
                            //Execute any code needed to retrieve the pre_field value
                            echo nbf_common::parse_and_execute_code($field->post_field);
                            ?></div>
                            <div id="help_<?php echo $field->id; ?>" class="nbill-field-help" style="display:inline;vertical-align:top;"><?php
                            if (nbf_common::nb_strlen($field->help_text) > 0)
                            {
                                $field->help_text = nbf_common::parse_and_execute_code($field->help_text);
                                nbf_html::show_overlib($field->help_text);
                            } ?></div>
                        </div>
                    </td>
                </tr>
                <?php if ($field->confirmation)
                {
                    ?>
                    <tr id="row_<?php echo $field->name; ?>_confirm"<?php if (!$on_current_page) {echo ' style="display:none;"';} ?> class="nbill-field-tr-confirm">
                        <?php if (!$field->merge_columns)
                        { ?>
                            <td class="field-title" align="left" class="nbill-field-label-confirm-td">
                                <span id="lbl_ctl_<?php echo $field->id; ?>_confirm"><?php echo $field->label ? sprintf((nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . NBILL_CONFIRM_LABEL, ((defined(str_replace("* ", "", $field->label)) ? constant(str_replace("* ", "", $field->label)) : str_replace("* ", "", $field->label)))) : sprintf(NBILL_CONFIRM_LABEL, "&nbsp;"); ?></span>
                            </td>
                        <?php } ?>
                        <td class="field-value" class="nbill-field-value-confirm-td" align="left"<?php if ($field->merge_columns) {echo ' colspan="2"';} ?>>
                            <?php
                            if ($field->merge_columns)
                            { ?>
                                <span id="lbl_ctl_<?php echo $field->id; ?>_confirm" class="nbill-field-label-confirm-merged"><?php echo $field->label ? sprintf((nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . NBILL_CONFIRM_LABEL, ((defined(str_replace("* ", "", $field->label)) ? constant(str_replace("* ", "", $field->label)) : str_replace("* ", "", $field->label)))) : sprintf(NBILL_CONFIRM_LABEL, "&nbsp;"); ?></span>
                            <?php } ?>
                            <div id="conf_val_<?php echo $field->id; ?>" class="nbill_value nbill_confirm_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                            <?php
                            $control->suffix = "_confirm";
                            $control->value = nbf_common::get_param($_REQUEST, 'ctl_' . $field->name . '_confirm');
                            $control->render_control(false, $on_current_page);
                            ?>
                            </div>
                        </td>
                    </tr>
                    <?php
                }
            }
            else
            {
                ?>
                <div id="fld_<?php echo $field->id; ?>" class="nbill_field" style="<?php if (!$on_current_page || substr($field->field_type, 0, 1) == 'H'){echo "display:none;";} ?>top:<?php echo $field->y_pos; ?>px;left:<?php echo $field->x_pos; ?>px;height:auto;<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo "z-index:1000;";} ?>">
                    <div id="lbl_<?php echo $field->id; ?>" class="nbill_label" style="width:<?php if ($field->merge_columns) {echo "auto !important;";} else {echo $page->label_width . "px;";} ?>"><span id="lbl_ctl_<?php echo $field->id; ?>"><?php echo $field->label ? ((defined(str_replace("* ", "", $field->label)) ? (nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . constant(str_replace("* ", "", $field->label)) : $field->label)) : "&nbsp;"; ?></span></div>
                    <div id="val_<?php echo $field->id; ?>" class="nbill_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                        <div id="pre_field_<?php echo $field->id; ?>" class="nbill-field-pre" style="display:inline;"><?php
                        //Execute any code needed to retrieve the pre_field value
                        echo nbf_common::parse_and_execute_code($field->pre_field);
                        ?></div>
                        <div id="rendered_control_<?php echo $field->id; ?>" class="nbill-field-control" style="display:inline;">
                            <?php
                            $control->render_control(false, $on_current_page);
                            ?>
                        </div>
                        <div id="post_field_<?php echo $field->id; ?>" class="nbill-field-post" style="display:inline;"><?php
                        //Execute any code needed to retrieve the pre_field value
                        echo nbf_common::parse_and_execute_code($field->post_field);
                        ?></div>
                        <div id="help_<?php echo $field->id; ?>" class="nbill-field-help" style="display:inline;vertical-align:top;"><?php
                        if (nbf_common::nb_strlen($field->help_text) > 0)
                        {
                            $field->help_text = nbf_common::parse_and_execute_code($field->help_text);
                            nbf_html::show_overlib($field->help_text);
                        } ?></div>
                    </div>
                    <?php if ($field->confirmation)
                    { ?>
                        <div id="confirm_<?php echo $field->id; ?>">
                            <div id="conf_lbl_<?php echo $field->id; ?>" class="nbill_label nbill_confirm_label" style="width:<?php if ($field->merge_columns) {echo "auto !important;";} else {echo $page->label_width . "px;";} ?>"><label id="conf_lbl_ctl_<?php echo $field->id; ?>" for="ctl_<?php echo $field->id; ?>_confirm"><?php echo $field->label ? sprintf((nbf_common::nb_strpos($field->label, "* ") !== false ? "* " : "") . NBILL_CONFIRM_LABEL, ((defined(str_replace("* ", "", $field->label)) ? constant(str_replace("* ", "", $field->label)) : str_replace("* ", "", $field->label)))) : sprintf(NBILL_CONFIRM_LABEL, "&nbsp;"); ?></label></div>
                            <div id="conf_val_<?php echo $field->id; ?>" class="nbill_value nbill_confirm_value<?php if (array_search('ctl_' . $field->name . $suffix, nbf_globals::$fields_in_error) !== false) {echo " field_in_error";} ?>">
                            <?php
                            $control->suffix = "_confirm";
                            $control->value = nbf_common::get_param($_REQUEST, 'ctl_' . $field->name . '_confirm');
                            $control->render_control(false, $on_current_page);
                            ?>
                            </div>
                        </div>
                    <?php } ?>
                </div>
                <?php
                //Return height
                $option_count = $control->horizontal_options ? 1 : count($field_options) + count($sql_field_options);
                return $control->height_allowance + ($control->option_height_allowance * ($option_count - 1));
            }
        }
        return 0;
    }
}