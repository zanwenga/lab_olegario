<?php
//Ensure this file has been reached through a valid entry point
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

?>
<!DOCTYPE html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0">
<meta http-equiv="Content-Type" content="text/html; charset=<?php echo nbf_cms::$interop->char_encoding; ?>" />
<title><?php echo nbf_common::parse_translation($client_lang, "email", 'NBILL_EM_NEW_QUOTE_REQUEST'); ?></title>
<style type="text/css">
body, td
{
    font-family: Arial, Helvetica, sans-serif;
    font-size: 10pt;
}
#nbill_summary_table
{
    width: auto;
    min-width: 500px;
    border-collapse: collapse;
}
#nbill_summary_table th, #nbill_summary_table td
{
    border: solid 1px #cccccc;
    padding: 3px;
    padding-top: 5px;
    padding-bottom: 5px;
    line-height: 1;
}
.nbill_summary_sub_heading
{
    background-color: #dddddd;
    text-align: left;
    vertical-align: middle;
    font-weight: bold;
    line-height: 1;
}
.nbill_summary_last_row
{
    border-bottom: solid 1px #000000 !important;
}
</style>
</head>
<body>
    <table cellpadding="3" cellspacing="0" border="0">
        <tr>
            <?php if (file_exists(nbf_cms::$interop->nbill_fe_base_path . "/images/vendors/$vendor_id.gif") || file_exists(nbf_cms::$interop->nbill_fe_base_path . "/images/vendors/$vendor_id.png")) { ?><th align="left"><img src="<?php echo nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix; ?>&action=show_image&file_name=vendors/<?php echo $vendor_id; if (file_exists(nbf_cms::$interop->nbill_fe_base_path . "/images/vendors/$vendor_id.gif")){echo ".gif";}else{echo ".png";} echo nbf_cms::$interop->public_site_page_suffix(); ?>" alt="<?php echo nbf_cms::$interop->site_name; ?> Logo" /></th><?php } ?>
            <th align="left"><h3><?php echo nbf_cms::$interop->site_name; ?> - <?php echo nbf_common::parse_translation($client_lang, "email", 'NBILL_EM_NEW_QUOTE_REQUEST'); ?></h3></th>
        </tr>
        <tr>
            <td align="left" colspan="2">
                <p><?php echo sprintf(nbf_common::parse_translation($client_lang, "email", 'NBILL_EM_GREETING'), $contact_name); ?></p>
                <p><?php echo sprintf(nbf_common::parse_translation($client_lang, "email", 'NBILL_EM_NEW_QUOTE_REQUEST_PAR_1'), nbf_cms::$interop->live_site); ?></p>
                <div style="margin-top:7px;margin-bottom:7px;">
                    <?php echo $order_summary; ?>
                </div>
                <p><?php echo nbf_common::parse_translation($client_lang, "email", 'NBILL_EM_REGARDS'); ?><br />
                <?php echo nbf_cms::$interop->site_name; ?></p>
            </td>
        </tr>
    </table>
</body>
</html>