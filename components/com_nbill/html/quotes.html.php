<?php
/**
* HTML output for quotes
* @version 2
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

require_once(nbill_framework_locator::find_framework() . "/classes/nbill.html.class.php");
require_once(nbf_cms::$interop->nbill_fe_base_path . "/pages/quotes/quotes.custom.html.php");

class nBillFrontEndQuotes
{
    public static function show_quotes($rows, $first_description, $date_format)
    {
        $fe_quotes = new nbill_fe_quotes_custom();

        if (nbf_frontend::get_display_option("new_quote_link"))
        {?>
            <div style="float:right" class="nbill-new-quote-link"><a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=quotes&task=new' . nbf_cms::$interop->site_page_suffix); ?>"><?php echo NBILL_CLIENT_NEW_QUOTE; ?></a></div>
            <?php
        }

        $fe_quotes->show_pathway();
        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
        }

        $css = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.css");
        nbf_cms::$interop->add_html_header('<style type="text/css">' . $css . '</style>');
        echo "<script type=\"text/javascript\">" . "\n/*<![CDATA[*/\nvar pathToImages = '" . nbf_cms::$interop->live_site . "/" . nbf_cms::$interop->site_popup_page_prefix . "&action=show_image&file_name=../calendar/images/';" . "\n/*]]>*/\n</script>";
        $js = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/calendar/dhtmlgoodies_calendar/dhtmlgoodies_calendar.js");
        nbf_cms::$interop->add_html_header('<script type="text/javascript">' . "\n/*<![CDATA[*/\n" . $js . "\n/*]]>*/\n" . '</script>');
        ?>

        <form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="quotes" id="quotes">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="action" value="quotes" />
            <input type="hidden" name="task" value="" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
            <?php
            //Output the main body of the page
            $fe_quotes->show_quotes($rows, $first_description, $date_format);
            ?>
        </form>
        <?php
    }

    public static function edit_quote($row, $items, $date_format)
    {
        $css = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/style/nbill_quotes.css");
        nbf_cms::$interop->add_html_header('<style type="text/css">' . $css . '</style>');

        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
        }
        ?>

        <form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="quotes" id="quotes">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="action" value="quotes" />
            <input type="hidden" name="task" value="edit" />
            <input type="hidden" name="cid" value="<?php echo $row->document_id; ?>" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
            <?php
            //Output the main body of the page
            $fe_quotes->edit_quote($row, $items, $date_format);
            ?>
        </form>
        <?php
    }

    public static function quote_rejected_successfully()
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        $fe_quotes->quote_rejected_successfully();
    }

    public static function quote_part_rejected_successfully()
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        $fe_quotes->quote_part_rejected_successfully();
    }

    public static function quote_accepted_successfully($invoice_generated = false)
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        $fe_quotes->quote_accepted_successfully($invoice_generated);
    }

    public static function quote_nothing_new_accepted()
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        $fe_quotes->quote_nothing_new_accepted();
    }

    public static function show_quote_payment_summary($quote_id, $quote_no, $new_accepts, $select_gateway, $gateways, $gateway, $quote_summary_total, $additional_payment_required, $voucher_available = false)
    {
        $fe_quotes = new nbill_fe_quotes_custom();

        //Load the stylesheet
        $css = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/style/nbill_forms.css");
        nbf_cms::$interop->add_html_header('<style type="text/css">' . $css . '</style>');

        if (nbf_cms::$interop->user->id && nbf_frontend::get_display_option("pathway"))
        {
            $fe_quotes->show_pathway(true);
        }

        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
        }
        echo "<h2 class=\"componentheading\">" . NBILL_PAY_QUOTE_TITLE . "</h2>";
        echo "<p align=\"left\" class=\"nbill-renew-intro\">" . NBILL_PAY_QUOTE_INTRO . "</p>";
        if ($additional_payment_required)
        {
            echo "<p align=\"left\" class=\"nbill-renew-intro\">" . NBILL_PAY_QUOTE_INTRO_ADDITIONAL_PAYMENT_REQD . "</p>";
        }
        ?>
        <form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="select_gateway" id="select_gateway">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="action" value="quotes" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
            <input type="hidden" name="task" value="edit" />
            <input type="hidden" name="cid" value="<?php echo $quote_id; ?>" />
            <input type="hidden" name="nbill_quote_submit" value="1" />
            <?php
            foreach ($new_accepts as $new_accept)
            {
                ?>
                <input type="hidden" name="nbill_quote_accept_<?php echo $new_accept; ?>" value="1" />
                <?php
            }
            //Output the main body of the page
            $fe_quotes->show_quote_payment_summary($quote_no, $select_gateway, $gateways, $gateway, $quote_summary_total, $voucher_available);
            ?>
        </form>
        <?php
    }

    public static function show_message($message = "", $editing_quote = false)
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway($editing_quote);
        $fe_quotes->show_message($message);
    }

    public static function pay_offline($message = "")
    {
        $fe_quotes = new nbill_fe_quotes_custom();
        $fe_quotes->show_pathway(true);
        $fe_quotes->pay_offline($message);
    }
}