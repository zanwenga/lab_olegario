<?php
/**
* HTML output for pending orders and the order list
* @todo Separate out order list - it does not belong here! But don't put it in order form processor as that will waste memory
* @version 2
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

require_once(nbill_framework_locator::find_framework() . "/classes/nbill.html.class.php");
require_once(nbf_cms::$interop->nbill_fe_base_path . "/pages/pending/pending.custom.html.php");

class nBillFrontEndPending
{
	public static function show_orders($rows, $date_format, $pay_frequencies, $downloadables = array(), $pending_orders)
	{
        include_once(nbf_cms::$interop->nbill_admin_base_path . "/framework/classes/nbill.date.class.php");

        $fe_pending = new nbill_fe_pending_custom();

        if (nbf_frontend::get_display_option("new_order_link"))
        { ?>
            <div style="float:right" class="nbill-new-order-link"><a href="<?php echo nbf_cms::$interop->process_url(nbf_cms::$interop->site_page_prefix . '&action=orders&task=new' . nbf_cms::$interop->site_page_suffix); ?>"><?php echo NBILL_CLIENT_NEW_ORDER; ?></a></div>
            <?php
        }

        $fe_pending->show_pathway();
		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		}
		?>

		<form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="orders" id="orders">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
			<input type="hidden" name="action" value="orders" />
			<input type="hidden" name="task" value="view" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
            <?php
            $fe_pending->show_orders($rows, $date_format, $pay_frequencies, $downloadables, $pending_orders);
            ?>
		</form>
	<?php }

    public static function order_detail($order, $date_format, $field_labels = array(), $field_options = array(), $downloadables = array())
    {
        include_once(nbf_cms::$interop->nbill_admin_base_path . "/framework/classes/nbill.date.class.php");
        $fe_pending = new nbill_fe_pending_custom();
        $fe_pending->show_pathway(false, true);
        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
        }
        $fe_pending->show_order_detail($order, $date_format, $field_labels, $field_options, $downloadables);
    }

	public static function show_order_cancellation($order)
	{
        $fe_pending = new nbill_fe_pending_custom();
        $fe_pending->show_pathway(true);
		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		}
		?>

		<form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="cancel_order" id="cancel_order">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
			<input type="hidden" name="action" value="orders" />
			<input type="hidden" name="task" value="delete" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
			<input type="hidden" name="id" value="<?php echo $order->id; ?>" />
			<?php
            $fe_pending->show_order_cancellation($order);
            ?>
		</form>
	<?php }

	public static function delete_pending_are_you_sure($pending_order_id)
	{?>
		<form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="delete_pending" id="delete_pending">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
			<input type="hidden" name="action" value="pending" />
			<input type="hidden" name="task" value="confirm_delete" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
			<input type="hidden" name="id" value="<?php echo $pending_order_id; ?>" />
			<?php
            $fe_pending = new nbill_fe_pending_custom();
            $fe_pending->delete_pending_are_you_sure();
            ?>
		</form>
	<?php }
}