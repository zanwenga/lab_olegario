<?php
/**
* HTML output for order form processing in front end
* @version 2
* @package nBill
* @copyright (C) 2015 Netshine Software Limited
* @license http://www.nbill.co.uk/eula.html
*
* @access private
* PLEASE NOTE: This is NOT free software. You must purchase a license in order to use this component.
* For more information, see www.nbill.co.uk and the license agreement at www.nbill.co.uk/eula.html
*
* All Rights Reserved. You may make amendments to any unencrypted files for your own use only or
* for the use of your customers if you are a website developer. HOWEVER, you are not permitted to
* re-distribute or re-sell this software in any form without the express permission of the copyright
* holder.
* This software was developed by Netshine Software Limited (www.netshinesoftware.com). Use of this
* software is entirely at your own risk.
*/

//Ensure this file has been reached through a valid entry point (not always necessary eg. for class files, but included on every file to be safe!)
(defined('_VALID_MOS') || defined('_JEXEC') || defined('ABSPATH') || defined('NBILL_VALID_NBF')) or die('Access Denied.');

require_once(nbill_framework_locator::find_framework() . "/classes/nbill.html.class.php");
require_once(nbf_cms::$interop->nbill_fe_base_path . "/pages/orders/orders.custom.html.php");

class nBillFrontEndOrders
{
	public static function show_order_forms($forms, $form_type = 'OR', $custom_link_text = null, $custom_intro = null, $custom_action = null, $custom_path = null, $custom_link_params = '')
	{
        $fe_orders = new nbill_fe_orders_custom();
        $fe_orders->show_pathway("", false, $form_type == 'QU', $custom_path);

		if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
		{
			echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
		}
		?>

		<form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="order_forms" id="order_forms">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
			<input type="hidden" name="action" value="orders" />
			<input type="hidden" name="task" value="new" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && intval($_REQUEST['Itemid']) ? $_REQUEST['Itemid'] : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
			<?php
            $fe_orders = new nbill_fe_orders_custom();
            $fe_orders->show_form_list($forms, $form_type == 'QU', $custom_link_text, $custom_intro, $custom_action, $custom_link_params);
            ?>
		</form>
		<?php
	}

    public static function show_order_and_quote_forms($quote_forms, $forms)
    {
        $fe_orders = new nbill_fe_orders_custom();
        $fe_orders->show_pathway();

        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
        }
        ?>

        <form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="order_forms" id="order_forms">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="action" value="orders" />
            <input type="hidden" name="task" value="new" />
            <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
            <?php
            $fe_orders = new nbill_fe_orders_custom();
            $fe_orders->show_form_list($forms);
            $fe_orders->show_form_list($quote_forms, true, NBILL_CLIENT_NEW_QUOTE);
            ?>
        </form>
        <?php
    }

    public static function order_form_not_allowed($custom_message)
    {
        $fe_orders = new nbill_fe_orders_custom();
        $fe_orders->form_not_allowed($custom_message);
    }

    public static function show_order_form($clients, $selected_client, $form_height, &$form, &$page, &$fields, &$field_options, &$sql_field_options, $show_previous, $extra_form_output = array())
    {
        $fe_orders = new nbill_fe_orders_custom();

        //Load the stylesheet (have to embed in header, as Joomla messes with the URL if we try to link)
        $css = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/style/nbill_forms.css");
        nbf_cms::$interop->add_html_header('<style type="text/css">' . $css . '</style>');

        //Add any external JS files
        if (nbf_common::nb_strlen($page->external_js_files) > 0)
        {
            $js_files = explode("\n", str_replace("\r", "", $page->external_js_files));
            foreach ($js_files as $js_file)
            {
                if (nbf_common::nb_strlen($js_file) > 0)
                {
                    nbf_cms::$interop->add_html_header('<script src="' . $js_file . '" type="text/javascript"></script>');
                }
            }
        }

        //Add any javascript functions
        if (nbf_common::nb_strlen($form->javascript_functions) > 0)
        {
            nbf_cms::$interop->add_html_header("<script type=\"text/javascript\" language=\"javascript\">\n/*<![CDATA[*/\n" . $form->javascript_functions . "\n/*]]>*/\n</script>");
        }

        $custom_path = (isset($extra_form_output[@$page->page_no]) && array_key_exists('custom_path', $extra_form_output[@$page->page_no])) ? $extra_form_output[@$page->page_no]['custom_path'] : null;
        $fe_orders->show_pathway($form->title, false, $form->form_type == "QU", $custom_path);

        if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
        {
            ?>
            <div class="nbill-message"><?php echo nbf_globals::$message; ?></div>
            <?php
        }
        ?>
        <div id="nbill_form_container">
            <form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="order_form" id="order_form" enctype="multipart/form-data">
            <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
            <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
            <input type="hidden" name="Itemid" value="<?php
            echo intval(@$_REQUEST['Itemid']);
            ?>" />
            <input type="hidden" name="action" value="<?php echo nbf_common::get_param($_REQUEST, 'action', 'orders'); ?>" />
            <input type="hidden" name="task" value="order" />
            <input type="hidden" name="cid" value="<?php echo intval(nbf_common::get_param($_REQUEST, 'cid')); ?>" />
            <input type="hidden" name="postback" value="1" />
            <input type="hidden" name="page_no" value="<?php echo $page->page_no; ?>" />
            <?php
            if (count($clients) > 1)
            {
                ?>
                <div style="margin-top:3px;margin-bottom: 5px;">
                <?php
                //Allow user to select which client they are representing
                echo NBILL_USER_SELECT_CLIENT . "&nbsp;";
                $client_list = array();
                foreach ($clients as $client)
                {
                    $client_name = nbf_common::nb_strlen($client->company_name) > 0 ? $client->company_name : sprintf(NBILL_CLIENT_NONAME, $client->id);
                    $client_list[] = nbf_html::list_option($client->id, $client_name);
                }
                echo nbf_html::select_list($client_list, "nbill_entity_id", "onchange=\"document.order_form.submit();\"", $selected_client->id);
                ?>
                <noscript><input type="submit" name="nbill_entity_change" value="<?php echo NBILL_SUBMIT; ?>" /></noscript>
                </div><?php
            }
            else
            {
                ?>
                <input type="hidden" name="nbill_entity_id" value="<?php echo isset($selected_client) ? $selected_client->id : '0'; ?>" />
                <?php
            }

            //Output the main body of the page
            $fe_orders->form_display($form_height, $form, $page, $fields, $field_options, $sql_field_options, $show_previous, $extra_form_output);
            ?>
            </form>
        </div>
        <?php

        //Execute any onload javascript
        if (nbf_common::nb_strlen($page->onload) > 0)
        { ?>
            <script type="text/javascript">
                <?php echo $page->onload; ?>
            </script>
        <?php }
    }

    public static function show_message($message = "", $form_title = "", $quotes = false)
    {
        $fe_orders = new nbill_fe_orders_custom();
        $fe_orders->show_pathway($form_title, false, $quotes);
        $fe_orders->show_message($message);
    }

	public static function confirm_renew(&$existing_orders, &$orders, &$currency, &$order_summary_total, &$date_format, $gateways = array(), $default_gateway = "", $frequencies = array(), $current_frequency = "")
	{
        $fe_orders = new nbill_fe_orders_custom();
		if ($existing_orders && count($existing_orders) > 0)
		{
			//Load the stylesheet
            $css = file_get_contents(nbf_cms::$interop->nbill_fe_base_path . "/style/nbill_forms.css");
            nbf_cms::$interop->add_html_header('<style type="text/css">' . $css . '</style>');

            if (nbf_cms::$interop->user->id && nbf_frontend::get_display_option("pathway"))
            {
                $fe_orders->show_pathway("", true);
            }

            if (nbf_common::nb_strlen(nbf_globals::$message) > 0)
			{
				echo "<div class=\"nbill-message\">" . nbf_globals::$message . "</div>";
			}

			echo "<h2 class=\"componentheading\">" . NBILL_ORDER_RENEW_TITLE . "</h2>";
			echo "<p align=\"left\" class=\"nbill-renew-intro\">" . NBILL_ORDER_RENEW_INTRO . "</p>";
			if ($existing_order->auto_renew)
			{
				//Warn that they need to ensure they don't already have a payment schedule set up
				echo "<p align=\"left\" style=\"color:#ff0000;\" class=\"nbill-renew-warning\"><strong>" . NBILL_ORDER_RENEW_WARNING . "</strong></p>";
			}
			?>

			<form action="<?php echo nbf_cms::$interop->fe_form_action; ?>" method="post" name="renewal_form" id="renewal_form">
                <input type="hidden" name="option" value="<?php echo NBILL_BRANDING_COMPONENT_NAME; ?>" />
                <input type="hidden" name="<?php echo nbf_cms::$interop->component_name; ?>" value="my-account" />
				<input type="hidden" name="action" value="orders" />
				<input type="hidden" name="task" value="renew" />
                <input type="hidden" name="show_due" value="<?php echo nbf_common::get_param($_REQUEST, 'show_due'); ?>" />
                <input type="hidden" name="Itemid" value="<?php echo isset($_REQUEST['Itemid']) && $_REQUEST['Itemid'] ? intval($_REQUEST['Itemid']) : (isset($GLOBALS['Itemid']) && $GLOBALS['Itemid'] ? intval($GLOBALS['Itemid']) : ""); ?>" />
				<input type="hidden" name="summary" id="nbill_order_renew_summary" value="1" />
                <input type="hidden" name="auto_renew" value="<?php echo nbf_common::get_param($_REQUEST, 'auto_renew'); ?>" />
				<input type="hidden" name="order_id" value="<?php
                $order_ids = "";
                foreach ($existing_orders as $existing_order)
                {
                    if (nbf_common::nb_strlen($order_ids > 0))
                    {
                        $order_ids .= ", ";
                    }
                    $order_ids .= $existing_order->order_id;
                }
                echo $order_ids;
                ?>" />
                <?php
                //Output the main body of the page
                $fe_orders->renewal_display($existing_orders, $orders, $currency, $order_summary_total, $date_format, $gateways, $default_gateway, $frequencies, $current_frequency);
                ?>
			</form>
		<?php
		}
		else
		{
			echo NBILL_ORDER_RENEW_ORDER_NOT_FOUND;
		}
	}

	public static function show_order_total_summary($orders, $currency, $standard_totals)
	{
		$fe_orders = new nbill_fe_orders_custom();
        $fe_orders->show_order_total_summary($orders, $currency, $standard_totals);
	}
}