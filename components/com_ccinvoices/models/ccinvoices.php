<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.modellist');

class ccinvoicesModelccinvoices extends JModelList
{

	var $_query;
	var $_data;
	var $_total=null;
	var $_pagination=null;
	/**
	 * Constructor that retrieves the ID from the request
	 *
	 * @access	public
	 * @return	void
	 */
	function _buildQuery()
	{
		$mainframe = JFactory::getApplication();
		$db = JFactory::getDBO();
		$user	= JFactory::getUser();
		$filter				= JRequest::getVar('jwd_filter');
		$sortColumn		= $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.sortColumn','filter_order','ordering');
		$sortOrder		= $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.sortOrder','filter_order_Dir','asc');
		$mainframe->setUserState('com_ccinvoices.invoices.sortColumn',$sortColumn);
		$mainframe->setUserState('com_ccinvoices.invoices.sortOrder',$sortOrder);

		$this->_query="SELECT i.*,c.contact FROM #__ccinvoices_invoices AS i"
				. " LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id"
				. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
				. " WHERE u.user_id =".$user->id." AND i.status <> 1 ORDER BY $sortColumn $sortOrder";

	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return (version_compare('1.6.0', $current_version) <= 0);
	}
	function __construct()
	{
		parent::__construct();

		$this->_buildQuery();
		$mainframe = JFactory::getApplication();

		// Get pagination request variables
		$limit = $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.limit', 'limit', $mainframe->getCfg('list_limit'), 'int');
		$limitstart = $mainframe->getUserStateFromRequest('com_ccinvoices.invoices.limitstart', 'limitstart', 0, 'int');

		// In case limit has been changed, adjust it
		$limitstart = ($limit != 0 ? (floor($limitstart / $limit) * $limit) : 0);

		$this->setState('com_ccinvoices.invoices.limit', $limit);
		$this->setState('com_ccinvoices.invoices.limitstart', $limitstart);
	}
	function getData()
	{
		if (empty($this->_data))
			$this->_data=$this->_getList($this->_query,$this->getState('com_ccinvoices.invoices.limitstart'), $this->getState('com_ccinvoices.invoices.limit'));
		return $this->_data;
	}

	function getTotal()
	{
		if (empty($this->_total))
			$this->_total = $this->_getListCount($this->_query);
		return $this->_total;
	}

	function getPagination()
	{
		if (empty($this->_pagination))
		{
			jimport('joomla.html.pagination');
			$this->_pagination = new JPagination($this->getTotal(), $this->getState('com_ccinvoices.invoices.limitstart'), $this->getState('com_ccinvoices.invoices.limit'));
		}
		return $this->_pagination;
	}
	function getStudents()
	{
		$cid = JRequest::getVar('cid');
		if(is_array($cid)) $cid = intval($cid[0]);
		$row=  JTable::getInstance('invoices','Table');
		$row->load($cid);
		return $row;
	}
	function setId($id)
	{
		// Set id and wipe data
		$this->_id		= $id;
		$this->_data	= null;
	}

	function gettemplatelayout($id)
	{
		jimport("joomla.filesystem.file");
		$db 	= JFactory::getDBO();
		$post	= JRequest::get( 'post',JREQUEST_ALLOWRAW );
		$query	= "SELECT invoice_template  FROM #__ccinvoices_templates where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$template =$db->loadResult();

		$query	= "SELECT *  FROM #__ccinvoices_invoices where id=".$id." LIMIT 1";
	    $db->setQuery($query);
		$invRow =$db->loadObject();
		if(count($invRow)<=0)
		{
			return 0;
		}

		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();

		$query = 'SELECT i.*,c.tax_id,c.name,c.contact,c.address,c.email,c.contact_number'
			. ' FROM #__ccinvoices_invoices AS i'
			. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
			. ' where i.id='.$id.' LIMIT 1';
		$db->setQuery($query);
		$rows = $db->loadObject();
        $quantity= explode("|",$rows->quantity);
		$pname=explode("|",$rows->pname);
		$amount=explode("|",$rows->price);
		$tax=explode("|",$rows->tax);
		$quant='';
		$pnames='';
		$amounts='';
        $taxes='';
        $subTotal='';
        $tax_percent = '';
        $breaka="<br/>";
        $price = '';
		$totalPrice = '';
		$excl_tax = '';
		$item_total_excl_tax = '';
		$invoice_total_excl_tax = '';
		$amt_tmp1 = '';
		$subto1 = '';
		$invoice_subtotal = '';
		$product_total_price = '';
		$inv_subtotal_amount = '';
		$op_val['product_total_price'] = '';
		$op_val['invoice_subtotal_amount'] = '';
		$order_tax_total_split_tmp = '';
		$tax_rate_values = array();
		$itemPriceIncTax = array();
		$itPriceIncTax='';
		$inv_tax_total = '';
		for($i=0;$i<count($quantity);$i++)
		{
			$breakVariable = "";
			$produtStringLen = strlen($pname[$i]);
			$noOrBr = $produtStringLen / 30;
			for($k=1;$k<=$noOrBr;$k++)
			{
				$breakVariable = $breakVariable."<br/>";
			}
			if($breakVariable=="")
				$breakVariable="<br/>";
			$quant.= $quantity[$i].$breakVariable;
			$pnames.= $pname[$i]."<br/>";

			//Discount Calc Starts

			//Discount Calc Ends
			$amounts.= $this->changeCurrencyFormat($amount[$i]).$breakVariable;
			$product_total_price += $amount[$i] * $quantity[$i];
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$inv_subtotal_amount += $disc_amt* $quantity[$i];

			}else
			{
				$inv_subtotal_amount += $amount[$i] * $quantity[$i];
			}
			/*$price += $amount[$i] * $quantity[$i];
			$price_tmp = $amount[$i] * $quantity[$i];
			$totalPrice += $price_tmp * $tax[$i] / 100;
*/
            $discount_price = "0";
			if($rows->discount > 0)
			{
				$disc_amt = $amount[$i] - ($amount[$i] * ($rows->discount / 100));
				$amt_tmp1 = $disc_amt * $tax[$i] / 100;
				$subto1 = ($disc_amt+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}else
			{
				$amt_tmp1 = $amount[$i] * $tax[$i] / 100;
				$subto1 = ($amount[$i]+$amt_tmp1 )* $quantity[$i];
				$discount_price += $subto1;
			}

			$amt_tmp = $amount[$i] * $tax[$i] / 100;
			$itemPriceIncTax[$i]=$amount[$i]+$amt_tmp;
			$itPriceIncTax.=ccInvoicesControllerInvoices::changeCurrencyFormat($itemPriceIncTax[$i]).$breakVariable;
			$taxes.= $this->changeCurrencyFormat($amt_tmp).$breakVariable;
			$subto = ($amount[$i]+$amt_tmp )* $quantity[$i];
			$invoice_subtotal += $subto;
			$subTotal.= $this->changeCurrencyFormat($subto).$breakVariable;
			//{item_total_excl_tax}
			$excl_tax = $amount[$i]* $quantity[$i];
			$invoice_total_excl_tax += $excl_tax;
			$item_total_excl_tax.= $this->changeCurrencyFormat($excl_tax).$breakVariable;
			if($tax[$i] == '')
			{
				$tax_percent .= "0%".$breakVariable;
			}else
			{
			$tax_percent .= $tax[$i]."%".$breakVariable;
		}
			$tmp = '';
			$tax_rate = $tax[$i];
			$product_tax_amt = ($amount[$i] * $quantity[$i]) * $tax[$i] / 100 ;
			$inv_tax_total +=$product_tax_amt;
			if(count($tax_rate_values) == 0)
			{
				$tax_rate_values[$tax_rate] = $product_tax_amt;
			}else
			{
				if (array_key_exists($tax_rate, $tax_rate_values))
				{
					$tmp = $tax_rate_values[$tax_rate];
				    $tax_rate_values[$tax_rate] = $tmp + $product_tax_amt;
				}else
				{
					$tax_rate_values[$tax_rate] = $product_tax_amt;
				}
			}

		}
		ksort($tax_rate_values);
		foreach($tax_rate_values as $key=>$value)
		{
			$order_tax_total_split_tmp .= $this->changeCurrencyFormat($value)."&nbsp;(".$key."%)&nbsp;<br/>";
		}
	    //{order_tax_total_split}
	    $orderitem_tax_total = $this->changeCurrencyFormat($inv_tax_total);
	    $order_tax_total_split = JText::_("CC_TEMPLATE_TOTAL_TAX").$orderitem_tax_total."<br/>";
	    $order_tax_total_split .= JText::_("CC_TEMPLATE_TOTAL_TAX_INCLUDES")."<br/>";
	    $order_tax_total_split .= $order_tax_total_split_tmp;
	    $op_val['tax_total_split'] = $order_tax_total_split;
		//print_r ($tax_rate_values);exit;
		$post['quantity'] =$quant;
		$post['pname'] =$pnames;
		$post['price'] =$amounts;
		$post['tax'] =$taxes;
        /* contact values */
		$op_val['invoice_note'] = $rows->note;
		//GETTING PAYMENT METHOD NAMES
		$paymentmethodnames=$this->getPaymentMethodsName();
		$op_val['payment_methods']= $paymentmethodnames;
        $op_val['contact_name'] = $rows->name;
        $op_val['contact_number'] = $rows->contact_number;
		$op_val['contact'] = $rows->contact;
		$op_val['contact_person'] = $rows->contact;

		$status_text="";
		if($rows->status=="1")
			$status_text=JText::_( 'CC_CONCEPT');
		if($rows->status=="2")
			 $status_text=JText::_( 'CC_OPEN');
		if($rows->status=="3")
			 $status_text=JText::_( 'CC_LATE');
		if($rows->status=="4")
			 $status_text=JText::_( 'CC_PAID');

		$op_val['invoice_status']=$status_text;


		$contact_address = str_replace("\n", "<br/>", $rows->address);
		$op_val['contact_address'] = $contact_address;
		$op_val['contact_taxid']= $rows->tax_id;
		$op_val['contact_tax_id']= $rows->tax_id;
		$op_val['contact_email'] = $rows->email;
	    $op_val['invoice_number'] = $this->getInvoiceNumberFormat($invRow->id);
		$op_val['invoice_date'] = $this->dateChangeFormat($rows->invoice_date,$conf->date_format);
		$op_val['invoice_due_date'] = $this->dateChangeFormat( $rows->duedate,$conf->date_format);
		if(strtotime($invRow->invoice_sent_date) == '')
		{
			$op_val['invoice_sent_date'] = JText::_('CC_SENT_DATE_LABEL');
		}
		else
		{
		$op_val['invoice_sent_date'] = $this->dateChangeFormat( $invRow->invoice_sent_date,$conf->date_format);
		}
       	$op_val['item_quantity'] = $quant;
	    $op_val['item_name'] = $pnames;
		$op_val['item_amount'] = $amounts;
		$op_val['item_price_excl_tax'] = $amounts;
		$op_val['item_price_incl_tax'] = $itPriceIncTax;

		$op_val['item_tax'] = $taxes;
		if($rows->discount == '' OR $rows->discount == '0')
		{
			$op_val['invoice_discount'] = '';
		}else
		{
			$op_val['invoice_discount'] = $rows->discount."&nbsp;%";
		}
		$op_val['product_total_price'] = $this->changeCurrencyFormat($product_total_price);
		$op_val['invoice_subtotal_amount'] = $this->changeCurrencyFormat($inv_subtotal_amount);
		$op_val['invoice_tax'] = $this->changeCurrencyFormat($rows->totaltax);
		$op_val['invoice_tax_total'] = $this->changeCurrencyFormat($rows->totaltax);
		$op_val['tax_percentage'] = $tax_percent;
		$op_val['item_tax_percentage'] = $tax_percent;
		$op_val['invoice_total'] = '';
		$op_val['invoice_total_excl_tax'] = '';
    	$op_val['invoice_total'] = $this->changeCurrencyFormat($rows->total);
    	$op_val['invoice_total_excl_tax'] = $this->changeCurrencyFormat($invoice_total_excl_tax);
    	$op_val['invoice_subtotal'] = $this->changeCurrencyFormat($invoice_subtotal);
    	$op_val['invoice_subtotal_excl_discount'] = $this->changeCurrencyFormat($invoice_subtotal);
		$op_val['invoice_total_incl_tax'] = $op_val['invoice_total'];
		$op_val['item_total_excl_tax'] = $item_total_excl_tax;
		$op_val['item_total_incl_tax'] = $subTotal;

		$op_val['product_tax'] = $taxes;
		$op_val['item_tax_amount'] = $taxes;
		$op_val['user_name'] = $conf->user_name;
		//$op_val['user_email'] =  $conf->user_email;
		$op_val['company_name'] = $conf->user_company;
		$op_val['company_email'] = $conf->company_email;
		$op_val['company_phone'] = $conf->company_phone;
		$company_address = str_replace("\n", "<br/>", $conf->company_address);
		$op_val['company_address'] = $company_address;
		$other_details = str_replace("\n", "<br/>", $conf->other_details);
		$op_val['company_details'] = $other_details;
		$op_val['company_url'] = $conf->company_url;
		$op_val['tax_id'] = $conf->tax_id;
		$op_val['company_tax_id'] = $conf->tax_id;
		$logo_path_check =  JPATH_SITE.DS."media".DS."com_ccinvoices".DS."logo".DS.$conf->logo;
		if($conf->logo != "")
		{
			if(JFile::exists($logo_path_check))
			{
		$op_val['logo'] = '<img src="'.JURI::root()."media/com_ccinvoices/logo/".$conf->logo.'"/>' ;
		}else
		{
				$op_val['logo'] = "";
			}
		}else
		{
			$op_val['logo'] = '';
		}
		$template = $this->convertImgTags($template);
		$invoicesendmsg= JText::_( 'CC_INVOICEEMAIL_MSG' );

		//Get Custom Tags From The Plugin
		$val["id"] = $id;
		$options = array( $val);
		JPluginHelper::importPlugin( 'ccinvoicetags' );
		$dispatcher = JDispatcher::getInstance();
		$customTag = $dispatcher->trigger( '_getCustomTags',$options);
		foreach ($customTag as $customTags)
		{
			foreach ($customTags as $customTagName=>$value)
			{
				$op_val[$customTagName] = $value;
			}
		}

		foreach($op_val as $op_vals=>$value)
		{
			$find = "{".$op_vals."}";
			$replace = $value;
			$template = str_replace($find,$replace,$template);
			$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
		}
		return $template;
	}
	function getPaymentMethodsName()
	{
		$plugins_name= JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher = JDispatcher::getInstance();
		$methodnames = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$paymentmethodname="";
		if(count($methodnames)>0)
		{
			$i=0;
			foreach($methodnames as $pluginname)
			{
				if($i==count($methodnames)-2)
				{
					$paymentmethodname.=$pluginname["payment_method"]." or ";
				}
				else
				{
					$paymentmethodname.=$pluginname["payment_method"].", ";
				}
				$i=$i+1;
			}
		}
		$paymentmethodname=substr($paymentmethodname,0,strlen($paymentmethodname)-2);
		return $paymentmethodname;
	}
	function changeCurrencyFormat($cur_val)
	{
		$db 	= JFactory::getDBO();
		$query	= "SELECT cformat,currency_symbol,symbol_display FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
	    $db->setQuery($query);
		$conf = $db->loadObject();
		$format = $conf->cformat;
		$tmp = '';
		$price_amount = "";
		if($format == 0)
		{
			$tmp = number_format($cur_val, 2, '.', ',');
		}else if($format == 1)
		{
			$tmp = number_format($cur_val, 2, ',', '.');
		}else if($format == 2)
		{
			$tmp = number_format($cur_val, 2, '.', ' ');
		}else if($format == 3)
		{
			$tmp = number_format($cur_val, 2, ".", "'");
		}

		if($conf->symbol_display == '1')
		{
			$price_amount = $conf->currency_symbol.$tmp;
		}else
		{
			$price_amount = $tmp.$conf->currency_symbol;
		}
		return $price_amount;
	}
	function convertImgTags($html_content)
	{
		//print_r($html_content);
		global $mainframe;
		$mod_html_content=null;
		$patterns = array();
		$replacements = array();
		$i = 0;
		$src_exp = "/src=\"(.*?)\"/";
		$link_exp =  "[^http:\/\/www\.|^www\.|^https:\/\/|^http:\/\/]";

		preg_match_all($src_exp, $html_content, $out, PREG_SET_ORDER);

		foreach ($out as $val)
		{
			$links = preg_match($link_exp, $val[1], $match, PREG_OFFSET_CAPTURE);
			if($links=='0')
			{
				$patterns[$i] = $val[1];
				$patterns[$i]="\"$val[1]";
				//print_r($patterns[$i]);
				$replacements[$i] = JURI::root().$val[1];
				$replacements[$i]="\"$replacements[$i]";
			}
			$i++;
	 	}
        $mod_html_content=str_replace($patterns,$replacements,$html_content);
		return $mod_html_content;
	}
	function createInvoicePDF($template,$id)
	{
		$mainframe = JFactory::getApplication();
		$sitename=$mainframe->getCfg("sitename");
		$db = JFactory::getDBO();
		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS.'tcpdf.php');
        require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."tcpdf".DS."config".DS."lang".DS.'eng.php');
        $pdf = new ccInvoicesTCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetCreator(PDF_CREATOR);
        $pdf->SetAuthor($sitename);
        $pdf->SetTitle('Invoice');
        $pdf->SetSubject('Invoice');
        $pdf->SetKeywords('Invoice');

		$query = "SELECT *  FROM #__ccinvoices_configuration WHERE id = 1";
		$db->setQuery( $query);
		$confrows = $db->loadObject();

		$topmargin=$confrows->invoice_top_margin;
		$leftmargin=$confrows->invoice_left_margin;
		$rightmargin=$confrows->invoice_right_margin;
		$bottommargin=$confrows->invoice_bottom_margin;
		$invoicemode=$confrows->invoice_mode;

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
        $pdf->setPrintHeader(false);
        $pdf->setPrintFooter(false);
		$pdf->SetMargins($leftmargin, $topmargin, $rightmargin);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		$pdf->SetAutoPageBreak(TRUE,$bottommargin);
		//set auto page breaks
		//set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		$l = '';
		$pdf->setLanguageArray($l);

		if($confrows->fontname_invoice=="1")
			$pdf->SetFont('times', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="2")
			$pdf->SetFont('arialunicid0', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="3")
			$pdf->SetFont('Verdana', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="4")
			$pdf->SetFont('helvetica', '', $confrows->pdffontsize_invoice);
		elseif($confrows->fontname_invoice=="5")
			$pdf->SetFont('FreeSans', '', $confrows->pdffontsize_invoice);
		if($invoicemode=="0")
			$pdf->AddPage('P', 'A4');
		elseif($invoicemode=="1")
			$pdf->AddPage('L', 'A4');
		else
        $pdf->AddPage();
        //Background image process
        if($confrows->backgimage=="1")
        {
        	$backImgPath=JPATH_ROOT.DS.'media'.DS.'com_ccinvoices'.DS.'logo'.DS.$confrows->pdfimagefile;
			$pdf->Image($backImgPath, $confrows->pdfimagexpos, $confrows->pdfimageypos, $confrows->pdfimagewidth, $confrows->pdfimageheight, '', '', '', false, 300, '', false, false, 0);
        }
        $v=$pdf->writeHTML($template, true, false, false, false, '');
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();
		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if($conf->invoice_format != "")
		{
		$file_name = $this->getInvoiceNumberFormat($invRow->id)."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}else
		{
			$file_name = $invRow->number."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}
		$filenm=JRequest::getVar('filenm','0');
		if($filenm=="1")
		{
			if($conf->invoice_format != "")
			{
				$file_name = $this->getInvoiceNumberFormat($invRow->id).".pdf";
			}else
			{
				$file_name = $invRow->number.".pdf";
			}
		}
        $file_path = JPATH_COMPONENT_ADMINISTRATOR.DS.'assets'.DS.'files'.DS.'pdf'.DS.$file_name;
        $pdf->Output($file_path, 'F');
		return $file_path;
	}
	function downloadInvoice($file_path,$id)
	{
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","0");
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		$query	= "SELECT *  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
		$db->setQuery($query);
		$invRow = $db->loadObject();

		$charfound=strpos($_SERVER['SERVER_NAME'],".");
		if($conf->invoice_format != "")
		{
		$file_name = $this->getInvoiceNumberFormat($invRow->id)."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}else
		{
			$file_name = $invRow->number."_".strtotime($invRow->invoice_date).ord(base64_encode("pdf")).ord(substr($_SERVER['SERVER_NAME'],$charfound+1,1)).ord(substr($_SERVER['SERVER_NAME'],$charfound+2,1)).".pdf";
		}

		$sourcefile=JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS."files".DS."pdf".DS.$file_name;

		$filedsdf = fopen($sourcefile,"r");
		$cont=file_get_contents($sourcefile);
		$charfound=strrpos($file_name,"_");
		$file_n=substr($file_name,0,$charfound).substr($file_name,strlen($file_name)-4);

		define('BASE_DIR',JPATH_COMPONENT_ADMINISTRATOR.DS."assets".DS.'files'.DS.'pdf'.DS);
		define('LOG_DOWNLOADS',false);
		define('LOG_FILE','downloads.log');
		$allowed_ext = array (
		  'pdf' => 'application/pdf',

		);
		//set_time_limit(0);
		if (!isset($file_name) || empty($file_name)) {
		  die("Direct Initialization is not Allowed");
		}
		$fname = basename($file_name);
		$file_path = BASE_DIR.$file_name;
		$fsize = filesize($file_path);
		$fext = strtolower(substr(strrchr($fname,"."),1));
		if ($allowed_ext[$fext] == '') {
		  $mtype = '';
		  // mime type is not set, get from server settings
		  if (function_exists('mime_content_type')) {
		    $mtype = mime_content_type($file_path);
		  }
		  else if (function_exists('finfo_file')) {
		    $finfo = finfo_open(FILEINFO_MIME); // return mime type
		    $mtype = finfo_file($finfo, $file_path);
		    finfo_close($finfo);
		  }
		  if ($mtype == '') {
		    $mtype = "application/force-download";
		  }
		}
		else {
		  // get mime type defined by admin
		  $mtype = $allowed_ext[$fext];
		}

		// Browser will try to save file with this filename, regardless original filename.
		// You can override it if needed.
		// set headers
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: $mtype");
		header("Content-Disposition: attachment; filename=\"$file_n\"");
		echo $cont;
		header("Content-Transfer-Encoding: binary");
		header("Content-Length: " . $fsize);

		// download
		@readfile($file_path);

		// log downloads
		if (!LOG_DOWNLOADS) die();

		$f = @fopen(LOG_FILE, 'a+');
		if ($f) {
		  @fputs($f, date("m.d.Y g:ia")."  ".$_SERVER['REMOTE_ADDR']."  ".$fname."\n");
		  @fclose($f);
		}
	}
	function dateChangeFormat($date_temp,$date_format)
	{
		if(strtotime($date_temp) != '')
		{
		return strftime($date_format,strtotime($date_temp));

		}else
		{
			return;
		}
	}
	function getInvoiceNumberFormat($invNum)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * from #__ccinvoices_configuration LIMIT 1 ';
		$db->setQuery($query);
		$conf = $db->loadObject();
		$invoice_format = $conf->invoice_format;
		$query = 'SELECT custom_invoice_number,number from #__ccinvoices_invoices WHERE id='.$invNum.' LIMIT 1 ';
			$db->setQuery($query);
		$rowobj = $db->loadObject();

		$custom_invoice_number=$rowobj->custom_invoice_number;
		if($rowobj->custom_invoice_number == '' || $rowobj->custom_invoice_number == '0')
			{
			$custom_invoice_number = $rowobj->number;
			}
			return $custom_invoice_number;
	}
	function getOverviewPageParams()
	{
		$db = JFactory::getDBO();
		$query = "SELECT params from #__menu where link='index.php?option=com_ccinvoices&view=ccinvoices'";
		$db->setQuery($query);
		return $params = $db->loadResult();
	}
}
?>