<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/
// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
// Getting menu id(Itemid)

$itemid = JRequest::getVar('Itemid');
?>

<script src="<?php echo  JURI::base();?>components/com_ccinvoices/assets/js/jsfile.js"></script>
<?php
if($this->message!="")
{
	?>
	<table style="width:100%;">
		<tr>
			<td align="center" width="100%">
				<table class="ccinvoices_messageCl" cellpadding="0" cellspacing="10">
					<tr>
						<td class="ccinvoices_cancelImgcl"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/cross.png"></td>
						<td align="left" class="ccinvoices_cancelMsg">
							<b color="#4A4646">
							<?php
								echo $this->message;
							?>
							</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
<?php
}
?>
<table cellpadding="0" style="margin-top:-20px;" cellspacing="0" width="98%" border="0">
	<tr>
		<td>
			<h2 style="font-size:2.1em;line-height:30px;"><?php echo sprintf(JText::_("CC_PAY_INVOICE"),$this->num); ?></h2>
		</td>
	</tr>
	<tr>
		<td style="padding-bottom:5px !important;">
			<p style="text-align:justify;"><?php echo sprintf(JText::_("CC_PAYMENT_OVERVIEW_PAGE_MSG"),$this->user_company,$this->invoice_date,$this->duedate); ?></p>
		</td>
	</tr>
	<tr>
		<td>
			<table cellpadding="0" cellspacing="0" width="100%" border="0">
				<?php
					$i = 0;
					foreach($this->PaymentMethodDetails AS $pminfo)
					{
							$paymentLogoPath = JURI::root()."plugins/ccinvoices_payment/".$this->plugin_info[$i]->name."/".$this->plugin_info[$i]->name."/images/".@$pminfo["logo"];

						?>
							<tr>
								<td class="tdIdClass">
									<?php echo $pminfo; ?>
								</td>
							</tr>
							<tr>
								<td>&nbsp;</td>
							</tr>
						<?php
						$i++;
					}
				?>
			</table>
		</td>
	</tr>
</table>
