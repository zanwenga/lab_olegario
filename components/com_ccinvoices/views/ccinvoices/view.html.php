<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');



class  ccinvoicesViewccinvoices  extends CCINVOISViews
{
	function display($tpl = null)
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$user	= JFactory::getUser();
		$action=JRequest::getVar('action','');
		$ptype=JRequest::getVar('ptype','');

		$filter					= JRequest::getVar('jwd_filter');
		$rows		= $this->get('data');
		$pagination		= $this->get('pagination');

		$this->param=$this->getModel()->getOverviewPageParams();



		$sql = "SELECT * FROM #__ccinvoices_configuration WHERE id = 1  LIMIT 1";
		$db->setQuery($sql);
		$config = $db->loadObject();
		$showPaymentIcon = 0;
		$paymentIconImg = "";
		$plugins_name= JPluginHelper::importPlugin( 'ccinvoices_payment');
		$dispatcher = JDispatcher::getInstance();
		$paymentIcons = $dispatcher->trigger( 'onSiteInvoiceOverviewPaymentIcons');
		$plugin_infos = JPluginHelper::getPlugin('ccinvoices_payment', $plugin=null);
		$i = 0;
		$k = 0;

		foreach($paymentIcons as $paymentIcon)
		{
			$paymentIconPath = JURI::root()."plugins/ccinvoices_payment/".$plugin_infos[$i]->name."/".$plugin_infos[$i]->name."/images/".$paymentIcon["icon"];
			if($plugin_infos[$i]->name == "ccinvoicePaypal" OR $plugin_infos[$i]->name == "ccinvoiceMollieIDEAL")
			{
				if($k < 5)
				{
					$paymentIconImg .= "<img src='".$paymentIconPath."' title='".$paymentIcon["payment_method"]."'/>&nbsp;";
					$k++;
				}
			}
			$i++;
		}

		$i = 0;

		foreach($paymentIcons as $paymentIcon)
		{
			if($plugin_infos[$i]->name != "ccinvoiceOffline")
			{
				$showPaymentIcon = 1;
			}
			$i++;
		}
		$PaymentMethodMessage="";
		if($action=="showresult")
		{
			$id = JRequest::getVar("id","");
			$sql = "SELECT id,number,invoice_date,duedate FROM #__ccinvoices_invoices WHERE MD5(id) ='".$id."' LIMIT 1";
			$db->setQuery($sql);
			$row = $db->loadObject();
			$inv_id="";
			$inv_id[0] = $row->id;
			$PaymentMethodMessage = $dispatcher->trigger( 'onAfterSuccessfulPayment',array($inv_id));
			$message=$this->getPaymentPluginStatus($PaymentMethodMessage,$plugin_infos,$ptype);
		}

		$status[1] = JText::_( 'CC_CONCEPT');
        $status[2] = JText::_( 'CC_OPEN');
        $status[3] = JText::_( 'CC_LATE');
        $status[4] = JText::_( 'CC_PAID');
        $this->assignRef('status',	$status);
		$this->assignRef('currency_symbol',  $config->currency_symbol);
		$this->assignRef('symbol_display',	$config->symbol_display);
		$this->assignRef('cformat',		$config->cformat);
		$this->assignRef('date_format',  $config->date_format);
		$this->assignRef('showPaymentIcon',  $showPaymentIcon);
		$this->assignRef('paymentIconImg',  $paymentIconImg);
		$this->assignRef("message",$message);
		$this->assignRef("rows",$rows);
		$this->addCSS();
		if(versionCompare()>=3)
		{
			$this->_layout='default';
		}
		else
		{
			$this->_layout='default25';
		}
		parent::display($tpl);
	}
	function getPaymentPluginStatus($PaymentMethodMessage,$plugin_infos,$ptype)
	{
		$i = 0;
		foreach($PaymentMethodMessage as $PaymentMethodMessages)
		{

			if($plugin_infos[$i]->name == $ptype)
			{
				return $PaymentMethodMessages;
			}
			$i++;
		}
	}
	function addCSS()
	{
	    $document = JFactory::getDocument();
	    $document->addStyleSheet('components/com_ccinvoices/assets/css/style.css');
	}
}