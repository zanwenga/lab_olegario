<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');

// Getting menu id(Itemid)
//onAfterSuccessfulPayment()
$itemid = JRequest::getVar('Itemid');

$background_color="#D1D1D1";
$font_color="#424141";

if(strpos($this->param,'show_page_heading":1'))
{
	$result=@str_replace('"','',$this->param);
	$result=@str_replace('{','',$result);
	$result=@str_replace('}','',$result);
	$newarra=@explode(",",$result);
	foreach(@$newarra as $newarras)
	{
		$tmparr=@explode(":",$newarras);
		$newarra1[$tmparr[0]]=@$tmparr[1];
	}
	echo '<h1>'.@$newarra1['page_heading'].'</h1>';
}


?>

<?php
if($this->message!="")
{
	?>

	<table style="width:100%;">
		<tr>
			<td align="center" width="100%" >
				<table class="ccinvoices_messageBlue" cellpadding="0" cellspacing="10">
					<tr>
						<td class="ccinvoices_successImgcl"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/ticknew.png"></td>
						<td align="left" class="ccinvoices_successMsg">
							<b color="#060606">
							<?php
								echo $this->message;
							?>
							</b>
						</td>
					</tr>
				</table>
			</td>
		</tr>
	</table>
	<br/>
<?php
}
?>





<form action="index.php"  method="post" name="adminForm">
<table cellpadding="0" cellspacing="0" width="100%" id="tableMain">
	<tr>
		<th align="center" id="headStyle" style="text-decoration:none !important;">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_ID' )."&nbsp;", 'i.number', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th align="center" id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_DATE' )."&nbsp;", 'i.invoice_date', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th class="tableMain">
		<th align="center" id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_DUE_DATE' )."&nbsp;", 'i.duedate', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th align="right" id="headStyle" style="padding-right:5px;">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_AMOUNT' )."&nbsp;", 'i.total', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th  id="headStyle">
			<?php echo JHTML::_('grid.sort',  JText::_( 'CC_INVOICE_STATUS' )."&nbsp;", 'i.status', @$this->lists['order_Dir'], @$this->lists['order'] ); ?>
		</th>
		<th  id="headStyle">
			<?php echo JText::_( 'CC_INVOICE_ACTION' ); ?>
		</th>
	</tr>
<?php
$params = array('size'=>array('x'=>100, 'y'=>100));
JHTML::_('behavior.modal', 'a.ccinvoicesmodal', $params);
$k = 0;
for ($i=0, $n=count( $this->rows ); $i < $n; $i++)
{
	$row = &$this->rows[$i];
?>
	<tr class="<?php echo "invoiceRow$k"; ?>">
		<td align="center" id="tdRecStyle">
			<?php
					if($row->custom_invoice_number != '')
					{
						echo $row->custom_invoice_number;
					}else
					{
						echo $row->number;
					}
			?>
		</td>
		<!--
		<td align="center" id="tdRecStyle">
			<?php echo $row->contact; ?>
		</td>
		-->
		<td align="center" id="tdRecStyle">
			<?php echo strftime($this->date_format,strtotime($row->invoice_date));

			?>
		</td>
		<td align="center" id="tdRecStyle">
			<?php echo strftime($this->date_format,strtotime($row->duedate));	?>
		</td>
		<td align="center" id="tdRecStyle">
			<?php

			if($this->symbol_display == '1')
			{
				if($this->cformat == 0)
				{
					echo $this->currency_symbol.@number_format($row->total, 2, '.', ',');
				}else if($this->cformat == 1)
				{
					echo $this->currency_symbol.@number_format($row->total, 2, ',', '.');
				}else if($this->cformat == 2)
				{
					echo $this->currency_symbol.@number_format($row->total, 2, '.', ' ');
				}else if($this->cformat == 3)
				{
					echo $this->currency_symbol.@number_format($row->total, 2, ".", "'");
				}

			}else
			{
				if($this->cformat == 0)
				{
					echo @number_format($row->total, 2, '.', ',').$this->currency_symbol;
				}else if($this->cformat == 1)
				{
					echo @number_format($row->total, 2, ',', '.').$this->currency_symbol;
				}else if($this->cformat == 2)
				{
					echo @number_format($row->total, 2, '.', ' ').$this->currency_symbol;
				}else if($this->cformat == 3)
				{
					echo @number_format($row->total, 2, ".", "'").$this->currency_symbol;
				}
			}


			 ?>
		</td>
		<td align="center" id="tdRecStyle" style="padding:5px;">
			<table cellpadding="0" cellspacing="0" width="100%">
			<tr>
			<?php
			if($row->status == 1)
			{
				$color = "ccinvoices_statusbox1";
			}else if($row->status == 2)
			{
				$color = "ccinvoices_statusbox2";
			}else if($row->status == 3)
			{
				$color = "ccinvoices_statusbox3";
			}else if($row->status == 4)
			{
				$color = "ccinvoices_statusbox4";
			}
			?>
				<td class="<?php echo $color; ?>" align="center">
			<?php echo $this->status[$row->status]; ?>
		</td>
			</tr>
			</table>
		</td>
		<td id="tdRecStyle1" >
			<table cellpadding="4" style="margin-left:10px;" cellspacing="0" border="0">
				<tr align="right">
					<td>
						<?php
							$links=JRoute::_("index.php?option=com_ccinvoices&task=emailInv&filenm=1&id=".$row->id."&Itemid=".$itemid);
						?>
						<a href="<?php echo $links; ?>"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/send.png" width="16" height="16" title="<?php echo JText::_("CC_EMAIL_INVOICE"); ?>"></a>
					</td>
					<?php if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<?php } ?>
					<td>
						<?php
						$links=JRoute::_("index.php?option=com_ccinvoices&task=viewInv&id=".$row->id."&tmpl=component");
						?>
						<a  href= "<?php echo $links; ?>" class="ccinvoicesmodal"  rel="{handler: 'iframe', size: {x: 950, y: 500}}"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/view.png" width="16" height="16" title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>"></a>
					</td>
					<?php if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<?php } ?>
					<td>
						<?php
							$links=JRoute::_("index.php?option=com_ccinvoices&task=download&id=".$row->id);
						?>
						<a href="<?php echo $links; ?>"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/download.png" width="16" height="16" title="<?php echo JText::_("CC_DOWNLOAD_INVOICE"); ?>"></a>
					</td>
					<?php if(versionCompare()!="1.5"){ ?>
						<Td>&nbsp;</td>
					<?php } ?>
					<?php
					if($this->showPaymentIcon)
					{
						if($row->status == 4 || $row->status == 1)
						{
							?>
							<td>
								<a href="javascript:void(0);"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/pay_now_button_inactive.png" width="89" height="28" ></a>
							</td>
							<?php
						}else
						{
							?>
							<td>
								<a href="<?php echo JRoute::_('index.php?option=com_ccinvoices&task=paymentOverview&id='.md5($row->id)); ?>"><img src="<?php echo JURI::root(); ?>/components/com_ccinvoices/assets/images/pay_now_button_active.png" width="89" height="28" ></a>
							</td>
							<?php
						}
					}
					?>
				</tr>
			</table>
		</td>
	</tr>
<?php
	$k = 1 - $k;
}
?>
<input type="hidden" name="filter_order" value="<?php echo @$this->lists['order']; ?>" />
<input type="hidden" name="filter_order_Dir" value="<?php echo @$this->lists['order_Dir']; ?>" />
<input type="hidden" name="option" value="com_ccinvoices">
<input type="hidden" name="view" value="ccinvoices">
<input type="hidden" name="controller" value="ccinvoices" />
<input type="hidden" name="Itemid" value="<?php echo $itemid; ?>" />
</table>
</form>