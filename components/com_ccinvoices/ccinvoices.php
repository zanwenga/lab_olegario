<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');
@define( 'DS', DIRECTORY_SEPARATOR );
$language = JFactory::getLanguage();
$language->load('com_ccinvoices', JPATH_SITE, 'en-GB', true);
$language->load('com_ccinvoices', JPATH_SITE, null, true);


function versionCompare()
{
	$jversion = new JVersion();
	$current_version =  $jversion->getShortVersion();
	return substr($current_version,0,1);
}

//Controllers
if(versionCompare()>=3)
{
	class CCINVOISController extends JControllerLegacy{}
}
else
{
	jimport('joomla.application.component.controller');
	class CCINVOISController extends JController{}
}

//Views
if(versionCompare()>=3)
{
	class CCINVOISViews extends JViewLegacy{}
}
else
{
	jimport( 'joomla.application.component.view' );
	class CCINVOISViews extends JView{}
}

$controllerName = 'ccinvoices';
require_once( JPATH_COMPONENT.DS.'controllers'.DS.$controllerName.'.php' );
$controllerName = 'ccinvoicesController'.$controllerName;

// Create the controller
$controller = new $controllerName();

// Perform the Request task
$controller->execute( JRequest::getCmd('task') );

// Redirect if set by the controller
$controller->redirect();

?>