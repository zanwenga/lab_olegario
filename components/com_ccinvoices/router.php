<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

 /* Weblinks Component Route Helper
 *
 * @package		Joomla.Site
 * @subpackage	com_ccinvoices
 * @since 1.6
 */

defined('_JEXEC') or die;

jimport('joomla.application.categories');

/**
 * Build the route for the com_ccinvoices component
 *
 * @param	array	An array of URL arguments
 *
 * @return	array	The URL arguments to use to assemble the subsequent URL.
 */
function ccinvoicesBuildRoute(&$query)
{
//print_r($query);
	// Initialise variables.
	$segments = array();

	//If SEF is disabled
	$conf = JFactory::getConfig();
	 if( !$conf->get('sef') || $conf->get('sef')=="") {

			foreach ($query as $key => $value){
					if  ($key != 'option')  {
						if ($key != 'Itemid') {
							$segments[]=$key.'/'.$value;
							unset($query[$key]);
						}
					}

				}
			return $segments;
	}

	// Get all relevant menu items.
			$app	= JFactory::getApplication();
			$menu	= $app->getMenu();
			$com	= JComponentHelper::getComponent('com_ccinvoices');
			$items	= $menu->getItems('component_id', $com->id);

			$params		= JComponentHelper::getParams('com_ccinvoices');
			$advanced	= $params->get('sef_advanced_link', 0);



		$task = '';

       if(isset($query['task']))
       {
			$task = $query['task'];
			unset( $query['task']);
       }

       if(isset($query['view']))
       {
			$view = $query['view'];
			$task='';
			unset( $query['view']);
       }

switch ($task) {


		case 'viewInv':

			    $segments[] 		= 'viewInv';
				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}
				$segments[] 		= "component";
				$query['Itemid']	= "";
			    unset ($query['Itemid'],$query['tmpl']);

				return $segments;

		break;
		case 'emailInv':

			    $segments[] 		= 'emailInv';
				$segments[] 		= $query['filenm'];
				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}
				$segments[] =$query['Itemid'];
				$query['filenm']="";
				$query['Itemid'] = "";
			    unset ($query['Itemid'],$query['filenm']);
				return $segments;
		break;
		case 'download':

			    $segments[] 		= 'download';
				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}

				$query['Itemid'] = "";
			    unset ($query['Itemid'],$query['id']);
				return $segments;
		break;
		case 'paymentOverview':

			    $segments[] 		= 'paymentOverview';

				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}
				$segments[] 		= "";
				$query['Itemid']	= "";

			    unset ($query['Itemid']  );

				return $segments;

 		break;
		case 'processPayment':

			    $segments[] 		= 'processPayment';
			    $segments[] 		= $query['ptype'];
			    $segments[] 		= $query['pactiontype'];
				if(isset($query['controller']))
				{
					$segments[] = $query['controller'];
					unset($query['controller']);
				}
				else
				{
					 $segments['0'] 		= 'processPayment1';

				}

				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}
				$query['Itemid']	= "";
			    unset ($query['Itemid'],$query['ptype'],$query['pactiontype']);
				return $segments;

 		break;
		case 'paymentReturnUrl':

			    $segments[] 		= 'paymentReturnUrl';
			    $segments[] 		= $query['action'];
			    $segments[] 		= $query['ptype'];

				if(isset($query['id']))
				{
					$segments[] = $query['id'];
					unset($query['id']);
				}
				$query['Itemid']	= "";
			    unset ($query['Itemid'],$query['ptype'],$query['action']);
				return $segments;
 		break;
		default :

			    $segments[] 		= 'invoices';

				return $segments;
		break;


	}

       return $segments;
}
/**
 * Parse the segments of a URL.
 *
 * @param	array	The segments of the URL to parse.
 *
 * @return	array	The URL attributes to be used by the application.
 */
function ccinvoicesParseRoute($segments)
{

	   $vars = array();
	   $app = JFactory::getApplication();
	   $menu = $app->getMenu();
	   $item = $menu->getActive();



 // Count segments
       $count = count( $segments );

       if($segments[0] == 'viewInv'){

		 	$vars['task'] 	=	$segments[0] ;
		 	$vars['id']		=   @$segments[1];
		 	$vars['tmpl']		=   @$segments[2];
			if(!empty($segments[4]))
		 	$vars['Itemid'] = 	$segments[4] ;
			unset($segments['Itemid'],$segments['tmpl']);

       }
       elseif($segments[0] == 'emailInv'){

		 	$vars['task'] 	=	$segments[0] ;
		 	$vars['filenm']		=   @$segments[1];
		 	$vars['id']		=   @$segments[2];
			if(!empty($segments[3]))
		 	$vars['Itemid'] = 	$segments[3];
			unset($segments['Itemid'],$segments['filenm']);

       }
 		elseif($segments[0] == 'download'){

		 	$vars['task'] 	=	$segments[0] ;
		 	$vars['id']		=   @$segments[1];
			if(!empty($segments[2]))
		 	$vars['Itemid'] = 	$segments[2];
			unset($segments['Itemid']);

       }
       elseif($segments[0] =='paymentOverview'){
       		$vars['task'] 	=	$segments[0] ;
	  		if(!empty($segments[1]))
			$vars['id'] = 	$segments[1] ;
	  		if(!empty($segments[2]))
			$vars['Itemid'] = 	$segments[2] ;
			unset($segments['Itemid'],$segments['id']);

       }
       elseif($segments[0] =='processPayment'){
       		$vars['task'] 	=	$segments[0] ;
       		$vars['ptype'] 	=	$segments[1] ;
       		$vars['pactiontype'] 	=	$segments[2] ;

	  		if(!empty($segments[3]))
	  		{
			$vars['controller'] = 	$segments[3] ;
	  		}

	  		if(!empty($segments[4]))
	  		{
			$vars['id'] = 	$segments[4] ;
	  		}

	  		if(!empty($segments[5]))
			$vars['Itemid'] = 	$segments[5] ;
			unset($segments['Itemid']);
       }
       elseif($segments[0] =='processPayment1'){
       		$vars['task'] 	=	str_replace("1","",$segments[0]);
       		$vars['ptype'] 	=	$segments[1] ;
       		$vars['pactiontype'] 	=	$segments[2] ;

	  		if(!empty($segments[3]))
	  		{
				$vars['id'] = 	$segments[3];
	  		}

	  		if(!empty($segments[4]))
			$vars['Itemid'] = 	$segments[4] ;


			unset($segments['Itemid']);
       }
       elseif($segments[0] =='paymentReturnUrl'){

       		$vars['task'] 	=	$segments[0];
       		$vars['action'] 	=	$segments[1] ;
       		$vars['ptype'] 	=	$segments[2] ;

	  		if(!empty($segments[3]))
	  		{
				$vars['id'] = 	$segments[3];
	  		}

	  		if(!empty($segments[4]))
			$vars['Itemid'] = 	$segments[4] ;

			unset($segments['Itemid']);
       }
       elseif($segments[0] =='invoices')
       {

       }
	return $vars;
}
