<?php
/**
* @package    [ccInvoices]
* @author     Chill Creations <info@chillcreations.com>
* @link     http://www.chillcreations.com
* @copyright    Copyright (C) [2009 - 2012] Chill Creations
* @license    GNU/GPL, see LICENSE.php for full license.

* See COPYRIGHT.php for more copyright notices and details.

This file is part of [ccInvoices].

This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

**/

// Check to ensure this file is included in Joomla!
defined('_JEXEC') or die('Restricted access');


class ccinvoicesControllerccinvoices extends CCINVOISController
{
	function __construct( $config = array() )
	{
		parent::__construct( $config );
	}

	function display($cachable = false, $urlparams = false)
	{
		$user	= JFactory::getUser();
		if ( $user->get('guest')) {
			$url = base64_encode("index.php?option=com_ccinvoices&view=ccinvoices");
			if($this->versionCompare()=="1.5")
			$link = "index.php?option=com_user&view=login&return=".$url;
			else
				$link = "index.php?option=com_users&view=login&return=".$url;
			$this->setRedirect( $link);
		}
		JRequest::setVar( 'view', 'ccinvoices');
		parent::display();
	}
	function processPayment()
	{
		JRequest::setVar( 'view', 'payment' );
		JRequest::setVar( 'layout', 'process' );

		parent::display();
	}

	function paymentOverview()
	{
		/*$user	=& JFactory::getUser();
		$id = JRequest::getVar("id");
		if ( $user->get('guest')) {
			$url = base64_encode("index.php?option=com_ccinvoices&task=paymentOverview&id=".$id);
			$link = "index.php?option=com_user&view=login&return=".$url;
			$this->setRedirect( $link);
		}*/
		JRequest::setVar( 'view', 'payment');
		parent::display();
	}
	function paymentReturnUrl()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$inv_id = JRequest::getVar("id","0");

		$sql = "SELECT count(*) FROM #__ccinvoices_payment WHERE status=1 AND MD5(inv_id) ='".$inv_id."'";
		$db->setQuery($sql);
		$count_inv=$db->loadResult();
		if($count_inv > 0)
		{
        	JRequest::setVar( 'view', 'ccinvoices' );
		}
		else
		{
			JRequest::setVar( 'view', 'payment' );
		}
		parent::display();
	}
	function download()
	{
		global $mainframe;
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","0");
		$user	= JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id." AND i.id=".$id."";
		$db->setQuery($sql);
		$count = $db->loadResult();
		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			$model   = $this->getModel('ccinvoices');
			require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."controllers".DS.'invoices.php');
			$ccInvoicesControllerInvoices = new ccInvoicesControllerInvoices();
			$template=$ccInvoicesControllerInvoices->gettemplatelayout($id);
			$file_path = $model->createInvoicePDF($template,$id);
			$model->downloadInvoice($file_path,$id);

		}
	}
	function viewInv()
	{
		$styleshhits="style";
		$styleshhits.="=";
		$styleshhits.="text-decoration:none;color:gray;font-size:14px;";
        	?>
				<script language="javascript">
				function actioncolor(tmp)
				{
					document.getElementById(tmp).style.color='blue';
				}
				function actioncolor_change(tmp)
				{
					document.getElementById(tmp).style.color='gray';

				}
				</script>
			<?php
		global $mainframe;
		$db = JFactory::getDBO();
		$id = JRequest::getInt("id","0");
		$user	= JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id." AND i.id=".$id."";
		$db->setQuery($sql);
		$count = $db->loadResult();
		//echo $count;
		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."controllers".DS.'invoices.php');
			$ccInvoicesControllerInvoices = new ccInvoicesControllerInvoices();
			$template=$ccInvoicesControllerInvoices->getTemplateLayout($id);
			if($template == "0")
			{
				echo JText::_("CC_NOT_AUTHO");
				return;
			}
			if($this->versionCompare()=="1.5")
			{
			?>
				<style type="text/css">
					.contentpane
					{
						font-family:Arial,Helvetica,sans-serif;
						font-size:11px;
						margin:10px;
						padding:0 0 1px;
						color:#000000;
					}
				</style>
			<?php
			}
			else
			{
			?>
				<style type="text/css">
					.contentpane
					{
						font-family:Arial,Helvetica,sans-serif;
						font-size:14px;
						margin:10px;
						padding:0 0 1px;
						color:#000000;

					}
					tr, td {
    				border: 0 solid #DDDDDD;
					}
				</style>
			<?php
			}

			//next invoice
			$query = "SELECT i.id FROM #__ccinvoices_invoices AS i "
						. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
						. " WHERE u.user_id=".$user->id." AND i.id >".$id." order by i.id asc LIMIT 1";
			$db->setQuery( $query );
			$next_order = $db->loadResult();

			//==========================
			//previous invoice
			$query = "SELECT i.id FROM #__ccinvoices_invoices AS i "
					. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
					. " WHERE u.user_id=".$user->id." AND i.id <".$id." order by i.id desc LIMIT 1";
			$db->setQuery( $query );
			$prev_order = $db->loadResult();
			//==========================
		?>
			<table border="0" width="100%">
				<tr>
					<td width="50%">
		<?php
			$link_prev_order = "index.php?option=com_ccinvoices&task=viewInv&id=".$prev_order."&tmpl=component";
			if(trim($prev_order)!="")
			{
				$prev_invNumber=@ccInvoicesControllerInvoices::getInvoiceNumberFormat($prev_order);
				?>
					<div  style="width:250px;display:block;"><a title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>" class="modal"  href="<?php echo $link_prev_order; ?>" rel="{handler:'iframe',size:{x:window.getSize().scrollSize.x-80, y: window.getSize().size.y-80}, onShow:$('sbox-window').setStyles({'padding': 0})}" <?php echo $styleshhits;?>><div id="divblok1"  onmouseover="actioncolor(this.id)" onmousemove="actioncolor(this.id)" onmouseout="actioncolor_change(this.id)"><?php echo sprintf (  JText::_('CC_PREV_ORDER'),$prev_invNumber); ?></div></a></div>
				<?php
			}
			?>
					</td>
					<td width="50%">
			<?php
			$link_next_order = "index.php?option=com_ccinvoices&task=viewInv&id=".$next_order."&tmpl=component";
			if(trim($next_order)!="")
			{
				$next_invNumber=@ccInvoicesControllerInvoices::getInvoiceNumberFormat($next_order);
				?>
					<div style="float:right;display:block;" ><a title="<?php echo JText::_("CC_VIEW_INVOICE"); ?>" class="modal"  href="<?php echo $link_next_order; ?>" rel="{handler:'iframe',size:{x:window.getSize().scrollSize.x-80, y: window.getSize().size.y-80}, onShow:$('sbox-window').setStyles({'padding': 0})}" <?php echo $styleshhits;?>><div id="divblok2" onmouseover="actioncolor(this.id)" onmousemove="actioncolor(this.id)" onmouseout="actioncolor_change(this.id)"><?php echo sprintf (  JText::_('CC_NEXT_ORDER'),$next_invNumber); ?></div></a></div>
				<?php
			}
			?>
						</td>
					</tr>
				</table>
			<?php
			echo $template;
		exit;
		}
	}
	function versionCompare()
	{
		$jversion = new JVersion();
		$current_version =  $jversion->getShortVersion();
		return substr($current_version,0,3);
	}
	function emailInv()
	{
		$mainframe = JFactory::getApplication();
		$app = JFactory::getApplication();
		$sitename= $app->getCfg('sitename');
		//$sitename = $mainframe->getCfg( 'sitename' );
		$db = JFactory::getDBO();
		$query	= "SELECT *  FROM #__ccinvoices_configuration where id = 1 LIMIT 1";
		$db->setQuery($query);
		$conf = $db->loadObject();
		if($conf->email_cc == '')
		{
			$EmailCC = NULL;
		}else
		{
			$EmailCC =explode(",",$conf->email_cc);
		}
		if($conf->email_bcc == '')
		{
			$EmailBCC = NULL;
		}else
		{
			$EmailBCC =explode(",",$conf->email_bcc);
		}
		//$MailFrom	= $mainframe->getCfg('mailfrom');
		//$FromName	= $mainframe->getCfg('fromname');
		$MailFrom	= $conf->company_email;
		$FromName	= $conf->user_company;
		$confcom_username	= $conf->user_name;
		$id = JRequest::getInt("id","0");
		$invoice_send_date	= date("Y-m-d");
		$sql = "UPDATE #__ccinvoices_invoices SET invoice_sent_date = '".$invoice_send_date."' WHERE id=".$id;
		$db->setQuery($sql);
		$db->query();
		$itemid = JRequest::getVar("Itemid","");
		$user	= JFactory::getUser();
		$sql = "SELECT count(*) FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_users AS u ON u.contact_id = i.contact_id"
			. " WHERE u.user_id=".$user->id;
		$db->setQuery($sql);
		$count = $db->loadResult();

		$sql = "SELECT c.email FROM #__ccinvoices_invoices AS i "
			. " LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id"
			. " WHERE i.id=".$id;
		$db->setQuery($sql);
		$contact_email = $db->loadResult();

		if($count == 0)
		{
			echo JText::_("CC_NOT_AUTHO");
			return;
		}else
		{
			$query = 'SELECT i.*,c.name,c.contact,c.email'
				. ' FROM #__ccinvoices_invoices AS i'
				. ' LEFT JOIN #__ccinvoices_contacts AS c ON c.id = i.contact_id'
				. ' where i.id='.$id.' LIMIT 1';
			$db->setQuery($query);
			$rows = $db->loadObject();

			$model   = $this->getModel('ccinvoices');
			require_once(JPATH_COMPONENT_ADMINISTRATOR.DS."controllers".DS.'invoices.php');
			$ccInvoicesControllerInvoices = new ccInvoicesControllerInvoices();
			$template=$ccInvoicesControllerInvoices->getTemplateLayout($id);
			$file_path = $model->createInvoicePDF($template,$id);
			$query	= "SELECT number  FROM #__ccinvoices_invoices where id = ".$id." LIMIT 1";
			$db->setQuery($query);
			$invNumber = $db->loadResult();


			if($conf->default_email_sub == "")
			{
				$conf->default_email_sub = JText::_( 'CC_INVOICEEMAIL_SUB' );
			}
			$email_subject= $conf->default_email_sub;


			$invoicesendmsg = $conf->default_email;



			$itemid = JRequest::getVar("Itemid","");
			$front_end_url = JRoute::_(JURI::root()."index.php?option=com_ccinvoices&view=ccinvoices&Itemid=".$itemid);
			$pay_invoice_link=JRoute::_(JURI::root()."index.php?option=com_ccinvoices&task=paymentOverview&id=".MD5($id)."&Itemid=".$itemid);


			$outputArr["all_invoices_link"] = "<a href='".$front_end_url."' target='_blank'>".JText::_("CC_WEBSITE_LINK")."</a>";
			$outputArr["pay_invoice_link"] = "<a href='".$pay_invoice_link."' target='_blank'>".JText::_("CC_PAY_INVOICE_LINK")."</a>";
			foreach($outputArr as $outputArrs=>$value)
			{
				$find = "{".$outputArrs."}";
				$replace = $value;
				$invoicesendmsg = str_replace($find,$replace,$invoicesendmsg);
				$email_subject = str_replace($find,$replace,$email_subject);
			}
			$email_subject=$ccInvoicesControllerInvoices->getTemplateLayout($id,4,$email_subject);
			$invoicesendmsg=$ccInvoicesControllerInvoices->getTemplateLayout($id,4,$invoicesendmsg);
			$invoicesendmsg = html_entity_decode($invoicesendmsg, ENT_QUOTES);

			$emails = explode(",",$contact_email);
			JFactory::getMailer()->sendMail($MailFrom, $FromName, $emails, $email_subject, $invoicesendmsg ,1,$EmailCC,$EmailBCC, $file_path);
			if($file_path!="")
			{
				jimport('joomla.filesystem.file');
				if(JFile::exists($file_path)) {
					JFile::delete($file_path);
				}
			}
			$msg = JText::_("CC_INVOICE_SEND_SUCCESS");
			$link = JRoute::_("index.php?option=com_ccinvoices&Itemid=".$itemid);
			$this->setRedirect( $link ,$msg);
		}
	}

	function validateCreditcard_number()
	{
		$credit_card_number = JRequest::getVar("cardnum","");

	    //Get the first digit
	    $firstnumber = substr($credit_card_number, 0, 1);
	    //Make sure it is the correct amount of digits. Account for dashes being present.
	    switch ($firstnumber) {
	        case 3:
	            if (!preg_match('/^3\d{3}[ \-]?\d{6}[ \-]?\d{5}$/', $credit_card_number)) {
	               echo 'This is not a valid American Express card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	            }
	            break;
	        case 4:
	        	if(strlen($credit_card_number)!="13" AND strlen($credit_card_number)!="15" AND strlen($credit_card_number)!="16"){
	                echo 'This is not a valid Visa card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	            }
	            break;
	        case 5:
	            if (!preg_match('/^5\d{3}[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number)) {
	                 echo 'This is not a valid MasterCard card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	            }
	            break;
	        case 6:
	            if (!preg_match('/^6011[ \-]?\d{4}[ \-]?\d{4}[ \-]?\d{4}$/', $credit_card_number)) {
	                echo 'This is not a valid Discover card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	            }
	            break;
	        default:
	            echo 'This is not a valid credit card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	    } //END Switch statement

		//Luhn Algorithm
	    $credit_card_number = str_replace('.', '', $credit_card_number);
		$credit_card_number = str_replace('-', '', $credit_card_number);
		$credit_card_number = str_replace(' ', '', $credit_card_number);
	    $map = array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
	                0, 2, 4, 6, 8, 1, 3, 5, 7, 9);
	    $sum = 0;
	    $last = strlen($credit_card_number) - 1;

	    for ($i = 0; $i <= $last; $i++) {
	        $sum += $map[$credit_card_number[$last - $i] + ($i & 1) * 10];
	    }

	    if ($sum % 10 != 0) {
	       //echo '\n This is not a valid credit card number. Please use a different credit/debit card or re-enter your credit/debit card details.';
	    } else {
	       //If we made it this far the credit card number is in a valid format
	       echo 'This is a valid credit card number' ;
	    }
		exit;
	}
}
?>