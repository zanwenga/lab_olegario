<?php

defined('_JEXEC') or die('Invalid access');
jimport('joomla.application.component.model');

class invoiceModelproduct extends JModelLegacy {

	function getProduct($id){

		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_product' );
        $query->where( 'id_product = '. $id );
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	
	}

	function getListProducts(){
		
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_product' );
        $query->order( 'id_product ASC');
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        return $result;
	}

	function setProduct($data){
        $row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->insertObject('#__invcmp_product', $row);
        return  $this->_db->insertid();
	}

	function updateProduct($id_product, $data){
		$data['id_product'] = $id_product;
		$row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->updateObject('#__invcmp_product', $row, 'id_product');
        return $result;
	}

	function deleteProduct($id){
		$query = $this->_db->getQuery(true);
		$query->delete('#__invcmp_product');
		$query->where('id_product = '. $id);
		$this->_db->setQuery($query);
		$result = $this->_db->execute();
		return $result;
	}
}

?>