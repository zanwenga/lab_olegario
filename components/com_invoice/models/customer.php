<?php

defined('_JEXEC') or die('Invalid access');
jimport('joomla.application.component.model');

class invoiceModelcustomer extends JModelLegacy {

	function getCustomer($id){

		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_customer' );
        $query->where( 'id_customer = '. $id );
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	}

	function getListCustomers(){
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_customer' );
        $query->order( 'id_customer ASC');
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        return $result;
	}

	function setCustomer($data){
        $row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->insertObject('#__invcmp_customer', $row);
        return  $this->_db->insertid();
	}

	function updateCustomer($id_customer, $data){
		$data['id_customer'] = $id_customer;
		$row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->updateObject('#__invcmp_customer', $row, 'id_customer');
        return $result;
	}

	function deleteCustomer($id){
		$query = $this->_db->getQuery(true);
		$query->delete('#__invcmp_customer');
		$query->where('id_customer = '. $id);
		$this->_db->setQuery($query);
		$result = $this->_db->execute();
		return $result;
	}
}

?>