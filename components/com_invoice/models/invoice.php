<?php

defined('_JEXEC') or die('Invalid access');
jimport('joomla.application.component.model');

class invoiceModelinvoice extends JModelLegacy {

	/* DETALLE DE FACTURA */

	function getInvoice($id){

		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_invoice' );
        $query->where( 'id_invoice = '. $id );
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	
	}

	function getListInvoices($id_customer){
		
		$query = $this->_db->getQuery(true);
        $items = array(
        	'#__invcmp_invoice.*',
        	'#__invcmp_customer.name',
        	'#__invcmp_customer.idExt AS idExtCustomer',
        	'#__invcmp_customer.cif',
        	'#__invcmp_invoice_status.detail AS status'
        );
		$query->select( $items );
        $query->from( '#__invcmp_invoice' );
        $query->join('INNER', '#__invcmp_customer ON #__invcmp_customer.id_customer = #__invcmp_invoice.id_customer' );
        $query->join('INNER', '#__invcmp_invoice_status ON #__invcmp_invoice.id_status_invoice = #__invcmp_invoice_status.id_status_invoice' );

        if($id_customer != 0){
        	$query->where( '#__invcmp_invoice.id_customer = '. $id_customer );
        }

        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        return $result;
	}

	function setInvoice($data){
        
        $row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->insertObject('#__invcmp_invoice', $row);
        return  $this->_db->insertid();
	}

	function updateInvoice($id_invoice, $data){
		$data['id_invoice'] = $id_invoice;
		$row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->updateObject('#__invcmp_invoice', $row, 'id_invoice');
        return $result;
	}

	function deleteInvoice($id_invoice){
		$query = $this->_db->getQuery(true);
		$query->delete('#__invcmp_invoice');
		$query->where('id_invoice = '. $id_invoice);
		$this->_db->setQuery($query);
		$result = $this->_db->execute();
		return $result;
	}

	/* DETALLE DE FACTURA */

	function getListInvoiceDetail($id_invoice){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_invoice_detail' );
        $query->where('id_invoice = '. $id_invoice);
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        return $result;
	}

	function getInvoiceDetail($id_detail){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_invoice_detail' );
        $query->where('id_detail = '. $id_detail);
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	}

	function setInvoiceDetail($data){
        $row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->insertObject('#__invcmp_invoice_detail', $row);
        return  $this->_db->insertid();
	}


	function updateInvoiceDetail($id_detail, $data){
		$data['id_detail'] = $id_detail;
		$row = new JObject();
        $row->setProperties($data);
        $result = $this->_db->updateObject('#__invcmp_invoice_detail', $row, 'id_detail');
        return $result;
	}

	function deleteInvoiceDetail($id_detail){
		$query = $this->_db->getQuery(true);
		$query->delete('#__invcmp_invoice_detail');
		$query->where('id_detail = '. $id_detail);
		$this->_db->setQuery($query);
		$result = $this->_db->execute();
		return $result;
	}

	/* STATUS */
	function getListInvoiceStatus(){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_invoice_status' );
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        
        return $result;
	}

	/* TAX */
	function getListInvoiceTaxs(){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_tax' );
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssocList();
        
        return $result;
	}

	function getInvoiceTax($id){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_tax' );
        $query->where('id_tax = '. $id);
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        return $result;
	}

	/* COMPANY */
	function getCompany(){
        
		$query = $this->_db->getQuery(true);
        $items = array('*');
		$query->select( $items );
        $query->from( '#__invcmp_company' );
        $query->where('id_company = 1');
        $this->_db->setQuery( $query );
        $result = $this->_db->loadAssoc();
        
        return $result;
	}
}

?>