<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-gb" lang="en-gb" dir="ltr">
  <head>
    <meta charset="utf-8">
    <title><?php echo $this->datos['idExt'] ?></title>
    <style>
	    html{
	    	margin:0px;
	    	padding:20px;
	    }
		.clearfix:after {
		  content: "";
		  display: table;
		  clear: both;
		}

		a {
		  color: #5D6975;
		  text-decoration: underline;
		}

		body {
		  position: relative;
		  width: 21cm;  
		  height: 29.7cm; 
		  margin: 0 auto; 
		  color: #001028;
		  background: #FFFFFF; 
		  font-family: Arial, sans-serif; 
		  font-size: 12px; 
		  /*font-family: Arial;*/
		}

		body { font-family: DejaVu Sans; }

		header {
		  padding: 10px 0;
		  margin-bottom: 30px;
		}

		#logo {
		  text-align: center;
		  margin-bottom: 10px;
		}

		#logo img {
		  width: 90px;
		}

		h1 {
		  border-top: 1px solid  #5D6975;
		  border-bottom: 1px solid  #5D6975;
		  color: #5D6975;
		  font-size: 2.4em;
		  line-height: 1.4em;
		  font-weight: normal;
		  text-align: center;
		  margin: 0 0 20px 0;
		  background: url(dimension.png);
		}

		#project {
		  float: left;
		}

		#project span {
		  color: #5D6975;
		  text-align: right;
		  width: 52px;
		  margin-right: 10px;
		  display: inline-block;
		  font-size: 0.8em;
		}

		#company {
		  float: right;
		  text-align: right;
		}

		#project div,
		#company div {
		  white-space: nowrap;        
		}

		table {
		  width: 100%;
		  border-collapse: collapse;
		  border-spacing: 0;
		  margin-bottom: 20px;
		}

		table tr:nth-child(2n-1) td {
		  background: #F5F5F5;
		}

		table th,
		table td {
		  text-align: center;
		}

		table th {
		  padding: 5px 20px;
		  color: #5D6975;
		  border-bottom: 1px solid #C1CED9;
		  white-space: nowrap;        
		  font-weight: normal;
		}

		table .service,
		table .desc {
		  text-align: left;
		}

		table td {
		  padding: 20px;
		  text-align: right;
		}

		table td.service,
		table td.desc {
		  vertical-align: top;
		}

		table td.unit,
		table td.qty,
		table td.total {
		  font-size: 1.2em;
		}

		table td.grand {
		  border-top: 1px solid #5D6975;;
		}

		#notices .notice {
		  color: #5D6975;
		  font-size: 1.2em;
		}

		footer {
		  color: #5D6975;
		  width: 100%;
		  height: 30px;
		  position: absolute;
		  bottom: 0;
		  border-top: 1px solid #C1CED9;
		  padding: 8px 0;
		  text-align: center;
		}
		*{
		  zoom: 95%;
		  }
    </style>
  </head>

  <body style="width:650px;">

  	<br style="clear:both;height:50px;">
  	<br style="clear:both;">
  	<br style="clear:both;">
    
    <header class="clearfix">
      <h1><?php echo $this->datos['idExt'] ?></h1>
      <div id="company" style="box-sizing:border-box;">
        <div><?php echo $this->datos['company']['name'] ?></div>
        <div><?php echo $this->datos['company']['cif'] ?></div>
        <div><?php echo $this->datos['company']['address']?>, (<?php echo $this->datos['company']['pc'] ?>) <?php echo $this->datos['company']['city'] ?><br> <?php echo $this->datos['company']['region'] ?>, <?php echo $this->datos['company']['country'] ?></div>
        <div><?php echo $this->datos['company']['phone1'] ?></div>
        <div><a href="mailto:<?php echo $this->datos['company']['email1'] ?>"><?php echo $this->datos['company']['email1'] ?></a></div>
      </div>
      <div id="project" style="box-sizing:border-box;">
        <div><span>CLIENTE</span><?php echo $this->datos['customer']['name'] ?></div>
        <div><span>CIF/NIF</span><?php echo $this->datos['customer']['cif'] ?></div>
        <div><span>DIRECCIÓN</span><?php echo $this->datos['customer']['address']?>, (<?php echo $this->datos['customer']['pc'] ?>) <?php echo $this->datos['customer']['city'] ?><br> <?php echo $this->datos['customer']['region'] ?>, <?php echo $this->datos['customer']['country'] ?></div>
        <div><span>EMAIL</span> <a href="mailto:<?php echo $this->datos['customer']['email1'] ?>"><?php echo $this->datos['customer']['email1'] ?></a></div>
        <div><span>FECHA</span><?php echo $this->datos['date_emision'] ?></div>
        <!--<div><span>DUE DATE</span> September 17, 2015</div>-->
      </div>
    </header>
	
	
	<br style="clear:both;">
	
    <main >
      <table style="margin-top:100px;">
        <thead>
          <tr>
            <th class="service">#</th>
            <th class="desc">DESCRIPCION</th>
            <th>PRECIO</th>
            <th>CANTIDAD</th>
            <th>TOTAL</th>
          </tr>
        </thead>
        <tbody>
        <?php foreach($this->datos['details'] as $detail){?>  
          <tr>
            <td class="service"><?php echo $detail['id_product']; ?></td>
            <td class="desc"><?php echo $detail['description']; ?></td>
            <td class="unit"><?php echo $detail['price']; ?> €</td>
            <td class="qty"><?php echo $detail['num_products']; ?></td>
            <td class="total"><?php echo round(((float)$detail['price'] * (int)$detail['num_products']),2);?> €</td>
          </tr>
        <?php }?>
          <tr>
            <td colspan="4">SUBTOTAL</td>
            <td class="total"><?php echo $this->datos['total_nt'];?> €</td>
          </tr>
          <tr>
            <td colspan="4"><?php echo $this->datos['taxs']['description'];?></td>
            <td class="total"><?php echo ((float)$this->datos['total_nt']*(float)$this->datos['taxs']['tax']); ?> €</td>
          </tr>
          <tr>
            <td colspan="4" class="grand total">TOTAL</td>
            <td class="grand total"><?php echo $this->datos['total_wt'];?> €</td>
          </tr>
        </tbody>
      </table>
    
    </main>
    
  </body>
</html>