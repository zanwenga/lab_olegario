InvCmp.directive('headerinvoicing', function() {
    return {
        restrict: 'E',
        controller: 'InitHeaderCtrl',
        templateUrl: 'views/header/main.html',
        link: function (scope, element, attr) {

		}
	}
});

InvCmp.directive('modal', function () {
    return {
        template: '<div class="modal fade">' + 
          '<div class="modal-dialog">' + 
            '<div class="modal-content">' + 
              '<div class="modal-header">' + 
                '<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>' + 
                '<h4 class="modal-title">{{ title }}</h4>' + 
              '</div>' + 
              '<div class="modal-body" ng-transclude><modaltype><modaltype></div>' + 
            '</div>' + 
          '</div>' + 
        '</div>',
        restrict: 'E',
        transclude: true,
        replace:true,
        scope:true,
        link: function(scope, element, attrs) {
            
            scope.title = attrs.title;
            scope.$watch(attrs.visible, function(value){
                if(value == true){
                    $(element).modal('show');
                }else{
                    $(element).modal('hide');
                }
            });
            
            $(element).on('shown.bs.modal', function(){
                scope.$apply(function(){
                console.log(scope.$parent[attrs.visible])
                    scope.$parent[attrs.visible] = true;
                    scope.type      = scope.$parent.type;
                    scope.content   = scope.$parent.content;
                });
            });

            $(element).on('hidden.bs.modal', function(){
                scope.$apply(function(){
                console.log(scope.$parent[attrs.visible]);
                    scope.$parent[attrs.visible] = false;
                    scope.contents  = {};
                    scope.type      = "";
                });
            });

        }
    };
});

InvCmp.directive('modaltype', function () {
    return {
        templateUrl: 'views/modal/modal.html',
        restrict: 'E',
        scope: false,
        link:function(scope, element, attrs){
            scope.type = attrs.info;
            console.log('SCOPE DENTRO DEL MODAL TYPE', scope );
        }
    };
});

InvCmp.directive('modalcustomer', function ($rootScope, MainInvoicesFactory,  DTOptionsBuilder,DTColumnDefBuilder,PropertiesInvoiceFactory) {
    return {
        templateUrl: 'views/modal/type/customer.html',
        restrict: 'E',
        scope: false,
        link:function(scope, element, attrs){
            var mc = {};
            mc.dtOptions = DTOptionsBuilder.newOptions();
            mc.dtOptions.withPaginationType('full_numbers');
            mc.dtOptions.withBootstrap();
            mc.dtOptions.withOption('order', [1, 'asc']).withLanguage(PropertiesInvoiceFactory.getLanguageTables());
            mc.dtColumnDefs = [];

            scope.modalcustomer = mc;
            scope.type = attrs.info;
            scope.customers = scope.content.customers;
            
            console.log('MODAL CUSTOMER', scope);
            scope.selected = -1;

            scope.selectRow = function(id_customer){
                scope.selected = id_customer;
            }

            scope.selectCustomer = function(){
                
                console.log( 'Guardar cliente', scope.selected );
                var customer = {};
                var obj = _.findWhere(scope.customers, {'id_customer':scope.selected});
                console.log('FIND', obj)
                angular.copy(obj, customer);
                scope.$parent.formData.customer = customer;
                scope.$parent.formData.id_customer = customer.id_customer;
                scope.$parent.$parent.showModal = false;
            }
        }
    };
});

InvCmp.directive('modalproduct', function ($rootScope, MainInvoicesFactory, DTOptionsBuilder,DTColumnDefBuilder,PropertiesInvoiceFactory) {
    return {
        templateUrl: 'views/modal/type/product.html',
        restrict: 'E',
        scope: false,
        link:function(scope, element, attrs){
            var mp = {};
            mp.dtOptions = DTOptionsBuilder.newOptions();
            mp.dtOptions.withPaginationType('full_numbers');
            mp.dtOptions.withBootstrap();
            mp.dtOptions.withOption('order', [1, 'asc']).withLanguage(PropertiesInvoiceFactory.getLanguageTables());
            mp.dtColumnDefs = [];

            scope.modalproduct = mp;
            scope.type = attrs.info;
            scope.products = scope.content.products;
            
            console.log('MODAL PRODUCTS', scope);
            scope.selected = -1;

            scope.selectRow = function(id_product){
                scope.selected = id_product;
            }
            
            scope.selectProduct = function(){
                
                console.log( 'Guardar producto', scope.selected );
                var product = {};
                var obj = _.findWhere(scope.products, {'id_product':scope.selected});
                
                angular.copy(obj, product);
                scope.$parent.formData = MainInvoicesFactory.transformToDetail(scope.$parent.formData, product);
                scope.$parent.$parent.showModal = false;
            }
        }
    };
});


InvCmp.directive('compile', [
    '$compile',
    function ($compile) {
        return function (scope, element, attrs) {
            scope.$watch(function (scope) {
            return scope.$eval(attrs.compile);
        }, function (value) {
            element.html(value);
            $compile(element.contents())(scope);
        });
    };
  }
]);