InvCmp.controller('MainProductCtrl', [
	'$scope',
	'$location',
	'ProductService',
	'$rootScope',
	'DTOptionsBuilder',
	'DTColumnDefBuilder',
	'PropertiesInvoiceFactory',
	function($scope, $location, ProductService, $rootScope, DTOptionsBuilder,DTColumnDefBuilder,PInvoiceFactory){
		
    	var dtOptions = DTOptionsBuilder.newOptions();
		dtOptions.withPaginationType('full_numbers');
		dtOptions.withBootstrap();
        dtOptions.withOption('order', [1, 'asc']).withLanguage(PInvoiceFactory.getLanguageTables());
		var dtColumnDefs = [];

		$scope.dtOptions 	= dtOptions;
		$scope.dtColumnDefs = dtColumnDefs;
		$rootScope.showModal = false;
	    $scope.toggleModal = function(){
	        $rootScope.showModal = !$rootScope.showModal;
	    };

		$scope.options 			= [
			{'code': 'edit',  'name':'Editar'}, 
			{'code': 'show',  'name':'Visualizar'}, 
			{'code': 'delete','name':'Eliminar'}
		];	
		
		ProductService.getListProducts().then(function(res){
			console.log('RESPUESTA DE getListProducts', res)
			$scope.products = res;
		});

		$scope.selectAction = function(option, idProduct){
			
			console.log('SELECCIONADO', option.code);
			
			switch(option.code){
				case 'show':
					$location.path('/product/form/'+idProduct+'/show/');	
				break;
				case 'edit':
					$location.path('/product/form/'+idProduct+'/edit/');
				break;
				case 'delete':
					ProductService.deleteProduct(idProduct).then(function(res){
						console.log('ELIMINADO');
						$location.path('/product/main/');
					});
					
				break;
			}
		}

		$scope.new = function(){

			$location.path('/product/form/');
		};
	}
]);

InvCmp.controller('FormProductCtrl', [
	'$scope',
	'$location',
	'ProductService',
	'$routeParams',
	function($scope, $location, ProductService, $routeParams){
		
		$scope.formData = {};
		$scope.title = 'Nuevo producto';
		$scope.show = ($routeParams.type == 'show')?true:false;
		
		var isEditing = $routeParams.id_product || null;


		if(isEditing != null){
			$scope.title = 'Editar producto';
			ProductService.getProduct($routeParams.id_product).then(function(res){
				$scope.formData = res;
			});
		}

		$scope.submit = function(){
			console.log('DATOS DEL FORMULARIO', $scope.formData);
			ProductService.setProduct($scope.formData).then(function(res){
				console.log(res);
				$location.path('/product/main/');
			});
		}

		$scope.showList = function(){
			$location.path('/product/main/');
		}
	}
]);
