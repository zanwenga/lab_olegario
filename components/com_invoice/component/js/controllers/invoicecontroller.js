InvCmp.controller('MainInvoiceCtrl', [
	'$scope',
	'$location',
	'InvoiceService',
	'$rootScope',
	'MainInvoicesFactory',
	'$rootScope',
	'Paths',
	'DTOptionsBuilder',
	'DTColumnDefBuilder',
	'PropertiesInvoiceFactory',
	function($scope, $location, InvoiceService, $rootScope, MainInvoicesFactory,$rootScope,Paths, DTOptionsBuilder,DTColumnDefBuilder,PInvoiceFactory){
		
		console.log('MainInvoicesFactory', MainInvoicesFactory);
		var dtOptions = DTOptionsBuilder.newOptions();
		dtOptions.withPaginationType('full_numbers');
		dtOptions.withBootstrap();
        dtOptions.withOption('order', [1, 'asc']).withLanguage(PInvoiceFactory.getLanguageTables());
		var dtColumnDefs = [];

		$scope.dtOptions 	= dtOptions;
		$scope.dtColumnDefs = dtColumnDefs;
		$rootScope.showModal 	= false;
		$scope.info 		= '';
	    
	    $scope.toggleModal = function(){
	        $rootScope.showModal = !$rootScope.showModal;
	    };

		$scope.options 			= [
			{'code': 'edit',  	'name':'Editar'}, 
			{'code': 'show',  	'name':'Visualizar'}, 
			{'code': 'delete',	'name':'Eliminar'}
		];	
		
		InvoiceService.getListInvoices().then(function(res){
			console.log('RESPUESTA DE getListInvoices', res)
			$scope.invoices = res;
		});

		$scope.selectAction = function(option, idInvoice, idExt){
			
			console.log('SELECCIONADO', option.code);
			
			switch(option.code){
				case 'show':
					var url = Paths.pathBase()+'components/com_invoice/invoices/'+idExt+'.pdf';
					window.open(url);	
				break;
				case 'edit':
					$location.path('/invoice/form/'+idInvoice+'/edit/');
				break;
				case 'delete':
					InvoiceService.deleteInvoice(idInvoice).then(function(res){
						console.log('ELIMINADO');
						$location.path('/invoice/main/');
					});
					
				break;
			}
		}

		$scope.new = function(){

			$location.path('/invoice/form/');
		};
	}
]);

InvCmp.controller('FormInvoiceCtrl', [
	'$scope',
	'$location',
	'InvoiceService',
	'$routeParams',
	'ProductService',
	'CustomerService',
	'MainInvoicesFactory',
	'$rootScope',
	function($scope, $location, InvoiceService, $routeParams, ProductService, CustomerService, MainInvoicesFactory, $rootScope){
		
		$scope.formData 	= {};
		$scope.title 		= 'Nueva factura';
		$scope.show 		= ($routeParams.type == 'show')?true:false;
		$scope.showModal	= false;
		$scope.info 		= '';
		$scope.products 	= [];
		$scope.customers 	= [];
		$scope.content 		= {};
		$scope.taxs 		= MainInvoicesFactory.listTaxs();
		$scope.status 		= MainInvoicesFactory.listStatus();

		var isEditing = $routeParams.id_invoice || null;
		var formData = {};

		if(isEditing != null){

			$scope.title = 'Editar Factura';
			console.log('EDITA FACTURA')
			InvoiceService.getInvoice($routeParams.id_invoice).then(function(res){
				console.log('getInvoice', res);

				formData 				= res;
				formData.deleteDetails 	= [];
				
				var indext 				= _.findLastIndex($scope.taxs.options, {'id_tax': res.id_tax});
				$scope.taxs.selected 	= $scope.taxs.options[indext];
				formData.taxs 			= $scope.taxs.selected;

				var indexs 				= _.findLastIndex($scope.status.options, {'id_status_invoice': res.id_status_invoice});
				$scope.status.selected 	= $scope.status.options[indexs];
				formData.status			= $scope.status.selected;

				//Contenido del formulario
				$scope.formData 		= formData;
			});
		}else{

			formData.id_invoice 	= '-1';
			formData.id_customer 	= '-1';
			formData.id_company 	= '1';
			formData.id_tax 		= '1';
			formData.id_status_invoice = '1';
			formData.deleteDetails 	= [];
			formData.details 		= [];
			formData.date_emision 	= new Date();
			formData.date_payment 	= "";
			formData.date_create 	= MainInvoicesFactory.reformatDate(new Date());
			formData.total_nt 		= 0;
			formData.total_wt 		= 0;
			
			//Contenido del formulario
			$scope.formData  				= formData;
			$scope.taxs.selected 			= $scope.taxs.options[0];
			$scope.status.selected 			= $scope.status.options[0];
		}

		console.log('LISTADO DE IMPUESTOS', MainInvoicesFactory.listTaxs());
		
		$scope.$watch('taxs.selected', function(oldValue, newValue){
			$scope.formData.taxs 	= $scope.taxs.selected;
			$scope.formData 		= MainInvoicesFactory.updateInvoice($scope.formData);
			console.log('UPDATE INVOICE', $scope.formData);
		}, true);

		$scope.$watch('status.selected', function(oldValue, newValue){
			$scope.formData.status 	= $scope.status.selected;
			$scope.formData 		= MainInvoicesFactory.updateInvoice($scope.formData);
			console.log('UPDATE INVOICE', $scope.formData);
		}, true);

		$scope.$watch('formData', function(oldValue, newValue){
			$scope.formData = MainInvoicesFactory.updateInvoice($scope.formData);
			console.log('UPDATE INVOICE', $scope.formData);
		}, true);

		$scope.submit = function(){
			$scope.formData.date_emision 		= MainInvoicesFactory.reformatDate($scope.formData.date_emision);
			$scope.formData.id_status_invoice 	= $scope.formData.status.id_status_invoice;
			$scope.formData.id_tax 				= $scope.formData.taxs.id_tax;
			console.log('DATOS DEL FORMULARIO', $scope.formData);
			var jsonInvoice = JSON.stringify($scope.formData);
			InvoiceService.setInvoice(jsonInvoice).then(function(res){
				console.log(res);
				console.log('GUARDADO');
				$location.path('/invoice/main/');
			});
		}
		$scope.showList = function(){

			$location.path('/invoice/main/');
		}
		$scope.deleteDetail = function(index, event){
			event.preventDefault();
			var deleteObj = {};
			angular.copy($scope.formData.details[index], deleteObj);
			if(parseInt(deleteObj.id_detail) != -1){
				$scope.formData.deleteDetails.push(deleteObj);	
			}
			$scope.formData.details = _.without($scope.formData.details, $scope.formData.details[index]);
		}
		$scope.showListCustomers = function(event){
			event.preventDefault();
			CustomerService.getListCustomers().then(function(customers){
				
				$scope.content.customers 	= customers;
				$scope.type 		= 'customer';
                $scope.showModal 	= !$scope.showModal;    
				console.log('EN CONTROLADOR PRINCIPAL:',$scope);
			});
		}
		$scope.showListProducts = function(event){
			event.preventDefault();
			ProductService.getListProducts().then(function(products){
				$scope.content.products = products;
				$scope.type 			= 'product';
				$scope.showModal 	= !$scope.showModal;    
				console.log('PRODUCTOS PRINCIPAL scope' , $scope);
			});
		}
	}
]);