InvCmp.controller('MainCustomerCtrl', [
	'CustomerService',
	'$scope',
	'$location',
	'DTOptionsBuilder',
	'DTColumnDefBuilder',
	'PropertiesInvoiceFactory',
	function(CustomerService, $scope, $location, DTOptionsBuilder,DTColumnDefBuilder,PInvoiceFactory){
		
		var dtOptions = DTOptionsBuilder.newOptions();
		dtOptions.withPaginationType('full_numbers');
		dtOptions.withBootstrap();
        dtOptions.withOption('order', [1, 'asc']).withLanguage(PInvoiceFactory.getLanguageTables());
		var dtColumnDefs = [];

		$scope.dtOptions 	= dtOptions;
		$scope.dtColumnDefs = dtColumnDefs;
		$scope.options 			= [
			{'code': 'edit',  'name':'Editar'}, 
			{'code': 'show',  'name':'Visualizar'}, 
			{'code': 'delete','name':'Eliminar'}
		];	
		
		CustomerService.getListCustomers().then(function(res){
			console.log('RESPUESTA DE getListCustomers', res)
			$scope.customers = res;
		});


		$scope.selectAction = function(option, idCustomer){
			
			console.log('SELECCIONADO', option.code);
			
			switch(option.code){
				case 'show':
					$location.path('/customer/form/'+idCustomer+'/show/');	
				break;
				case 'edit':
					$location.path('/customer/form/'+idCustomer+'/edit/');
				break;
				case 'delete':
					CustomerService.deleteCustomer(idCustomer).then(function(res){
						console.log('ELIMINADO');
						$location.path('/customer/main/');
					});
				break;
			}

		}

		$scope.new = function(){

			$location.path('/customer/form/');
		};
	}
]);


InvCmp.controller('FormCustomerCtrl', [
	'$scope',
	'$location',
	'CustomerService',
	'$routeParams',
	function($scope, $location, CustomerService, $routeParams){

		$scope.formData = {};
		$scope.title = 'Nuevo cliente';
		var isEditing = $routeParams.id_customer || null;


		if(isEditing != null){
			$scope.title = 'Editar cliente';
			CustomerService.getCustomer($routeParams.id_customer).then(function(res){
				console.log('EDITA CUSTOMER', res);
				$scope.formData = res;
			});
		}

		$scope.showList = function(){
			$location.path('/customer/main/');
		};

		$scope.submit = function(){
			console.log('DATOS DEL FORMULARIO', $scope.formData);
			CustomerService.setCustomer($scope.formData).then(function(res){
				console.log(res);
				$location.path('/customer/main/');
			});
		}
	}
]);
