InvCmp.controller('InitHeaderCtrl', [
	'$scope',
	'$location',
	function($scope, $location){

		$scope.redirect = function(section){
			
			switch(section){
				case 'listCustomer':
					$location.path('/customer/main/');
				break;
				case 'newCustomer':
					$location.path('/customer/form/');
				break;
				case 'listProduct':
					$location.path('/product/main/');
				break;
				case 'newProduct':
					$location.path('/product/form/');
				break;
				case 'listInvoice':
					$location.path('/invoice/main/');
				break;
				case 'newInvoice':
					$location.path('/invoice/form/');
				break;
				default:

			}

		}
	}
]);