InvCmp.factory('Paths', [
	function(){

		var self 		= this;
		var base 		= null;
		var basePath 	= null;
		var controller 	= null;
		
		return{
			initBasePath: function(basePath, base){
				self.basePath 		= basePath || 'http://localhost/j35/';
				self.base 			= base  || 'http://localhost/j35/index.php?option=com_invoice'
				console.log('Path base:', self.base)
				return base;
			},
			pathBase: function(){
				return self.basePath;
			},
			pathBaseController: function(){
				return self.base;
			},
			
			/* Customer */
			getCustomer: function(){
				return self.base+'&controller=Customer&task=showCustomer&format=raw';
			},
			getListCustomers: function(){
				return self.base+'&controller=Customer&task=showListCustomers&format=raw';
			},
			setCustomer: function(){
				return self.base+'&controller=Customer&task=saveCustomer&format=raw';
			},
			deleteCustomer: function(){
				return self.base+'&controller=Customer&task=deleteCustomer&format=raw';
			},

			/* Product */
			getProduct: function(){
				return self.base+'&controller=Product&task=showProduct&format=raw';
			},
			getListProducts: function(){
				return self.base+'&controller=Product&task=showListProducts&format=raw';
			},
			setProduct: function(){
				return self.base+'&controller=Product&task=saveProduct&format=raw';
			},
			deleteProduct: function(){
				return self.base+'&controller=Product&task=deleteProduct&format=raw';
			},

			/* Invoice */
			getInvoice: function(){
				return self.base+'&controller=Invoice&task=showInvoice&format=raw';
			},
			getListInvoices: function(){
				return self.base+'&controller=Invoice&task=showListInvoices&format=raw';
			},
			setInvoice: function(){
				return self.base+'&controller=Invoice&task=saveInvoice&format=raw';
			},
			deleteInvoice: function(){
				return self.base+'&controller=Invoice&task=deleteInvoice&format=raw';
			},

			/* Details */
			getListInvoiceDetails: function(){
				return self.base+'&controller=Invoice&task=getListInvoiceDetail&format=raw';
			},
			setInvoiceDetail: function(){
				return self.base+'&controller=Invoice&task=saveInvoiceDetail&format=raw';
			},
			deleteInvoiceDetail: function(){
				return self.base+'&controller=Invoice&task=deleteInvoiceDetail&format=raw';
			},

			/* Status */
			getListInvoiceStatus: function(){
				return self.base+'&controller=Invoice&task=showListInvoiceStatus&format=raw';
			},

			/* Taxs */
			getListInvoiceTaxs: function(){
				return self.base+'&controller=Invoice&task=showListInvoiceTaxs&format=raw';
			},
		}
	}
]);

InvCmp.factory('Config', [
	'MainInvoicesFactory',
	function(MainInvoicesFactory){
		return {
			initConfig:function(){
				MainInvoicesFactory.init();
			}
		}
	}
]);

InvCmp.factory('MainInvoicesFactory', 
	function (ProductService, CustomerService, InvoiceService) {
	  	
	  	var self 		= this;
	  	var status 		= [];
	  	var taxs 		= [];

		var init = function(){
			InvoiceService.getListInvoiceStatus().then(function(resStatus){
				status = resStatus;
			});
			InvoiceService.getListInvoiceTaxs().then(function(resTaxs){
				taxs = resTaxs;
				console.log('OBTENGO ESTO ',resTaxs,'Y LO METO AQUI',self.taxs)
			});
	  	};
	  	var getTaxs = function(){
	  		var obj = {};
	  		obj.options = taxs;
	  		obj.selected = {};
	  		return obj;
	  	};
	  	var getStatus = function(){
	  		var obj = {};
	  		obj.options = status;
	  		obj.selected = {};
	  		return obj;
	  	};
	  	var updateInvoice = function(formData){
	  		
			var totalNoTax 		= 0;
			var totalWithTax 	= 0;
			var tax 			= parseFloat(formData.taxs.tax);
			if(formData.details){
				for(var i=0;i<formData.details.length;i++){
					
					var numProducts = parseInt(formData.details[i].num_products);
					var price 		= parseFloat(formData.details[i].price);	
					var totalPrice  = Math.round((numProducts * price)*100)/100;;

					console.log('DATOS FACTURA', numProducts, price, tax, totalPrice);
					
					formData.details[i].price 			= parseFloat(price.toFixed(2));
					formData.details[i].num_products 	= numProducts;
					formData.details[i].total_nt 		= parseFloat(totalPrice.toFixed(2));
					var wt 								= totalPrice + totalPrice * tax;
					formData.details[i].total_wt 		= parseFloat(wt.toFixed(2)); 

					console.log('DATOS FACTURA 2', totalPrice, (formData.details[i].total_wt));
					
					totalNoTax 		+= formData.details[i].total_nt;
					totalWithTax	+= formData.details[i].total_wt;
				}

				formData.total_wt = parseFloat(totalWithTax.toFixed(2));
				formData.total_nt = parseFloat(totalNoTax.toFixed(2));
			}
			return formData;
	  	};
	  	var transformToDetail = function(formData, product){
	  		
	  		console.log('PRODUCTO', product);
	  		console.log('DETALLE', formData.details[0]);

	  		var detail 	= {};
	  		var price 	= Math.round(parseFloat(product.price)*100)/100;
			var pricewt = price+price* parseFloat(formData.taxs.tax);
	  		
	  		detail.description 	= product.description;
	  		detail.id_detail 	= "-1";
	  		detail.id_invoice 	= formData.id_invoice;
	  		detail.id_product	= product.id_product;
	  		detail.num_products = 1;
	  		detail.price 		= price;
	  		detail.total_nt 	= price;
	  		detail.total_wt 	= Math.round(pricewt*100)/100;
	  		
	  		formData.details.push(detail);
	  		return formData;

	  	};
	  	var reformatDate = function(date ){
		    var d 		= new Date(date),
		        month 	= '' + (d.getMonth() + 1),
		        day 	= '' + d.getDate(),
		        year 	= d.getFullYear();

		    if (month.length < 2) month = '0' + month;
		    if (day.length < 2) day = '0' + day;
		    return [year, month, day].join('-');
	  	};

		return {
			init: 				init,
			updateInvoice: 		updateInvoice,
			transformToDetail: 	transformToDetail,
			listStatus: 		getStatus,
	  		listTaxs: 			getTaxs,
	  		reformatDate: 		reformatDate,
	  	}
	}
);


InvCmp.factory('PropertiesInvoiceFactory', 
	function(){
		return {
			getLanguageTables: function(){
				return {
					"sProcessing":     "",//Procesando...
					"sLengthMenu":     "Mostrar _MENU_ registros",
					"sZeroRecords":    "No se encontraron resultados",
					"sEmptyTable":     "Ning&Uacute;n dato registro en esta tabla",
					"sInfo":           "",
					"sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
					"sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
					"sInfoPostFix":    "",
					"sSearch":         "Buscar:",
					"sUrl":            "",
					"sInfoThousands":  ",",
					"sLoadingRecords": "Cargando...",
					"oPaginate": {
						"sFirst":    "<<",
						"sLast":     ">>",
						"sNext":     ">",
						"sPrevious": "<"
					},
					"oAria": {
						"sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
						"sSortDescending": ": Activar para ordenar la columna de manera descendente"
					}
				}
			}
		}
		
	}
)



InvCmp.factory('Utils', [
	function(){
		this.getURLParameter = function(string, parametro){
    		return this.getGETString(string)[ parametro ] || null;
	    }
	    
	    this.getGETString = function(string){
	    	var get;
	        if( typeof get === "undefined" ){
	            get = {};
	            var loc = string;
	            if(loc.indexOf('?')>0){
	                var getString = loc.split('?')[1];
	                var GET = getString.split('&');
	                for(var i = 0, l = GET.length; i < l; i++){
	                    var tmp = GET[i].split('=');
	                    get[ tmp[0] ] = unescape( decodeURI(tmp[1]) );
	                }
	            }
	            return get;
	        }else{
	            return get;
	        }
	    }
	    return this;
	}
]);