
var InvCmp = angular.module('InvoicesComponent', ['ngRoute','ngAnimate','datatables','datatables.bootstrap']);

InvCmp.config([
	'$routeProvider',
		function($routeProvider) {
		
			$routeProvider.
				when('/invoice/main/:id_customer?', {
					templateUrl: 'views/invoice/main.html',
					controller: 'MainInvoiceCtrl'
				}).
				when('/invoice/form/:id_invoice?/:type?/', {
					templateUrl: 'views/invoice/form.html',
					controller: 'FormInvoiceCtrl'
				}).
				when('/customer/main', {
					templateUrl: 'views/customer/main.html',
					controller: 'MainCustomerCtrl'
				}).
				when('/customer/form/:id_customer?/:type?/', {
					templateUrl: 'views/customer/form.html',
					controller: 'FormCustomerCtrl'
				}).
				when('/product/main', {
					templateUrl: 'views/product/main.html',
					controller: 'MainProductCtrl'
				}).
				when('/product/form/:id_product?/:type?/', {
					templateUrl: 'views/product/form.html',
					controller: 'FormProductCtrl'
				}).
				otherwise({

					redirectTo: '/invoice/main'
				});
		
		}
]);

InvCmp.run([
	'Config',
	'Paths',
	'Utils',
	'$rootScope',
	function(Config, Paths, Utils, $rootScope) {
		
		var urlComponent 	= window.location.href;
		
		var baseUrl 		= Utils.getURLParameter(urlComponent, 'base');
		var config = {
			'param1': Utils.getURLParameter(urlComponent, 'param1')
		};
		$rootScope.showModal = false;
		Paths.initBasePath(baseUrl);
		Config.initConfig(config);
		console.log('RUN');
	}
]);