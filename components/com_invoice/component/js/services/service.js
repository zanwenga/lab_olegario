InvCmp.service('InvoiceService', [
	'Paths',
	'$http',
	'$q',
	function(Paths, $http , $q){
		return{

			setInvoice: function(json){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= {'json':json};
				var req = {
					method: 'POST',
					url: Paths.setInvoice(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('setInvoice', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			},

			getListInvoices:function(data){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
	        	var data 		= {};
				
				var req = {
					method: 'POST',
					url: Paths.getListInvoices(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getListInvoices', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;				
			},

			getInvoice: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.getInvoice(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getInvoice', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					var b = results.data.date_emision.split(/\D/);
  					results.data.date_emision =	new Date(b[0], --b[1], b[2]);
  					console.log('FECHA', results.data.date_emision);
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			},

			deleteInvoice: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.deleteInvoice(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('deleteInvoice', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			},

			//DETAIL
			
			
			//STATUS
			getListInvoiceStatus:function(data){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
	        	var data 		= {};
				
				var req = {
					method: 'POST',
					url: Paths.getListInvoiceStatus(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getListInvoiceStatus', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;				
			},
			
			//TAX
			getListInvoiceTaxs:function(data){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
	        	var data 		= {};
				
				var req = {
					method: 'POST',
					url: Paths.getListInvoiceTaxs(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getListInvoiceTaxs', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;				
			},
		}
	}
]);

InvCmp.service('ProductService', [
	'Paths',
	'$http',
	'$q',
	function(Paths, $http , $q){
		return{

			setProduct: function(data){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				
				var req = {
					method: 'POST',
					url: Paths.setProduct(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('setProduct', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;

			},

			getListProducts:function(){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
	        	var data 		= {};
				
				var req = {
					method: 'POST',
					url: Paths.getListProducts(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getListProducts', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;				
			},

			getProduct: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.getProduct(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getProduct', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;

			},

			deleteProduct: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.deleteProduct(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('deleteProduct', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;

			}
		}
	}
]);

InvCmp.service('CustomerService', [
	'Paths',
	'$http',
	'$q',
	function(Paths, $http , $q){
		return{

			setCustomer: function(data){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				
				var req = {
					method: 'POST',
					url: Paths.setCustomer(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('setCustomer', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			},

			getListCustomers:function(){

				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= {};
				var req = {
					method: 'POST',
					url: Paths.getListCustomers(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getListCustomer', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;				
			},

			getCustomer: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.getCustomer(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('getCustomer', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			},

			deleteCustomer: function(id){
				
				var defered 	= $q.defer();
	        	var promise 	= defered.promise;
	        	var response 	= null;
				var data 		= { "id" : id };
				var req = {
					method: 'POST',
					url: Paths.deleteCustomer(),
					data: $.param(data),
					headers: {'Content-Type': 'application/x-www-form-urlencoded'}
				};

				console.log('deleteCustomer', req);
				
				$http(req).then(function(results){
					console.log('ACIERTO!!!');
					console.log( results.data );
					defered.resolve(results.data);
				}, function(){
					console.log('ERROR!!!');
					defered.resolve(response);
				});
				return promise;
			}


		}
	}
]);