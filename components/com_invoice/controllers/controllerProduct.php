<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class invoiceControllerProduct extends JControllerLegacy {

    private $model = null;

    function __construct(){
        parent::__construct();
        $this->model = $this->getModel('product');
    }

    public function showProduct(){
        $idProduct = JRequest::getInt('id');
        $product   = $this->model->getProduct($idProduct);
        echo json_encode($product);
    }

    public function showListProducts(){

        $product   = $this->model->getListProducts();
        echo json_encode($product);
    }

    public function saveProduct(){
        $idProduct = JRequest::getInt('id_product');
        $data       = array(
            'description'   =>JRequest::getString('description'),
            'stock'         =>JRequest::getInt('stock'),
            'price'         =>JRequest::getFloat('price')
        );

        if($idProduct != null){
            //UPDATE
            $res = $this->model->updateProduct($id_product, $data);
        }else{
            //INSERT
            $id             = $this->model->setProduct($data);
            $data['idExt']  = $this->getLargeId($id);
            $res = $this->model->updateProduct($id, $data);
        }
        $response = array(
            "response" => (is_null($res))?false:true
        );
        
        echo json_encode($response);
    }

    public function deleteProduct(){
        $idProduct = JRequest::getInt('id');
        $this->model->deleteProduct($idProduct);
        $response = array(
            "response" => true
        );
        echo json_encode($response);
    }

    private function getLargeId($id){
        $maxLength = (11 - strlen('PRD-'));
        $idExt = 'PD-' . str_pad($id, $maxLength, '0', STR_PAD_LEFT);
        return $idExt;
    }
}

?>