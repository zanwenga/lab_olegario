<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class invoiceControllerMain extends JControllerLegacy {


    function __construct(){

        parent::__construct();
    }
    
    public function index(){
        
        $baseController             = JURI::root().'index.php?option=com_invoice';
        $base                       = JURI::root();
        $baseServer                 = JURI::root(true);
        $datos['pathComponent']     = JURI::root().'components'.DS.'com_invoice'.DS.'component'.DS.'index.html?base='.$base.'&basePath='.$baseController . '&pathServer=' . $baseServer;

        $view = $this->getView('mainPanel', 'html');
        $view->datos = $datos;
        $view->display();
        
    }
}

?>