<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');
require_once(JPATH_COMPONENT . DS.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;



class invoiceControllerInvoice extends JControllerLegacy {

    private $m_invoice  = null;

    function __construct(){
        parent::__construct();
        $this->m_invoice    = $this->getModel('invoice');
        $this->m_customer   = $this->getModel('customer');
        $this->m_product    = $this->getModel('product');
    }

    /* INVOICE */

    public function showInvoice(){
        $idInvoice = JRequest::getInt('id');
        
        $invoice   = $this->m_invoice->getInvoice($idInvoice);
        $company   = $this->m_invoice->getCompany();
        $details   = $this->m_invoice->getListInvoiceDetail($invoice['id_invoice']);
        $taxs      = $this->m_invoice->getInvoiceTax($invoice['id_tax']);
        $customer  = $this->m_customer->getCustomer($invoice['id_customer']);
        
        $invoice['details']     = $details;
        $invoice['customer']    = $customer;
        $invoice['taxs']        = $taxs; 
        $invoice['company']     = $company;
        
        echo json_encode($invoice);
    }

    public function showListInvoices(){
        $idCustomer = JRequest::getInt('id_customer');
        $invoice   = $this->m_invoice->getListInvoices($idCustomer);
        echo json_encode($invoice, true);
    }   

    public function saveInvoice(){
        
        $json       = JRequest::getString('json');
        $obj        = json_decode($json);
        $product    = array();
        //FACTURA
        $id_invoice = null;
        $idExt      = null;
        //Pagado
        $date_payment = null;
        if($obj->id_status_invoice == '3'){
            $date_payment = date('Y-m-d');
        }

        $data = array(
            'total_nt'          => $obj->total_nt,
            'total_wt'          => $obj->total_wt,
            'id_company'        => $obj->id_company,
            'id_customer'       => $obj->id_customer,
            'id_status_invoice' => $obj->id_status_invoice,
            'date_payment'      => $date_payment,
            'date_emision'      => $obj->date_emision,
            'date_create'       => $obj->date_create,
            'id_tax'            => $obj->id_tax,
        );

        if($obj->id_invoice != "-1"){
            //EDITA
            $id_invoice = $obj->id_invoice;
            $res = $this->m_invoice->updateInvoice($id_invoice, $data);
        }else{
            //CREA
            $id_invoice     = $this->m_invoice->setInvoice($data);
            $data['idExt']  = $this->getLargeId($id_invoice);
            $res            = $this->m_invoice->updateInvoice($id_invoice, $data);
        }

        $idExt = $this->getLargeId($id_invoice);;
        //DETALLES DE FACTURA
        //1.- Elimina posibles
        foreach($obj->deleteDetails as $detail){
            if($detail->id_detail != '-1'){
                $detailBefore = $this->m_invoice->getInvoiceDetail($detail->id_detail);

                $this->m_invoice->deleteInvoiceDetail($detail->id_detail);
                $stock = $detail->num_products;
                //Suma productos
                $product    = $this->m_product->getProduct($detail->id_product);
                $stock      = (int)$product['stock'] + (int)$detailBefore['num_products'];
                $update     = array('stock'=> $stock);
                $this->m_product->updateProduct($detail->id_product, $update);
            }
        }
        //2.- Inserta/Actualiza Detalles + updatea productos
        foreach($obj->details as $detail){
            $id_detail = $detail->id_detail;
            $data = array(
                'description'   => $detail->description,
                'price'         => $detail->price,
                'id_product'    => $detail->id_product,
                'id_invoice'    => $id_invoice,
                'num_products'  => $detail->num_products,
            );
            if($id_detail != '-1'){
                //EDITA
                $detailBefore = $this->m_invoice->getInvoiceDetail($id_detail);
                $this->m_invoice->updateInvoiceDetail($id_detail, $data);
                
                //ACTUALIZA STOCK DE PRODUCTOS
                //ahora - antes -> suma
                $product    = $this->m_product->getProduct($detail->id_product);
                $stock      = (int)$product['stock'] + ( (int)$detail->num_products - (int)$detailBefore['num_products'] );
                $update     = array('stock'=> $stock);
                $this->m_product->updateProduct($detail->id_product, $update);
            }else{
                //INSERT
                $this->m_invoice->setInvoiceDetail($data);
                //resta
                $product    = $this->m_product->getProduct($detail->id_product);
                $stock      = (int)$product['stock'] - (int)$detail->num_products;
                $update     = array('stock'=> $stock);
                $this->m_product->updateProduct($detail->id_product, $update);
            }
        }
        
        $this->generateInvoicePdf($id_invoice, $idExt);
        $response = array(
            "response" => (is_null($res))?false:true
        );
        echo json_encode($response);
    }

    public function getInvoiceTemplateHtml(){
        
        $id_invoice = JRequest::getInt('id_invoice');
        $invoice   = $this->m_invoice->getInvoice($id_invoice);

        $company   = $this->m_invoice->getCompany();
        $details   = $this->m_invoice->getListInvoiceDetail($invoice['id_invoice']);
        $taxs      = $this->m_invoice->getInvoiceTax($invoice['id_tax']);
        $customer  = $this->m_customer->getCustomer($invoice['id_customer']);
        
        $invoice['details']     = $details;
        $invoice['customer']    = $customer;
        $invoice['taxs']        = $taxs; 
        $invoice['company']     = $company;

        $view = $this->getView('mainPanel', 'html');
        $view->datos = $invoice;
        $view->display('pdf');
    }

    private function getHtml($id){
        ob_start();
        echo file_get_contents(JURI::root().'index.php?option=com_invoice&controller=Invoice&task=getInvoiceTemplateHtml&id_invoice='.$id.'&format=raw');
        $html = ob_get_contents();
        ob_end_clean();

        return $html;
    }

    private function generateInvoicePdf($id, $idExt){
        
        $html = $this->getHtml($id);
        $dompdf = new Dompdf();
        $dompdf->set_option('defaultFont', 'Arial');
    
        $dompdf->load_html($html);
        $dompdf->render();
        $pdf    = $dompdf->output();
        $file   = JPATH_COMPONENT . DS .'invoices/'.$idExt.'.pdf';
        if(is_file($file)){
            unlink($file);
        }
        file_put_contents($file, $pdf);


     
        return true;
    }

    public function deleteInvoice(){
        $idInvoice = JRequest::getInt('id');
        $this->m_invoice->deleteInvoice($idInvoice);
        $response = array(
            "response" => true
        );
        echo json_encode($response);
    }

    /* INVOICE DETAIL */

    public function getListInvoiceDetail(){
        $idInvoice = JRequest::getInt('id');
        $details   = $this->m_invoice->getListInvoiceDetail($idInvoice);
        echo json_encode($details);   
    }

    public function saveInvoiceDetail(){
        $idDetail = JRequest::getInt('id_detail');
        $data = array(
            'description'   => JRequest::getString('description'),
            'price'         => JRequest::getFloat('price'),
            'id_invoice'    => JRequest::getInt('id_invoice'),
            'id_product'    => JRequest::getInt('id_product'),
            'id_tax'        => JRequest::getInt('id_tax'),
        );
        if($idDetail != null && $idDetail != 0){
            //UPDATE
            $res = $this->m_invoice->updateInvoiceDetail($idDetail, $data);
        }else{
            //INSERT
            $id             = $this->m_invoice->setInvoiceDetail($data);
        }
        $response = array(
            "response" => (is_null($res))?false:true
        );
        
        echo json_encode($response);
    }

    public function deleteInvoiceDetail(){
        $idDetail = JRequest::getInt('id');
        $this->m_invoice->deleteInvoiceDetail($idDetail);
        $response = array(
            "response" => true
        );
        echo json_encode($response);   
    }

    /* STATUS */
    public function showListInvoiceStatus(){

        $status  = $this->m_invoice->getListInvoiceStatus();
        echo json_encode($status);
    }

    /* TAX */
    public function showListInvoiceTaxs(){

        $taxs  = $this->m_invoice->getListInvoiceTaxs();
        echo json_encode($taxs);
    }

    /* PRIVATE METHODS */

    private function getLargeId($id){
        $maxLength = (18 - strlen(date('Y').'-INV-'));
        $idExt = date('Y'). '-INV-' . str_pad($id, $maxLength, '0', STR_PAD_LEFT);
        return $idExt;
    }
}

?>