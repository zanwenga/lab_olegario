<?php

defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.controller');

class invoiceControllerCustomer extends JControllerLegacy {

    private $model = null;

    function __construct(){
        parent::__construct();
        $this->model = $this->getModel('customer');
    }

    public function showCustomer(){
        $idCustomer = JRequest::getInt('id');
        $customer   = $this->model->getCustomer($idCustomer);
        echo json_encode($customer);
    }

    public function showListCustomers(){
        $customer   = $this->model->getListCustomers();
        echo json_encode($customer);
    }

    public function saveCustomer(){
        $idCustomer = JRequest::getInt('id_customer');
        $data       = array(
            'name'          =>JRequest::getString('name'),
            'cif'           =>JRequest::getString('cif'),
            'address'       =>JRequest::getString('address'),
            'city'          =>JRequest::getString('city'),
            'region'        =>JRequest::getString('region'),
            'pc'            =>JRequest::getString('pc'),
            'country'       =>JRequest::getString('country'),
            'contact_name'  =>JRequest::getString('contact_name'),
            'phone1'        =>JRequest::getString('phone1'),
            'phone2'        =>JRequest::getString('phone2'),
            'fax'           =>JRequest::getString('fax'),
            'email1'        =>JRequest::getString('email1'),
            'email2'        =>JRequest::getString('email2'),
            'observations'  =>JRequest::getString('observations'),
        );

        if($idCustomer != null){
            //UPDATE
            $res = $this->model->updateCustomer($idCustomer, $data);
        }else{
            //INSERT
            $id             = $this->model->setCustomer($data);
            $data['idExt']  = $this->getLargeId($id);
            $res = $this->model->updateCustomer($id, $data);
        }
        $response = array(
            "response" => (is_null($res))?false:true
        );
        
        echo json_encode($response);
    }

    public function deleteCustomer(){
        $idCustomer = JRequest::getInt('id');
        $this->model->deleteCustomer($idCustomer);
        $response = array(
            "response" => true
        );
        echo json_encode($response);
    }

    private function getLargeId($id){
        $maxLength = (11 - strlen('CST-'));
        $idExt = 'CST-' . str_pad($id, $maxLength, '0', STR_PAD_LEFT);
        return $idExt;
    }
}

?>