<?php
//No permite el acceso directo
defined('_JEXEC') or die;

if(!defined('DS')){
	define('DS', DIRECTORY_SEPARATOR);
}

require_once(JPATH_COMPONENT . DS.'library/pdfCreator.php');
/* ----------------------------------- */
/* AQUI VAN TODA LA CARGA DE LIBRERIAS */
/* ----------------------------------- */

//require_once (JPATH_COMPONENT . DS . 'helpers/CheckUser.php'); 
//$document 	= JFactory::getDocument();
//$app 		= JFactory::getApplication();
//$document->addscript(JURI::root(true).DS.'components'.DS.'com_invoice'.DS.'resources'.DS.'js'.DS.'jquery.js');
//$document->addStyleSheet(JURI::root(true).DS.'components'.DS.'com_invoice'.DS.'resources'.DS.'css'.DS.'styles.css','text/css',null,array());
//$check 		= new CheckUser();
//$user 		= JFactory::getUser();
//$arrayCheck = $check->getGruposUsuarioByName($user->name);

$task 		= JRequest::getWord('task');
if($task != ""){
	$taskMethod = $task;
}else{
	$taskMethod = 'index';
}

$controller = JRequest::getWord('controller');
$controller = ($controller == "")?'Main':$controller;
if($controller != ""){
	$classname 			= 'invoiceController'.$controller;
	require_once JPATH_COMPONENT . DS . 'controllers' . DS .'controller'.$controller.'.php';
}else{
	$classname 			= 'invoiceController';
	require_once JPATH_COMPONENT . DS . 'controller.php';
}


//$pdfCreator = new pdfCreator();
//$pdfCreator->getPdf3();

$controller = new $classname();
$controller->execute($taskMethod);

?>