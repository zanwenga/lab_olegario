<?php
require_once(JPATH_COMPONENT . DS.'library/dompdf/autoload.inc.php');
use Dompdf\Dompdf;

class pdfCreator {

	public function __construct() {

	}
	
	private function getHtml() {
		ob_start();
		include(JPATH_COMPONENT . DS . 'library/template.php');
		$retStr = ob_get_contents();
		ob_end_clean();
		return $retStr;
	}
	
	public function getPdf() {		
		

		$html = $this->getHtml();
		$dompdf = new  Dompdf();
		print_r($html);
		$html = "hola";
		$dompdf->loadHtml($html);
		$dompdf->render();
		$output = $dompdf->output(); 
			
		$pdfFileName = JPATH_COMPONENT . DS .'library/invoice3.pdf';
		file_put_contents($pdfFileName, $output);
	}

	public function getPdf2(){
		$html = $this->getHtml();
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		//$dompdf->setPaper('A4', 'landscape');
		$dompdf->render();
		$dompdf->stream();	
	}

	public function getPdf3(){
		$html = $this->getHtml();
		$dompdf = new Dompdf();
		$dompdf->loadHtml($html);
		//$dompdf->setPaper('A4', 'landscape');
		$dompdf->render();

		$pdf 	= $dompdf->output();
		$file 	= JPATH_COMPONENT . DS .'library/saved_888.pdf';
		$myfile = fopen($file, "w") or die("Unable to open file!");
		fwrite($myfile, $pdf);
		fclose($myfile);
	}
	
}
